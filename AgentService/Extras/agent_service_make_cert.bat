@rem "C:\Program Files (x86)\Windows Kits\8.0\bin\x86\makecert.exe"

@if ""%1""=="" goto usage
@if ""%2""=="" goto usage
@netsh http add urlacl url=https://+:%2/ user=EVERYONE
@if errorlevel 1 goto error
@set as_cert_name=InnovatorAgentService
@makecert.exe -sk RootCA -sky signature -pe -n CN=%1 -r -sr LocalMachine -ss Root %as_cert_name%.cer
@if errorlevel 1 goto error
@makecert.exe -sk server -sky exchange -pe -n CN=%1 -ir LocalMachine -is Root -ic %as_cert_name%.cer -sr LocalMachine -ss My %as_cert_name%Server.cer
@if errorlevel 1 goto error
@Aras.Utility.AgentServiceInstaller.exe %as_cert_name%Server.cer %2
@if errorlevel 1 goto error
:error
@exit /b %errorlevel%
:usage
@echo Usage: agent_service_make_cert.bat "<host name>" "<TCP port number>"