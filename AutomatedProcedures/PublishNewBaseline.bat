@echo off

echo PublishNewBaseline.bat:
echo Target audience: Deployment engineer;
echo Purpose: Create CodeTree.zip and DB.bak based on current innovator state and publishes it using Path.To.CodeTree.Zip and Path.To.DB.Bak properties.
echo Note: The procedure backups old baseline.

SET NAntTargetsToRun=Publish.CodeTree.Backup Publish.DB.Backup
SET PathToThisBatFileFolder=%~dp0
SET NAntParameters=%*

CALL "%PathToThisBatFileFolder%BatchUtilityScripts\SetupExternalTools.bat
IF errorlevel 1 GOTO END

"%PathToNantExe%" "/f:%PathToThisBatFileFolder%NantScript.xml" %NAntTargetsToRun%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause