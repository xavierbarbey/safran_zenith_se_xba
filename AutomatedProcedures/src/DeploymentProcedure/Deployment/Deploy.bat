@echo off

call %~dp0AutomatedProcedures\BatchUtilityScripts\CheckAdminPrivileges.bat 
if errorlevel 1 goto end

if not exist %~dp0\Deployment-Package-Content (
	echo Folder Deployment-Package-Content not found - please extract Deployment-Package-Content.zip
	goto end
)

set PathToDeploymentProcedureFolder="%~dp0AutomatedProcedures\tools\DeploymentProcedure"
set PathToInstanceDeployConfig="%~dp0InstanceTemplate.deploy.xml"
set /P PathToInstanceDeployConfig=Enter PathToInstanceDeployConfig (It describes Innovator instance structure and deployment steps. Default value is '%PathToInstanceDeployConfig%'):

set OriginalPath="%cd%"
cd %PathToDeploymentProcedureFolder%

DeploymentProcedure.exe -t Deploy -c %PathToInstanceDeployConfig%

cd %OriginalPath%

:end
pause