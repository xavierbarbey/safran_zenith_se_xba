﻿using DeploymentProcedure.Steps.Base;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using DeploymentProcedure.Components.Base;

namespace DeploymentProcedure.Steps
{
	[XmlType("setup")]
	public class SetupStep : BaseStep
	{
		private IList<string> _componentsIdList;

		[XmlAttribute("components")]
		public string ComponentsIds { get; set; }

		public IList<string> ComponentsIdList => _componentsIdList ?? (_componentsIdList = ComponentsIds.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries));

		public bool ContainsComponent(string componentId)
		{
			return ComponentsIdList.Contains(componentId);
		}

		public override void Execute(IReadOnlyCollection<Component> instanceComponents)
		{
			if (instanceComponents == null)
			{
				throw new ArgumentNullException(nameof(instanceComponents));
			}

			foreach (Component component in instanceComponents)
			{
				if (ComponentsIdList.Contains(component.Id))
				{
					component.RunPreSetupValidation();
				}
			}

			foreach (Component component in instanceComponents)
			{
				if (ComponentsIdList.Contains(component.Id))
				{
					component.Setup();
				}
			}
		}
	}
}
