﻿using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Components.Type;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Utility;
using DeploymentProcedure.Utility.WebAdministration;
using DeploymentProcedure.Utility.WebAdministration.Base;
using System.Globalization;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("web")]
	public class WebComponent : CodeTreeComponent
	{
		private string _url;

		protected IWebAdministration WebAdministration { get; } = new WebAdministrationWrapper();

		public string VirtualDirectoryPath { get; set; }
		public string ApplicationPoolName { get; set; }
		public string ManagedRuntimeVersion { get; set; } = "v4.0";
		public string ManagedPipelineMode { get; set; } = "Integrated";
		public string SiteName { get; set; } = "Default web Site";
		public string Protocol { get; set; } = "http";
		public string ApplicationPoolUser { get; set; }
		public SecretString ApplicationPoolUserPassword { get; set; }
		public string Url
		{
			get { return _url ?? string.Format(CultureInfo.InvariantCulture, "{0}://{1}{2}", Protocol, ServerName, VirtualDirectoryPath); }
			set { _url = value; }
		}

		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			WebAdministration.SetupApplicationPool(ServerName, ApplicationPoolName, ManagedRuntimeVersion, ManagedPipelineMode, ApplicationPoolUser, ApplicationPoolUserPassword);
			WebAdministration.SetupApplication(ServerName, SiteName, VirtualDirectoryPath, ApplicationPoolName, InstallationPath);
		}
		#endregion

		#region Implementing Cleanup logic
		public override void Remove()
		{
			Logger.Instance.Log(LogLevel.Info, "\nRemoving component ({0}):\n", Id);

			WebAdministration.RemoveApplicationPoolWithApplications(ServerName, ApplicationPoolName);

			base.Remove();
		}
		#endregion

		#region Implementing ApplyPackage
		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckWebApplicationAvailability(this);
		}
		#endregion
	}
}
