﻿using DeploymentProcedure.Utility;
using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("oauth")]
	public class OAuthComponent : WebComponent
	{
		public override string BaselineSourcePath { get; set; } = Path.Combine(Properties.PathToCodeTree, "OAuthServer");
		public override string DeploymentPackageDirectoryName { get; set; } = "OAuthServer";
		public string InnovatorUrl { get; set; }
		public override string PathToCodeTreeTemplates => Path.Combine(Properties.PathToTemplates, "OAuthServer");
		public override string PathToConfig => Path.Combine(InstallationPath, "Web.config");

		public override void Setup()
		{
			base.Setup();

			SetupNtfsPermissionsToFolder(Path.Combine(InstallationPath, "App_Data"));
			WebAdministration.SetupWinAuth(ServerName, SiteName + VirtualDirectoryPath + "/signin-windows");
			TargetFileSystem.XmlHelper.XmlPoke(PathToConfig, "/configuration/appSettings/add[@key = 'InnovatorServerUrl']/@value", InnovatorUrl);
		}

		public override void Remove()
		{
			WebAdministration.RemoveWinAuth(ServerName, SiteName + VirtualDirectoryPath + "/signin-windows");

			base.Remove();
		}
	}
}
