﻿using DeploymentProcedure.Logging;
using DeploymentProcedure.Packages;
using DeploymentProcedure.Utility;
using DeploymentProcedure.Utility.FileSystem;
using DeploymentProcedure.Utility.FileSystem.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Ionic.Zip;

namespace DeploymentProcedure.Components.Base
{
	public abstract class CodeTreeComponent : Component
	{
		[XmlIgnore]
		internal IFileSystem LocalFileSystem => FileSystemFactory.Local;
		[XmlIgnore]
		internal IFileSystem TargetFileSystem => FileSystemFactory.GetFileSystem(ServerName);

		public virtual string BaselineSourcePath { get; set; }
		public virtual string DeploymentPackageDirectoryName { get; set; }
		public string InstallationPath { get; set; }
		public string ServerName { get; set; } = InitializeServerNameFromEnvironment() ?? "localhost";
		public virtual string PathToCodeTreeTemplates { get; }
		public virtual string PathToConfig { get; set; }
		public virtual string PathToConfigTemplate { get; }
		public virtual string PathToBasicConfig { get; }

		#region Implementing Setup logic
		public override void RunPreSetupValidation()
		{
			base.RunPreSetupValidation();

			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckWritePermissionsToDirectory(TargetFileSystem, InstallationPath);
		}

		public override void Setup()
		{
			base.Setup();

			LocalFileSystem.CopyDirectory(BaselineSourcePath, TargetFileSystem, InstallationPath);
			SetupConfig();
			SetupCodeTreeTemplates();
		}

		protected void SetupConfigFromTemplate(string pathToConfig, string pathToTemplate)
		{
			if (LocalFileSystem.FileExists(pathToTemplate))
			{
				if (!TargetFileSystem.FileExists(pathToConfig))
				{
					Logger.Instance.Log(LogLevel.Info, "\tCreating config '{0}' from '{1}' template",
						TargetFileSystem.GetFullPath(pathToConfig),
						LocalFileSystem.GetFullPath(pathToTemplate));

					LocalFileSystem.CopyFile(pathToTemplate, TargetFileSystem, pathToConfig, true);
				}

				ExpandEnvironmentVariablesInConfig(TargetFileSystem.GetFullPath(pathToConfig));
			}
		}

		private void ExpandEnvironmentVariablesInConfig(string pathToConfig)
		{
			Logger.Instance.Log(LogLevel.Info, "\tLooking for properties to replace in the '{0}' file ...", pathToConfig);
			string configContents = TargetFileSystem.ReadAllText(pathToConfig);
			configContents = Properties.ExpandEnvironmentVariables(configContents);
			TargetFileSystem.WriteAllTextToFile(pathToConfig, configContents);
			Logger.Instance.Log(LogLevel.Info, "\tProperties replacement is complete.");
		}

		protected void SetupNtfsPermissionsToFolder(string pathToFolder)
		{
			if (!TargetFileSystem.DirectoryExists(pathToFolder))
			{
				TargetFileSystem.CreateDirectory(pathToFolder);
			}

			ProcessWrapper.Execute("icacls", "\"{0}\" /grant *S-1-5-11:(OI)(CI)(M)", TargetFileSystem.GetFullPath(pathToFolder));
		}

		private void SetupCodeTreeTemplates()
		{
			if (LocalFileSystem.DirectoryExists(PathToCodeTreeTemplates))
			{
				LocalFileSystem.CopyDirectory(PathToCodeTreeTemplates, TargetFileSystem, InstallationPath);

				IEnumerable<string> configFileTemplatess = LocalFileSystem.EnumerateFiles(PathToCodeTreeTemplates, recursive: true);
				foreach (string configFileTemplate in configFileTemplatess)
				{
					string configRelativePath = BaseFileSystem.GetRelativePath(LocalFileSystem, PathToCodeTreeTemplates, configFileTemplate);
					string configPath = TargetFileSystem.GetFullPath(BaseFileSystem.CombinePaths(InstallationPath, configRelativePath));
					ExpandEnvironmentVariablesInConfig(configPath);
				}
			}
		}

		private void SetupConfig()
		{
			Logger.Instance.Log(LogLevel.Info, "\nSetting up config for component ({0}):\n", Id);

			SetupConfigFromTemplate(PathToConfig, PathToConfigTemplate);

			if (!string.IsNullOrEmpty(PathToBasicConfig))
			{
				Logger.Instance.Log(LogLevel.Info, "\tCreating basic config '{0}'",
					TargetFileSystem.GetFullPath(PathToBasicConfig));

				string configContent = string.Format(CultureInfo.InvariantCulture, "<ConfigFilePath value=\"{0}\" />", PathToConfig);
				TargetFileSystem.WriteAllTextToFile(PathToBasicConfig, configContent);
			}
		}
		#endregion

		#region Implementing Cleanup logic
		public override void Remove()
		{
			if (TargetFileSystem.FileExists(PathToConfig))
			{
				TargetFileSystem.DeleteFile(PathToConfig);
			}

			if (TargetFileSystem.DirectoryExists(InstallationPath)
				&& !string.Equals(LocalFileSystem.GetFullPath(BaselineSourcePath), TargetFileSystem.GetFullPath(InstallationPath), StringComparison.OrdinalIgnoreCase))
			{
				TargetFileSystem.DeleteDirectory(InstallationPath);
			}
		}
		#endregion

		#region Implementing ApplyPackage
		public override void ApplyPackage(Package package)
		{
			if (package == null)
			{
				throw new ArgumentNullException(nameof(package));
			}

			base.ApplyPackage(package);

			string deploymentPackageSourcePath = BaseFileSystem.CombinePaths(package.PathToDeploymentPackage, DeploymentPackageDirectoryName);
			if (LocalFileSystem.DirectoryExists(deploymentPackageSourcePath))
			{
				LocalFileSystem.CopyDirectory(deploymentPackageSourcePath, TargetFileSystem, InstallationPath);
			}
			else
			{
				Logger.Instance.Log(LogLevel.Info, "\tNothing found to deploy to component ({0}):\n", Id);
			}
		}

		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckDirectoryExistence(TargetFileSystem, InstallationPath);
			ValidationHelper.CheckWritePermissionsToDirectory(TargetFileSystem, InstallationPath);
		}
		#endregion

		#region Impementing Backup logic
		public override void Backup(string pathToBackupDir)
		{
			base.Backup(pathToBackupDir);

			string pathToCodeTreeArchive = BaseFileSystem.CombinePaths(pathToBackupDir, "CodeTree.zip");
			using (ZipFile zipFile = new ZipFile(pathToCodeTreeArchive, Encoding.UTF8))
			{
				zipFile.AddDirectory(InstallationPath, Path.GetFileName(InstallationPath));
				zipFile.Save();
			}
		}
		#endregion

		private static string InitializeServerNameFromEnvironment()
		{
			string urlOfDeploymentServer = Environment.GetEnvironmentVariable("Url.Of.Deployment.Server");
			return !string.IsNullOrEmpty(urlOfDeploymentServer) ? new Uri(urlOfDeploymentServer).Host : null;
		}
	}
}
