﻿using DeploymentProcedure.Utility;
using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("conversion")]
	public class ConversionComponent : WebComponent
	{
		public string Name { get; set; } = "Default";
		public string ConversionServiceAsmxUrl => Url.TrimEnd('/') + "/ConversionService.asmx";

		#region Overriding CodeTreeComponent properties
		private string _pathToConfig;
		public override string PathToConfig
		{
			get { return _pathToConfig ?? Path.Combine(InstallationPath, "..\\ConversionServerConfig.xml"); }
			set { _pathToConfig = value; }
		}
		public override string PathToConfigTemplate => Path.Combine(Properties.PathToTemplates, "ConversionServerConfig.xml");
		public override string PathToBasicConfig => Path.Combine(InstallationPath, "ConversionServer.xml");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Properties.PathToCodeTree, "ConversionServer");
		public override string DeploymentPackageDirectoryName { get; set; } = "ConversionServer";
		#endregion
	}
}
