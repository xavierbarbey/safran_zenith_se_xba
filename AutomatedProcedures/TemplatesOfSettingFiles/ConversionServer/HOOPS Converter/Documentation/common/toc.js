//Add String.endsWith if necessary
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	}
}

//Lookup table for all table of contents links, in the form of 'somelink.html' => doc element
var allLinks = {};

function getTocAncestorWithTagName($element, tagName) {
	//Searches upward in the element hierarchy for an element with the specified tagName
	for (var $parent = $element.parent(); $parent.length > 0; $parent = $parent.parent()) {
		if ($parent.hasClass('toc_div')) {
			//Reached the top of the table of contents. Exit early
			break;
		}			
		
		var parentTagName = $parent.prop('tagName');		
		if (parentTagName && parentTagName.toLowerCase() === tagName.toLowerCase())
			return $parent;		
	}
	
	return null;
}

function selectAccordionSectionContainingElement($element) {
	//Selects ancestor accordion sections containing the specified element
	
	//Search upward for an accordion containing the element
	$section = getTocAncestorWithTagName($element, 'div');
	$accordion = $section ? $section.parent() : null;	
	if ($section && $accordion && $accordion.length > 0) {
		//Figure out the correct accordion index
		var sectionIndex = Math.floor($section.index() / 2);	
		$accordion.accordion({active : sectionIndex});			
		
		//Now recursively select the accordion parent containing this accordion (in case accordions are nested)
		selectAccordionSectionContainingElement($accordion);
	}
}

function tryToSelectTocLink(path) {
	//Tries to select a table of contents link for the specified path
	//This may be called multiple times in the course of trying to figure out what was clicked
	
	var handledLink = null; //The found link, if one was found. A null value indicates the link was not in the table of contents
	
	for (href in allLinks) {
		if (allLinks.hasOwnProperty(href)) {
			var $link = $(allLinks[href]);	
			if (href === path || href.endsWith('../' + path)) {	
				//Found the link
				handledLink = path;
				
				//Select all the ancestor accordions
				selectAccordionSectionContainingElement($link);
				
				//Add the 'selected' class to the styling
				if (!$link.hasClass('selected-link'))
					$link.addClass('selected-link');
			} else {
				//Ensure the link does not appear to be selected
				$link.removeClass('selected-link');
			}			
		}
	}
	
	return handledLink;
}

//Document ready handler
$(document).ready(function() {	
	//Create accordions
	$('.toc-container').accordion({
		collapsible: true,
		heightStyle: 'content',
		active: false,
		animate: { duration: 200 }
	});
	
	//Find all links and add click handlers
	$('.toc-container a').each(function() {
		var href = $(this).attr('href');
		if (href)
			allLinks[href] = this;
	}).click(function(e) {
		var href = $(this).attr('href');
		if (href && href.indexOf('http') != 0) {
			//A table of contents item was clicked. 			
			if (href.search('#') >= 0) {
				//An item with a fragment was clicked
				//It may or may not be a link on the current page. Select the link just in case it is and the page doesn't reload
				tryToSelectTocLink(href);
			}			
		}
	});
	
	//Try to select the full location, then successfully shorter locations, until a table of contents link is found
	var locationParts = window.location.href.split('/');
	for (var i = 0; i < locationParts.length; i++) {
		var path = locationParts.slice(i).join('/');
		if (tryToSelectTocLink(path) !== null) {
			//Successfully selected table of contents link
			break;
		}
	};
});