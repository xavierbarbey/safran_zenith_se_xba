//Lookup table for all table of contents links, in the form of 'somelink.html' => doc element
var allLinks = {};

function getAncestorWithTagName($element, tagName) {
	//Searches upward in the element hierarchy for an element with the specified tagName
	for (var $parent = $element.parent(); $parent.length > 0; $parent = $parent.parent()) {
		var parentTagName = $parent.prop('tagName');
		if (parentTagName && parentTagName.toLowerCase() === tagName.toLowerCase())
			return $parent;
	}
	
	return null;
}

function selectAccordionSectionContainingElement($element) {
	//Selects ancestor accordion sections containing the specified element
	
	//Search upward for an accordion containing the element
	$section = getAncestorWithTagName($element, 'div');
	$accordion = $section ? $section.parent() : null;	
	if ($section && $accordion && $accordion.length > 0) {
		//Figure out the correct accordion index
		var sectionIndex = Math.floor($section.index() / 2);	
		$accordion.accordion({active : sectionIndex});			
		
		//Now recursively select the accordion parent containing this accordion (in case accordions are nested)
		selectAccordionSectionContainingElement($accordion);
	}
}

function tryToSelectTocLink(path) {
	//Tries to select a table of contents link for the specified path
	//This may be called multiple times in the course of trying to figure out what was clicked
	
	var handledLink = null; //The found link, if one was found. A null value indicates the link was not in the table of contents
	
	for (href in allLinks) {
		if (allLinks.hasOwnProperty(href)) {
			var $link = $(allLinks[href]);	
			if (href === path) {		
				//Found the link
				handledLink = path;
				
				//Select all the ancestor accordions
				selectAccordionSectionContainingElement($link);
				
				//Add the 'selected' class to the styling
				if (!$link.hasClass('selected-link'))
					$link.addClass('selected-link');
			} else {
				//Ensure the link does not appear to be selected
				$link.removeClass('selected-link');
			}			
		}
	}
	
	return handledLink;
}

function _handleInternalLinkClick(path) {
	//Handler for a link click that was known to be within a topic page
	
	var result = null;
	
	//Was it a 'class members' link?
	if (!result) {	
		var match = new RegExp('/class.*-members\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/functions.html'));		
	}
	
	//Was it a 'functions' link (similar to class members)?
	if (!result) {
		var match = new RegExp('/functions.*\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/functions.html'));		
	}
	
	//Was it a 'class' link?
	if (!result) {
		var match = new RegExp('/class.*\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/classes.html'));		
	}
	
	//Was it a 'cpp source' link?
	if (!result) {
		var match = new RegExp('/cpp/.*_source\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/cpp/classes.html'));
	}
	
	//Was it a 'cs source' link?
	if (!result) {
		var match = new RegExp('/cs/.*\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/cs/classes.html'));
	}
	
	//Was it a 'deprecated' link?
	if (!result) {
		var match = new RegExp('/deprecated.*\\.html').exec(path);
		if (match)
			result = tryToSelectTocLink(path.replace(match, '/deprecated.html'));		
	}
	
	//Was it a 'guide/code' link?
	if (!result) {
		var guideCode = new RegExp('/guides/code/.*').exec(path);
		if (guideCode)
			result = tryToSelectTocLink(path.replace(guideCode, '/guides/sample_code.html'));
	}
	
	return result;
}

function handleLinkClick(path) {
	//Front end link click handler for any link click (whether it's in the table of contents, or in a topic page)
	
	var result = null;
	
	if (path.indexOf('file:///') == 0) {
		//Local file path. Iteratively re-compose paths without the 'file:///' part to see if a match could be found
		var parts = path.substring(8).split('/');
		for (var i = parts.length - 1; i > 0; i--) {
			var subpath = parts.slice(i).join('/');
			result = _handleInternalLinkClick(subpath) || tryToSelectTocLink(subpath);
			if (result)
				break;			
		}
	} else if (path.indexOf('http://') == 0) {
		//HTTP path. Iteratively re-compose paths without the 'http://' part to see if a match could be found
		var parts = path.substring(7).split('/');
		for (var i = parts.length - 1; i > 0; i--) {
			var subpath = parts.slice(i).join('/');
			result = _handleInternalLinkClick(subpath) || tryToSelectTocLink(subpath);
			if (result)
				break;			
		}
	} else if (path in allLinks) {
		//A path known to be in the table of contents
		result = tryToSelectTocLink(path);
	} else {
		//A path not in the table of contents
		result = _handleInternalLinkClick(path);
	}
	
	return result;
}

function handleMessage(e) {
	//Message handler
	
	var path = e.data.split('#')[0];
	handleLinkClick(path);
}

//Add message handler
if (window.attachEvent)
	window.attachEvent('onmessage', handleMessage);
else
	window.addEventListener('message', handleMessage, false);

//Document ready handler
$(document).ready(function() {	
	//Create accordions
	$('.toc-container').accordion({
		collapsible: true,
		heightStyle: 'content',
		active: false,
		animate: { duration: 200 }
	});
	
	//Find all links and add click handlers
	$('.toc-container a').each(function() {
		var href = $(this).attr('href');
		if (href)
			allLinks[href] = this;
	}).click(function(e) {
		var href = $(this).attr('href');
		if (href && href.indexOf('http') != 0) {
			//A table of contents item was clicked
			
			//Notify index page so it can handle the new topic page
			window.top.postMessage('path:' + href, '*');
			
			//Update table of contents
			handleLinkClick(href);
			
			//Prevent browser from loading clicked link into topic page
			e.preventDefault();
		}
	});
});