function joinPath(a, b) {
	//Joins two paths
	var aParts = a.split('/');
	var bParts = b.split('/');
	
	if (aParts.length > 0) 
		aParts.pop();
		
	for (var i = 0; i < bParts.length; i++) {
		if (bParts[i] == '..')
			aParts.pop();
		else
			aParts.push(bParts[i]);
	}
	
	return aParts.join('/');
}

function getQueryParameterByName(name) {
	//Gets a query string value
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(window.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function isPrinterFriendlyPage() {
	//Determines whether the page should be shown in a printer friendly manner
	return getQueryParameterByName('printerfriendly') === 'yes';
}

function createNotPrinterFriendlyUrl(url) {
	//Removes the printer friendly parts from the URL
	return url.replace('?printerfriendly=yes', '');
}

function notPrinterFriendlyUrl() {
	//Removes the printer friendly parts from the current page URL
	return createNotPrinterFriendlyUrl(window.location.href);
}

function createPrinterFriendlyUrl(notPrinterFriendlyUrl) {
	//Adds the printer friendly parts to the URL
	return notPrinterFriendlyUrl.replace('.html', '.html?printerfriendly=yes');
}

function printerFriendlyUrl() {
	//Adds the printer friendly parts to the current page URL
	return createPrinterFriendlyUrl(notPrinterFriendlyUrl());
}

function findPathToIndexPage() {
	//Figure out relative path to index.html
			
	//Find the 'common/topic.js' script reference
	var commonJsScript = $('script').filter(function() {
		return this.src && this.src.indexOf('common/topic.js') >= 0;
	});
	var src = commonJsScript.attr('src');
	
	//Extract all the movements upward in the directory structure
	var prefix = '';
	if (src) {
		while (true) {
			var pathOffset = src.indexOf('../');
			if (pathOffset != 0)
				break;
				
			prefix += '../';
			src = src.substring(3);
		}
		
		src = src.substring(0, pathOffset);			
	}
	
	return joinPath(window.location.href, prefix + 'index.html');
}

var pathToIndexPage = findPathToIndexPage();

function redirectToFullDocumentation() {
	//Redirect to index.html, passing in this page as the topic page
	window.top.location.href = pathToIndexPage + '#' + createNotPrinterFriendlyUrl(window.location.href);
}

function handleMessage(e) {
	//Message handler
	
	//An anchor was specified. Try to scroll to it
	var $target = $('a[name="' + e.data + '"]');
	if (!$target.length)
		$target = $('a[id="' + e.data + '"]');
	if ($target.length) {
		//Found the anchor
		$('html,body').animate({scrollTop: $target.offset().top}, 0);
	}
}

//Add message handler
if (window.attachEvent)
	window.attachEvent('onmessage', handleMessage);
else
	window.addEventListener('message', handleMessage, false);

//Document ready handler
$(document).ready(function() {	
	if (!window.top.toc) {
		if (isPrinterFriendlyPage()) {
			//Printer friendly page being shown. Add link to get back to full documentation
			$('body').prepend($('<div style="float: right;"><a href="' + pathToIndexPage + '#' + notPrinterFriendlyUrl() + '" class="full-documentation-link">Show full documentation</a></div>'));
		} else {	
			//Redirect
			redirectToFullDocumentation();			
		}
	} else {	
		//Full documentation being shown. Add link to get to a printer friendly topic page
		$('body').prepend($('<div style="float: right;"><a href="' + printerFriendlyUrl() + '" target="_parent">Show printer-friendly page</a></div>'));
	}

	$('.api-toggle').click(function(e) {
		//Handle the +/- button image clicks
		var divId = e.target.id.substring(0, e.target.id.length - '_link'.length + 1);
		
		document.getElementById(divId).style.display = (document.getElementById(divId).style.display == 'none') ? 'block' : 'none';	
	
		var imageElement = document.getElementById(divId + "_img");	
		imageElement.src = (imageElement.src.indexOf('plus.gif') >= 0) ? imageElement.src.replace('plus.gif', 'minus.gif') : imageElement.src.replace('minus.gif', 'plus.gif');
	
		e.preventDefault();
	});	

	$('a').click(function(e) {		
		var href = $(this).attr('href');
		if (href) {
			if (href === '#' || (href.charAt(0) === '#' && href.indexOf('Snippet') > 0)) {
				//A +/- or snippet was clicked. Ignore.
				e.preventDefault();
			} else if (window.top.toc) {
				//The full documentation is on display
				
				var target = $(this).attr('target');
				
				if (href.charAt(0) === '#') {
					//An anchor was clicked
					window.top.postMessage('anchor:' + href.substring(1), '*');					
					
					//Prevent browser from jumping to the anchor
					e.preventDefault();
				} else if (!target && href.indexOf('http') != 0 && href.indexOf('mail') != 0) {
					//An internal link was clicked
					window.top.postMessage('relativePath:' + href, '*');
					
					//Prevent the browser from loading the linked page
					e.preventDefault();
				}
			} else if ($(this).hasClass('full-documentation-link')) {
				//The 'show full documentation' link was clicked
				
				//Redirect
				redirectToFullDocumentation();			
			} else if (isPrinterFriendlyPage()) {
				//The current topic page is being shown in printer-friendly mode. Stay in that mode.
				window.location.href = createPrinterFriendlyUrl(createNotPrinterFriendlyUrl(href));
				e.preventDefault();
			}
		}
	});
});

//No longer used---------------------------------------------------------
var dge = document.getElementById;
function cl_expcol(a) {
	if (!dge) return;
	document.getElementById(a).style.display = (document.getElementById(a).style.display == 'none') ? 'block' : 'none';	
	swap_exp_col_img(a);
}

function swap_exp_col_img(imgID) {
	var e = document.getElementById(imgID + "_img");
	
	var backslashIndex = e.src.lastIndexOf('/');
	
	var filename = e.src.substring(backslashIndex);
	var path = e.src.substring(0, backslashIndex);
				
	if (filename == '/plus.gif')
		e.src = path + '/minus.gif';
	else
		e.src = path + '/plus.gif';
}
