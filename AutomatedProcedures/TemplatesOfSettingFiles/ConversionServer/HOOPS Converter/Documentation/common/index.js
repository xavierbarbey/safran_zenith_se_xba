function getTopicPageFromHash() {
	//Gets the topic page from the hash. For example, with http://www.techsoft3d.com/docs/index.html#about.html, it will get 'about.html'
	var hash = window.location.hash;
	if (hash && hash.charAt(0) == '#')
		hash = hash.substring(1);
	if (hash) {
		var parts = hash.split('#');
		return parts[0];
	}
	return null;
}

function getTopicAnchorFromHash() {
	//Gets the topic page anchor from the hash. For example, with http://www.techsoft3d.com/docs/index.html#about.html#bottom, it will get 'bottom'
	var hash = window.location.hash;
	if (hash && hash.charAt(0) == '#')
		hash = hash.substring(1);
	if (hash) {
		var parts = hash.split('#');
		if (parts.length > 1)
			return parts[1];
	}
	return null;
}

function joinPath(a, b) {
	//Joins two paths
	var aParts = a.split('/');
	var bParts = b.split('/');
	
	if (aParts.length > 0) 
		aParts.pop();
		
	for (var i = 0; i < bParts.length; i++) {
		if (bParts[i] == '..')
			aParts.pop();
		else
			aParts.push(bParts[i]);
	}
	
	return aParts.join('/');
}

function animateToTopicAnchor(topicAnchor) {
	//Notifies the topic page that it should scroll to the specified anchor
	if (window.topic && topicAnchor)
		window.topic.postMessage(topicAnchor, '*');	
}

function handleMessage(e) {
	//Message handler
	
	var eDataParts = e.data.split(':');
	var eDataKey = eDataParts[0];
	var eDataValue = eDataParts.slice(1).join(':');
	
	var currentPath;
	var hash;	
	if (eDataKey === 'path') {
		//Table of contents path specified
		currentPath = eDataValue;
		hash = '#' + eDataValue;
	} else if (eDataKey === 'relativePath') {
		//Path relative to the current topic specified
		currentPath = getTopicPageFromHash();
		
		if (currentPath) {
			currentPath = joinPath(currentPath, eDataValue);
			hash = '#' + currentPath;
		} else {
			currentPath = eDataValue;
			hash = '#' + currentPath;
		}
	} else if (eDataKey === 'anchor') {		
		//Anchor specified 
		
		//Don't use 'currentPath', since we're only going to adjust the hash
		var currentPathFromHash = getTopicPageFromHash();		
		if (currentPathFromHash) {
			//This should always be the case
			hash = '#' + currentPathFromHash + '#' + eDataValue;
		} else {
			hash = '#' + eDataValue;
		}
		
		//Notify topic page to scroll
		animateToTopicAnchor(eDataValue);
	}
	
	//Update history
	if (window.history && window.history.pushState)
		window.history.pushState('', '', hash);
	else
		window.location.hash = hash;
	
	//Navigate to new page
	if (currentPath) {
		window.topic.location.replace(currentPath);
		
		//Notify table of contents if an internal topic link was clicked on
		if (eDataKey === 'relativePath' && eDataValue && window.toc)
			window.toc.postMessage(currentPath, '*');
	}
}

//Add message handler
if (window.attachEvent)
	window.attachEvent('onmessage', handleMessage);
else
	window.addEventListener('message', handleMessage, false);

//Hash change handler
if ('onhashchange' in window) {
	function hashChange() {
		var topicPage = getTopicPageFromHash();
		var topicAnchor = getTopicAnchorFromHash();

		var currentPath = topicPage || defaultTopicPage;		
		if (currentPath) {
			//Append anchor if there is one
			if (topicAnchor)
				currentPath += '#' + topicAnchor;
			
			//Navigate to new page
			window.topic.location.replace(currentPath);
			
			//Notify table of contents
			window.toc.postMessage(topicPage || defaultTopicPage, '*');
			
			//Notify topic page
			if (window.topic && topicAnchor) {
				setTimeout(function() {
					animateToTopicAnchor(topicAnchor);
				}, 300);			
			}
		}
	}
	
	if (window.attachEvent)
		window.attachEvent('onhashchange', hashChange);
	else
		window.addEventListener('hashchange', hashChange, false);
}

//Document ready handler
$(document).ready(function() {
	var topicPage = getTopicPageFromHash();
	var topicAnchor = getTopicAnchorFromHash();

	if (topicPage) {
		//A topic page was specified
		
		//Concatenate the active page + anchor
		var topicPageAndAnchor = topicPage;	
		if (topicAnchor)
			topicPageAndAnchor += '#' + topicAnchor;
			
		var hash = '#' + topicPageAndAnchor;
		
		//Update history
		if (window.history && window.history.replaceState)
			window.history.replaceState('', '', hash);
		else
			window.location.hash = hash;
			
		//Navigate to new page
		window.topic.location.replace(topicPageAndAnchor);
		
		//Delay table of contents notification so that the table of contents has a chance to load
		if (window.toc) {
			setTimeout(function() {
				window.toc.postMessage(topicPage, '*');
			}, 500);			
		}
		
		//Notify topic page
		if (window.topic && topicAnchor) {
			setTimeout(function() {
				animateToTopicAnchor(topicAnchor);
			}, 300);			
		}
	} else if (defaultTopicPage) {
		//No topic page specified. Navigate to default topic page		
		
		var hash = '#' + defaultTopicPage;
		
		//Update history
		if (window.history && window.history.replaceState)
			window.history.replaceState('', '', hash);
		else
			window.location.hash = hash;
			
		//Navigate to new page
		window.topic.location.replace(defaultTopicPage);
		
		//Delay table of contents notification so that the table of contents has a chance to load
		if (window.toc) {
			setTimeout(function() {
				window.toc.postMessage(defaultTopicPage, '*');
			}, 500);			
		}
	}
});
