﻿(function() {
	var treePaneWidget = null;
	var containerWidget = null;
	var rm;
	var splitter;

	window.loadTree = function(callback) {
		const navContainer = document.getElementById('leading');
		const navComponent = document.createElement('aras-nav');
		navComponent.className = 'aras-nav-toc';
		navContainer.appendChild(navComponent);
		const contextMenu = new ArasModules.ContextMenu();
		callback(navComponent, contextMenu);
	};

	window.initSvgManager = function() {
		ArasModules.SvgManager.init();
	};

	window.loadStatusbar = function() {
		var resourceUrl = aras.getI18NXMLResource('statusbar.xml', aras.getBaseURL());
		return clientControlsFactory.createControl('Aras.Client.Controls.Experimental.StatusBar', {
			id: 'bottom_statusBar',
			aras: aras,
			resourceUrl: resourceUrl
		}).then(function(statusbarCtrl) {
			var bottomNode = document.getElementById('bottom');
			bottomNode.appendChild(statusbarCtrl.domNode);
			statusbarCtrl.startup();
			return clientControlsFactory.createControl('Aras.Client.Frames.StatusBar', {
				aras: aras,
				statusbar: statusbarCtrl
			});
		}).then(function(control) {
			window.statusbar = control;
		});
	};

	function populateTitlesForTogglePanel() {
		var togglePanel = document.getElementById('togglePanel');
		if (togglePanel) {
			togglePanel.firstElementChild.setAttribute('title', rm.getString('common.showMainTreeTooltip'));
		}
	}

	window.loadMainMenu = function(args, callback) {
		return clientControlsFactory.createControl('Aras.Client.Controls.Experimental.MainMenu', {
			id: 'menulayout_mainMenu',
			XML: args.xml,
			xmlArgType: args.xmlArgType,
			aras: args.aras,
			cuiContext: args.cuiContext
		}).then(function(menu) {
			menu.placeAt('menulayout', 'first');
			menu.startup();
			if (callback) {
				callback(menu);
			}
		});
	};

	window.hideOrShowTree = function() {
		var togglePanel = document.getElementById('togglePanel');
		togglePanel.classList.toggle('aras-hide');
		treePaneWidget.classList.toggle('aras-hide');
		splitter.classList.toggle('aras-hide');
		menu.setControlState('ShowMainTree', !treePaneWidget.classList.contains('aras-hide'));
	};

	window.isMainTreeShown = function() {
		return !treePaneWidget.classList.contains('aras-hide');
	};

	window.registerShortcutsAtMainWindowLocation = function(settings, itemTypeName, itemType) {
		if (settings) {
			var loadParams = {
				locationName: 'MainWindowShortcuts',
				'item_classification': '%all_grouped_by_classification%',
				itemTypeName: itemTypeName,
				itemType: itemType
			};

			window.cui.loadShortcutsFromCommandBarsAsync(loadParams, settings).then(function() {
				window.focus();
			});
		}
	};

	function checkCachingMechanism() {
		var checkCachingMechanismUrl = aras.getScriptsURL() + 'CheckCachingMechanism.aspx';
		var requestSettings = {
			url: checkCachingMechanismUrl,
			restMethod: 'GET',
			async: true
		};

		var firstRequestResponse;

		return ArasModules.soap('', requestSettings)
			.then(function(responseText) {
				firstRequestResponse = responseText;

				return ArasModules.soap('', requestSettings);
			})
			.then(function(secondRequestResponse) {

				return firstRequestResponse === secondRequestResponse;
			});
	}

	function disableFileDrop() {
		// disable drop file by all iframes in the window
		var prevent = function(e) {
			e.preventDefault();
		};
		[].forEach.call(window.document.querySelectorAll('iframe'), function(elm) {
			elm.contentWindow.addEventListener('drop', prevent);
			elm.contentWindow.addEventListener('dragover', prevent);
		});

		// disable drop file by window
		window.addEventListener('drop', prevent);
		window.addEventListener('dragover', prevent);
	}

	window.setUserName = function() {
		var userNameNode = document.getElementById('profileName');
		if (userNameNode) {
			var userData = aras.getLoggedUserItem();
			var userKeyedName = aras.getItemProperty(userData, 'keyed_name');
			userNameNode.textContent = (userKeyedName === userData.getAttribute('id')) ? aras.getLoginName() : userKeyedName;
		}
	};

	window.setUserImg = function() {
		var userImgNode = document.getElementById('profileImg');
		if (userImgNode) {
			var userData = aras.getLoggedUserItem();
			var pictItem = userData.selectSingleNode('picture');
			if (pictItem.getAttribute('is_null') !== '1') {
				var imgItemId = pictItem.text.replace('vault:///?fileId=', '');
				var imgurl = aras.IomInnovator.getFileUrl(imgItemId, aras.Enums.UrlType.SecurityToken);
				userImgNode.src = imgurl;
			}
		}
	};

	window.onLogoutCommand = function(event) {
		if (window.aras.isDirtyItems()) {
			if (window.parent !== parent.parent) {
				window.location.reload();
				return;
			}
			aras.dirtyItemsHandler(window);
		} else {
			window.setTimeout(function() {
				window.close();
			}, 0);
		}

		if (event) {
			event.preventDefault();
		}
	};

	/**
	 * Initialize main window. Called from onSuccessfulLogin function of login.aspx.
	 *
	 * @returns {boolean}
	 */
	window.initialize = function() {
		fixDojoSettings();
		initSvgManager();

		rm = new ResourceManager(new Solution('core'), 'ui_resources.xml', aras.getSessionContextLanguageCode());
		aras.setUserReportServiceBaseUrl(window.location.href.replace(/(\/Client?)(\/|$)(.*)/i, '$1') + '/../SelfServiceReporting');

		var userNd = aras.getLoggedUserItem(true);
		if (!userNd) {
			window.onbeforeunload = '';
			window.close();
			return false;
		}
		if (!document.frames) {
			document.frames = [];
		}

		aras.getPreferenceItemProperty('SSVC_Preferences', null, 'default_bookmark');
		aras.getPreferenceItemProperty('ES_Settings', null, 'max_analyzed_chars');

		var update = new InnovatorUpdate();
		update.BeginIsNeedCheckUpdates();

		containerWidget = document.getElementById('main-container');
		treePaneWidget = document.getElementById('leading');
		window.menu = document.frames.menu = window;
		window.tree = document.frames.tree = document.getElementById('tree').contentWindow;
		var workIframe = document.getElementById('work');
		window.work = workIframe.contentWindow;
		window.work.frameElement.addEventListener('load', function() {
			aras.browserHelper.toggleSpinner(document, false);
		});

		loadStatusbar();
		var shortcutSettings = {
			windows: [window, window.work],
			context: window
		};

		registerShortcutsAtMainWindowLocation(shortcutSettings);
		workIframe.addEventListener('load', function() {
			var workIframeShortcutSettings = {
				windows: [this.contentWindow],
				context: window
			};

			var itemTypeName;
			if (window.itemType) {
				itemTypeName = window.itemType.getProperty('name');
			}

			registerShortcutsAtMainWindowLocation(workIframeShortcutSettings, itemTypeName, window.itemType);
		}, false);

		checkCachingMechanism()
			.then(function(isCachingMechanismWork) {
				if (!isCachingMechanismWork) {
					aras.AlertError(rm.getString('setup.cache_is_disabled'));
				}
			})
			.catch(function(error) {
				console.error(error);
			});

		aras.UpdateFeatureTreeIfNeed();

		document.corporateToLocalOffset = aras.getCorporateToLocalOffset();
		updateBanners();
		showCorporateTime();
		PopulateDocByLabels();

		populateTitlesForTogglePanel();

		disableFileDrop();

		initializeMainMenu();
		window.tree.initialize();

		var mode = aras.getMainWindow().arasMainWindowInfo.ClientDisplayMode;
		if (aras.Browser.isEdge() || mode !== 'Windows') {
			window.arasTabs = new Tabs('main-tab');
		} else {
			document.querySelector('header').classList.add('window-mode');
		}

		setUserName();

		var userLogoutNode = document.getElementById('profileLogout');
		userLogoutNode.addEventListener('click', window.onLogoutCommand);

		setUserImg();

		splitter = document.getElementById('main-container-splitter');
		window.ArasModules.splitter(splitter);
		arasMainWindowInfo.setProvider(new SyncMainWindowInfoProvider());
    
		var activeItemID = setActiveItem();
		if(activeItemID != null){
			top.aras.uiShowItem("ZS_Project", activeItemID);
		}
		top.aras.AlertWarning('Please be aware that you are in the Production Environment!');
		
		return true;
	};
})();
