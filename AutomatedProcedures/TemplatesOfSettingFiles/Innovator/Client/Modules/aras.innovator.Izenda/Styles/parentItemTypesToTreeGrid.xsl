﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="text()|@*"/>
	<xsl:template match="/Result/Item">

			<xsl:variable name="node_id" select="concat(source_id/Item/instance_data, '/ItemTypes/', name)"/>
			<xsl:variable name="icon" select="'/images/checkbox-unchecked.svg'"/>

			<tr level="0" id="{$node_id}" icon0="{$icon}" icon1="{$icon}">
				<userdata key="className" value="tree-base-item-type-prop" />
				<userdata key="nodeType" value="3" />
				<userdata key="guid" value="" />
				<userdata key="columnName">
					<xsl:attribute name="value">
						<xsl:value-of select="name"/>
					</xsl:attribute>
				</userdata>
				<userdata key="format" value="rich" />
				<xsl:element name="userdata">
					<xsl:attribute name="key">itemTypeId</xsl:attribute>
					<xsl:attribute name="value">
						<xsl:value-of select="source_id/Item/id"/>
					</xsl:attribute>
				</xsl:element>
				<xsl:variable name="parent_prop_name">
					<xsl:choose>
						<xsl:when test="label != ''">
							<xsl:value-of select="label"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="name"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="parent_type_name">
					<xsl:choose>
						<xsl:when test="not(source_id/Item/label = '')">
							<xsl:value-of select="source_id/Item/label"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="source_id/Item/name"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>				
				<td>
					<img class="lefttree_reltypeicon" src="%InnovatorClientUrl%/images/ItemProperty.svg"/>
					<span>
						<xsl:value-of select="concat($parent_prop_name, ' (', $parent_type_name, ')')"/>
					</span>
				</td>
			</tr>

	</xsl:template>
</xsl:stylesheet>