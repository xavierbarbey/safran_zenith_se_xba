﻿/* global define,dojo, treeNodeTemplate */
define(['../Scripts/conditionsTreeRenderer.js',
	'dojo/_base/declare', 'dojo/_base/connect', 'dijit/popup', 'Aras/Client/Controls/Experimental/ContextMenu'
], function(renderer, declare, connect, popup, ContextMenu) {
	function insertTextAtCursor(node) {
		var sel;
		var range;
		sel = window.getSelection();
		if (sel.getRangeAt && sel.rangeCount) {
			range = sel.getRangeAt(0);
			range.deleteContents();
			range.insertNode(node);
		}

		var selection = window.getSelection();
		selection.removeAllRanges();
		range = document.createRange();
		range.selectNode(node);
		range.collapse(false);
		selection.addRange(range);
	}

	function getLastWordOfNode(node) {
		var text = node.textContent;
		var reg = /[ \(\[]/;
		return text.slice(0, window.getSelection().focusOffset).split(reg).pop().replace('\n', '');
	}

	function getIntersectionWithSource(source, keyword) {
		return source.filter(function(sourceItem) {
			return sourceItem.name.toLowerCase().indexOf(keyword.toLowerCase()) === 0;
		}).slice();
	}

	var showIntelliSenseMenu = function(targetGroup, menuItems, acceptType, onSelect) {
		if (targetGroup && menuItems.length) {
			var node = targetGroup.nodeType === 3 ? targetGroup.parentNode : targetGroup;
			var groupBoundingRect = node.getBoundingClientRect();
			var intelliSenseMenu = this.intelliSenseMenu;
			var menuWidget;

			// at this point menu widget will be recreated
			intelliSenseMenu.removeAll();

			menuWidget = intelliSenseMenu.menu;
			intelliSenseMenu.addRange(menuItems);

			connect.connect(menuWidget, 'onBlur', function() {
				this.close();
			}.bind(this));

			menuWidget.domNode.addEventListener('click', function(e) {
				var menuItem = intelliSenseMenu.menu.selected;
				onSelect(node, menuItem);
				this.close();
			}.bind(this), true);

			menuWidget.domNode.addEventListener('keyup', function(keyEvent) {
				switch (keyEvent.keyCode) {
					case 27:
						this.close();
						this.editor.focus();
						break;
					case acceptType === 'enter' ? 13 : 9:
						var menuItem = intelliSenseMenu.menu.selected;
						onSelect(node, menuItem);
						this.close();
						break;
					default:
						if (acceptType !== 'enter' && keyEvent.keyCode !== 38 && keyEvent.keyCode !== 40 && keyEvent.keyCode !== 13) {
							this.editor.focus();
						}
						break;
				}
			}.bind(this));
			menuWidget.domNode.addEventListener('keydown', function(keyEvent) {
				if (acceptType !== 'enter' && keyEvent.keyCode !== 38 && keyEvent.keyCode !== 40 && keyEvent.keyCode !== 9 && keyEvent.keyCode !== 13) {
					node.focus();
				}
			}.bind(this));

			var selection = window.getSelection();
			if (selection.rangeCount > 0) {
				groupBoundingRect = selection.getRangeAt(0).getBoundingClientRect();
			}

			popup.open({
				popup: menuWidget,
				x: groupBoundingRect.left,
				y: groupBoundingRect.top + groupBoundingRect.height + 10,
				maxHeight: 175
			});
			intelliSenseMenu.active = true;
		}
	};

	var IntelliSense = function() {};

	IntelliSense.prototype.attachTo = function(node, getMenuItems, onSelect) {
		this.intelliSenseMenu = new ContextMenu();
		this.editor = node;

		this.editor.addEventListener('keydown', function(e) {
			if (e.ctrlKey === true && e.keyCode === 32) {
				this.show(window.getSelection().focusNode, getIntersectionWithSource(getMenuItems(), ''), 'enter', function(node, value) {
					this.editor.focus();
					onSelect(node, value, 'enter');
				}.bind(this));
				this.intelliSenseMenu.menu.focus();
			}

			if (this.intelliSenseMenu.active && (e.keyCode === 38 || e.keyCode === 40)) {
				this.intelliSenseMenu.menu.focus();
				e.preventDefault();
				e.stopPropagation();
			}
		}.bind(this));

		this.editor.addEventListener('keyup', function(e) {
			if (!window.getSelection().focusNode) {
				return;
			}
			var keyword = getLastWordOfNode(window.getSelection().focusNode);
			if (keyword.length === 0) {
				if (this.intelliSenseMenu.active) {
					this.close();
				}
				return;
			}
			var intersection = getIntersectionWithSource(getMenuItems(), keyword);
			if (intersection.length) { //last word
				this.show(window.getSelection().focusNode, intersection, 'tab', function(node, value) {
					this.editor.focus();
					onSelect(node, value, 'tab', keyword);
				}.bind(this));
				e.target.focus();
			} else if (!intersection.length && this.intelliSenseMenu.active) {
				this.close();
			}
			var upArrow = 38;
			var downArrow = 40;

			if (this.intelliSenseMenu.active && (e.keyCode === upArrow || e.keyCode === downArrow)) {
				this.intelliSenseMenu.menu.focus();
			}
		}.bind(this));
	};

	IntelliSense.prototype.show = showIntelliSenseMenu;
	IntelliSense.prototype.close = function() {
		popup.close(this.intelliSenseMenu.menu);
	};
	IntelliSense.prototype.insertTextAtCursor = insertTextAtCursor;

	return IntelliSense;
});
