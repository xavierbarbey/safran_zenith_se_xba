﻿Utils.Page.LoadModules(["Controls/mtButton", "Widgets/Dialog"]);

require([
	"dojo/_base/declare"],
function(declare) {
	return dojo.setObject("VC.Widgets.ViewDirectionDialog", dojo.declare([VC.Widgets.Dialog],
		{
			dialogName: "viewDirection",

			postCreate: function() {
				this.inherited(arguments);

				this.btnIsoView.onClick = dojo.partial(dojo.hitch(this, this._isoViewClick), this.btnIsoView);
				this.btnBottomView.onClick = dojo.partial(dojo.hitch(this, this._bottomViewClick), this.btnBottomView);
				this.btnOrientToFace.onClick = dojo.partial(dojo.hitch(this, this._orientToFaceClick), this.btnOrientToFace);
				this.btnFrontView.onClick = dojo.partial(dojo.hitch(this, this._frontViewClick), this.btnFrontView);
				this.btnLeftView.onClick = dojo.partial(dojo.hitch(this, this._leftViewClick), this.btnLeftView);
				this.btnBackView.onClick = dojo.partial(dojo.hitch(this, this._backViewClick), this.btnBackView);
				this.btnRightView.onClick = dojo.partial(dojo.hitch(this, this._rightViewClick), this.btnRightView);
				this.btnTopView.onClick = dojo.partial(dojo.hitch(this, this._topViewClick), this.btnTopView);
			},

			onIsoViewClick: function() { },
			onBottomViewClick: function() { },
			onOrientToFaceClick: function() { },
			onFrontViewClick: function() { },
			onLeftViewClick: function() { },
			onBackViewClick: function() { },
			onRightViewClick: function() { },
			onTopViewClick: function() { },

			_isoViewClick: function() {
				this._resetButtonsState();
				this.btnIsoView.SetPressedState(true);

				this.onIsoViewClick();
			},

			_bottomViewClick: function() {
				this._resetButtonsState();
				this.btnBottomView.SetPressedState(true);

				this.onBottomViewClick();
			},

			_orientToFaceClick: function() {
				this._resetButtonsState();
				this.btnOrientToFace.SetPressedState(true);

				this.onOrientToFaceClick();
			},

			_frontViewClick: function() {
				this._resetButtonsState();
				this.btnFrontView.SetPressedState(true);

				this.onFrontViewClick();
			},

			_leftViewClick: function() {
				this._resetButtonsState();
				this.btnLeftView.SetPressedState(true);

				this.onLeftViewClick();
			},

			_backViewClick: function() {
				this._resetButtonsState();
				this.btnBackView.SetPressedState(true);

				this.onBackViewClick();
			},

			_rightViewClick: function() {
				this._resetButtonsState();
				this.btnRightView.SetPressedState(true);

				this.onRightViewClick();
			},

			_topViewClick: function() {
				this._resetButtonsState();
				this.btnTopView.SetPressedState(true);

				this.onTopViewClick();
			},

			_resetButtonsState: function() {
				this.btnIsoView.SetPressedState(false);
				this.btnBottomView.SetPressedState(false);
				this.btnOrientToFace.SetPressedState(false);
				this.btnFrontView.SetPressedState(false);
				this.btnLeftView.SetPressedState(false);
				this.btnBackView.SetPressedState(false);
				this.btnRightView.SetPressedState(false);
				this.btnTopView.SetPressedState(false);
			},

			disableButtons: function() {
				this.btnOrientToFace.Disable();
			},

			enableButtons: function() {
				this.btnOrientToFace.Enable();
			}
		}));
});