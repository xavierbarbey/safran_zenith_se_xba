﻿function ModelBrowser() {
    this.windowSelector = "#modelBrowserWindow";
    this.modelTreeSelector = "#modelTree";
 
    this.modelTree = null;
    this.shiftPressed = false;
    this.isMinimized = false;
    this.isInit = false;

    this.explicitlyHidden = {};

    this.restoredDimensions = {
        width: 0,
        height: 0
    };

    this.parts = {};

    this.icons = {
        root: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/document_icon.png",
        part: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/part_icon2.png",
        product: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/assembly_icon2.png",
        body: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/body_icon2.png",
        note: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/pin_icon.png",
        pmi: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/pmi.png",
        measurement: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/measure_icon.png",
        captures: "../Scripts/3rdPartyLibs/hoopswebviewer/viewer_images/capture_icon.png"
    };

    this.hwvCallbacks = null;
    this._registerHwvCallbacks();
}

ModelBrowser.prototype.show = function () {
    $(this.windowSelector).show();
};

ModelBrowser.prototype.hide = function () {
    $(this.windowSelector).hide();
};

ModelBrowser.prototype.toggle = function () {
    if (this.isMinimized)
        this.restore();
    else
        this.minimize();
};

ModelBrowser.prototype.minimize = function () {
    if (!this.isMinimized) {
        var $content = $("#modelBrowserContent");
        var self = this;

        this.isMinimized = true;
        $(this.modelTreeSelector).hide();

        this.restoredDimensions.width = $content.width();
        this.restoredDimensions.height = $content.height();

        $(this.windowSelector).resizable('destroy');

        $("#modelBrowserContent").slideUp({
            duration: "fast",
            step: function () {
                var newWindowHeight = Math.max($content.height() + 10, 28);

                $(self.windowSelector).css({
                    height: newWindowHeight + "px"
                });
            }
        });
    }
};

ModelBrowser.prototype.restore = function () {
    var self = this;

    if (this.isMinimized) {
        this.isMinimized = false;

        $("#modelBrowserContent").css({
            height: this.restoredDimensions.height + "px"
        });

        $(this.windowSelector).css({
            height: (this.restoredDimensions.height + 35) + "px"
        });

        $("#modelBrowserContent").show();
        $(this.modelTreeSelector).show();

        $(this.windowSelector).resizable({
            resize: function (event, ui) {
                self._handleResize(ui.size);
            }
        });
    }
};




ModelBrowser.prototype.onParse = function (name, parent, type, uniqueId) {
    var treeId = 's' + uniqueId;
    var parentId = (type == "root") ? null : 's' + parent;

    var item = {
        id: treeId,
        title: name,
        nodeParent: parentId,
        icon: this.icons[type],
        children: [],
    };

    if (type == "root") {
        this.modelTree = item;
    }

    
    parentItem = this.parts[parentId];

    if (parentItem)
        parentItem.children.push(item);
    else if (type != "root") {
        console.log("UNKNOWN: ", uniqueId, name, type, parent);
    }

    this.parts[treeId] = item;

};

ModelBrowser.prototype.onNoteCreated = function (note) {
    var uniqueId = "note-" + note.uniqueId;
    var node = this._createTopLevelNode(note.headline, uniqueId, this.icons.note);
    $(this.modelTreeSelector).jstree("create_node", "#notes", "last", node);
};

ModelBrowser.prototype.onUserViewCreated = function (view) {
    if (view.name != "auto_save") {
        var uniqueId = "userView-" + view.uniqueId;
        var node = this._createTopLevelNode(view.name, uniqueId, this.icons.captures);
        node.attr.rel = "disabled";
        $(this.modelTreeSelector).jstree("create_node", "#markup", "last", node);
    }
};

ModelBrowser.prototype.onNoteDeleted = function (uniqueId) {
    $(this.modelTreeSelector).jstree("delete_node", "#note-" + uniqueId);
};

ModelBrowser.prototype.onuserViewDeleted = function (uniqueId) {
    $(this.modelTreeSelector).jstree("delete_node", "#userView-" + uniqueId);
};

ModelBrowser.prototype.onMeasurementCreated = function (measurement) {
    console.log("model browser measurement created: ", measurement);

    var node = this._createTopLevelNode(measurement.type, "measure-" + measurement.uniqueId, this.icons.measurement);
    $(this.modelTreeSelector).jstree("create_node", "#Measurements", "last", node);

};

ModelBrowser.prototype.onMeasurementDeleted = function (uniqueId) {
    $(this.modelTreeSelector).jstree("delete_node", "#measure-" +uniqueId);
};

ModelBrowser.prototype.onNodeDeleted = function (uniqueId) {
    $(this.modelTreeSelector).jstree("delete_node", "#s" +uniqueId);
};


ModelBrowser.prototype._handleResize = function (newSize) {
    $("#modelBrowserContent").css({
        height: newSize.height - 35
    });

    if (this._isInternetExplorer()) {
        hwv.requestImmediateUpdate();
    }
};

ModelBrowser.prototype._treeDisplayCallback = function (node, func) {
    var nodeId = $(node).attr("id");

    if (node == -1) {
        func(this._createTopLevelTreeElements());
    }
    else {
        func(this._getNodeJSONForPart(nodeId));
    }
};

ModelBrowser.prototype.initializeModelBrowser = function () {
    if (this.isInit)
        return;
    var self = this;

    $(this.windowSelector).bind("touchmove", function (event) {
        event.originalEvent.preventDefault();
    });

    $(this.windowSelector).draggable({ scroll:false, handle: "#modelBrowserHeader"}).resizable({
        resize: function (event, ui) {
            self._handleResize(ui.size);
        }
    });

    $.jstree._themes = "../Scripts/3rdPartyLibs/hoopswebviewer/js/jquery/themes/";

    $(this.modelTreeSelector).jstree({
        json_data: {
            data: function (node, func) { self._treeDisplayCallback( node, func); }
        },
        themes:{
            theme: "classic"
        },
        progressive_render: true,
        plugins: ["themes","checkbox", "json_data", "hotkeys", "ui", "types"],
        checkbox : { "two_state" :  false }, 
        core: { "animation": 10 },
    });


    $(this.modelTreeSelector).bind("select_node.jstree", function (e, data) {
        var uniqueId = data.rslt.obj[0].id;
        var tagName = data.args[0].tagName;

        if (tagName == "A" || tagName == "INS")
            self._processItemClick(uniqueId);
    });

    $(this.modelTreeSelector).bind("change_state.jstree", function (e, d) {
        var tagName = d.args[0].tagName;
        var refreshing = d.inst.data.core.refreshing;

        if (tagName == "A" || tagName == "INS")
            self.processCheckboxClick(e, d);
    });

    $(this.modelTreeSelector).bind("open_node.jstree", function (event, data) {
        self._processOpenNode(event, data);
    });
    
    $(document).bind('keyup keydown', function (e) { self.shiftPressed = e.shiftKey; return true; });
    $(document).bind('keydown', 'del', function () {
        self.deleteKeyPressed();
    });

    $("#modelBrowserClose").bind("click", function () {
        self.hide();
    });

    this.isInit = true;
    this.hide();
};

ModelBrowser.prototype._processOpenNode = function (event, data) {
    var $ref = $.jstree._reference('#' + this.modelTree.id);
    var uniqueId = data.rslt.obj[0].id;

    var node = this.parts[uniqueId];

    if (node) {
        for (var i = 0; i < node.children.length; i++) {
            var child = node.children[i];

            if (this.explicitlyHidden[child.id]) {
                $ref.uncheck_node('#' + child.id);
                delete this.explicitlyHidden[uniqueId];
            }
        }
    }
    
};

ModelBrowser.prototype.deleteKeyPressed = function () {
    var selected = $(this.modelTreeSelector).jstree('get_selected').attr('id');

    if (selected !== undefined) {
		var uniqueId;
        if (selected.indexOf("measure-") != -1) {
            uniqueId = selected.substring(selected.indexOf('-') + 1);
            hwv.deleteMeasurement(uniqueId);
        }
        else if (selected.indexOf("note-") != -1) {
            uniqueId = selected.substring(selected.indexOf('-') + 1);
            hwv.deleteNote(uniqueId);
        }
        else if (selected.indexOf("userView-") != -1) {
            uniqueId = selected.substring(selected.indexOf('-') + 1);
            hwv.deleteView(uniqueId);
        }
    }
};

ModelBrowser.prototype._processItemClick = function (uniqueId) {
    if (uniqueId.indexOf("note-") != -1) {
        uniqueId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.activateNote(uniqueId);
    }
    else if (uniqueId.indexOf("measure-") != -1) {
        uniqueId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.selectMeasurement(uniqueId);
    }
    else if (uniqueId.indexOf("capture-") != -1) {
        captureName = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.applyCapture(captureName);
    }
    else if (uniqueId.indexOf("userView-") != -1) {
        viewId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.setCurrentView(viewId);
    }
    else if (uniqueId.indexOf("pmi-") != -1) {
        uniqueId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.selectByUniqueId(uniqueId);
    }
    else if (uniqueId != "notes" && uniqueId != "captures" && uniqueId[0] == 's'){
        hwv.selectByUniqueId(uniqueId.substring(1));
    }
};

ModelBrowser.prototype.processCheckboxClick = function (e, d) {
    var uniqueId = d.rslt[0].id;
    var checked = $(this.modelTreeSelector + " #" + uniqueId).hasClass("jstree-checked");

    if (uniqueId.indexOf("pmis") != -1) {
        hwv.setPMIVisibility(checked);
    }
    if (uniqueId.indexOf("pmi-") != -1) {
            var r = uniqueId.split("-");
            hwv.setPartVisibility(r[1], checked);
    }
    else if (uniqueId.indexOf("notes") != -1) {
        hwv.setNotesVisibility(checked);
    }
    else if (uniqueId.indexOf("note-") != -1) {
        uniqueId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.setNoteVisibility(uniqueId, checked);
    }
    else if (uniqueId.indexOf("measurements") != -1) {
        hwv.setMeasurementsVisibility(checked);
    }
    else if (uniqueId.indexOf("measure-") != -1) {
        uniqueId = uniqueId.substring(uniqueId.indexOf('-') + 1);
        hwv.setMeasurementVisibility(uniqueId, checked);
    }
    else {
        if (this.shiftPressed) {
            this._doIsolate(uniqueId);
        }
        else {
            hwv.setPartVisibility(uniqueId.substring(1), checked);
        }
    }
};

ModelBrowser.prototype._doIsolate = function (uniqueId) {
    this.explicitlyHidden = {};

    var $ref = $.jstree._reference('#' + this.modelTree.id);
    $ref.uncheck_all();


    hwv.isolatePart(uniqueId.substring(1));
    $ref.check_node('#' + uniqueId);
};

ModelBrowser.prototype._openParentNodes = function (uniqueId) {
    var node = this.parts[uniqueId];

    if (node.nodeParent) {
        this._openParentNodes(node.nodeParent);
    }
    
    var $ref = $.jstree._reference('#' + this.modelTree.id);
    $ref.open_node("#" + node.id);
};

ModelBrowser.prototype._createTopLevelTreeElements = function () {
    var elements = [];

    if (this.modelTree) {
        var el = this._createTopLevelNode(this.modelTree.title, this.modelTree.id, this.modelTree.icon);

        if (this.modelTree.children.length > 0)
            el.state = "closed";

        elements.push(el);
    }

    var notes = null;
    var measurements = null;
    var captures = null;
    
    if (hwv.getModelIs2D()) {
        captures = this._createTopLevelNode("Sheets", "captures", this.icons.captures);
    }
    else {
        notes = this._createTopLevelNode("Notes", "notes", this.icons.pin);
        elements.push(notes);

        measurements = this._createTopLevelNode("Measurements", "measurements", this.icons.measurement);
        elements.push(measurements);

        captures = this._createTopLevelNode("Views", "captures", this.icons.captures);
    }

    captures.attr.rel = "disabled";
    elements.push(captures);

    var markup = this._createTopLevelNode("Markup", "markup", this.icons.captures);
    markup.attr.rel = "disabled";
    elements.push(markup);

    var pmis = null;
    if (hwv.getModelContainsPMI()) {
        pmis = this._createTopLevelNode("PMI", "pmis", this.icons.pmi);
    }

    elements.push(pmis);


    this._addMarkup(notes, measurements, captures, markup, pmis);

    return elements;
};

ModelBrowser.prototype._adjustParentVisibilitySettings = function (uniqueId) {
    var $ref = $.jstree._reference('#' + this.modelTree.id);
    var currentNode = this.parts[uniqueId];

    if (currentNode) {
        while (currentNode.nodeParent) {
            currentNode = this.parts[currentNode.nodeParent];

            var nodeId = "#" + currentNode.id;
            var $node = $(nodeId);

            if ($node.length > 0) {
                if (!currentNode.nodeParent && currentNode.children.length > 0) {
                    $node.removeClass("jstree-unchecked").addClass("jstree-undetermined");
                }
                else {
                    $ref.uncheck_node(nodeId);
                }

                return;
            }
        }
    }
};
ModelBrowser.prototype.onRefresh = function () {
    $(this.modelTreeSelector).jstree("refresh");
};
ModelBrowser.prototype.onPartVisbilityChange = function (uniqueId, visible, pmitype) {
    if (!this.isInit)
        return;

    var nodeId;
    var $ref;
    if (pmitype !== undefined)
    {
          var r = uniqueId.split("s");
          nodeId = "#pmi-" + r[1];
          $ref = $.jstree._reference('#pmis');
    }
    else
    {
        nodeId = "#" + uniqueId;
        $ref = $.jstree._reference('#' + this.modelTree.id);
    }

    if (visible && !$ref.is_checked(nodeId)) {
        $ref.check_node(nodeId);
    }
    else if (!visible && $ref.is_checked(nodeId)) {
        $ref.uncheck_node(nodeId);
    }

    if (visible) {
        if (this.explicitlyHidden[uniqueId])
            delete this.explicitlyHidden[uniqueId];
    }
    else {
        //if a node has not been rendered into the dom yet then we want to remember that it should not be shown
        var $node = $(nodeId);
        if ($node.length === 0) {
            this.explicitlyHidden[uniqueId] = true;
            this._adjustParentVisibilitySettings(uniqueId);
        }
    }
};

ModelBrowser.prototype._addMarkup = function (notes, measurements, captures, markups, pmis) {
    var self = this;
    var markup = hwv.getMarkupData();

    if (notes) {
        $.each(markup.notes, function (k, n) {
            var note = self._createTopLevelNode(n.headline, "note-" + n.uniqueId, self.icons.note);
            notes.children.push(note);
        });
    }

    if (measurements) {
        $.each(markup.measurements, function (k, m) {
            var measurement = self._createTopLevelNode(m.type, "measure-" + m.uniqueId, self.icons.measurement);
            measurements.children.push(measurement);
        });
    }

    if (captures) {
        var capturenames = hwv.getCaptureNames();
        $.each(capturenames, function (k, c) {
            if (c != "cad_default") {
                var capture = self._createTopLevelNode(c, "capture-" + c, self.icons.captures);
                capture.attr.rel = "disabled";
                captures.children.push(capture);
            }
        });
    }

    if (pmis) {

        var pmiItems = hwv.getPMIItems();

        for (var i=0;i<pmiItems.length;i++)
        {
                var pmi = self._createTopLevelNode(pmiItems[i].pmi_type, "pmi-" + pmiItems[i].uniqueId, self.icons.pmi);
                pmis.children.push(pmi);
        }
    }


    $.each(markup.views, function (k, v) {
        if (v.name != "auto_save") {
            var view = self._createTopLevelNode(v.name, "userView-" + v.uniqueId, self.icons.captures);
            view.attr.rel = "disabled";
            markups.children.push(view);
        }
    });
};

ModelBrowser.prototype._getNodeJSONForPart = function (uniqueId) {
    var self = this;
    var partNode = this.parts[uniqueId];

    var nodes = [];

    $.each(partNode.children, function (k, child) {
        var node = {
            data: {
                title: child.title,
                icon: child.icon
            },
            attr: {
                "id": child.id,
                
            },
            metadata: {
                id: child.id
            },
            nodeParent: child.parent,
        };

        if (child.children.length > 0)
            node.state = "closed";

        nodes.push(node);
    });

    return nodes;
};

ModelBrowser.prototype._createTopLevelNode = function (title, id, icon) {
    return {
        attr: {
            id: id,
            "class": "jstree-checked"
        },
        data: {
            title: title,
            icon: icon,
        },
        checked: true,
        state: "checked",
        children: []
    };
};

ModelBrowser.prototype._doPartSelection = function (selection) {
    if (this.modelTree) {
        var selectedId = "s" + selection.properties.uniqueId;
        var $ref = $.jstree._reference('#' + this.modelTree.id);

        $ref.deselect_all();

        if (selectedId !== undefined) {
            var node = this.parts[selectedId];
            if (node)
            {
                if (node.nodeParent)
                    this._openParentNodes(node.nodeParent);

                $ref.select_node('#' + selectedId);
            }
        }
    }
};

ModelBrowser.prototype._doPMISelection = function (selection) {
    if (this.modelTree) {
        var selectedId = selection.properties.uniqueId;
        var $ref = $.jstree._reference('#pmis');

        $ref.deselect_all();

        if (selectedId !== undefined) {
           var r = selectedId.split("s");
            $ref.select_node('#' + 'pmi-' + r[1]);
        }
    }
};

ModelBrowser.prototype._doRedlineSelection = function (selection) {
    try {
         $.jstree._reference('#' + this.modelTree.id).deselect_all();
    }
    catch(err){
    
    }

};

ModelBrowser.prototype.onSelectionChange = function (selection) {
    switch (selection.properties.resultType) {
        case "part":
            this._doPartSelection(selection);
            break;

        case "redline":
            this._doRedlineSelection(selection);
            break;
        case "pmi":
            this._doPMISelection(selection);
            break;
    }
};

ModelBrowser.prototype._getIconForType = function (type) {
    switch (type) {
        
        case "part":
            return this.m_partIcon;

        case "product":
            return this.m;

        default:
            return this.m_rootIcon;
    }
};

ModelBrowser.prototype._isInternetExplorer = function () {
    return navigator.userAgent.indexOf("Trident") >= 0;
};

ModelBrowser.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvCallbacks = {
        notes: {
            created: function (note) { self.onNoteCreated(note); },
            remove: function (uniqueId) { self.onNoteDeleted(uniqueId); }
        },

        views: {
            created: function (view) { self.onUserViewCreated(view); },
            remove: function (uniqueId) { self.onuserViewDeleted(uniqueId); },
        },

        measurements: {
            save: function (measurement) { self.onMeasurementCreated(measurement); },
            remove: function (uniqueId) { self.onMeasurementDeleted(uniqueId); }
        },

        loading: {
            complete: function () { self.initializeModelBrowser();}
        },

        selection: function (selection) { self.onSelectionChange(selection); },
        modelParse: function (name, parent, type, uniqueId) { self.onParse(name, parent, type, uniqueId); },
        modelRefresh: function () {    self.onRefresh();  },
        modelNodeDelete: function (uniqueId) {    self.onNodeDeleted(uniqueId);  },
        partVisibility: function (uniqueId, visible, pmitype) { self.onPartVisbilityChange(uniqueId, visible, pmitype); }
    };

    hwv.setCallbacks(this.hwvCallbacks);
};