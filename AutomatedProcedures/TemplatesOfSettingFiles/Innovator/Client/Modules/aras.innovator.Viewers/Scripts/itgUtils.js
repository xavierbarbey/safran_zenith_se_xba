﻿var scriptPath = top.aras.getInnovatorUrl() + "Client/Solutions/ITG/javascript/aras.itg.viewers/Scripts";
var widgetPath = top.aras.getInnovatorUrl() + "Client/Solutions/ITG/javascript/aras.itg.viewers/Scripts/Widgets";

dojo.registerModulePath("itgScripts", scriptPath);
dojo.registerModulePath("itgWidgets", widgetPath);

VC.Utils.Page.LoadItgScripts = function (strArr) {
	require(this.preloadScripts(strArr, "itgScripts"));
};

VC.Utils.Page.LoadItgWidgets = function (strArr) {
	require(this.preloadScripts(strArr, "itgWidgets"));
};

VC.Utils.GetItgResource = function (key) {
	return top.aras.getResource("../Solutions/ITG/javascript/aras.itg.viewers/", key);
};

VC.Utils.GetResource = function (key) {
	return top.aras.getResource("../Solutions/ITG/javascript/aras.itg.viewers/", key);
};

//Parses full file path to three part: { pathString, fileName and fileExtension }
VC.Utils.parseFileUrl = function (filePath) {
	var regex = new RegExp("^.*[\\\/]"),
			fileNameWithExtension = null,
			returnedValue = null,
			pathString = null;

	if (!filePath || filePath === "") {
		return null;
	}

	pathString = filePath.match(regex);

	if (pathString !== null) {
		returnedValue = {};
		fileNameWithExtension =  filePath.replace(regex, "");
		returnedValue.pathString = pathString[0];
		returnedValue.fileName = fileNameWithExtension.split(".").shift();
		returnedValue.fileExtension = fileNameWithExtension.split(".").pop();
	}

	return returnedValue;
};

VC.Utils.roundZoomValue = function (value) {
	if (value < 1.0) {
		return 1.0;
	}
	else {
		return Math.round(value);
	}
};

VC.Utils.getXYDPIs = function () {
	// Create 1" square div using points
	var testDiv = document.createElement("div");
	testDiv.style.height = "72pt";
	testDiv.style.width = "72pt";
	testDiv.style.visibility = "hidden";
	document.body.appendChild(testDiv);

	// Retrieve pixel measurements
	var xRes = testDiv.offsetWidth;
	var yRes = testDiv.offsetHeight;

	// Remove the test element
	testDiv.parentNode.removeChild(testDiv);

	// Return results in an array
	return Array(xRes, yRes);
};

VC.Utils.getPdfFileInfo = function (fileUrl, parameters) {
	if (!parameters) {
		return null;
	}

	var url = top.aras.getInnovatorUrl() + "Client/scripts/ConvertPdfToXOD.ashx?file=" + encodeURIComponent(fileUrl) + parameters;
	var xmlHttp = top.aras.XmlHttpRequestManager.CreateRequest();
	xmlHttp.open("GET", url, false);
	xmlHttp.send(null);

	if (xmlHttp.status === 200) {
		var xmlDoc = xmlHttp.responseXML;
		var resultEl = xmlDoc.getElementsByTagName("Result")[0];
		if (resultEl && resultEl.childNodes.length > 0) {
			return resultEl.childNodes[0].nodeValue;
		}
		else {
			return null;
		}
	}
	else {
		return null;
	}
};

/// <summary>Sets visibility to element.</summary>
/// <param name="visible" type="Boolean">Does element set visible or not.</param>
if (!Element.prototype.visible) {
	Element.prototype.visible = function (visible) {
		if (this.style) {
			this.style.display = visible ? "block" : "none";
		}
	};
}

/// <summary>Sets disabling to element.</summary>
/// <param name="disable" type="Boolean">Does element set disable or not.</param>
if (!Element.prototype.disable) {
	Element.prototype.disable = function (disable) {
		if (this.attributes) {
			if (disable) {
				this.setAttribute("disabled", "disabled");
			}
			else {
				this.removeAttribute("disabled");
			}
		}
	};
}