function HotkeyManager(viewer, focus) {
    this.viewers = [];

    this.pmiVisibilityKeyCode = HotkeyManager.DEFAULT_PMI_VISIBILITY_KEYCODE;
    this.backNavigationKeyCode = HotkeyManager.DEFAULT_BACKNAVIGATION_KEYCODE;
    this.fullscreenKeyCode = HotkeyManager.DEFAULT_FULLSCREEN_KEYCODE;

    if (viewer) {
        this.registerViewer(viewer)
    }
}

HotkeyManager.prototype.registerViewer = function (viewer) {
    var self = this;

    var viewerData = {
        viewer: viewer,
        loaded: false,

        pmiVisibilityKeyCode: this.pmiVisibilityKeyCode,
        backNavigationKeyCode: this.backNavigationKeyCode,
        fullscreenKeyCode: this.fullscreenKeyCode,
        pmiVisibility: true,
    };

    viewerData.keydown = function (event) { self.doKeyDown(viewerData, event); };
    viewerData.keyup = function (event) { self.doKeyUp(viewerData, event); };
    viewerData.loadComplete = null;

    this.viewers.push(viewerData);

    if (viewer.isStarted()) {
        this._doLoadComplete(viewerData.viewer);
    }
    else {
        viewerData.loadComplete = function () { self._doLoadComplete(viewerData.viewer); }
        viewerData.viewer.setCallbacks({
            sceneReady: viewerData.loadComplete
        });
    }
}

HotkeyManager.prototype._doLoadComplete = function (viewer) {
    var self = this;

    for (var i = 0; i < this.viewers.length; i++) {
        if (this.viewers[i].viewer == viewer && this.viewers[i].loaded == false) {
            var viewerData = this.viewers[i];
            viewerData.loaded = true;
            var canvas = viewer.getContext().canvas;

            canvas.addEventListener("keydown", viewerData.keydown, false);
            canvas.addEventListener("keyup", viewerData.keyup, false);

            break;
        }
    }
}

HotkeyManager.prototype.unregisterViewer = function (viewer) {
    var viewerData = null;

    for (var i = 0; i < this.viewers.length; i++) {
        if (this.viewers[i].viewer == viewer) {
            viewerData = this.viewers[i];
            this.viewers = this.viewers.slice(i);
            break;
        }
    }

    if (viewerData) {
        var canvas = viewer.getContext().canvas;

        canvas.removeEventListener("keydown", viewerData.keydown);
        canvas.removeEventListener("keyup", viewerData.keyup);
    }
};

HotkeyManager.prototype.doMouseEnter = function (viewerData, event) {
    var context = viewerData.viewer.getContext();

    context.canvas.focus();
};

HotkeyManager.prototype.doMouseLeave = function (viewerData, event) {
    var context = viewerData.viewer.getContext();

    context.canvas.blur();
}

HotkeyManager.prototype.doKeyDown = function (viewerData, event) {
    var context = viewerData.viewer.getContext();

    switch (event.keyCode) {
        case viewerData.pmiVisibilityKeyCode:
            viewerData.pmiVisibility = !viewerData.pmiVisibility;
            viewerData.viewer.setPMIVisibility(viewerData.pmiVisibility);
            break;

        case viewerData.backNavigationKeyCode:
            event.preventDefault();
            break;
    };
};

HotkeyManager.prototype.doKeyUp = function (viewerData, event) { };

HotkeyManager.DEFAULT_PMI_VISIBILITY_KEYCODE = 80;
HotkeyManager.DEFAULT_BACKNAVIGATION_KEYCODE = 8;
HotkeyManager.DEFAULT_FULLSCREEN_KEYCODE = 70;