/*
NoteWindow.js implements the default custom note window.

Requires:   hoops_web_viewer.js
            jquery
*/

function NoteWindow() {
    this.pendingNoteUpdate = 0;
    this.noteSelector = "#noteMarkup";
    this.focusNextTextBox = false;
    this.noteSaveCallback = null

    this.init();

    this.hwvCallbacks = null;
    this._registerHwvCallbacks();
}

NoteWindow.prototype.setActiveNoteColorCallback = function (event) {
    var uniqueId = hwv.getActiveNoteId();
    if (uniqueId) {
        var $target = $(event.target);

        var color = [parseFloat($target.data("r")), parseFloat($target.data("g")), parseFloat($target.data("b"))];

        hwv.editNoteMarkup({ uniqueId: uniqueId, color: color });
    }
}

NoteWindow.prototype.noteCreatedCallback = function (note) {
    this.focusNextTextBox = true;
    this.noteActivatedCallback(note);

    if (this.noteSaveCallback) {
        this.noteSaveCallback(note);
    }
}

NoteWindow.prototype.noteShownCallback = function (note) {

    if (this.focusNextTextBox)
        $("#noteText").focus();

    this.focusNextTextBox = false;
}

NoteWindow.prototype.noteActivatedCallback = function (note) {

    var $note = $(this.noteSelector);

    var $text = $("#noteText")
    $text.val(note.text);

    $note.find(".noteOwner").text("Owner");
}

NoteWindow.prototype.noteDeactivatedCallback = function (note) {
    if (this.pendingNoteUpdate) {
        clearTimeout(this.pendingNoteUpdate);
        this.updateActiveNoteText();
    }
}

NoteWindow.prototype.deleteActiveNoteCallback = function () {
    var uniqueId = hwv.getActiveNoteId();
    hwv.deleteNote(uniqueId);
}

NoteWindow.prototype.updateActiveNoteText = function () {
    var uniqueId = hwv.getActiveNoteId();

    if (uniqueId) {
        var text = $("#noteText").val();

        hwv.editNoteMarkup({ uniqueId: uniqueId, text: text });
        this.pendingNoteUpdate = 0;
    }
}

NoteWindow.prototype.scheduleNoteRemoteUpdate = function () {

    if (this.pendingNoteUpdate)
        window.clearTimeout(this.pendingNoteUpdate);

    var self = this;
    this.pendingNoteUpdate = window.setTimeout(function () { self.updateActiveNoteText() }, 1000);
}

NoteWindow.prototype.init = function () {
    var self = this;

    $(".noteButton.color").live("click", this.setActiveNoteColorCallback);
    $(".noteButton.trash").live("click", this.deleteActiveNoteCallback);

    $("#noteText").live("input paste", function () { self.scheduleNoteRemoteUpdate() });
}

NoteWindow.prototype.setNoteSaveCallback = function (callback) {
    this.noteSaveCallback = function (note) { callback(note); };
}

NoteWindow.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvCallbacks = {
        notes: {
            activate: function (note) {
                self.noteActivatedCallback(note);
            },
            deactivate: function (note) {
                self.noteDeactivatedCallback(note);
            },
            shown: function (note) {
                self.noteShownCallback(note);
            },
            created: function (note) {
                self.noteCreatedCallback(note);
            },
        },

        interaction: {
            begin: function () {
                $(self.noteSelector).addClass("mouseOperatorNavigating");
            },
            end: function () {
                $(self.noteSelector).removeClass("mouseOperatorNavigating");
            }
        },
    };

    hwv.setCallbacks(this.hwvCallbacks);
    hwv.setNoteMarkupElementId(this.noteSelector);
}