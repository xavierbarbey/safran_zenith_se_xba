var UserInterface = function (viewer) {
    this.hwv = viewer;

    this.modelBrowser = new ModelBrowser2();
    this.loadingBar = new LoadingBar();
    this.propertyWindow = new PropertyWindow(this.hwv);
    this.toolBar = new Toolbar(this.hwv);
    this.noteWindow = new NoteWindow();
    this.helpWindow = new HelpWindow();
    this.infoDialog = new InfoDialog();
    this.hotkeyManager = new HotkeyManager(this.hwv);

    this.firstLoadComplete = false;
    this.viewerString = "HOOPS Web Viewer";

    this._initViewer();
};

UserInterface.prototype._initViewer = function () {
    var self = this;
    document.onselectstart = function () { return false; };


  document.addEventListener('touchmove', function (event) {
      event.preventDefault();
  });

    this._setModelFromUrlBar();

    $(window).resize(function ($event) {
        if ($event.target == window)
            hwv.resizeCanvas();
    });

    if (typeof (HWV_SSR) !== "undefined" && (this.hwv instanceof HWV_SSR)) {
        self.viewerString = "HOOPS Remote Web Viewer";

        $(window).unload(function () {
            hwv.kill();
        });
    }

    this.hwv.setLoadCompleteCallback(function (filename) {
        if (self.firstLoadComplete) return;

        var canvas = hwv.getContext().canvas;
        canvas.addEventListener("mouseenter", function () {
            self.hwv.focusInput(true);
        }, true);


        if (filename!= undefined)
            document.title = filename + " | " + self.viewerString;

        self.firstLoadComplete = true;
    });
}

UserInterface.prototype._setModelFromUrlBar = function () {
    var fileName = this._getParameterByName("file");
    if (fileName == null) fileName = this._getParameterByName("filename");

    if (fileName != null)
        this.hwv.setFilename(fileName);
}

//http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values/901144#901144
UserInterface.prototype._getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return null;
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

