function LoadingBar() {
    this.markupSelector = "#progressdiv";

    this.initLoadingBar();

    this.hwvCallbacks = null;
    this._registerHwvCallbacks();
}

LoadingBar.prototype.initLoadingBar = function () {
    this.setProgress(0);
}

LoadingBar.prototype.progress = function (value) {
    this.setProgress(value);
}

LoadingBar.prototype.setProgress = function (progress) {
    $(this.markupSelector).progressbar({
        value: progress
    });

    $(".progress-label").text(progress.toFixed(0) + "%");
}

LoadingBar.prototype.show = function () {
    $(this.markupSelector).show();
}

LoadingBar.prototype.hide = function () {
    $(this.markupSelector).hide();
}

LoadingBar.prototype.loadStart = function () {
    this.show();
}

LoadingBar.prototype.loadComplete = function () {
    this.hide();
}

LoadingBar.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvCallbacks = {
        loading: {
            begin: function () {  self.loadStart(); },
            progress: function (progress) { self.progress(progress); },
            complete: function () { self.loadComplete(); }
        }
    };

    hwv.setCallbacks(this.hwvCallbacks);
}