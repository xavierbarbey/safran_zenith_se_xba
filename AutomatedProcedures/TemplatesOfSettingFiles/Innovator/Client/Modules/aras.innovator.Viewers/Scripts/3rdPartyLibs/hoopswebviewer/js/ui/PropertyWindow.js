function PropertyWindow(viewer) {
    this.hwv = viewer;
    this.screenElementSelector = "#theCanvas";
    if (typeof (HWV_SSR) !== "undefined" && (this.hwv instanceof HWV_SSR))
        this.screenElementSelector = "#screen";

    this.propertyWindowSelector = "#propertywindow";
    this.propertyAreaSelector = this.propertyWindowSelector + " .propertyArea";

    this._initPropertyWindow();

    this.selectionBoundingBox = false;

    this.hwvCallbacks = null;
    this._registerHwvCallbacks();
}

PropertyWindow.prototype._initPropertyWindow = function () {
    var self = this;

    $(window).resize(function () {
        self.reposition();
    });

    $(this.propertyWindowSelector).bind("touchmove", function (event) {
        event.originalEvent.preventDefault();
    });

    $(this.propertyWindowSelector).draggable({ scroll:false, handle: ".propertyWindowHeader"});

    $("#parentSelectButton").click(function () {
        hwv.setSelectionToParent();
    });

    $("#selectionTransparentButton").click(function () {
        hwv.toggleSelectionTransparency();
    });

    $("#boundingboxbutton").click(function () {
        self.selectionBoundingBox = !self.selectionBoundingBox;
        hwv.showSelectionBoundingBox(self.selectionBoundingBox);
    });
}

PropertyWindow.prototype._doPartSelection = function (selection) {
    if (selection.result == "selection") {
        if (selection.properties == undefined || selection.properties.uniqueId == undefined)
            return;

        //SSR will return property info to potentially save a round trip to the server
        var properties = null;
        if (selection.properties.Name != undefined)
            properties = selection.properties;
        else
            properties = hwv.getPartProperties(selection.properties.uniqueId);

        this.show();
        this.setProperties(properties);
    }
    else {
        hwv.showSelectionBoundingBox(false);
        this.hide();
    }
}

PropertyWindow.prototype._doRedlineSelection = function (selection) {
}

PropertyWindow.prototype.onSelection = function (selection) {
    if (selection.properties != undefined) {
        switch (selection.properties.resultType) {
            case "part":
                this._doPartSelection(selection);
                break;

            case "redline":
                this._doRedlineSelection(selection);
                break;
            default:
                this.selectionBoundingBox = false;
                this.hide();
                break; 

        };
    }
}

PropertyWindow.prototype.reposition = function (x, y) {
    var $canvas = $(this.screenElementSelector);

    
    if (this.attributesSet) {
        
        var $canvas = $(this.screenElementSelector);       
        if ($canvas.outerHeight() < 600)
        {
            var d = $(this.propertyWindowSelector).css({ 'top': 50, 'left': $canvas.outerWidth() - 280 })
            var d = $(this.propertyWindowSelector).css({ 'height': $canvas.outerHeight() - 80 })
            var d = $(this.propertyAreaSelector).css({ 'height': $canvas.outerHeight() - 150 })
        }
        else
        {
            var d = $(this.propertyWindowSelector).css({ 'top': 300, 'left': $canvas.outerWidth() - 280 })
            var d = $(this.propertyWindowSelector).css({ 'height': $canvas.outerHeight() - 330 })
            var d = $(this.propertyAreaSelector).css({ 'height': $canvas.outerHeight() - 400 })
        }

    }
    else {

        var d = $(this.propertyWindowSelector).css({ 'height': 203 })
        var d = $(this.propertyAreaSelector).css({ 'height': 135 })
        var d = $(this.propertyWindowSelector).css({ 'top': $canvas.outerHeight() - 230, 'left': $canvas.outerWidth() - 280 })

    }


}

function insertBrowserNode(data, name)
 {

             data += "{ 'data' : '" + name + "'},"; 
                            
 }


 function gothroughnodesresurvive(at)
 {
   for (var s in at)
   {
          alert(s);
          alert(at[s].count);
          gothroughnodesresurvive(at[s]);
   }
}

 function gatherAttributeNodes(attributes)
 {
    var at = new Array();

    for (var i=0;i<attributes.length;i++) {
        var attribi =  attributes[i].replace(/'/g, "\\'");

        var equalindex = attribi.indexOf("=");
        var beforeequal;
        if (equalindex == -1)
        {
            beforeequal = attribi;
            afterequal = "";
       }
        else
        {
            beforeequal =  attribi.substr(0, equalindex);
            afterequal = attribi.substr(equalindex, attribi.length);
        }
        var sp = beforeequal.split("/");

        var previousat = at;
        for (var j = 0; j < sp.length; j++) {

            var rsp;
            if (j == sp.length - 1) {
                rsp = sp[j] + afterequal;
            }
            else
                rsp = sp[j];

            if (previousat[rsp] == undefined)
                previousat[rsp] = new Array();

            previousat = previousat[rsp];
         }
          
    }        
    return at;  
}

PropertyWindow.prototype.buildDataRecursive = function (at, first) {

    var start = true;
    for (var s in at) {
        if (!first && start) {
            start = false;
            this.data += "'children' : [";
        }
        this.data += "{ 'data' : '" + s + "',";
        this.buildDataRecursive(at[s], false);
        this.data += "},";
       
    }

    if (!start) {
        this.data += "]";

    }
}




PropertyWindow.prototype.setProperties = function (properties) {
    var html = "";

    $(this.propertyAreaSelector).empty();

    html = "<table style='height: 96.5%; font-size:12px;'> <tr><td>";
    html += this._addPropertyIfPresent("Name", properties);
//    html += this._addPropertyIfPresent("MapId", properties);
//    html += this._addPropertyIfPresent("CadId", properties);
    html += this._addPropertyIfPresent("Parent", properties);
    html += this._addPropertyIfPresent("Surface Area", properties);
    html += this._addPropertyIfPresent("Volume", properties);
    html += this._addPropertyIfPresent("COG", properties);
    html += "</td></tr><tr><td style= 'height: 100%; width:400px'>";
    if (properties.Attributes != undefined) {

        this.attributesSet = true;
        this.data = "{ 'data' : [";
        var at = gatherAttributeNodes(properties.Attributes)
        this.buildDataRecursive(at, true);
        this.data += "] }";
        html += "<div style='font-weight:bold; top:10px; left:5px; position:relative;'>Attributes</div>";
        html += "<div id='attributeTree' style='padding-top:5px; position:relative; left: 5px;width:258px; top: 10px; height: 95%; cursor: default; border-style:solid; border-width: 1px; overflow: auto'></div>";
        html += "</td></tr></table>";
        $(this.propertyAreaSelector).html(html);

        //   this.data = "{ }";
        $.jstree._themes = "js/3rdParty/jquery/themes/";


        var ddata = "$('#attributeTree').jstree({'json_data' :" + this.data + ", themes:{ theme: 'classic', icons:false}, progressive_render: false, plugins: ['themes', 'json_data', 'hotkeys', 'ui', 'types'], core: { 'animation': 10 },});";
        ddata = ddata.replace(/sLaSh/g, "/");

        eval(ddata);


    }
    else {
        this.attributesSet = false;

        $(this.propertyAreaSelector).html(html);
    }

    this.reposition();


}

PropertyWindow.prototype.show = function () {
    $(this.propertyWindowSelector).show();
}

PropertyWindow.prototype.hide = function () {
    $(this.propertyWindowSelector).hide();
}

PropertyWindow.prototype._addPropertyIfPresent = function (name, properties) {
    var value = properties[name];
    var html = "";

    if (value) {
        html = "<div class='property' data-name='" + name + "'>"
        html +=     "<div class='propertyName'>" + name + ":&nbsp;&nbsp;</div>";
        html +=     "<div class='propertyValue'>" + value + "</div>";
        html += "</div>"
    }

    return html;
}

PropertyWindow.prototype.createTree = function (attributes) {
    var value = properties[name];
    var html = "";

    if (value) {
        html = "<div class='property' data-name='" + name + "'>"
        html += "<div class='propertyName'>" + name + ":&nbsp;&nbsp;</div>";
        html += "<div class='propertyValue'>" + value + "</div>";
        html += "</div>"
    }

    return html;
}

PropertyWindow.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvCallbacks = {
        selection: function (selection) { self.onSelection(selection); },

        loading: {
            complete: function () { self.reposition(); }
        },

        interaction: {
            begin: function () {
                $(self.propertyWindowSelector).addClass("mouseOperatorNavigating");
            },
            end: function () {
                $(self.propertyWindowSelector).removeClass("mouseOperatorNavigating");
            }
        },
    };

    hwv.setCallbacks(this.hwvCallbacks);
}