function HelpWindow() {
    this.windowAnchorSelector = "#helpWindowIcon";
    this.windowContentSelector = "#helpWindowContent";

    this.callbacks = null;

    this.helpWindowVisibile = false;

    this._registerHwvCallbacks();
}

HelpWindow.prototype._registerHwvCallbacks = function () {
    var self = this;
    this.callbacks = {
        sceneReady: function () { self._initHelpWindow(); },

        interaction: {
            begin: function () {
                $(self.windowAnchorSelector).addClass("mouseOperatorNavigating");
            },
            end: function () {
                $(self.windowAnchorSelector).removeClass("mouseOperatorNavigating");
            }
        },
    }

    hwv.setCallbacks(this.callbacks);
}

HelpWindow.prototype._initHelpWindow = function () {
    var self = this;

    this.showHelpWindowIcon();

    $(".help-viewer-version").text("Viewer version: " + hwv.getViewerVersion());
    $(".help-file-version").text("File Version: " + hwv.getFileVersion());

    $(this.windowAnchorSelector).bind("mouseenter", function () {
        self.showHelpWindow();
    });

    $(this.windowAnchorSelector).bind("mouseleave", function () {
        self.hideHelpWindow();
    });
}

HelpWindow.prototype.showHelpWindow = function () {
    if (!this.helpWindowVisibile) {
        $(this.windowContentSelector).show();
        this.helpWindowVisibile = true;
    }
}

HelpWindow.prototype.hideHelpWindow = function () {
    if (this.helpWindowVisibile) {
        $(this.windowContentSelector).hide();
        this.helpWindowVisibile = false;
    }
}

HelpWindow.prototype.showHelpWindowIcon = function () {
    $(this.windowAnchorSelector).show();
}

HelpWindow.prototype.HideHelpWindowIcon = function () {
    $(this.windowAnchorSelector).hide();
}