
if (typeof(CanvasRenderingContext2D) != 'undefined') {
    CanvasRenderingContext2D.prototype.drawSvgTreeNoStyle = function (svgNode, dx, dy, dw, dh) {
        //Convert node to XML text since this is what the canvg library uses
        var xmlSerializer = new XMLSerializer();
        var svgXml = xmlSerializer.serializeToString(svgNode);

        //Remove style attribute from the SVG text
        //The style being removed looks something like this: var styleString = 'style="width: 100%; height: 100%; position: absolute; pointer-events: none;"'; //This style is set in js/hoops_web_viewer.js
        var openBracketIndex = svgXml.indexOf('<');  //All replacements should occur between the first open and close bracket      
        var closeBracketIndex = svgXml.indexOf('>');
        var styleStartIndex = svgXml.indexOf('style');
        if (styleStartIndex > openBracketIndex && styleStartIndex < closeBracketIndex) {
            var firstQuoteStart = svgXml.indexOf('"', styleStartIndex + 1);
            var secondQuoteStart = svgXml.indexOf('"', firstQuoteStart + 1);

            //New SVG is formed from everything before and after the style attribute
            svgXml = svgXml.substr(0, styleStartIndex) + svgXml.substr(secondQuoteStart + 1);
        }

        //Draw the SVG into 2D context
        this.drawSvg(svgXml, dx, dy, dw, dh);
	}
}