﻿function ViewerSettings() {
    this.elementSelector = "#viewer-settings-dialog"
    this.maxCullingLevel = 1;
    this.maxCameraRotationMomentum = 1;
    this.context = "client_side";

    this.remoteQualitySetting = 3;
    this.currentValues = {};
}

ViewerSettings.prototype.init = function () {
    var self = this;

    $("INPUT.color-picker").miniColors({});
    

    $("#viewer-settings-ok-button").button().click(function () {
        self.applySettings();
        self.hide();
    });

    $("#viewer-settings-cancel-button").button().click(function () {
        self.hide();
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 13) { $('#viewer-settings-ok-button').click(); }
        if (e.keyCode == 27) { $('#viewer-settings-cancel-button').click(); }
    });

    if (this.context == "client_side") {
        $("#cullingSlider").slider();        
    }
    else {
        $("#qualitySlider").slider({
            min: 1,
            max: 5,
            step: 1,
            value:3
        });
    }
}

ViewerSettings.prototype.readSettings = function () {
    this.currentValues.selectionColor = hwv.getSelectionColor();
    this.currentValues.backgroundColor = hwv.getBackgroundColor();
    this.currentValues.projection = hwv.getProjectionMode();
    this.currentValues.backfaces = hwv.getShowBackfaces();
    this.currentValues.cameraRotationMomentumEnabled = hwv.getCameraRotationMomentumEnabled();
    
    $("#settings-selection-color").miniColors("value", this._colorToHex(this.currentValues.selectionColor));
    $("#settings-background-top").miniColors("value", this._colorToHex(this.currentValues.backgroundColor.top));
    $("#settings-background-bottom").miniColors("value", this._colorToHex(this.currentValues.backgroundColor.bottom));

    $("#settings-projection-mode").val(this.currentValues.projection);
    $("#settings-show-backfaces").prop("checked", this.currentValues.backfaces);
    $("#settings-orbit-turntable-camera-momentum").prop("checked", this.currentValues.cameraRotationMomentumEnabled);
    

    if (this.context == "client_side") {
        this.currentValues.measurementColor = hwv.getMeasurementGeometryColor();
        this.currentValues.selectionFaceColor = hwv.getSelectionFaceColor();
        this.currentValues.selectionMode = hwv.getSelectionMode();
        this.currentValues.cullinglevel = parseInt((hwv.getCullingLevel() / this.maxCullingLevel) * 100);
        
        $("#settings-measurement-color").miniColors("value", this._colorToHex(this.currentValues.measurementColor));
        $("#settings-selection-face-color").miniColors("value", this._colorToHex(this.currentValues.selectionFaceColor));
        $("#settings-selection-mode").val(this.currentValues.selectionMode);
        $("#cullingSlider").slider({ value: this.currentValues.cullinglevel });        
    }
    else {
        $("#qualitySlider").slider({ value: this.remoteQualitySetting });
    }
}

ViewerSettings.prototype.applySettings = function () {
    var selectionColor = this._hexToRgb($("#settings-selection-color").val());
    var backgroundColorTop = this._hexToRgb($("#settings-background-top").val());
    var backgroundColorBottom = this._hexToRgb($("#settings-background-bottom").val());
    var projection = $("#settings-projection-mode").val();
    var culling = $("#settings-show-backfaces").prop("checked");
    var cameraRotationMomentumEnabled = $("#settings-orbit-turntable-camera-momentum").prop("checked");
    

    if (!this._colorsAreEqual(this.currentValues.selectionColor, selectionColor))
        hwv.setSelectionColor(selectionColor.r, selectionColor.g, selectionColor.b);


    if (!this._colorsAreEqual(this.currentValues.backgroundColor.top, backgroundColorTop) || !this._colorsAreEqual(this.currentValues.backgroundColor.bottom, backgroundColorBottom))
        hwv.setBackgroundColor(backgroundColorTop.r, backgroundColorTop.g, backgroundColorTop.b, backgroundColorBottom.r, backgroundColorBottom.g, backgroundColorBottom.b);

    if (this.currentValues.projection != projection)
        hwv.setProjection(projection);

    if (this.currentValues.backfaces != culling)
        hwv.setShowBackfaces(culling);

    if (this.currentValues.cameraRotationMomentumEnabled != cameraRotationMomentumEnabled)
        hwv.setCameraRotationMomentumEnabled(cameraRotationMomentumEnabled);

    if (this.context == "client_side") {
        var measurementColor = this._hexToRgb($("#settings-measurement-color").val());
        var cullinglevel = ($("#cullingSlider").slider("value") / 100.0) * this.maxCullingLevel;
        var selectionMode = $("#settings-selection-mode").val();
        var selectionFaceColor = this._hexToRgb($("#settings-selection-face-color").val());

        if (!this._colorsAreEqual(this.currentValues.measurementColor, measurementColor))
        {
            hwv.setMeasurementGeometryColor(measurementColor.r, measurementColor.g, measurementColor.b);
//            hwv.setMeasurementFontColor(measurementColor.r, measurementColor.g, measurementColor.b);
        }

        if (this.currentValues.cullinglevel != cullinglevel)
            hwv.setCullingThreshold(cullinglevel);

        if (this.currentValues.selectionMode != selectionMode)
            hwv.setSelectionMode(selectionMode);

        if (!this._colorsAreEqual(this.currentValues.selectionFaceColor, selectionFaceColor))
            hwv.setSelectionFaceColor(selectionFaceColor.r, selectionFaceColor.g, selectionFaceColor.b);
    }
    else {
        var qualitySetting = $("#qualitySlider").slider("value");

        if (this.remoteQualitySetting != qualitySetting)
            this._setRemoteQualitySetting(qualitySetting);
    }
}

ViewerSettings.prototype._setRemoteQualitySetting = function (qualitySetting) {
    this.remoteQualitySetting = qualitySetting;

    console.log("new quality setting:", this.remoteQualitySetting);

    switch (this.remoteQualitySetting) {
        case 5:
            hwv.setImageQuality(100, 50);
            hwv.setScalingFactor(1.0, 0.7);
            hwv.setCullingThreshold(1, 10);
            break;

        case 4:
            hwv.setImageQuality(100, 50);
            hwv.setScalingFactor(1.0, 0.5);
            hwv.setCullingThreshold(1, 10);
            break;

        case 3:
            hwv.setImageQuality(100, 65);
            hwv.setScalingFactor(1.0, 0.25);
            hwv.setCullingThreshold(1, 10);
            break;

        case 2:
            hwv.setImageQuality(55, 35);
            hwv.setScalingFactor(0.75, 0.2);
            hwv.setCullingThreshold(1, 10);
            break;

        case 1:
            hwv.setImageQuality(40, 25);
            hwv.setScalingFactor(0.5, 0.2);
            hwv.setCullingThreshold(1, 7);
            break;
    };
}

ViewerSettings.prototype._colorsAreEqual = function (c1, c2) {
    return (c1.r == c2.r) && (c1.g == c2.g) && (c1.b == c2.b);
}

ViewerSettings.prototype._colorToHex = function (color) {
    return "#"+this._toHex(color.r * 255) + this._toHex(color.g * 255) + this._toHex(color.b * 255)
}

ViewerSettings.prototype._toHex = function(n) {
    n = parseInt(n, 10);
    if (isNaN(n)) return "00";
    n = Math.max(0, Math.min(n, 255));
    return "0123456789ABCDEF".charAt((n - n % 16) / 16)
         + "0123456789ABCDEF".charAt(n % 16);
}

ViewerSettings.prototype.hide = function () {
    var $dialog = $(this.elementSelector).hide();

    $("#toolbar-modal").remove();
}

ViewerSettings.prototype.show = function () {
    var self = this;
    this.readSettings();

    var $dialog = $(this.elementSelector);
    $dialog.show();


    $dialog.css({
        top: "calc(50% - 150px)",
        left: "calc(50% - 250px)",
    });

    $("body").append("<div id='toolbar-modal' class='toolbar-modal-overlay'></div>");
    $("#toolbar-modal").css({
        "z-index": 20999,
    });
}

ViewerSettings.prototype._hexToRgb = function(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var rgb =  result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;

    if (rgb) {
        rgb.r /= 255;
        rgb.g /= 255;
        rgb.b /= 255;
    }

    return rgb;
}