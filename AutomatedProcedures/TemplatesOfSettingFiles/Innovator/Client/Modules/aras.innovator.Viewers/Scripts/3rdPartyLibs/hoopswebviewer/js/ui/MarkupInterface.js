/*
MarkupInterface.js - javascript interface for maniupating markup data on the web server

Requires:   jquery
*/

function MarkupInterface() {
    this.viewerdataFile = "";
    this.markupDataUrl = "";
    this.defaultViewName = "";

    this.init();


    this.hwvCallbacks = null;
    this._registerHwvCallbacks();
}

MarkupInterface.prototype.init = function () {
    this.setViewerdata();
    this.setMarkupDataUrl();
}

MarkupInterface.prototype.setDefaultViewName = function (name) {
    this.defaultViewName = name;
}

MarkupInterface.prototype.setViewerdata = function () {
    var path = window.location.search.replace("?", "");
    this.viewerdataFile = path.substring(0, path.lastIndexOf(".")) + ".viewerdata";
}

MarkupInterface.prototype.setMarkupDataUrl = function () {
    //todo: this method needs to be fixed in order to work when a ? is not present.

    var url = window.location.href.substring(0, window.location.href.indexOf('?'))
    var host = url.substring(0, url.lastIndexOf("/"));

    var fileName = this._getParameterByName("file");
    if (fileName == "") {
        var queryString = window.location.href.substring(window.location.href.indexOf('?') + 1);
        var queryParams = queryString.split("&");
        fileName = queryParams[0];
    }
    else {
        fileName = fileName.substring(1);
        this.viewerdataFile = fileName.substring(0, fileName.lastIndexOf(".hwf")) + ".viewerdata";
    }

    fileName = fileName.substring(0, fileName.lastIndexOf(".hwf"));
    this.markupDataUrl = host + '/' + fileName + ".viewerdata?" + new Date().getTime().toString();
    hwv.setMarkupDataUrl(this.markupDataUrl);
}

MarkupInterface.prototype.viewSaveCallback = function (view) {
    var result = false;

    $.ajax({
        url: "ViewHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "camera": JSON.stringify(view) },
        success: function () { result = true; }
    });

    return result;
}

MarkupInterface.prototype.viewDeleteCallback = function (uniqueId) {
    var result = false;

    $.ajax({
        url: "ViewDeleteHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "uniqueId": uniqueId },
        success: function () { result = true; }
    });

    return result;
}

MarkupInterface.prototype.noteSaveCallback = function (note) {
    console.log("noteSaveCallback", note);
    $.ajax({
        url: "NoteHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "note": JSON.stringify(note) },
        success: function () { console.log("note updated remotely"); }
    });
}

MarkupInterface.prototype.noteDeleteCallback = function (uniqueId) {
    console.log("noteDeleteCallback", uniqueId);
    $.ajax({
        url: "NoteDeleteHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "uniqueId": uniqueId },
        success: function () { console.log("note deleted remotely"); }
    });
}

MarkupInterface.prototype.measurementSaveCallback = function (measurement) {
    console.log("measurementSaveCallback", measurement);

    $.ajax({
        url: "DimensionHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "type": measurement.type, measurement: JSON.stringify(measurement) },
        success: function () { console.log("note updated remotely"); }
    });
}

MarkupInterface.prototype.measurementDeleteCallback = function (uniqueId) {
    console.log("measurementDeleteCallback", uniqueId);

    $.ajax({
        url: "DimensionDeleteHandler.ashx",
        type: "POST",
        data: { "file": this.viewerdataFile, "uniqueId": uniqueId },
        success: function () { console.log("measurement deleted remotely"); }
    });
}


//todo this should be moved into a common code area.
MarkupInterface.prototype._getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

MarkupInterface.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvCallbacks = {
        notes: {
            save: function (note) { self.noteSaveCallback(note); },
            remove: function (uniqueId) { self.noteDeleteCallback(uniqueId); }
        },
        views: {
            created: function (view) { self.viewSaveCallback(view); },
            save: function (view) { self.viewSaveCallback(view); },
            remove: function (uniqueId) { self.viewDeleteCallback(uniqueId); }
        },
        measurements: {
            save: function (measurement) { self.measurementSaveCallback(measurement); },
            remove: function (uniqueId) { self.measurementDeleteCallback(uniqueId); }
        }
    };

    hwv.setCallbacks(this.hwvCallbacks);
}