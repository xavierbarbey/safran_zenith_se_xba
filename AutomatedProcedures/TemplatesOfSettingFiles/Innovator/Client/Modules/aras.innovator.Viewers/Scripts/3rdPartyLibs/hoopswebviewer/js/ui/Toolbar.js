function Toolbar(viewer) {
    this.hwv = viewer;

    this.toolbarSelector = "#toolBar";
    this.screenElementSelector = "#theCanvas";

    this.submenuHeightOffset = 10;
    this.viewOrientationDuration = 500;

    this.viewerSettings = new ViewerSettings();

    if (typeof(HWV_SSR) !== "undefined" && (this.hwv instanceof HWV_SSR)) {
        this.screenElementSelector = "#screen";
        this.viewerSettings.context = "remote";
    }

    this.activeSubmenu = null;

    this.sliderCallbacks = {
        explodeStart: null,
        explodeStop: null,
        cuttingPlaneStart: null,
        cuttingPlaneStop: null
    };

    this.hwvMethods = {};
    this.actions = {};

    this.isInit = false;

    this._registerHwvCallbacks();
}

Toolbar.prototype._initSSRSpecific = function () {
    var self = this;

    this.setSliderCallbacks({
        explodeStart: function () { self.hwv.cancelPendingDeferredUpdate() },
        explodeStop: function () { self.hwv.requestDeferredUpdate(); },
        cuttingPlaneStart: function () { self.hwv.cancelPendingDeferredUpdate() },
        cuttingPlaneStop: function () { self.hwv.requestDeferredUpdate(); }
    });

    this.disableSubmenuItem(["face", "measure-angle", "measure-distance", "measure-edge", "measure-point"]);
}

Toolbar.prototype.init = function () {
    var self = this;

    if (this.isInit)
        return;

    this._initIcons();
    this._initSnapshot();
    this.viewerSettings.init();

    this._grayOutNonApplicableIcons();

    if (typeof(HWV_SSR) !== "undefined" && (this.hwv instanceof HWV_SSR))
        this._initSSRSpecific();

    $(".hoops-tool").bind("click", function (event) {
        event.preventDefault();
        self._processButtonClick(event);
        return false;
    });

    
    $(".submenu-icon").bind("click", function (event) {
        event.preventDefault();
        self._submenuIconClick(event.target);
        return false;
    });

    $(this.toolbarSelector).bind("touchmove", function (event) {
        event.originalEvent.preventDefault();
    });
	
	$(this.toolbarSelector).bind("mouseenter", function(){
		self._mouseEnter();
	});
	
	$(this.toolbarSelector).bind("mouseleave", function(){
		self._mouseLeave();
	});

	$(".tool-icon, .submenu-icon").bind("mouseenter", function (event) {
	    self._mouseEnterItem(event);
	});

	$(".tool-icon, .submenu-icon").bind("mouseleave", function (event) {
	    self._mouseLeaveItem(event);
	});

	$(window).resize(function () {
	    self.reposition();
	});

	$(this.toolbarSelector).click(function () {
	    if (self.activeSubmenu != null) {
	        self._hideActiveSubmenu();
	    }
	});

	$(".toolbar-cp-plane").click(function (event) {
	    self._cuttingPlaneButtonClick(event);
	});

	this._initSliders();
	this._initActions();

	this.updateEdgeFaceButton();

	this.reposition();
	this.show();

	this.isInit = true;
}

Toolbar.prototype.disableSubmenuItem = function (item) {
    if (typeof (item) == "string") {
        $("#submenus .toolbar-" + item).addClass("disabled");
    }
    else if (typeof (item) == "object") {
        $.each(item, function (k, v) {
            $("#submenus .toolbar-" + v).addClass("disabled");
        });
    }
}

Toolbar.prototype.enableSubmenuItem = function (item) {
    if (typeof (item) == "string") {
        $("#submenus .toolbar-" + item).removeClass("disabled");
    }
    else if (typeof (item) == "object") {
        $.each(item, function (k, v) {
            $("#submenus .toolbar-" + v).removeClass("disabled");
        });
    }
}

Toolbar.prototype.setCorrespondingButtonForSubmenuItem = function (value) {
    var $item = $("#submenus .toolbar-" + value);
    this._activateSubmenuItem($item);
}

Toolbar.prototype._mouseEnterItem = function (event) {
    var $target = $(event.target);

    if (!$target.hasClass("disabled"))
        $target.addClass("hover");
}

Toolbar.prototype._mouseLeaveItem = function (event) {
    $(event.target).removeClass("hover");
}

Toolbar.prototype.setSliderCallbacks = function (callbacks) {
    $.extend(this.sliderCallbacks, callbacks);
}

Toolbar.prototype.show = function () {
    $(this.toolbarSelector).show();
}

Toolbar.prototype.hide = function () {
    $(this.toolbarSelector).hide();
}

Toolbar.prototype._initSliders = function () {
    var self = this;

    $("#explosion-slider").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 0,
        slide: function (event, ui) {
            hwv.setExplosionLevel(ui.value);
        },
        start: function (event, ui) {
            if (self.sliderCallbacks.explodeStart)
                self.sliderCallbacks.explodeStart();
        },
        stop: function (event, ui) {
            if (self.sliderCallbacks.explodeStop)
                self.sliderCallbacks.explodeStop();
        }
    });

    $("#cutting-slider").slider({
        orientation: "vertical",
        min: 0,
        max: 100,
        value: 0,
        slide: function (event, ui) {
            hwv.setCuttingPlanePosition(ui.value);
        },
        start: function (event, ui) {
            if (self.sliderCallbacks.cuttingPlaneStart)
                self.sliderCallbacks.cuttingPlaneStart();
        },
        stop: function (event, ui) {
            if (self.sliderCallbacks.cuttingPlaneStop)
                self.sliderCallbacks.cuttingPlaneStop();
        }
    });
}

Toolbar.prototype._mouseEnter = function(){
	if (this.activeSubmenu == null){
		var $tools = $(this.toolbarSelector).find( ".toolbar-tools" );
		$tools.stop();
		
		$tools.css({
			opacity: 1.0
		});
	}
}

Toolbar.prototype._mouseLeave = function(){
	if (this.activeSubmenu == null){
	  $( ".toolbar-tools" ).animate({
		opacity: 0.6,
	  }, 500, function() {
		// Animation complete.
	  });
	}
}

Toolbar.prototype.reposition = function () {
    var $toolbar = $(this.toolbarSelector);
    var $screen = $(this.screenElementSelector);

    var canvasCenterX = $screen.width() / 2;
    var toolbarX = canvasCenterX - ($toolbar.width() /2);

    $toolbar.css({
        left: toolbarX + "px",
        bottom: "15px"
    });
}

Toolbar.prototype._processButtonClick = function (event) {

    if (this.activeSubmenu != null) {
        this._hideActiveSubmenu();
    }
    else {
        var $tool = $(event.target).closest(".hoops-tool");


        if ($tool.hasClass("toolbar-radio")) {

            if ($tool.hasClass("active-tool")) {
                this._showSubmenu(event.target)
            }
            else {
                $(this.toolbarSelector).find(".active-tool").removeClass("active-tool");
                $tool.addClass("active-tool");
                this._performAction($tool.data("operatorclass"));
            }
        }
        else if ($tool.hasClass("toolbar-menu")) {
            this._showSubmenu(event.target)
        }
        else if ($tool.hasClass("toolbar-slider")) {
            this._togglSiderTool($tool);
        }
        else {
            this._performAction($tool.data("operatorclass"));
        }
    }
}

Toolbar.prototype._togglSiderTool = function ($tool) {
    var $sliderMenu = $("#" + $tool.data("slider"));

    if ($sliderMenu.is(":visible")) {
        $sliderMenu.hide();
        this._performAction($tool.data("operatorclass"), false);
    }
    else {
        this._alignMenuToTool($sliderMenu, $tool);
        this._performAction($tool.data("operatorclass"), true);
    }
}

Toolbar.prototype._startModal = function () {
    $("body").append("<div id='toolbar-modal' class='toolbar-modal-overlay'></div>");

    var self = this;
    $("#toolbar-modal").bind("click", function () {
        self._hideActiveSubmenu();
    });
}

Toolbar.prototype._alignMenuToTool = function ($submenu, $tool) {
    var position = $tool.position();
    var topPos = -(this.submenuHeightOffset + $submenu.height());
    var leftpos = position.left - $submenu.width()/2+ 20;
    $submenu.css({
        display: "block",
        top: topPos + "px",
        left: leftpos
    });
}

Toolbar.prototype._showSubmenu = function (item) {
    this._hideActiveSubmenu();

    var $tool = $(item).closest(".hoops-tool");
    var submenuId = $tool.data("submenu");

    if (submenuId != null) {
        var $submenu = $(this.toolbarSelector + " #submenus #" + submenuId);

        if (!$submenu.hasClass("disabled")) {
            this._alignMenuToTool($submenu, $tool);
            this.activeSubmenu = $submenu[0];
            this._startModal();

            $(this.toolbarSelector).find(".toolbar-tools").css({
                opacity: 0.3
            });
        }
    }
}

Toolbar.prototype._hideActiveSubmenu = function () {
    $("#toolbar-modal").remove();

    if (this.activeSubmenu != null) {
        $(this.activeSubmenu).hide();
		
		$(this.toolbarSelector).find(".toolbar-tools").css({
			opacity: 1.0
		});
    }

    this.activeSubmenu = null;
}

Toolbar.prototype._activateSubmenuItem = function (submenuItem) {
    var $submenu = submenuItem.closest(".toolbar-submenu");
    var action = submenuItem.data("operatorclass")

    var $tool = $('#' + $submenu.data("button"));
    

    var $icon = $tool.find(".tool-icon");
    $icon.removeClass($tool.data("operatorclass"));
    $icon.addClass(action);

    $tool.data("operatorclass", action);
    $tool.attr("title", submenuItem.attr("title"))

    return action;
}

Toolbar.prototype._submenuIconClick = function (item) {
    var $selection = $(item);

    if ($selection.hasClass("disabled"))
        return;

    var action = this._activateSubmenuItem($selection);

    this._hideActiveSubmenu();
    this._performAction(action);
}

Toolbar.prototype._initIcons = function () {
    $(this.toolbarSelector).find(".hoops-tool").each(function () {
        var $element = $(this);

        $element.find(".tool-icon").addClass($element.data("operatorclass"));
    });

    $(this.toolbarSelector).find(".submenu-icon").each(function () {
        var $element = $(this);

        $element.addClass($element.data("operatorclass"));
    });
}

Toolbar.prototype._grayOutNonApplicableIcons = function () {

    if (!hwv.getModelCanBeMeasured()) {
        this.disableSubmenuItem(["measure-edge"]);
        this.disableSubmenuItem(["measure-angle"]);
        this.disableSubmenuItem(["measure-distance"]);
        this.disableSubmenuItem(["face"]);
    }

    if (hwv.getPartsOptimized()) 
        $("#explode-button").remove();

    if (hwv.getModelIs2D()) {
        this.disableSubmenuItem(["measure-point", "note"]);

        $("#view-button").remove();
        $("#edgeface-button").remove();
        $("#camera-button").remove();
        $("#cuttingplane-button").remove();
        $("#explode-button").remove();
        $(".tool-seperator").remove();

        this.setSubmenuEnabled("click-button", false);
    }
}

Toolbar.prototype.setSubmenuEnabled = function (button, enabled) {
    var $button = $("#" + button);
    var $submenu = $('#' + $button.data("submenu"));

    if (enabled) {
        $button.find(".smarrow").show();
        $submenu.removeClass("disabled");
    }
    else {
        $button.find(".smarrow").hide();
        $submenu.addClass("disabled");
    }
}

Toolbar.prototype._registerHwvCallbacks = function () {
    var self = this;

    this.hwvMethods.loadComplete = function () {
        self.init();
    };

    hwv.setCallbacks({
        sceneReady: this.hwvMethods.loadComplete,
        interaction: {
            begin: function () {
                $(self.toolbarSelector).addClass("mouseOperatorNavigating");
            },
            end: function () {
                $(self.toolbarSelector).removeClass("mouseOperatorNavigating");
            }
        }
    });
}

Toolbar.prototype._performAction = function (action, arg) {
    var func = this.actions[action];

    if (func) {
        func.apply(null, [action,arg]);
    }
}

Toolbar.prototype._edgeFaceVisibilityClick = function (action) {
    hwv.setRenderMode(hwv_RenderModeShaded);

    switch (action) {
        case "toolbar-shaded":
            hwv.setEdgeVisibility(false);
            hwv.setFaceVisibility(true);
            break;

        case "toolbar-wireframe":
            hwv.setEdgeVisibility(true);
            hwv.setFaceVisibility(false);
            break;

        default:
            hwv.setEdgeVisibility(true);
            hwv.setFaceVisibility(true);
    };
}

Toolbar.prototype._updateButtons = function() 
{

            jQuery("#click-button").addClass("active-tool");
            jQuery("#redline-button").removeClass("active-tool");
            var element = jQuery("#click-button").html();
			if (element.indexOf("toolbar-select") != -1)		
                hwv.setCurrentOperator(hwv_SelectOperator)		
			else if (element.indexOf("toolbar-note") != -1)		
                hwv.setCurrentOperator(hwv_NoteOperator); 
			else if (element.indexOf("toolbar-measure-edge") != -1)		
                hwv.setCurrentOperator(hwv_LengthMeasureOperator); 
			else if (element.indexOf("toolbar-measure-point") != -1)		
                hwv.setCurrentOperator(hwv_PointMeasureOperator); 
			else if (element.indexOf("toolbar-measure-angle") != -1)		
                hwv.setCurrentOperator(hwv_AngleMeasureOperator); 
			else if (element.indexOf("toolbar-measure-distance") != -1)		
                hwv.setCurrentOperator(hwv_DistanceMeasureOperator); 
}

Toolbar.prototype._initActions = function () {
    var self = this;
    this.actions["toolbar-home"] = function () { hwv.resetCamera(); };

    this.actions["toolbar-redline-circle"] = function () { hwv.setCurrentOperator(hwv_RedlineCircleOperator); };
    this.actions["toolbar-redline-freehand"] = function () { hwv.setCurrentOperator(hwv_RedlineLineOperator); };
    this.actions["toolbar-redline-rectangle"] = function () { hwv.setCurrentOperator(hwv_RedlineRectangleOperator); };
    this.actions["toolbar-redline-note"] = function () { hwv.setCurrentOperator(hwv_RedlineTextOperator); };

    this.actions["toolbar-orbit"] = function () { hwv.setCameraOperator(hwv_CameraOrbitOperator); self._updateButtons(); };
    this.actions["toolbar-turntable"] = function () { hwv.setCameraOperator(hwv_TurntableOperator); self._updateButtons(); };
    this.actions["toolbar-walk"] = function () { hwv.setCameraOperator(hwv_WalkOperator); self._updateButtons(); };

    this.actions["toolbar-select"] = function () { hwv.setCurrentOperator(hwv_SelectOperator); };
    this.actions["toolbar-note"] = function () { hwv.setCurrentOperator(hwv_NoteOperator); };
    this.actions["toolbar-measure-edge"] = function () { hwv.setCurrentOperator(hwv_LengthMeasureOperator); };
    this.actions["toolbar-measure-point"] = function () { hwv.setCurrentOperator(hwv_PointMeasureOperator); };
    this.actions["toolbar-measure-angle"] = function () { hwv.setCurrentOperator(hwv_AngleMeasureOperator); };
    this.actions["toolbar-measure-distance"] = function () { hwv.setCurrentOperator(hwv_DistanceMeasureOperator); };

    var edgeFaceClick = function (action) { self._edgeFaceVisibilityClick(action); };
    this.actions["toolbar-wireframeshaded"] = edgeFaceClick;
    this.actions["toolbar-shaded"] = edgeFaceClick;
    this.actions["toolbar-wireframe"] = edgeFaceClick;
    this.actions["toolbar-hidden-line"] = function () { hwv.setRenderMode(hwv_RenderModeHiddenLine); }

    this.actions["toolbar-front"] = function () { hwv.setViewOrientation(hwv_FrontView, self.viewOrientationDuration); };
    this.actions["toolbar-back"] = function () { hwv.setViewOrientation(hwv_BackView, self.viewOrientationDuration); };
    this.actions["toolbar-left"] = function () { hwv.setViewOrientation(hwv_LeftView, self.viewOrientationDuration); };
    this.actions["toolbar-right"] = function () { hwv.setViewOrientation(hwv_RightView, self.viewOrientationDuration); };
    this.actions["toolbar-bottom"] = function () { hwv.setViewOrientation(hwv_BottomView, self.viewOrientationDuration); };
    this.actions["toolbar-top"] = function () { hwv.setViewOrientation(hwv_TopView, self.viewOrientationDuration); };
    this.actions["toolbar-iso"] = function () { hwv.setViewOrientation(hwv_IsoView, self.viewOrientationDuration); };
    this.actions["toolbar-face"] = function () { hwv.setCurrentOperator(hwv_ZoomToFaceOperator); };
    this.actions["toolbar-cuttingplane"] = function (action, visibility) { hwv.setCuttingPlaneVisibility(visibility); };
    this.actions["toolbar-explode"] = function (action, visibility) { self._explosionSliderButtonClick(); };
    this.actions["toolbar-snapshot"] = function () { self._doSnapshot(); };
    this.actions["toolbar-settings"]=  function () { self._doSettings(); };
}

Toolbar.prototype._cuttingPlaneButtonClick = function (event) {
    var $element = $(event.target).closest(".toolbar-cp-plane");
    var plane = parseInt($element.data("plane"));

    if (plane <= 2) {
        $(".toolbar-cp-plane.selected").removeClass("selected");
        $element.addClass("selected");
        $('#cutting-slider').slider("option", "value", 0);
    }

    hwv.setCuttingPlaneOrientation(plane);
}

Toolbar.prototype._explosionSliderButtonClick = function () {
    hwv.setExplosionLevel(0);
    $("#explosion-slider").slider("option", "value", 0);
}

Toolbar.prototype._explosionButtonClick = function (visibility) {
    if (visibility) {
        var explosionValue = hwv.getExplosionLevel();
        $('#explosion-slider').slider("option", "value", explosionValue);
    }
    else {
        hwv.setExplosionLevel(0);
    }
}

Toolbar.prototype._initSnapshot = function () {
    $("#snapshot-dialog-cancel-button").button().click(function () {
        $("#snapshot-dialog").hide();
    });
}

Toolbar.prototype._doSnapshot = function () {
    var $screen = $(this.screenElementSelector);
    var windowWidth = $screen.width();
    var windowHeight = $screen.height();
    
    var percentageOfWindow = .7; 
    if (percentageOfWindow > 0) {
        //Using a percentage of the window size since allows the user to resize the image
        var renderHeight = windowHeight * percentageOfWindow;
        var renderWidth = windowWidth * percentageOfWindow;    
    } else {
        //Old style of using a somewhat fixed size. User can still resize, though the height is constant
        var windowAspect = windowWidth / windowHeight;

        var renderHeight = 480;
        var renderWidth = windowAspect * renderHeight;    
    }

    renderWidth = parseInt(renderWidth);
    renderHeight = parseInt(renderHeight);

    var dialogWidth = renderWidth + 40;

    var handler = function (dataUrl) {
        var xpos = parseInt((windowWidth - renderWidth) / 2);
        var $dialog = $("#snapshot-dialog");

        $("#snapshot-dialog-image").attr("src", dataUrl).attr("width", renderWidth).attr("height", renderHeight);

        $dialog.css({
            top: "45px",
            left: xpos.toString() + "px",
        });

        $dialog.show();
    };

    var imageFormat = null; //Use default image format

    hwv.getSnapshot(renderWidth, renderHeight, handler, imageFormat);
}

Toolbar.prototype._doSettings = function () {
    this.viewerSettings.show();
}

Toolbar.prototype.updateEdgeFaceButton = function () {
    var edgeVisibility = hwv.getEdgeVisibility();
    var faceVisibility = hwv.getFaceVisibility();

    if (edgeVisibility && faceVisibility)
        this.setCorrespondingButtonForSubmenuItem("wireframeshaded");
    else if (!edgeVisibility && faceVisibility) 
        this.setCorrespondingButtonForSubmenuItem("shaded");
    else
        this.setCorrespondingButtonForSubmenuItem("wireframe");
}