function InfoDialog() {
    this.windowSelector = "#infoDialog";


    this._initDialog();
    this._registerHwvCallbacks();
    this.buttonAction = "close";
}

InfoDialog.prototype._initDialog = function () {
    var self = this;

    $(this.windowSelector).draggable({ scroll: false, handle: ".infoDialogHeader" });

    $(this.windowSelector + " #resetButton").button().click(function () {
        self._buttonClick();
    });
}

InfoDialog.prototype._registerHwvCallbacks = function () {
    var self = this;

    hwv.setInfoCallback(function (messageType, message, code) {
        self._onInfoMessage(messageType, message, code);
    });
}

InfoDialog.prototype._updateButton = function (messageType, message, code) {

    switch (messageType) {
        case hwv_Info:
        case hwv_Warning:
            this.buttonAction = InfoDialog.BUTTON_ACTION_CLOSE;
            break;

        case hwv_Error:
            this.buttonAction = InfoDialog.BUTTON_ACTION_RELOAD;
            break;
    };

    jQuery(this.windowSelector + " #resetButton span").text(this.buttonAction);
};

InfoDialog.prototype._onInfoMessage = function (messageType, message, code) {
    var titleString = "";

    switch (messageType) {
        case hwv_Info:
            titleString = "Info";
            break;

        case hwv_Warning:
            titleString = "Warning";
            break;

        case hwv_Error:
            titleString = "Error";
            break;
    };

    $(this.windowSelector + " .messageType").text(titleString);
    $(this.windowSelector + " .messageText").text(message);

    this._updateButton(messageType, message, code);

    this.center();

    this.show();
}

InfoDialog.prototype.center = function () {
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();

    var width = $(this.windowSelector).width();
    var height = $(this.windowSelector).height();

    var left = parseInt((windowWidth - width) / 2);
    var top = parseInt((windowHeight - height) / 2);

    $(this.windowSelector).css({
        left: left.toString() + "px",
        top: top.toString() + "px"
    });
    
}

InfoDialog.prototype._buttonClick = function () {
    if (this.buttonAction == InfoDialog.BUTTON_ACTION_CLOSE)
        this.hide();
    else if (this.buttonAction == InfoDialog.BUTTON_ACTION_RELOAD)
        location.reload();
}

InfoDialog.prototype.show = function () {
    $(this.windowSelector).show();
}

InfoDialog.prototype.hide = function () {
    $(this.windowSelector).hide();
}

InfoDialog.BUTTON_ACTION_CLOSE = "Close";
InfoDialog.BUTTON_ACTION_RELOAD = "Reload";