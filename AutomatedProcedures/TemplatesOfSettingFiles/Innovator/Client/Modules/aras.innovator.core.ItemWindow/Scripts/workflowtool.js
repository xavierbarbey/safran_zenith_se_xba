﻿var containerWidget = null;
var editorPaneWidget = null;
var formsPaneWidget = null;
var tabsPaneWidget = null;

//ready state flags
var isBusy = true;
var propsFReady = false;
var editorFReady = false;

var isWorkflowTool = true;
var currWFID = itemID;
var currWFNode = null;
var tabbars = {};

aras.setDefaultMessage(aras.getResource('', 'common.ready'), '');
var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.stage1'), urlToResources.progressBarGif);

var pathIdConst = aras.getRelationshipTypeId('Workflow Map Path');
var activityIdConst = aras.getRelationshipTypeId('Workflow Map Activity');
var menuFrame;

aras.clearStatusMessage(statusId);

function getFrameWindow(frameId) {
	return document.getElementById(frameId).contentWindow;
}

function setAnyMode(vORe) {
	updateWorkflowItem();
	item = currWFNode;
	refreshMenuState();
	getFrameWindow('tabs')['set' + vORe + 'Mode']();
	getFrameWindow('editor').populateWorkflow();
}

function setEditMode() {
	isEditMode = true;
	setAnyMode('Edit');
	if (parent.setTitle) {
		parent.setTitle(isEditMode);
	}
}

function setViewMode(newID) {
	isEditMode = false;
	if (newID) {
		currWFID = newID;
	}
	setAnyMode('View');
	if (parent.setTitle) {
		parent.setTitle(isEditMode);
	}
}

function setFlag(flgName, state) {
	window[flgName] = state;
	var prevBusy = isBusy;
	isBusy = !(propsFReady && editorFReady);
	/*
	if ( (prevBusy==true) && (isBusy==false) )
		document.frames['editor'].workflowApplet.enablePopup(true);
	else if (isBusy){
		if (editorFReady)
			document.frames['editor'].workflowApplet.enablePopup(false);
	}
	*/
}

var formsCache = null;
function initFormsCache(defaultContainerId) {
	formsCache = {};
	formsCache.defaultMode = 'view';
	formsCache.defaultContainerId = defaultContainerId;
	formsCache.cache = {};
}
initFormsCache('forms');

function getItemForm(itemTypeName, formMode) {
	if (formsCache) {
		if (formsCache.cache.hasOwnProperty(itemTypeName)) {
			return formsCache.cache[itemTypeName][formMode];
		}
	}

	return undefined;
}

function hideAllItemForms(formContainer) {
	var itemFormCache;
	var form;

	for (var itemTypeName in formsCache.cache) {
		itemFormCache = formsCache.cache[itemTypeName];

		for (var modeName in itemFormCache) {
			form = itemFormCache[modeName];
			if (form && form.containerElement === formContainer) {
				form.style.display = 'none';
			}
		}
	}
}

// returns first visible form in container
function getVisibleItemForm(containerElement) {
	var itemFormCache;
	var form;

	containerElement = containerElement || document.getElementById(formsCache.defaultContainerId);
	for (var itemTypeName in formsCache.cache) {
		itemFormCache = formsCache.cache[itemTypeName];

		for (var modeName in itemFormCache) {
			form = itemFormCache[modeName];
			if (form && form.containerElement == containerElement && form.style.display !== 'none') {
				return form;
			}
		}
	}
	return null;
}

// adds form for itemTypeName in formMode to cache
function addFormToCache(itemTypeName, formMode, form) {
	if (itemTypeName && formMode) {
		if (!formsCache.cache.hasOwnProperty(itemTypeName)) {
			formsCache.cache[itemTypeName] = {};
		}

		formsCache.cache[itemTypeName][formMode] = form;
	}
}

// descriptionNode: xmlElement with item description, containerElement: domElement which form will be attached to
function showItemForm(itemTypeName, formMode, descriptionNode, containerElement, userChangeHandler) {
	if (formsCache) {
		var cachedForm = null;
		itemTypeName = itemTypeName || '';
		formMode = formMode || formsCache.defaultMode;
		containerElement = containerElement || document.getElementById(formsCache.defaultContainerId);

		if (itemTypeName) {
			cachedForm = getItemForm(itemTypeName, formMode);

			if (!cachedForm) {
				var formId = itemTypeName + '_' + formMode;

				cachedForm = document.createElement('iframe');
				cachedForm.setAttribute('id', formId);
				cachedForm.setAttribute('frameBorder', '0');
				cachedForm.setAttribute('width', '100%');
				cachedForm.setAttribute('height', '100%');
				cachedForm.style.position = 'relative';

				cachedForm.formContentLoaded = false;
				cachedForm.itemTypeName = itemTypeName;
				containerElement.appendChild(cachedForm);
				cachedForm.containerElement = containerElement;
				addFormToCache(itemTypeName, formMode, cachedForm);
			}
			// if user send description then fill form with item properties
			if (descriptionNode) {
				propsFReady = false;
				if (cachedForm.formContentLoaded) {
					aras.uiPopulateFormWithItemEx(cachedForm.contentDocument, descriptionNode, '', formMode == 'edit');
					propsFReady = true;
				} else {
					aras.uiShowItemInFrameEx(cachedForm.contentWindow, descriptionNode, formMode);
					cachedForm.onload = function() {
						ITEM_WINDOW.registerStandardShortcuts(this.contentWindow);
						if (returnBlockerHelper) {
							returnBlockerHelper.attachBlocker(this.contentWindow);
						}

						cachedForm.contentDocument.userChangeHandler = userChangeHandler;
						cachedForm.contentDocument.documentElement.focus();
						cachedForm.formContentLoaded = true;
						propsFReady = true;
					};
				}
			}
		}

		hideAllItemForms(containerElement);
		if (cachedForm) {
			cachedForm.style.display = '';
		}

		return cachedForm;
	}
}

function refreshMenuState() {
	if (!menuFrame) {
		menuFrame = tearOffMenuController;
	}

	var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.updating_menu'), '');
	var isTemp = aras.isTempEx(currWFNode);

	menuFrame.setControlEnabled('new', true);
	menuFrame.setControlEnabled('edit', false);
	menuFrame.setControlEnabled('view', false);
	menuFrame.setControlEnabled('print', true);
	menuFrame.setControlEnabled('lock', !isEditMode && !aras.isLocked(currWFNode));
	menuFrame.setControlEnabled('delete', !isEditMode || isTemp);
	menuFrame.setControlEnabled('save', isEditMode);
	menuFrame.setControlEnabled('save_unlock_close', isEditMode);
	menuFrame.setControlEnabled('unlock', (isEditMode || aras.getLoginName().search(/^admin$|^root$/) === 0 && aras.isLocked(currWFNode)) && !isTemp);
	menuFrame.setControlEnabled('undo', isEditMode && !isTemp);

	if (menuFrame.populateAccessMenuLazyStart) {
		menuFrame.populateAccessMenuLazyStart(isEditMode, currWFNode);
	}

	aras.clearStatusMessage(statusId);
	var topWindow = aras.getMostTopWindowWithAras(window);
	if (topWindow && topWindow.cui) {
		topWindow.cui.callInitHandlers('UpdateTearOffWindowState');
	}
}

function setupToolOnLoad() {
	refreshMenuState();

	containerWidget = dijit.byId('worflowToolContent');
	editorPaneWidget = dijit.byId('editorPane');
	formsPaneWidget = dijit.byId('forms');
	tabsPaneWidget = dijit.byId('relationshipsPane');

	window.tabsHeightRatio = 0.25;
	window.editorHeightRatio = 0.4;

	refreshPanesHeightRatio();

	var frameWindow = getFrameWindow('editor');

	frameWindow.frameElement.addEventListener('load', function() {
		aras.browserHelper.toggleSpinner(document, false);
	});

	frameWindow.location.replace(urlToResources.workflowEditor);

	topSplitter = containerWidget.getSplitter('top');
	dojo.connect(topSplitter, '_stopDrag', window, 'refreshPanesHeightRatio');
	bottomSplitter = containerWidget.getSplitter('bottom');
	dojo.connect(bottomSplitter, '_stopDrag', window, 'refreshPanesHeightRatio');

	//register standart chortcuts
	ITEM_WINDOW.registerStandardShortcuts(window, true);
	aras.setDefaultMessage('status', aras.getResource('', 'common.ready'), '');
	aras.setDefaultMessage('page_status', '"' + aras.getNodeElement(currWFNode, 'name') + '" Workflow Map');
}

/* remove from cache old workflow, get new from server, put to cache,store in currWFNode */
function updateWorkflowItem() {
	//---------------------------------------------------------------------
	//Added during "Consolidate Delegated check box" bug fixing
	//In "ActivityTemplate_Request" below the "select" attribute had not contained field "consolidate_ondelegate", so I append it.
	//But name of this field may be changed by Innovator's administrator in 'Forms' table.
	//May be, should delete the "select" atrribute completely? I did it and save the
	//"select" atrribute in comments.
	//select='timeout_duration,priority,reminder_interval,expected_duration,reminder_count,escalate_to,can_delegate,
	//keyed_name,x,y,is_end,message,wait_for_all_votes,wait_for_all_inputs,icon,is_start,is_auto,can_refuse,name,subflow,consolidate_ondelegate'
	//---------------------------------------------------------------------
	var workflowRqBody = '' +
		'<id>' + currWFID + '</id>' +
		'<Relationships>' +
		'<Item type="Workflow Map Activity" action="get" isCriteria="0" select="related_id">' +
		'<related_id>' +
		'<Item type="Activity Template" action="get">' +
		'<Relationships>' +
		'<Item type="Workflow Map Path" action="get" isCriteria="0" select="is_override,' +
		'authentication, x, y, name, label, segments, related_id, source_id, is_default" />' +
		'</Relationships>' +
		'</Item>' +
		'</related_id>' +
		'</Item>' +
		'</Relationships>';
	var workflowItem = aras.getItem('Workflow Map', '@id=\'' + currWFID + '\'', workflowRqBody, 0, '',
		'description,node_label1_color,node_label2_font,node_bg_color,transition_line_color,node_name_color,node_label1_font,' +
		'transition_name_color,node_name_font,node_label2_color,node_size,state,name,not_lockable,locked_by_id,keyed_name,process_owner,label');
	if (!workflowItem) {
		return;
	}

	currWFNode = workflowItem;
	aras.itemsCache.updateItem(currWFNode);
}
updateWorkflowItem();

try {
	//isEditMode = (aras.isTempEx(currWFNode) || aras.isLockedByUser(currWFNode));
	window.addEventListener('loadWidgets', setupToolOnLoad);

	window.onunload = function() {
		if (currWFNode) {
			aras.itemsCache.updateItem(currWFNode);
		}
	};
} catch (e) {
	aras.AlertError(aras.getResource('', 'workflowtool.internal_err_init'));
}

function initializeTabbars(typesArray) {
	for (var i = 0; i < typesArray.length; i++) {
		var tmpTabbar = [];
		var tabsRoot = aras.uiGenerateRelationshipsTabbar(typesArray[i], aras.getItemTypeId(typesArray[i]));
		var tabNds = tabsRoot.selectNodes('/tabbar/tab');
		for (var j = 0; j < tabNds.length; j++) {
			var tab = tabNds[j];
			tmpTabbar[j] = {'type_name': tab.getAttribute('type_name'), 'label': tab.getAttribute('label'), 'id': tab.getAttribute('id')};
		}

		tabbars[typesArray[i]] = tmpTabbar;
	}
}
initializeTabbars(['Workflow Map', 'Activity Template', 'Workflow Map Path']);

function refreshPanesHeightRatio() {
	var containerHeight = containerWidget.domNode.offsetHeight;

	window.editorHeightRatio = editorPaneWidget.domNode.offsetHeight / containerHeight;
	if (!window.hideTabs) {
		window.tabsHeightRatio = tabsPaneWidget.domNode.offsetHeight / containerHeight;
	} else {
		if (window.editorHeightRatio + window.tabsHeightRatio > 0.95) {
			window.tabsHeightRatio = (1 - window.editorHeightRatio) / 2;
			tabsPaneWidget.domNode.style.height = Math.round(containerHeight * window.tabsHeightRatio) + 'px';
		}
	}
}

function resizeHandler() {
	if (!containerWidget) {
		return;
	}

	var containerHeight = containerWidget.domNode.offsetHeight;
	containerWidget.previousHeight = containerWidget.previousHeight || 0;
	if (containerHeight != containerWidget.previousHeight) {
		editorPaneWidget.domNode.style.height = Math.round(containerHeight * window.editorHeightRatio) + 'px';
		tabsPaneWidget.domNode.style.height = Math.round(containerHeight * window.tabsHeightRatio) + 'px';

		containerWidget.previousHeight = containerHeight;
	}
}
