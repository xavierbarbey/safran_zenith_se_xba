﻿/* jshint ignore:start */
// jscs:disable
//TODO: remove ignore
define([
	'dojo/_base/declare',
	'Aras/Client/Controls/Public/TreeGridContainer'
],
function(declare, TreeGridContainer) {
	var dojoxGridLazyExpandoSetImageSrc = dojox.grid._LazyExpando.prototype._setImageSrc;

	dojo.extend(dojox.grid._LazyExpando, {
		_setImageSrc: function(item, open) {
			var collapsedIcon = item.icon && item.icon[0],
				expandedIcon = item.expandedIcon && item.expandedIcon[0];

			if (collapsedIcon) {
				if (collapsedIcon.indexOf('tgv=1') < 0) {
					// add suffix to avoid taking svg icon(without suffix) that already resized in browser cache.
					// (bug of IE and Edge)
					collapsedIcon += collapsedIcon.indexOf('?') < 0 ? '?tgv=1' : '&tgv=1';
				}
				item.icon[0] = dojoConfig.arasContext.adjustIconUrl(collapsedIcon);
			}
			if (expandedIcon && open) {
				item.expandedIcon[0] = dojoConfig.arasContext.adjustIconUrl(expandedIcon);
			}
			dojoxGridLazyExpandoSetImageSrc.apply(this, arguments);
		}
	});

	return declare('TreeGridView.Scripts.RBTreeGridContainer', TreeGridContainer, {
	});
});
/* jshint ignore:end */