﻿function ItemDefault(inputConditions) {
	var outputConditions = {};
	var outputCondition;
	var inputCondition;
	var	i;

	for (i in inputConditions) {
		if (!inputConditions.hasOwnProperty(i)) {
			continue;
		}
		inputCondition = inputConditions[i];
		outputCondition = inputCondition !== 'id' ? parent.aras.getItemProperty(parent.item, inputCondition) : parent.item.getAttribute('id');
		outputConditions[i] = outputCondition;
	}

	this._outputConditions = outputConditions;
}

ItemDefault.prototype.getConditions = function() {
	return this._outputConditions;
};
