﻿define(['SSVC/Scripts/Classes/ViewSettingsManager'],
	function(ViewSettingsManager) {
		var SSVCDataManager = (function() {
			function SSVCDataManager(aras) {
				this.aras = aras;
				this.defaultFilesForViewing = [];
				this.defaultSsvcFormViewSettings = {};
				this.filesForViewing = this.defaultFilesForViewing;
				this.ssvcFormViewSettings = this.defaultSsvcFormViewSettings;
			}
			/**
			 * Update SSVC files for viewing and form view settings.
			 * Use @{link SSVCDataManager#getFilesForViewing} and @{link SSVCDataManager#getSsvcFormViewSettings} to get updated data.
			 * Method will set files for viewing to empty array if update files fails.
			 * Method will set SSVC form view settings to empty object if update settings fails.
			 *
			 * @param {Object} itemNode
			 * @return {Promise}
			 */
			SSVCDataManager.prototype.updateDataForItem = function(itemNode) {
				var _this = this;
				var itemId = itemNode.getAttribute('id');
				var itemTypeName = itemNode.getAttribute('type');
				var configId = this.aras.getItemProperty(itemNode, 'config_id');
				var isNew = this.aras.isNew(itemNode);
				var filesForViewing = Promise.resolve()
					.then(function() {
						return isNew ? _this.defaultFilesForViewing : _this.fetchFilesForViewing(itemTypeName, itemId);
					})
					.then(function(files) {
						return {
							success: true,
							result: files
						};
					})
					.catch(function(error) {
						return {
							success: false,
							result: error
						};
					});
				var ssvcFormViewSettings = this.fetchSsvcFormViewSettings(itemTypeName, itemId, configId)
					.then(function(settings) {
						return {
							success: true,
							result: settings
						};
					})
					.catch(function(error) {
						return {
							success: false,
							result: error
						};
					});
				return Promise.all([filesForViewing, ssvcFormViewSettings])
					.then(function(results) {
						var filesResult = results[0];
						var settingsResult = results[1];
						_this.filesForViewing = filesResult.success ? filesResult.result : _this.defaultFilesForViewing;
						_this.ssvcFormViewSettings = settingsResult.success ? settingsResult.result : _this.defaultSsvcFormViewSettings;
						if (!filesResult.success && !settingsResult.success) {
							throw new Error('Errors occurred during update data for SSVC.');
						} else if (!filesResult.success) {
							return Promise.reject(filesResult.result);
						} else if (!settingsResult.success) {
							return Promise.reject(settingsResult.result);
						} else {
							return;
						}
					});
			};
			SSVCDataManager.prototype.getFilesForViewing = function() {
				return this.filesForViewing;
			};
			SSVCDataManager.prototype.getSsvcFormViewSettings = function() {
				return this.ssvcFormViewSettings;
			};
			SSVCDataManager.prototype.fetchFilesForViewing = function(itemTypeName, itemId) {
				var getFilesForViewingRequestItem = ArasModules.jsonToXml({
					Item: {
						'@attrs': {
							type: 'Method',
							action: 'VC_GetFilesForViewingForItem'
						},
						itemTypeName: itemTypeName,
						itemId: itemId
					}
				});
				return ArasModules.soap(getFilesForViewingRequestItem, {async: true})
					.then(function(resNode) {
						return resNode.childNodes;
					});
			};
			SSVCDataManager.prototype.fetchSsvcFormViewSettings = function(itemTypeName, itemId, configId) {
				var settingsManager = new ViewSettingsManager({aras: this.aras});
				return settingsManager.getSsvcFormViewSettings({itemTypeName: itemTypeName, itemId: itemId, configId: configId});
			};
			return SSVCDataManager;
		}());
		return new SSVCDataManager(window.aras);
	});
