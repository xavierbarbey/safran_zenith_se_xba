﻿// (c) Copyright by Aras Corporation, 2004-2013.
var currQryItem;
var currItemType = null;
var itemTypeID = '';
var itemTypeName = '';
var itemTypeLabel = '';
var visiblePropNds;
var userMethodColumnCfgs = {}; //to support OnSearchDialog grid event
var searchLocation = '';

var page = 1;
var pagemax = -1;
var itemmax = 0;
var pagesize = '';
var maxRecords = '';
var inputCol = -1;
var ps;
var inputRowId = 'input_row';

var xmlReadyFlag = false;
var promiseCountResult;

function initPage(isPopup) {
	var criteriaName;
	var criteriaValue;

	if (itemTypeID) {
		//itemTypeID has higher priority because of poly items
		criteriaName = 'id';
		criteriaValue = itemTypeID;
	} else if (itemTypeName) {
		criteriaName = 'name';
		criteriaValue = itemTypeName;
	} else {
		aras.AlertError(aras.getResource('', 'search.neither_input_item_type_name_nor_id_specified'));
		if (isPopup) {
			window.close();
		}
		return false;
	}

	var iomItemType = aras.getItemTypeForClient(criteriaValue, criteriaName);
	if (iomItemType.isError()) {
		if (isPopup) {
			window.close();
		}
		return false;
	}

	currItemType = iomItemType.node;
	itemTypeID = currItemType.getAttribute('id');
	itemTypeName = aras.getItemProperty(currItemType, 'name');
	itemTypeLabel = aras.getItemProperty(currItemType, 'label');
	if (!itemTypeLabel) {
		itemTypeLabel = itemTypeName;
	}

	currQryItem = aras.newQryItem(itemTypeName);

	visiblePropNds = [];

	visiblePropNds = aras.getvisiblePropsForItemType(currItemType);
	aras.uiInitItemsGridSetups(currItemType, visiblePropNds);

	setTimeout(function() {
		showStatus();
	}, 300);
}

function initToolbar() {
	var searchToolbar = null;
	if (window.searchbar) {
		searchToolbar = searchbar.getActiveToolbar();

		var ps = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'page_size', null);
		if (ps === null) {
			ps = aras.getItemProperty(currItemType, 'default_page_size');
		}
		searchToolbar.getItem('page_size').setText(ps);

		var maxRecords = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'max_records', null);
		if (maxRecords === null) {
			maxRecords = aras.getItemProperty(currItemType, 'maxrecords');
		}
		searchToolbar.getItem('max_search').setText(maxRecords);

		if (isMainGrid) {
			initEffectivityControls();
		}

		if (!isMainGrid) {
			if (!multiselect) {
				searchToolbar.getItem('select_all').disable();
			}

			if (aras.isPolymorphic(currItemType)) {
				var cb = searchToolbar.getItem('implementation_type');
				cb.removeAll();
				cb.Add(itemTypeID, itemTypeLabel);
				var morphae = aras.getMorphaeList(currItemType);
				for (var i = 0; i < morphae.length; i++) {
					var m = morphae[i];
					cb.Add(morphae[i].id, morphae[i].label);

				}
				cb = null;
				searchToolbar.showItem('implementation_type');
			} else {
				searchToolbar.hideItem('implementation_type');
			}
		}
	}
}

function updateToolBar() {
	if (window.searchbar) {
		var isAppend = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_append_items') == 'true');
		var activeToolbar = searchbar.getActiveToolbar();
		if (activeToolbar.getItem('prev_page')) {
			activeToolbar.getItem('prev_page').setEnabled(!isAppend && page > 1);
		}
		if (activeToolbar.getItem('next_page')) {
			pagesize = currQryItem.getPageSize();
			maxRecords = currQryItem.getMaxRecords();
			var nodes = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item');
			if (maxRecords && pagesize) {
				var itemsQuantity = pagesize * (page - 1) + nodes.length;
				if (isAppend) {
					itemsQuantity = nodes.length;
				}
			}
			activeToolbar.getItem('next_page').setEnabled(!(pagemax === page || itemsQuantity == maxRecords || nodes.length < pagesize));
		}
	}
}

function doSearch() {
	if (searchContainer) {
		searchContainer.runSearch();
	} else {
		setTimeout(function() {
			doSearch();
		}, 50);
	}
}

function doSelectAll() {
	statusId = aras.showStatusMessage('status', aras.getResource('', 'search.selecting_all'), '../images/Progress.gif');

	grid.selectAll();
	var ids = grid.GetSelectedItemIDs();
	if (ids.length > 0 && onSelectItem) {
		onSelectItem(ids[0]);
	}

	aras.clearStatusMessage(statusId);
}

function onSelectItem() {
}

function setupPageNumber(anyItem) {
	if (!anyItem) {
		anyItem = currQryItem.getResponseDOM().selectSingleNode('//Item');
	}
	page = 1;
	pagemax = -1;
	var itemsWithNoAccesCount = currQryItem.getResponse().getMessageValue('items_with_no_access_count');
	if (itemsWithNoAccesCount) {
		currentSearchMode.setCacheItem('itemsWithNoAccessCount', parseInt(itemsWithNoAccesCount));
	}
	if (anyItem) {
		var pagesize = currQryItem.getPageSize();
		if (pagesize == '-1') {
			pagemax = 1;
			itemmax = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item').length;
		} else {
			pagemax = anyItem.getAttribute('pagemax');
			itemmax = anyItem.getAttribute('itemmax');
		}
		page = anyItem.getAttribute('page');
		var cacheValue = function(value, cacheKey) {
			if (value && !currentSearchMode.getCacheItem(cacheKey)) {
				currentSearchMode.setCacheItem(cacheKey, value);
			} else {
				value = currentSearchMode.getCacheItem(cacheKey);
			}
			return value;
		};
		currentSearchMode.setCacheItem('criteriesHash', ArasModules.utils.hashFromString(currQryItem.getCriteriesString()));
		itemmax = cacheValue(itemmax, 'itemmax');
		pagemax = cacheValue(pagemax, 'pagemax');
		if (!page) {
			page = 1;
		}
		if (!pagemax) {
			pagemax = -1;
		}
	}
}

function updateToolStatusBar() {
	updateToolBar();
	showStatus();
}

function setupGrid(isGridInitXml, doNotRenderRows) {
	var isRelationshipsGrid = (searchLocation === 'Relationships Grid');

	if (isMainGrid) {
		setupMenu();
	}

	var resDom = currQryItem.getResultDOM();
	if (!resDom) {
		return;
	}

	var itTypeId = '';
	if (isRelationshipsGrid) {
		itTypeId = aras.getRelationshipTypeId(window['RelType_Nm']);
		syncWithClient(resDom);
	} else {
		itTypeId = aras.getItemTypeId(itemTypeName);
		currQryItem.syncWithClient();
	}

	aras.uiPrepareDOM4XSLT(resDom, itTypeId, (isRelationshipsGrid ? 'RT_' : 'IT_'));

	var gridXml = '';
	var params = aras.newObject();
	params['only_rows'] = !isGridInitXml;
	if (!isRelationshipsGrid) {
		gridXml = aras.uiGenerateItemsGridXML(resDom, visiblePropNds, itTypeId, params);
	} else {
		params['enable_links'] = !isEditMode;
		params.enableFileLinks = true;
		params.bgInvert = true;
		if (window['RelatedItemType_ID']) {
			params[window['RelatedItemType_ID']] = '';
		}

		var tableNd = resDom.selectSingleNode(aras.XPathResult('/table'));
		tableNd.setAttribute('editable', (isEditMode ? 'true' : 'false'));

		gridXml = aras.uiGenerateRelationshipsGridXML(resDom, DescByVisibleProps, RelatedVisibleProps, window['DescByItemType_ID'], params, true);
	}

	if (isGridInitXml) {
		grid['InitXML_Experimental'](gridXml, doNotRenderRows);

		if (window.previewPane) {
			var userPreviewMode = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'preview_state', 'Properties');
			previewPane.setType(userPreviewMode);
		}
	} else {
		if (grid.getRowCount() === 0) {
			grid.addXMLRows(gridXml);
		} else {
			grid['InitXMLRows_Experimental'](gridXml);
		}
	}

	xmlReadyFlag = true;
}

function createEmptyResultDom() {
	var resDom = aras.createXMLDocument();
	resDom.loadXML(SoapConstants.EnvelopeBodyStart + '<Result/>' + SoapConstants.EnvelopeBodyEnd);
	return resDom;
}

onbeforeunload = function onbeforeunloadHandler() {
	window.isOnBeforeUnload = true;

	if (isMainGrid) {
		aras.showStatusMessage('page_status', '');
		aras.unregisterEventHandler('VariableChanged', window, window.onVariableChanged);
	}

	saveSetups();

	window.isOnBeforeUnload = false;
};

function saveSetups() {
	if (!xmlReadyFlag) {
		return;
	}

	if (searchContainer) {
		searchContainer.onEndSearchContainer();
	}

	var varsHash = {};
	var isRelationshipsGrid = (searchLocation === 'Relationships Grid');
	var currToolBar = (isRelationshipsGrid ? document.toolbar : searchbar);

	if (currToolBar) {
		if (currToolBar.getItem('page_size')) {
			ps = currToolBar.getItem('page_size').getText();
			if (ps) {
				ps = parseInt(ps, 10);
			}
			if (!isNaN(ps) && ps >= 0) {
				varsHash['page_size'] = ps;
			}
		}

		if (currToolBar.getItem('max_search')) {
			var maxSearch = currToolBar.getItem('max_search').getText();
			if (maxSearch) {
				maxSearch = parseInt(maxSearch, 10);
			}
			if (!isNaN(maxSearch && maxSearch >= 0)) {
				varsHash['max_records'] = maxSearch;
			}
		}

		if (isMainGrid && isVersionableIT) {
			var queryType = currToolBar.getItem('query_type').getSelectedItem();
			var queryDate = currToolBar.getItem('query_date_calendar').getText();
			queryDate = aras.convertToNeutral(queryDate, 'date', GetDatePattern(queryType));
			varsHash['query_type'] = queryType;
			if (queryType != 'Current') {
				aras.setVariable(window['varName_queryDate'], queryDate);
			}
		}
	}

	varsHash['col_widths'] = window.grid.GetColWidths();
	varsHash['col_order'] = window.grid.getLogicalColumnOrder();

	var tmpId;
	var tmpTypeNm;
	if (isRelationshipsGrid) {
		tmpId = aras.getRelationshipTypeId(window['RelType_Nm']);
		tmpTypeNm = 'Core_RelGridLayout';
	} else {
		if (window.previewPane) {
			varsHash['preview_state'] = window.previewPane.getType();
		}

		tmpId = aras.getItemTypeId(itemTypeName);
		tmpTypeNm = 'Core_ItemGridLayout';
		if (window.grid._grid && window.grid._grid.view.defaultSettings.freezableColumns) {
			varsHash['frozen_columns'] = String(window.grid._grid.settings.frozenColumns);
		}
	}
	aras.setPreferenceItemProperties(tmpTypeNm, tmpId, varsHash);
}

function onLink(itemTypeName, itemID) {
	aras.uiShowItem(itemTypeName, itemID);
}

function startCellEditCommon(rowId, field) {
	if (inputRowId == rowId) {
		setupFilteredListIfNeed(rowId, field);
	}
}

function applyCellEditCommon(rowId, field) {
	if (inputRowId == rowId && searchReady) {
		currentSearchMode.setPageNumber(1);

		var grid = this;
		var columnIndex = this.GetColumnIndex(field);
		var inputCell = this['grid_Experimental'].inputRowCollections[field] || {get: function() {},
			focus: function() {
				grid._grid.settings.focusedCell = {rowId: 'searchRow', headId: field};
				grid._oldSearchHeadId = field;
			}
		};
		var criteria = this.inputRow.get(field, 'value');
		var useWildcards = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true');
		var condition = (useWildcards ? 'like' : 'eq');

		var propDef = searchContainer.getPropertyDefinitionByColumnIndex(columnIndex);
		var propXpath = searchContainer.getPropertyXPathByColumnIndex(columnIndex);

		if (currentSearchMode.setSearchCriteria(propDef, propXpath, criteria, condition)) {
			inputCell._lastValueReported = inputCell.get('value');
		} else {
			inputCell._lastValueReported = '';
			if (aras.confirm(aras.getResource('', 'search.invalid_criteria'))) {
				inputCell.focus();
			} else {
				currentSearchMode.removeSearchCriteria(propXpath);
				this.inputRow.set(field, 'value', '');
			}
		}
	}

}

function setupFilteredListIfNeed(rowID, field) {
	if (inputRowId !== rowID) {
		return;
	}
	var col = window.grid['columns_Experimental'].get(field, 'index');

	var propNd = visiblePropNds[col - 1];
	if (!propNd || !propNd.xml) {
		return;
	}

	var propName = aras.getItemProperty(propNd, 'name');
	var propDataType = aras.getItemProperty(propNd, 'data_type');
	if (propDataType != 'filter list') {
		return;
	}

	var propPatternName = aras.getItemProperty(propNd, 'pattern');
	if (!propPatternName) {
		return;
	}

	var patternColIndexArr = [];
	for (var i = 0; j < visiblePropNds.length; i++) {
		var curPropName = aras.getItemProperty(visiblePropNds[i], 'name');
		if (curPropName == propPatternName) {
			patternColIndexArr.push(i);
		}
	}

	for (var j = 0; j < patternColIndexArr.length; j++) {
		var filterValue = window.grid.inputRow.get(patternColIndexArr[j] + 1, 'value') || '';
		var resObj = aras.uiGetFilteredObject4Grid(itemTypeID, propName, filterValue);
		if (resObj.hasError) {
			return;
		}

		window.grid.inputRow.set(col, 'comboList', resObj.labels, resObj.values);
	}
}

function removeFilterListValueIfNeed(rowID, field) {
	if (inputRowId !== rowID) {
		return;
	}
	var col = grid['columns_Experimental'].get(field, 'index');

	var propNd = visiblePropNds[col - 1];
	if (!propNd || !propNd.xml) {
		return;
	}

	var propName = aras.getItemProperty(propNd, 'name');
	var propDataType = aras.getItemProperty(propNd, 'data_type');
	if (propDataType != 'filter list') {
		return;
	}

	var filteredColIndexArr = [];
	var j;
	for (j = 0; j < visiblePropNds.length; j++) {
		var curPropPattern = aras.getItemProperty(visiblePropNds[j], 'pattern');
		if (curPropPattern == propName) {
			filteredColIndexArr.push(j);
		}
	}

	for (j = 0; j < filteredColIndexArr.length; j++) {
		grid.inputRow.set(filteredColIndexArr[j] + 1, 'value', '');
	}
}

function getAndUpdatePageSizeAndMaxRecords(event) {
	if (event && !event.target.classList.contains('page-count')) {
		return;
	}
	currentSearchMode.removeCacheItem('itemmax');
	currentSearchMode.removeCacheItem('pagemax');
	currentSearchMode.removeCacheItem('criteriesHash');
	itemmax = currentSearchMode.getCacheItem('itemmax');
	pagemax = currentSearchMode.getCacheItem('pagemax');
	var pagesize = currQryItem.getPageSize();
	var maxRecords = currQryItem.getMaxRecords();
	if (!itemmax || !pagemax) {
		var statusMsg = getCurrentPageStatus();

		showStatus(true);
		var item = aras.newIOMItem(currQryItem.itemTypeName, 'get');
		item.setAttribute('returnMode','countOnly');
		// 07/26/2019 to fix IR-080049 [Safran_SE Upgrade] Nonexistent Work or Source Item leads error
		item.setAttribute('select','id, container, item');
		item.setAttribute('pagesize',pagesize);
		if (maxRecords) {
			item.setAttribute('maxRecords',maxRecords);
		}
		var criteries = currQryItem.dom.selectNodes('/Item/*');
		for (var i = 0; i < criteries.length; i++) {
			item.dom.firstChild.appendChild(criteries[i].cloneNode(true));
		}
		promiseCountResult = Promise.resolve(aras.soapSend('ApplyItem', item.node.xml));
		promiseCountResult.then(parseAnswerGetCount)
			.then(updateToolStatusBar);
	}
}

function parseAnswerGetCount(res) {
	promiseCountResult = null;
	if (res.isFault()) {
		aras.AlertError(res);
		updateToolStatusBar();
		return;
	}
	res = aras.getMessageNode(res.results);
	//end of fix IR-080049
	itemmax = res.selectSingleNode('event[@name="itemmax"]').getAttribute('value');
	pagemax = res.selectSingleNode('event[@name="pagemax"]').getAttribute('value');
	currentSearchMode.setCacheItem('criteriesHash', ArasModules.utils.hashFromString(currQryItem.getCriteriesString()));
	currentSearchMode.setCacheItem('itemmax', itemmax);
	currentSearchMode.setCacheItem('pagemax', pagemax);
	currentSearchMode.setCacheItem('itemsWithNoAccessCount', parseInt(res.selectSingleNode('event[@name="items_with_no_access_count"]').getAttribute('value')));
	return Promise.resolve();
}

function getSearchResultStatusMessage() {
	var statusMsg;

	pagesize = currQryItem.getPageSize();
	maxRecords = currQryItem.getMaxRecords();
	if (isNaN(pagesize) || pagesize == '-1') {
		pagesize = '';
	}
	if (isNaN(maxRecords) || maxRecords == '-1') {
		maxRecords = '';
	}

	var isAppend = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_append_items') == 'true');
	var anyItem;
	if (!isAppend) {
		anyItem = currQryItem.getResult().selectSingleNode('Item');
	}
	setupPageNumber(anyItem);

	var nodes = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item');
	if (pagesize) {
		var i1 = pagesize * (page - 1) + 1;
		var i2 = i1 + nodes.length - 1;
		if (isAppend) {
			itemmax = nodes.length;
			i1 = 1;
			i2 = itemmax;
		}

		statusMsg = getMessage(i1, i2, itemmax, page, pagemax, maxRecords);
	} else if (pagemax) {
		if (itemmax === null) {
			itemmax = nodes.length;
		}

		statusMsg = getMessage('1', nodes.length, itemmax, page, pagemax, maxRecords);
	} else if (pagemax === 0) {
		statusMsg = aras.getResource('', 'search.n_items_found', '0');
	}

	if (statusMsg) {
		var itemsWithNoAccessCount = currentSearchMode && currentSearchMode.getCacheItem('itemsWithNoAccessCount');
		if (itemsWithNoAccessCount > 0) {
			statusMsg += aras.getResource('', 'search.permissions_limited_suffix');
		}
	}

	return statusMsg;
}

function getMessage(offsetItems, currItems, itemmax, page, pagemax, maxRecords) {
	var statusMsg = '';
	if (currItems === 0) {
		statusMsg = aras.getResource('', 'search.n_items_found', '0');
	} else {
		statusMsg = aras.getResource('', 'search.how_much_items_current', offsetItems, currItems, page);
		if (itemmax === undefined && pagemax === -1) {
			statusMsg += '<span class="page-count">';
			statusMsg += aras.getResource('', 'search.how_much_items_max_undefined');
		} else {
			statusMsg += '<span class="page-count">';
			statusMsg += aras.getResource('', 'search.how_much_items_max', itemmax, pagemax);
		}
		statusMsg += '</span>';

		if (currItems === parseInt(maxRecords)) {
			statusMsg += '<span>';
			statusMsg += aras.getResource('', 'search.how_much_items_max_reached');
			statusMsg += '</span>';
		}
	}

	return statusMsg;
}

function getCurrentPageStatus() {
	var page = currQryItem.getPage();
	var pagesize = currQryItem.getPageSize();
	var nodes = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item');
	var i1 = pagesize * (page - 1) + 1;
	var i2 = i1 + nodes.length - 1;
	var statusMsg = aras.getResource('', 'search.how_much_items_current', i1, i2, page);
	return statusMsg;
}

function showStatus(isPending) {
	var statusMsg = getSearchResultStatusMessage() || '';
	var statusbar;

	if ('statusbar' in window && window.statusbar.setStatus) {
		statusbar = window.statusbar;
		if (aras._selectStatusBar('status', statusbar)) {
			statusbar.setStatus('status', '', '');
		}
	} else if ('statusbar' in parent.window && parent.window.statusbar && parent.window.statusbar.setStatus) {
		statusbar = parent.window.statusbar;
	}
	if (!statusbar || !aras._selectStatusBar('page_status', statusbar)) {
		return;
	}
	if (isPending) {
		statusbar.setStatus('page_status', statusMsg, '../images/Progress.gif', 'right');
	} else {
		statusbar.setStatus('page_status', statusMsg);
	}
}
