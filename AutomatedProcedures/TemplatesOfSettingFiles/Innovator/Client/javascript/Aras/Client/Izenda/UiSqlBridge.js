// contains current Izenda version-specific CSS selectors; jQuery, Utils & ReportDesigner dependent
// all selection right tree string constants & operations should be here
// Considering XML/HTML tree data, remember tr/div, attr()/data() etc. replacements
define(['dojo/_base/declare', 'dojo/_base/unload'], function(declare, baseUnload) {
	var self;
	return declare('Aras.Client.Izenda.UiSqlBridge', null, {
		joinsContainerId: 'ctl00_PlaceHolder_Adhocreportdesigner1_ctl01_jtc',
		containerId: 'ctl00_PlaceHolder_Adhocreportdesigner1_ctl01',
		treeXmlRoot: '<table enablehtml="false" icon0="" icon1="" treelines="1" thinborder="true" id="selectionData"></table>',
		selectionData: null, // right tree XMLstring-created jq that points to <table>; can be XmlDocument when removing jq
		rightSelected: null, // jQuery selectionData tr(div) pointer (current node selected)

		// true if cooresponding tab changes NOT applied to Izenda UI
		typesDirty: false, propsDirty: false, allowNullsDirty: false,

		dirty: false, // changes not saved to DB; affects Innovator actions availability

		constructor: function(args) {
			this.args = args;
			if (args.containerId) {
				this.joinsContainerId = args.containerId + '_jtc';
				this.containerId = args.containerId;
			}
			self = this;
		},

		isDirty: function() {
			consoleLog('isDirty', this.dirty, this.typesDirty, this.propsDirty);
			return this.dirty || this.typesDirty || this.propsDirty;
		},

		getReportTitleElement: function() {
			return document.getElementsByName(this.containerId + '_ctl103_miscControls_title');
		},

		getReportTitle: function() {
			return this.getReportTitleElement()[0].value;
		},

		getReportName: function() {
			return document.getElementById('innovatorReportName').value;
		},

		getItemTypeId: function(node, secondary) {
			/// <param name="secondary">itemTypeId2 is missing and unused because of parent usage</param>
			return this.getItemTypeUserData(node, format('itemTypeId{0}', secondary ? '2' : ''));
		},

		getItemTypeUserData: function(node, key) {
			/// <summary>Gets from right tree XML</summary>
			if (typeof node.children != 'function') {
				node = jq$(node);
			}
			return node.children(format('label[data-key="{0}"]', key)).data('value');
		},

		switchDirty: function() {
			var value = this.isDirty();
			var toolbar = window.parent.tearOffMenuController.toolbar;
			toolbar.getItem('save').SetEnabled(value);
			toolbar.getItem('save_unlock_close').SetEnabled(value);
			window.toolbar.enableSaveAsButton(!value);
		},

		setDirty: function() {
			self.dirty = true;
			self.switchDirty();
		},

		cleanDirty: function() {
			self.dirty = false;
			self.switchDirty();
		},

		getJoins: function (joins, parent) {
			/// <summary></summary>
			/// <param name="joins" type="Array">Array of Arrays</param>
			/// <param name="parent" type="HTMLElement"></param>
			/// <returns type="Array">Array of Arrays</returns>
			joins = joins || [];
			var self = this;
			for (var i = 0; i < parent.childNodes.length; ++i) {
				if (parent.childNodes[i].nodeName != 'DIV') {
					continue;
				}

				var node = jq$(parent.childNodes[i]);

				var itemTypeId = self.getItemTypeId(node);
				var tableName = self.getTableNameForIzenda(itemTypeId);

				var rule = self.getItemTypeUserData(node, 'rule'), columnName = self.getItemTypeUserData(node, 'columnName');
				var tableName2 = null, suffix2 = '', columnSuffix2 = '';
				if (rule != JoinRule.Root) {
					var _parent = node.parent(), parentItemTypeId = self.getItemTypeId(_parent);
					suffix2 = // ""
					self.getItemTypeUserData(_parent, 'countSuffix') // apply this to enable aliases
					;
					columnSuffix2 = suffix2 ? ('\'' + self.getTableNameOnly(parentItemTypeId) + suffix2) : '';
					suffix2 = suffix2 ? ('*' + suffix2) : suffix2;
					tableName2 = self.getTableNameForIzenda(parentItemTypeId);
				}

				var data = [];
				var leftColumn, rightColumn;
				var izendaAllowNullsCheckbox = djQuery('input[name="AllowNulls"]')[0];
				var joinType = (izendaAllowNullsCheckbox != undefined && izendaAllowNullsCheckbox.checked) ? 'LEFT_OUTER' : 'INNER';
				switch (rule) {
					case JoinRule.Root:
						break;
					case JoinRule.Relationship:
						leftColumn = format('{0}.[SOURCE_ID]', tableName);
						rightColumn = format('{0}.[ID]{1}', tableName2, columnSuffix2);
						tableName2 = tableName2 + suffix2;
						break;
					case JoinRule.RelationshipTarget:
						leftColumn = format('{0}.[ID]', tableName);
						rightColumn = format('{0}.[RELATED_ID]{1}', tableName2, columnSuffix2);
						tableName2 = tableName2 + suffix2;
						break;
					case JoinRule.ParentRelationship:
						leftColumn = format('{0}.[RELATED_ID]', tableName);
						rightColumn = format('{0}.[ID]', tableName2);
						break;
					case JoinRule.ParentRelationshipTarget:
						leftColumn = format('{0}.[ID]', tableName);
						rightColumn = format('{0}.[SOURCE_ID]', tableName2);
						break;
					case JoinRule.ItemProperty:
						leftColumn = format('{0}.[ID]', tableName);
						rightColumn = format('{0}.[{1}]', tableName2, columnName.toUpperCase());
						break;
					case JoinRule.ParentItemProperty:
						leftColumn = format('{0}.[{1}]', tableName, columnName.toUpperCase());
						rightColumn = format('{0}.[ID]', tableName2);
						break;
					case JoinRule.ListProperty:
						leftColumn = format('{0}.[VALUE]', tableName);
						rightColumn = format('{0}.[{1}]', tableName2, columnName.toUpperCase());
						joinType = 'LEFT_OUTER';
						break;
					default:
						throw 'Not implemented';
				}

				data.push(tableName);

				if (rule != JoinRule.Root) {
					data.push(leftColumn);
					data.push(tableName2);
					data.push(rightColumn);
					data.push(joinType);
				}

				joins.push(data);
				self.getJoins(joins, parent.childNodes[i]);
			} // for
			return joins;
		},

		getTableNameOnly: function(itemTypeId) {
			return this.args.arasObj.getItemTypeForClient(itemTypeId, 'id').getProperty('instance_data');
		},

		getTableNameForIzenda: function(itemTypeId) {
			return format('[innovator].[{0}]', this.getTableNameOnly(itemTypeId));
		},

		getSelectValue: function(trIndex, selectIndex) {
			return jq$(format('#{0} tr:eq({1}) select:eq({2})', this.joinsContainerId, trIndex + 1, selectIndex)).val(); // zero index TR is with labels
		},

		fix2ndTableInJoins: function() {
			if (!reportItemExists) {
				return;
			}
			var self = this;
			jq$(format('#{0} tr', this.joinsContainerId)).each(function(ind, elem) {
				if (ind < 1) {
					return;
				}
				ind--;
				var jq = jq$('select:eq(2)', elem);
				if (jq.val() == '...' && reportSet.JoinedTables[ind]) {
					var jtDef = reportSet.JoinedTables[ind];
					var parts = jtDef.RightConditionTable.split('.');
					var tableName = parts[parts.length - 1].replace('[', '').replace(']', '');
					var re = new RegExp(tableName + '\\d+');
					if (re.test(jtDef.RightAlias)) {
						var innerCallback = function(ind, jtDef) {
							self.setSelectValue(ind, 1, jtDef.LeftConditionColumn);
							self.setSelectValue(ind, 3, jtDef.RightConditionColumn + '\'' + jtDef.RightAlias);
						}.bind(self, i, jtDef);
						self.setSelectValue(ind, 2, jtDef.RightConditionTable + '*' + jtDef.RightAlias.substr(tableName.length), innerCallback); // tmp test
					}
				}
			});
		},

		setSelectValue: function(trIndex, selectIndex, value, callback, index) {
			/// <summary>Modifies Izenda UI</summary>
			/// <param name="trIndex" type="int">0-based SELECTs data row</param>
			try {
				var jq = jq$(format('#{0} tr:eq({1}) select:eq({2})', this.joinsContainerId, trIndex + 1, selectIndex)); // zero index TR is with labels
				jq[0].value = value;// sometimes jq.val(value) fails!
				if (jq.val() != value) {
					for (var i = 0; i < jq[0].length; i++) {
						if (jq[0][i].value.toLowerCase() === value.toLowerCase()) {
							jq[0].value = jq[0][i].value;
							break;
						}
					}
				}

				switch (selectIndex) {
					case 0: JTC_TableChanged(jq[0], callback); break;
					case 1: JTC_LeftColumnChanged(jq[0]);
						break;
					case 2: JTC_RightTableChanged(jq[0]); break;
					case 3: JTC_RightColumnChanged(jq[0]);
						break;
					case 4: 
						break;
					default: throw 'Not implemented';
				}
			}
			catch (e) {
				this.blockUi();
				throw e;
			}
		},

		insertJoinBelow: function(tableIndex) {
			var jq = jq$(format('#{0} tr:eq({1}) img', this.joinsContainerId, tableIndex + 1));
			var jq2 = jq.last();
			consoleLog(format('insertJoinBelow {0} {1} {2}', tableIndex, jq.length, jq2.attr('src')));
			EBC_InsertBelowHandler(jq2[0]); // Izenda onclick direct call on prev. row img. btn
		},

		insertJoinBelowAndWait: function(tableIndex) { // used for 2nd table join insertion only
			var self = this;
			curr_AdHoc_ResponseServer_InnerCallback = function() {
				curr_AdHoc_ResponseServer_InnerCallback = null;
				self.blockUi();
			};
			this.insertJoinBelow(tableIndex);
		},

		validateReport: function() {
			/// <summary>Returns null either error message string</summary>
			if (this.typesDirty) {
				return Izenda.Utils.getI18NText('reportdesigner.validation.apply_types_to_izenda');
			}
			/*if (this.propsDirty) { // unnecessary now, props are autoapplied
				return Izenda.Utils.getI18NText("reportdesigner.validation.apply_props_to_izenda");
			}*/

			if (this.getReportName().trim().length === 0) {
				return Izenda.Utils.getI18NText('reportdesigner.validation.empty_name');
			}

			var selects = document.querySelectorAll('#' + this.containerId + '_sc_Column');
			var rtn = false;
			for (var i = 0; i < selects.length; ++i) {
				if (selects[i].value != '...') {
					rtn = true;
					break;
				}
			}
			return !rtn ? Izenda.Utils.getI18NText('reportdesigner.validation.no_properties_selected') : null;
		},

		setFirstTableSelect: function(tableIndex, tableName) {
			this.blockUi(true);
			var xhrCount = 0;
			var self = this;
			curr_AdHoc_ResponseServer_InnerCallback = function() {
				xhrCount++;
				consoleLog(format('setFirstTableSelect {0}', xhrCount));
				if (self.args.arasObj.Browser.isIe() && xhrCount == 13 || ((self.args.arasObj.Browser.isFf() || self.args.arasObj.Browser.isCh()) && xhrCount == 12)) {
					self.insertJoinBelowAndWait(tableIndex);
				}
			};
			this.setSelectValue(tableIndex, 0, tableName);
		},

		blockUi: function(block) {
			if (!_inDebug) {
				document.getElementById('loadingOverlay').style.display = block ? 'block' : 'none';
			}
		},

		applyJoins: function(addRows, callback) {
			/// <parameter name="addRows">Now unused</parameter>
			this.blockUi(true);

			// remove prev. joins; if down->up no XHRs; remember th tr
			var joinRows = jq$(format('#{0} tr', this.joinsContainerId));
			consoleLog('joinRows cnt: ' + joinRows.length);
			for (var i = joinRows.length - 1; i > 2; --i) { // 2 because for base item type 2nd empty row is added below
				var removeRowImgBtn = jq$(format('#{0} tr:eq({1}) img', this.joinsContainerId, i));
				JTC_RemoveHandler(removeRowImgBtn[0]); // no XHRs here
			}

			// reset always existing after adding base item type 2nd row to empty values
			this.setSelectValue(1, 0, '...');

			function finishing(dontResetCallBack) {
				if (!dontResetCallBack) {
					curr_AdHoc_ResponseServer_InnerCallback = null;
				}
				this.typesDirty = false;
				this.allowNullsDirty = false;
				this.blockUi();
				if (callback && typeof callback == 'function') {
					callback();
				}
			}

			var joins = this.getJoins(null, this.selectionData[0]);
			if (joins.length == 1) {
				finishing.call(this, true);
				return; // only 1 table is selected
			}

			var tableIndex = 1, xhrCount = 0, self = this;
			var joinSelectIndices = [0, 2, 1, 3, 4];

			globalJoinsLengthCounter = joins.length;

			for (i = 1; i < joins.length; i++) {
				var innerCallback = function(i) {
					for (var j = 1; j < joinSelectIndices.length; j++) {
						var ind = joinSelectIndices[j];
						self.setSelectValue(i, ind, joins[i][ind]);
					}
				}.bind(this, i);
				this.setSelectValue(i, 0, joins[i][0], innerCallback);
				if (i !== joins.length - 1) {
					self.insertJoinBelow(i);
				}

			}

			finishing.call(self);
		},

		htmlToTree: function() {
			/// <summary>Converts HTML/jQuery compatible markup to Aras Tree markup</summary>
			return this.selectionData[0].outerHTML // todo: why not this.selectionData.html()?
				.replaceAll('<div', '<tr')
				.replaceAll('</div>', '</tr>')
				.replace(/<span\>(.*?)<\/span\>/mig, '<td>$1</td>')
              /*.replaceAll("<span>", "<td>")
              .replaceAll("</span>", "</td>")*/
				.replaceAll('<label', '<userdata')
				.replaceAll('</label>', '</userdata>')
				.replaceAll('data-key', 'key')
				.replaceAll('data-value', 'value');
		},

		restoreSelection: function(itemTypeId, xml) {
			/// <summary>Restore checkbox selection in XML for left tree from rightSelected children</summary>
			/// <param name="xml">Aras Tree table-tr-td ItemType XML for base ItemType left tree</param>
			/// <returns>XML updated with checked icons</returns>
			var doc = this.args.arasObj.createXMLDocument();
			doc.loadXML(xml);
			var typeSafeName = this.args.arasObj.getItemTypeForClient(itemTypeId, 'id').getProperty('instance_data');
			var checkedIconUrl = '/images/checkbox-checked.svg';
			var self = this;
			this.rightSelected.children().each(function(ind, _child) {
				var child = jq$(_child);
				if (child[0].nodeName.toLowerCase() != 'div') {
					return;
				}
				var rule = self.getItemTypeUserData(child, 'rule'), itemTypeId = self.getItemTypeUserData(child, 'itemTypeId');
				var node, childItemTypeName = self.args.arasObj.getItemTypeForClient(itemTypeId, 'id').getProperty('instance_data');
				var itemPropertyXPath = '//tr[@id="{0}/ItemTypes/{1}"]';
				var columnName = self.getItemTypeUserData(child, 'columnName');
				switch (rule) {
					case JoinRule.Relationship:
					case JoinRule.ParentRelationship:
						node = doc.selectSingleNode(format('//tr[@id="{0}/Relationships/{1}"]', typeSafeName, childItemTypeName));
						break;
					case JoinRule.ItemProperty:
					case JoinRule.ListProperty:
						node = doc.selectSingleNode(format(itemPropertyXPath, typeSafeName, columnName));
						break;
					case JoinRule.ParentItemProperty:
						node = doc.selectSingleNode(format(itemPropertyXPath, childItemTypeName, columnName));
						break;
				}

				if (node) {
					node.selectSingleNode('userdata[@key="guid"]').setAttribute('value', _child.id);
					node.setAttribute('icon0', checkedIconUrl);
					node.setAttribute('icon1', checkedIconUrl);
				}
			});
			return doc.xml;
		},

		getBaseItemTypeId: function() {
			return this.getItemTypeId(this.getRightRoot());
		},

		getRightRoot: function() {
			return jq$('div', this.selectionData);
		},

		getRightItemId: function(node) {
			///<param name="node">jQuery element</param>
			return node.attr('id');
		},

		setRoot: function() {
			this.rightSelected = this.getRightRoot();
		},

		getItemTypesCountSuffixSelector: function(itemTypeId) {
			///<summary>// all labels for tree nodes of given item type id</summary>
			return jq$(format('label[data-key="itemTypeId"][data-value="{0}"]', itemTypeId), this.selectionData);
		},

		getItemTypesCountSuffix: function(itemTypeId) {
			var cnt = this.getItemTypesCountSuffixSelector(itemTypeId).length;
			return cnt > 0 ? (cnt + 1) : '';
		},

		rewriteItemTypesCountSuffix: function(itemTypeId) {
			var jq = this.getItemTypesCountSuffixSelector(itemTypeId);
			jq.each(function(ind, _elem) {
				var label = jq$(_elem);
				var treeNode = label.parent();

				var span = treeNode.children('span').first();
				var name = treeNode.children('label[data-key=\'caption\']').first().data('value');
				var re = new RegExp('\\b' + name + '\\d*\\b', 'i');
				var suffix = ind > 0 ? (ind + 1) : '';
				span.html(span.html().replace(re, name + suffix));

				// don't use jq$.data() for changing values created via template - at least IE11 problem for curr. Izenda jQuery 1.7.x
				var countSuffix = treeNode.children('label[data-key=\'countSuffix\']').first();
				countSuffix.attr('data-value', suffix);
			});
		},

		getAllUsedItemTypeIds: function() {
			var ids = [];
			jq$('label[data-key=\'itemTypeId\']', this.selectionData).each(function(index, dataTag) {
				ids.push(jq$(dataTag).data('value'));
			});
			return ids;
		},

		moveRightSelectedToParent: function() {
			var parent = this.rightSelected.parent();
			this.rightSelected = parent && parent.attr('id') != 'selectionData' ? parent : this.rightSelected;
		},

		getSelectedItemTypeProps: function() {
			/// <returns>All right tree nodes that were selected as props of data type = &quot;item&quot;</returns>
			return jq$('label[data-key="rule"][data-value="1"], label[data-key="rule"][data-value="4"]', this.selectionData);
		}
	});
});
