﻿///ItemInToc
function ItemInToc(rowId) {
	/// <summary>
	/// Base class for Items in Grid
	/// </summary>
	/// <param name="rowId">Id of item in TOC.</param>
	this.rowId = rowId;
}

ItemInToc.prototype.onClick = function ItemInToc_onClick() {
	return Promise.resolve();
};

ItemInToc.prototype.containsProperties = function ItemInToc_containsProperties() {
	return true;
};

ItemInToc.prototype.Select = function ItemInToc_Select() {
	/// <summary>
	/// Select function should contains a logic for select action
	/// if user calls onClick function manually
	/// </summary>
};

ItemInToc.prototype._getMostTopWindowWithAras = function ItemInToc_getMostTopWindowWithAras() {
	return parent;
};

ItemInToc.prototype.loadToolbarForItemType = function ItemInToc_loadToolbarForItemType(itemTypeName, itemType) {
	var subPrefix = '';

	if (itemType) {
		// It's necessary to set subPrefix for Inbasket. In other cases subPrefix should be empty.
		var itemTypeId = itemType.getID();
		switch (itemTypeId) {
			case '85924010F3184E77B24E9142FDBB481B':
				subPrefix = /com.aras.innovator.cui_default.mwt_inbasket_toolbar_/g;
				break;
			case 'BC7977377FFF40D59FF14205914E9C71':
				subPrefix = /com.aras.innovator.cui_default.mwt_inbasketTask_toolbar_/g;
				break;
			default:
				break;
		}
	}

	var cuiContext = {toolbarId: itemTypeName, itemType: itemType};
	var topWindow = this._getMostTopWindowWithAras();
	return topWindow.cui.getExistToolbarIdAsync('MainWindowToolbar', cuiContext).then(function(toolbarId) {
		var mainMenu = topWindow.menu;
		if (!mainMenu.activeToolbar.isToolbarExist(toolbarId)) {
			cuiContext.toolbarApplet = mainMenu.activeToolbar;
			cuiContext.locationName = 'MainWindowToolbar';
			cuiContext.item_classification = '%all_grouped_by_classification%';
			cuiContext.mainSubPrefix = /com.aras.innovator.cui_default.mwt_main_toolbar_/g;
			cuiContext.subPrefix = subPrefix;
			cuiContext.toolbarId = toolbarId;

			return topWindow.cui.loadToolbarFromCommandBarsAsync(cuiContext).then(function() {
				return mainMenu.setActiveToolbar(toolbarId);
			});
		} else {
			return Promise.resolve(mainMenu.setActiveToolbar(toolbarId));
		}
	});
};

ItemInToc.prototype.loadMenuForItemType = function ItemInToc_loadMenuForItemType(itemTypeName, itemType) {
	var cuiContext = {menuId: itemTypeName, itemType: itemType};
	var topWindow = this._getMostTopWindowWithAras();
	return topWindow.cui.getExistMenuBarIdAsync('MainWindowMainMenu', cuiContext).then(function(menuBarId) {
		function showAndInitMenu(menu, menuBarId, isMenuInitialized) {
			menu.menuApplet.showMenuBar(menuBarId);
			if (!isMenuInitialized) {
				topWindow.cui.callInitHandlersForMenu({}, 'SelectInToc');
			}
		}

		var isMenuInitialized = topWindow.menu.menuApplet.activeMenuBarId == menuBarId;
		if (!topWindow.menu.menuApplet.isMenuExist(menuBarId)) {
			return topWindow.cui.loadMenuFromCommandBarsAsync('MainWindowMainMenu', cuiContext).then(function(menuXml) {
				topWindow.menu.menuApplet.setXML(menuXml, 'xml');
				showAndInitMenu(topWindow.menu, menuBarId, isMenuInitialized);
			});
		} else {
			showAndInitMenu(topWindow.menu, menuBarId, isMenuInitialized);
			return Promise.resolve();
		}
	});
};
///ItemInToc

///TocCategory
function TocCategory(rowId) {
	this.rowId = rowId;
}

TocCategory.prototype = new ItemInToc();

TocCategory.prototype.onClick = function TocCategory_onClick() {
	const formId = mainTreeApplet.data.get(this.rowId).form_id;

	disableMenu();
	this.showTOCCategory(formId);

	return Promise.resolve();
};

TocCategory.prototype.containsProperties = function TocCategory_containsProperties() {
	return false;
};

TocCategory.prototype.onContextMenuCreate = function TocCategory_onContextMenuCreate(menuItems) {
	return Promise.resolve(menuItems.map(function(item) {
		item.visible = false;
		return item;
	}));
};

TocCategory.prototype.showTOCCategory = function TocCategory_showTOCCategory(formId) {
	const topWnd = this._getMostTopWindowWithAras();
	if (topWnd.work) {
		topWnd.itemType = undefined;
		this.loadMenuForItemType('main_menubar');
		const toolbarLoadedPromise = this.loadToolbarForItemType('main_toolbar');
		const shortcutSettings = {
			windows: [topWnd],
			context: topWnd
		};
		topWnd.registerShortcutsAtMainWindowLocation(shortcutSettings);

		const arasObj = topWnd.aras;

		if (!formId) {
			topWnd.work.location.replace(arasObj.getScriptsURL() + 'blank.html');
			return;
		}

		const form = arasObj.getFormForDisplay(formId, 'by-id', false);
		if (form.isError() || !form.node) {
			topWnd.work.location.replace(arasObj.getScriptsURL() + 'blank.html');
			return;
		}

		arasObj.uiShowItemInFrameEx(topWnd.work, undefined, 'edit', 0, form.node);
		if (topWnd.cui) {
			return toolbarLoadedPromise.then(function() {
				topWnd.cui.callInitHandlers('SelectInToc');
			});
		}
	}
};
///TocCategory

///ItemTypeInToc
function ItemTypeInToc(rowId) {
	this.itemTypeName = rowId ? rowId.slice(rowId.lastIndexOf('/') + 1) : rowId;
	this.location = 'Main Grid';
	this._locationsWithout = {
		'contextMenu': ['MYDISCUSSIONS.HTML'],
		'refreshOnContextMenuCall': ['MYDISCUSSIONS.HTML', '../MODULES/ARAS.INNOVATOR.IZENDA/MYREPORTS']
	};
	this._currentLocation = null;
	this._isContextMenuCreationPending = false;
}

ItemTypeInToc.prototype = new ItemInToc();

ItemTypeInToc.prototype.onClick = function ItemTypeInToc_onClick() {
	return this.showItemTypeViewerInWorkSpace('');
};

ItemTypeInToc.prototype.containsProperties = function ItemTypeInToc_containsProperties() {
	if (this.itemTypeName) {
		var itemType = this._getMostTopWindowWithAras().aras.getItemTypeForClient(this.itemTypeName);
		if (itemType) {
			var TOCView = this._getMostTopWindowWithAras().aras.uiGetTOCView4ItemTypeEx(itemType.node);
			return TOCView ? false : true;
		}
	}
	return false;
};

ItemTypeInToc.prototype._isActionAllowed = function ItemTypeInToc__isActionAllowed(actionName) {
	var notAllowedLocations = this._locationsWithout[actionName],
		notAllowedLocationsCount = notAllowedLocations.length,
		currLocation, i;

	if (this._currentLocation) {
		currLocation = this._currentLocation.toUpperCase();
		for (i = 0; i < notAllowedLocationsCount; i++) {
			if (-1 !== currLocation.indexOf(notAllowedLocations[i])) {
				return false;
			}
		}
	}

	return true;
};

ItemTypeInToc.prototype.onContextMenuCreate = function ItemTypeInToc_onContextMenuCreate(menuItems) {
	var rowID = this.itemTypeName;
	const ssrItemTypeId = 'A46890D3535C41D4A5D79240B8C373B0';

	this._isContextMenuCreationPending = true;
	this.onClick();
	this._isContextMenuCreationPending = false;

	var visibleMenuItems = {
		'com.aras.innovator.cui_default.pmtoc_createNewReport': (rowID === 'MyReports' && aras.getPermissions('can_add', ssrItemTypeId)),
		'com.aras.innovator.cui_default.pmtoc_ListAll': false,
		'com.aras.innovator.cui_default.pmtoc_runSavedSearch': false,
		'com.aras.innovator.cui_default.pmtoc_changeSavedSearchShowingOnTOC': false,
		'com.aras.innovator.cui_default.pmtoc_delSavedSearch': false,
		'com.aras.innovator.cui_default.pmtoc_addSavedSearchToForum': false,
		'com.aras.innovator.cui_default.pmtoc_createNewInstance': false
	};
	menuItems.forEach(function(menuItem) {
		menuItem.visible = visibleMenuItems[menuItem.name];
	});

	var topWnd = this._getMostTopWindowWithAras();
	return new Promise(function(resolve, reject) {
		var showMenu = function() {
			if (topWnd.work.isItemsGrid || topWnd.work.itemTypeName === 'MyReports') {
				var label = topWnd.aras.getItemProperty(topWnd.work.currItemType, 'label') || rowID;
				var New_PPMCmdLabel = topWnd.aras.getResource('', 'maintree.new', label);
				menuItems.forEach(function(menuItem) {
					if (menuItem.name === 'com.aras.innovator.cui_default.pmtoc_ListAll') {
						menuItem.visible = (rowID !== 'MyReports');
					}
				});
				if (topWnd.work.itemTypeName === rowID && topWnd.work.can_addFlg) {
					menuItems.forEach(function(menuItem) {
						if (menuItem.name === 'com.aras.innovator.cui_default.pmtoc_createNewInstance') {
							menuItem.visible = true;
						}
						if (menuItem.name === 'com.aras.innovator.cui_default.pmtoc_createNewInstance') {
							menuItem.label = New_PPMCmdLabel;
						}
					});
				}
				resolve(menuItems);
			} else {
				setTimeout(showMenu, 100);
			}
		};
		showMenu();
	});
};

ItemTypeInToc.prototype.ListAll = function ItemTypeInToc_ListAll() {
	var topWnd = this._getMostTopWindowWithAras();
	if (topWnd.work.isItemsGrid && topWnd.work.itemTypeName == this.itemTypeName && topWnd.work.doSearch) {
		topWnd.work.doSearch();
	} else {
		this.onClick();
	}
};

ItemTypeInToc.prototype.createNewInstance = function ItemTypeInToc_createNewInstance() {
	var self = this;
	var itemTypeName = self.itemTypeName;
	if(itemTypeName === "File"){
		aras.vault.selectFile().then(function(item) {
				var fileNode = aras.newItem('File', item);
				aras.uiShowItemEx(fileNode, "new");
				aras.itemsCache.addItem(fileNode);
			});
	} else {
		setTimeout(function() {
		var itemType = aras.getItemTypeForClient(itemTypeName).node;

		if (aras.isPolymorphic(itemType) && !window.showModalDialog) {
			aras.newItem(itemTypeName).then(function (node) {
				if (node) {
					aras.itemsCache.addItem(node);
					aras.uiShowItemEx(node, "new");
				}
			});
		} else {
			self._getMostTopWindowWithAras().aras.uiNewItemEx(itemTypeName);
		}
		}, 10);
	}
};

ItemTypeInToc.prototype.createNewReport = function ItemTypeInToc_createNewReport() {
	var self = this;
	setTimeout(function() {
		self._getMostTopWindowWithAras().aras.uiNewItemEx('SelfServiceReport');
	}, 10);
};

ItemTypeInToc.prototype.showItemTypeViewerInWorkSpace = function ItemTypeInToc_showItemTypeViewerInWorkSpace(savedSearchId) {
	var itemTypeName = this.itemTypeName,
		topWnd = this._getMostTopWindowWithAras(),
		itemType = topWnd.aras.getItemTypeForClient(itemTypeName),
		itemTypeID = itemType.getId(),
		workFrame = topWnd.work;

	if (itemType.getErrorCode() !== 0 && !itemTypeID) {
		return;
	}

	topWnd.itemType = itemType;
	var menuLoadedPromise = this.loadMenuForItemType(itemTypeName, itemType);
	var toolbarLoadedPromise = this.loadToolbarForItemType(itemTypeName, itemType);
	var loadControlsPromise = null;
	var initHandlersPromise = null;

	disableMenu();
	showProgressStatus();

	if (itemType) {
		var TOCView = topWnd.aras.uiGetTOCView4ItemTypeEx(itemType.node),
			url = '',
			parameters = '',
			parametersResult = '';

		if (TOCView) {
			var form = topWnd.aras.getItemProperty(TOCView, 'form');

			if (form) {
				url = 'ShowFormInFrame.html';
				parameters = '\'formId=' + form + '&formType=edit&item=undefined\'';
			} else {
				url = topWnd.aras.getItemProperty(TOCView, 'start_page');
				parameters = topWnd.aras.getItemProperty(TOCView, 'parameters');
			}
		}

		if (statusAnimId != null) {
			topWnd.aras.clearStatusMessage(statusAnimId);
		}

		if (url === '') {
			url = topWnd.aras.getScriptsURL() + 'itemsGrid.html';
		}

		this._currentLocation = url;

		if (parameters === '') {
			parameters = '\'itemtypeID=\'+itemTypeID+\'&itemtypeName=\'+itemTypeName+\'&savedSearchId=\'+savedSearchId';
		}

		try {
			parametersResult = eval(parameters);
		}
		catch (excep) {
			topWnd.aras.AlertError(topWnd.aras.getResource('', 'maintree.params_arent_valid', parameters));
			return Promise.reject();
		}

		var shortcutSettings = {
			windows: [topWnd],
			context: topWnd
		};

		if (url.toUpperCase().indexOf('ITEMSGRID.HTML') > -1 &&
			workFrame.isItemsGrid &&
			workFrame.searchbar !== undefined &&
			workFrame.grid !== undefined &&
			workFrame.document.readyState == 'complete') {
			loadControlsPromise = workFrame.loadControls.then(function() {
				return menuLoadedPromise;
			}).then(function() {
				workFrame.onReinitialize(itemTypeName, itemTypeID, savedSearchId);
			});

			shortcutSettings.windows.push(topWnd.work);
		} else if (!this._isContextMenuCreationPending || this._isActionAllowed('refreshOnContextMenuCall')) {
			const parser = document.createElement('a');
			// 07/25/2019 to fix IR-080052 [Safran_SE Upgrade]Dashboard doesn't load when change it to anot
			const currentHref = workFrame.location.href;
			const targetUri = url + '?' + parametersResult;

			// HTMLAnchorElement contains native parser of URI's
			// https://gist.github.com/jlong/2428561
			parser.href = targetUri;

			// if item Node is not selected in TOC
			if (parser.href !== currentHref) {
				topWnd.aras.browserHelper.toggleSpinner(topWnd.document, true);
				workFrame.location.replace(targetUri);
				// end of fix IR-080052
			}
		}
		topWnd.registerShortcutsAtMainWindowLocation(shortcutSettings, itemTypeName, itemType);
	} else {
		if (statusAnimId != null) {
			topWnd.aras.clearStatusMessage(statusAnimId);
		}
		return Promise.all([menuLoadedPromise, toolbarLoadedPromise]);
	}

	if (topWnd.cui) {
		initHandlersPromise = toolbarLoadedPromise.then(function(toolbar) {
			topWnd.cui.callInitHandlers('SelectInToc');
		});
	}

	return Promise.all([
		loadControlsPromise ? loadControlsPromise : menuLoadedPromise,
		initHandlersPromise ? initHandlersPromise : toolbarLoadedPromise
	]);
};
///ItemTypeInToc

///SavedSearchInToc
function SavedSearchInToc(itemKey) {
	function getControlItemKey(lastPortionOfId) {
		var targetKey;
		mainTreeApplet.data.forEach(function(value, key) {
			if (key.slice(key.lastIndexOf('/') + 1) === lastPortionOfId){
				targetKey = key;
			}
		});
		return targetKey;
	}

	var targetItem = mainTreeApplet.data.get(itemKey);
	if (targetItem) {
		this.gridRowIdForItemTypeName = targetItem.name;
		this.itemTypeName = targetItem.name.substr(targetItem.name.lastIndexOf('/') + 1);
		this.rowId = targetItem.saved_search_id;
		this.showOnTOC = true;
		return;
	}

	this.rowId = itemKey;
	var res = this.getSavedSearchFromDataBase();
	if (res) {
		this.showOnTOC = res.getProperty('show_on_toc') === '1';
		this.itemTypeName = res.getProperty('itname');
		var targetKey = getControlItemKey(this.itemTypeName + (this.showOnTOC ? this.rowId : ''));
		targetItem = mainTreeApplet.data.get(targetKey);
		this.savedSearchId = itemKey;
		this.gridRowIdForItemTypeName = targetItem.name;
	}
}

SavedSearchInToc.prototype = new ItemTypeInToc();

SavedSearchInToc.prototype.getSavedSearchFromDataBase = function SavedSearch_getSavedSearchFromDataBase() {
	var requestItem = this._getMostTopWindowWithAras().aras.newIOMItem('SavedSearch', 'get');
	requestItem.setID(this.rowId);

	var resultItem = requestItem.apply();
	if (resultItem.isError()) {
		this._getMostTopWindowWithAras().aras.AlertError(resultItem);
		return false;
	}

	return resultItem;
};

SavedSearchInToc.prototype.Select = function SavedSearch_Select() {
	if (this.showOnTOC) {
		selectRow(this.rowId);
	} else {
		if (mainTreeApplet.data.has(this.gridRowIdForItemTypeName) && this.savedSearchId) {
			selectRow(this.gridRowIdForItemTypeName, this.rowId);
		}
	}
};

SavedSearchInToc.prototype.onClick = function SavedSearchInToc_onClick() {
	return this.showItemTypeViewerInWorkSpace(this.rowId);
};

// new CUI ID handlers BEGIN; old handlers left for backward compatibility
SavedSearchInToc.prototype.changeSavedSearchShowingOnTOC = function SavedSearchInToc_changeSavedSearchShowingOnTOC() {
	// don't be confused: in spite of its name this method ALWAYS hide saved search
	var hideCode = '0';
	return this.changeShowOnTOC(hideCode);
};
SavedSearchInToc.prototype.addSavedSearchToForum = function SavedSearchInToc_addSavedSearchToForum() {
	return this.addToForum();
};
SavedSearchInToc.prototype.runSavedSearch = function SavedSearchInToc_runSavedSearch() {
	return this.run();
};
SavedSearchInToc.prototype.delSavedSearch = function SavedSearchInToc_delSavedSearch() {
	return this.del();
};
// new CUI ID handlers END

SavedSearchInToc.prototype.run = function SavedSearchInToc_run() {
	this.onClick();
};

SavedSearchInToc.prototype.del = function SavedSearchInToc_del() {
	var topWnd = this._getMostTopWindowWithAras();
	if (topWnd.aras.confirm(topWnd.aras.getResource('', 'search_center.confirm_delete'))) {
		var currSavedSearch = topWnd.aras.newIOMItem('SavedSearch', 'delete');
		currSavedSearch.setID(this.rowId);

		var res = currSavedSearch.apply();
		if (res.isError()) {
			return false;
		}

		updateTree();
		return true;
	}

	return false;
};

SavedSearchInToc.prototype.changeShowOnTOC = function SavedSearchInToc_changeShowOnTOC(show) {
	var currSavedSearch = this._getMostTopWindowWithAras().aras.newIOMItem('SavedSearch', 'edit');

	currSavedSearch.setID(this.rowId);
	currSavedSearch.setProperty('location', this.location);
	currSavedSearch.setProperty('show_on_toc', show || '0');

	var res = currSavedSearch.apply();
	if (res.isError()) {
		this._getMostTopWindowWithAras().aras.AlertError(res);
		return false;
	}

	updateTree();
	return true;
};

SavedSearchInToc.prototype.addToForum = function SavedSearchInToc_addToForum() {
	var searchId = this.rowId;
	var topWnd = this._getMostTopWindowWithAras();

	topWnd.ArasModules.Dialog.show('iframe', {
		aras: topWnd.aras,
		title: topWnd.aras.getResource('', 'ssvc.forum.add_search_to_forum'),
		includeMyBookmarks: false,
		dialogWidth: 300,
		dialogHeight: 100,
		resizable: false,
		content: '../Modules/aras.innovator.SSVC/Views/SelectForum.html'
	}).promise.then(function(forumId) {
		var result = addForumSearch(forumId, searchId);
		if (result.isError()) {
			topWnd.aras.AlertError(result.getErrorString());
		}
	});

	function addForumSearch(forumId, savedSearchId) {
		var search = topWnd.aras.newIOMItem('ForumSearch', 'add');
		search.setProperty('source_id', forumId);
		search.setProperty('related_id', searchId);
		return search.apply();
	}
};

SavedSearchInToc.prototype.onContextMenuCreate = function SavedSearchInToc_onContextMenuCreate(menuItems) {
	var rowID = this.rowId;
	var topWnd = this._getMostTopWindowWithAras();
	var menu = mainTreeApplet.contextMenu;
	var itName = this.itemTypeName;

	var showAddForum = isSSVCEnabledForIT(itName) && isSavedSearchCreatedByAdmin(rowID);
	var visibleMenuItems = {
		'com.aras.innovator.cui_default.pmtoc_createNewInstance': false,
		'com.aras.innovator.cui_default.pmtoc_ListAll': false,
		'com.aras.innovator.cui_default.pmtoc_runSavedSearch': true,
		'com.aras.innovator.cui_default.pmtoc_createNewReport': false,
		'com.aras.innovator.cui_default.pmtoc_addSavedSearchToForum': showAddForum,
		'com.aras.innovator.cui_default.pmtoc_changeSavedSearchShowingOnTOC': topWnd.aras.getPermissions('can_update', rowID, '', 'SavedSearch'),
		'com.aras.innovator.cui_default.pmtoc_delSavedSearch': topWnd.aras.getPermissions('can_delete', rowID, '', 'SavedSearch')
	};

	menuItems.forEach(function(menuItem) {
		menuItem.visible = visibleMenuItems[menuItem.name];
	});

	return Promise.resolve(menuItems);

	function isSSVCEnabledForIT(/*string*/itName) {
		if (!topWnd.aras.commonProperties.IsSSVCLicenseOk) {
			return false;
		}
		var item = topWnd.aras.newIOMItem('DiscussionTemplate', 'get');
		var source = topWnd.aras.newIOMItem('ItemType', 'get');
		source.setProperty('name', itName);
		item.setPropertyItem('source_id', source);
		var result = item.apply();
		if (result.isError() || result.getItemCount() < 1) {
			return false;
		}
		return true;
	}

	function isSavedSearchCreatedByAdmin(id) {
		var ss = getSavedSearchItem(id);
		var createdBy = ss.getProperty('created_by_id');
		return isUserAdmin(createdBy);

		function isUserAdmin(id) {
			var result = topWnd.aras.newIOMItem('User', 'VC_IsUserAdmin');
			result.setID(id);
			result = result.apply();
			var isAdmin = result.getResult();
			if (isAdmin.toUpperCase() === 'TRUE') {
				return true;
			} else {
				return false;
			}
		}

		function getSavedSearchItem(id) {
			var item = topWnd.aras.newIOMItem('SavedSearch', 'get');
			item.setID(id);
			return item.apply();
		}
	}
};
///SavedSearchInToc
