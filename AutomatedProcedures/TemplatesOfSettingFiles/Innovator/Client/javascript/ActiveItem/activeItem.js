function isActiveItemInstalled(){
	return true;
}

function openActiveItem(activeItemID){
	activeItemID = top.aras.applyMethod("AI_GetActiveItemID");
   	top.aras.uiShowItem("ZS_Project", activeItemID.htmlDecode());
}

function hideActiveItemDiv(){
	var div = top.document.getElementById("activeItem");
	if(div != null){
		div.style = "visibility: hidden; width: 0;";
	}
}

function showActiveItemDiv(){
	var div = top.document.getElementById("activeItem");
	if(div != null){
		div.style = "";
		top.document.getElementById("activeItem").setAttribute("style","");
	}
}

function setActiveItem(){
	showActiveItemDiv();

	top.document.getElementById('activeItemName').innerHTML = "CURRENT PROJECT <a href='#'' onclick='clearActiveItem();'>[X]</a>";
	top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='setActiveItem();'>None</a>";

	var activeItemID = top.aras.applyMethod("AI_GetActiveItemID");
	if(!activeItemID || activeItemID.htmlDecode() == "NONE"){
		return;
	}
	activeItemID = activeItemID.htmlDecode();

	var activeItemKeyedName = top.aras.applyMethod("AI_GetActiveItemKeyedName");
	if(!activeItemKeyedName || activeItemKeyedName.htmlDecode() == "NONE"){
		return;
	}
	activeItemKeyedName = activeItemKeyedName.htmlDecode();
	top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='openActiveItem(\""+activeItemID+"\");'>"+activeItemKeyedName+"</a>";
	return activeItemID;
}

function clearActiveItem(){
	top.aras.applyMethod("AI_ClearActiveItem");
	top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='setActiveItem();'>None</a>";
}

function setActiveItemById(activeItemID, itemKeyedName){
	top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='openActiveItem(\""+activeItemID+"\");'>"+itemKeyedName+"</a>";
}

function removeActiveItem(){
	top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='setActiveItem();'>None</a>";
}

function setActiveItemConfig(activeItemName){
	if(activeItemName == null){
		hideActiveItemDiv();
	}else{
		showActiveItemDiv();
		top.document.getElementById('activeItemName').innerHTML = "CURRENT PROJECT <a href='#'' onclick='clearActiveItem();'>[X]</a>";
		top.document.getElementById('itemID').innerHTML = "<a href='#' onclick='setActiveItem();'>None</a>";
	}
}