﻿<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Aras.Client.Core" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="Aras.Web.Configuration" %>
<script language="VB" runat="Server">
	'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	'
	' (c) Copyright by Aras Corporation, 2004-2007.
	'
	' $FileName: utils.aspx
	' $Purpose:  utilities to support client ASPX
	' $Routines:
	'------------------------------------------------------------------------------

	' In order to make it easier to tweek the client configuration
	' we will not use Application state, and instead read our configuration
	' xml file every time.

	Dim ApplicationXML As XmlDocument
	Dim ClientConfigHook As Object

	Private Sub init_application()
		Dim clientConfig As ClientConfig = ClientConfig.GetServerConfig()
		ApplicationXML = clientConfig.GetXmlConfig()
		
		Dim nodes As XmlNodeList = ApplicationXML.SelectNodes("/Innovator/DB-Connection[@database]")
		Dim node As XmlElement
		For Each node In nodes
			'Adjust for legacy configuration files.
			If node.GetAttribute("id") = "" Then
				node.SetAttribute("id", node.GetAttribute("database"))
			End If
		Next

		ClientConfigHook = Nothing
		Dim freshload As Boolean = True
		Dim clientConfigNd As XmlElement = CType(ApplicationXML.SelectSingleNode("/Innovator/ClientConfig"), XmlElement)
		If clientConfigNd Is Nothing OrElse String.IsNullOrEmpty(clientConfigNd.GetAttribute("AssemblyName")) Then
			clientConfigNd = AddDefaultClientConfigElement(ApplicationXML, clientConfigNd)
		End If

		Dim assemblyName As String = clientConfigNd.GetAttribute("AssemblyName")
		'Performance Note: This lookup costs 500 microseconds.
		Dim clientHookAssembly As Assembly = find_assembly_by_name(assemblyName)
		If Not clientHookAssembly Is Nothing Then
			freshload = False
			Trace.Write("Already Loaded", clientHookAssembly.GetName().FullName)
		Else
			'Note: As observed in IIS, the only time you will see this
			'      is when the /Client application starts before the dll in question
			'      has been placed into the /Client/bin folder. Otherwise all
			'      /Client/bin dlls are static referenced assemblies when
			'      the ASPX is transformed into a VB.NET and compiled.
			Trace.Write("Loading " + clientConfigNd.GetAttribute("AssemblyNameType"), assemblyName)
			If clientConfigNd.GetAttribute("AssemblyNameType") = "partial" Then
				clientHookAssembly = Assembly.LoadWithPartialName(assemblyName)
			Else
				clientHookAssembly = Assembly.Load(assemblyName)
			End If
		End If

		If clientHookAssembly Is Nothing Then
			Trace.Write("failed to load", assemblyName)
		Else
			If freshload Then
				Trace.Write("Freshly loaded", clientHookAssembly.GetName().FullName)
			End If

			Trace.Write("CreateInstance", clientConfigNd.GetAttribute("TypeName"))
			'Performance Note: This initialization costs 50 microseconds.
			ClientConfigHook = clientHookAssembly.CreateInstance(clientConfigNd.GetAttribute("TypeName"))
			If ClientConfigHook Is Nothing Then
				Trace.Write("CreateInstance failed to find type", clientConfigNd.GetAttribute("TypeName"))
			Else
				Dim cchType As Type = ClientConfigHook.GetType()
				Dim args1() As [Object] = {ApplicationXML}
				cchType.InvokeMember("SetApplicationCfg", BindingFlags.Public Or BindingFlags.InvokeMethod Or BindingFlags.Instance, _
				  Nothing, ClientConfigHook, args1)
			End If
		End If
	End Sub

	Private Shared Function AddDefaultClientConfigElement(ByVal applicationConfig As XmlDocument, clientConfigNd As XmlElement) As XmlElement
		'------------------------------------------------------------
		' $Name: AddDefaultClientConfigElement
		' $Purpose: Register the default logon hooks if none explicitly specified.
		' $Description: If no logon hooks dll is defined through <ClientConfig>,
		'           then add the following default element:
		'           <ClientConfig AssemblyName="Aras.LogonHooks" AssemblyNameType="partial" TypeName="Aras.LogonHooks.PasswordAuth" />
		' $Arguments: The parent DOM
		' $Return: newly added element
		'------------------------------------------------------------
		Dim cce As XmlElement
		If clientConfigNd Is Nothing Then
			cce = applicationConfig.CreateElement("ClientConfig")
			applicationConfig.DocumentElement.AppendChild(cce)
		Else
			cce = clientConfigNd
		End If
		cce.SetAttribute("AssemblyName", "Aras.LogonHooks")
		cce.SetAttribute("AssemblyNameType", "partial")
		cce.SetAttribute("TypeName", "Aras.LogonHooks.PasswordAuth")

		Return cce
	End Function

	Public Function Client_Config(ByVal inKey As String) As Object
		Dim value As Object
		Dim tmp As XmlElement
		Dim key As String = inKey.ToLowerInvariant()
		If key = "branding_img" OrElse _
		 key = "login_splash" OrElse key = "product_name" OrElse key = "top_close_on_logout" OrElse _
		 key = "window_resize_on_login" OrElse _
		 key = "login_window_width" OrElse key = "login_window_height" Then
			tmp = CType(ApplicationXML.SelectSingleNode("/Innovator/UI-Tailoring"), XmlElement)
			If tmp Is Nothing Then
				value = ""
			Else
				value = tmp.GetAttribute(key)
			End If

		ElseIf key = "server_baseurl" OrElse key = "server_ext" Then
			tmp = CType(ApplicationXML.SelectSingleNode("/Innovator/Server"), XmlElement)
			Dim key2 As String
			If key = "server_baseurl" Then
				key2 = "BaseURL"
			Else
				key2 = "EXT"
			End If
			If tmp Is Nothing Then
				value = ""
			Else
				value = tmp.GetAttribute(key2)
			End If

		ElseIf key = "login_database" Then
			value = Request.QueryString.Item("database") ' documented and released.
			If CType(value, String) = "" Then
				value = Request.QueryString.Item("DB")		' used in the Demo database
			End If
		ElseIf key = "setup_qs" Then
			value = Request.ServerVariables("QUERY_STRING")
		Else
			tmp = CType(ApplicationXML.SelectSingleNode("/Innovator/operating_parameter[translate(@key, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-', 'abcdefghijklmnopqrstuvwxyz0123456789_-')=translate('" + key + "', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-', 'abcdefghijklmnopqrstuvwxyz0123456789_-')]"), XmlElement) ' attempt case-insensitive search
			If tmp Is Nothing Then
				tmp = CType(ApplicationXML.SelectSingleNode("/Innovator/operating_parameter[@key='" + key + "']"), XmlElement)
				If tmp Is Nothing Then				
					value = ""
				Else
					value = tmp.GetAttribute("value")
				End If
			Else
				value = tmp.GetAttribute("value")
			End If
		End If
		
		If value Is Nothing Then
			value = ""
		End If

		Trace.Write(key, CType(value, String))
		Return GetHandlerForUI({key, value}, "Client_config")
	End Function

	Public Function GetHtmlForUI(ByVal dt As Integer) As String

		Return CType(GetHandlerForUI({dt}, "GetHtmlForUI"), String)
	End Function

	Public Function GetJSHandlerForUI(ByVal dt As Integer) As String

		Return CType(GetHandlerForUI({dt}, "GetJSHandlerForUI"), String)
	End Function

	Private Function GetHandlerForUI(
	  ByVal args() As Object,
	  ByVal functionName As String) As Object

		Dim value As Object = ""
		
		If functionName = "GetJSHandlerForUI" or functionName = "GetHtmlForUI" Then
			Return value
		End If
		
		If Not ClientConfigHook Is Nothing Then
			Dim cchType As Type = ClientConfigHook.GetType()
			value = cchType.InvokeMember(functionName, BindingFlags.Public Or BindingFlags.InvokeMethod Or BindingFlags.Instance, Nothing, ClientConfigHook, args)
		End If
		Return value
	End Function

	Private Shared Function find_assembly_by_name(ByVal name As String) As Assembly
		Dim ass As Assembly
		Dim assname As AssemblyName
		For Each ass In AppDomain.CurrentDomain.GetAssemblies()
			assname = ass.GetName()
			If assname.Name = name Or assname.FullName = name Then
				Return ass
			End If
		Next
		Return Nothing
	End Function
</script>
<%
	init_application()
%>
