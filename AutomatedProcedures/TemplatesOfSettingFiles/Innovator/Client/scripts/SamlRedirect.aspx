﻿<!DOCTYPE html>
<!-- (c) Copyright by Aras Corporation, 2004-2018. -->
<!-- #INCLUDE FILE="include/utils.aspx" -->
<% 
	Dim finalUrl As String = String.Empty
	If ClientConfig.GetServerConfig().UseCachingModule Then
		finalUrl = "~/" + ClientHttpApplication.FullSaltForCachedContent + "/scripts/Innovator.aspx"
	Else
		finalUrl = "Innovator.aspx"
	End If
	Response.Redirect(finalUrl)
%>
