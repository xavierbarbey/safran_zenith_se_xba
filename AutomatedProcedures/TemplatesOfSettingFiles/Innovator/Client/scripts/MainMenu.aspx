﻿<!DOCTYPE html>
<!-- (c) Copyright by Aras Corporation, 2004-2013. -->
<!-- #INCLUDE FILE="include/utils.aspx" -->
<html>
<head>
	<title></title>
	<script type="text/javascript" src="../javascript/include.aspx?classes=XmlDocument"></script>

	<%-- include before ScriptSet2, order is important --%>
	<script type="text/javascript" src="../javascript/include.aspx?classes=Licensing"></script>

	<script type="text/javascript" src="../javascript/include.aspx?classes=ScriptSet2"></script>
	<script type="text/javascript">
		var topWindow = null,
			toolbarApplet = null,
			menuApplet = null,
			aras = null;

		function initialize() {
			topWindow = TopWindowHelper.getMostTopWindowWithAras(window)
			sessionStorage.setItem("defaultDState", "defaultDState");
			aras = topWindow.aras;
			window.parent.window.loadToolBar(function(control) {
				document.toolbar = toolbarApplet = control;
				initToolbar();
			});

			ArasModules.soap(null, {
				url: aras.getServerURL(),
				method: "ApplyItem",
				headers: {
					"AUTHUSER": encodeURIComponent(aras.getCurrentLoginName()),
					"AUTHPASSWORD": aras.getCurrentPassword(),
					"DATABASE": encodeURIComponent(aras.getDatabase()),
					"LOCALE": aras.getCommonPropertyValue("systemInfo_CurrentLocale"),
					"TIMEZONE_NAME": aras.getCommonPropertyValue("systemInfo_CurrentTimeZoneName")
				}
			});

			var cuiContext = { menuId: 'main_menubar' };
			// call getExistMenuBarId is necessary for calculating id of main menubar. This value will be stored in cui and it will be used for menubar switching.
			topWindow.cui.getExistMenuBarIdAsync("MainWindowMainMenu", cuiContext).then(function() {
				topWindow.cui.loadMenuFromCommandBarsAsync("MainWindowMainMenu", cuiContext).then(function(mainMenuXml) {
					var prefix = 'com.aras.innovator.cui_default.mwmm_';
					window.parent.window.loadMainMenu({
						aras: aras,
						xml: mainMenuXml,
						xmlArgType: "xml",
						cuiContext: cuiContext},
						function (control) {
							menuApplet = document.menu = control;
							menuApplet.set("default_package_name_prefix", prefix);
							topWindow.cui.initMenuEvents(menuApplet, {prefix: prefix});
							menuFrameReady = true;
						}
					);
				});
			});
			var boundedInit = topWindow.cui.callInitHandlersForMenu.bind(topWindow.cui, {}, 'SelectInToc');
			topWindow.aras.registerEventHandler("PreferenceValueChanged", window, boundedInit);

			window.addEventListener('unload', function() {
				aras.unregisterEventHandler("PreferenceValueChanged", window, boundedInit);
			});
		}

		function UpdateMenuAfterCacheReset(itemTypeName) {
			if (topWindow.main && topWindow.main.work && topWindow.main.tree) {
				topWindow.cui.resetMenuCache();
				var itemInToc = new topWindow.main.tree.ItemInToc('')
				itemInToc.loadMenuForItemType(itemTypeName, topWindow.main.work.currItemType);
			}
		}

		function setControlEnabled(ctrlName, b) {
			if (b == undefined) b = true;
			try {
				var mi = menuApplet.findItem(ctrlName);
				if (mi) mi.setEnabled(b);
			}
			catch (excep) { }
		}

		function setControlState(ctrlName, b) {
			if (b == undefined) b = true;

			try {
				var tbi = activeToolbar.getItem(ctrlName);
				if (tbi) tbi.setState(b);
			}
			catch (excep) { }

			try {
				var mi = menuApplet.findItem(ctrlName);
				if (mi) mi.setState(b);
			}
			catch (excep) { }
		}

		function setActiveToolbar(toolbarId) {
			if (!toolbarApplet) return null;
			toolbarApplet.showToolbar(toolbarId);
			activeToolbar = toolbarApplet.getActiveToolbar();
			return activeToolbar;
		}

		function showDefaultToolbar() {
			var cuiContext = {toolbarId: 'main_toolbar'};
			var toolbarId = topWindow.cui.getExistToolbarId('MainWindowToolbar', cuiContext);
			return setActiveToolbar(toolbarId);
		}

		function setAllControlsEnabled(b) {
			if (b == undefined) b = true;

			var mainMenuElements = new Array("new", "save", "open", "download", "print", "export2Excel", "export2Word", "purge", "delete", "edit", "view", "saveAs", "lock", "unlock", "undo", "promote", "revisions", "copy2clipboard");
			for (var i = 0; i < mainMenuElements.length; i++) {
				var mi = menuApplet.findItem(mainMenuElements[i]);
				if (mi) mi.setEnabled(b);
			}
		}

		var menuFrameReady = false;
		var activeToolbar = null;

		function initToolbar() {
			topWindow.cui.initToolbarEvents(toolbarApplet);
			toolbarApplet.showLabels(topWindow.aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_show_labels') == 'true');
			var cuiContext = {toolbarId: 'main_toolbar', 'item_classification': '%all_grouped_by_classification%'};
			topWindow.cui.getExistToolbarIdAsync('MainWindowToolbar', cuiContext).then(function(toolbarId) {
				if (!toolbarApplet.isToolbarExist(toolbarId)) {
					cuiContext.toolbarApplet = toolbarApplet;
					cuiContext.locationName = 'MainWindowToolbar';
					cuiContext.mainSubPrefix = /com.aras.innovator.cui_default.mwt_main_toolbar_/g;
					cuiContext.toolbarId = toolbarId;

					topWindow.cui.loadToolbarFromCommandBarsAsync(cuiContext).then(function() {
						setActiveToolbar(toolbarId);
					});
				} else {
					setActiveToolbar(toolbarId);
				}
			});
		}
	</script>
</head>
</html>
