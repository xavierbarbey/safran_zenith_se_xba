﻿<?xml version="1.0" encoding="utf-8"?>
<!-- FILENAME: Server/web.config
     PURPOSE:  configure Innovator Server application.
     USE:      Default web.config
     NOTE:     For details on editing this file you must refer to http://msdn.microsoft.com/
               "NET Framework Developer's Guide" "ASP.NET Configuration"
  -->
<configuration>
  <configSections>
    <section name="oauth" type="Aras.OAuth.Configuration.OAuthSection, Aras.OAuth.Configuration" />
    <section name="webservicepublishing" type="System.Configuration.NameValueSectionHandler" />
    <sectionGroup name="Aras">
      <sectionGroup name="Net">
        <section name="RequestProvider" type="Aras.Net.Configuration.RequestProviderConfigurationSection, Aras.Net" />
      </sectionGroup>
    </sectionGroup>
  </configSections>
  <oauth configSource="OAuth.config" />
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <!-- Binding redirects from Aras.Web.Server/App.config -->
      <dependentAssembly>
        <assemblyIdentity name="System.IdentityModel.Tokens.Jwt" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.20622.1351" newVersion="4.0.20622.1351" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-8.0.0.0" newVersion="8.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <location path="UserNotifications/Auth/NotificationServer.aspx" allowOverride="false">
    <system.web>
      <identity impersonate="false" />
      <authorization>
        <allow roles="BUILTIN\AppDomainOwner" />
        <deny users="*" />
      </authorization>
    </system.web>
  </location>
  <location path="UserNotifications/ProxyPage.aspx" allowOverride="false">
    <system.web>
      <identity impersonate="false" />
    </system.web>
  </location>
  <system.web>
    <httpRuntime maxRequestLength="10000" executionTimeout="3600" targetFramework="4.5.2" />
    <customErrors mode="On" />
    <!--
		Use culture attribute to tell Innovator Server which locale to use when parsing float numbers.
        Example:    culture="en-US" for english (United States)
                    culture="en-GB" for english (United Kingdom)
                    culture="de-DE" for german (Germany)
                    culture="de-CH" for german (Switzerland)-->
    <globalization requestEncoding="utf-8" responseEncoding="utf-8" culture="en-US" uiCulture="en-US" />
    <pages validateRequest="false" />
    <roleManager enabled="true" defaultProvider="WindowsTokenRoleProvider">
      <providers>
        <clear />
        <add name="WindowsTokenRoleProvider" type="Aras.Server.RoleProviders.WindowsTokenRoleProvider" />
      </providers>
    </roleManager>
    <sessionState mode="InProc" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;Trusted_Connection=yes" cookieless="false" timeout="480" />
    <!--trace enabled="false"
			   requestLimit="100"
			   pageOutput="false"
			   traceMode="SortByTime"
			   localOnly="false"/-->
    <webServices>
      <protocols>
        <add name="HttpGet" />
        <add name="HttpPost" />
      </protocols>
    </webServices>
    <compilation>
      <assemblies>
        <remove assembly="minisat_x64" />
      </assemblies>
    </compilation>
  </system.web>
  <!--Specify the user credentials on whose behalf will be published web services-->
  <webservicepublishing>
    <add key="user" value="{user name}" />
    <add key="domain" value="{domain name}" />
    <add key="password" value="{password}" />
  </webservicepublishing>
  <system.webServer>
    <modules>
      <add name="ArasHttpSessionStateConfigurationModule" type="Aras.Server.HttpModules.SessionStateConfigurationModule.HttpSessionStateConfigurationModule,Aras.Server.HttpModules" />
    </modules>
    <handlers accessPolicy="Read, Script">
      <add verb="GET,POST,DELETE,PATCH,PUT" name="OData" path="*/server/OData" type="Aras.Server.Core.OData.ODataHttpHandler" resourceType="Unspecified" />
    </handlers>
    <directoryBrowse enabled="false" />
    <defaultDocument>
      <files>
        <clear />
        <add value="Default.aspx" />
        <add value="default.asp" />
        <add value="index.html" />
        <add value="index.htm" />
        <add value="default.html" />
        <add value="default.htm" />
      </files>
    </defaultDocument>
    <httpErrors>
      <clear />
    </httpErrors>
  </system.webServer>
  <!--Specify the providers; if authentication mode="Basic" then use Example1, if mode="Windows" or another use Example2-->
  <!--If you want to use NotificationServer then you need to use Example3-->
  <Aras>
    <Net>
      <RequestProvider>
        <providers>
          <!--Example1:-->
          <!--<provider uriPattern="{uriPattern}" type="{type}">
							<authentication mode="{mode}">
								<Basic>
									<credentials>
										<user name="{name}" password="{password}"/>
									</credentials>
								</Basic>
							</authentication>
							<Proxy mode="{mode}">
								<Basic>
									<credentials>
										<user name="{name}" password="{password}"/>
									</credentials>
								</Basic>
							</Proxy>
					</provider>-->
          <!--Example2:-->
          <!--<provider uriPattern="{uriPattern}" type="{type}">
							<authentication mode="{mode}">
							</authentication>
							<Proxy mode="{mode}">							
							</Proxy>
					</provider>-->
          <!--Example3:-->
          <provider uriPattern="NotificationServer.aspx">
            <authentication mode="Windows" />
            <!--<Proxy mode="Basic">
							<Basic>
								<credentials>
									<user name="{name}" password="{password}"/>
								</credentials>
							</Basic>
						</Proxy>-->
          </provider>
        </providers>
      </RequestProvider>
    </Net>
  </Aras>
</configuration>