﻿const rollup = require('rollup');
const rollupConfig = require('./nodejs/rollup.config');
const jsCompileHelper = require('./nodejs/jsCompileHelper');
const babelConfig = require('./nodejs/babel.config');
const rollupBabel = require('rollup-plugin-babel');
const rollupResolve = require('rollup-plugin-node-resolve');

const rollupPlugins = rollupConfig.plugins.concat([{
	onwrite: jsCompileHelper.getRollupWriteHandler()
}]);

const watchOptions = [];
jsCompileHelper.getBundleList().forEach(function(rollupJSModule) {
	watchOptions.push({
		input: rollupJSModule.input,
		output: [{
			format: rollupConfig.output[0].format,
			sourcemap: rollupConfig.output[0].sourcemap,
			name: rollupJSModule.name,
			file: rollupJSModule.file
		}],
		plugins: rollupPlugins,
	});
});

const babelConfigForTests = Object.assign({}, babelConfig, {
	'plugins': babelConfig.plugins.concat([require.resolve('babel-plugin-rewire-exports')])
});

const rollupPluginsForTests = [
	rollupResolve(),
	rollupBabel(babelConfigForTests)
];

const testCasesForESCompile = require('./tests/javascriptTests').includingJSLibrares.testCasesForESCompile;
testCasesForESCompile.forEach(function(esTestModule) {
	watchOptions.push({
		input: esTestModule,
		output: [{
			format: rollupConfig.output[0].format,
			sourcemap: 'inline',
			file: esTestModule.replace('testCases', 'testCasesCompiled')
		}],
		plugins: rollupPluginsForTests,
	});
});

const watcher = rollup.watch(watchOptions);
watcher.on('event', function(event) {

	const eventHandlers = {
		START: () => {
			console.log('\x1Bc');
		},
		BUNDLE_START: () => {
			console.log('Build Bundle start (' + event.input + ')...');
		},
		BUNDLE_END: () => {
			console.log('\x1b[32m', 'Build Bundle complete (' + event.input + ') \x1b[35m' + event.duration, '\x1b[37m');
		},
		END: () => {
			console.log('\x1b[42m                             \x1b[0m');
			console.log('\x1b[42m  %s\x1b[0m', 'Build All Bundles complete ');
			console.log('\x1b[42m                             \x1b[0m');
		},
		ERROR: () => {
			console.log('\x1b[31m', event.error, '\x1b[37m');
		},
		FATAL: () => {
			console.log('\x1b[31m', event.error, '\x1b[37m');
		}
	};

	if (eventHandlers[event.code]) {
		eventHandlers[event.code]();
	}
});
