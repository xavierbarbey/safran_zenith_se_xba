﻿function XmlRequestImplementation() {
	this.xhr = new XMLHttpRequest();
	this.headers = {};
}

XmlRequestImplementation.prototype.open = function(method, url, isAsync) {
	isAsync = (isAsync === 1 || isAsync === true);
	this.xhr.open(method, url, isAsync);
};

Object.defineProperty(XmlRequestImplementation.prototype, 'onreadystatechange', {
	set: function(value) {
		this.xhr.onreadystatechange = value;
	}
});

XmlRequestImplementation.prototype.setRequestHeader = function(name, value) {
	this.headers[name] = value;
};

XmlRequestImplementation.prototype.send = function(body) {
	for (var key in this.headers) {
		if (this.headers.hasOwnProperty(key)) {
			this.xhr.setRequestHeader(key, this.headers[key]);
		}
	}
	//this line is required for IE 10.
	try { this.xhr.responseType = 'msxml-document'; } catch (e) { }
	this.xhr.send(body);
};

XmlRequestImplementation.prototype.getResponseHeader = function(header) {
	return this.xhr.getResponseHeader(header);
};

XmlRequestImplementation.prototype.getAllResponseHeaders = function() {
	return this.xhr.getAllResponseHeaders();
};

Object.defineProperty(XmlRequestImplementation.prototype, 'readyState', {
	get: function() {
		return this.xhr.readyState;
	}
});

Object.defineProperty(XmlRequestImplementation.prototype, 'status', {
	get: function() {
		return this.xhr.status;
	}
});

Object.defineProperty(XmlRequestImplementation.prototype, 'responseText', {
	get: function() {
		return this.xhr.responseText;
	}
});

Object.defineProperty(XmlRequestImplementation.prototype, 'responseXML', {
	get: function() {
		return this.xhr.responseXML;
	}
});

function XmlHttpRequestManager() {
}

XmlHttpRequestManager.prototype.CreateRequest = function() {
	return new XmlRequestImplementation();
};
