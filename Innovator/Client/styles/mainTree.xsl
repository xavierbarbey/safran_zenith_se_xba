﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" />
	<xsl:template match="/">
		<table enableHTML="false" icon0="" icon1="" treelines="1"
				thinBorder="true">
			<menu></menu>
			<thead>
				<th align="center"></th>
			</thead>
			<xsl:apply-templates select="/*/*/Result/Item[@type='Tree']" />
		</table>
	</xsl:template>
	<xsl:template match="Item[@type='Tree']">
		<xsl:apply-templates select="root/Item[@type='Tree Node']"/>
	</xsl:template>
	<xsl:template match="Item[@type='Tree Node'][classification='Tree Node/TocCategory']">
		<xsl:param name="level">0</xsl:param>
		<tr>
			<xsl:attribute name="level">
				<xsl:value-of select="$level"/>
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="icon0">
				<xsl:value-of select="close_icon"/>
			</xsl:attribute>
			<xsl:attribute name="icon1">
				<xsl:value-of select="open_icon"/>
			</xsl:attribute>
			<userdata key="className" value="TocCategory"/>
			<td align="left">
				<xsl:value-of select="label"/>
			</td>
			<xsl:apply-templates select="Relationships/Item[@type='Tree Node Child']/related_id/Item[@type='Tree Node']">
				<xsl:with-param name="level" select="$level+1"/>
			</xsl:apply-templates>
		</tr>
	</xsl:template>
	<xsl:template match="Item[@type='Tree Node'][classification='Tree Node/ItemTypeInToc']">
		<xsl:param name="level">0</xsl:param>
		<tr>
			<xsl:attribute name="level">
				<xsl:value-of select="$level"/>
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="icon0">
				<xsl:value-of select="open_icon"/>
			</xsl:attribute>
			<xsl:attribute name="icon1">
				<xsl:value-of select="close_icon"/>
			</xsl:attribute>
			<userdata key="className" value="ItemTypeInToc"/>
			<td align="left">
				<xsl:value-of select="label"/>
			</td>
			<xsl:apply-templates select="Relationships/Item[@type='Tree Node Child']/related_id/Item[@type='Tree Node']">
				<xsl:with-param name="level" select="$level+1"/>
			</xsl:apply-templates>
		</tr>
	</xsl:template>
	<xsl:template match="Item[@type='Tree Node'][classification='Tree Node/SavedSearchInToc']">
		<xsl:param name="level">0</xsl:param>
		<tr>
			<xsl:attribute name="level">
				<xsl:value-of select="$level"/>
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="saved_search_id"/>
			</xsl:attribute>
			<xsl:attribute name="icon0">
				<xsl:value-of select="open_icon"/>
			</xsl:attribute>
			<xsl:attribute name="icon1">
				<xsl:value-of select="close_icon"/>
			</xsl:attribute>
			<xsl:attribute name="iconsMultiple">
				<xsl:value-of select="'../images/SavedSearchOverlay.svg'"/>
			</xsl:attribute>
			<userdata key="className" value="SavedSearchInToc"/>
			<userdata key="itName">
				<xsl:attribute name="value">
					<xsl:value-of select="name"/>
				</xsl:attribute>
			</userdata>
			<td align="left">
				<xsl:value-of select="label"/>
			</td>
			<xsl:apply-templates select="Relationships/Item[@type='Tree Node Child']/related_id/Item[@type='Tree Node']">
				<xsl:with-param name="level" select="$level+1"/>
			</xsl:apply-templates>
		</tr>
	</xsl:template>
</xsl:stylesheet>