﻿var aras = parent.aras ? parent.aras : parent.parent.aras;
var mainWnd = aras.getMostTopWindowWithAras(window);
var innovator = new mainWnd.Innovator();
function getRelationshipTypeFromCacheById(relTypeId) {
	// Gets the RelationshipType from the cache and returns is as an IOM item
	var relType = innovator.getItemById("RelationshipType", relTypeId);
	// Get the relationship item separately and add to the item dom
	var relItem = innovator.getItemById("ItemType", relType.getProperty("relationship_id"));
	relType.setPropertyItem("relationship_id", relItem);
	return relType;
}

function getVisibleProperties(relType) {
	var props = innovator.newItem();

	props.loadAML(relType.applyStylesheet("../Client/Solutions/PLM/styles/visibleProperties.xsl", "URL"));
	// Special handling for lists
	var listProps = props.getItemsByXPath("AML/Item[@type='Property'][data_type='list']");
	var thisList = innovator.newItem();
	for (var i = 0; i < listProps.getItemCount() ; i++) {
		var listId = listProps.getItemByIndex(i).getProperty("data_source");
		thisList.loadAML(aras.getItemById("List", listId).xml);
		listProps.getItemByIndex(i).setProperty("data_source", "");
		listProps.getItemByIndex(i).node.selectSingleNode("data_source").appendChild(thisList.node);
	}
	return props;
}

function getGridSetupXML(props) {
	var gridDom = props.newXMLDocument();
	gridDom.loadXML(props.applyStylesheet("../Client/Solutions/PLM/styles/gridSetup.xsl", "URL"));
	return gridDom;
}

function syncWithClient(serverRelationships) {

	var res = serverRelationships.dom.selectSingleNode(aras.XPathResult());
	var clientRelationships = thisItem.getRelationships(relTypeName);

	if (!res || serverRelationships.isError()) {
		serverRelationships.nodeList = clientRelationships.nodeList;
		return;
	}

	for (var i = 0; i < clientRelationships.getItemCount() ; i++) {
		var clientRelationship = clientRelationships.getItemByIndex(i);
		var action = clientRelationship.getAction();
		var isDirty = clientRelationship.getAttribute("isDirty") == "1";
		var isTemp = clientRelationship.getAttribute("isTemp") == "1";
		if (!(action == "delete" || action == "purge" || isDirty || isTemp)) {
			continue;
		}

		if (isTemp) {
			res.appendChild(clientRelationship.node.cloneNode(true));
			continue;
		}

		var itmId = clientRelationship.getID();
		var bad_itm = res.selectSingleNode("Item[@id='" + itmId + "']");
		if (bad_itm) {
			res.replaceChild(clientRelationship.node.cloneNode(true), bad_itm);
		}
		else {
			if (clientRelationship.getAttribute("isDirty") == "1") {
				res.appendChild(clientRelationship.node(true));
			}
		}
	}
	serverRelationships.nodeList = serverRelationships.dom.selectNodes(aras.XPathResult("/Item"));
}

function getRelationshipsGridData(itemId, relType, props) {

	var i;
	var selectList = "";
	var relationshipProps = props.getItemsByXPath("//Item[@type='Property'][source_id='" + relType.getProperty("relationship_id") + "']");
	for (i = 0; i < relationshipProps.getItemCount() ; i++) {
		selectList = selectList + relationshipProps.getItemByIndex(i).getProperty("name") + ",";
	}

	var relatedSelectList = "";
	var relatedProps = props.getItemsByXPath("//Item[@type='Property'][source_id='" + relType.getProperty("related_id") + "']");
	for (i = 0; i < relatedProps.getItemCount() ; i++) {
		relatedSelectList = relatedSelectList + relatedProps.getItemByIndex(i).getProperty("name") + ",";
	}

	var qryItem = innovator.newItem();
	qryItem.setAttribute("type", relType.getPropertyItem("relationship_id").getProperty("name"));
	qryItem.setAction("get");
	qryItem.setAttribute("select", selectList + ",related_id(" + relatedSelectList + ")");
	qryItem.setAttribute("page", "1");
	qryItem.setAttribute("pagesize", relType.getPropertyItem("relationship_id").getProperty("default_page_size"));
	qryItem.setProperty("source_id", itemId);
	var results = qryItem.apply();

	return results;
}
//function getRelationshipsGridData

function getGridDataXML4Cell(itm, thisProp, thisData) {

	//  var thisProp = oldThisProp.clone(true);
	if (this.modifyPropertyAndData) {
		modifyPropertyAndData(thisProp, thisData);
	}

	var gridDataXML = "";
	var propName = thisProp.getProperty("name");
	var propType = thisProp.getProperty("data_type");
	var propSource = thisProp.getProperty("data_source");
	var prop;
	if (propType == "item") {
		prop = itm.getPropertyAttribute(propName, "keyed_name");
	}
	else {
		prop = itm.getProperty(propName);
	}

	if (propType == "boolean") {
		gridDataXML += "<td><![CDATA[<checkbox state=\"" + prop + "\" />]]></td>";
	}
	else {
		gridDataXML += "<td ";
		if (propType == "item" && propSource && !isEditMode) {
			var sourceItemType = innovator.getItemById("ItemType", propSource);
			if (sourceItemType) {
				var sourceItemTypeName = sourceItemType.getProperty("name");
				gridDataXML += " link=\"'" + sourceItemTypeName + "','" + itm.getProperty(propName) + "'\" ";
			}
		}

		if (thisData.getAction() == "delete") {
			gridDataXML += " TextFont='" + deletedFont_const + "' TextColor='" + deletedTextColor_const + "' >";
		}
		else {
			gridDataXML += " >";
		}

		if (prop !== undefined) {
			gridDataXML += "<![CDATA[" + prop + "]]>";
		}
		gridDataXML += "</td>";
	}
	return gridDataXML;
}

function getGridDataXML4Row(relType, props, thisData) {
	var gridDataXML = "";
	var thisId = thisData.getID();
	gridDataXML += "<tr  id='" + thisId + "' action=\"'" + thisId + "'\">";
	for (var j = 0; j < props.getItemCount() ; j++) {
		var thisProp = props.getItemByIndex(j);

		var itm;
		if ((relType.getProperty("relationship_id")) && (thisProp.getProperty("source_id") != relType.getProperty("relationship_id"))) {
			itm = thisData.getPropertyItem("related_id");
		}
		else {
			itm = thisData;
		}
		gridDataXML += getGridDataXML4Cell(itm, thisProp, thisData);

	}
	gridDataXML += "</tr>";
	return gridDataXML;
}

function getGridDataXML(relType, props, data) {
	var gridDataXML = "<table>";
	for (var i = 0; i < data.getItemCount() ; i++) {
		var thisData = data.getItemByIndex(i);
		gridDataXML += getGridDataXML4Row(relType, props, thisData);
	}
	gridDataXML += "</table>";
	return gridDataXML;
}

function getPropsArr(relType, props) {
	var propsArr = [];

	for (var i = 0; i < props.getItemCount() ; i++) {
		var thisProp = props.getItemByIndex(i);
		var propNm = thisProp.getProperty("name");
		var propDRL;
		if (thisProp.getProperty("source_id") == relType.getProperty("relationship_id")) {
			propDRL = "D";
		}
		else {
			propDRL = "R";
		}
		var data_type = thisProp.getProperty("data_type");
		var data_source = thisProp.getProperty("data_source");
		var propID_val = thisProp.getID();

		propsArr.push(
			{
				name: propNm,
				DRL: propDRL,
				data_type: data_type,
				data_source: data_source,
				propID: propID_val
			}
		);
	}

	return propsArr;
}

// return relationship with specified id from thisItem
function getRelationship(id) {
	var relTypeName = relType.getProperty("name");

	var relationshisCollection = thisItem.getRelationships(relTypeName);
	var count = relationshisCollection.getItemCount();

	for (var i = 0; i < count; i++) {
		var relationship0 = relationshisCollection.getItemByIndex(i);
		if (relationship0.getID() == id) {
			return relationship0;
		}
	}

	var relationship = innovator.getItemById(relTypeName, id);
	thisItem.addRelationship(relationship);

	return relationship;
}

function setDirtyAttribute(relationship) {
	var tmpItemTypeName = relationship.getType();
	var act = relationship.getAction();
	if (!act || (act != "add" && act != "create" && act != "delete" && act != "purge")) {
		relationship.setAction("update");
	}

	if (relationship.getAttribute("isDirty") != "1") {
		relationship.setAttribute("isDirty", "1");
	}
}

function setupProperty(propVal, markDirty) {
	if (!currSelCell) {
		return false;
	}

	relationship = getRelationship(currSelRowId);
	var prop = propsArr[currSelCol];

	var propNm = prop.name;
	var oldValue;

	var item;
	if (prop.DRL == "D") {
		item = relationship;
	}
	else if (prop.DRL == "R") {
		item = relationship.getRelatedItem();
	}

	if (!propVal.node) {
		oldValue = item.getProperty(propNm);
	}
	else {
		oldValue = item.getPropertyItem(propNm);
	}

	if (oldValue == propVal) {
		return;
	}

	if (!propVal.node) {
		item.setProperty(propNm, propVal);
	}
	else {
		item.setPropertyItem(propNm, propVal);
		// set keyed_name (needed to update Grid Applet)
		var keyed_name_value = propVal.getAttribute("keyed_name");
		if (keyed_name_value === null) {
			keyed_name_value = "";
		}
		item.setPropertyAttribute(propNm, "keyed_name", keyed_name_value);
	}

	if (markDirty) {
		setDirtyAttribute(thisItem);
		setDirtyAttribute(relationship);
		if (relationship != item) {
			setDirtyAttribute(item);
		}
		if (mainWnd.updateItemsGrid) {
			mainWnd.updateItemsGrid(thisItem.node);
		}
	}

	onAfterPropertyChange(propNm, oldValue, propVal);
}

function onAfterPropertyChange(propNm, oldValue, propVal) {
	// call specific function
	var functionName = "onAfter_" + propNm + "_Change";
	var executed = false;
	var res;
	var code = "try {res = " + functionName + "(oldValue, propVal); executed=true;} catch (ex) {res = undefined;}";
	var evalFuntion = window.eval;
	evalFuntion(code);
	if (executed) {
		return res;
	}
}

function getItemById(ITName, id, levels) {
	if (!id || !ITName) {
		return null;
	}
	if (levels === undefined) {
		levels = 0;
	}

	var item = new Item(ITName, "get");
	item.setID(id);
	item.setAttribute("levels", levels);
	item = item.apply();

	if (item.isError()) {
		return null;
	}
	return item;
}

function getLockedStatusStr(rowId) {
	if (!rowId) {
		return;
	}
	var relationship = getRelationship(rowId);

	if (!relationship || relationship.isError()) {
		return;
	}

	var relatedItem = relationship.getRelatedItem();
	if (!relatedItem || relatedItem.isError()) {
		return;
	}

	if (relatedItem.getAttribute("isNew") == "1") {
		return "new";
	}
	else if (aras.isLockedByUser(relatedItem.node)) {
		return "user";
	}
	else if (aras.isLocked(relatedItem.node)) {
		return "alien";
	}
	else {
		return "";
	}
}

////////////////////////////  +++ Handlers +++  ////////////////////////////

function onDeleteClick() {
	var relId = gridApplet.getSelectedId();
	var relationship = getRelationship(relId);
	var action = relationship.getAction();

	if (!relId) {
		alert(aras.getResource("plm", "relationships_method.select_relship"));
		return;
	}

	if (action == "add") {
		thisItem.removeRelationship(relationship);
		if (gridApplet.getRowIndex(relId) > -1) {
			gridApplet.deleteRow(relId);
		}
	}
	else {
		relationship.setAction("delete");
		for (var i = 0; i < gridApplet.getColumnCount() ; i++) {
			var cell = gridApplet.cells(relId, i);
			cell.setFont(deletedFont_const);
			cell.setTextColor(deletedTextColor_const);
		}
		setDirtyAttribute(thisItem);
	}
	if (mainWnd.updateItemsGrid) {
		mainWnd.updateItemsGrid(thisItem.node);
	}
	onRowSelect(relId);
}

function onNewClick() {
	var relationship = new Item(relTypeName, "add");
	relationship.setAttribute("isTemp", "1");
	if (relatedTypeName) {
		var related = new Item(relatedTypeName, "add");
		relationship.setAttribute("isTemp", "1");
		relationship.setRelatedItem(related);
	}

	thisItem.addRelationship(relationship);
	addRow(relationship);

	//  updateGridApplet();
	setDirtyAttribute(thisItem);
	if (mainWnd.updateItemsGrid) {
		mainWnd.updateItemsGrid(thisItem.node);
	}
	return relationship;
}

function onShow_itemClick() {
	if (!currSelRowId) {
		alert(aras.getResource("plm", "relationships_method.select_relship"));
		return;
	}
	showRelatedItemById(currSelRowId);
}

function onShow_relationshipClick() {
	if (!currSelRowId) {
		alert(aras.getResource("plm", "relationships_method.select_relship"));
		return;
	}
	showRelationshipById(currSelRowId);
}

function onLockClick() {
	if (!currSelRowId) {
		alert(aras.getResource("plm", "relationships_method.select_relship"));
		return;
	}
	var relationship = getRelationship(currSelRowId);
	var related = relationship.getRelatedItem();
	aras.lockItemEx(related.node);
	updateRow(relationship);
	updateToolbar();
}

function onUnlockClick() {
	if (!currSelRowId) {
		alert(aras.getResource("plm", "relationships_method.select_relship"));
		return;
	}
	var relationship = getRelationship(currSelRowId);
	var related = relationship.getRelatedItem();
	aras.unlockItemEx(related.node);
	updateRow(relationship);
	updateToolbar();
}

function showRelatedItemById(relID) {

	var ritems = thisItem.getItemsByXPath("Relationships/Item[@id=\"" + relID + "\"]/related_id/Item");
	if (ritems.getItemCount() < 1) {
		return;
	}
	var ritem = ritems.getItemByIndex(0);

	var ritemID = ritem.getID();
	var win = aras.uiFindWindowEx(ritemID);
	if (win && (win.fromRelationships === undefined || win.fromRelationships !== itemID)) {
		aras.AlertError(aras.getResource("plm", "relationships_method.already_opened"));
		return true;
	}

	aras.uiShowItemEx(ritem.node, "tab view");
	return true;
}

function showRelationshipById(relID) {
	var rels = thisItem.getItemsByXPath("Relationships/Item[@id=\"" + relID + "\"]");
	if (rels.getItemCount() < 1) {
		return;
	}
	var rel = rels.getItemByIndex(0);

	var win = aras.uiFindWindowEx(relID);
	if (win && (win.fromRelationships === undefined || win.fromRelationships !== itemID)) {
		aras.AlertError(aras.getResource("plm", "relationships_method.already_opened"));
		return true;
	}

	aras.uiShowItemEx(rel.node, "tab view");
	return true;
}
