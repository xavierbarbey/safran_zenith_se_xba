﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" />

<xsl:template match="/">
 <AML>
  <xsl:apply-templates select="Item[@type='RelationshipType']"/>
  <xsl:apply-templates select="Item[@type='ItemType']"/>
  <xsl:apply-templates select="Envelope/Body/Result/Item[@type='ItemType']" />
 </AML>
</xsl:template>

<xsl:template match="Item[@type='RelationshipType'][grid_view='intermix']">
 <xsl:apply-templates select="*/Item/Relationships/Item[@type='Property'][(name(../../..)='related_id' and is_hidden2='0') or (name(../../..)='relationship_id' and is_hidden='0')]">
  <xsl:sort select="sort_order" data-type="number"/>
 </xsl:apply-templates>
</xsl:template>

<xsl:template match="Item[@type='RelationshipType'][grid_view='right']">
 <xsl:apply-templates select="related_id/Item/Relationships/Item[@type='Property'][is_hidden2='0']">
  <xsl:sort select="sort_order" data-type="number"/>
 </xsl:apply-templates>
 <xsl:apply-templates select="relationship_id/Item/Relationships/Item[@type='Property'][is_hidden='0']">
  <xsl:sort select="sort_order" data-type="number"/>
 </xsl:apply-templates>
</xsl:template>

<xsl:template match="Item[@type='RelationshipType'][grid_view='left' or grid_view='']">
 <xsl:apply-templates select="relationship_id/Item/Relationships/Item[@type='Property'][is_hidden='0']">
  <xsl:sort select="sort_order" data-type="number"/>
 </xsl:apply-templates>
 <xsl:apply-templates select="related_id/Item/Relationships/Item[@type='Property'][is_hidden2='0']">
  <xsl:sort select="sort_order" data-type="number"/>
 </xsl:apply-templates>
</xsl:template>

<xsl:template match="Item[@type='ItemType']">
 <xsl:apply-templates select="Relationships/Item[@type='Property'][is_hidden='0']">
	 <xsl:sort select="sort_order" data-type="number"/>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Item[@type='Property']">
  <xsl:if test="name!='related_id'">
   <xsl:copy-of select="."/>
 </xsl:if>
</xsl:template>

</xsl:stylesheet>
