﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" />

<xsl:template match="/">
 <table 
				delim="|"
        font="Arial-8"
        link_func="onLink"
        draw_grid="true" 
        multiselect="false" 
        column_draggable="false" 
        enableHtml="false" 
        enterAsTab="false" 
        bgInvert="false" 
        onEditCell = "onEditCell"
        onRowSelect= "onRowSelect"
        onXmlLoaded = "onXmlLoaded"
        onKeyPressed= "onKeyPressed"
        onMenuInit="onMenuCreate"
        onMenuClick="onMenuClicked"
        editable="true">
  <thead>

   <xsl:apply-templates select="/AML/Item[@type='Property']">
    <xsl:with-param name="type">th</xsl:with-param>
   </xsl:apply-templates>
  </thead>
  <columns>
   <xsl:apply-templates select="/AML/Item[@type='Property']">
    <xsl:with-param name="type">column</xsl:with-param>
   </xsl:apply-templates>
  </columns>
  
  <xsl:apply-templates select="/AML/Item[@type='Property']/data_source/Item[@type='List']"/>
  <menu/>

</table>
</xsl:template>


<xsl:template match="Item[@type='Property']">
 <xsl:param name="type"/>

 <xsl:choose>
  <xsl:when test="$type='th'">
   <th align="c"><xsl:value-of select="label"/></th>
  </xsl:when>
  <xsl:when test="$type='column'">
   <column>
    <xsl:attribute name="width">
     <xsl:choose>
      <xsl:when test="not(column_width) or column_width=''">100</xsl:when>
      <xsl:otherwise><xsl:value-of select="column_width"/></xsl:otherwise>
     </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="edit">
     <xsl:choose>
      <xsl:when test="data_type='list'">
       <xsl:text>COMBO:</xsl:text><xsl:value-of select="count(preceding-sibling::Item/data_source/Item[@type='List'])+1"/>
      </xsl:when>
     </xsl:choose>
    </xsl:attribute>
   </column>
  </xsl:when>
 </xsl:choose>
</xsl:template>

<xsl:template match="Item[@type='List']">
 <list>
  <xsl:attribute name="id"><xsl:value-of select="position()"/></xsl:attribute>
  <xsl:apply-templates select="Relationships/Item[@type='Value']"/>
 </list>
</xsl:template>

<xsl:template match="Item[@type='Value']">
 <listitem>
  <xsl:attribute name="label"><xsl:value-of select="label"/></xsl:attribute>
  <xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
 </listitem>
</xsl:template>

</xsl:stylesheet>
