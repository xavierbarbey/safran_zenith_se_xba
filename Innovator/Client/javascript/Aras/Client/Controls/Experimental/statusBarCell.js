﻿define([
	'dojo/_base/declare',
	'dojo/on',
	'./PopupNotification',
	'dijit/_base/popup'
],
function(declare, on, PopupNotification, popup) {

	return declare('Aras.Client.Controls.Experimental.statusBarCell', null, {
		_cell: null,
		_popup: null,

		constructor: function(cell) {
			this._cell = cell;
			this._popup = new PopupNotification(this._cell);
			if ('messages' == this._cell.itemId) {
				on(this._cell, 'click', this.ShowPopup.bind(this));
			}
		},

		GetNotificationControl: function() {
			return this._popup;
		},

		ShowPopup: function(timeout) {
			var tempPopup = dijit.popup.open({
				id: this._cell.itemId + 'Popup',
				popup: this._popup._contener,
				around: this._cell.domNode,
				onCancel: function() {
					dijit.popup.close();
				}
			});
			var self = this;
			if ('messages' != this._cell.itemId) {
				if (timeout == -1) {
					dijit.popup.close(this._popup._contener);
					this._popup.Clear();
				} else if (timeout > 0) {
					setTimeout(function() { dijit.popup.close(self._popup._contener); self._popup.Clear(); }, timeout * 1000);
				} else {
					this._popup._contener.domNode.focus();
				}
				return;
			} else {
				this._popup._contener.domNode.parentNode.style.left = '';
				this._popup._contener.domNode.parentNode.style.right = '10px';
			}
			this._popup._contener.domNode.focus();
		}
	});
});
