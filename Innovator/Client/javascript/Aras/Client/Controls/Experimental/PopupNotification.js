﻿define([
	'dojo/_base/declare',
	'dojo/_base/connect',
	'dojo/dom-construct',
	'dijit/dijit',
	'dijit/_base/popup',
	'dijit/layout/ContentPane',
	'dijit/layout/BorderContainer'
],
function(declare, connect, Dom, dijit, popup, ContentPane, BorderContainer) {
	var NotificationContainer = declare(BorderContainer, {
		_layoutChildren: function(/*String?*/changedChildId, /*Number?*/changedChildSize) {
			// summary:
			//		This is the main routine for setting size/position of each child.
			// description:
			//		With no arguments, measures the height of top-bottom panes, the width
			//		of left/right panes, and then sizes all panes accordingly.
			//
			//		With changedRegion specified (as "left", "top", "bottom", or "right"),
			//		it changes that region's width/height to changedRegionSize and
			//		then resizes other regions that were affected.
			// changedChildId:
			//		Id of the child which should be resized because splitter was dragged.
			// changedChildSize:
			//		The new width/height (in pixels) to make specified child
		},

		'class': 'popupMessages'
	});

	return declare('Aras.Client.Controls.Experimental.PopupNotification', null, {

		_cell: null,
		_contener: null,
		Count: 0,

		constructor: function(cell) {
			this._cell = cell;
			this._contener = new NotificationContainer({tabindex: '0'});
			this._dir = cell.aras.getLanguageDirection();
			var self = this;
			connect.connect(this._contener, 'onBlur', function() {
				//leave notification open while 5 seconds for the case when message focus is accidentally lost
				setTimeout(function() {
					self.notificationMessageClicked();
				}, 5000);
			});

			this._isMessagesCell = function() {
				return ('messages' == this._cell.itemId);
			};

			this._updateMessagesCount = function() {
				if (this._isMessagesCell()) {
					this._cell.domNode.classList.toggle('hasAcknowledgeOnceMessages', hasAcknowledgeOnceMessages());
					var message = this._cell.message_template.replace('{0}', this.Count);
					this._cell.SetText(message);
				}
			};

			function hasAcknowledgeOnceMessages() {
				var messageNodes = self._contener.getChildren();
				return messageNodes.some(function(node) {
					return node.acknowledge.toLowerCase() === 'once';
				});
			}
		},

		Add: function(id, text, image, messageItem) {
			var type;
			var acknowledge;
			var aggregatedMessageCount;
			if (messageItem && typeof messageItem === 'object') {
				type = messageItem.getAttribute('type');
				acknowledge = parent.aras.getItemProperty(messageItem, 'acknowledge');
				aggregatedMessageCount = +parent.aras.getItemProperty(messageItem, 'count');
			}
			if(!aggregatedMessageCount) {
				aggregatedMessageCount = 1;
			}
			var panel = new ContentPane({id: id + '_popup', acknowledge: acknowledge, aggregatedMessageCount: aggregatedMessageCount});
			panel.set('dir', this._dir);
			var span = Dom.create('span');
			span.innerHTML = text;
			Dom.place(span, panel.domNode, 'first');

			if (image) {
				imageNode = Dom.create('img', {src: image, align: 'left'});
				Dom.place(imageNode, panel.domNode, 'first');
			}

			var self = this;
			connect.connect(panel, 'onClick', function(e) {
				var text = '';
				var nodes = e.currentTarget.childNodes;
				for (var i = 0; i < nodes.length; i++) {
					if ('SPAN' == nodes[i].tagName) {
						text = nodes[i].innerHTML;
					}
				}
				self.notificationMessageClicked({id: id, text: text, type: type});
			});

			this._contener.addChild(panel);
			this.Count += aggregatedMessageCount;

			this._updateMessagesCount();
		},

		AddOrUpdate: function(id, text, image, messageItem) {
			var message = dijit.byId(id + '_popup');
			if (!message) {
				this.Add(id, text, image, messageItem);
			} else {
				var nodes = message.domNode.childNodes;
				for (var i = 0; i < nodes.length; i++) {
					if (undefined !== text && 'SPAN' == nodes[i].tagName) {
						nodes[i].innerHTML = text;
					}
					if (undefined !== image && 'IMAGE' == nodes[i].tagName) {
						nodes[i].src = image;
					}
				}
				message.oldChild = false;
			}
		},

		Clear: function() {
			var child = this._contener.getChildren();
			for (var i = 0; i < child.length; i++) {
				this.removeMessage(child[i]);
			}
			this._updateMessagesCount();
		},

		ClearOld: function() {
			var child = this._contener.getChildren();
			for (var i = 0; i < child.length; i++) {
				if (child[i].oldChild) {
					this.removeMessage(child[i]);
				}
			}
			this._updateMessagesCount();
		},

		MakeOld: function() {
			var child = this._contener.getChildren();
			for (var i = 0; i < child.length; i++) {
				child[i].oldChild = true;
			}
		},

		Remove: function(id) {
			var message = dijit.byId(id + '_popup');
			if (message) {
				this.removeMessage(message);
			} else {
				return false;
			}
			this._updateMessagesCount();
			return true;
		},

		removeMessage: function(messageNode) {
			this.Count -= messageNode.aggregatedMessageCount;
			this._contener.removeChild(messageNode);
			messageNode.destroy(true);
		},

		notificationMessageClicked: function(sender) {
			dijit.popup.close();
			if (!this._isMessagesCell()) {
				this.Clear();
			}
		}
	});
});
