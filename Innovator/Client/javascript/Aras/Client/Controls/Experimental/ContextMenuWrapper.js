﻿function ContextMenuWrapper(domNode, skipBindDomNode) {
	this.hiddenItems = [];

	this.skipBindDomNode = skipBindDomNode;
	this.menu = new ArasModules.ContextMenu(domNode);
	this.menu.on('contextmenu', function(menuEvent) {
		menuEvent.preventDefault();
		menuEvent.stopPropagation();
	});

	this.collectionMenu = {};
	this.collectionSeparator = {};
}

ContextMenuWrapper.prototype = {
	menu: null,
	collectionMenu: null,
	collectionSeparator: null,
	rowId: null,
	columnIndex: null,

	onItemClick: function(command, rowID, columnIndex) {
		//event click on item menu
	},

	add: function(id, text, rootMenuId, args) {
		var options = {};
		var parentMenu;
		var rootItem;
		var itemProperties;

		// third and fourth arguments can be interchanged, this code provides for this situation
		// must be removed later
		if (typeof(args) == 'boolean') {
			options = {disable: !args};
		} else if (typeof(args) == 'object' && args) {
			options = args;
		} else if (typeof(rootMenuId) == 'object' && rootMenuId) {
			options = rootMenuId;
		}

		if (typeof(rootMenuId) == 'string' && rootMenuId) {
			rootItem = this.collectionMenu[rootMenuId];
		} else if (typeof(args) == 'string' && args) {
			rootItem = this.collectionMenu[args];
		}

		parentMenu = (rootItem && rootItem.item.popup) ? rootItem.item.popup : this.menu;

		itemProperties = {
			label: text || '',
			disabled: Boolean(options.disable),
			icon: options.icon
		};

		parentMenu.data.set(id, itemProperties);

		parentMenu.roots.push(id);
		this.collectionMenu[id] = parentMenu.data.get(id);
		return this.collectionMenu[id];
	},

	addRange: function(itemsArray, subMenu) { //itemsArray - Array,subMenu - String
		var item;
		var id;
		var i;

		for (i = 0; i < itemsArray.length; i++) {
			item = itemsArray[i];
			id = item.id || item.name;

			if (item.subMenu && item.subMenu instanceof Array) {
				this.add(id, item.name, subMenu);
				this.addRange(item.subMenu, id);
			} else if (item.separator) {
				this.addSeparator(subMenu, item.id);
			} else {
				this.add(id, item.name, subMenu, {disable: item.disable, pos: item.pos, onClick: item.onClick, checked: item.checked,
				icon: item.icon, iconClass: item.iconClass});
			}
		}
	},

	setHide: function(id, bool) {
		var item;
		if (bool) {
			item = this.menu.data.get(id);
			if (!item) {
				return;
			}
			this.hiddenItems[id] = item;
			this.menu.data.delete(id);
		} else {
			item = this.hiddenItems[id];
			if (!item) {
				return;
			}
			this.menu.data.set(id, item);
		}
	},

	setChecked: function(id, bool) {
		var item = this.menu.data.get(id);
		if (item) {
			item.checked = bool;
		}
	},

	setLabel: function(id, label) {
		var item = this.menu.data.get(id);
		if (item) {
			item.label = label;
		}
	},

	setDisable: function(id, bool) {
		var item = this.menu.data.get(id);
		if (item) {
			item.disabled = bool;
		}
	},

	addSeparator: function(rootMenuId, id) {
		var separator = {};
		var rootItem = rootMenuId ? this.collectionMenu[rootMenuId].item : null;
		var parentMenu = (rootItem && rootItem.popup) ? rootItem.popup : this.menu;

		if (id) {
			separator.id = id;
		} else {
			id = separator.id;
		}
		this.collectionSeparator[id] = separator;
		parentMenu.data.set(id, separator);
		parentMenu.roots.push(id);
	},

	setHideSeparator: function(id, bool) {
		this.setHide(id, bool);
	},

	getItemCount: function() {
		return this.menu.data.size;
	},

	getItemById: function(id) {
		//TODO: implement appropriate class and return its instance here
		if (id) {
			return this.collectionMenu[id] ? this.collectionMenu[id] : null;
		}
	},

	removeAll: function() {
		this.collectionMenu = {};
		this.collectionSeparator = {};
		this.menu.roots = [];
		this.menu.data.clear();
	},

	show: function(coords, args) {
		var data = this.menu.data;
		this.menu.roots = this.menu.roots.filter(function(id) {
			return data.get(id);
		});
		this.menu.show(coords, args);
	}
};
