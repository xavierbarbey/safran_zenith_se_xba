﻿define(['dojo/_base/declare',
		'dojo/_base/connect',
		'Aras/Client/Controls/Experimental/StatusBar'],

	function(declare, connect, StatusBar) {

		return declare('Aras.Client.Frames.StatusBar', null, {

			contentWindow: null,
			statusbar: null,
			dom: null,
			aras: null,

			constructor: function(args) {
				if (!args.aras) {
					return;
				}
				this.aras = args.aras;
				this.contentWindow = this;
				var self = this;
				if (!document.frames) {
					document.frames = [];
				}

				document.frames.statusbar = this;
				this.dom = args.statusbar._statusbarxml;
				this.statusbar = args.statusbar;

				if (!this.aras.UserNotification) {
					var notificationControl = this.statusbar.getStatusBarCell('messages');
					this.aras.UserNotification = new UserNotificationContainer(notificationControl.GetNotificationControl());
					this.aras.UserNotification.UpdateMessageCollection();
					connect.connect(notificationControl.GetNotificationControl(), 'notificationMessageClicked', function(args) {
						if (args) {
							var itemTypeName = args.type;
							var topWnd = window.TopWindowHelper.getMostTopWindowWithAras();
							var itemNode = topWnd.aras.newItem(itemTypeName);
							var viewMode = 'notification';
							itemNode.setAttribute('id', args.id);
							topWnd.aras.uiShowItemEx(itemNode, viewMode);
						}
					});
				}
				if (!this.PopupNotification) {
					var popupControl = this.statusbar.getStatusBarCell('status');
					this.PopupNotification = popupControl;
				}

				connect.connect(window, 'onunload', function() {
					if (self.aras && self.aras.UserNotification) {
						self.aras.UserNotification.Dispose();
					}
				});
			},

			setStatus: function(id, text, imagepath, imageposition) {
				return this.statusbar.setStatus(id, text, imagepath, imageposition);
			},

			clearStatus: function(id) {
				return this.statusbar.clearStatus(id);
			},

			containsStatusBarCell: function(id) {
				return !!this.statusbar.listBar[id];
			},

			getStatusBarCell: function(id) {
				return this.statusbar.getStatusBarCell(id);
			}
		});
	});
