﻿function MLDialogHelper() {
}

MLDialogHelper.prototype._newItem = function MLDialogHelper_newItem(type, action) {
	if (typeof Item === 'undefined') {
		return new parent.Item(type, action);
	}
	return new Item(type, action);
};

MLDialogHelper.prototype.show = function MLDialogHelper_show(win, aras, itemNd, mlPropertyInfo, isReadOnly, callback) {
	//possible return values
	//undefined. If user closes dialog without changes.
	//null. If user selects "Set Nothing"
	//AML string. "Fake Item AML" with entered properties.
	var mlPropertyName = mlPropertyInfo.name;

	var fillFakeItem = function(itemNd, fakeItem, langs, mlPropertyName) {
		var val, langCode, sessionLangCode = aras.getSessionContextLanguageCode(), sessionLangIndex;
		//perhaps it would be better to compare langs.length and number of translations on a client side. In case of new item
		//there may be redundant request to server. Needs investigation.

		var i18nNodes = [],
			getItemTranslationCallback = function(foundNode) {
				if (foundNode) {
					i18nNodes.push(foundNode);
					fakeItem.setProperty('fake_property_' + i, foundNode.text);
				}
			};
		for (var i = 0; i < langs.length; i++) {
			langCode = aras.getItemProperty(langs[i], 'code');
			if (langCode === sessionLangCode) {
				sessionLangIndex = i;
			}
			aras.getItemTranslation(itemNd, mlPropertyName, langCode, null, getItemTranslationCallback);
		}

		if (i18nNodes.length != langs.length) {
			var itmNd = aras.getItemFromServer(itemNd.getAttribute('type'), itemNd.getAttribute('id'), mlPropertyName, false, '*').node;
			if (itmNd) {
				for (var j = 0; j < langs.length; j++) {
					langCode = aras.getItemProperty(langs[j], 'code');
					val = aras.getItemTranslation(itmNd, mlPropertyName, langCode, '');
					fakeItem.setProperty('fake_property_' + j, val);
				}
			}
		}
		fakeItem.setProperty('fake_property_' + sessionLangIndex, aras.getItemProperty(itemNd, mlPropertyName));
	};

	var fakeForm = this._newItem('Form', ''), fakeField, fakeFormBody, fakeItemType = this._newItem('ItemType', ''), fakeProperty, fakeItem;
	fakeForm.setID('349CD00F696A419EB1CF5812AE7FF5EA');
	fakeForm.setProperty('name', 'fake_MLDialog_349CD00F696A419EB1CF5812AE7FF5EA');
	fakeForm.setProperty('stylesheet', '../styles/default.css');
	fakeFormBody = this._newItem('Body', '');
	fakeForm.addRelationship(fakeFormBody);
	fakeField = this._newItem('Field', '');
	fakeField.setID('fakeHtml_45A45476CCFE4836AC1F2A5AC94D75E1');
	fakeField.setProperty('field_type', 'html');
	fakeField.setProperty('sort_order', '0');
	fakeField.setProperty('x', '0');
	fakeField.setProperty('y', '0');
	fakeField.setProperty('name', 'fake_field_html');
	fakeField.setProperty('html_code', '<script src="../javascript/MLDialog.js"></' + 'script>');
	fakeFormBody.addRelationship(fakeField);

	fakeItemType.setID('5901A03E806B47DA87BC7D4DA6527975');
	fakeItemType.setProperty('name', 'fake_MLDialog_5901A03E806B47DA87BC7D4DA6527975');
	fakeItem = this._newItem(fakeItemType.getProperty('name'), 'add');

	var getLanguagesResultNd = aras.getLanguagesResultNd();
	var langNd, langNds = getLanguagesResultNd.selectNodes('Item[@type=\'Language\']'), lastModifiedOn = '', tmp,
		displayLength = Math.min(parseInt(19 + 4.74 * (mlPropertyInfo.stored_length ? mlPropertyInfo.stored_length : '40')), 1000);
	for (var i = 0; i < langNds.length; i++) {
		langNd = langNds[i];
		tmp = aras.getItemProperty(langNd, 'modified_on');
		if (!tmp) {
			throw new Error('modified_on of language is not specified');
		}
		if (tmp > lastModifiedOn) {
			lastModifiedOn = tmp;
		}
		fakeProperty = this._newItem('Property', '');
		fakeProperty.setID('fake' + i + '_DC0BE44F68724EE3A4F96A709C9B9121');
		fakeProperty.setProperty('data_type', 'string');
		fakeProperty.setProperty('stored_length', mlPropertyInfo.stored_length ? mlPropertyInfo.stored_length : '40');
		fakeProperty.setProperty('name', 'fake_property_' + i);
		fakeProperty.setProperty('pattern', mlPropertyInfo.pattern ? mlPropertyInfo.pattern : '');
		fakeItemType.addRelationship(fakeProperty);

		fakeField = this._newItem('Field', '');
		fakeField.setID('fake' + i + '_45A45476CCFE4836AC1F2A5AC94D75E1');
		fakeField.setProperty('display_length', displayLength);
		fakeField.setProperty('display_length_unit', 'px');
		fakeField.setProperty('field_type', 'text');
		fakeField.setProperty('font_color', '#000000');
		fakeField.setProperty('font_family', 'arial, helvetica, sans-serif');
		fakeField.setProperty('font_size', '8pt');
		fakeField.setProperty('font_weight', 'bold');

		fakeField.setProperty('is_visible', '1');
		fakeField.setProperty('label', aras.getItemProperty(langNd, 'name'));
		fakeField.setProperty('label_position', 'top');
		fakeField.setProperty('propertytype_id', fakeProperty.getID('id'));
		fakeField.setProperty('sort_order', i);
		fakeField.setProperty('tab_index', i + 1000);
		fakeField.setProperty('x', '40');
		fakeField.setProperty('y', 40 + i * 40);
		fakeField.setProperty('name', 'fake_field_' + i);
		fakeFormBody.addRelationship(fakeField);
	}

	//to make dialog generated html properly cached
	fakeItemType.setProperty('modified_on', lastModifiedOn);
	fakeForm.setProperty('modified_on', lastModifiedOn);

	fillFakeItem(itemNd, fakeItem, langNds, mlPropertyName);

	var param = {
		title: aras.getResource('', 'mldialog.ml_entry'),
		formNd: fakeForm.node,
		itemTypeNd: fakeItemType.node,
		aras: aras,
		formType: isReadOnly ? 'view' : 'edit',
		item: fakeItem,
		dialogWidth: displayLength + 2 * 40 + 2 * 7 /*paddings*/,
		dialogHeight: langNds.length * 40 + 2 * 40,
		resizable: true,
		content: 'ShowFormAsADialog.html',
		type: 'Multilingual'
	};

	win.ArasModules.Dialog.show('iframe', param).promise.then(callback);
};

MLDialogHelper.prototype.updateItemByShowResult = function MLDialogHelper_updateItemByShowResult(aras, itemNd, mlPropertyName, showResult) {
	//returns true if filled
	if (showResult === undefined) {
		return false;
	}
	if (!showResult) {
		showResult = '<Item/>';
	}
	var fakeItem = this._newItem('Tmp', '');
	fakeItem.loadAML(showResult);

	var val, langCode, sessionLangCode = aras.getSessionContextLanguageCode();
	var getLanguagesResultNd = aras.getLanguagesResultNd();
	var langNds = getLanguagesResultNd.selectNodes('Item[@type=\'Language\']');

	for (var i = 0; i < langNds.length; i++) {
		langCode = aras.getItemProperty(langNds[i], 'code');
		val = fakeItem.getProperty('fake_property_' + i) || null;
		if (langCode === sessionLangCode) {
			aras.setItemProperty(itemNd, mlPropertyName, val || '', this._applyToCachedItems());
		}
		aras.setItemTranslation(itemNd, mlPropertyName, val, langCode);
	}
	return true;
};

MLDialogHelper.prototype._applyToCachedItems = function MLDialogHelper_applyToCachedItems() {
	return document.applyChanges2All;
};
