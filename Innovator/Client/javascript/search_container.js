﻿// © Copyright by Aras Corporation, 2004-2012.

// Must be initialized on the page search been added.
var searchContainer = null;
var currentSearchMode = null;
var fromImplementationItemTypeName = undefined;

function SearchContainer(itemTypeName, toolbarInfo, gridInfo, menuInfo, searchLocation, searchPlaceholder, requiredProperties) {
	/// <summary>
	///   Mission of the SearchContainer class is to simplify adding search mechanism to any page in the Innovator.
	///   To use search at the page, a new instance of the class must be created.
	///   Class operates by Aras.Client.JS.SearchMode objects.
	/// </summary>
	/// <example>
	///   <code language="html">
	///<![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	///<html>
	///<head>
	///  <title></title>
	///  <link rel="stylesheet" type="text/css" href="../styles/default.css" />
	///
	///  <script type="text/javascript" src="../javascript/include.aspx?classes=ScriptSet6"></script>
	///
	///</head>
	///<body>
	///
	///  <script type="text/javascript">
	///    onload = function onload_handler()
	///    {
	///      searchContainer = new SearchContainer(itemTypeName, toolbar, grid, "My Search Location", searchPlaceholder);
	///      searchContainer.initSearchModesInToolbar();
	///      searchContainer.initSavedSearchesInToolbar();
	///      searchContainer.showAutoSavedSearchMode();
	///    }
	///  </script>
	///
	///  <table id="main_table" border="0" style="width: 100%; height: 100%; table-layout: fixed;"
	///    cellspacing="0" cellpadding="0">
	///    <tr style="height: 28px;">
	///      <td>
	///        <object id="toolbar">
	///        </object>
	///      </td>
	///    </tr>
	///    <tr id="searchPlaceholder" style="display: none; height: 0px;">
	///    </tr>
	///    <!-- IE has bug not allowing to set rows height in % when standards turned on. //
	///    tr_IE_fix_bug_with_height - special style from styles\default.css that helps to
	///    solve this problem.
	///    -->
	///    <tr class="tr_IE_fix_bug_with_height">
	///      <td>
	///        <object id="grid">
	///        </object>
	///      </td>
	///    </tr>
	///  </table>
	///</body>
	///</html>]]>
	/// </code>
	/// </example>
	/// <summary locid="M:J#Aras.Client.JS.SearchContainer.#ctor">
	/// This is summary for constructor.
	/// </summary>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="itemTypeName" type="string" mayBeNull="true">
	///   Name of the ItemType which items you want to search for.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="toolbar" mayBeNull="true" type="BaseComponent">
	///   Toolbar that will be used to operate by search.
	///   Instance of the Aras.Client.Controls.Toolbar. Must be loaded before passing into SearchContainer.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="grid" mayBeNull="true" type="BaseComponent">
	///   Grid to display search results. Also can be used by search mode for generating query (like "Simple" search mode do).
	///   Instance of the Aras.Client.Controls.GridContainer. Must be loaded before passing into SearchContainer.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="menu" mayBeNull="true" type="BaseComponent">
	///   Menu to operate by search.
	///   Instance of the Aras.Client.Controls.MainMenu. Must be loaded before passing into SearchContainer.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="searchLocation" type="string" mayBeNull="false">
	///   Name of the place where search will be used.
	///   This parameter has sence because search behaviour can be different in different search locations.
	///   See "SearchLocations" list for all possible search location values.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="searchPlaceholder" type="Object" mayBeNull="false">
	///   Object at the page which will contain search modes.
	/// </param>
	/// <param locid="M:J#Aras.Client.JS.InnovatorClient.#ctor" name="requiredProperties" type="Object" mayBeNull="true">
	///   Name:Value collection of properties that must be applied to every quiery.
	/// </param>

	if (arguments.length == 0) {
		return;
	}

	this.itemTypeName = itemTypeName;

	function getObject(info) { return info && info.object && info.dojoOfObject ? info.object : info; }
	function getDojo(info) { return info && info.object && info.dojoOfObject ? info.dojoOfObject : window.dojo; }

	this.grid = getObject(gridInfo);
	this._gridDojo = getDojo(gridInfo);

	this.menu = getObject(menuInfo);
	this._menuDojo = getDojo(menuInfo);

	this.toolbar = getObject(toolbarInfo);
	this._toolbarDojo = getDojo(toolbarInfo);

	var grid = this.grid, menu = this.menu, toolbar = this.toolbar;

	this.searchLocation = searchLocation;
	this.searchPlaceholder = searchPlaceholder;
	this.requiredProperties = requiredProperties;
	this.defaultSearchProperties = new Object();
	this.redlineController = null;
	var tmpXmlDocument = aras.createXMLDocument();
	this.searchParameterizedHelper = new SearchParameterizedHelper(tmpXmlDocument);
	this.itemTypeCache = {};

	if (this.searchPlaceholder) {
		var newChildElementMustBeCreated = true;
		this.searchPlaceholderCell = null;
		if (this.searchPlaceholder.childNodes.length > 0) {
			for (var i = 0; i < this.searchPlaceholder.childNodes.length; i++) {
				if (this.searchPlaceholder.childNodes[i].id == 'searchPlaceholderCell') {
					this.searchPlaceholderCell = this.searchPlaceholder.childNodes[i];
					newChildElementMustBeCreated = false;
					break;
				}
			}
		}

		if (newChildElementMustBeCreated) {
			var searchPlaceholderNodeName = this.searchPlaceholder.nodeName.toUpperCase();
			if ('TR' == searchPlaceholderNodeName || 'DIV' == searchPlaceholderNodeName) {
				this.searchPlaceholderCell = this.searchPlaceholder.appendChild(this.searchPlaceholder.ownerDocument.createElement(searchPlaceholderNodeName == 'TR' ? 'td' : 'div'));
				this.searchPlaceholderCell.id = 'searchPlaceholderCell';
			} else {
				this.searchPlaceholderCell = searchPlaceholder;
			}
		}
	}

	// ************************************* Private SearchContainer members *************************************
	var getCriteriaFromAutoSavedSearch = false;
	var onSearchDialogUpdatesQueryExplicitly = false;

	// Variable will store last selected value in SavedSearch comboBox.
	// If user select new savedSearch and it's not compatible with current searchMode, user can cancel switching.
	// Variable will be used to return previously selected search.
	var currentSavedSearchId = '';

	var worldIdentityId = null;
	const searchCollection = {};
	var currentSearchFrame = null;

	function getSearchModeInstance(searchItemId, searchModeId, searchContainer) {
		if (!searchCollection[searchItemId]) {
			const searchModeItem = aras.getSearchMode(searchModeId);
			const newSearchModeName = aras.getItemProperty(searchModeItem, 'name');
			const newSearchModeLabel = aras.getItemProperty(searchModeItem, 'label');

			dojo.eval(aras.getItemProperty(searchModeItem, 'search_handler'));
			const newSearchMode = new window[newSearchModeName](searchContainer, aras);
			newSearchMode.id = searchModeId;
			newSearchMode.name = newSearchModeName;
			newSearchMode.label = newSearchModeLabel || newSearchModeName;
			searchCollection[searchItemId] = newSearchMode;
		}
		return {id: searchItemId, searchMode: searchCollection[searchItemId]};
	}

	function getWorldIdentityId() {
		return 'A73B655731924CD0B027E4F4D5FCC0A9';
	}

	function toolbarOnChangeHandler(tbId) {
		var tbItemId = tbId.getId();
		if (tbItemId == 'search_mode' || tbItemId == 'saved_search') {
			var selected_item = tbId.getSelectedItem();
			if (tbItemId == 'search_mode' && tbId.getEnabled()) {
				searchContainer.showSearchMode(selected_item);
			} else {
				searchContainer._onSavedSearchChange(selected_item);
			}

			searchContainer._updateSearchMenu();
			if (window.onresize) {
				window.onresize();
			}

			if (typeof(searchContainer.grid.RefreshHeight) == 'function') {
				searchContainer.grid.RefreshHeight();
			}

		}
	}

	function toolbarOnClickHandler(tbId) {
		var tbItemId = tbId.getId();
		var searchMustBeRun = false;
		if (tbItemId == 'search') {
			if (!currentSearchMode.setPageNumber(1)) {
				return;
			}
			currentSearchMode.removeCacheItem('itemmax');
			currentSearchMode.removeCacheItem('pagemax');
			currentSearchMode.removeCacheItem('itemsWithNoAccessCount');
			currentSearchMode.removeCacheItem('criteriesHash');
			searchMustBeRun = true;

		} else if (tbItemId == 'newsearch') {
			if (searchContainer) {
				searchContainer.defaultSearchProperties = new Object();
			}
			currentSearchMode.removeCacheItem('itemmax');
			currentSearchMode.removeCacheItem('pagemax');
			currentSearchMode.removeCacheItem('itemsWithNoAccessCount');
			currentSearchMode.removeCacheItem('criteriesHash');
			currentSearchMode.clearSearchCriteria();

		} else if (tbItemId == 'stop_search') {
			stopSearch();

		} else if (tbItemId == 'add_criteria' && currentSearchMode.newCriteriaRow) {
			currentSearchMode.newCriteriaRow();

		} else if (tbItemId == 'select_all') {
			doSelectAll();

		} else if (tbItemId == 'next_page') {
			var currentPage = currentSearchMode.getPageNumber();
			if (!currentSearchMode.setPageNumber((currentPage != -1) ? ++currentPage : 1)) {
				return;
			}
			searchMustBeRun = true;

		} else if (tbItemId == 'prev_page') {
			var currentPage = currentSearchMode.getPageNumber();
			if (!currentSearchMode.setPageNumber((currentPage != -1) ? --currentPage : 1)) {
				return;
			}
			searchMustBeRun = true;
		}

		if (searchMustBeRun) {
			var currentQueryPage = currentSearchMode.getPageNumber();
			var currentQueryPageSize = currentSearchMode.getPageSize();
			var currentQueryMaxRecords = currentSearchMode.getMaxRecords();
			if (toolbar) {
				var tbControl = toolbar.getItem('page_size');
				if (tbControl) {
					var newPageSize = tbControl.getText();
					if (!currentSearchMode.setPageSize(newPageSize)) {
						tbControl.setText('');
						return;
					}
					if (('' == newPageSize && '-1' != currentQueryPageSize) || (currentQueryPageSize != newPageSize)) {
						if (!currentSearchMode.setPageNumber(1)) {
							return;
						}
					}
				}

				tbControl = toolbar.getItem('max_search');
				if (tbControl) {
					var newMaxRecords = tbControl.getText();
					if (!currentSearchMode.setMaxRecords(newMaxRecords)) {
						tbControl.setText('');
						return;
					}
					if ('' == newMaxRecords && '-1' == currentQueryMaxRecords) {
					} else if (currentQueryMaxRecords != newMaxRecords) {
						if (!currentSearchMode.setPageNumber(1)) {
							return;
						}
					}
				}
			}
			searchContainer.runSearch();
		}
	}

	function showIncompatibleAMLPromptDialog(searchModeLabel) {
		const win = aras.getMostTopWindowWithAras(window);
		const dialogMessage = aras.getResource('', 'search_container.aml_is_not_compatible_with_new_search_mode', searchModeLabel);
		return win.ArasModules.Dialog.confirm(dialogMessage, {
			title: aras.getResource('', 'search.warning'),
 			additionalButton: {
 				text: aras.getResource('', 'common.clear_all'),
 				actionName: 'clear'
 			}
		});
	}

	function getSavedSearch(savedSearchId) {
		var savedSearch = null;

		var savedSearches = aras.getSavedSearches(itemTypeName, searchLocation);
		if (savedSearches && savedSearches.length > 0) {
			for (var i = 0; i < savedSearches.length; i++) {
				if (aras.getItemProperty(savedSearches[i], 'id') == savedSearchId) {
					savedSearch = savedSearches[i];
					break;
				}
			}
		}

		return savedSearch;
	}

	function menuOnCheckEventHandler(menuItem) {
		var cmdID = menuItem.getId();

		var searchModes = aras.getSearchModes();
		if (searchModes) {
			for (var i = 0; i < searchModes.length; i++) {
				var modeId = aras.getItemProperty(searchModes[i], 'id');
				if (modeId == cmdID) {
					menuItem.setState(true);
					searchContainer.showSearchMode(cmdID);
				}
			}
		}
	}

	function menu_setControlEnabled(ctrlName, b) {
		if (!menu) {
			return;
		}

		if (b == undefined) {
			b = true;
		}
		try {
			var mi = menu.findItem(ctrlName);
			if (mi) {
				mi.setEnabled(b);
			}
		} catch (excep) {}
	}

	function menu_setControlState(ctrlName, b) {
		if (!menu) {
			return;
		}

		if (b == undefined) {
			b = true;
		}
		try {
			var mi = menu.findItem(ctrlName);
			if (mi) {
				mi.setState(b);
			}
		} catch (excep) {}
	}

	function gridSortEventHandler(columnIdx, asc, ctrl) {
		if (aras.getVariable('SortPages') !== 'true') {
			return false;
		}
		var orderByValue = this.grid_Experimental
			.getSortProps()
			.filter(function(sortProperty) {
				return sortProperty.attribute !== '_newRowMarker' && sortProperty.attribute !== 'uniqueId';
			})
			.map(function(sortProperty) {
				var columnIndex = this.grid_Experimental.parentContainer.getColumnIndex(sortProperty.attribute);
				var itemTypePropertyNode = searchContainer.getPropertyDefinitionByColumnIndex(columnIndex);
				var sortDirection = sortProperty.descending ? 'DESC' : 'ASC';

				return {
					name: aras.getItemProperty(itemTypePropertyNode, 'name'),
					sortDirection: sortDirection
				};
			}, this)
			.map(function(property) {
				return property.name + ' ' + property.sortDirection;
			})
			.join(', ');
		currentSearchMode.setOrderBy(orderByValue);
		searchContainer.runSearch();

		return true;
	}
	// ************************************* Private SearchContainer members *************************************

	// ************************************* Privileged SearchContainer members *************************************
	this._applyRequiredProperties = function(searchAml) {
		if (!this.requiredProperties) {
			return searchAml;
		}

		var query = aras.newQryItem(this.itemTypeName);
		query.loadXML(searchAml);

		var useWildcards = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true');
		for (var propName in this.requiredProperties) {
			var criteria = this.requiredProperties[propName];
			query.setCriteria(propName, criteria, ((useWildcards && /[%|*]/.test(criteria)) ? 'like' : 'eq'));
		}

		return query.dom.xml;
	};

	this._applyDefaultSearchProperties = function(searchAml) {
		if (!this.defaultSearchProperties) {
			return searchAml;
		}

		var currItemType = this._getCurrentItemType();
		if (!currItemType) {
			return searchAml;
		}

		var query = aras.newQryItem(this.itemTypeName);
		query.loadXML(searchAml);

		var useWildcards = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true');

		for (var propName in this.defaultSearchProperties) {
			var propNd = null;
			var propDT = '';
			var propDS = '';
			if (currItemType) {
				propNd = currItemType.selectSingleNode('Relationships/Item[@type=\'Property\' and name=\'' + propName + '\']');
			}

			if (propNd) {
				propDT = aras.getItemProperty(propNd, 'data_type');
				propDS = aras.getItemPropertyAttribute(propNd, 'data_source', 'name');
			}

			var criteria = this.defaultSearchProperties[propName];
			var condition = ((useWildcards && /[%|*]/.test(criteria)) ? 'like' : 'eq');

			var criteriaProp = query.item.selectSingleNode(propName);
			if (!criteriaProp || userMethodColumnCfgs[propName].isFilterFixed) {
				if ('item' == propDT) {
					query.setPropertyCriteria(propName, 'keyed_name', criteria, condition, propDS);
				} else {
					query.setCriteria(propName, criteria, condition);
				}
			}
		}

		return query.dom.xml;
	};

	this._saveSearch = function() {
		/// <summary>
		/// Allows to save current search criteria into SavedSearch.
		/// </summary>
		/// <remarks>
		/// There are two types of SavedSearches : shared (created by administrators and available to everybody) and identity-based(available to a particular identity).
		/// Administrator are able to create\modify all searches in the system;
		/// all other users are able to create\modify only searches which are assigned to their identity with is_alias=true.
		/// This method shows dialog allowing to input label for SavedSearch and specify type of it - shared or identity-based.
		/// </remarks>
		var formNd = aras.getItemByName('Form', 'SavedSearch Save Dialog', 0);
		if (formNd) {
			var param = new Object();
			param.aras = aras;
			param.title = 'Save new SavedSearch';
			param.formId = formNd.getAttribute('id');
			param.item = this._createNewSavedSearch(false, currentSearchMode.getAml(), currentSearchMode.id);

			if (this.toolbar && this.toolbar.isButtonVisible('saved_search')) {
				var savedSearchItem = getSavedSearch(this.toolbar.getItem('saved_search').getSelectedItem());
				if (savedSearchItem && '1' != aras.getItemProperty(savedSearchItem, 'auto_saved')) {
					var savedSearchWithLabels = aras.getItemFromServer('SavedSearch', savedSearchItem.getAttribute('id'), 'label', false, '*').node;
					if (savedSearchWithLabels) {
						var languages = aras.getLanguagesResultNd().selectNodes('Item[@type=\'Language\']');
						for (var i = 0, j = languages.length; i < j; i++) {
							var languageCode = aras.getItemProperty(languages[i], 'code');
							var searchLabel = aras.getNodeTranslationElement(savedSearchWithLabels, 'label', languageCode);
							aras.setNodeTranslationElement(param.item.node, 'label', searchLabel, languageCode);
						}

						param.item.setProperty('owned_by_id', aras.getItemProperty(savedSearchItem, 'owned_by_id'));
						param.item.setProperty('show_on_toc', aras.getItemProperty(savedSearchItem, 'show_on_toc'));
					}
				}
			}

			var width = aras.getItemProperty(formNd, 'width') || 500;
			var height = aras.getItemProperty(formNd, 'height') || 150;
			var updateMainTree = false;
			param.dialogWidth = width;
			param.dialogHeight = height;
			param.content = 'ShowFormAsADialog.html';
			var win = aras.getMostTopWindowWithAras(window);
			(win.main || win).ArasModules.Dialog.show('iframe', param).promise.then(
				function(resultItem) {
					if (resultItem) {
						if (resultItem.getAttribute('action') === 'edit') {
							// This means that such SavedSearch already exists and it must be updated.
							var savedSearchItem = getSavedSearch(aras.getItemProperty(resultItem, 'id'));
							if (savedSearchItem && aras.getItemProperty(savedSearchItem, 'show_on_toc') == 1) {
								updateMainTree = true;
							}
						}

						var res = aras.soapSend('ApplyItem', resultItem.xml);
						if (res.getFaultCode() != 0) {
							aras.AlertError(res);
							return;
						}

						this._updateAutoSavedSearch();
						aras.MetadataCache.RemoveItemById('56E808C94358462EAA90870A2B81AD96');
						this._initSavedSearchesInToolbar();

						var showOnTOC = (aras.getItemProperty(resultItem, 'show_on_toc') == 1);
						if ((updateMainTree && !showOnTOC) || (!updateMainTree && showOnTOC)) {
							var main = aras.getMostTopWindowWithAras(window).main;
							if (main && main.tree.updateTree) {
								main.tree.updateTree();
							}
						}
					}
				}.bind(this)
			);
		}
	};

	this._deleteSearch = function() {
		/// <summary>
		/// Deletes current SavedSearch displayed by SearchContainer.
		/// </summary>
		/// <remarks>
		/// Administrator are able to delete all searches in the system;
		/// all other users are able to delete only searches which are assigned to their identity with is_alias=true.
		/// </remarks>
		if (!this.toolbar) {
			return;
		}

		var savedSearchChoice = this.toolbar.getItem('saved_search');
		if (!savedSearchChoice) {
			return;
		}

		var selected_item_id = savedSearchChoice.getSelectedItem();
		var selected_item_label = savedSearchChoice.getItem(savedSearchChoice.getSelectedIndex());
		var savedSearchItemType = aras.getItemTypeForClient('SavedSearch').node;
		var savedSearchLabel = aras.getItemProperty(savedSearchItemType, 'label');

		var res = aras.confirm(aras.getResource('', 'itemsgrid.delete_confirmation', savedSearchLabel, selected_item_label));
		if (res) {
			// If user clicks “Yes” on it the currently selected saved search is deleted,
			// the drop-down list of saved searches is updated; search criteria (if shown in UI) is cleared
			// and the drop-down list shows that no saved search is currently selected (i.e. displays empty string).

			var savedSearchItem = getSavedSearch(selected_item_id);
			aras.deleteItem('SavedSearch', selected_item_id, true);

			if (savedSearchItem && aras.getItemProperty(savedSearchItem, 'show_on_toc') == 1) {
				var main = aras.getMostTopWindowWithAras(window).main;
				if (main && main.tree.updateTree) {
					main.tree.updateTree();
				}
			}

			currentSearchMode.clearSearchCriteria();
			this._initSavedSearchesInToolbar();
			this._updateAutoSavedSearch();
			this._updateSaveDeleteMenu();
		}
	};

	this._initSearchMenu = function() {
		if (!menu) {
			return;
		}

		menu_setControlEnabled('search', true);

		var searchModes = aras.getSearchModes();
		if (searchModes) {
			for (var i = 0; i < searchModes.length; i++) {
				var sModeId = aras.getItemProperty(searchModes[i], 'id');
				menu_setControlEnabled(sModeId, true);
				menu_setControlState(sModeId, false);
			}
		}

		menu_setControlState(currentSearchMode.id, true);
	};

	this._populateSearchMenu = function() {
		if (!this.menu) {
			return;
		}

		// Section that initializes search modes in Search menu.
		// --------------------------------------------------------//
		var tmp = this.menu.findMenu('search_menu');
		var searchItemId = (this.menu.get("default_package_name_prefix") || "") + "search";
		if (tmp) {
			var runSearchPos = 0;
			var tmpMenu = tmp.getItemAt(runSearchPos);
			while (tmpMenu && !(tmpMenu.getId() === searchItemId || tmpMenu.getId() === "search")) {
				runSearchPos++;
				tmpMenu = tmp.getItemAt(runSearchPos);
			}

			var searchModes = aras.getSearchModes();
			if (searchModes) {
				tmp.addSeparatorItem(++runSearchPos);
				for (var i = 0; i < searchModes.length; i++) {
					var searchMode = searchModes[i];
					var modeId = aras.getItemProperty(searchMode, 'id');
					var modeLabel = aras.getItemProperty(searchMode, 'label');
					if (!modeLabel) {
						modeLabel = aras.getItemProperty(searchMode, 'name');
					}

					tmp.addCheckItem(modeLabel, modeId, false, false, ++runSearchPos);
				}
			}
		}
		// --------------------------------------------------------//
	};

	this._resetSearchMenu = function() {
		if (!this.menu) {
			return;
		}

		var searchModes = aras.getSearchModes();
		if (searchModes) {
			var searchMenu = this.menu.findMenu('search_menu');
			if (searchMenu) {
				searchMenu.deleteItemAt(1);
				for (var i = 0; i < searchModes.length; i++) {
					searchMenu.deleteItem(aras.getItemProperty(searchModes[i], 'id'));
				}
			}
		}

		menu_setControlEnabled('search', false);
		menu_setControlEnabled('saveSavedSearch', false);
		menu_setControlEnabled('deleteSavedSearch', false);
	};

	this._initSearchModesInToolbar = function() {
		/// <summary>
		/// Gets available SearchModes and populates choice with id="search_mode" in search toolbar.
		/// </summary>
		if (!this.toolbar) {
			return;
		}

		var searchModeChoice = this.toolbar.getItem('search_mode');
		if (!searchModeChoice) {
			return;
		}

		searchModeChoice.removeAll();

		var searchModes = aras.getSearchModes();
		if (searchModes) {
			for (var i = 0; i < searchModes.length; i++) {
				var searchModeNd = searchModes[i];
				var modeId = aras.getItemProperty(searchModeNd, 'id');
				var modeLabel = aras.getItemProperty(searchModeNd, 'label');
				if (!modeLabel) {
					modeLabel = aras.getItemProperty(searchModeNd, 'name');
				}

				searchModeChoice.Add(modeId, modeLabel);
			}
		}
	};

	this._initSavedSearchesInToolbar = function() {
		/// <summary>
		/// Gets available savedSearches and populates choice with id="saved_search" in search toolbar.
		/// </summary>
		/// <remarks>
		///   If only auto_saved SavedSearch available, comboBox will be hidden.
		///   If more that one SavedSearch available, auto_saved SavedSearch will be selected by default.
		/// </remarks>
		if (!this.toolbar) {
			return;
		}

		var savedSearchChoice = this.toolbar.getItem('saved_search');
		if (!savedSearchChoice) {
			return;
		}

		savedSearchChoice.removeAll();
		var savedSearches = aras.getSavedSearches(this.itemTypeName, this.searchLocation);

		if ('Search Dialog' == this.searchLocation) {
			var savedSearchesFromMainGrid = aras.getSavedSearches(this.itemTypeName, 'Main Grid');
			if (savedSearchesFromMainGrid && savedSearchesFromMainGrid.length > 0) {
				if (savedSearches) {
					for (var i = 0; i < savedSearchesFromMainGrid.length; i++) {
						if (aras.getItemProperty(savedSearchesFromMainGrid[i], 'auto_saved') != '1') {
							savedSearches.push(savedSearchesFromMainGrid[i]);
						}
					}
				}
			}
		}

		if (!savedSearches || savedSearches.length == 0 || (savedSearches.length == 1 && aras.getItemProperty(savedSearches[0], 'auto_saved') == '1')) {
			this.toolbar.hideItem('saved_search');
			return;
		}

		var sharedSearches = new Array();
		var identityBasedSearches = new Array();
		var autoSavedSearch = null;
		for (var i = 0; i < savedSearches.length; i++) {
			if (aras.getItemProperty(savedSearches[i], 'owned_by_id') == getWorldIdentityId()) {
				sharedSearches.push(savedSearches[i]);
			} else {
				if (aras.getItemProperty(savedSearches[i], 'auto_saved') != '1') {
					identityBasedSearches.push(savedSearches[i]);
				} else {
					autoSavedSearch = savedSearches[i];
				}
			}
		}

		if (sharedSearches.length > 0) {
			for (var i = 0; i < sharedSearches.length; i++) {
				AddSearchToComboBox(sharedSearches[i]);
			}

			if (identityBasedSearches.length > 0) {
				savedSearchChoice.addSeparator();
			}
		}

		for (var i = 0; i < identityBasedSearches.length; i++) {
			AddSearchToComboBox(identityBasedSearches[i]);
		}

		if (!autoSavedSearch) {
			autoSavedSearch = this._getAutoSavedSearch();
		}

		if (autoSavedSearch) {
			var autoSavedId = aras.getItemProperty(autoSavedSearch, 'id');
			AddSearchToComboBox(autoSavedSearch);
			savedSearchChoice.setSelected(autoSavedId);
			this.currentSavedSearchId = autoSavedId;
		}

		function AddSearchToComboBox(savedSearchNd) {
			var searchId = aras.getItemProperty(savedSearchNd, 'id');
			var searchLabel = aras.getItemProperty(savedSearchNd, 'label');
			if (searchLabel == undefined) {
				searchLabel = '';
			}

			savedSearchChoice.Add(searchId, searchLabel);
		}

		if (!this.toolbar.isButtonVisible('saved_search')) {
			this.toolbar.showItem('saved_search');
		} else {
			this.toolbar.refreshToolbar_Experimental();
		}
	};

	this._selectPrevSavedSearch = function() {
		if (!this.toolbar) {
			return;
		}

		var savedSearchChoice = this.toolbar.getItem('saved_search');
		if (savedSearchChoice) {
			savedSearchChoice.setSelected(this.currentSavedSearchId);
		}
	};

	this._onSavedSearchChange = function(savedSearchId) {
		var savedSearch = null;
		var savedSearches = aras.getSavedSearches(this.itemTypeName, this.searchLocation);
		if (savedSearches && savedSearches.length > 0) {
			for (var i = 0; i < savedSearches.length; i++) {
				if (aras.getItemProperty(savedSearches[i], 'id') == savedSearchId) {
					savedSearch = savedSearches[i];
					break;
				}
			}
		}

		if (!savedSearch && 'Search Dialog' == this.searchLocation) {
			var savedSearchesFromMainGrid = aras.getSavedSearches(itemTypeName, 'Main Grid');
			if (savedSearchesFromMainGrid && savedSearchesFromMainGrid.length > 0) {
				for (var i = 0; i < savedSearchesFromMainGrid.length; i++) {
					if (aras.getItemProperty(savedSearchesFromMainGrid[i], 'id') == savedSearchId) {
						savedSearch = savedSearchesFromMainGrid[i];
						break;
					}
				}
			}
		}

		if (!savedSearch) {
			return;
		}

		var sModeToShow = aras.getSearchMode(aras.getItemProperty(savedSearch, 'search_mode'));
		if (!sModeToShow) {
			return;
		}

		var sModeToShowId = aras.getItemProperty(sModeToShow, 'id');
		var criteriaToShow = aras.getItemProperty(savedSearch, 'criteria');
		if (aras.getItemProperty(savedSearch, 'auto_saved') == 1) {
			criteriaToShow = currentSearchMode.getAml();
		}

		if (!criteriaToShow) {
			criteriaToShow = '<Item type=\'' + this.itemTypeName + '\' action=\'get\'/>';
		}

		if (currentSearchMode.name == 'NoUI') {
			this._updateAutoSavedSearch(criteriaToShow);
			this._setAml(criteriaToShow);
			return;
		}

		this.getCriteriaFromAutoSavedSearch = false;

		// If selected saved search has the same mode as the search mode currently set in UI then the selected search is rendered in UI.
		if (sModeToShowId == currentSearchMode.id) {
			var switchToNewMode = 'ok';
			var callback = function(swichMode) {
				switchToNewMode = swichMode || switchToNewMode;
				if ('ok' == switchToNewMode || 'clear' == switchToNewMode) {
					this.currentSavedSearchId = savedSearchId;
					if ('clear' == switchToNewMode) {
						currentSearchMode.clearSearchCriteria();
						criteriaToShow = currentSearchMode.getAml();
					}

					this._setAml(criteriaToShow);
					this._updateAutoSavedSearch(criteriaToShow);
				} else {
					this._selectPrevSavedSearch();
				}
			}.bind(this);
			var testResult = currentSearchMode.testAmlForCompatibility(criteriaToShow);
			if (currentSearchMode.name === 'Simple' && !testResult) {
				var switchToAml = aras.confirm(aras.getResource('', 'search_container.aml_is_not_compatible_with_simple_search_mode'));
				if (switchToAml) {
					this.getCriteriaFromAutoSavedSearch = true;
					this.currentSavedSearchId = savedSearchId;
					this._updateAutoSavedSearch(criteriaToShow);
					this.showSearchMode('BEF4CDA54AA74362A2C40BD530D4D9DD'); //Id of AML search mode
				} else {
					this.isSelectPrevSavedSearch = true;
					this._selectPrevSavedSearch();
					this.isSelectPrevSavedSearch = false;
				}
				return;
			}
			if (!testResult) {
				switchToNewMode = 'cancel';
				if (!currentSearchMode.isValidAML) {
					showIncompatibleAMLPromptDialog(currentSearchMode.label)
					.then(callback);
				} else {
					callback();
				}
			} else {
				callback();
			}
		} else {
			if (currentSearchMode.testAmlForCompatibility(criteriaToShow)) {
				this.currentSavedSearchId = savedSearchId;
				this._updateAutoSavedSearch(criteriaToShow);
				this._setAml(criteriaToShow);
			} else if (!this.isSelectPrevSavedSearch) {
				if (currentSearchMode.isValidAML) {
					this._selectPrevSavedSearch();
					return;
				}

				var currentSearchModeItem = aras.getSearchMode(currentSearchMode.id);
				var curModeLabel = aras.getItemProperty(currentSearchModeItem, 'label');
				var newModeLabel = aras.getItemProperty(sModeToShow, 'label');
				var bSwitchToNewSearchMode = aras.confirm(aras.getResource('', 'search_container.aml_is_not_compatible_with_new_search_mode2', curModeLabel, newModeLabel));
				if (bSwitchToNewSearchMode) {
					this.getCriteriaFromAutoSavedSearch = true;
					this.currentSavedSearchId = savedSearchId;
					this._updateAutoSavedSearch(criteriaToShow);
					this.showSearchMode(sModeToShowId);
				} else {
					this.isSelectPrevSavedSearch = true;
					this._selectPrevSavedSearch();
					this.isSelectPrevSavedSearch = false;
				}
			}
		}
	};

	this._setAml = function(criteriaToShow) {
		criteriaToShow = this._applyDefaultSearchProperties(criteriaToShow);
		currentSearchMode.setAml(criteriaToShow);
		currentSearchMode.setSelect(aras.getSelectCriteria(getCurrentItemTypeId(), this.searchLocation == 'Relationships Grid', this.getVisibleXProps()));
	};

	function getCurrentItemTypeId() {
		return aras.getItemTypeId(itemTypeName);
	}

	this._transformAml = function(searchAML, indent) {
		/// <summary>
		/// Causes child elements to be indented.
		/// </summary>
		/// <param name="searchAML">AML to indent.</param>
		tmpXmlDocument.loadXML(searchAML);

		var stylesheet = aras.createXMLDocument();
		stylesheet.loadXML(
			'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' +
			'  <xsl:output method="xml" indent="' + (indent ? 'yes' : 'no') + '" omit-xml-declaration="yes"/>' +
			(indent ? '  ' : '  <xsl:strip-space elements="*" />') +
			'  <xsl:template match="*">' +
			'    <xsl:copy>' +
			'      <xsl:copy-of select="@*"/>' +
			'      <xsl:apply-templates/>' +
			'    </xsl:copy>' +
			'  </xsl:template>' +
			'  <xsl:template match="comment()|processing-instruction()">' +
			'    <xsl:copy/>' +
			'  </xsl:template>' +
			'</xsl:stylesheet>');

		return tmpXmlDocument.transformNode(stylesheet);
	};

	this._onSearchDialog = function SearchContainer_private_onSearchDialog(searchQueryAML) {
		var resItem = this._fireSearchEvents(searchQueryAML, true);
		searchQueryAML = resItem.item.xml;

		if (this._onSearchDialogEventMustBeInvoked()) {
			if (this.toolbar) {
				var searchModeChoice = this.toolbar.getItem('search_mode');
				var savedSearchChoice = this.toolbar.getItem('saved_search');
				if (!resItem.item.getAttribute('disableSearchMode')) {
					if (searchModeChoice) {
						searchModeChoice.disable();
					}
				}
				if (savedSearchChoice) {
					savedSearchChoice.disable();
				}
			}

			var searchModeToShow = '';
			if (resItem.item.getAttribute('idlist')) {
				searchModeToShow = 'NoUI';
				this.forceAutoSearch = true;
				if (this.toolbar) {
					var newSearchButton = this.toolbar.getItem('newsearch');
					if (newSearchButton) {
						//because user cannot clear search criteria when idlist is specified
						newSearchButton.disable();
					}
				}
			} else {
				searchModeToShow = this.defaultSearchMode;
			}

			if (resItem.item.getAttribute('searchMode')) {
				searchModeToShow = resItem.item.getAttribute('searchMode');
			}

			var sMode = this._getSearchModeByName(searchModeToShow);
			if (sMode) {
				var sModeId = aras.getItemProperty(sMode, 'id');
				if (sModeId != currentSearchMode.id) {
					this._updateAutoSavedSearch(searchQueryAML);
					this._onSearchModeChange(sModeId, true, resItem);
					return;
				}

				currentSearchMode.onSearchDialogUpdatesQueryExplicitly = onSearchDialogUpdatesQueryExplicitly;
				currentSearchMode.userMethodColumnCfgs = userMethodColumnCfgs;
			}
			if (searchQueryAML) {
				this._updateAutoSavedSearch(searchQueryAML);
				this._setAml(searchQueryAML);
			}
		}
	};

	this._onDefaultSearch = function SearchContainer_private_onDefaultSearch(searchQueryAML) {
		var resItem = this._fireSearchEvents(searchQueryAML, true);
		if (resItem.item) {
			searchQueryAML = resItem.item.xml;
		}

		if (searchQueryAML) {
			if (this._getSearchQueryAML() !== searchQueryAML) {
				this._updateAutoSavedSearch(searchQueryAML);
				this._setAml(searchQueryAML);
			}
		}
	};

	this._getSearchQueryAML = function SearchContainer_private_getSearchQueryAML(searchEventsResult) {
		var searchQueryAML;
		var re = new RegExp('^' + getCurrentItemTypeId() + '-');
		if (searchEventsResult) {
			searchQueryAML = searchEventsResult.item.xml;
		} else if (currentSearchMode && currentSearchFrame && currentSearchFrame.id.search(re) != -1 && !this.getCriteriaFromAutoSavedSearch) {
			searchQueryAML = currentSearchMode.getAml();
		} else {
			var autoSavedSearch = this._getAutoSavedSearch();
			searchQueryAML = aras.getItemProperty(autoSavedSearch, 'criteria');
		}
		return searchQueryAML;
	};

	this._onSearchModeChange = function SearchContainer_private_onSearchModeChange(searchModeId, isRecursiveCall, searchEventsResult) {
		if (this.redlineController && this.redlineController.IsRedlineCanBeDisable()) {
			this.redlineController.DisableRedline();
		}

		var newFrameId = getCurrentItemTypeId() + '-' + searchModeId;
		if (currentSearchFrame && currentSearchFrame.id == newFrameId) {
			return;
		}
		var newSearchIFrame = getSearchModeInstance(newFrameId, searchModeId, this);
		var newSearchMode = newSearchIFrame.searchMode;
		var searchQueryAML = this._getSearchQueryAML(searchEventsResult);
		var callback = function(swichMode) {
			var switchToNewMode = swichMode;
			if (switchToNewMode == 'ok' || switchToNewMode == 'clear') {
				// Continue – convert whatever is possible but drop conditions that could not be converted;
				// Clear – switch to Simple mode but clear all currently set conditions.

				// In AML search mode we need to indent AML, to make it readable for user.
				var amlSearchMode = this._getSearchModeByName('Aml');
				searchQueryAML = this._transformAml(searchQueryAML, (amlSearchMode && aras.getItemProperty(amlSearchMode, 'id') == searchModeId));

				// If switch to new search mode accepted, call onEndSearchMode method for currently loaded search mode.
				if (currentSearchMode && currentSearchMode.onEndSearchMode) {
					currentSearchMode.onEndSearchMode();
				}

				// Hide current search modes in search container.
				if (currentSearchMode && currentSearchMode.hide) {
					currentSearchMode.hide();
				} else if (currentSearchFrame && currentSearchFrame.style) {
					currentSearchFrame.style.display = 'none';
				}

				currentSearchMode = newSearchMode;
				currentSearchFrame = newSearchIFrame;
				// Show current search mode
				if (currentSearchMode.show) {
					currentSearchMode.show();
				} else if (currentSearchFrame.style) {
					currentSearchFrame.style.display = '';
				}

				// Call onStartSearchMode method for new search mode. Must be initialized by searchContainer object.
				if (currentSearchMode.onStartSearchMode) {
					currentSearchMode.onStartSearchMode(this);
				}

				if (switchToNewMode == 'clear') {
					currentSearchMode.clearSearchCriteria();
					searchQueryAML = currentSearchMode.getAml();
				}

				currentSearchMode.onSearchDialogUpdatesQueryExplicitly = onSearchDialogUpdatesQueryExplicitly;
				currentSearchMode.userMethodColumnCfgs = userMethodColumnCfgs;
				this._setAml(searchQueryAML);
				if (!onSearchDialogUpdatesQueryExplicitly) {
					this._updateAutoSavedSearch(searchQueryAML);
				}
			} else if (switchToNewMode == 'cancel' || !switchToNewMode) {
				// Cancel – do not switch mode;

				/*
				If rendering of the search is not possible in the lower complexity mode then a warning dialog appears:
				“The selected search could not be shown in {xxx} mode. Would you like to switch to {yyy} search mode?”
				If user says “Yes” then UI search mode is changed and selected search is rendered in UI;
				if user says “No” then UI remains unchanged (i.e. UI still shows old search criteria).
				*/
				var searchModeChoice = this.toolbar.getItem('search_mode');
				if (!currentSearchFrame || !currentSearchMode) {
					var simpleSearchMode = this._getSearchModeByName(this.defaultSearchMode);
					if (simpleSearchMode) {
						this.showSearchMode(aras.getItemProperty(simpleSearchMode, 'id'));
					}

					return;
				} else if (searchModeChoice) {
					searchModeChoice.setSelected(currentSearchMode.id);
				}
			}

			if (this.toolbar) {
				var searchModeChoice = this.toolbar.getItem('search_mode');
				if (searchModeChoice && searchModeChoice.getSelectedItem() != currentSearchMode.id) {
					searchModeChoice.setSelected(currentSearchMode.id);
				}
			}
		}.bind(this);

		// switchToNewMode can have next values:
		// Cancel – do not switch mode;
		// Continue - convert whatever is possible but drop conditions that could not be converted;
		// Clear - switch to Simple mode but clear all currently set conditions.
		if (searchQueryAML != undefined) {
			// Check AML compatibility with new search mode
			if (onSearchDialogUpdatesQueryExplicitly || newSearchMode.testAmlForCompatibility(searchQueryAML)) {
				callback('ok');
			} else {
				showIncompatibleAMLPromptDialog(newSearchMode.label)
				.then(callback).then(this._updateSearchMenu.bind(this));
			}
		} else {
			callback('cancel');
		}
	};

	this.attachEvents = [];
	this.toolbarEvents = [];

	this._attachEventHandlersToControls = function() {
		var tmpDojo, connectedEvent;
		if (this.menu) {
			tmpDojo = this._menuDojo;
			connectedEvent = tmpDojo.connect(this.menu, 'onCheck', menuOnCheckEventHandler);
			this.attachEvents.push({dojo: tmpDojo, func: connectedEvent});
		}
		if (this.grid) {
			tmpDojo = this._gridDojo;
			connectedEvent = tmpDojo.connect(this.grid, 'gridSort', gridSortEventHandler);
			this.attachEvents.push({ dojo: tmpDojo, func: connectedEvent });
		}
		if (this.toolbar) {
			if (this.toolbar._toolbar) {
				var self = this;
				this.toolbarEvents.push(this.toolbar._toolbar.on('change', function(itemId) {
					var item = self.toolbar.getItem(itemId);
					if (!item || !item.getEnabled()) {
						return;
					}
					toolbarOnChangeHandler(item);
				}));
				this.toolbarEvents.push(this.toolbar._toolbar.on('click', function(itemId) {
					var item = self.toolbar.getItem(itemId);
					if (!item || !item.getEnabled()) {
						return;
					}
					toolbarOnClickHandler(item);
				}));
			} else {
				tmpDojo = this._toolbarDojo;
				connectedEvent = tmpDojo.connect(this.toolbar, 'onClick', toolbarOnClickHandler);
				this.attachEvents.push({dojo: tmpDojo, func: connectedEvent});

				connectedEvent = tmpDojo.connect(this.toolbar, 'onChange', toolbarOnChangeHandler);
				this.attachEvents.push({dojo: tmpDojo, func: connectedEvent});
			}
		}
	};

	this._removeEventHandlersFromControls = function() {
		for (var i = 0; i < this.attachEvents.length; i++) {
			this.attachEvents[i].dojo.disconnect(this.attachEvents[i].func);
		}
		this.attachEvents = [];
		for (var i = 0; i < this.toolbarEvents.length; i++) {
			this.toolbarEvents[i]();
		}
		this.toolbarEvents = [];
	};

	this.removeIFramesCollection = function() {
		Object.keys(searchCollection).forEach(function(id) {
			const searchMode = searchCollection[id];
			if (searchMode.remove) {
				searchMode.remove();
			}
		})
	};

	this._updateAutoSavedSearch = function(searchQueryAML) {
		if (!searchQueryAML && !currentSearchMode) {
			return;
		}

		if (this.redlineController && this.redlineController.isRedlineActive) {
			return;
		}

		var autoSavedSearch = this._getAutoSavedSearch();
		if (!autoSavedSearch) {
			return;
		}

		var setEdit = false;

		if (currentSearchMode && currentSearchMode.id != aras.getItemProperty(autoSavedSearch, 'search_mode')) {
			// if we pass id as a param, setItemProperty method will send aml request to server."
			// It's because setItemProperty need KeyedName. That's why we need to get SearchMode Item from cache and pass it to setItemProperty
			var currentSearchModeItem = aras.getSearchMode(currentSearchMode.id);
			aras.setItemProperty(autoSavedSearch, 'search_mode', currentSearchModeItem);
			setEdit = true;
		}

		if (searchQueryAML == undefined) {
			searchQueryAML = currentSearchMode.getAml();
		}
		if (searchQueryAML != undefined) {
			if (this._onSearchDialogEventMustBeInvoked()) {
				if (onSearchDialogUpdatesQueryExplicitly) {
					var tmpQueryItm = aras.newQryItem(this.itemTypeName);
					tmpQueryItm.loadXML(searchQueryAML);
					tmpQueryItm.removeAllCriterias();
					tmpQueryItm.item.removeAttribute('idlist');
					tmpQueryItm.item.removeAttribute('disableSearchMode');
					tmpQueryItm.item.removeAttribute('searchMode');
					searchQueryAML = tmpQueryItm.item.xml;
				} else {
					tmpXmlDocument.loadXML(searchQueryAML);
					tmpXmlDocument.documentElement.removeAttribute('idlist');
					searchQueryAML = tmpXmlDocument.xml;
				}
			}

			if (aras.getItemProperty(autoSavedSearch, 'criteria') != searchQueryAML) {
				aras.setItemProperty(autoSavedSearch, 'criteria', searchQueryAML);
				setEdit = true;
			}
		}

		if (setEdit) {
			autoSavedSearch.setAttribute('action', 'edit');
		}
	};

	this._onSearchDialogEventMustBeInvoked = function() {
		if (this.searchLocation == 'Search Dialog' && sourceItemTypeName && sourcePropertyName) {
			var sourceItemTypeNd = aras.getItemTypeForClient(sourceItemTypeName, 'name');

			if (!sourceItemTypeNd || !sourceItemTypeNd.node) {
				return false;
			}

			return (sourceItemTypeNd.node.selectNodes
				('Relationships/Item[@type="Property" and name="' + sourcePropertyName + '"]/' +
				'Relationships/Item[@type="Grid Event" and grid_event="onsearchdialog"]/related_id/Item[@type="Method"]').length > 0);
		}
		return false;
	};

	this._updateSearchMenu = function() {
		if (!menu || !currentSearchMode) {
			return;
		}

		this._initSearchMenu();
		this._updateSaveDeleteMenu();
	};

	this._updateSaveDeleteMenu = function(selected_item) {
		var canDeleteSavedSearch = false;
		var canSaveSavedSearch = false;

		if (currentSearchMode.name != 'NoUI') {
			canSaveSavedSearch = true;

			if (this.toolbar && this.toolbar.isButtonVisible('saved_search')) {
				if (selected_item == undefined) {
					selected_item = this.toolbar.getItem('saved_search').getSelectedItem();
				}
				var autoSavedSearch = this._getAutoSavedSearch();
				if (aras.getItemProperty(autoSavedSearch, 'id') !== selected_item) {
					var savedSearch = getSavedSearch(selected_item);
					if (savedSearch) {
						canDeleteSavedSearch = aras.getPermissions('can_delete', selected_item, undefined, 'SavedSearch');
					}
				}
			}
		}

		this._setMenuEnabled('saveSavedSearch', canSaveSavedSearch);
		this._setMenuEnabled('deleteSavedSearch', canDeleteSavedSearch);
	};

	this._setMenuEnabled = function(menuId, enabled) {
		if (!this.menu) {
			return;
		}

		menu_setControlEnabled(menuId, enabled);
	};

	this._getSearchModeByName = function(sModeName) {
		if (!sModeName) {
			return null;
		}

		var modes = aras.getSearchModes();
		if (modes) {
			for (var i = 0; i < modes.length; i++) {
				if (aras.getItemProperty(modes[i], 'name') == sModeName) {
					return modes[i];
				}
			}
		}

		return null;
	};

	this._getAutoSavedSearch = function() {
		var autoSavedSearch = aras.getSavedSearches(this.itemTypeName, this.searchLocation, true);
		if (!autoSavedSearch || !autoSavedSearch.length) {
			//  If no auto_saved SavedSearch was found - create default new with
			//  auto_saved = 1
			//  owned_by_id = currently logged is_alias identity id.
			//  managed_by_id = currently logged is_alias identity id.
			//  search mode = this.defaultSearchMode
			var searchModeId;
			var sMode = this._getSearchModeByName(this.defaultSearchMode);
			if (sMode) {
				searchModeId = aras.getItemProperty(sMode, 'id');
			}
			autoSavedSearch = this._createNewSavedSearch(true, this._getDefaultSearchQueryAML(), searchModeId);
			autoSavedSearch = autoSavedSearch.apply();
			if (!autoSavedSearch.isEmpty() && !autoSavedSearch.isError()) {
				autoSavedSearch = autoSavedSearch.dom.selectSingleNode(aras.XPathResult('/Item'));
				autoSavedSearch = aras.getSavedSearches(this.itemTypeName, this.searchLocation, true, autoSavedSearch.getAttribute('id'));
			} else {
				aras.AlertError(autoSavedSearch, window);
				autoSavedSearch = null;
			}
		}
		if (autoSavedSearch) {
			autoSavedSearch = autoSavedSearch[0];
		}
		return autoSavedSearch;
	};

	this._createNewSavedSearch = function SearchContainer_private_createNewSavedSearch(isAutoSaved, criteria, searchModeId) {
		/// <summary>
		/// Creates but DOESN'T save an instance of SavedSearch.
		/// </summary>
		/// <param name="isAutoSaved" type="boolean">If specified, criteria will be populated with default_search property values.</param>
		/// <param name="criteria" type="boolean">AML query to store as criteria in the created SavedSearch instance.</param>
		/// <returns type="Item" mayBeNull="false">IOM Item</returns>

		//  If no auto_saved SavedSearch was found - create default new with
		//  auto_saved = 1
		//  owned_by_id = currently logged is_alias identity id.
		//  default search mode must be this.defaultSearchMode
		var aliasIdentityId = aras.getIsAliasIdentityIDForLoggedUser();

		var item = new Item('SavedSearch', 'add');
		item.setProperty('itname', this.itemTypeName);
		item.setProperty('auto_saved', (isAutoSaved ? '1' : '0'));
		item.setProperty('location', this.searchLocation);
		item.setProperty('owned_by_id', aliasIdentityId);
		item.setProperty('managed_by_id', aliasIdentityId);
		item.setProperty('criteria', criteria);
		if (searchModeId) {
			item.setProperty('search_mode', searchModeId);
		}
		return item;
	};

	this._getDefaultSearchQueryAML = function() {
		/// <summary>
		/// Generates default search aml for specified itemType.
		/// </summary>
		/// <returns type="string" mayBeNull="false">Returns string like <Item type="ItemTypeName" action="get" select="..."></Item></returns>
		var currItemType = this._getCurrentItemType();
		if (!currItemType) {
			return '';
		}

		var itTypeName = aras.getItemProperty(currItemType, 'name');
		var itTypeId = aras.getItemProperty(currItemType, 'id');
		var newCriteriaItem = aras.newQryItem(itTypeName, 'get');

		newCriteriaItem.setPage(1);
		newCriteriaItem.setSelect(aras.getSelectCriteria(itTypeId, this.searchLocation == 'Relationships Grid', this.getVisibleXProps()));

		var condition = aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true' ? 'like' : 'eq';
		var visiblePropsItms = currItemType.selectNodes(aras.getVisiblePropertiesXPath(itTypeName));
		for (i = 0; i < visiblePropsItms.length; i++) {
			var propName = aras.getItemProperty(visiblePropsItms[i], 'name');
			var defSearch = aras.getItemProperty(visiblePropsItms[i], 'default_search');
			var propDataType = aras.getItemProperty(visiblePropsItms[i], 'data_type');
			if (defSearch) {
				newCriteriaItem.setCriteria(propName, defSearch, ('boolean' == propDataType ? 'eq' : condition));
			}
		}

		return newCriteriaItem.item.xml;
	};

	this._getCurrentItemType = function() {
		var isSearchDialog = (this.searchLocation == 'Search Dialog');
		var currItemType;
		if (this.itemTypeCache[this.itemTypeName]) {
			currItemType = this.itemTypeCache[this.itemTypeName];
		} else {
			currItemType = aras.getItemTypeForClient(this.itemTypeName, 'name');
			this.itemTypeCache[this.itemTypeName] = currItemType;
		}
		if (currItemType.isError()) {
			if (isSearchDialog) {
				window.close();
			}
			return null;
		}

		return currItemType.node;
	};

	this._fireSearchEvents = function(searchAML, withDefaultSearch) {
		var newCriteriaItem = aras.newQryItem(this.itemTypeName);
		newCriteriaItem.loadXML(searchAML);

		if (this.searchLocation != 'Search Dialog') {
			//search events are applicable to Search Dialog only
			return newCriteriaItem;
		}

		var currItemType = this._getCurrentItemType();
		if (!currItemType) {
			return newCriteriaItem;
		}

		if (!this.requiredProperties) {
			this.requiredProperties = new Object();
		}

		if (!searchAML) {
			newCriteriaItem.loadXML(this._getSearchQueryAML());
		}

		var useWildcards = (aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_use_wildcards') == 'true');

		var methodArgs = new Object();
		methodArgs.itemTypeName = itemTypeName;
		methodArgs.QryItem = newCriteriaItem;
		methodArgs.windowContext = searchArguments.aras;
		methodArgs.itemContext = searchArguments.itemContext;
		methodArgs.itemSelectedID = searchArguments.itemSelectedID;
		methodArgs.fromImplementationItemTypeName = fromImplementationItemTypeName; // input argument
		methodArgs.toImplementationItemTypeName = undefined; //output argument

		if (withDefaultSearch != false) {
			var propertiesWithDefaultSearchEvent = currItemType.selectNodes(
				'Relationships/Item[@type="Property"]/' +
				'Relationships/Item[@type="Grid Event" and grid_event="default_search"]/' +
				'related_id/Item[@type="Method"]'
			);

			for (var i = 0; i < propertiesWithDefaultSearchEvent.length; i++) {
				var methodNd = propertiesWithDefaultSearchEvent[i];
				var methodName = aras.getItemProperty(methodNd, 'name');

				var propNd = methodNd.selectSingleNode('../../../..');
				var propName = aras.getItemProperty(propNd, 'name');

				methodArgs.property_name = propName;
				var defSearch = aras.evalItemMethod(methodName, newCriteriaItem.item, methodArgs);
				if (defSearch) {
					if (-1 === this.grid.columns_Experimental.get(propName + '_D', 'index')) {
						this.defaultSearchProperties[propName] = defSearch;
					} else {
						var condition = ((useWildcards && /[%|*]/.test(defSearch)) ? 'like' : 'eq');
						var propDT = aras.getItemProperty(propNd, 'data_type');
						var propDS = aras.getItemPropertyAttribute(propNd, 'data_source', 'name');

						if ('item' == propDT) {
							newCriteriaItem.setPropertyCriteria(propName, 'keyed_name', defSearch, condition, propDS);
						} else {
							newCriteriaItem.setCriteria(propName, defSearch, condition);
						}
					}
				} else {
					newCriteriaItem.removeCriteria(propName);
				}
			}
		}

		if (sourceItemTypeName && sourcePropertyName) {
			var sourceItemTypeNd = aras.getItemTypeForClient(sourceItemTypeName, 'name');
			if (sourceItemTypeNd && sourceItemTypeNd.node) {
				var propertiesWithOnSearchEvent = sourceItemTypeNd.node.selectNodes(
					'Relationships/Item[@type="Property" and name="' + sourcePropertyName + '"]/' +
					'Relationships/Item[@type="Grid Event" and grid_event="onsearchdialog"]/' +
					'related_id/Item[@type="Method"]'
				);

				for (var index = 0; index < propertiesWithOnSearchEvent.length; index++) {
					var methodNd = propertiesWithOnSearchEvent[index];
					var methodName = aras.getItemProperty(methodNd, 'name');

					var propNd = methodNd.selectSingleNode('../../../..');
					var propName = aras.getItemProperty(propNd, 'name');

					var searchAmlBeforeMethod = '';
					if (newCriteriaItem.item != null) {
						searchAmlBeforeMethod = newCriteriaItem.item.xml;
					}
					userMethodColumnCfgs = aras.evalItemMethod(methodName, newCriteriaItem.item, methodArgs);

					if (methodArgs.QryItem.item != null && searchAmlBeforeMethod != methodArgs.QryItem.item.xml) {
						onSearchDialogUpdatesQueryExplicitly = true;
						if (currentSearchMode) {
							currentSearchMode.onSearchDialogUpdatesQueryExplicitly = true;
						}
						newCriteriaItem = methodArgs.QryItem;
					}

					if (!userMethodColumnCfgs || typeof (userMethodColumnCfgs) != 'object') {
						userMethodColumnCfgs = new Object();
					}

					var filteredPropName;
					for (filteredPropName in userMethodColumnCfgs) {
						if (!userMethodColumnCfgs[filteredPropName].filterValue) {
							userMethodColumnCfgs[filteredPropName].filterValue = '';
						}
						if (!userMethodColumnCfgs[filteredPropName].isFilterFixed) {
							userMethodColumnCfgs[filteredPropName].isFilterFixed = false;
						}
					}

					for (filteredPropName in userMethodColumnCfgs) {
						var filterValue = userMethodColumnCfgs[filteredPropName].filterValue;
						this.defaultSearchProperties[filteredPropName] = filterValue;

						if (filterValue) {
							var colNum = this.grid.GetColumnIndex(filteredPropName + '_D');
							if (colNum == -1 && userMethodColumnCfgs[filteredPropName].isFilterFixed) {
								this.requiredProperties[filteredPropName] = filterValue;
							} else {
								var condition = ((useWildcards && /[%|*]/.test(filterValue)) ? 'like' : 'eq');
								var propNd = currItemType.selectSingleNode('Relationships/Item[@type="Property" and name="' + filteredPropName + '"]');
								if (propNd && 'item' == aras.getItemProperty(propNd, 'data_type')) {
									var propDS = aras.getItemPropertyAttribute(propNd, 'data_source', 'name');
									newCriteriaItem.setPropertyCriteria(filteredPropName, 'keyed_name', filterValue, condition, propDS);
								} else {
									newCriteriaItem.setCriteria(filteredPropName, filterValue, condition);
								}
							}
						} else {
							newCriteriaItem.removeCriteria(filteredPropName);
						}
					}
				}
			}
		}

		if (methodArgs.toImplementationItemTypeName && methodArgs.toImplementationItemTypeName !== itemTypeName) {
			var implementationItemType = aras.getItemTypeForClient(itemTypeName, 'name').node;
			if (aras.isPolymorphic(implementationItemType)) {
				var switchToItemType = (itemTypeName == methodArgs.toImplementationItemTypeName) ? implementationItemType : implementationItemType.selectSingleNode('Relationships/Item[@type=\'Morphae\']/related_id/Item[name=\'' + methodArgs.toImplementationItemTypeName + '\']');
				if (switchToItemType) {
					var switchToItemTypeId = switchToItemType.getAttribute('id');
					setTimeout(function() {
						var searchToolbar = searchbar.getActiveToolbar();
						var cb = searchToolbar.getItem('implementation_type');
						cb.SetSelected(switchToItemTypeId);
					}, 1);
				} else {
					aras.AlertError('ItemType \'' + itemTypeName + '\' doesn\'t contain morhae \'' + methodArgs.toImplementationItemTypeName + '\'');
				}
			}
		}
		fromImplementationItemTypeName = itemTypeName;

		return newCriteriaItem;
	};

	this._isSearchCriteriaEmpty = function(searchAML) {
		if (!searchAML) {
			return true;
		}

		var newCriteriaItem = aras.newQryItem(this.itemTypeName);
		newCriteriaItem.loadXML(searchAML);
		return (newCriteriaItem.dom.documentElement.childNodes.length == 0);
	};

	this._showAutoSavedSearchMode = function() {
		/// <summary>
		///   Gets searchMode from auto_saved SavedSearch and loads it into the UI.
		/// </summary>
		/// <remarks>
		///   If no auto_saved SavedSearch found, "Simple" search mode will be shown.
		/// </remarks>
		var searchModeId = '';
		var autoSavedSearch = this._getAutoSavedSearch();

		if (autoSavedSearch && !this._onSearchDialogEventMustBeInvoked()) {
			this.searchCriteria = aras.getItemProperty(autoSavedSearch, 'criteria');
			if (!this._isSearchCriteriaEmpty(this.searchCriteria)) {
				searchModeId = aras.getItemProperty(autoSavedSearch, 'search_mode');
				var sMode = aras.getSearchMode(searchModeId);
				if (!sMode) {
					searchModeId = '';
				}
			}
		}

		if (!searchModeId) {
			// If no SearchMode with specified id found, try to use this.defaultSearchMode search.
			var simpleSearchMode = this._getSearchModeByName(this.defaultSearchMode);
			if (simpleSearchMode) {
				searchModeId = aras.getItemProperty(simpleSearchMode, 'id');
			} else {
				// If this.defaultSearchMode search not found - use any available SearchMode.
				var modes = aras.getSearchModes();
				if (modes && modes.length > 0) {
					searchModeId = aras.getItemProperty(modes[0], 'id');
				}
			}
		}

		this.showSearchMode(searchModeId);
	};
	// ************************************* End of privileged SearchContainer members *************************************
	this._initSearchModesInToolbar();
	this._initSavedSearchesInToolbar();
	this._showAutoSavedSearchMode();
	if (this._onSearchDialogEventMustBeInvoked()) {
		this._onSearchDialog();
	} else {
		this._onDefaultSearch(this.searchCriteria);
	}
}

SearchContainer.prototype.defaultSearchMode = 'Simple';
SearchContainer.prototype.forceAutoSearch = false;

SearchContainer.prototype.getGrid = function SearchContainer_getGrid() {
	/// <summary>
	/// Gets grid object used to display search results.
	/// </summary>
	/// <returns type="Aras.Client.Controls.GridContainer" mayBeNull="true">TreeTable instance used by search.</returns>
	return this.grid;
};

SearchContainer.prototype.getToolbar = function SearchContainer_getToolbar() {
	/// <summary>
	/// Gets toolbar object containing search elements.
	/// </summary>
	/// <returns type="Aras.Client.Controls.Toolbar" mayBeNull="true">Toolbar instance used by search.</returns>
	return this.toolbar;
};

SearchContainer.prototype.getMenu = function SearchContainer_getMenu() {
	/// <summary>
	/// Gets menu object containing search control objects.
	/// </summary>
	/// <returns type="Aras.Client.Controls.MainMenu" mayBeNull="true">MainMenu instance used by search.</returns>
	return this.menu;
};

SearchContainer.prototype.getStyleAttribute = function SearchContainer_getStyleAttribute(strAttributeName) {
	/// <summary>
	/// Retrieves the value of the specified Style attribute of the SearchContainer.
	/// </summary>
	/// <param name="strAttributeName">Name of the attribute.</param>
	/// <returns>
	/// Variant that returns a String, number, or Boolean value as defined by the attribute.
	/// If the attribute is not present, this method returns null.
	/// </returns>
	this.searchPlaceholder.style.getAttribute(strAttributeName, attributeValue);
};

SearchContainer.prototype.setStyleAttribute = function SearchContainer_setStyleAttribute(strAttributeName, attributeValue) {
	/// <summary>
	/// Sets the value of the specified CSS attribute on the DOM element associated with <see cref="SearchContainer"/>.
	/// </summary>
	/// <remarks>
	///   Can be used to set height, visibility, etc. attributes.
	/// </remarks>
	/// <param name="strAttributeName">Name of the attribute.</param>
	/// <param name="attributeValue">Variant that specifies the string, number, or Boolean to assign to the attribute.</param>
	if (strAttributeName != 'height') {
		this.searchPlaceholder.style[strAttributeName] = attributeValue;
	}

	if (this.searchPlaceholderCell) {
		this.searchPlaceholderCell.style[strAttributeName] = attributeValue;
	}

	if (strAttributeName == 'height' && typeof(this.grid.RefreshHeight) == 'function') {
		this.grid.RefreshHeight();
	}
};

SearchContainer.prototype.getRequiredProperties = function SearchContainer_getRequiredProperties() {
	/// <summary>
	/// Gets requiredProperties object for SearchContainer.
	/// </summary>
	/// <returns type="Object">Object representing key/value collection of XPath to properties and values.</returns>
	return this.requiredProperties;
};

SearchContainer.prototype.setRequiredProperties = function SearchContainer_setRequiredProperties(requiredPropertiesObject) {
	/// <summary>
	/// Sets requiredProperties object for SearchContainer.
	/// </summary>
	/// <remarks>
	/// In some cases SearchMode must return query containing immutable properties/values.
	/// This method allows user to specify such immutable criterias.
	/// </remarks>
	/// <example>
	/// <code language="JavaScript">
	/// <![CDATA[
	///   var reqProperties = new Object();
	///   reqProperties["source_id/Item[@type='MySourceItem']/name"] = 'my_item_name';
	///   reqProperties["related_id/Item[@type='MyRelatedItem']/label"] = 'related_item_label';
	///   searchContainer.setRequiredProperties(reqProperties);
	///   // In this case SearchMode.getAml() method will always return query containing next aml:
	///   // ....
	///   // <source_id>
	///   //  <Item type='MySourceItem'>
	///   //    <name>my_item_name</name>
	///   //  </Item>
	///   // </source_id>
	///   // <related_id>
	///   //  <Item type='MyRelatedItem'>
	///   //    <label>related_item_label</label>
	///   //  </Item>
	///   // </related_id>
	///   // ....
	/// ]]>
	/// </code>
	/// </example>
	/// <param name="requiredPropertiesObject">Object representing key/value collection of XPath to properties and values.</param>
	this.requiredProperties = requiredPropertiesObject;
};

SearchContainer.prototype.getPropertyDefinitionByColumnIndex = function SearchContainer_getPropertyDefinitionByColumnIndex(columnIdx) {
	/// <summary>
	/// Gets definition for property corresponding to column in the grid with specified index.
	/// </summary>
	/// <remarks>
	/// Some SearchModes could use information from grid as source data for search query generation.
	/// And in some cases it will be required to know what property corresponds to the column.
	/// </remarks>
	/// <param name="columnIdx" type="Number" integer="true">Index of the column in grid.</param>
	/// <returns type="System.Xml.XmlNode">XmlNode containing property info.</returns>
	var propNd = null;

	if (!this.grid || columnIdx === undefined) {
		return null;
	}

	var columnName = this.grid.GetColumnName(columnIdx);
	var drl = columnName.substr(columnName.length - 2, columnName.length);
	var propertyName = columnName.substr(0, columnName.length - 2);
	if (columnName == 'L') {
		propertyName = 'locked_by_id';
		if (searchLocation == 'Relationships Grid') {
			drl = '_R';
		} else {
			drl = '_D';
		}
	}

	const handleXPropertyDefinition = function(propertyName, propertyXPath, itemTypeNd) {
		const propNd = itemTypeNd.selectSingleNode(propertyXPath);
		aras.setItemProperty(propNd, 'source_id', itemTypeNd.getAttribute('id'));
		aras.setItemPropertyAttribute(propNd, 'source_id', 'name', aras.getItemProperty(itemTypeNd, 'name'));
		return propNd;
	};

	var propXPath;
	var currItemType = this._getCurrentItemType();
	const isXProperty = propertyName.startsWith('xp-');
	if (isXProperty) {
		propXPath = 'Relationships/Item[@type=\'xItemTypeAllowedProperty\']/related_id/Item[@type=\'xPropertyDefinition\' and name=\'' + propertyName + '\']';
	} else {
		propXPath = 'Relationships/Item[@type=\'Property\' and name=\'' + propertyName + '\']';
	}

	if (drl == '_D') {
		if (currItemType) {
			if (isXProperty) {
				propNd = handleXPropertyDefinition(propertyName, propXPath, currItemType);
			} else {
				propNd = currItemType.selectSingleNode(propXPath);
			}
		}
	} else if (drl == '_R') {
		var relshipTypeNd = aras.getRelationshipType(aras.getRelationshipTypeId(this.itemTypeName));
		if (relshipTypeNd && relshipTypeNd.node) {
			var relatedId = aras.getItemProperty(relshipTypeNd.node, 'related_id');
			if (relatedId) {
				var relatedItemTypeNd = aras.getItemTypeDictionary(relatedId, 'id');
				if (relatedItemTypeNd && relatedItemTypeNd.node) {
					if (isXProperty) {
						propNd = handleXPropertyDefinition(propertyName, propXPath, relatedItemTypeNd.node);
					} else {
						propNd = relatedItemTypeNd.node.selectSingleNode(propXPath);
					}
				}
			}
		}
	}

	return propNd;
};

SearchContainer.prototype.getPropertyXPathByColumnIndex = function SearchContainer_getPropertyXPathByColumnIndex(columnIdx) {
	/// <summary>
	/// Gets XPath to property corresponding to the column in the grid with specified index.
	/// </summary>
	/// <remarks>
	/// Some SearchModes could use information from grid as source data for search query generation.
	/// For example "Simple" search mode works in such way.
	/// This method allows to get explicitly path to criteria in search criteria dom.
	/// </remarks>
	/// <param name="columnIdx" type="Number" integer="true">Index of the column in grid.</param>
	/// <returns type="string">XPath to the property in grid; returns undefined in case when there are no grid or columnIdx not specified.</returns>
	var XPath = undefined;

	if (!this.grid || columnIdx === undefined) {
		return undefined;
	}

	var propNd = this.getPropertyDefinitionByColumnIndex(columnIdx);
	if (propNd) {
		XPath = 'Item[@type=\'' + this.itemTypeName + '\']/';
		var columnName = this.grid.GetColumnName(columnIdx);
		var drl = columnName.substr(columnName.length - 2, columnName.length);
		var sourceTypeName = propNd.selectSingleNode('source_id').getAttribute('name');

		if (drl == '_R' || (drl == 'L' && searchLocation == 'Relationships Grid')) {
			XPath += 'related_id/Item[@type=\'' + sourceTypeName + '\']/';
		}

		XPath += aras.getItemProperty(propNd, 'name');
	}

	return XPath;
};

SearchContainer.prototype.showSearchMode = function SearchContainer_showSearchMode(searchModeId) {
	/// <summary>
	/// Shows SearchMode in SearchContainer.
	/// </summary>
	/// <remarks>
	/// Method loads SearchMode into SearchContainer and initialize it with current search criteria.
	/// </remarks>
	/// <param name="searchModeId">Id of search mode to show.</param>
	this._onSearchModeChange(searchModeId);
	this._updateSearchMenu();
};

SearchContainer.prototype.onEndSearchContainer = function SearchContainer_onEndSearchContainer() {
	/// <summary>
	/// This method have sense in case when you work with more than one SearchContainer.
	/// Method resets Search menu and removes handlers from controls for currently selected SearchContainer instance.
	/// </summary>
	this._resetSearchMenu();
	this._updateAutoSavedSearch();
	this._removeEventHandlersFromControls();
};

SearchContainer.prototype.onStartSearchContainer = function SearchContainer_onStartSearchContainer() {
	/// <summary>
	/// This method have sense in case when you work with more than one SearchContainer. If you switch between them you may need to reinitilize controls.
	/// Method reinitializes Search menu and forces controls to work with currently selected SearchContainer instance.
	/// </summary>
	/// <remarks>
	/// You don't need to call this method if constructor of the SearchContainer is called.
	/// </remarks>
	this._populateSearchMenu();
	this._updateSearchMenu();
	this._attachEventHandlersToControls();
};

SearchContainer.prototype.runSearch = function SearchContainer_runSearch() {
	/// <summary>
	/// This method have sense in case when you work with more than one SearchContainer. If you switch between them you may need to reinitilize controls.
	/// Method reinitializes Search menu and forces controls to work with currently selected SearchContainer instance.
	/// </summary>
	/// <remarks>
	/// You don't need to call this method if constructor of the SearchContainer is called.
	/// </remarks>
	var searchAml = currentSearchMode.getAml();
	if (searchAml != undefined) {
		this._updateAutoSavedSearch(searchAml);
		searchAml = this._transformAml(searchAml, false);

		this.searchParameterizedHelper.replaceParametersInQuery(searchAml, null, function(searchAml) {
			if (searchAml != undefined) {
				searchAml = this._applyDefaultSearchProperties(searchAml);
				searchAml = this._applyRequiredProperties(searchAml);
				currQryItem.loadXML(searchAml);
				doSearch_internal();
			}
		}.bind(this));
	}
};

SearchContainer.prototype.runAutoSearch = function SearchContainer_runAutoSearch() {
	/// <summary>
	/// Determines if search should be started automatically. And if should starts the search.
	/// </summary>
	var startSearch = this.forceAutoSearch;

	if (!startSearch) {
		var currItemType = this._getCurrentItemType();
		if (currItemType) {
			startSearch = (aras.getItemProperty(currItemType, 'auto_search') == '1');
		}
	}

	if (startSearch) {
		this.runSearch();
	}
};

SearchContainer.prototype._isNoCountModeForCurrentItemType = function() {
	var countModeExc = aras.getCommonPropertyValue('SearchCountModeException');
	var countModeShowCount = (aras.getCommonPropertyValue('SearchCountMode').toLowerCase() === 'count');
	var itemId = this._getCurrentItemType().attributes.getNamedItem('id').value;
	return countModeShowCount === (countModeExc.indexOf(itemId) > -1);
};

SearchContainer.prototype.getVisibleXProps = function(currItemTypeId) {
	var self = this;
	var colWidths = this.grid.getColWidths().split(';');

	let currItemType;
	if (currItemTypeId) {
		currItemType = aras.getItemTypeForClient(currItemTypeId, 'id');
		if (currItemType && !currItemType.isError()) {
			currItemType = currItemType.node;
		} else {
			return;
		}
	} else {
		currItemType = this._getCurrentItemType();
		currItemTypeId = aras.getItemProperty(currItemType, 'id');
	}

	const currItemTypeName = aras.getItemProperty(currItemType, 'name');
	const isRelationshipType = (aras.getItemProperty(currItemType, 'is_relationship') == '1');

	var currItemTypeXProperties = this.grid.getLogicalColumnOrder().split(';').reduce(function(prev, curr, idx) {
		var propName = self.grid.GetColumnName(idx).slice(0, -2);
		if (propName.startsWith('xp-') && colWidths[idx] !== '0') {
			const propXPath = 'Relationships/Item[@type=\'xItemTypeAllowedProperty\' and not(inactive=\'1\')]/related_id/Item[@type=\'xPropertyDefinition\' and name=\'' + propName + '\']';
			const xProp = currItemType.selectSingleNode(propXPath);
			if (xProp) prev.add(xProp);
		}
		return prev;
	}, new Set());

	const allXProperties = {};
	allXProperties[currItemTypeId] = [];
	currItemTypeXProperties.forEach(function(el) {
		allXProperties[currItemTypeId].push(el);
	});
	if (isRelationshipType) {
		const relType = aras.getRelationshipType(aras.getRelationshipTypeId(currItemTypeName));
		if (relType && !relType.isError()) {
			const relatedTypeId = relType.getProperty('related_id');
			if (relatedTypeId) {
				allXProperties.related = this.getVisibleXProps(relatedTypeId);
			}
		}
	}
	return allXProperties;
};
/*@cc_on
@if (@register_classes == 1)
Type.registerNamespace("Aras");
Type.registerNamespace("Aras.Client");
Type.registerNamespace("Aras.Client.JS");

Aras.Client.JS.SearchContainer = SearchContainer;
Aras.Client.JS.SearchContainer.registerClass("Aras.Client.JS.SearchContainer");
@end
@*/
