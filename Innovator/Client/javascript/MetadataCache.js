﻿function MetadataCache(aras) {

	var scopeVariable = {};
	scopeVariable.aras = aras;
	scopeVariable.cache = aras.IomFactory.CreateItemCache();
	scopeVariable.cacheVariable = null;
	scopeVariable.preloadDates = {};

	scopeVariable.typeInfo = {
		ItemType: {
			typeKey: '3EC33FE3B3C333333E33CF3D33AC33C3',
			getDatesMethod: 'GetItemTypesMetadata',
			getMethod: 'GetItemType'
		},
		RelationshipType: {
			typeKey: '76381576909211E296CE0B586188709B',
			getDatesMethod: 'GetRelationshipTypesMetadata',
			getMethod: 'GetRelationshipType'
		},
		Form: {
			typeKey: '2EC22FE2B2C222222E22CF2D22AC22C2',
			getDatesMethod: 'GetFormsMetadata',
			getMethod: 'GetForm'
		},
		Method: {
			typeKey: '6E02B71E7A6E4FF38A9866C27837906D',
			getDatesMethod: 'GetClientMethodsMetadata',
			getMethod: 'GetClientMethod'
		},
		GetAllClientMethodsMetadata: {
			typeKey: 'F29AC97834104075AA42EE4984AEDC68',
			getDatesMethod: 'GetAllClientMethodsMetadata',
			getMethod: 'GetAllClientMethods'
		},
		List: {
			typeKey: '4EC44FE4B4C444444E44CF4D44AC44C4',
			getDatesMethod: 'GetListsMetadata',
			getMethod: 'GetList'
		},
		Identity: {
			typeKey: '36F92EC1A0CF43C1801F50510D86FEAD',
			getDatesMethod: 'GetIdentitiesMetadata',
			getMethod: 'GetIdentity'
		},
		GetLastModifiedSearchModeDate: {
			typeKey: 'BAD4F21DBF1C41B8B14BD3060FF5E8F5',
			getDatesMethod: 'GetLastModifiedSearchModeDate',
			getMethod: 'GetSearchModes'
		},
		ConfigurableUI: {
			typeKey: '64494CAB13F846A0AD19216DBC3E1980',
			getDatesMethod: 'GetConfigurableUiMetadata',
			getMethod: 'GetConfigurableUi'
		},
		PresentationConfiguration: {
			typeKey: 'BDE98AE974C24A759AF406C526EFD1A7',
			getDatesMethod: 'GetPresentationConfigurationMetadata',
			getMethod: 'GetPresentationConfiguration'
		},
		CommandBarSection: {
			typeKey: '7962E50B66E44BCC9BEDC3ECAE899455',
			getDatesMethod: 'GetCommandBarSectionMetadata',
			getMethod: 'GetCommandBarSection'
		},
		// note that it returns zero-filled (GU)ID if there's no cmf_ContentType for given linked_document_type
		ContentTypeByDocumentItemType: {
			typeKey: 'E76EC697E76248CC8D08FE56C1DB880B',
			getDatesMethod: 'GetContentTypeByDocumentItemTypeMetadata',
			getMethod: 'GetContentTypeByDocumentItemType'
		},
		GetAllXClassificationTreesMetadata: {
			typeKey: '8626C9253E564BDB92C54891E36BC014',
			getDatesMethod: 'GetAllXClassificationTreesMetadata',
			getMethod: 'GetAllXClassificationTrees'
		}
	};

	scopeVariable.findTypeInfoNameById = function(id) {
		var res;
		for (var prop in this.typeInfo) {
			if (this.typeInfo.hasOwnProperty(prop)) {
				if (this.typeInfo[prop].typeKey === id) {
					res = prop;
					break;
				}
			}
		}
		return res;
	};

	scopeVariable.getPreloadDate = function(name) {
		if (!this.preloadDates[name]) {
			this.updatePreloadDates();
		}
		return this.preloadDates[name];
	};

	scopeVariable._getMostTopWindowWithAras = function() {
		return window;
	};

	scopeVariable.extractDateFromCache = function(criteriaValue, criteriaType, itemType) {
		var id = criteriaType == 'id' ? criteriaValue : this.extractIdByName(criteriaValue, itemType);
		return this.extractDateById(id, itemType);
	};

	scopeVariable.extractIdByName = function(name, itemType) {
		var key = this.createCacheKey('MetadataIdsByNameInLowerCase', this.typeInfo[itemType].typeKey);
		var container = this.getItem(key);
		if (!container) {
			this.refreshMetadata(itemType);
			container = this.getItem(key);
		}
		return container ? container.content[name.toLowerCase()] : '';
	};

	scopeVariable.extractNameById = function(id, itemType) {
		var key = this.createCacheKey('MetadataNamesById', this.typeInfo[itemType].typeKey);
		var container = this.getItem(key);
		if (!container) {
			this.refreshMetadata(itemType);
			container = this.getItem(key);
		}
		return container ? container.content[id] : '';
	};

	scopeVariable.extractDateById = function(id, itemType) {
		var key = this.createCacheKey('MetadataDatesById', this.typeInfo[itemType].typeKey);
		var container = this.getItem(key);
		if (!container) {
			this.refreshMetadata(itemType);
			container = this.getItem(key);
		}
		var date = container ? container.content[id] : '';
		if (!date) {
			var currentTime = new Date();
			date = currentTime.getFullYear() + '-' + currentTime.getMonth() + '-' + currentTime.getDate() +
			'T' + currentTime.getHours() + ':' + currentTime.getMinutes() + ':' + currentTime.getSeconds() + '.00';
		}
		return date;
	};

	scopeVariable.refreshMetadata = function(itemType, async) {
		var methodName = this.typeInfo[itemType].getDatesMethod;
		var typeKey = this.typeInfo[itemType].typeKey;

		this.removeById(typeKey, true);
		var self = this;
		var requestURL = this.generateRequestURL(methodName, '', this.getPreloadDate(itemType));
		return this.sendSoapInternal(methodName, requestURL, async ? true : false).then(function(res) {
			self.refreshMetadataInternal(res, typeKey);
		});
	};

	scopeVariable.refreshMetadataInternal = function(res, typeKey) {
		if (res.getFaultCode() !== 0) {
			this.aras.AlertError(res);
			return;
		}
		const metadataItems = JSON.parse(res.getResult().text);

		const obj = metadataItems.reduce(function(res, m) {
			res.MetadataIdsByNameInLowerCase[m.name.toLowerCase()] = m.id;
			res.MetadataNamesById[m.id] = m.name;
			res.MetadataDatesById[m.id] = m.modified_on;
			return res;
		}, {
			'MetadataIdsByNameInLowerCase': {},
			'MetadataNamesById': {},
			'MetadataDatesById': {}
		});

		Object.keys(obj).forEach(function(name) {
			const key = this.createCacheKey(name, typeKey);
			const container = this.aras.IomFactory.CreateCacheableContainer(obj[name], obj[name]);
			this.setItem(key, container);
		}, this);
	};

	scopeVariable.getSoapFromServerOrFromCache = function(methodName, queryParameters) {
		var requestURL = this.generateRequestURL(methodName, queryParameters);
		var res = this.getSoapFromCache(methodName, requestURL);
		if (!res) {
			res = this.sendSoap(methodName, requestURL);
			if (res.getFaultCode() === 0) {
				this.putSoapToCache(methodName, requestURL, res);
			}
		}
		return res;
	};

	scopeVariable.getSoapFromCache = function(methodName, requestURL) {
		var key = this.createCacheKey('MetadataServiceCache', requestURL);
		var container = this.getItem(key);
		return container ? container.content : undefined;
	};

	scopeVariable.putSoapToCache = function(methodName, requestURL, content) {
		var key = this.createCacheKey('MetadataServiceCache', requestURL);
		var container = this._getMostTopWindowWithAras().aras.IomFactory.CreateCacheableContainer(content, content);
		this.setItem(key, container);
	};

	scopeVariable.generateRequestURL = function(methodName, queryParameters, cacheKey) {
		function appendParameter(param, value) {
			if (param !== '') {
				param += '&';
			}
			param += value;
			return param;
		}

		queryParameters = appendParameter(queryParameters, 'database=' + this.aras.getDatabase());
		queryParameters = appendParameter(queryParameters, 'user=' + this.aras.getCurrentLoginName());
		//Language Code (not Locale!!!) is only necessary value for I18N
		queryParameters = appendParameter(queryParameters, 'lang=' + this.aras.getSessionContextLanguageCode());
		queryParameters = appendParameter(queryParameters, 'cache=' + (cacheKey ? cacheKey : '0'));
		return this._getMostTopWindowWithAras().aras.getServerBaseURL() + 'MetaData.asmx/' + methodName + (queryParameters ? '?' + queryParameters : '');
	};

	scopeVariable.sendSoap = function(methodName, requestURL) {
		var finalRetVal;
		this.sendSoapInternal(methodName, requestURL, false).then(function(res) {
			finalRetVal = res;
		});
		return finalRetVal;
	};

	scopeVariable.sendSoapAsync = function(methodName, requestURL) {
		return this.sendSoapInternal(methodName, requestURL, true);
	};

	scopeVariable.sendSoapInternal = function(methodName, requestURL, async) {
		var aras = this.aras;
		var promiseFunction = function(resolve) {
			var finalRetVal;
			ArasModules.soap('', {
				url: requestURL,
				method: methodName,
				restMethod: 'GET',
				async: async
			}).then(function(resultNode) {
				var text;
				if (resultNode.ownerDocument) {
					var doc = resultNode.ownerDocument;
					text = doc.xml || (new XMLSerializer()).serializeToString(doc);
				} else {
					text = resultNode;
				}
				finalRetVal = new SOAPResults(aras, text, false);
				resolve(finalRetVal);
			}, function(xhr) {
				finalRetVal = new SOAPResults(aras, xhr.responseText, false);
				resolve(finalRetVal);
			});
		};

		var promise;
		if (!async) {
			promise = new ArasModules.SyncPromise(promiseFunction);
		} else {
			promise = new Promise(promiseFunction);
		}
		return promise;
	};

	scopeVariable.getItemType = function(criteriaValue, criteriaName) {
		var itemType = 'ItemType';
		var id = criteriaName == 'id' ? criteriaValue : this.extractIdByName(criteriaValue, itemType);
		var date = this.extractDateById(id, itemType);
		var queryParameters = 'id=' + id + '&date=' + date;
		var methodName = this.typeInfo.ItemType.getMethod;
		var requestURL = this.generateRequestURL(methodName, queryParameters);

		var res = this.getSoapFromCache(methodName, requestURL);
		if (!res) {
			res = this.sendSoap(methodName, requestURL);
			if (res.getFaultCode() === 0) {
				this.putSoapToCache(methodName, requestURL, res);

				var rels = res.results.selectNodes(this.aras.XPathResult() + '/Item/Relationships/Item[@type="RelationshipType"]');
				for (var i = 0; i < rels.length; i++) {
					var rel = rels[i];
					var relId = this.aras.getItemProperty(rel, 'id');
					var relDate = this.extractDateById(relId, 'RelationshipType');
					var relQueryParameters = 'id=' + relId + '&date=' + relDate;
					var relRequestURL = this.generateRequestURL(this.typeInfo.RelationshipType.getMethod, relQueryParameters);
					var relRes = new SOAPResults(this.aras, '<Result>' + rel.xml + '</Result>');
					this.putSoapToCache(this.typeInfo.RelationshipType.getMethod, relRequestURL, relRes);
				}
			}
		}
		return res;
	};

	scopeVariable.stdGet = function(typeInfo, itemType, criteriaValue, criteriaName) {
		var id = criteriaName == 'id' ? criteriaValue : this.extractIdByName(criteriaValue, itemType);
		var date = this.extractDateById(id, itemType);
		var queryParameters = 'id=' + id + '&date=' + date;

		return this.getSoapFromServerOrFromCache(typeInfo.getMethod, queryParameters);
	};

	scopeVariable.getRelationshipType = function(criteriaValue, criteriaName) {
		return this.stdGet(this.typeInfo.RelationshipType, 'RelationshipType', criteriaValue, criteriaName);
	};

	scopeVariable.getForm = function(criteriaValue, criteriaName) {
		return this.stdGet(this.typeInfo.Form, 'Form', criteriaValue, criteriaName);
	};

	scopeVariable.getClientMethod = function(criteriaValue, criteriaName) {
		var methodNd = this.getClientMethodNd(criteriaValue, criteriaName);
		if (methodNd) {
			return new SOAPResults(this.aras,'<Result>' + methodNd.xml + '</Result>');
		}
		return this.stdGet(this.typeInfo.Method, 'Method', criteriaValue, criteriaName);
	};

	scopeVariable.getClientMethodNd = function(criteriaValue, criteriaName) {
		var allMethods = this.getAllClientMethods();
		var methodId;
		if (criteriaName != 'id') {
			var idsByNames = this.getItem(this.createCacheKey('MetadataClientMethodsIdsByNames', '307C85932F514F70A81B7A09376A8D6C'));
			if (!idsByNames) {
				return;
			}
			methodId = idsByNames[criteriaValue.toLowerCase()];
		} else {
			methodId = criteriaValue;
		}
		return allMethods[methodId];
	};

	scopeVariable.getAllClientMethods = function() {
		var typeKey = this.typeInfo.GetAllClientMethodsMetadata.typeKey;
		var dateMethodName = this.typeInfo.GetAllClientMethodsMetadata.getDatesMethod;
		var methodName = this.typeInfo.GetAllClientMethodsMetadata.getMethod;

		var date = this.getLastModifiedItemDateFromCache('MetadataLastModifiedClientMethodsDate', typeKey, dateMethodName);
		var requestURL = this.generateRequestURL(methodName, 'date=' + date);
		var res = this.getSoapFromCache(methodName, requestURL);
		if (!res) {
			res = this.sendSoap(methodName, requestURL);
			var methodDict = {};
			if (res.getFaultCode() === 0) {
				var nodes = res.getResult().selectNodes(this.aras.XPathResult('/Item[@type=\'Method\']'));
				if (nodes.length > 0) {
					var idsByNames = {};
					for (var i = 0; i < nodes.length; i++) {
						var id = nodes[i].getAttribute('id');
						idsByNames[nodes[i].selectSingleNode('name').text.toLowerCase()] = id;
						methodDict[id] = nodes[i];
					}
					this.putSoapToCache(methodName, requestURL, methodDict);
					var cacheKey = this.createCacheKey('MetadataClientMethodsIdsByNames', '307C85932F514F70A81B7A09376A8D6C');
					this.setItem(cacheKey, idsByNames);
				}
			}
			res = methodDict;
		}

		return res;
	};

	scopeVariable.getQueryParametersForGetCUI = function(context) {
		// date is single maximal for all CUI config, no any per id/context division
		var date = this.extractDateById('83F725B93D9840E7A4B139E40DCDA8C4', 'ConfigurableUI');
		var complexId = 'item_type_id=' + context.item_type_id + '&location_name=' + context.location_name + '&item_classification=' + context.item_classification;
		if (context.item_id) {
			complexId += '&item_id=' + context.item_id;
		}
		var queryParameters = 'id=' + escape(complexId) + '&date=' + date;

		return queryParameters;
	};

	scopeVariable.getConfigurableUi = function(context) {
		var queryParameters = this.getQueryParametersForGetCUI(context);
		return this.getSoapFromServerOrFromCache(this.typeInfo.ConfigurableUI.getMethod, queryParameters);
	};

	scopeVariable.getConfigurableUiAsync = function(context) {
		var queryParameters = this.getQueryParametersForGetCUI(context);
		var methodName = this.typeInfo.ConfigurableUI.getMethod;
		var requestURL = this.generateRequestURL(methodName, queryParameters);
		var res = this.getSoapFromCache(methodName, requestURL);
		if (!res) {
			return this.sendSoapAsync(methodName, requestURL).then(function(result) {
				if (result.getFaultCode() === 0) {
					this.putSoapToCache(methodName, requestURL, result);
				}
				return result;
			}.bind(this));
		}

		return Promise.resolve(res);
	};

	scopeVariable.stdGetById = function(typeInfo, itemType, id) {
		var date = this.extractDateById(id, itemType);
		return this.getSoapFromServerOrFromCache(typeInfo.getMethod, 'id=' + id + '&date=' + date);
	};

	scopeVariable.getPresentationConfiguration = function(id) {
		return this.stdGetById(this.typeInfo.PresentationConfiguration, 'PresentationConfiguration', id);
	};

	scopeVariable.getCommandBarSection = function(id) {
		return this.stdGetById(this.typeInfo.CommandBarSection, 'CommandBarSection', id);
	};

	scopeVariable.getContentTypeByDocumentItemType = function(id) {
		return this.stdGetById(this.typeInfo.ContentTypeByDocumentItemType, 'ContentTypeByDocumentItemType', id);
	};

	scopeVariable.getList = function(listIds, filterListIds) {
		//listIds - list of ids for which Value type is needed
		//filterListIds - list of ids for which Filter Value type is needed
		var queryParameters; //variable for request params
		var response; //variable with response to check content
		var methodName = this.typeInfo.List.getMethod;

		var result = '<Result>';
		var i;
		//check if list isn't empty
		if (listIds.length !== 0) {
			//for each single element in collection request for metadata to service
			for (i = 0; i < listIds.length; i++) {
				queryParameters = 'id=' + listIds[i] + '&valType=value&date=' + this.extractDateById(listIds[i], 'List');
				response = this.getSoapFromServerOrFromCache(methodName, queryParameters);
				//if fault code was returned we return object with this fault
				if (response.getFaultCode() !== 0) {
					return response;
				}
				result += response.getResultsBody();
			}
		}
		if (filterListIds.length !== 0) {
			//for each single element in collection request for metadata to service
			for (i = 0; i < filterListIds.length; i++) {
				queryParameters = 'id=' + filterListIds[i] + '&valType=filtervalue&date=' + this.extractDateById(filterListIds[i], 'List');
				response = this.getSoapFromServerOrFromCache(methodName, queryParameters);
				//if fault code was returned we return object with this fault
				if (response.getFaultCode() !== 0) {
					return response;
				}
				result += response.getResultsBody();
			}
		}
		result += '</Result>';
		result = new SOAPResults(this.aras, result);
		return result;
	};

	scopeVariable.getIdentity = function(criteriaValue, criteriaName) {
		return this.stdGet(this.typeInfo.Identity, 'Identity', criteriaValue, criteriaName);
	};

	scopeVariable.getSearchModes = function() {
		var typeKey = this.typeInfo.GetLastModifiedSearchModeDate.typeKey;
		var dateMethodName = this.typeInfo.GetLastModifiedSearchModeDate.getDatesMethod;
		var methodName = this.typeInfo.GetLastModifiedSearchModeDate.getMethod;

		var date = this.getLastModifiedItemDateFromCache('MetadataLastModifiedSearchModeDate', typeKey, dateMethodName);
		var queryParameters = 'date=' + date;
		var res = this.getSoapFromServerOrFromCache(methodName, queryParameters);
		if (res.getFaultCode() !== 0) {
			this.aras.AlertError(res);
			var resIOMError = this.aras.newIOMInnovator().newError(res.getFaultString());
			return resIOMError;
		}
		return res.getResult();
	};

	scopeVariable.getAllXClassificationTrees = function() {
		var typeKey = this.typeInfo.GetAllXClassificationTreesMetadata.typeKey;
		var dateMethodName = this.typeInfo.GetAllXClassificationTreesMetadata.getDatesMethod;
		var methodName = this.typeInfo.GetAllXClassificationTreesMetadata.getMethod;

		var date = this.getLastModifiedItemDateFromCache('GetAllXClassificationTreesMetadata', typeKey, dateMethodName);
		var queryParameters = 'date=' + date;
		var res = this.getSoapFromServerOrFromCache(methodName, queryParameters);
		if (res.getFaultCode() !== 0) {
			this.aras.AlertError(res);
			var resIOMError = this.aras.newIOMInnovator().newError(res.getFaultString());
			return resIOMError;
		}
		return res.getResult();
	};

	scopeVariable.getLastModifiedItemDateFromCache = function(cacheKey, typeKey, dateMethodName) {
		var keyDateItems = this.createCacheKey(cacheKey, typeKey);
		var date = this.getItem(keyDateItems);
		if (!date) {
			date = this.getLastModifiedItemDateFromServer(dateMethodName);
			this.removeItemById(typeKey, true);
			this.setItem(keyDateItems, date);
		}
		return date;
	};

	scopeVariable.getLastModifiedItemDateFromServer = function(dateMethodName) {
		return this.getPreloadDate(dateMethodName);
	};

	scopeVariable.updatePreloadDates = function(metadataDates) {
		if (!metadataDates) {
			metadataDates = aras.getMainWindow().arasMainWindowInfo.GetAllMetadataDates;
		}

		for (var i = 0; i < metadataDates.childNodes.length; i++) {
			this.preloadDates[metadataDates.childNodes[i].nodeName] = metadataDates.childNodes[i].text;
		}

	};

	scopeVariable.deleteListDatesFromCache = function() {
		this.removeItemById(this.typeInfo.List.typeKey);
	};

	scopeVariable.deleteFormDatesFromCache = function() {
		this.removeItemById(this.typeInfo.Form.typeKey);
	};

	scopeVariable.deleteClientMethodDatesFromCache = function() {
		this.removeItemById(this.typeInfo.Method.typeKey);
	};

	scopeVariable.deleteAllClientMethodsDatesFromCache = function() {
		this.removeItemById(this.typeInfo.GetAllClientMethodsMetadata.typeKey);
	};

	scopeVariable.deleteITDatesFromCache = function() {
		this.removeItemById(this.typeInfo.ItemType.typeKey);
	};

	scopeVariable.deleteRTDatesFromCache = function() {
		this.removeItemById(this.typeInfo.RelationshipType.typeKey);
	};

	scopeVariable.deleteIdentityDatesFromCache = function() {
		this.removeItemById(this.typeInfo.Identity.typeKey);
	};

	scopeVariable.deleteConfigurableUiDatesFromCache = function() {
		this.removeItemById(this.typeInfo.ConfigurableUI.typeKey);
	};

	scopeVariable.deletePresentationConfigurationDatesFromCache = function() {
		this.removeItemById(this.typeInfo.PresentationConfiguration.typeKey);
	};

	scopeVariable.deleteCommandBarSectionDatesFromCache = function() {
		this.removeItemById(this.typeInfo.CommandBarSection.typeKey);
	};

	scopeVariable.deleteContentTypeByDocumentItemTypeDatesFromCache = function() {
		this.removeItemById(this.typeInfo.ContentTypeByDocumentItemType.typeKey);
	};

	scopeVariable.deleteSearchModeDatesFromCache = function() {
		this.removeItemById(this.typeInfo.GetLastModifiedSearchModeDate.typeKey);
	};

	scopeVariable.deleteXClassificationTreesDates = function() {
		this.removeItemById(this.typeInfo.GetAllXClassificationTreesMetadata.typeKey);
	};

	scopeVariable.getItem = function(key) {
		return this.cache.GetItem(key);
	};

	scopeVariable.removeById = function(id, doNotClearDate) {
		this.cache.RemoveById(id);

		if (!doNotClearDate) {
			var typeInfoName = this.findTypeInfoNameById(id);
			delete this.preloadDates[typeInfoName];
		}
	};

	scopeVariable.setItem = function(key, item) {
		this.cache.SetItem(key, item);
	};

	scopeVariable.getItemsById = function(key) {
		return this.cache.GetItemsById(key);
	};

	scopeVariable.clearCache = function() {
		this.cache.ClearCache();
		this.preloadDates = {};
		this.updatePreloadDates();
	};

	//This function accepts any number of parameters
	scopeVariable.createCacheKey = function() {
		var key = aras.IomFactory.CreateArrayList();
		for (var i = 0; i < arguments.length; i++) {
			key.add(arguments[i]);
		}
		return key;
	};

	scopeVariable.removeItemById = function(id) {
		var mainArasObj = aras.getMainArasObject();
		if (mainArasObj && mainArasObj != aras) {
			mainArasObj.MetadataCache.RemoveItemById(id);
		} else {
			this.removeById(id);
		}
	};

	return {
		GetItemType: function(criteriaValue, criteriaName) {
			return scopeVariable.getItemType(criteriaValue, criteriaName);
		},
		GetRelationshipType: function(criteriaValue, criteriaName) {
			return scopeVariable.getRelationshipType(criteriaValue, criteriaName);
		},
		GetForm: function(criteriaValue, criteriaName) {
			return scopeVariable.getForm(criteriaValue, criteriaName);
		},
		GetClientMethod: function(criteriaValue, criteriaName) {
			return scopeVariable.getClientMethod(criteriaValue, criteriaName);
		},
		GetClientMethodNd: function(criteriaValue, criteriaName) {
			return scopeVariable.getClientMethodNd(criteriaValue, criteriaName);
		},
		GetAllClientMethods: function() {
			return scopeVariable.getAllClientMethods();
		},
		GetList: function(listIds, filterListIds) {
			return scopeVariable.getList(listIds, filterListIds);
		},
		GetApplicationVersion: function() {
			var requestUrl = scopeVariable.generateRequestURL('GetApplicationVersion', '');
			return scopeVariable.sendSoap('GetApplicationVersion', requestUrl);
		},
		GetIdentity: function(criteriaValue, criteriaName) {
			return scopeVariable.getIdentity(criteriaValue, criteriaName);
		},
		GetSearchModes: function() {
			return scopeVariable.getSearchModes();
		},
		GetAllXClassificationTrees: function() {
			return scopeVariable.getAllXClassificationTrees();
		},
		GetItemTypeName: function(id) {
			return scopeVariable.extractNameById(id, 'ItemType');
		},
		GetRelationshipTypeName: function(id) {
			return scopeVariable.extractNameById(id, 'RelationshipType');
		},
		GetFormName: function(id) {
			return scopeVariable.extractNameById(id, 'Form');
		},
		GetItemTypeId: function(name) {
			return scopeVariable.extractIdByName(name, 'ItemType');
		},
		GetRelationshipTypeId: function(name) {
			return scopeVariable.extractIdByName(name, 'RelationshipType');
		},
		GetFormId: function(name) {
			return scopeVariable.extractIdByName(name, 'Form');
		},
		ExtractDateFromCache: function(criteriaValue, criteriaType, itemType) {
			return scopeVariable.extractDateFromCache(criteriaValue, criteriaType, itemType);
		},
		DeleteListDatesFromCache: function() {
			return scopeVariable.deleteListDatesFromCache();
		},
		DeleteFormDatesFromCache: function() {
			return scopeVariable.deleteFormDatesFromCache();
		},
		DeleteClientMethodDatesFromCache: function() {
			return scopeVariable.deleteClientMethodDatesFromCache();
		},
		DeleteAllClientMethodsDatesFromCache: function() {
			return scopeVariable.deleteAllClientMethodsDatesFromCache();
		},
		DeleteITDatesFromCache: function() {
			return scopeVariable.deleteITDatesFromCache();
		},
		DeleteRTDatesFromCache: function() {
			return scopeVariable.deleteRTDatesFromCache();
		},
		DeleteIdentityDatesFromCache: function() {
			return scopeVariable.deleteIdentityDatesFromCache();
		},
		DeleteConfigurableUiDatesFromCache: function() {
			return scopeVariable.deleteConfigurableUiDatesFromCache();
		},
		DeletePresentationConfigurationDatesFromCache: function() {
			return scopeVariable.deletePresentationConfigurationDatesFromCache();
		},
		DeleteCommandBarSectionDatesFromCache: function() {
			return scopeVariable.deleteCommandBarSectionDatesFromCache();
		},
		DeleteXClassificationTreesDates: function() {
			return scopeVariable.deleteXClassificationTreesDates();
		},
		DeleteContentTypeByDocumentItemTypeDatesFromCache: function() {
			return scopeVariable.deleteContentTypeByDocumentItemTypeDatesFromCache();
		},
		DeleteSearchModeDatesFromCache: function() {
			return scopeVariable.deleteSearchModeDatesFromCache();
		},
		GetItem: function(key) {
			return scopeVariable.getItem(key);
		},
		RemoveById: function(id) {
			return scopeVariable.removeById(id);
		},
		SetItem: function(key, item) {
			return scopeVariable.setItem(key, item);
		},
		GetItemsById: function(key) {
			return scopeVariable.getItemsById(key);
		},
		ClearCache: function() {
			return scopeVariable.clearCache();
		},
		CreateCacheKey: function() {
			return scopeVariable.createCacheKey.apply(null, arguments);
		},
		RemoveItemById: function(id) {
			return scopeVariable.removeItemById(id);
		},
		GetConfigurableUi: function(context) {
			return scopeVariable.getConfigurableUi(context);
		},
		GetConfigurableUiAsync: function(context) {
			return scopeVariable.getConfigurableUiAsync(context);
		},
		GetPresentationConfiguration: function(id) {
			return scopeVariable.getPresentationConfiguration(id);
		},
		GetCommandBarSection: function(id) {
			return scopeVariable.getCommandBarSection(id);
		},
		GetContentTypeByDocumentItemType: function(id) {
			///<returns>Zero-filled (GU)ID if there's no cmf_ContentType for given linked_document_type</returns>
			return scopeVariable.getContentTypeByDocumentItemType(id);
		},
		RefreshMetadata: function(itemTypeName) {
			return scopeVariable.refreshMetadata(itemTypeName, true);
		},
		UpdatePreloadDates: function(metadataDates) {
			return scopeVariable.updatePreloadDates(metadataDates);
		}
	};
}
