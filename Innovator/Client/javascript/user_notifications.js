﻿function UserNotificationContainer(notification_control) {
	if (!notification_control) {
		throw new Error(1, 'ArgumentException: notification_control can\'t be undefined');
	}

	this.NotificationControl = notification_control;
	this.CheckInterval = null;
	this.CheckTimeout = null;
	this.constAcknowledge = 'Acknowledge';
	this.constClose = 'Close';

	this.url = parent.aras.getInnovatorUrl() + 'NotificationServer/UserNotifications/ProxyPage.aspx';

	this.UpdateInfoDom = null;
	this.PopupParametersArray = [];
	var variable = parent.aras.commonProperties.MessageCheckInterval;
	if (variable) {
		this.CheckInterval = variable;
	}

	if (!this.CheckInterval) {
		parent.aras.AlertError(parent.aras.getResource('', 'user_notifications.failed_get_interval_variable'), '', '');
		return;
	}

	this.CheckTimeout = setTimeout(function() { parent.aras.UserNotification.StartIterativeUpdateMessageCollection(); }, this.CheckInterval);
}

UserNotificationContainer.prototype.AddMessage = function UserNotificationContainer_AddMessage(id, text, image, messageItem) {
	this.NotificationControl.AddOrUpdate(id, text, image, messageItem);
};

UserNotificationContainer.prototype.RemoveMessage = function UserNotificationContainer_RemoveMessage(id) {
	return this.NotificationControl.Remove(id);
};

UserNotificationContainer.prototype.ClearCollection = function UserNotificationContainer_ClearCollection() {
	this.NotificationControl.Clear();
};

UserNotificationContainer.prototype.MakeAllMessagesOld = function UserNotificationContainer_MakeAllMessagesOld() {
	this.NotificationControl.MakeOld();
};

UserNotificationContainer.prototype.ClearOldMessages = function UserNotificationContainer_ClearOldMessages() {
	this.NotificationControl.ClearOld();
};

UserNotificationContainer.prototype.GetMessageQuery = function UserNotificationContainer_GetMessageQuery() {
	var query_dom = parent.aras.createXMLDocument();

	var dtObj = new Date();
	var yyyy = String(dtObj.getFullYear());
	var h = {
		MM: String('0' + (dtObj.getMonth() + 1)),
		dd: String('0' + dtObj.getDate()),
		hh: String('0' + dtObj.getHours()),
		mm: String('0' + dtObj.getMinutes()),
		ss: String('0' + dtObj.getSeconds())
	};
	for (var k in h) {
		h[k] = h[k].substr(h[k].length - 2);
	}
	var r = yyyy + '-' + h.MM + '-' + h.dd + 'T' + h.hh + ':' + h.mm + ':' + h.ss;

	query_dom.loadXML('<Parameters/>');
	var currentUserIDNode = query_dom.createElement('CurrentUserID');
	var dateTimeNode = query_dom.createElement('DateTime');

	currentUserIDNode.text = parent.aras.getCurrentUserID();
	dateTimeNode.text = r;

	query_dom.documentElement.appendChild(currentUserIDNode);
	query_dom.documentElement.appendChild(dateTimeNode);

	return query_dom.documentElement.xml;
};

UserNotificationContainer.prototype.AsyncCheckNewMessages = function UserNotificationContainer_AsyncCheckNewMessages(getCollectionCallback) {
	var soapController = new SoapController(resultCallbackHandler);
	soapController.NotificationContainer = this;

	var query = this.GetMessageQuery();
	var soap_res = parent.aras.soapSend('GetNotifications', query, this.url, null, soapController);

	function resultCallbackHandler(soap_res) {
		if (this.NotificationContainer) {
			var message_collection = this.NotificationContainer.ProcessSoapResult(soap_res);
			getCollectionCallback(this.NotificationContainer, message_collection);
		}
	}
};

UserNotificationContainer.prototype.SyncGetMessageCollection = function UserNotificationContainer_SyncGetMessageCollection() {
	var query = this.GetMessageQuery();
	var soap_res = parent.aras.soapSend('GetNotifications', query, this.url);

	return this.ProcessSoapResult(soap_res);
};

UserNotificationContainer.prototype.ProcessSoapResult = function UserNotificationContainer_ProcessSoapResult(soap_res) {
	if (soap_res.getFaultCode() !== 0) {
		parent.aras.AlertError(soap_res);
		return null;
	}

	var result_dom;
	if (soap_res.isFault()) {
		return null;
	} else {
		result_dom = soap_res.getResult();
	}

	return result_dom;
};

UserNotificationContainer.prototype.RefreshUpdateDom = function UserNotificationContainer_RefreshUpdateDom(result_dom) {
	if (!result_dom) {
		return;
	}
	this.UpdateInfoDom = result_dom.selectSingleNode('Item[@id=\'UpdateInfoMessage\']');
};

UserNotificationContainer.prototype.FillStandardMessageCollection = function UserNotificationContainer_FillStandardMessageCollection(result_dom, clear_before) {
	if (clear_before) {
		this.ClearCollection();
	} else {
		this.MakeAllMessagesOld();
	}

	if (!result_dom) {
		this.ClearCollection();
		return;
	}

	var standard_collection = result_dom.selectNodes('Item[type/text()=\'Standard\']')

	for (var i = 0; i < standard_collection.length; i++) {
		var messageItem = standard_collection[i];
		var message_id = messageItem.getAttribute('id');
		var title = parent.aras.getItemProperty(messageItem, 'title');
		var image_url = parent.aras.getItemProperty(messageItem, 'icon');
		this.AddMessage(message_id, title, image_url, messageItem);
	}

	this.ClearOldMessages();
};

UserNotificationContainer.prototype.ShowPopupCollection = function UserNotificationContainer_ShowPopupCollection(result_dom, showAsModeless) {
	if (!result_dom) {
		return;
	}

	var popup_collection = result_dom.selectNodes('Item[type/text()=\'Popup\']');

	var sorted_list = this.SortMessagesByPriority(popup_collection);
	var i = 0;
	var nextSortedList = function() {
		if (i >= sorted_list.length) {
			return;
		}
		var index = showAsModeless ? i : sorted_list.length - i - 1;
		if (!sorted_list[index]) {
			i++;
			nextSortedList();
		} else {
			nextMsgIndex(index, 0);
		}
	}.bind(this);
	var nextMsgIndex = function(index, msg_index) {
		if (msg_index >= sorted_list[index].length) {
			i++;
			nextSortedList();
		} else {
			this.DisplayMessageDialog(sorted_list[index][msg_index], showAsModeless, function() {
				window.setTimeout(function() {
					msg_index++;
					nextMsgIndex(index, msg_index);
				}, 0);
			});
		}
	}.bind(this);
	nextSortedList();
};

UserNotificationContainer.prototype.SortMessagesByPriority = function UserNotificationContainer_SortMessagesByPriority(message_collection) {
	var sorted_list = [];

	for (var i = 0; i < message_collection.length; i++) {
		var priority = parent.aras.getItemProperty(message_collection[i], 'priority');
		if (!sorted_list[priority]) {
			sorted_list[priority] = [];
		}

		sorted_list[priority][sorted_list[priority].length] = message_collection[i];
	}

	return sorted_list;
};

UserNotificationContainer.prototype.UpdateMessageCollection = function UserNotificationContainer_UpdateMessageCollection(doAsync, callback) {
	if (!doAsync) {
		doAsync = false;
	}

	if (doAsync) {
		var fn = function getCollectionCallback(container, message_collection) {
			container.RefreshUpdateDom(message_collection);
			container.FillStandardMessageCollection(message_collection);
			container.ShowPopupCollection(message_collection, true);
			callback();
		};
		this.AsyncCheckNewMessages(fn);
	} else {
		var message_collection = this.SyncGetMessageCollection();
		this.RefreshUpdateDom(message_collection);
		this.FillStandardMessageCollection(message_collection);
		this.ShowPopupCollection(message_collection, false);
	}
};

UserNotificationContainer.prototype.StartIterativeUpdateMessageCollection = function UserNotificationContainer_StartIterativeUpdateMessageCollection() {
	this.UpdateMessageCollection(true, callback);

	function callback() {
		this.CheckTimeout = setTimeout(function() { parent.aras.UserNotification.StartIterativeUpdateMessageCollection(); }, parent.aras.UserNotification.CheckInterval);
	}
};

UserNotificationContainer.prototype.DisplayMessageDialogById = function UserNotificationContainer_DisplayMessageDialogById(message_id, showAsModeless) {
	if ('UpdateInfoMessage' == message_id) {
		this.DisplayMessageDialog(this.UpdateInfoDom, showAsModeless);
	} else {
		var message_item = parent.aras.getItemById('Message', message_id);
		if (message_item) {
			this.DisplayMessageDialog(message_item, showAsModeless);
		} else {
			parent.aras.AlertError(parent.aras.getResource('', 'user_notifications.message_no_more_available'), '', '');
			this.RemoveMessage(message_id);
		}
	}
};

UserNotificationContainer.prototype.DisplayMessageDialog = function UserNotificationContainer_DisplayMessageDialog(message_item, showAsModeless, callback) {
	if (message_item == null) {
		return;
	}

	var template_url = parent.aras.getI18NXMLResource('notification_popup_template.xml', parent.aras.getBaseURL());
	var template_dom = parent.aras.createXMLDocument();
	template_dom.load(template_url);

	var parameters = this.PopupParametersArray[parent.aras.getItemProperty(message_item, 'id')];
	if (!parameters) {
		parameters = {};
	}

	var opened_message_window = parameters.window;

	parameters.id = parent.aras.getItemProperty(message_item, 'id');
	parameters.default_template = template_dom.selectSingleNode('template/html').text;
	parameters.custom_html = parent.aras.getItemProperty(message_item, 'custom_html');

	parameters.dialogWidth = parent.aras.getItemProperty(message_item, 'width');
	parameters.dialogHeight = parent.aras.getItemProperty(message_item, 'height');
	parameters.css = parent.aras.getItemProperty(message_item, 'css');
	parameters.is_standard_template = parent.aras.getItemProperty(message_item, 'is_standard_template') == '1';

	if (!parameters.dialogWidth || parameters.is_standard_template) {
		parameters.dialogWidth = template_dom.selectSingleNode('template/dialog_width').text;
	}

	if (!parameters.dialogHeight || parameters.is_standard_template) {
		parameters.dialogHeight = template_dom.selectSingleNode('template/dialog_height').text;
	}

	parameters.title = parent.aras.getItemProperty(message_item, 'title');
	parameters.text = parent.aras.getItemProperty(message_item, 'text');
	parameters.url = parent.aras.getItemProperty(message_item, 'url');
	parameters.icon = parent.aras.getItemProperty(message_item, 'icon');

	parameters.OK_IsVisible = ('1' == parent.aras.getItemProperty(message_item, 'show_ok_button'));
	parameters.Exit_IsVisible = ('1' == parent.aras.getItemProperty(message_item, 'show_exit_button'));

	parameters.OK_Label = parent.aras.getItemProperty(message_item, 'ok_button_label');
	parameters.Exit_Label = parent.aras.getItemProperty(message_item, 'exit_button_label');

	parameters.container = this;
	parameters.opener = window;
	parameters.writeContent = writeContent;
	parameters.aras = parent.aras;

	this.PopupParametersArray[parameters.id] = parameters;

	if (!showAsModeless) {
		parameters.dialogWidth = parameters.dialogWidth;
		parameters.dialogHeight = parameters.dialogHeight;
		parameters.content = 'modalDialog.html';

		window.ArasModules.Dialog.show('iframe', parameters).promise.then(
			function(res) {
				if (res == this.constAcknowledge) {
					this.AcknowledgeMessage(message_item);
				}
				if (callback) {
					callback();
				}
			}.bind(this)
		);
	} else {
		var sFeatures = '';
		sFeatures += 'height=' + parameters.dialogHeight + ', ';
		sFeatures += 'width=' + parameters.dialogWidth + '';

		if (opened_message_window) {
			opened_message_window.close();
		}

		var fn = function OnBeforeUnloadHandler() {
			if (parameters.window.returnValue == parameters.window.parameters.container.constAcknowledge) {

				parameters.window.parameters.container.AcknowledgeMessage(message_item);
			}
			parameters.window = null;
		};

		parameters.window = window.open(parent.aras.getScriptsURL() + 'blank.html', '', sFeatures);
		parameters.window.focus();
		writeContent(parameters.window);
		parameters.window.parameters = parameters;
		parameters.window.addEventListener('beforeunload', fn);
		if (callback) {
			callback();
		}
	}

	//////////////////////////////////////////
	function writeContent(w) {
		var doc = w.document,
			template,
			title_expr = new RegExp('{TITLE}', 'g'),
			url_expr = new RegExp('{URL}', 'g'),
			text_expr = new RegExp('{TEXT}', 'g'),
			icon_expr = new RegExp('{ICON}', 'g');

		if (!parameters.is_standard_template) {
			template = parameters.custom_html.replace(title_expr, parameters.title);
			template = template.replace(url_expr, parameters.url);
			template = template.replace(text_expr, parameters.text);
			template = template.replace(icon_expr, parameters.icon);

			doc.write(template);
		} else {
			template = parameters.default_template.replace(title_expr, parameters.title);
			template = template.replace(url_expr, parameters.url);
			template = template.replace(text_expr, parameters.text);
			template = template.replace(icon_expr, parameters.icon);
			doc.write(template);

			if (parameters.css) {
				var style = doc.createElement('style');
				style.innerHTML = parameters.css;
				doc.head.appendChild(style);
			}
			var notificationTitle = doc.getElementById('notification_title');
			var titleElement = notificationTitle.appendChild(doc.createElement('h2'));
			titleElement.setAttribute('class', 'title');
			if (parameters.url) {
				var urlElement = titleElement.appendChild(doc.createElement('a'));
				urlElement.setAttribute('href', parameters.url);
				urlElement.setAttribute('target', '_about');
				urlElement.setAttribute('class', 'sys_item_link');
				urlElement.textContent = parameters.title;
			} else {
				titleElement.textContent = parameters.title;
			}

			if (!parameters.icon) {
				doc.getElementById('message_icon').style.display = 'none';
			}
		}
		w.returnValue = parameters.container.constClose;
		w.closeWindow = function(value) {
			if (w.dialogArguments && w.dialogArguments.dialog) {
				w.dialogArguments.dialog.close(value);
			} else {
				w.returnValue = value || w.returnValue;
				w.close();
			}
		};
		doc.close();

		var innerHTML = '';
		if (parameters.OK_IsVisible) {
			innerHTML += parameters.container.uiDrawInputButton('OK_button', parameters.OK_Label, ' window.closeWindow(\'' + parameters.container.constAcknowledge + '\');', 'notification_ok_btn');
		}
		if (parameters.Exit_IsVisible) {
			innerHTML += parameters.container.uiDrawInputButton('Exit_button', parameters.Exit_Label, ' window.closeWindow(\'' + parameters.container.constClose + '\');', 'cancel_button notification_exit_btn');
		}
		if (innerHTML) {
			var el = doc.createElement('center');
			el.innerHTML = innerHTML;
			var source = doc.getElementById('btns') || doc.getElementsByTagName('body')[0];
			if (source) {
				source.appendChild(el);
			}
		}
	}
};

UserNotificationContainer.prototype.uiDrawInputButton = function UserNotificationContainer_uiDrawInputButton(name, value, handler_code, class_name) {
	return '<input name="' + name + '" type="button" value="' + value + '" onclick="' + handler_code + '" class="btn ' + class_name + '" />';
};

UserNotificationContainer.prototype.AcknowledgeMessage = function UserNotificationContainer_AcknowledgeMessage(message_item) {
	if (!message_item) {
		return;
	}

	var acknowledge_dom = message_item.selectSingleNode('acknowledge');

	if (acknowledge_dom.text == 'Once') {
		var message_id = message_item.getAttribute('id');

		var relsh = '' +
			'<Item type="Message Acknowledgement" action="add">' +
			'	<related_id>' + parent.aras.getCurrentUserID() + '</related_id>' +
			'	<source_id>' + message_item.getAttribute('id') + '</source_id>' +
			'</Item>';

		var soap_res = parent.aras.soapSend('ApplyItem', relsh);

		if (soap_res.getFaultCode() !== 0) {
			parent.aras.AlertError(soap_res);
			return null;
		}

		if (soap_res.isFault()) {
			return null;
		}

		this.RemoveMessage(message_id);
	}
};

UserNotificationContainer.prototype.Dispose = function UserNotificationContainer_Dispose() {
	if (this.CheckTimeout) {
		clearTimeout(this.CheckTimeout);
	}
};
