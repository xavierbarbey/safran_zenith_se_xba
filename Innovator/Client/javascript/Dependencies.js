﻿function Dependencies() { }

Dependencies.View = function DependenciesView(itemTypeName, itemId, viewWhereUsed, aras, isInTearOff) {
	var params = '';
	var itemType = aras.getItemTypeDictionary(itemTypeName, 'name');
	if (!itemType || itemType.isError()) {
		return;
	}

	var item = aras.getItemById(itemTypeName, itemId, 0);
	if (!item) {
		return;
	}

	if (aras.isNew(item)) {
		aras.AlertError(aras.getResource('', 'dependencies.is_new_error', itemType.getProperty('label')));
		return;
	}

	var url = aras.getScriptsURL();
	var strBrItemID;

	if (viewWhereUsed) {
		url += 'whereUsed.html?id=' + itemId + '&type_name=' + itemTypeName;
		strBrItemID = itemId + '_whereUsed';
	} else {
		url += 'StructureBrowser.html?id=' + itemId + '&type_name=' + itemTypeName;
		strBrItemID = itemId + '_StructureBrowser';
	}

	var wndWidth = screen.width * 0.7;
	var wndHeight = screen.height * 0.7;
	var leftCoord = screen.width / 2 - wndWidth / 2;
	var topCoord = screen.height / 2 - wndHeight / 2;
	params = 'left=' + leftCoord + ', top=' + topCoord + ', width=' + wndWidth + ', height=' +
	wndHeight + ', menubar=0, resizable=1, scrollbars=0, location=0, toolbar=0, status=0' + (isInTearOff ? ', isOpenInTearOff=true' : '');

	var win = aras.uiFindWindowEx(strBrItemID);
	if (!win || aras.isWindowClosed(win)) {
		win = aras.uiOpenWindowEx(strBrItemID, params);
		if (!win) {
			return;
		}

		aras.uiRegWindowEx(strBrItemID, win);
		window.open(url, win.name, params);
	} else {
		win.focus();
	}
};
