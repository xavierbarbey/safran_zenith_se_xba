﻿function InnovatorUpdate() {}

InnovatorUpdate.prototype.BeginIsNeedCheckUpdates = function InnovatorUpdateBeginIsNeedCheckUpdates() {
	var arasObj = parent.aras;
	var updateInfo = arasObj.commonProperties.innovatorUpdateInfo = {key: '', iv: '', info: '', version: ''};
	var res = parent.arasMainWindowInfo.getCheckUpdateInfoResult;
	if (res.getFaultCode() === 0) {
		var updateInfoNode = res.results.selectSingleNode(arasObj.XPathResult('/UpdateNotNeeded'));
		if (updateInfoNode) {
			arasObj.commonProperties.innovatorUpdateInfo.version = updateInfoNode.text;
			return;
		}
		updateInfoNode = res.results.selectSingleNode(arasObj.XPathResult('/CheckUpdateInfo'));

		var keyNode = updateInfoNode.selectSingleNode('Key');
		var ivNode = updateInfoNode.selectSingleNode('IV');
		var infoNode = updateInfoNode.selectSingleNode('Info');
		var versionNode = updateInfoNode.selectSingleNode('Version');

		if (keyNode) {
			updateInfo.key = keyNode.text;
		}
		if (ivNode) {
			updateInfo.iv = ivNode.text;
		}
		if (infoNode) {
			updateInfo.info = infoNode.text;
		}
		if (versionNode) {
			updateInfo.version = versionNode.text;
		}
	} else {
		arasObj.AlertError('GetUpdateInfo failed');
		return;
	}

	if (updateInfo.key && updateInfo.iv && updateInfo.info) {
		getUpdateInfo();
	}

	function escapeXml(str) {
		return str.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;');
	}

	function getUpdateInfo() {
		var method = 'SecureGetUpdateInfo';
		var methodNm = 'http://www.aras.com/Notifications';
		var str = '<info>' + escapeXml(updateInfo.info) + '</info>' +
				'<key>' + escapeXml(updateInfo.key) + '</key>' +
				'<iv>' + escapeXml(updateInfo.iv) + '</iv>';

		ArasModules.soap(str, {
			async: true,
			url: location.protocol + '//www.aras.com/notifications/updatecheck.asmx',
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + '/' + method,
			headers: {}
		})
			.then(function(responseText) {
				var tmpContent = responseText.indexOf('<SecureGetUpdateInfoResult>');
				if (tmpContent > -1) {
					tmpContent = responseText.substr(
						tmpContent,
						responseText.indexOf('</SecureGetUpdateInfoResult>') + '</SecureGetUpdateInfoResult>'.length - tmpContent
					);

					var dom = arasObj.createXMLDocument();

					dom.loadXML(tmpContent);
					tmpContent = dom.selectSingleNode('/*/content');

					var tmpSignature = dom.selectSingleNode('/*/signature');

					storeToVersionFile(tmpContent ? tmpContent.text : null, tmpSignature ? tmpSignature.text : null);
				}
			});
	}

	function storeToVersionFile(updateInfoContent, updateInfoSignature) {
		if (!updateInfoContent) {
			return;
		}

		var xml = '<UpdateInfo>' +
				'<content xmlns="http://www.aras.com/Notifications">' + escapeXml(updateInfoContent) + '</content>' +
				'<signature xmlns="http://www.aras.com/Notifications">' + escapeXml(updateInfoSignature) + '</signature>' +
				'</UpdateInfo>';
		var xml4Client = xml.replace(/UpdateInfo>/g, 'updateInfo>') + '<serverVersion>' + escapeXml(updateInfo.version) + '</serverVersion>';

		var method = 'StoreToVersionFile';
		var methodNm = 'http://www.aras.com/Notifications';

		ArasModules.soap(xml4Client, {
			async: true,
			url: arasObj.getScriptsURL() + 'InnovatorClient.asmx',
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + '/' + method,
			headers: {}
		});
		arasObj.soapSend('StoreVersionFile', xml, undefined, undefined, new SoapController(function() {}));
	}
};
