﻿function ModulesHelper() {
	if (!window.__staticVariablesStorage.__modulesHelperCodeCache) {
		window.__staticVariablesStorage.__modulesHelperCodeCache = {};
	}

	function using(classes, func, isAsync) {
		var args = [];
		var asyncResult;

		if (isAsync) {
			asyncResult = this.createAsyncResult();
			asyncResult.then(func);
		}

		function callFuncIfArgLoaded() {
			var isComplete = true;
			for (var argIndex = 0; argIndex < classes.length; argIndex++) {
				if (!args[argIndex]) {
					isComplete = false;
				}
			}

			if (isComplete) {
				if (isAsync) {
					asyncResult.resolve(args);
				} else {
					func.apply(window, args);
				}
			}
		}

		function ifExistsInCache(i, cache) {
			return function() {
				args[i] = cache[classes[i]];
				callFuncIfArgLoaded();
			};
		}

		function ifNotExistsInCache(i, cache, events, xhr) {
			return function() {
				var code;
				if (xhr) {
					if (xhr.readyState == 4 && xhr.status == 200) {
						code = xhr.responseText;
					} else {
						return;
					}
				} else {
					code = window.__staticVariablesStorage.__modulesHelperCodeCache[classes[i]];
				}

				var setArg = function() {
					args[i] = cache[classes[i]];
					callFuncIfArgLoaded();
					events.removeListener(classes[i], setArg);
				};
				events.addListener(classes[i], setArg);
				/* jshint ignore:start */
				eval(code);
				/* jshint ignore:end */
			};
		}

		for (var classIndex = 0; classIndex < classes.length; classIndex++) {
			if (this.classCache[classes[classIndex]]) {
				if (isAsync) {
					setTimeout(ifExistsInCache(classIndex, this.classCache), 0);
				} else {
					ifExistsInCache(classIndex, this.classCache)();
				}
			} else {
				if (window.__staticVariablesStorage.__modulesHelperCodeCache[classes[classIndex]]) {
					if (isAsync) {
						setTimeout(ifNotExistsInCache(classIndex, this.classCache, this.eventHelper), 0);
					} else {
						ifNotExistsInCache(classIndex, this.classCache, this.eventHelper)();
					}
				} else {
					var tmp = classes[classIndex].split('/');
					var moduleName = tmp.slice(0, tmp.length - 1).join('/'); // get everything before last segment.
					var className = tmp.slice(-1)[0]; // class name is last segment
					var classUrl = aras.getBaseURL('/Modules/' + moduleName + '/Scripts/Classes/' + className + '.js');
					var xmlHttpRequest = new XMLHttpRequest();
					if (isAsync) {
						xmlHttpRequest.open('GET', classUrl, true);
					} else {
						xmlHttpRequest.open('GET', classUrl, false);
					}
					xmlHttpRequest.onreadystatechange = ifNotExistsInCache(classIndex, this.classCache, this.eventHelper, xmlHttpRequest);
					xmlHttpRequest.send();
				}
			}
		}

		return asyncResult;
	}

	function define(classes, classFullName, func, isAsync) {
		var args = [];

		function callFuncIfArgLoaded(cache, events) {
			var isComplete = true;
			for (var argIndex = 0, l = (classes ? classes.length : 0); argIndex < l; argIndex++) {
				if (!args[argIndex]) {
					isComplete = false;
				}
			}

			if (isComplete) {
				cache[classFullName] = func.apply(window, args);
				events.raiseEvent(classFullName);
			}
		}

		function ifExistsInCache(i, cache, events) {
			return function() {
				args[i] = cache[classes[i]];
				callFuncIfArgLoaded(cache, events);
			};
		}

		function ifNotExistsInCache(i, cache, events, xhr) {
			return function() {
				var code;
				if (xhr) {
					if (xhr.readyState == 4 && xhr.status == 200) {
						code = xhr.responseText;
						window.__staticVariablesStorage.__modulesHelperCodeCache[classes[i]] = code;
					} else {
						return;
					}
				} else {
					code = window.__staticVariablesStorage.__modulesHelperCodeCache[classes[i]];
				}

				var setArg = function() {
					args[i] = cache[classes[i]];
					callFuncIfArgLoaded(cache, events);
					events.removeListener(classes[i], setArg);
				};
				events.addListener(classes[i], setArg);
				/* jshint ignore:start */
				eval(code);
				/* jshint ignore:end */
			};
		}

		if (!classes || classes.length === 0) {
			var runWithoutClasses = function(cache, events) {
				return function() {
					callFuncIfArgLoaded(cache, events);
				};
			};
			if (isAsync) {
				setTimeout(runWithoutClasses(this.classCache, this.eventHelper), 0);
			} else {
				runWithoutClasses(this.classCache, this.eventHelper)();
			}
			return;
		}

		for (var classIndex = 0; classIndex < classes.length; classIndex++) {
			if (this.classCache[classes[classIndex]]) {
				if (isAsync) {
					setTimeout(ifExistsInCache(classIndex, this.classCache, this.eventHelper), 0);
				} else {
					ifExistsInCache(classIndex, this.classCache, this.eventHelper)();
				}
			} else {
				if (window.__staticVariablesStorage.__modulesHelperCodeCache[classes[classIndex]]) {
					if (isAsync) {
						setTimeout(ifNotExistsInCache(classIndex, this.classCache, this.eventHelper), 0);
					} else {
						ifNotExistsInCache(classIndex, this.classCache, this.eventHelper)();
					}
				} else {
					var tmp = classes[classIndex].split('/');
					var moduleName = tmp.slice(0, tmp.length - 1).join('/'); // get everything before last segment.
					var className = tmp.slice(-1)[0]; // class name is last segment
					var classUrl = aras.getBaseURL('/Modules/' + moduleName + '/Scripts/Classes/' + className + '.js');
					var xmlHttpRequest = new XMLHttpRequest();
					if (isAsync) {
						xmlHttpRequest.open('GET', classUrl, true);
					} else {
						xmlHttpRequest.open('GET', classUrl, false);
					}
					xmlHttpRequest.onreadystatechange = ifNotExistsInCache(classIndex, this.classCache, this.eventHelper, xmlHttpRequest);
					xmlHttpRequest.send();
				}
			}
		}
	}

	// class format -> ['moduleName/className', 'moduleName2/className2']
	this.using = function(classes, isAsync) {
		var thisModelHelper = this;
		if (isAsync) {
			return using.call(thisModelHelper, classes, function() { return arguments; }, true);

		} else {
			var usingAsyncResult = this.createAsyncResult();
			var resultAsyncResult = usingAsyncResult.then(function(classes) {
				var types;
				using.call(thisModelHelper, classes, function() {
					types = arguments;
				});
				return types;
			});

			usingAsyncResult.resolve([classes]);
			return resultAsyncResult;
		}
	};

	this.define = function(classes, classFullName, func, isAsync) {
		var thisModelHelper = this;
		define.call(thisModelHelper, classes, classFullName, func, isAsync);
	};

	this.createAsyncResult = function() {
		var thisModelHelper = this;
		var AsyncResultType;

		using.call(thisModelHelper, ['aras.innovator.core.Core/AsyncResult'], function(AsyncResult) {
			AsyncResultType = AsyncResult;
		}, false);

		return new AsyncResultType();
	};
}

ModulesHelper.prototype.classCache = {};

ModulesHelper.prototype.eventHelper = {
	_events: {},
	addListener: function(eventName, callback) {
		var events = this._events;
		var callbacks = events[eventName] = events[eventName] || [];
		callbacks.push(callback);
	},

	raiseEvent: function(eventName, args) {
		var callbacks = this._events[eventName] || [];
		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(null, args);
		}
	},

	removeListener: function(eventName, callback) {
		var events = this._events;
		var callbacks = events[eventName] = events[eventName] || [];
		var newCallbacks = [];
		for (var i = 0; i < callbacks.length; i++) {
			if (callbacks[i] !== callback) {
				newCallbacks.push(callbacks[i]);
			}
		}
		events[eventName] = newCallbacks;
	}
};

var ModulesManager = new ModulesHelper();
