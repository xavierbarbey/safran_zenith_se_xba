﻿// (c) Copyright by Aras Corporation, 2006-2007.

/*
This file contains logic common for all search grids (grid + searchbar)
*/

var grid = null;
var isMainGrid = false; //this flag is used because behavior is not completely identical
var soapController = null; //controller to manage async soap requests
var statusId; //variable to store message id during async search
var prevQryItem; //previos search results
//------------------------

function setupSearchButtonsStates(searchIsInProgress) {
	/*
	setups states of search buttons:
	stop_search, search, prev_page, next_page
	----
	searchIsInProgress - boolean flag indicating if search is in progress
	*/

	if (!searchContainer || (searchContainer && !searchContainer.getToolbar())) {
		return;
	}

	var t = searchContainer.getToolbar().getActiveToolbar();
	var tb = t.getItem('stop_search');
	if (tb) {
		tb.setEnabled(searchIsInProgress);
	}

	var f = !searchIsInProgress;
	tb = t.getItem('search');
	if (tb) {
		tb.setEnabled(f);
	}
	tb = t.getItem('prev_page');
	if (tb) {
		tb.setEnabled(f);
	}
	tb = t.getItem('next_page');
	if (tb) {
		tb.setEnabled(f);
	}
}

function whenGetResponse(result) {
	soapController = null;

	var faultCode = result.getFaultCode();
	setupSearchButtonsStates(false);
	clearSearchInProgressMessage();
	if (parseInt(faultCode) !== 0) {
		aras.AlertError(result);
		return;
	} else if (faultCode === '0' && currQryItem.getPage() !== '1' && ArasModules.utils.hashFromString(currQryItem.getCriteriesString()) === currentSearchMode.getCacheItem('criteriesHash')) {
		var currentPage = Math.max(currentSearchMode.getPageNumber() - 1, 1);
		currentSearchMode.setPageNumber(currentPage);
		pagemax = currentPage.toString();
		updateToolStatusBar();
		setupGrid(false);
		return;
	}


	currQryItem.setResponse(result);

	setupPageNumber();

	setupGrid(false);
	updateToolStatusBar();

	var nodes = currQryItem.getResultDOM().selectNodes('/' + SoapConstants.EnvelopeBodyXPath + '/Result/Item');
	if (nodes.length === 0 && searchContainer._isNoCountModeForCurrentItemType()) {
		getAndUpdatePageSizeAndMaxRecords();
	}
}

function clearSearchInProgressMessage() {
	if (!statusId) {
		return;
	}

	if (isMainGrid) {
		aras.clearStatusMessage(statusId);
	} else if (document.frames && document.frames.statusbar) {
		document.frames.statusbar.clearStatus(statusId);
	}

	statusId = '';
}

function doSearch_internal() {
	stopSearch(false);
	setupSearchButtonsStates(true);

	if (isMainGrid) {
		statusId = aras.showStatusMessage('page_status', '', '../images/Progress.gif');
	} else {
		var statusbar = document.frames ? document.frames.statusbar : null;
		if (statusbar) {
			statusId = statusbar.contentWindow.setStatus('status', aras.getResource('', 'common.searching_msg'), '../images/Progress.gif');
		}
	}
	
	if(promiseCountResult) {
		promiseCountResult.abort();
		promiseCountResult = null;
	}
	
	soapController = new SoapController(whenGetResponse);
	currQryItem.execute(undefined, soapController);
}

onunload = function onunload_handler() {
	stopSearch(false);
};

function stopSearch(refresh) {
	if (refresh === undefined) {
		refresh = true;
	}
	
	if (soapController && soapController.stop) {
		soapController.stop();
		setupSearchButtonsStates(false);
		soapController = null;
	} else {
		return;
	}
	
	clearSearchInProgressMessage();

	if (refresh && prevQryItem) {
		currQryItem.dom.loadXML(prevQryItem);
		currQryItem.item = currQryItem.dom.documentElement;
		if (!isMainGrid) {
			page = currQryItem.getPage();
		}

		setupGrid(false);
		if (isMainGrid) {
			updateToolStatusBar();
		}
	}

	if (!isMainGrid) {
		if (refresh) {
			showStatus();
		}
	}
}

function InputHelperDialogResultHandler(col, val) {
	if (val || '' === val) {
		grid.inputRow.set(col, 'value', val);
		currQryItem.setPage(1);
		if (grid._grid) {
			const indexHead = grid._grid.settings.indexHead;
			grid._grid.dom.dispatchEvent(new CustomEvent('focusCell', {
				detail: {
					indexRow: 'searchRow',
					indexHead: indexHead.indexOf(grid.getColumnName(col))
				}
			}));
		}
	}
}

function showInputHelperDialog(rowId, col) {
	var prop = null;
	if (searchContainer && searchContainer.getPropertyDefinitionByColumnIndex) {
		prop = searchContainer.getPropertyDefinitionByColumnIndex(col);
	} else {
		var colName = grid.getColumnName(col);
		var propName = colName.substr(0, colName.length - 2);

		for (var i = 0; i < visiblePropNds.length; i++) {
			prop = visiblePropNds[i];
			if (aras.getItemProperty(prop, 'name') === propName) {
				break;
			}
		}
	}

	var aWindow = TopWindowHelper.getMostTopWindowWithAras(window);
	aWindow = aWindow.main || aWindow;
	var propDT = aras.getItemProperty(prop, 'data_type');
	var propName = aras.getItemProperty(prop, 'name');
	var val = null;
	var inputCell = grid.cells('input_row', col);
	var params;
	if (propDT === 'date') {
		var format = null;

		if (currentSearchMode && currentSearchMode.name === 'Simple') {
			format = aras.getDotNetDatePattern('short_date');
		} else {
			format = aras.getItemProperty(prop, 'pattern');
			format = aras.getDotNetDatePattern(format);
		}

		params = {
			format: format,
			aras: aras,
			type: 'Date'
		};

		var wndRect = aras.uiGetElementCoordinates(inputCell.cellNod_Experimental);
		var dateDialog = aWindow.ArasModules.Dialog.show('iframe', params);
		dateDialog.move(wndRect.left - wndRect.screenLeft, wndRect.top - wndRect.screenTop);
		dateDialog.promise.then(
			function(newDate) {
				var val;
				if (newDate) {
					val = aras.convertToNeutral(newDate, 'date', format);
				} else if (newDate === '') {
					val = '';
				}
				InputHelperDialogResultHandler(col, val);
				inputCell.cellNod_Experimental.querySelector('input').focus();
			}
		);

	} else if (propDT === 'image') {
		params = {
			aras: aras,
			image: grid.inputRow.get(col, 'value'),
			type: 'ImageBrowser'
		};
		aWindow.ArasModules.Dialog.show('iframe', params).promise.then(
			function(res) {
				val = 'set_nothing' === res ? '' : res;
				InputHelperDialogResultHandler(col, val);
			}
		);

	} else if (propDT === 'text') {
		params = {
			isEditMode: true,
			content: grid.inputRow.get(col, 'value'),
			aras: aras,
			type: 'Text'
		};
		aWindow.ArasModules.Dialog.show('iframe', params).promise.then(function(val) {
			InputHelperDialogResultHandler(col, val);
		}
		);
	} else if (propDT === 'formatted text') {
		params = {
			aras: aras,
			sHTML: grid.inputRow.get(col, 'value'),
			title: aras.getResource('', 'htmleditor.inn_formatted_text_editor'),
			type: 'HTMLEditorDialog'
		};
		aWindow.ArasModules.Dialog.show('iframe', params).promise.then(function(val) {
			InputHelperDialogResultHandler(col, val);
		}
		);

	} else if (propDT === 'color') {
		var oldColor = grid.inputRow.get(col, 'value');
		params = {
			oldColor: oldColor,
			aras: aras,
			type: 'Color'
		};
		aWindow.ArasModules.Dialog.show('iframe', params).promise.then(
			function(val) {
				InputHelperDialogResultHandler(col, val);
			}
		);
	} else if (propDT === 'item') {
		var propDS = aras.getItemProperty(prop, 'data_source');
		if (!propDS) {
			return;
		}

		var itName = aras.getItemTypeName(propDS);
		if (!itName) {
			return;
		}

		params = {
			aras: aWindow.aras,
			itemtypeName: itName,
			type: 'SearchDialog'
		};

		if (isMainGrid) {
			params.newWindowSizeHandler = function(popupDialog, params) {
				var mainWindow = aras.getMainWindow();
				aras.browserHelper.resizeWindowTo(mainWindow, params.newWidth, params.newHeight);
				params.cancelCallbacks.push(function() {
					aras.browserHelper.resizeWindowTo(mainWindow, params.oldWidth, params.oldHeight);
				});
			};
		}

		aWindow.ArasModules.MaximazableDialog.show('iframe', params).promise.then(
			function(res) {
				var val = res ? res.keyed_name : null;
				InputHelperDialogResultHandler(col, val);
			}
		);
	} else if (propDT === 'string' && propName === 'classification') {
		const classStructure = aras.getItemProperty(grid._itemType, 'class_structure');

		params = {
			title: aras.getItemProperty(prop, 'label'),
			isEditMode: true,
			aras: aWindow.aras,
			class_structure: classStructure,
			dialogType: 'classification',
			itemTypeName: aras.getItemProperty(grid._itemType, 'name'),
			selectLeafOnly: true,
			isRootClassSelectForbidden: true,
			dialogWidth: 600,
			dialogHeight: 700,
			resizable: true,
			content: 'ClassStructureDialog.html',
			expandClassPath: grid.inputRow.get(col, 'value')
		};

		aWindow.ArasModules.Dialog.show('iframe', params).promise.then(function(val) {
			InputHelperDialogResultHandler(col, val);
		});
	} else {
		aras.AlertError(aras.getResource('', 'search_grid_object.lookup_not_available', propDT));
	}
}

function saveEditedData() {
	grid.turnEditOff();
}
