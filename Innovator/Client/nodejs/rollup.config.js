﻿/*
example of command:
cls && rollup -c --file-input ../Modules/JS_code_module/core.js --file-output ../jsBundles/files/core.js --name ESxmlJs --silent
*/

const rollupResolve = require('rollup-plugin-node-resolve');
const rollupBabel = require('rollup-plugin-babel');
const rollupUglify = require('rollup-plugin-uglify');
const babelConfig = require('./babel.config');
const uglifyConfig = require('./uglify.config');
const jsCompileHelper = require('./jsCompileHelper');

const fileNameInput = process.argv[4];
const pathFileNameOutput = process.argv[6];
const name = process.argv[8];

module.exports = {
	input: fileNameInput,
	plugins: [
		rollupResolve(),
		rollupBabel(babelConfig),
		rollupUglify(uglifyConfig),
		{
			onwrite(arg) {
				if (pathFileNameOutput) {
					console.log(jsCompileHelper.rootClientAbsolutePath + jsCompileHelper.trimRelativePath(pathFileNameOutput));
					arg.bundle.map.sources.forEach(function(dependencyPath) {
						if (dependencyPath.indexOf('.js') > 0) {
							console.log(jsCompileHelper.rootClientAbsolutePath + jsCompileHelper.trimRelativePath(dependencyPath));
						}
					})
				}
			}
		}
	],
	output: [
		{
			name: name,
			format: 'iife',
			file: pathFileNameOutput,
			sourcemap: true
		}
	],
}
