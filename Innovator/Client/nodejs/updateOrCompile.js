const rollup = require('rollup');
const rollupConfig = require('./rollup.config');
const jsCompileHelper = require('./jsCompileHelper');

const rollupPlugins = rollupConfig.plugins;

const rollupJSModuleListForReCompile = []
const readCacheData = jsCompileHelper.getCacheModuleList();

readCacheData.then(function() {
	const rollupJSModuleList = jsCompileHelper.getBundleList();

	let chechModulesPromises = [];
	rollupJSModuleList.forEach(function(rollupJSModule) {
		let cacheModule = jsCompileHelper.cacheModuleList.find((cacheModule) => {
			return cacheModule.srcPath.indexOf(rollupJSModule.input.substr(2)) > -1;
		});

		if (!cacheModule) {
			rollupJSModuleListForReCompile.push(rollupJSModule);
			return;
		};

		const currentXXHashPromise = jsCompileHelper.getCacheModuleXXHash(cacheModule);
		const currentTimeHashPromise = jsCompileHelper.getCacheModuleTimeHash(cacheModule);

		const chechModulePromise = Promise.all([currentXXHashPromise, currentTimeHashPromise]).then((res) => {
			const currentXXHash  = res[0];
			const currentTimeHash  = res[1];

			if (currentXXHash !== cacheModule.moduleHash && currentTimeHash !== cacheModule.moduleHash) {
				rollupJSModuleListForReCompile.push(rollupJSModule);
			} else if (currentXXHash === cacheModule.moduleHash) {
				cacheModule.moduleHash = currentTimeHash;
			}
		});
		chechModulesPromises.push(chechModulePromise);
	});

	return Promise.all(chechModulesPromises).then(()=>{
		if (rollupJSModuleListForReCompile.length > 0) {
			rollupPlugins.push({
				onwrite: jsCompileHelper.getRollupWriteHandler()
			});
			rollupJSModuleListForReCompile.forEach((rollupJSModule) => {
				rollup.rollup({
					input: '../' + jsCompileHelper.trimRelativePath(rollupJSModule.input),
					plugins: rollupPlugins,
				}).then((bundleResult) => {
					return bundleResult.write({
						file: '../' + jsCompileHelper.trimRelativePath(rollupJSModule.file),
						format: rollupConfig.output[0].format,
						sourcemap: rollupConfig.output[0].sourcemap,
						name: rollupJSModule.name,
					});
				});
			});
		} else {
			jsCompileHelper.writeCacheConfigFile();
		}
	});
}).catch(function(error) {
	console.error(error);
});
