const fs = require('fs');

let bundleFolder = 'Modules';
let bundleConfig = bundleFolder + '/jsBundleList.json';

cacheFolder = 'jsBundles';
let cacheConfig = cacheFolder + '/data.json';

let rootClientAbsolutePath = process.cwd();

const portableNodeJsPartOfPath = 'nodejs';
const callFromPortableNodeJs = process.cwd().substr(-(portableNodeJsPartOfPath.length)) === portableNodeJsPartOfPath;
if (callFromPortableNodeJs) {
	cacheConfig = '../' + cacheConfig;
	bundleConfig = '../' + bundleConfig;
	rootClientAbsolutePath = rootClientAbsolutePath.substr(0, rootClientAbsolutePath.length - portableNodeJsPartOfPath.length);
}

rootClientAbsolutePath = rootClientAbsolutePath.split('\\').join('/');

if (rootClientAbsolutePath[rootClientAbsolutePath.length - 1] !== '/') {
	rootClientAbsolutePath = rootClientAbsolutePath + '/';
}

module.exports = {
	cacheModuleList: [],

	rootClientAbsolutePath: rootClientAbsolutePath,
	getCacheModuleXXHash: function(cacheModule) {
		return this.getHashOfPathsList(cacheModule.listOfDependencies, this.setXXHashFile);
	},
	getCacheModuleTimeHash: function(cacheModule) {
		return this.getHashOfPathsList(cacheModule.listOfDependencies, this.setTimeHashFile);
	},
	getHashOfPathsList: function(pathsList, hashFunction) {
		const hashObject = {};
		const listHashPromises = [];
		pathsList.forEach((path) => {
			listHashPromises.push(hashFunction(path, hashObject));
		});

		return Promise.all(listHashPromises).then(resolve => {
			let moduleHash = '';
			pathsList.forEach((path) => {
				moduleHash += hashObject[path];
			})
			return moduleHash;
		});
	},
	getCacheModuleList: function() {
		return new Promise((resolve, reject) => {
			if (!fs.existsSync(cacheConfig)) {
				this.cacheModuleList = [];
				return resolve();
			}

			fs.readFile(cacheConfig, 'utf8', (err, data) => {
				if (data) {
					this.cacheModuleList = JSON.parse(data);
					const bundleList = this.getBundleList();
					let clientPathFromCacheConfig = '';

					this.cacheModuleList.forEach((cacheModule) => {
						if (clientPathFromCacheConfig === '') {
							const currenBundletModule = bundleList.find((bundleModule) => {
								return bundleModule.name == cacheModule.name;
							});
							if (!currenBundletModule) {
								return;
							}
							clientPathFromCacheConfig = cacheModule.srcPath.slice(0, -currenBundletModule.input.length);
						}
						cacheModule.srcPath = rootClientAbsolutePath + cacheModule.srcPath.substring(clientPathFromCacheConfig.length);
						cacheModule.listOfDependencies = cacheModule.listOfDependencies.map((depPath) => {
							return rootClientAbsolutePath + depPath.substring(clientPathFromCacheConfig.length)
						});
					});
				}
				resolve();
			});
		});
	},
	trimRelativePath: function(url) {
		const re = /[.\/]+\//;
		const found = url.match(re);
		if (found && found[0] && url.substr(0, found[0].length) === found[0]) {
			return url.substr(found[0].length);
		}
		return url;
	},
	getRollupWriteHandler: function(isUsingXxHash) {
		return (arg) => {
			const rollupDependencyPathList = arg.bundle.map.sources.filter(function(rollupResultModule) {
				return rollupResultModule.substr(-3) === '.js';
			}).map((rollupResultModule) => {
				return rootClientAbsolutePath + this.trimRelativePath(rollupResultModule);
			});

			const srcAbsolutePath = rollupDependencyPathList[rollupDependencyPathList.length - 1];
			const hashStringPromise = this.getHashOfPathsList(rollupDependencyPathList, (isUsingXxHash == true) ? this.setXXHashFile  : this.setTimeHashFile);

			hashStringPromise.then((hashString) => {
				const moduleIndex = this.cacheModuleList.findIndex((module) => {
					return module.srcPath === srcAbsolutePath;
				});

				if (moduleIndex > -1) {
					this.cacheModuleList.splice(moduleIndex, 1);
				}

				const moduleCacheInfo = {
					name: arg.name,
					srcPath: srcAbsolutePath,
					listOfDependencies: rollupDependencyPathList,
					moduleHash: hashString
				};
				this.cacheModuleList.push(moduleCacheInfo);
			}).then(() => {
				return this.writeCacheConfigFile();
			}).catch(function(error) {
				console.error('Error on creating information for cache');
				console.error(error);
			});
		};
	},
	writeCacheConfigFile: function() {
		return new Promise((resolve, reject) => {
			const getDirName = require('path').dirname;
			const jsCacheFolder = getDirName(cacheConfig);
			if (!fs.existsSync(jsCacheFolder)) {
				fs.mkdirSync(jsCacheFolder);
			}

			fs.writeFile(cacheConfig, JSON.stringify(this.cacheModuleList), function(err) {
				if (err) {
					return reject(err);
				}
				return resolve();
			});
		});
	},
	setXXHashFile: function(filePath, hashStorage) {
		return new Promise(function(resolve, reject) {
			const XXHash = require('xxhash');
			const file = fs.readFileSync(filePath);
			hashStorage[filePath] = XXHash.hash(file, 0xCAFEBABE);
			resolve();
		});
	},
	setTimeHashFile: function(filePath, hashStorage) {
		return new Promise(function(resolve, reject) {
			fs.stat(filePath, function(err, fileInfo) {
				if (err) {
					return reject(err);
				}
				hashStorage[filePath] = fileInfo.mtime.getTime() / 1000 | 0;
				resolve();
			});
		});
	},
	getBundleList: function() {
		const resultList = [];
		let listOfBundles = {};
		if (fs.existsSync(bundleConfig)) {
			listOfBundles = JSON.parse(fs.readFileSync(bundleConfig, 'utf8'));
		}

		return Object.keys(listOfBundles).map((moduleName) => {
			return {
				name: moduleName,
				input: bundleFolder + '/' + this.trimRelativePath(listOfBundles[moduleName]),
				file: cacheFolder + '/' + moduleName + '.js'
			};
		});
	}
};
