module.exports = {
	babelrc: false, // stops babel from using .babelrc files
	presets: [
		[
			__dirname + '/node_modules/babel-preset-env',
			{
				'targets': {
					'browsers': [
						'ie >= 11',
						'edge >= 15',
						'firefox >= 45',
						'chrome >= 56'
					]
				},
				'modules': false
			}
		]
	],
	plugins: [
		[__dirname + '/node_modules/babel-plugin-transform-builtin-classes', {
			globals: ['HTMLElement']
		}],
		__dirname + '/node_modules/babel-plugin-transform-es2015-classes',
		__dirname + '/node_modules/babel-plugin-external-helpers',
		__dirname + '/node_modules/babel-plugin-transform-flow-strip-types'
	],
	exclude: [
		'node_modules/**'
	]
}
