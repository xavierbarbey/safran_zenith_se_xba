﻿<%-- (c) Copyright by Aras Corporation, 2004-2008. --%>
<!-- #INCLUDE FILE="include/utils.aspx" -->
<%
	If Request.QueryString("return") <> "token" Then
		Response.Write( "<Result><user>" + Client_Config("login_username") + "</user><password>" + Client_config("login_password") + "</password><hash>" + Client_Config("hash_password_flag") + "</hash></Result>" )
	Else
		Dim resultObject As Object = Client_Config("request_password_grant")
		Dim result As Dictionary(Of String, Object) = CType(resultObject, Dictionary(Of String, Object)) ' Dictionary was returned instead HttpResponseMessage because compiler does not recognize it
		Dim statusCode As Integer = CType(result.Item("statusCode"), Integer) ' Cannot use NameOf because used compiler does not recognize it
		Dim contentType As String = CType(result.Item("contentType"), String)
		Dim body As String = CType(result.Item("body"), String)

		Response.StatusCode = statusCode
		Response.ContentType = contentType
		Response.Write(body)
	End If
%>
