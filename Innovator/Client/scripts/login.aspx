﻿<!DOCTYPE html>
<!-- (c) Copyright by Aras Corporation, 2004-2014. -->
<!-- #INCLUDE FILE="include/utils.aspx" -->
<% 
	Dim updateInfo As UpdateInfoContent = VersionConfig.LoadFromVersionFile()
	Dim linkMessage As Message = New Message()
	Dim config As ClientConfig = ClientConfig.GetServerConfig()

	Dim defaultLanguage As String = "en"
	Dim language As String
	If (HttpContext.Current.Request.UserLanguages.Length <> 0) Then
		language = HttpContext.Current.Request.UserLanguages(0).Split("-".ToCharArray())(0)
	Else
		language = defaultLanguage
	End If

	Dim isUpdateAvailable As Boolean = False
	Dim isNoInfoAboutUpdate As Boolean = True

	If (updateInfo.CurrentServerVersion <> updateInfo.CurrentAvailableVersion) Then
		For Each pair As Generic.KeyValuePair(Of Version, Boolean) In updateInfo.UpdateAvailableCollection
			If (pair.Key >= updateInfo.CurrentServerVersion AndAlso updateInfo.CurrentServerVersion <> updateInfo.CurrentAvailableVersion) Then
				isUpdateAvailable = pair.Value
				isNoInfoAboutUpdate = False
				Exit For
			End If
		Next
	Else
		isNoInfoAboutUpdate = False
	End If

	If (isNoInfoAboutUpdate) Then
		updateInfo.Url = "https://www.aras.com/Notifications/LatestRelease.aspx"
		linkMessage.Title = "Find the latest version of Aras Innovator"
	Else
		If (isUpdateAvailable) Then
			If (Not updateInfo.Messages.TryGetValue(language, linkMessage)) Then
				updateInfo.Messages.TryGetValue(defaultLanguage, linkMessage)
			End If
		Else
			updateInfo.Url = "https://www.aras.com"
			linkMessage.Title = "www.aras.com"
		End If
	End If

	Dim slashAndSalt As String = ""
	If config.UseCachingModule Then
		slashAndSalt = "/" + ClientHttpApplication.FullSaltForCachedContent
	End If

	Dim splash As String = Client_Config("login_splash")
	If (String.IsNullOrEmpty(splash) Or splash = "../images/aras-innovator.svg") Then
		splash = ".." + slashAndSalt + "/images/aras-innovator.svg"
	End If

	Dim loginPassword As String = ""
	Dim useLogonHookPasswordGrant As String = Client_config("use_logon_hook_password_grant")
	If useLogonHookPasswordGrant <> "true" Then
		loginPassword = Client_config("login_password")
	End If

	Dim iservers As System.Xml.XmlNodeList = ApplicationXML.SelectNodes("/Innovator/Server[@name]")
	Dim serversOptionsHtml As String = ""
	If Client_Config("login_hide_server") <> "true" Then
		If iservers.Count > 1 Then

			Dim iserver As System.Xml.XmlElement
			For Each iserver In iservers
				serversOptionsHtml += "<option BaseURL=\'" + iserver.GetAttribute("BaseURL") + "\' EXT=\'" + iserver.GetAttribute("EXT") + "\'>" + iserver.GetAttribute("name") + "</option>"
			Next

		End If
	End If

	Dim oauthClientId As String = config.OAuthClientId
%>
<html>
<head>
	<title></title>
	<link href="..<%=slashAndSalt%>/styles/controls/dialog.css" rel="stylesheet" />
	<link href="..<%=slashAndSalt%>/styles/common.min.css" rel="stylesheet" />
	<link href="..<%=slashAndSalt%>/styles/login.min.css" rel="stylesheet" />
	<script type="text/javascript">
	window.__staticVariablesStorage = true; //For correct work ResourceManager after remove login page.

	var clientConfig = {
		productName : '<%=Client_Config("product_name")%>',
		loginSplash: '<%=Client_Config("login_splash")%>',
		loginHidePassword : '<%=Client_Config("login_hide_password")%>',
		loginHideDatabase : '<%=Client_Config("login_hide_database")%>',
		loginHideServer : '<%=Client_Config("login_hide_server")%>',
		loginNoPromptUsername : '<%=Client_config("login_noprompt_username")%>',
		loginIgnoreCookies : '<%=Client_config("login_ignore_cookies")%>',
		loginDatabase : '<%=Client_config("login_database")%>',
		loginUsername : decodeURI('<%=System.Web.HttpUtility.UrlEncode(Client_config("login_username"))%>'),
		loginPassword : '<%=loginPassword%>',
		loginServer : '<%=Client_config("login_server")%>',
		loginOnloadSubmit : '<%=Client_config("login_onload_submit")%>',
		useLogonHookPasswordGrant : '<%=useLogonHookPasswordGrant%>',
		bypassLogonWait : '<%=Client_config("bypass_logon_wait")%>',
		serverBaseUrl : '<%=Client_Config("server_BaseURL")%>',
		oauthClientId : '<%=oauthClientId%>',
		serverExt : '<%=Client_Config("server_EXT")%>',
		hashPasswordFlag : '<%=Client_Config("hash_password_flag")%>',
		slashAndSalt : '<%=slashAndSalt%>',
		httpAcceptLanguage : '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.ServerVariables.Item("HTTP_ACCEPT_LANGUAGE"),false)%>',
		serversOptionsHtml : '<%=serversOptionsHtml%>',
		updateInfoUrl : '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(updateInfo.Url,false)%>',
		updateInfoText : '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(linkMessage.Title,false)%>',
		isNoUpdateInfo : '<%=isNoInfoAboutUpdate%>'
	};
	</script>
	<script type="text/javascript" src="..<%=slashAndSalt%>/javascript/include.aspx?classes=ArasModules,ClientUrlsUtility,XmlUtils,Solution,ResourceManager,LoginPage"></script>
</head>
<body>
	<div class="login-page">
		<div class="login-page__logo">
			<img class="logo" src="<%=splash%>" alt="">
		</div>
		<div class="login-page__main">
			<form class="login-form aras-form" id="LoginForm" onsubmit="return false;">

				<div class="user">
					<input required type="text" id="username" name="username"/>
					<span></span>
				</div>

				<div class="password aras-tooltip" data-tooltip-break data-tooltip="𝗖𝗮𝗽𝘀 𝗟𝗼𝗰𝗸 𝗶𝘀 𝗢𝗻 &#xa; Looks up a localized string similar to Having Caps Lock on may cause you to enter your password incorrectly. &#xa; You should press Caps Lock to turn it off before entering your password.." data-tooltip-pos="down" data-tooltip-length="large"  data-tooltip-show="false">
					<input required type="password" id="password" name="password" autocomplete="off" />
					<span></span>
				</div>

				<div class="database">
					<select required id="database_select" name="database"></select>
					<span></span>
				</div>

				<div class="database">
					<select required id="server_select" name="server"></select>
					<span></span>
				</div>

				<div class="btn-group">
					<button type="button" id="login.login_btn_label" class="aras-btn left" onclick="loginPage.login()"></button>
					<button type="button" id="login.cancel_btn_label" class="aras-btn btn_cancel right" onclick="loginPage.close()"></button>
				</div>

			</form>
			<div class="update-info">
				<a id="login.default_link_html" class="aras-link"></a>
			</div>
		</div>
		<div class="login-page__footer">
			<div class="bookmark">
				<a rel="sidebar" id="login.bookmark_this_page_txt" class="bookmark__link"></a>
			</div>
			<div class="version">
				<span id="login.version_html"></span> <!-- #include file = "include/rev.inc"-->; <span id="login.build_html"></span> <!-- #include file = "include/build.inc"-->
			</div>
		</div>
	</div>
</body>
</html>
