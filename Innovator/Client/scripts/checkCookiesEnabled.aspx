﻿<% 
Response.Cache.SetCacheability(HttpCacheability.NoCache)
Dim result As Boolean = Not Request.Cookies("test_client_cookies") Is Nothing

Dim testServerCookie As HttpCookie = New HttpCookie("test_server_cookies" , "True")
Response.Cookies.Add(testServerCookie)


If (Not Request.Cookies("test_client_cookies") Is Nothing) Then
    Dim myCookie As HttpCookie
    myCookie = New HttpCookie("test_client_cookies")
    myCookie.Expires = DateTime.Now.AddDays(-1D)
    Response.Cookies.Add(myCookie)
End If

Response.Write(result) 
%>