﻿<!-- (c) Copyright by Aras Corporation, 2006-2011. -->
<!-- #INCLUDE FILE="utils.aspx" -->
<%
	Dim qs As String = Request.ServerVariables("QUERY_STRING")
	If Not String.IsNullOrEmpty(qs) Then
		qs = "?" + qs
	End If
	Dim clConfig As ClientConfig = ClientConfig.GetServerConfig()

	Dim actionNamespace As String = clConfig.ActionNamespace
	Dim serviceName As String = clConfig.ServiceName
	Dim serviceUrl As String = clConfig.ServiceUrl
	Dim login_window_width As String = Client_Config("login_window_width").ToString()
	Dim login_window_height As String = Client_Config("login_window_height").ToString()

	If login_window_width = "" Then login_window_width = "413"
	If login_window_height = "" Then login_window_height = "339"
%>
	<script type="text/javascript">
		window.__staticVariablesStorage = true; //this is a flag for StaticVariablesStorage to initialize static variables storage here in this window.
	</script>
	<script defer type="text/javascript" src="../javascript/include.aspx?classes=StaticVariablesStorage"></script>
	<%
		Dim iomScriptSharp As String = If(HttpContext.Current.IsDebuggingEnabled, "IOM.ScriptSharp.debug", "IOM.ScriptSharp")
	%>
	<script type="text/javascript" src="../javascript/include.aspx?classes=ArasModules,MainWindow,pageReturnBlocker&files=<%=iomScriptSharp%>"></script>
	<script src="../javascript/include.aspx?classes=arasStub/dojo.js" data-dojo-config="async: true, isDebug: false, baseUrl:'../javascript/dojo'"></script>
	<script type="text/javascript" src="../javascript/dojo/layout.js" ></script>
	<script type="text/javascript">
		//these global variable is required because may be overwritten on pages that include this file.
		var documentTitle = '<%=Client_Config("product_name")%>';//document title

		var arasObjectIsReady = undefined;

		function _createArasObject()
		{
			var aras = new Aras();
			window.aras = aras;
			window.aras.Browser = new BrowserInfo();

			aras.aboutData = {
				version: {
					revision: '<!-- #include file = "rev.inc"-->',
					build: '<!-- #include file = "build.inc"-->'
				},
				msBuildNumber: '<!-- #include file = "MSBuildNumber.inc"-->',
				copyright: '<!-- #include file = "copyright.inc"-->'
			};

			aras.setCommonPropertyValue('clientRevision', "<%=ClientHttpApplication.ClientVersion%>");
		}

		function showLoginFrame() {
			var timeZoneFrame = frames.tz;
			var loginFrame = frames.main;
			if (timeZoneFrame) {
				document.body.removeChild(timeZoneFrame.frameElement);
				deepLinking.beforeInitializeLoginForm();
				loginFrame.loginPage.initialize();
				document.querySelector('#dimmer_spinner_whole_window').classList.add('disabled-spinner');
			}
		}

		function showTimeZoneSelectFrame(tzName, winTzNames) {
			var timeZoneFrame = frames.tz;
			timeZoneFrame.initialize(tzName, winTzNames);
			document.querySelector('#dimmer_spinner_whole_window').classList.add('disabled-spinner');
		}

		function onSuccessfulLogin() {
			ArasModules.intl.locale = aras.getSessionContextLocale();
			var loginFrame = frames.main.frameElement;
			frames.main.frameElement.style.display = 'none';
			window.aras.browserHelper.toggleSpinner(document, true);
			resetSelfServiceReportingSession();
			initalizeArasSetupQuery();
			document.title = documentTitle;
			tryMaximizeWindow();
			deepLinking.onSuccessfulLogin();
			aras.setCommonPropertyValue("systemInfo_CurrentLocale", aras.IomInnovator.getI18NSessionContext().getLocale());

			Promise.all([
				prepareMainWindowInfo(),
				loadConfigurableUi()
			])
				.then(function() {
					window.main = window;
					window.initialize();
					window.aras.browserHelper.toggleSpinner(document, false);
					tryMaximizeWindow();
					document.body.removeChild(loginFrame);
					window.aras.browserHelper.restoreFocusForIE(window);
				})
				.catch(function (e) {
					console.error('An error has occurred during login', e);
					window.aras.browserHelper.toggleSpinner(document, false);
				})
				.then(function() {
					return Promise.all(['ItemType', 'RelationshipType', 'List', 'Form', 'ContentTypeByDocumentItemType'].map(function(name) {
						return aras.MetadataCache.RefreshMetadata(name);
					}));
				});
		}

		function initalizeArasSetupQuery() {
			aras.setup_query = {};

			<% Dim key As String
		For Each key In Request.QueryString.AllKeys
			Response.Write("aras.setup_query[""" +
			Server.HtmlEncode(key) + """]=""" +
			Server.HtmlEncode(Request.QueryString.Item(key)) +
			""";" + vbCrLf)
		Next %>
		}

		function prepareMainWindowInfo() {
			var asyncProvider = new AsyncCachedMainWindowInfoProvider();

			return asyncProvider.fetch()
				.then(function(provider) {
					window.arasMainWindowInfo = new ArasMainWindowInfo(aras);
					window.arasMainWindowInfo.setProvider(provider);

					// load metadataDates
					var metadataDates = arasMainWindowInfo.GetAllMetadataDates;
					aras.MetadataCache.UpdatePreloadDates(metadataDates);

					aras.buildIdentityList(window.arasMainWindowInfo.getIdentityListResult);
					aras.setCommonPropertyValue('IsSSVCLicenseOk', arasMainWindowInfo.IsSSVCLicenseOk);
					aras.setCommonPropertyValue('MessageCheckInterval', arasMainWindowInfo.MessageCheckInterval);
					aras.setCommonPropertyValue('SearchCountModeException', arasMainWindowInfo.Core_SearchCountModeException);
					aras.setCommonPropertyValue('SearchCountMode', arasMainWindowInfo.SearchCountMode);
				});
		}

		function loadConfigurableUi() {
			return ModulesManager.using(['aras.innovator.CUI/ConfigurableUI'], true)
				.then(function(ConfigurableUi) {
					window.cui = new ConfigurableUi();
				});
		}

		function resetSelfServiceReportingSession() {
			var xmlHttp = new XMLHttpRequest();
			// SSR is placed near Inn. Server
			var url = aras.getServerBaseURL() + "../SelfServiceReporting/SessionAbandon.aspx?_random=" + Math.random();
			xmlHttp.open("POST", url);
			xmlHttp.send();
		}

		function tryMaximizeWindow() {
			var isInnovatorInIFrame = window.parent !== window;

			if (!isInnovatorInIFrame) {
				<% If CType(Client_config("window_resize_on_login"), String) <> "false" Then %>
				//sometimes window.moveTo and window.resizeTo throws "Access is denied" exception
				//even though a repeated call to the same method after a little time period succeed
				//this feature was introduced in SP2
				try	{
					aras.browserHelper.moveWindowTo(window, 0, 0);
					aras.browserHelper.resizeWindowTo(window, screen.availWidth, screen.availHeight);
				}
				catch (e) {
					console.error("Exception while resize and move window", e);
				}
				<% End If %>
			}
		}

		function initArasObject() {
			_createArasObject();
			var aras = window.aras;

			aras.browserHelper = new BrowserHelper(aras, window);

			//++++++++ Initialize aras components
			/// this function is called from scrits/nash.aspx and scripts/login.aspx
			aras.IomFactory = new Aras.IOM.IomFactory();
			aras.IomInnovator = aras.newIOMInnovator();
			aras.InnovatorUser = new Aras.IOM.InnovatorUser();
			aras.GUIDManager = new GUIDManager(aras);
			aras.MetadataCache = new MetadataCache(aras);// aras.IomFactory.CreateItemCache();
			aras.setCommonPropertyValue("mainWindow", window);
			aras.browserHelper.initComponents();
			aras.vault = controlWrapperFactory.createVaultWrapper(aras);
			aras.utils = controlWrapperFactory.createUtilsWrapper(aras);
			aras.XmlHttpRequestManager = new XmlHttpRequestManager(aras.utils);

			aras.arasService = aras.newObject();
			aras.arasService.actionNamespace = "<%=actionNamespace%>";
			aras.arasService.serviceName = "<%=serviceName%>";
			aras.arasService.serviceUrl = "<%=serviceUrl%>";
			var tzLabel = aras.browserHelper.tzInfo.getTimeZoneLabel();
			var winTzName = aras.browserHelper.tzInfo.getTimeZoneNameFromLocalStorage(tzLabel);

			if(winTzName) {
				aras.setCommonPropertyValue("systemInfo_CurrentTimeZoneName", winTzName);
				showLoginFrame();
			} else {
				var winTzNames = aras.browserHelper.tzInfo.getWindowsTimeZoneNames(tzLabel);

				if (winTzNames.length === 0) {
					aras.AlertError(aras.getResource("", "tz.get_currentzone_fail")).then(function() {
						aras.browserHelper.closeWindow(window);
					});
				} else if (winTzNames.length === 1) {
					aras.setCommonPropertyValue("systemInfo_CurrentTimeZoneName", winTzNames[0]);
					aras.browserHelper.tzInfo.setTimeZoneNameInLocalStorage(tzLabel, winTzNames[0]);
					showLoginFrame();
				} else if (winTzNames.length > 1) {
					showTimeZoneSelectFrame(tzLabel, winTzNames);
				}
			}
			CultureInfo.initClass();

			arasObjectIsReady = true;
		}

		function setWindowSize() {
			var loginWindowWidth = parseInt('<%=login_window_width%>', 10);
			var loginWindowHeight = parseInt('<%=login_window_height%>', 10);
			var windowHeight = loginWindowHeight + window.outerHeight - window.innerHeight;
			var windowWidth = loginWindowWidth + window.outerWidth - window.innerWidth;

			window.aras.browserHelper.resizeWindowTo(window, windowWidth, windowHeight);
		}

		window.addEventListener('DOMContentLoaded', function() {
			window.__staticVariablesStorage["XmlHttpRequestManager"] = new XmlHttpRequestManager();
			makeItCaseInsensetive(
				Object,
				Array,
				Aras.IOME.CacheableContainer,
				Aras.IOME.ItemCache,
				Aras.IOM.I18NSessionContext,
				Aras.IOM.Innovator,
				Aras.IOM.IomFactory,
				Aras.IOM.Item,
				Aras.IOM.InnovatorUser,
				Aras.IOM.InnovatorServerConnector);
		});

		window.onload = function() {
			initArasObject();
			setWindowSize();
		};

		window.onunload = function onunload_handler() {
			var arasObj = typeof aras === "undefined" ? parent.aras : aras;

			if (!arasObj) {
				return;
			}

			arasObj.getOpenedWindowsCount(true);
			arasObj.logout();
		}
	</script>
