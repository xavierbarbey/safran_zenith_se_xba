﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- #INCLUDE FILE="include/utils.aspx" -->
<!-- (c) Copyright by Aras Corporation, 2004-2011. -->
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="../styles/default.css" />

  <style type="text/css">
    body
    {
      background-color: White;
      margin: 0px;
    }
    .product
    {
      font-weight: bold;
      font-size: 18pt;
      font-family: arial, helvetica, sans-serif;
      color: #FFFFFF;
      filter: progid:DXImageTransform.Microsoft.Shadow(color='#333333', Direction=135, Strength=4);
    }
    .time
    {
    	color: blue; 
    	font-size: 10pt;
    	margin:0px;
    	height:29px;
    }
  </style>
</head>
<body>  
  <div>
	<div><img class="header-logo" src="<%=Client_Config("branding_img")%>" alt="" style="display:block;" /></div>
  </div>
</body>
</html>