﻿const rollup = require('rollup');
const rollupConfig = require('./nodejs/rollup.config');
const jsCompileHelper = require('./nodejs/jsCompileHelper');

const usingXxHashLikeFileHash = true;

const rollupPlugins = rollupConfig.plugins.concat([{
	onwrite: jsCompileHelper.getRollupWriteHandler(usingXxHashLikeFileHash)
}]);

const bundleList = jsCompileHelper.getBundleList();
const compileTests = (process.argv[2] === '-compileTests');
if (compileTests) {
	const testCasesForESCompile = require('./tests/javascriptTests').includingJSLibrares.testCasesForESCompile;
	testCasesForESCompile.forEach(function(esTestModule) {
		bundleList.push({
			input: esTestModule,
			sourcemap: 'inline',
			file: esTestModule.replace('testCases', 'testCasesCompiled'),
			plugins: rollupConfig.plugins,
		});
	});
}

bundleList.forEach(function(rollupJSModule) {
	rollup.rollup({
		input: rollupJSModule.input,
		plugins: rollupJSModule.plugins || rollupPlugins,
	}).then((bundleResult) => {
		return bundleResult.write({
			file: rollupJSModule.file,
			format: rollupConfig.output[0].format,
			sourcemap: rollupJSModule.sourcemap || rollupConfig.output[0].sourcemap,
			name: rollupJSModule.name
		});
	});
});
