﻿define(['dojo/_base/declare'],
	function(declare) {
		return declare([], {
			constructor: function(mainPage) {
				Object.defineProperty(this, 'visibleChildrenMaxCount', {
					set: function(visibleChildrenMaxCount) {
						mainPage.visibleChildrenMaxCount = visibleChildrenMaxCount;
					},
					get: function() {
						return mainPage.visibleChildrenMaxCount;
					},
					enumerable: true,
					configurable: true
				});
				this.toolbar = {
					refresh: function() {
						mainPage._updateToolbarItems();
					}
				};
				this.reload = function() {
					mainPage.reload();
				};
				this.modifyParameters = function() {
					mainPage._modifyParameters();
				};
				this.getViewDefinitionId = function() {
					return mainPage._treeGridViewDefinitionNode.getAttribute('id');
				};
				this.grid = {
					collapseLevel: function() {
						var grid = mainPage._grid;

						grid.collapseLevel(grid.getSelectedItemIDs_Experimental());
					},
					expandAll: function() {
						return mainPage.expandAll();
					}
				};

				this.getGridData = function() {
					var grid = mainPage._grid;
					if (!grid) {
						return {};
					}
					var selectedColIndex;
					var selectedRowId;
					//we need to call getPrevFocusedCell because in time of showing ContextMenu the menu is focused and grid.getFocusedCell() returns null
					var gridFocusedCell = grid.getFocusedCell() || grid.getPrevFocusedCell();
					if (gridFocusedCell && gridFocusedCell.headId) {
						selectedColIndex = grid.getColumnIndex(gridFocusedCell.headId);
						selectedRowId = gridFocusedCell.rowId;
					}

					var selectedRows = [];
					var focusInfo = [];
					var dataObject = selectedRowId && grid.getUserData(selectedRowId, 'data_object');
					var cells = [];
					var focusedCell;
					var colCount = grid.getColumnCount();
					if (dataObject) {
						for (var j = 0; j < colCount; j++) {
							var cell = {
								data: dataObject[j],
								text: grid.getCellValue(selectedRowId, j)
							};
							cells.push(cell);
							if (j === selectedColIndex) {
								focusedCell = cell;
							}
						}
					}
					var row = {
						cells: cells
					};
					selectedRows.push(row);
					if (focusedCell) {
						focusInfo.push({
							//we haven't multiselect now, but, we need this only for a case if several items was selected.
							//cellColumnIndex: selectedColIndex,
							cell: focusedCell,
							row: row
						});
					}

					return {
						selection: {
							rows: selectedRows
						},
						focus: focusInfo
					};
				};
			}
		});
	});
