﻿define(['dojo/_base/declare',
	'./TgvTreeGridFormatters',
	'dojo/_base/connect',
	'Aras/Client/Controls/Experimental/ContextMenuWrapper'],

function(declare, TgvTreeGridFormatters, connect) {
	var columnNamePrefix = 'col';
	var privateProps = {};
	var aras = parent.aras;

	return declare('TgvTreeGrid', [], {
		//this object is used for Lazy Loading.
		//it has following fields/methods:
		//function loadData(@string parentRowId),  returns: rows @object, the same object like argument of function _createRows.
		onDemandLoader: null,

		_userData: {},

		_uniqueRowKey: 1,

		_contextMenu: null,

		_prevFocusedCell: null,

		_tgvFocusedCell: null,

		constructor: function(args) {
			new TgvTreeGridFormatters(this);
			var rows = new Map();
			var head = new Map();

			this.propsId_Experimental = args.connectId;
			var counter = 1;
			while (privateProps[this.propsId_Experimental]) {
				this.propsId_Experimental = args.connectId + counter;
				counter = counter + 1;
			}
			privateProps[this.propsId_Experimental] = {
				treeGrid: new TreeGrid(document.getElementById(args.connectId), {sortable: false, multiSelect: false})
			};

			var treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			var buildBranchData = treeGrid._buildBranchData;
			this._grid = treeGrid;
			var self = this;
			// Turn off TgvTreeGrid sorting
			var handlers = treeGrid.actions.handlers;
			handlers.forEach(function(item) {
				if (item.action === 'sort') {
					item.target.removeEventListener(item.action, item.handler);
					handlers.splice(handlers.indexOf(item), 1);
				}
			});
			Object.defineProperty(treeGrid.settings, '_focusedCell', {
				get: function() { return self._tgvFocusedCell; },
				set: function(value) {
					if (!value) {
						self._prevFocusedCell = self._tgvFocusedCell;
					}
					self._tgvFocusedCell = value;
				}
			});

			//This way we may prevent bubbling of oncontextmenu event from lower level parts of grid.
			this._contextMenu = new ContextMenuWrapper(document.body);
			treeGrid._buildBranchData = function(rowId) {
				return buildBranchData.call(this, rowId).then(function(value) {
					var row = treeGrid.rows.get(rowId);
					// Restore "+" in the case when user aborts the request for a case if user clicks '+' (not Expand All/Grow) to expand node.
					//note that another fix can be an option to fix this: to do a request before expand
					// (at a moment when user clicks '+') instead of a request in loadBranchDataHandler.
					row.children = row.children || true;
					treeGrid.rows.set(rowId, row);
					return value;
				});
			};
			treeGrid.loadBranchDataHandler = function(rowId) {
				let treeGrid = privateProps[self.propsId_Experimental].treeGrid;
				let rowToExpand = treeGrid.rows.get(rowId);
				if (rowToExpand.childRowsToAdd) {
					return new Promise(function(resolve) {
						let rowsToAdd = self._createRows(rowToExpand.childRowsToAdd, rowId);
						resolve(rowsToAdd);
					});
				}
				return self.onDemandLoader
					.loadData(rowId)
					.then(function(result) {
						if (!result || result.isCancellClicked) {
							return;
						}
						let rows = result.gridRows;

						return rows ? self._createRows(rows, rowId) : null;
					});
			};

			treeGrid.getRowClasses = function(rowId) {
				if (self.getUserData(rowId, 'offset_info')) {
					return 'aras-grid-row_show-more';
				}

				return '';
			};

			var firstColumnName = columnNamePrefix + 0;
			treeGrid.getCellType = function(headId, rowId) {
				var formatter;
				var viewType = self.getUserData(rowId, self._getUserDataKeyForCell('viewType', headId));
				if (self.getUserData(rowId, self._getUserDataKeyForCell('link', headId))) {
					formatter = headId === firstColumnName && !self.getUserData(rowId, 'offset_info') ? 'iconLink' : 'link';
				} else if (viewType === 'date') {
					formatter = headId === firstColumnName ? 'iconDate' : 'date';
				} else if (viewType === 'decimal') {
					formatter = headId === firstColumnName ? 'iconDecimal' : 'decimal';
				} else if (viewType === 'color') {
					formatter = headId === firstColumnName ? 'iconColor' : 'color';
				} else if (headId === firstColumnName) {
					formatter = 'iconText';
				}
				return formatter;
			};

			head.set(firstColumnName, {});
			treeGrid.head = head;
			treeGrid.rows = rows;
			treeGrid.roots = [];
			treeGrid.settings.treeHeadView = firstColumnName;

			this._attachEventHandlers();
		},

		_expandRowWithDescendants: function(gridRowId, levelsToExpand, childRows) {
			const promises = [];
			const treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			let rowToExpand = treeGrid.rows.get(gridRowId);
			if (!rowToExpand.children) {
				return;
			}
			treeGrid.settings.expanded.delete(gridRowId);
			this._removeDescendantRows(rowToExpand.children);
			rowToExpand.children = true;

			rowToExpand.childRowsToAdd = childRows;
			treeGrid.rows.set(gridRowId, rowToExpand);
			return treeGrid.expand(gridRowId).then(function() {
				this._expandDescendantRows(childRows, levelsToExpand, promises);

				return Promise.all(promises);
			}.bind(this)).catch(function(ex) {
				aras.AlertError(ex.message);
			});
		},

		expandAll: function(levelsToExpand) {
			const selectedGridRowId = this.getSelectedItemIDs_Experimental();
			let isToExpandRootItems = false;
			if (!selectedGridRowId && selectedGridRowId !== 0) {
				isToExpandRootItems = true;
			}
			return this.onDemandLoader
					.loadData(selectedGridRowId, isToExpandRootItems ? levelsToExpand + 1 : levelsToExpand)
					.then(function(result) {
						const promises = [];
						let rows = result && result.gridRows;
						if (result && result.isCancelClicked) {
							return;
						}

						if (isToExpandRootItems) {
							this.removeAllRows_Experimental();
							if (!rows) {
								return;
							}
							this.addRows(rows);

							for (let i = 0; i < rows.length; i++) {
								let row = rows[i];
								promises.push(this._expandRowWithDescendants(row.gridRowId, levelsToExpand, row.childRows));
							}

							return Promise.all(promises);
						}
						if (rows) {
							return this._expandRowWithDescendants(selectedGridRowId, levelsToExpand, rows);
						}
					}.bind(this));
		},

		_removeDescendantRows: function(rows) {
			const treeGrid = privateProps[this.propsId_Experimental].treeGrid;

			if (!rows || rows === true) {
				return;
			}

			for (let i = 0; i < rows.length; i++) {
				let gridRowId = rows[i];
				let row = treeGrid.rows.get(gridRowId);
				this._removeDescendantRows(row.children);
				treeGrid.rows.delete(gridRowId);
				treeGrid.settings.expanded.delete(gridRowId);
			}
		},

		_expandDescendantRows: function(rows, levelsToExpand, promises) {
			const treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			let row;

			if (levelsToExpand <= 1) {
				return;
			}

			for (let i = 0; i < rows.length; i++) {
				row = rows[i];
				if (row.childRows && row.childRows.length) {
					let rowToExpand = treeGrid.rows.get(row.gridRowId);
					rowToExpand.childRowsToAdd = row.childRows;
					treeGrid.rows.set(row.gridRowId, rowToExpand);
					promises.push(this._expandRowAndCallExpandDescendantRows(row, levelsToExpand - 1, promises));
				}
			}
		},

		_expandRowAndCallExpandDescendantRows: function(row, levelsToExpand, promises) {
			const treeGrid = privateProps[this.propsId_Experimental].treeGrid;

			return treeGrid.expand(row.gridRowId).then(function() {
				this._expandDescendantRows(row.childRows, levelsToExpand, promises);
			}.bind(this)).catch(function(ex) {
				aras.AlertError(ex.message);
			});
		},

		_attachEventHandlers: function() {
			var self = this;
			var treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			var rowClickHandler = function(headId, rowId, event) {
				if (!event.target.classList || !event.target.classList.contains('aras-grid-link')) {
					return;
				}
				var itemData = self.getUserData(rowId, self._getUserDataKeyForCell('link', headId));
				if (!itemData) {
					return;
				}
				self.gridLinkClick(itemData);
			};

			var selectRowHandler = function() {
				self.onSelectRow();
			};
			var onRowContextMenu = function(headId, rowId, e) {
				self._contextMenu.rowId = rowId;
				self.gridMenuInit(rowId, headId);

				self.getMenu().menu.show({
					x: e.clientX,
					y: e.clientY
				});
				e.preventDefault();
			};

			treeGrid.on('click', rowClickHandler, 'cell');
			treeGrid.on('contextmenu', onRowContextMenu, 'cell');

			var detachMenuClickEventCallback = this._contextMenu.menu.on('click', function(command, e) {
					var rowId = self._contextMenu.rowId;

					self._contextMenu.rowId = null;

					if (this.customClickHandler) {
						this.customClickHandler(rowId);
					}

					self.gridMenuClick(command, rowId);
					self._contextMenu.onItemClick(command, rowId);
				}
			);

			treeGrid.dom.addEventListener('selectRow', selectRowHandler);

			//note that treeGrid.off requires a callback, e.g., rowClickHandler. That's why we must to declare the function such a way.
			this._detachEventHandlers = function() {
				treeGrid.off('click', rowClickHandler);
				treeGrid.off('contextmenu', onRowContextMenu);
				treeGrid.off('expand', expandHandler);
				detachMenuClickEventCallback();
				treeGrid.dom.removeEventListener('selectRow', selectRowHandler);
			};
		},

		gridLinkClick: function(itemData) {
		},

		gridMenuInit: function(rowId, headId) {
		},

		onSelectRow: function() {
		},

		gridMenuClick: function(menuItem, rowId) {
		},

		createHeader: function(headerColumns) {
			var headerColumn;
			var head = privateProps[this.propsId_Experimental].treeGrid.head;

			for (var i = 0; i < headerColumns.length; i++) {
				headerColumn = headerColumns[i];
				head.set(columnNamePrefix + i, {
					label: headerColumn.label,
					width: headerColumn.width,
					resize: true
				});
			}
		},

		addRows: function(rows, parentId, showMoreRowId) {
			//logic of this method implementation for a case if user clicks showMore was taken from treeGrid.js of 'components' Module. See '_buildBranchData' method.
			//'showMoreRowId' is used to replace the showMore row instead of deleting of it. To save logic of scroll to row after render of grid on adding new rows.
			var rowsToAdd = this._createRows(rows, parentId, showMoreRowId);
			var treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			var parentRow = parentId && treeGrid.rows.get(parentId);
			var indexRows = treeGrid.settings.indexRows;
			var position = indexRows.indexOf(showMoreRowId);
			if (position === -1) {
				position = indexRows.indexOf(parentId);
			}
			rowsToAdd.forEach(function(item, key) {
				if (key !== showMoreRowId) {
					if (!parentId) {
						treeGrid.roots.push(key);
					} else {
						parentRow.children.push(key);
						treeGrid.rows._store.set(key, item);
						position++;
						indexRows.splice(position, 0, key);
					}
				}
				treeGrid.rows.set(key, item);
			}.bind(this));
			if (parentRow) {
				treeGrid.settings.indexTreeRows[parentId] = parentRow.children;
				treeGrid.metadata = treeGrid.actions._calcRowsMetadata(treeGrid.settings.indexTreeRows);
			}
		},

		_getUserDataKeyForCell: function(key, columnName) {
			return key + '_' + columnName;
		},

		_createRows: function(rows, parentId, showMoreRowId) {
			var rowsToAdd = new Map();
			var rowId;
			var row;
			var rowToAdd;
			var cell;
			var columnName;

			for (var i = 0; i < rows.length; i++) {
				row = rows[i];
				rowToAdd = {
					children: row.hasChildren,
					parentKey: parentId
				};
				rowId = (showMoreRowId && i === 0) ? showMoreRowId : ++this._uniqueRowKey;
				row.gridRowId = rowId;
				this._userData[rowId] = {
					row_id: row.rowId,
					row_context_data: row.rowContextData && JSON.stringify(row.rowContextData),
					icon_path: row.iconPath,
					offset_info: row.offsetInfo && JSON.stringify(row.offsetInfo),
					is_referencing_item: row.isReferencingItem,
					data_object: []
				};

				for (var j = 0; j < row.cells.length; j++) {
					cell = row.cells[j];
					columnName = columnNamePrefix + j;
					rowToAdd[columnName] = cell.value || '';
					this._userData[rowId][this._getUserDataKeyForCell('link', columnName)] = cell.link;
					this._userData[rowId][this._getUserDataKeyForCell('viewType', columnName)] = cell.viewType;
					this._userData[rowId].data_object.push(cell.dataObject);
				}
				rowsToAdd.set(rowId, rowToAdd);
			}
			return rowsToAdd;
		},

		getSelectedItemIDs_Experimental: function() {
			return privateProps[this.propsId_Experimental].treeGrid.settings.selectedRows[0] || '';
		},

		getUserData: function(rowId, key) {
			return this._userData[rowId] && this._userData[rowId][key];
		},

		getParentId: function(rowId) {
			return privateProps[this.propsId_Experimental].treeGrid.rows.get(rowId).parentKey;
		},

		removeAllRows_Experimental: function() {
			const treeGrid = privateProps[this.propsId_Experimental].treeGrid;

			treeGrid.rows = new Map();
			treeGrid.roots = [];
			treeGrid.settings.indexRows = [];
			treeGrid.settings.selectedRows = [];
			treeGrid.settings.expanded = new Set();
			treeGrid.roots = [];
			this._prevFocusedCell = null;
		},

		getMenu: function() {
			return this._contextMenu;
		},

		getFocusedCell: function() {
			var treeGrid = privateProps[this.propsId_Experimental].treeGrid;

			return treeGrid.settings.focusedCell;
		},

		getPrevFocusedCell: function() {
			return this._prevFocusedCell;
		},

		getColumnCount: function() {
			return privateProps[this.propsId_Experimental].treeGrid.head._store.size;
		},

		getColumnIndex: function(headId) {
			var headers = privateProps[this.propsId_Experimental].treeGrid.head._store;
			var i = 0;
			var toReturn;
			headers.forEach(function(header, key) {
				if (key === headId) {
					toReturn = i;
				}
				i++;
			});
			return toReturn;
		},

		_getHeadId: function(columnIndex) {
			var headers = privateProps[this.propsId_Experimental].treeGrid.head._store;
			var i = 0;
			var toReturn;
			headers.forEach(function(header, key) {
				if (i === columnIndex) {
					toReturn = key;
				}
				i++;
			});
			return toReturn;
		},

		getCellValue: function(rowId, columnIndex) {
			var headId = this._getHeadId(columnIndex);
			var row = privateProps[this.propsId_Experimental].treeGrid.rows.get(rowId);
			return row[headId];
		},
		collapseLevel: function(selectedRowId) {
			var promises = [];
			var treeGrid = privateProps[this.propsId_Experimental].treeGrid;
			var gridRows = privateProps[this.propsId_Experimental].treeGrid.rows;
			var parentRow = gridRows.get(this.getParentId(selectedRowId));
			var arr = parentRow && parentRow.children || treeGrid.roots;

			arr.forEach(function(childId) {
				promises.push(treeGrid.collapse(childId));
			});

			return Promise.all(promises);
		},
		destroy: function() {
			this._detachEventHandlers();
			//TODO: destroy should be called. But, we haven't destroy function in menu now.
			//this._contextMenu.menu.destroyRecursive(false);
		}
	});
});
