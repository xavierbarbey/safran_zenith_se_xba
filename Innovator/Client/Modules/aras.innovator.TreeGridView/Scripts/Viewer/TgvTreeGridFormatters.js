﻿define([
		'dojo/_base/config',
		'dojo/_base/declare'
	], function(config, declare) {
	return declare('TgvTreeGridFormatters', [], {
		_tgvTreeGrid: null,

		constructor: function(tgvTreeGrid) {
			this._tgvTreeGrid = tgvTreeGrid;
			var self = this;
			Grid.formatters.iconText = function(headId, rowId, value) {
				return {
					children: [
						self.getIconChild(rowId),
						{
							tag: 'span',
							children: [value]
						}
					]
				};
			};

			Grid.formatters.iconLink = function(headId, rowId, value, grid) {
				return (function() {
					var result = Grid.formatters.link(headId, rowId, value, grid);
					result.children.unshift(self.getIconChild(rowId));
					return result;
				})();
			};

			Grid.formatters.date = function(headId, rowId, value) {
				return {
					children: [
						{
							tag: 'span',
							children: [config.arasContext.converter.convertFromNeutral(value, 'date', aras.getDotNetDatePattern() || 'MM/dd/yyyy')]
						}
					]
				};
			};

			Grid.formatters.iconDate = function(headId, rowId, value) {
				return (function() {
					var result = Grid.formatters.date(headId, rowId, value);
					result.children.unshift(self.getIconChild(rowId));
					return result;
				})();
			};

			Grid.formatters.decimal = function(headId, rowId, value) {
				return {
					children: [
						{
							tag: 'span',
							children: [config.arasContext.converter.convertFromNeutral(value, 'decimal', aras.getDecimalPattern())]
							//example for decimal: can be added later: aras.getDecimalPattern(aras.getItemProperty(prop, "prec"), aras.getItemProperty(prop, "scale"));)]
						}
					]
				};
			};

			Grid.formatters.iconDecimal = function(headId, rowId, value) {
				return (function() {
					var result = Grid.formatters.decimal(headId, rowId, value);
					result.children.unshift(self.getIconChild(rowId));
					return result;
				})();
			};

			Grid.formatters.color = function(headId, rowId, value) {
				return {
					attrs: {
						style: 'background-color: ' + value
					},
					children: []
				};
			};

			Grid.formatters.iconColor = function(headId, rowId, value) {
				return (function() {
					var result = Grid.formatters.color(headId, rowId, value);
					result.children.unshift(self.getIconChild(rowId));
					return result;
				})();
			};
		},

		getIconChild: function(rowId) {
			return {
				tag: 'div',
				className: 'img-container' + (this._tgvTreeGrid.getUserData(rowId, 'is_referencing_item') ? ' glyph_icon' : ''),
				children: [
					{
						tag: 'img',
						className: 'aras-grid-row-icon',
						attrs: {
							src: this._getImageSrc(rowId)
						}
					}
				]
			};
		},

		_getImageSrc: function(rowId) {
			var iconPath = this._tgvTreeGrid.getUserData(rowId, 'icon_path');
			var imagesRelPath = dojo.baseUrl + '../../cbin/';
			var src;

			if (iconPath) {
				if (iconPath.match(/\{[\s\S]*?}/)) {
					iconPath = '../images/IconTemplate.svg';
				}
				if (iconPath.indexOf('tgv=1') < 0 && iconPath.indexOf('vault:///') < 0) {
					// add suffix to avoid taking svg icon(without suffix) that already resized in browser cache.
					// (bug of IE and Edge)
					iconPath += iconPath.indexOf('?') < 0 ? '?tgv=1' : '&tgv=1';
				}
				iconPath = config.arasContext.adjustIconUrl(iconPath);
				src = (/^http.*/i.test(iconPath) ? '' : imagesRelPath) + iconPath;
			}
			return src;
		}
	});
});
