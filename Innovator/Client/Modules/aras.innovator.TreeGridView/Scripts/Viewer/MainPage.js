﻿define(['dojo/_base/declare',
	'dojo/_base/connect',
	'./PublicAPI',
	'dojo/domReady!'],

function(declare, connect, PublicAPI) {
	const aras = parent.aras;

	function fixGridSize() {
		var gridElement = getTreeGridContainer();
		aras.fixLiquidContainerHeight(document, gridElement);
		var gridHeight = window.innerHeight - toolbar_container.offsetHeight + 'px';
		gridElement.style.height = gridHeight;
		var gridErrorDiv = getTreeGridErrorContainer();
		gridErrorDiv.style.height = gridHeight;
		gridErrorDiv.style.lineHeight = gridHeight;
	}

	function getTreeGridContainer() {
		return document.getElementById('rb_tree_grid');
	}

	function getTreeGridErrorContainer() {
		return document.getElementById('rb_tree_grid_error');
	}

	window.onresize = function() {
		fixGridSize();
	};

	return declare('MainPage', [], {
		_grid: null,

		_clientControlsFactory: false,

		_treeGridViewItemTypeInfo: null,

		_treeGridViewDefinitionNode: null,

		_startCondition: null,

		_eventHandlers: [],

		_toolbar: null,

		constructor: function(args) {
			this._clientControlsFactory = args.clientControlsFactory;
			this._treeGridViewItemTypeInfo = args.treeGridViewItemTypeInfo;
			this._treeGridViewDefinitionNode = args.treeGridViewDefinitionNode;
			this._parametersProvider = args.parametersProvider || new DefaultParametersProvider();

			let _visibleChildrenMaxCount;
			Object.defineProperty(this, 'visibleChildrenMaxCount', {
				set: function(visibleChildrenMaxCount) {
					_visibleChildrenMaxCount = visibleChildrenMaxCount;
				},
				get: function() {
					return (_visibleChildrenMaxCount ||
						aras.getItemProperty(this._treeGridViewDefinitionNode, 'max_child_items'));
				},
				enumerable: true,
				configurable: true
			});
		},

		load: function(startCondition) {
			this._startCondition = startCondition;
			fixGridSize();
			this._createToolbar();
		},

		reload: function(startCondition) {
			if (startCondition) {
				this._startCondition = startCondition;
			}
			this._makeTreeGridVisible();
			this._grid.removeAllRows_Experimental();
			this._fillTree();
			this._updateToolbarItems();
		},

		destroy: function() {
			var i;
			//remove attached handlers
			for (i = 0; i < this._eventHandlers.length; i++) {
				this._eventHandlers[i].remove();
			}

			this._eventHandlers = [];

			if (this._grid) {
				this._grid.destroy();
				this._grid = null;
			}
		},

		_getRequestItemForTreeGridData: function(includeHeaders, rowId, rowContextData, offsetInfo, levelsToExpand) {
			var tgvdItem = aras.newIOMItem();
			tgvdItem.node = this._treeGridViewDefinitionNode;

			var queryItem = aras.newIOMItem('Method', 'rb_GetTreeGridData');
			queryItem.setPropertyItem('tgvd_item', tgvdItem);
			if (includeHeaders) {
				queryItem.setProperty('include_headers', 1);
			}
			var startConditionJson = JSON.stringify(this._startCondition) || '';
			queryItem.setProperty('startCondition', startConditionJson);
			queryItem.setProperty('row_id', (rowId ? rowId : ''));
			queryItem.setProperty('row_context_data', (rowContextData ? rowContextData : ''));
			const showMoreCount = this.visibleChildrenMaxCount;
			queryItem.setProperty('fetch', showMoreCount);
			queryItem.setProperty('show_more_offset_info', offsetInfo);
			queryItem.setProperty('levels_to_expand', levelsToExpand);
			return queryItem;
		},

		getTreeGridData: function(rowId, rowContextData, includeHeaders, offsetInfo, levelsToExpand) {
			var ArasModules = aras.getMostTopWindowWithAras(window).ArasModules;
			var overlay = document.getElementById('dimmer_spinner');
			var requestItem = this._getRequestItemForTreeGridData(includeHeaders, rowId, rowContextData, offsetInfo, levelsToExpand);
			var soapPromise = ArasModules.soap(requestItem.node.xml, {async: true});
			var timerId = setTimeout(function() {
				overlay.classList.remove('overlay_transparent');
			}, 500);

			overlay.classList.add('overlay_transparent');
			overlay.classList.remove('overlay_hidden');

			return Promise.race([
					soapPromise,
					new Promise(function(resolve) {
						overlay.querySelector('button').addEventListener('click', function() {
							soapPromise.abort();
							resolve({isCancelClicked: true});
						});
					})
				])
				.then(function(result) {
					clearTimeout(timerId);
					overlay.classList.add('overlay_hidden');

					if (!result || result.isCancelClicked) {
						return result;
					}

					var resultItem = ArasModules.xml.parseString(result).selectSingleNode('Item');

					var toReturn = {};
					var headers;
					var gridRows;

					if (includeHeaders) {
						headers = aras.getItemProperty(resultItem, 'header_data');
						toReturn.headers = JSON.parse(headers);
					}

					gridRows = aras.getItemProperty(resultItem, 'grid_rows');
					toReturn.gridRows = gridRows && JSON.parse(gridRows);
					return toReturn;
				})
				.catch(function() {
					this._showTreeGridLoadError();
					overlay.classList.add('overlay_hidden');
				}.bind(this));
		},

		_updateToolbarItems: function() {
			var topWindow = aras.getMostTopWindowWithAras(window);
			var contextParams = this._getCuiContextParams();
			topWindow.cui.updateToolbarItems(this._toolbar, {}, null, true, contextParams);
		},

		_createEmptyTree: function() {
			this._clientControlsFactory.createControl('TreeGridView.Scripts.Viewer.TgvTreeGrid', {
				connectId: 'rb_tree_grid'
			}, function(control) {
				this._grid = control;
				control.onDemandLoader = {
					loadData: function(parentId, levelsToExpand) {
						var rowId = control.getUserData(parentId, 'row_id');
						var rowContextData = control.getUserData(parentId, 'row_context_data');
						return this.getTreeGridData(rowId, rowContextData, null, null, levelsToExpand);
					}.bind(this)
				};
				var eventHandler = connect.connect(control, 'gridLinkClick', this, function(itemData) {
					this._onLinkClick(itemData);
				});
				this._eventHandlers.push(eventHandler);
				eventHandler = connect.connect(control, 'gridMenuInit', this, function() {
					var topWindow = aras.getMostTopWindowWithAras(window);
					var menu = control.getMenu();
					var contextItem = this._getContextItem();
					var contextParams = this._getCuiContextParams();
					menu.removeAll();
					topWindow.cui.fillPopupMenu('TGV_ContextMenu', menu, contextItem, null, null, contextParams);
					topWindow.cui.initPopupMenu(menu, contextItem, contextParams);
				});
				this._eventHandlers.push(eventHandler);

				eventHandler = connect.connect(control, 'onSelectRow', this, this._updateToolbarItems);
				this._eventHandlers.push(eventHandler);

				this._fillTree();
			}.bind(this));
		},

		expandAll: function() {
			const expandAllCountTextBox = this._toolbar.getItem('rb_MaxQueryDepth');
			const expandAllCount = +expandAllCountTextBox.getText();

			if (isNaN(expandAllCount) || expandAllCount <= 0 || parseInt(expandAllCount) !== expandAllCount) {
				return aras.AlertError(aras.getResource('../Modules/aras.innovator.TreeGridView', 'expandAll_depth_error'));
			}
			return this._grid.expandAll(expandAllCount);
		},

		_onLinkClick: function(itemData) {
			if (!itemData || typeof itemData !== 'string') {
				return;
			}

			if (itemData === 'show_more') {
				var selectedRowId = this._grid.getSelectedItemIDs_Experimental();
				var offsetInfo = this._grid.getUserData(selectedRowId, 'offset_info');
				var parentGridRowId = this._grid.getParentId(selectedRowId);
				var parentRowId = this._grid.getUserData(parentGridRowId, 'row_id');
				var rowContextData = this._grid.getUserData(parentGridRowId, 'row_context_data');
				return this
					.getTreeGridData(parentRowId, rowContextData, false, offsetInfo)
					.then(function(result) {
						if (!result || result.isCancelClicked) {
							return;
						}
						this._grid.addRows(result.gridRows, parentGridRowId, selectedRowId);
					}.bind(this));
			}
			var itemDataSplitted = itemData.replace(/'/g, '').replace(/"/g, '').split(',');
			if (itemDataSplitted.length !== 2) {
				return;
			}

			var type = itemDataSplitted[0];
			var id = itemDataSplitted[1];

			if (type && id) {
				aras.uiShowItem(type, id);
			}
		},

		_getContextItem: function() {
			return {
				itemID: this._treeGridViewDefinitionNode.getAttribute('id'),
				item: this._treeGridViewDefinitionNode,
				itemType: this._treeGridViewItemTypeInfo
			};
		},

		_getCuiContextParams: function() {
			return {
				tgvContext: new PublicAPI(this)
			};
		},

		_createToolbar: function() {
			var toolbar;
			var toolbarId;
			var topWindow = aras.getMostTopWindowWithAras(window);
			var self = this;

			var contextItem = self._getContextItem();
			var cuiContextParams = self._getCuiContextParams();

			if (!topWindow.cui._tgvCounter) {
				topWindow.cui._tgvCounter = 0;
			}

			toolbarId = 'tgvMainPageToolbar_' + topWindow.cui._tgvCounter++;
			toolbar = new ToolbarWrapper({id: toolbarId, connectId: 'toolbar_container'});
			return topWindow.cui.dataLoader.loadCommandBarAsync('TGV_Toolbar', contextItem)
							.then(function(items) {
								topWindow.cui.initToolbarEvents(toolbar, contextItem, cuiContextParams);
								Object.assign(contextItem, {
									toolbarApplet: toolbar,
									connectId: 'toolbar_container',
									toolbarId: toolbarId,
									items: items,
									contextParams: cuiContextParams
								});

								return topWindow.cui.loadToolbarFromCommandBarsAsync(contextItem);
							})
							.then(function() {
								toolbar.show();
								this._toolbar = toolbar;
								this._toolbarCreated();
								this._updateToolbarItems();
							}.bind(this))
							.catch(function(err) {
								console.error(err);
							});
		},

		_toolbarCreated: function(toolbar) {
			this._createEmptyTree();
		},

		_showTreeGridLoadError: function() {
			var gridElement = getTreeGridContainer();
			var gridErrorDiv = getTreeGridErrorContainer();
			gridErrorDiv.style.display = 'block';
			gridElement.style.display = 'none';
			var errorResourceName = 'view.tree_grid_definition_error';
			gridErrorDiv.innerText = aras.getResource('../Modules/aras.innovator.TreeGridView', errorResourceName);

		},

		_makeTreeGridVisible: function() {
			var gridElement = getTreeGridContainer();
			var gridErrorDiv = getTreeGridErrorContainer();
			gridErrorDiv.style.display = 'none';
			gridElement.style.display = '';
		},

		_fillTree: function() {
			return this
					.getTreeGridData(null, null, true)
					.then(function(result) {
						if (!result || result.isCancelClicked) {
							return;
						}
						this._grid.createHeader(result.headers);
						this._grid.addRows(result.gridRows);
					}.bind(this));
		}
	});
});
