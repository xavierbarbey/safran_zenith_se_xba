﻿var isUIControlsCreated = false;
var isViewSettingUp = false;
var viewContext;
var aras;

function initializeView(inputParameters) {
	var ownerViewPanel = frameElement.ownerViewPanel;

	inputParameters = inputParameters || {};

	aras = inputParameters.aras || parent.aras;
	viewContext = {
		data: {
			structuredDocument: null,
			panelViewSettings: null,
			copiedViewSettings: null,
			isTreeExpanded: true,
			currentUser: null,
			isPanelActive: false,
			resourceStrings: {
			}
		},
		controls: {
			ownerPanel: ownerViewPanel,
			searchControl: null,
			editorControl: null,
			treeGridAdapter: null,
			headerControls: {
				headerContainer: document.querySelector('.viewHeader'),
				statusMark: document.querySelector('.statusMark'),
				documentInfo: document.querySelector('.documentInfo'),
				editInfo: document.querySelector('.editInfo'),
				filterInfo: document.querySelector('.filterInfo')
			},
			xmlContainer: document.querySelector('#xmlContainer'),
			htmlContainer: document.querySelector('#htmlContainer'),
			xmlContentFrame: document.querySelector('.xmlContentFrame'),
			htmlContainerWidget: null,
			treeContainerWidget: null,
			treeToggleButton: document.querySelector('.treeToggleButton')
		},
		modules: {
			XmlToHTMLTransform: null,
			Enums: null
		},
		topWindow: aras.getMostTopWindowWithAras(window),
		editState: null
	};
}

function getResourceString(resourceLocation, resourceId) {
	resourceLocation = resourceLocation || '_default';

	if (resourceId) {
		var resourceStrings = viewContext.data.resourceStrings;
		var locationStrings = resourceStrings[resourceLocation] || (resourceStrings[resourceLocation] = {});
		var foundResource = locationStrings[resourceId];

		if (!foundResource || arguments.length > 2) {
			foundResource = aras.getResource.apply(aras, arguments);
			locationStrings[resourceId] = foundResource;
		}

		return foundResource;
	}
}

function setupView(viewSettings, optionalParameters) {
	optionalParameters = optionalParameters || {};

	isViewSettingUp = true;
	viewContext.data.panelViewSettings = viewSettings;

	if (!isUIControlsCreated) {
		createUIControls();
	}

	applyViewSettings(viewSettings, optionalParameters);
	setEditState(isViewEditable());
	updateViewHeader();
	isViewSettingUp = false;
}

function updateViewHeader() {
	var panelViewSettings = viewContext.data.panelViewSettings;
	var documentItem = panelViewSettings.documentItem;
	var headerControls = viewContext.controls.headerControls;
	var systemLanguages = viewContext.controls.ownerPanel.getSharedData('systemLanguages');
	var languageName = systemLanguages[panelViewSettings.languageCode];
	var isCurrent = aras.getItemProperty(documentItem, 'is_current') == '1';
	var documentInfo = '';
	var filterInfo = '';
	var familyName;

	for (familyName in panelViewSettings.filterFamilies) {
		filterInfo += (filterInfo ? ', ' : '') + panelViewSettings.filterFamilies[familyName].join(',');
	}

	filterInfo = filterInfo ? getResourceString('../Modules/aras.innovator.TDF', 'viewpaneltitle.filterprefix') + ': ' + filterInfo : '';

	documentInfo = aras.getItemProperty(documentItem, 'item_number') + ' - ' + languageName + ' - ' +
		aras.getItemProperty(documentItem, 'major_rev') + '.' + aras.getItemProperty(documentItem, 'generation') +
		(isCurrent ? ' (Current)' : '');

	headerControls.documentInfo.textContent = documentInfo;
	headerControls.editInfo.innerHTML = isViewReadonly() ? '<span class="separatorSpan"> | </span>' +
		getResourceString('../Modules/aras.innovator.TDF', 'viewpaneltitle.readonly') : '';
	headerControls.filterInfo.innerHTML = filterInfo ? '<span class="separatorSpan"> | </span>' + filterInfo : '';

	headerControls.headerContainer.setAttribute('title', documentInfo + filterInfo);
}

function setupViewDisplayType(displayType) {
	var viewControls = viewContext.controls;

	if (displayType == 'html') {
		viewControls.xmlContainer.classList.add('inactiveDisplayContainer');
		viewControls.htmlContainer.classList.remove('inactiveDisplayContainer');
	} else {
		viewControls.xmlContainer.classList.remove('inactiveDisplayContainer');
		viewControls.htmlContainer.classList.add('inactiveDisplayContainer');
	}

	viewControls.htmlContainerWidget.layout();
}

function applyViewSettings(viewSettings, optionalParameters) {
	optionalParameters = optionalParameters || {};

	if (viewSettings) {
		var viewControls = viewContext.controls;
		var viewData = viewContext.data;
		var currentSettings = viewData.copiedViewSettings || viewData.panelViewSettings;
		var isItemChanged = currentSettings.documentItem !== viewSettings.documentItem;
		var isLanguageChanged = currentSettings.languageCode !== viewSettings.languageCode;
		var isDocumentReloadRequired = isItemChanged || optionalParameters.forceReload;

		if (currentSettings.displayType !== viewSettings.displayType) {
			setupViewDisplayType(viewSettings.displayType);
		}

		viewData.structuredDocument.SuspendInvalidation();

		if (isDocumentReloadRequired) {
			viewData.structuredDocument.Reload(viewSettings.documentItem, {
				languageCode: viewSettings.languageCode,
				forceReload: optionalParameters.forceReload
			});
		} else if (isLanguageChanged) {
			viewData.structuredDocument.SwitchLanguage(viewSettings.languageCode);
		}

		switch (viewSettings.displayType) {
			case 'html':
				var optionalContentHelper = viewData.structuredDocument.OptionalContent();
				var displayTypes = viewContext.modules.Enums.DisplayType;
				var isFiltrationHidden = viewSettings.filteredContentView == 'hidden';
				var isContentFiltered = currentSettings.filteredContentView !== viewSettings.filteredContentView ||
					(isFiltrationHidden && JSON.stringify(currentSettings.filterFamilies) !== JSON.stringify(viewSettings.filterFamilies));

				optionalContentHelper.DocumentView(viewSettings.filterFamilies);
				optionalContentHelper.DisplayPreference(isFiltrationHidden ? displayTypes.Hidden : displayTypes.Inactive);

				viewControls.searchControl.show();

				if (isItemChanged || isLanguageChanged) {
					viewControls.searchControl.cleanupResults();
				} else if (isContentFiltered && viewControls.searchControl.isSearchActive()) {
					viewControls.searchControl.runSearch();
				}
				break;
			case 'xml':
				var frameDocument = viewControls.xmlContentFrame.contentDocument;
				var xmlContent = prepareXmlContent(viewData.structuredDocument.origin.ownerDocument.xml);

				viewControls.searchControl.hide();

				frameDocument.open();
				frameDocument.write(xmlContent);
				frameDocument.close();
				break;

		}

		// During Reload method call queue is dropped and start state is inited, but due to ResumeInvalidation is used for rendering
		// optimization and additional state can be appended during that, the initial state should be dropped
		if (isDocumentReloadRequired) {
			viewData.structuredDocument.QueueChanges().dropCurrentState();
		}

		viewData.structuredDocument.ResumeInvalidation();

		viewData.copiedViewSettings = Object.assign({}, viewSettings);
	}
}

function prepareXmlContent(xmlData) {
	var expressionContent = '';

	if (xmlData) {
		var xslDocument = viewContext.modules.XmlToHTMLTransform;
		var expressionDocument = new XmlDocument();

		expressionDocument.loadXML(xmlData);
		expressionContent = expressionDocument.transformNode(xslDocument);
	}

	return expressionContent;
}

function getCurrentUser() {
	return viewContext.data.currentUser || (viewContext.data.currentUser = aras.getUserID());
}

function isViewEditable() {
	var panelViewSettings = viewContext.data.panelViewSettings;
	var documentItem = panelViewSettings.documentItem;
	var documentViewSettings = viewContext.controls.ownerPanel.getSharedData('documentViewSettings');

	return documentItem && (aras.isTempEx(documentItem) || (aras.getItemProperty(documentItem, 'locked_by_id') === getCurrentUser() &&
		panelViewSettings.languageCode === documentViewSettings.defaultLanguageCode));
}

function isViewReadonly() {
	var panelViewSettings = viewContext.data.panelViewSettings;
	var documentItem = panelViewSettings.documentItem;
	var documentViewSettings = viewContext.controls.ownerPanel.getSharedData('documentViewSettings');

	return !documentItem || aras.getItemProperty(documentItem, 'is_current') !== '1' ||
		panelViewSettings.languageCode !== documentViewSettings.defaultLanguageCode;
}

function setEditState(newEditState) {
	if (viewContext.editState !== newEditState) {
		viewContext.editState = Boolean(newEditState);

		if (isUIControlsCreated) {
			var editorControl = viewContext.controls.editorControl;

			editorControl.set('disabled', !newEditState);
		}
	}
}

function toggleTreeControl() {
	var viewData = viewContext.data;
	var viewControls = viewContext.controls;
	var containerWidget = viewControls.treeContainerWidget;
	var containerDomNode = containerWidget.domNode;

	viewData.isTreeExpanded = !viewData.isTreeExpanded;

	if (viewData.isTreeExpanded) {
		containerDomNode.style.width = viewData.treeOriginWidth + 'px';
		containerDomNode.classList.remove('collapsed');
	} else {
		viewData.treeOriginWidth = viewControls.treeContainerWidget.domNode.offsetWidth;
		containerDomNode.classList.add('collapsed');
	}

	if (containerWidget._splitterWidget) {
		containerWidget._splitterWidget.domNode.style.display = viewData.isTreeExpanded ? '' : 'none';
	}

	viewControls.htmlContainerWidget.layout();
}

function createUIControls() {
	require([
		'dojo/parser',
		'TechDoc/Aras/Client/Controls/TechDoc/Editor',
		'TechDoc/Aras/Client/Controls/TechDoc/Toolbar',
		'TechDoc/Aras/Client/Controls/TechDoc/UI/EditorViewTreeGrid/TreeGridAdapter',
		'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/StructuredDocument',
		'TechDoc/Aras/Client/Controls/TechDoc/UI/ToolbarParser',
		'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/DocumentationEnums',
		'dojo/aspect',
		'dojox/html/entities',
		'TechDoc/Aras/Client/Controls/TechDoc/UI/Search/SearchComponent',
		'TechDoc/Aras/Client/Controls/TechDoc/UI/Search/TextSchemaElementsSearchEngine',
		'dojo/text!TDF/Styles/XMLtoHTML.xsl',
		'dojo/domReady!'
	],
	function(parser, TechDocHtmlEditor, TechDocHtmlToolbar, TreeGridAdapter, TechDocStructuredDocument, ToolbarParser, Enums, aspect,
		entities, SearchComponent, TextSearchEngine, TranformXSL) {
		var viewControls = viewContext.controls;
		var viewData = viewContext.data;
		var panelViewSettings = viewData.panelViewSettings;
		var tdfSettings = viewControls.ownerPanel.getSharedData('tdfSettings');
		var xslDocument = new XmlDocument();
		var documentViewSettings = viewControls.ownerPanel.getSharedData('documentViewSettings');
		var toolbarParsedData;
		var structuredDocument;
		var toolbarDomNode;
		var editorControl;
		var searchControl;
		var optionalContentHelper;

		xslDocument.loadXML(TranformXSL);

		if (aras.Browser.isIe()) {
			xslDocument.setProperty('SelectionNamespaces', 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform"');
		} else {
			xslDocument.documentElement.setAttribute('xmlns:xsl', 'http://www.w3.org/1999/XSL/Transform');
		}

		viewContext.modules.XmlToHTMLTransform = xslDocument;
		viewContext.modules.Enums = Enums;

		structuredDocument = new TechDocStructuredDocument({
			aras: aras,
			item: panelViewSettings.documentItem,
			defaultLanguageCode: documentViewSettings.defaultLanguageCode,
			currentLanguageCode: panelViewSettings.languageCode,
			contentBuilderMethod: tdfSettings.contentBuilderMethod,
			optionFamilies: tdfSettings.optionFamilies,
			optionFamiliesBuilderMethod: tdfSettings.optionFamiliesBuilderMethod,
			referenceRelationshipNames: tdfSettings.referenceRelationshipNames
		});
		viewContext.data.structuredDocument = structuredDocument;
		parser.parse();

		toolbarParsedData = new ToolbarParser({
			xml: viewControls.ownerPanel.getSharedData('configurableUIData'),
			viewmodel: structuredDocument,
			aras: aras,
			toolbarContainerName: 'HtmlRichTextEditor',
			presentationConfigurationId: tdfSettings.presentationConfigurationId
		});
		toolbar = new TechDocHtmlToolbar({
			id: 'toolbar',
			connectId: 'toolbarContainer',
			toolbarObj: toolbarParsedData,
			structuredDocument: structuredDocument,
			additionalData: {
				documentViewSettings: documentViewSettings,
				isPrimary: panelViewSettings.isPrimaryView
			}
		});

		toolbar.startup();
		toolbarDomNode = toolbar.getCurrentToolBarDomNode_Experimental();

		searchControl = new SearchComponent({
			containerNode: toolbarDomNode,
			searchSource: structuredDocument,
			searchEngine: new TextSearchEngine(),
			collapseOnSpaceLack: true,
			resourceStrings: {
				placeholderText: getResourceString('../Modules/aras.innovator.TDF', 'search.placeholderText'),
				prevButtonTitle: getResourceString('../Modules/aras.innovator.TDF', 'search.prevButtonTitle'),
				nextButtonTitle: getResourceString('../Modules/aras.innovator.TDF', 'search.nextButtonTitle'),
				toggleButtonTitle: getResourceString('../Modules/aras.innovator.TDF', 'search.toggleButtonTitle'),
				toggleButtonActiveTitle: getResourceString('../Modules/aras.innovator.TDF', 'search.toggleButtonActiveTitle'),
				noMatchesLabel: getResourceString('../Modules/aras.innovator.TDF', 'search.noMatchesLabel'),
				notEnoughSpaceAlert: getResourceString('../Modules/aras.innovator.TDF', 'search.notEnoughSpaceAlert')
			}
		});

		aspect.after(structuredDocument, 'onSelectionChanged', function() {
			searchControl.adjustControlPlacement();
		}, true);

		editorControl = dijit.byId('techDocHtmlEditor');
		editorControl.setSearchControl(searchControl);

		var treeGridAdapter = new TreeGridAdapter({
			connectId: 'techDocTree',
			viewModel: structuredDocument
		});

		viewControls.searchControl = searchControl;
		viewControls.editorControl = editorControl;
		viewControls.treeGridAdapter = treeGridAdapter;
		viewControls.htmlContainerWidget = dijit.byId('htmlContainer');
		viewControls.treeContainerWidget = dijit.byId('treeContainer');

		optionalContentHelper = structuredDocument.OptionalContent();
		optionalContentHelper.addEventListener(window, null, 'onDisplayChanged', function(displayType) {
			if (!isViewSettingUp) {
				var viewSettings = viewContext.data.panelViewSettings;
				var isFiltrationHidden = viewSettings.filteredContentView == 'hidden';

				if (isFiltrationHidden && searchControl.isSearchActive()) {
					searchControl.runSearch();
				}
			}
		});

		aspect.before(structuredDocument, 'OnInvalidate', function(sender, earg) {
			var isItemChange = (earg.invalidationList.length != 0);

			if (isItemChange) {
				var currentLanguage = structuredDocument.CurrentLanguageCode();
				var currentDomXml = structuredDocument.origin.ownerDocument.xml;

				if (structuredDocument.IsEqualEditableLevel(Enums.EditLevels.IgnoreExternal)) {
					var savedDomXml = structuredDocument.getSavedDocumentXml(currentLanguage);
					var oldContent = GetContentPartFromXml(savedDomXml);
					var newContent = GetContentPartFromXml(currentDomXml);

					// comparing only old and new content nodes, this will prevent from setting "isDirty" attribute if
					// only refences were updated during reload event
					if (oldContent.length != newContent.length || oldContent != newContent) {
						var structureDocumentItem = structuredDocument.getDocumentItem();

						structuredDocument.saveDocumentXml(currentDomXml, currentLanguage);
						searchControl.cleanupResults();

						if (!aras.isDirtyEx(structureDocumentItem)) {
							structureDocumentItem.setAttribute('isDirty', '1');
							structureDocumentItem.setAttribute('action', 'update');

							if (viewContext.topWindow.updateItemsGrid) {
								viewContext.topWindow.updateItemsGrid(structureDocumentItem);
							}
						}
					}
				}
				var customEvent = document.createEvent('Event');
				customEvent.initEvent('change:item', true, true);
				window.dispatchEvent(customEvent);
			}
		}, true);

		viewControls.treeToggleButton.addEventListener('click', function(clickEvent) {
			toggleTreeControl();
			clickEvent.stopPropagation();
		});
		viewControls.treeContainerWidget.domNode.addEventListener('click', function() {
			if (!viewData.isTreeExpanded) {
				toggleTreeControl();
			}
		});

		registerViewShortcuts();
		attachActivationEventHandlers();
		attachContextMenuEventHanlers();
		attachViewControllerEventHandlers();
		setupViewDisplayType(panelViewSettings.displayType);

		document.body.classList.add('viewLoaded');
		isUIControlsCreated = true;
	});
}

function registerViewShortcuts() {
	var searchControl = viewContext.controls.searchControl;

	// ctrl+f shortcut registration
	aras.shortcutsHelperFactory.getInstance(window).subscribe({shortcut: 'ctrl+f', preventBlur: true, handler: function() {
		if (!searchControl.isActive()) {
			searchControl.setActiveState(true);

			setTimeout(function() {
				searchControl.focus();
			}, 200);
		} else {
			searchControl.focus();
		}
	}, context: window}, true);
}

function attachActivationEventHandlers() {
	var editorControl = viewContext.controls.editorControl;
	var xmlFrameControl = viewContext.controls.xmlContentFrame;

	document.addEventListener('click', function() { setViewActiveState(true);});
	document.addEventListener('focusin', function() { setViewActiveState(true);});

	editorControl.iframe.contentDocument.addEventListener('focusin', function() { setViewActiveState(true);});

	xmlFrameControl.addEventListener('load', function() {
		xmlFrameControl.contentDocument.addEventListener('mousedown', function(mouseEvent) { setViewActiveState(true);});
	});
}

function attachViewControllerEventHandlers() {
	var ownerPanel = viewContext.controls.ownerPanel;
	var viewController = ownerPanel.viewController;

	viewController.addEventListener(window, null, 'onViewPanelActivated', function(targetPanel) {
		if (targetPanel !== ownerPanel) {
			setViewActiveState(false);
		}
	});
}

function attachContextMenuEventHanlers() {
	var xmlFrameControl = viewContext.controls.xmlContentFrame;

	document.addEventListener('contextmenu', function(menuEvent) {
		menuEvent.preventDefault();
		menuEvent.stopPropagation();
	});

	xmlFrameControl.addEventListener('load', function() {
		xmlFrameControl.contentDocument.addEventListener('contextmenu', function(menuEvent) {
			menuEvent.preventDefault();
			menuEvent.stopPropagation();
		});
	});
}

function setViewActiveState(isFocused) {
	var headerControls = viewContext.controls.headerControls;
	var viewData = viewContext.data;

	if (viewData.isPanelActive !== isFocused) {
		viewData.isPanelActive = isFocused;

		if (isFocused) {
			var ownerPanel = viewContext.controls.ownerPanel;

			headerControls.headerContainer.classList.add('active');
			ownerPanel.raiseEvent('onPanelActivated', ownerPanel, viewContext.data.panelViewSettings);
		} else {
			headerControls.headerContainer.classList.remove('active');
		}
	}
}

function GetContentPartFromXml(documentXml) {
	var contentNodeXPath = 'aras:document/aras:content';
	var contentNode;

	if (typeof documentXml == 'string') {
		var tempDocument = new XmlDocument();

		tempDocument.preserveWhiteSpace = true;
		tempDocument.loadXML(documentXml);

		if (dojo.isIE || aras.Browser.isIe()) {
			tempDocument.setProperty('SelectionNamespaces', 'xmlns:aras="http://aras.com/ArasTechDoc"');
		} else {
			tempDocument.documentElement.setAttribute('xmlns:aras', 'http://aras.com/ArasTechDoc');
		}
		contentNode = tempDocument.selectSingleNode(contentNodeXPath);
	} else if (typeof documentXml == 'object') {
		contentNode = documentXml.selectSingleNode(contentNodeXPath);
	}

	return contentNode.xml || '';
}
