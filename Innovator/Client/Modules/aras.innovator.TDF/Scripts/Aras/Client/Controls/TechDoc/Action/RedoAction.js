﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.RedoAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function() {
			this._viewmodel.QueueChanges().Redo();
		}
	});
});
