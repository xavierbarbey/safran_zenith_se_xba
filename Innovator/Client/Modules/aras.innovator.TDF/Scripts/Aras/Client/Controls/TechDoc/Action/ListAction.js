﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.ListAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var _viewmodel = this._viewmodel;
			var selected = _viewmodel.GetSelectedItems();

			if (selected.length == 1) {
				var item = selected[0];

				if (item.is('ArasListXmlSchemaElement')) {
					item.SetNewStyleOfList(context.listtype);
				} else if (item.is('ArasListItemXmlSchemaElement')) {
					item.List().SetNewStyleOfList(context.listtype);
				}
			}
		}
	});
});
