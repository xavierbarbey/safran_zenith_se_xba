﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase',
	'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/DocumentationEnums'
],
function(declare, ActionBase, Enums) {
	return declare('Aras.Client.Controls.TechDoc.Action.ArasTextActions', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var cursor = this._viewmodel.Cursor();
			var ancesstorItem = cursor.commonAncestor;

			if (ancesstorItem && ancesstorItem.is('ArasTextXmlSchemaElement')) {
				var isEmphesExist = ancesstorItem.IsHasEmphes();
				var action = context.actionName;

				switch (action) {
					case 'addlink':
					case 'changelink':
						if (isEmphesExist) {
							this._CallLinkDialog(ancesstorItem);
						}
						break;
					case 'testlink':
						if (isEmphesExist) {
							this._CallLinkDialog(ancesstorItem, true);
						}
						break;
					case 'deletelink':
						if (isEmphesExist) {
							ancesstorItem.DeleteLink();
						}
						break;
					default: // "bold", "italic", "strike", "sub", "sup", "under"
						ancesstorItem.SetStyleForRange(action);
						break;
				}
			}
		},

		_CallLinkDialog: function(textElement, isViewOnly) {
			var suid = textElement.selection.From();
			var euid = textElement.selection.To();

			if (suid.id == euid.id) {
				var emph = textElement.GetEmphObjectById(suid.id);
				var linkData = emph.Link();
				var topWindow = this.actionsHelper.topWindow;

				if (linkData.type == Enums.LinkTypes.Url && isViewOnly) {
					// if it is "url link" in "view" mode, then just show link target in new window
					topWindow.open(linkData.url);
				} else {
					// in all other cases user will see linkDialog
					var viewModel = textElement.ownerDocument;
					var linkUrl = '../../Modules/aras.innovator.TDF/LinkEditorDialog';
					var param = {
						aras: this.aras,
						title: this.aras.getResource('../Modules/aras.innovator.TDF', 'action.linkeditordialog'),
						thisItem: viewModel._item,
						linkData: emph.Link(),
						lang: viewModel.CurrentLanguageCode(),
						dialogHeight: topWindow.innerHeight * 0.95,
						dialogWidth: topWindow.innerWidth * 0.85,
						tdfSettings: {
							contentBuilderMethod: viewModel.getDataRequestSetting('contentBuilderMethod')
						},
						content: linkUrl
					};

					if (!isViewOnly) {
						param.elementsSearchList = this._GetLinkSearchElementList().slice(0);
					} else {
						param.content += '?viewonly=true';
					}

					var linkEditorDialog = topWindow.ArasModules.Dialog.show('iframe', param);

					if (!isViewOnly) {
						linkEditorDialog.promise.then(function(/*Object*/ args) {
							var linkType;
							var linkData;
							if (!args) {
								return;
							}
							linkType = args.linkType;
							linkData = args.linkData;
							switch (linkType) {
								case 'external_document':
									if (linkData.elementId && linkData.blockId) {
										var currentDocumentId = viewModel.getDocumentProperty('id');

										textElement.LinkingText({
											type: linkData.blockId === currentDocumentId ? Enums.LinkTypes.InternalElement : Enums.LinkTypes.ExternalDocument,
											blockId: linkData.blockId,
											elementId: linkData.elementId,
											set: true
										});
									}
									break;
								case  'url':
									if (linkData.url) {
										textElement.LinkingText({type: Enums.LinkTypes.Url, url: linkData.url, set: true});
									}
									break;
								case 'deleteLink':
									textElement.DeleteLink();
									break;
							}
						});
					}
				}
			}
		},

		_GetLinkSearchElementList: function() {
			if (!this._linkSearchElementList) {
				this._linkSearchElementList = this._viewmodel.Schema().GetExpectedElements(this._viewmodel.Dom()).insert || [];
			}

			return this._linkSearchElementList;
		}
	});
});
