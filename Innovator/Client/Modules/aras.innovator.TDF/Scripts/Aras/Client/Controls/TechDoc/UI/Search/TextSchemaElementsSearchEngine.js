﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/UI/Search/SearchEngine',
	'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/DocumentationEnums'
],
function(declare, SearchEngineBase, Enums) {
	return declare([SearchEngineBase], {
		constructor: function(initialParameters) {
			initialParameters = initialParameters || {};
		},

		_initializeSearchContext: function(searchValue) {
			var escapedValue = searchValue.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			var searchContext = this.inherited(arguments);

			searchContext.regExp = new RegExp(escapedValue, 'gi');
		},

		_scanSource: function(sourceViewModel) {
			var foundMatches = [];

			if (sourceViewModel) {
				var allSchemaElements = sourceViewModel.getAllElements();
				var textSchemaElements = allSchemaElements.filter(function(schemaElement) {
					var isTextElement = schemaElement.is('ArasTextXmlSchemaElement') || schemaElement.is('XmlSchemaText');

					if (isTextElement) {
						var actualSchemaElement = schemaElement.is('XmlSchemaText') ? schemaElement.Parent : schemaElement;

						return !actualSchemaElement || !actualSchemaElement.isHidden();
					}
				});
				var searchRegExp = this._searchContext.regExp;
				var currentTextElement;
				var textContent;
				var currentMatch;
				var i;

				for (i = 0; i < textSchemaElements.length; i++) {
					currentTextElement = textSchemaElements[i];
					textContent = currentTextElement.is('ArasTextXmlSchemaElement') ? currentTextElement.GetTextAsString() : currentTextElement.Text();

					while ((currentMatch = searchRegExp.exec(textContent)) !== null) {
						foundMatches.push({
							schemaElement: currentTextElement,
							firstIndex: currentMatch.index,
							lastIndex: searchRegExp.lastIndex
						});
					}
				}
			}

			return foundMatches;
		}
	});
});
