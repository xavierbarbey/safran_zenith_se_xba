﻿(function() {
	function applyEventHandler(handler) {
		var events = ['click', 'dblclick', 'focusin', 'focusout', 'select', 'change', 'keydown', 'keypress'];
		for (var i = 0; i < events.length; i++) {
			var eventName = events[i];
			this.dom.addEventListener(eventName, handler);
		}
	}

	function applyEvents(eventList) {
		var events = eventList.split(',');
		for (var i = 0; i < events.length; i++) {
			var event = events[i].split(':');
			var callback = window[event[1]];
			var eventName = event[0];
			if (eventName === 'focus') {
				eventName = 'focusin';
			}
			if (eventName === 'blur') {
				eventName = 'focusout';
			}
			if (callback && typeof callback === 'function') {
				this.on(eventName, callback);
			}
		}
	}

	function runHandler(evt, handler) {
		try	{
			if (handler.bind(evt.target)(evt) === false) {
				return false;
			}
		}	catch (exp) {
			var errorMsg = aras.getResource('','ui_methods_ex.event_handler_failure_msg', exp.description ? exp.description : exp);
			aras.AlertError(aras.getResource('','ui_methods_ex.event_handler_failed'), errorMsg, aras.getResource('','common.client_side_err'));
			return false;
		}
	}

	function Field(container, component, eventList) {
		if (!container) {
			return;
		}
		this.dom = container;
		this.component = component;
		this.events = {};
		this.init();
		document.addEventListener('DOMContentLoaded', function() {
			applyEvents.call(this, eventList);
		}.bind(this));
	}

	Field.prototype = {
		init: function() {
			applyEventHandler.call(this, this._eventHandler.bind(this));
			this.component.render().then(function() {
				var zIndex = null;
				var dropdown = this.dom.querySelector('.aras-dropdown');
				var container = this.dom.closest('fieldset').parentNode;

				if (!dropdown || !container) {
					return;
				}

				var observer = new MutationObserver(function(mutations) {
					mutations.forEach(function(mutation) {
						var visibility = mutation.target.style.display;
						if (mutation.oldValue.indexOf(visibility) !== -1) {
							return;
						}

						var visible = visibility === 'block';
						if (visible && zIndex === null) {
							zIndex = container.style.zIndex;
						}

						container.style.zIndex = visible ? 999 : zIndex;
					});
				});

				observer.observe(dropdown, {
					attributes: true,
					attributeOldValue: true,
					attributeFilter: ['style']
				});
			}.bind(this));
		},
		on: function(event, callback) {
			var events = this.events;
			if (!events[event]) {
				events[event] = new Set();
			}
			if (!events[event].has(callback)) {
				events[event].add(callback);
				return function() {
					events[event].delete(callback);
				};
			}
		},
		setValue: function(val) {
			this.component.setState({value: val});
		},
		getValue: function() {
			return this.component.state.value || '';
		},
		setDisabled: function(bool) {
			this.component.setState({disabled: bool});
		},
		setReadOnly: function(bool) {
			this.component.setState({readonly: bool});
		},
		_eventHandler: function(evt) {
			const target = evt.target;
			if (target === this.component || target.tagName === 'INPUT' || (target.tagName === 'SPAN' && !target.classList.contains('aras-btn'))) {
				const eventHandlers = this.events[evt.type] || [];
				eventHandlers.forEach(function(handler) {
					if (typeof handler === 'function') {
						if (runHandler(evt, handler) === false) {
							return false;
						}
					}
				});
				if (evt.type === 'keydown' || evt.type === 'keypress') {
					if (evt.key === 'Enter') {
						evt.preventDefault();
					}
				}
			}
		}
	};

	window.Field = Field;
})();
