﻿(function(ArasCore) {
	const getAras = function() {
		return window.aras;
	};

	const xmlModule = ArasModules.xml;
	const storage = {};
	const faultToJSON = ArasModules.aml.faultToJSON;
	const hashModes = {
		true: 'sha256',
		false: 'md5'
	};

	const passwordExpiredConst = 'SOAP-ENV:Server.Authentication.PasswordIsExpired';

	function showDialog(type, addinArgs) {
		var arasObj = getAras();
		var data = {
			aras: arasObj,
			dialogWidth: 300,
			dialogHeight: 180,
			center: true,
			content: 'changeMD5Dialog.html'
		};

		Object.assign(data, addinArgs);

		if (type === 'passwordExpired') {
			Object.assign(data, {
				title: arasObj.getResource('', 'common.pwd_expired'),
				oldMsg: arasObj.getResource('', 'common.old_pwd'),
				newMsg1: arasObj.getResource('', 'common.new_pwd'),
				newMsg2: arasObj.getResource('', 'common.confirm_pwd'),
				errMsg1: arasObj.getResource('', 'common.old_pwd_wrong'),
				errMsg2: arasObj.getResource('', 'common.check_pwd_confirmation'),
				errMsg3: arasObj.getResource('', 'common.pwd_empty'),
				check_empty: true
			});
		}

		return ArasModules.Dialog.show('iframe', data).promise;
	}

	function getPasswordPolicies(msgValue) {
		var xml = xmlModule.parseString('<res>' + msgValue + '</res>');
		var obj = ArasModules.xmlToJson(xml);
		var items = obj.res.Item;
		var variables = '<Result>';
		var methodCode = '';

		for (var i = 0, len = items.length; i < len; i++) {
			if (items[i]['@attrs'].type === 'Variable') {
				variables += '<Item type="Variable" id=' + items[i]['@attrs'].id + '>' +
					'<name>' + items[i].name + '</name>' +
					'<value>' + items[i].value + '</value>' + '</Item>';
			} else if (items[i]['@attrs'].type === 'Method') {
				methodCode = items[i]['method_code'];
			}
		}

		variables += '</Result>';

		return {
			'code_to_check_pwd_policy': methodCode,
			'vars_to_check_pwd_policy': variables
		};
	}

	var hash = function(password, hashType) {
		switch (hashType) {
			case 'md5':
				return Promise.resolve(ArasModules.cryptohash.MD5(password).toString());
			case 'sha256':
				return ArasModules.cryptohash.SHA256(password);
			default:
				return Promise.resolve(password);
		}
	};

	var userMethods = {
		get id() {
			return storage.id;
		},
		get database() {
			return storage.database;
		},
		get type() {
			return storage.type;
		},
		get loginName() {
			return storage.loginName;
		},
		login: function(loginName, password, database, hashType, serverUrl) {
			const arasObj = getAras();
			const args = {
				loginName: loginName,
				password: password,
				database: database,
				hashType: hashType,
				serverUrl: serverUrl,
				timezoneName: aras.getCommonPropertyValue('systemInfo_CurrentTimeZoneName')
			};

			return hash(password, hashType)
				.then(function(hashPassword) {
					let grantType;
					let options;
					if (hashPassword !== null) {
						grantType = 'passwordGrant';
						options = {
							loginName: loginName,
							hashPassword: hashPassword,
							database: database
						};
					} else {
						grantType = 'logonHookPasswordGrant';
						options = {
							scriptsUrl: arasObj.getScriptsURL(),
							database: database
						};
					}
					return arasObj.OAuthClient.login(grantType, options);
				})
				.then(function() {
					const options = {
						url: serverUrl,
						method: 'ApplyItem',
						headers: {
							'TIMEZONE_NAME': args.timezoneName
						}
					};

					return ArasModules.soap(null, options);
				})
				.then(function() {
					return userMethods.validate(serverUrl, args.timezoneName);
				})
				.then(function(res) {
					var obj = ArasModules.xmlToJson(res);

					arasObj.setCommonPropertyValue('ValidateUserXmlResult', res.parentNode.parentNode.xml);
					storage.loginName = loginName;
					storage.database = database;
					storage.id = obj.id;
					storage.type = obj['user_type'];
				})
				.catch(function(errorRes) {
					if (!errorRes) {
						return Promise.reject(arasObj.getResource('', 'aras_object.validate_user_failed_communicate_with_innovator_server'));
					}

					if (errorRes.error) {
						const errorDescription = errorRes.error_description || errorRes.error;
						if (errorRes.error.indexOf('incompatible_hash') === 0) {
							return userMethods.errorHandlers.incorrectHash(errorRes.error, args, errorDescription);
						}
						return Promise.reject(errorDescription);
					}

					var promise = null;
					var faultObj = faultToJSON(errorRes.responseXML);

					if (!faultObj) {
						return Promise.reject(arasObj.getResource('', 'aras_object.validate_user_wrong_innovator_sever_response'));
					}

					var faultCode = faultObj.faultcode;

					if (faultCode === passwordExpiredConst) {
						promise = userMethods.errorHandlers.passwordExpired(faultObj, args);
					}

					return promise || Promise.reject({
						type: 'soap',
						data: new SOAPResults(aras, errorRes.responseText)
					});
				});
		},
		validate: function(serverUrl, timezoneName) {
			const options = {
				url: serverUrl,
				method: 'ValidateUser',
				async: true,
				headers: {
					'TIMEZONE_NAME': timezoneName
				}
			};

			var arasObj = getAras();
			Object.assign(options.headers, arasObj.OAuthClient.getAuthorizationHeader());

			return ArasModules.soap('', options);
		},
		errorHandlers: {
			incorrectHash: function(error, args, faultstring) {
				const arasObj = getAras();
				const mode = (error === 'incompatible_hash_use_md5') ? 'md5' : 'sha256';
				const isFipsMode = arasObj.getVariable('fips_mode') || 'false';

				if (hashModes[isFipsMode] !== mode) {
					arasObj.setVariable('fips_mode', isFipsMode === 'true' ? 'false' : 'true');
					return userMethods.login(args.loginName, args.password, args.database, hashModes[arasObj.getVariable('fips_mode')], args.serverUrl);
				}

				return Promise.reject(faultstring);
			},
			passwordExpired: function(faultObj, args) {
				var arasObj = getAras();
				var messageObj = faultObj.detail.message;
				var data = {};

				if (messageObj && ('key' in messageObj['@attrs']) && messageObj['@attrs'].key === 'password_validation_info') {
					data = getPasswordPolicies(messageObj['@attrs'].value);
				}

				data.validationCallback = function(oldPasswordHash, newPasswordHash) {
					const requestData = (
						'<old_password>' + oldPasswordHash + '</old_password>' +
						'<new_password>' + newPasswordHash + '</new_password>'
					);

					const options = {
						url: args.serverUrl,
						method: 'ChangeUserPassword',
						async: true
					};

					const handleError = function(errorRes) {
						const item = arasObj.newIOMItem();
						item.loadAML(errorRes.responseText);
						return ArasModules.Dialog.alert(item.getErrorString())
							.then(function() {
								return false;
							});
					};

					return ArasModules.soap(requestData, options)
						.then(function() {
							return true;
						})
						.catch(handleError);
				};

				return showDialog('passwordExpired', data)
					.then(function(newPasswordHash) {
						if (newPasswordHash === undefined) {
							return Promise.reject(arasObj.getResource('', 'aras_object.new_password_not_set'));
						}

						return userMethods.login(args.loginName, newPasswordHash, args.database, null, args.serverUrl);
					});
			}
		}
	};

	ArasCore = Object.assign(ArasCore, {'user': userMethods});
	window.ArasCore = window.ArasCore || ArasCore;
})(window.ArasCore || {});
