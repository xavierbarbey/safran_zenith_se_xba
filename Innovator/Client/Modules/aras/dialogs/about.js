﻿(function(externalParent) {
	if (!window.ArasModules.Dialog) {
		return;
	}

	var about = function() {
		var dialogTitle = aras.getResource('', 'aras_object.about_aras_innovator');
		var data = aras.aboutData;
		var aboutDialog = new ArasModules.Dialog('html', {
			title: dialogTitle
		});

		var content = document.createElement('div');
		var logoImage = document.createElement('img');
		logoImage.src = aras.getBaseURL('/images/aras-innovator.svg');
		var version = document.createElement('p');
		var msBuild = version.cloneNode();
		var supportLink = version.cloneNode();
		var arasLink = version.cloneNode();
		var copyright = version.cloneNode();
		var okBtn = document.createElement('button');
		content.appendChild(logoImage);
		content.appendChild(version);
		content.appendChild(msBuild);
		content.appendChild(supportLink);
		content.appendChild(arasLink);
		content.appendChild(copyright);
		content.classList.add('aras-dialog-about__content');

		version.textContent = 'Aras Innovator Version ' + data.version.revision + '  Build: ' + data.version.build;
		msBuild.textContent = 'MS Build Number: ' + data.msBuildNumber;
		var link = document.createElement('a');
		link.href = link.textContent = 'http://www.aras.com/support/';
		link.target = '_blank';
		link.classList.add('aras-link');
		supportLink.textContent = 'For support please go to ';
		supportLink.appendChild(link);

		link = link.cloneNode(true);
		link.href = link.textContent = 'http://www.aras.com';
		arasLink.textContent = 'Visit us on-line at ';
		arasLink.appendChild(link);

		copyright.textContent = data.copyright;

		okBtn.autofocus = true;
		okBtn.classList.add('aras-btn', 'aras-btn-right');
		okBtn.textContent = aras.getResource('', 'common.ok');
		okBtn.addEventListener('click', aboutDialog.close.bind(aboutDialog));

		aboutDialog.dialogNode.appendChild(content);
		aboutDialog.dialogNode.appendChild(okBtn);
		aboutDialog.dialogNode.classList.add('aras-dialog-about');

		aboutDialog.show();

		return aboutDialog.promise;
	};

	externalParent.Dialogs = externalParent.Dialogs || {};
	externalParent.Dialogs.about = about;
	window.ArasCore = window.ArasCore || externalParent;
})(window.ArasCore || {});
