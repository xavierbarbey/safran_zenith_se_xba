﻿(function(externalParent) {
	if (!window.ArasModules.Dialog) {
		return;
	}

	var createNewPackage = function(arrayOfPackage) {

		var dialogTitle = aras.getResource('', 'item_methods.create_new_package');

		var createNewPacakgeDialog = new ArasModules.Dialog('html', {
			title: dialogTitle
		});

		var packageNameContainer = document.createElement('div');
		var packageNameLabel = document.createElement('label');
		packageNameLabel.textContent = aras.getResource('', 'item_methods.package_name');
		packageNameLabel.classList.add('aras-dialog-createNewPackage-createNewPackageLabel');
		var packageNameInput = document.createElement('input');
		packageNameInput.type = 'text';
		packageNameInput.placeholder = 'new name';
		packageNameInput.classList.add('aras-dialog-createNewPackage-createNewPackageMainContent');
		packageNameContainer.appendChild(packageNameLabel);
		packageNameContainer.appendChild(packageNameInput);

		var dependencyContainer = document.createElement('div');
		var dependencyLabel = document.createElement('label');
		dependencyLabel.textContent = aras.getResource('', 'item_methods.dependency');
		dependencyLabel.classList.add('aras-dialog-createNewPackage-createNewPackageLabel');
		var dependencyPackageSelect = document.createElement('select');
		dependencyPackageSelect.size = 11;
		dependencyPackageSelect.multiple = true;
		arrayOfPackage.forEach(function(item, arrayOfPackage) {
			dependencyPackageSelect.add(new Option(item));
		});
		dependencyPackageSelect.classList.add('aras-dialog-createNewPackage-createNewPackageMainContent');
		dependencyContainer.appendChild(dependencyLabel);
		dependencyContainer.appendChild(dependencyPackageSelect);
		dependencyContainer.classList.add('aras-dialog-createNewPackage-dependencyContainer');

		var buttonsContainer = document.createElement('div');
		var okButton = document.createElement('button');
		okButton.textContent = aras.getResource('', 'common.ok');
		okButton.classList.add('aras-btn');
		okButton.addEventListener('click', function() {
			var objectPackage = {packageName: packageNameInput.value};
			var dependency = [];
			for (var i = 0; i < dependencyPackageSelect.options.length; i++) {
				if (dependencyPackageSelect.options[i].selected) {
					dependency.push(dependencyPackageSelect.options[i].value);
				}
			}
			objectPackage.dependency = dependency;
			this.close(objectPackage);
		}.bind(createNewPacakgeDialog));
		var cancelButton = document.createElement('button');
		cancelButton.textContent = aras.getResource('', 'common.cancel');
		cancelButton.addEventListener('click', createNewPacakgeDialog.close.bind(createNewPacakgeDialog, null));
		cancelButton.classList.add('aras-btn', 'btn_cancel');
		buttonsContainer.appendChild(okButton);
		buttonsContainer.appendChild(cancelButton);

		createNewPacakgeDialog.contentNode.appendChild(packageNameContainer);
		createNewPacakgeDialog.contentNode.appendChild(dependencyContainer);
		createNewPacakgeDialog.contentNode.appendChild(buttonsContainer);
		createNewPacakgeDialog.contentNode.classList.add('aras-form');
		createNewPacakgeDialog.dialogNode.classList.add('aras-dialog-createNewPackage');

		createNewPacakgeDialog.show();

		return createNewPacakgeDialog.promise;
	};

	externalParent.Dialogs = externalParent.Dialogs || {};
	externalParent.Dialogs.createNewPackage = createNewPackage;
	window.ArasCore = window.ArasCore || externalParent;
})(window.ArasCore || {});
