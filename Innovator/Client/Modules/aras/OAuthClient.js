﻿(function(wnd) {
	const SyncPromise = ArasModules.SyncPromise;

	const SECOND = 1000;
	const MINUTE = 60 * 1000;

	const storage = {
		oauthClientId: null,
		tokenType: null,
		accessToken: null,
		refreshToken: null,
		expiresIn: null,
		authorizationHeaderName: null,
		unauthorizedStatusCode: null,
		tokenEndpoint: null,
		accessTokenExpiresAt: null,

		grantType: null,
		database: null,
		scriptsUrl: null,

		tokenRenewPromise: null
	};

	const discoveryErrorMessage = 'Cannot access OAuth Server due to incorrect Discovery configuration';
	const httpErrorMessage = 'Cannot access OAuth Server due to {0} ({1})';
	const corsErrorMessage = 'Cannot access OAuth Server due to CORS policies';

	const configurationRoute = '.well-known/openid-configuration';

	const lastAccessibleConfigurationEndpointKey = 'lastAccessibleConfigurationEndpoint';

	wnd.OAuthClient = {
		get unauthorizedStatusCode() {
			return storage.unauthorizedStatusCode;
		},

		get authorizationHeaderName() {
			return storage.authorizationHeaderName;
		},

		init: function(oauthClientId, baseServerUrl) {
			storage.oauthClientId = oauthClientId;
			const self = this;
			return this._getConfigurationEndpoints(baseServerUrl)
				.then(function(configurationEndpoints) {
					// Optimization by host
					const priorityHost = self._getHost(baseServerUrl);
					configurationEndpoints = self._prioritizeHost(configurationEndpoints, priorityHost);

					// Optimization by last accessible url
					const storage = self._getStorage();
					const priorityUrl = storage.getItem(lastAccessibleConfigurationEndpointKey);
					if (priorityUrl) {
						configurationEndpoints = self._prioritizeUrl(configurationEndpoints, priorityUrl);
					}

					const errors = [];
					let promise = configurationEndpoints.reduce(function(promise, configurationEndpoint) {
						return promise.catch(function() {
							return self._initAuthorizationMetadata(configurationEndpoint)
								.then(function(response) {
									storage.setItem(lastAccessibleConfigurationEndpointKey, configurationEndpoint);
									return response;
								})
								.catch(function(error) {
									errors.push({
										error: error,
										configurationEndpoint: configurationEndpoint
									});
									return Promise.reject(error);
								});
						});
					}, Promise.reject());
					promise = promise.catch(function(lastError) {
						return Promise.reject(
							configurationEndpoints.length == 1 ?
								lastError :
								self._combineErrorsFromInit(errors));
					});
					return promise;
				});
		},

		login: function(grantType, options) {
			storage.grantType = grantType;

			if (grantType === 'passwordGrant') {
				return this._requestPasswordGrant(options.loginName, options.hashPassword, options.database);
			} else if (grantType === 'logonHookPasswordGrant') {
				storage.scriptsUrl = options.scriptsUrl;
				storage.database = options.database;

				return this._requestLogonHookPasswordGrantAsync(options.scriptsUrl, options.database);
			}
		},

		getToken: function() {
			const self = this;

			const timeLeft = storage.accessTokenExpiresAt - Date.now();
			const accessTokenLifetimeRelaxBorder = 10 * MINUTE;
			const accessTokenLifetimeLastChanceBorder = 5 * MINUTE;

			if (timeLeft >= accessTokenLifetimeLastChanceBorder && timeLeft < accessTokenLifetimeRelaxBorder) {
				this._startRenewTokenCycle(true);
			} else if (timeLeft < accessTokenLifetimeLastChanceBorder) {
				this._startRenewTokenCycle(false)
					.catch(function(error) {
						if (storage.grantType === 'logonHookPasswordGrant') {
							return self._requestLogonHookPasswordGrantSync(storage.scriptsUrl, storage.database);
						}
						return SyncPromise.reject(error);
					});
			}
			return storage.accessToken;
		},

		getAuthorizationHeader: function() {
			const headerObject = {};
			const headerName = this.authorizationHeaderName;
			if (headerName) {
				headerObject[headerName] = storage.tokenType + ' ' + this.getToken();
			}

			return headerObject;
		},

		_setAuthorizationData: function(requestHaveBeenSentTimestamp, authorizationData) {
			storage.tokenType = authorizationData.token_type;
			storage.accessToken = authorizationData.access_token;
			storage.refreshToken = authorizationData.refresh_token;
			storage.expiresIn = authorizationData.expires_in;

			// server returns value in seconds
			storage.accessTokenExpiresAt = requestHaveBeenSentTimestamp + (authorizationData.expires_in * SECOND);
		},

		_getConfigurationEndpoints: function(baseServerUrl) {
			const discoveryServiceUrl = baseServerUrl + 'OAuthServerDiscovery.aspx';

			return fetch(discoveryServiceUrl, {method: 'GET', credentials: 'same-origin'})
				.then(function(discoveryServiceResponse) {
					return discoveryServiceResponse.json();
				})
				.then(function(discoveryServiceResponse) {
					if (!discoveryServiceResponse || !Array.isArray(discoveryServiceResponse.locations) || !discoveryServiceResponse.locations.length) {
						return Promise.reject(new Error(discoveryErrorMessage));
					}

					const configurationEndpoints = discoveryServiceResponse.locations.map(function(location) {
						const oauthServerUrl = location.uri;
						const normalizedOAuthServerUrl = oauthServerUrl.endsWith('/') ? oauthServerUrl : (oauthServerUrl + '/');
						const configurationEndpoint = normalizedOAuthServerUrl + configurationRoute;

						return configurationEndpoint;
					});
					return configurationEndpoints;
				});
		},

		_initAuthorizationMetadata: function(configurationEndpoint) {
			return fetch(configurationEndpoint, {method: 'GET', credentials: 'include'})
				.then(function(response) {
					if (!response.ok && response.status !== 200) {
						const errorMessage = httpErrorMessage
							.replace('{0}', response.status)
							.replace('{1}', response.statusText);
						return Promise.reject(new Error(errorMessage));
					}

					return response.json();
				})
				.then(function(configuration) {
					storage.tokenEndpoint = configuration.token_endpoint;
					storage.authorizationHeaderName = configuration.protocol_info.authorization_header;
					storage.unauthorizedStatusCode = configuration.protocol_info.unauthorized_status_code;
				})
				.catch(function(error) {
					const polyfillCorsErrorMessage = 'Network request failed';
					const firefoxCorsErrorMessage = 'NetworkError when attempting to fetch resource.';
					const defaultCorsErrorMessage = 'Failed to fetch'; // chrome, edge

					const isCorsError = (
						error && error.message &&
						error.message === polyfillCorsErrorMessage ||
						error.message === firefoxCorsErrorMessage ||
						error.message === defaultCorsErrorMessage
					);

					if (isCorsError) {
						return Promise.reject(new Error(corsErrorMessage));
					}

					return Promise.reject(error);
				});
		},

		_requestPasswordGrant: function(loginName, hashPassword, database) {
			const self = this;

			const data = new URLSearchParams();
			data.append('grant_type', 'password');
			data.append('scope', 'Innovator offline_access');
			data.append('username', loginName);
			data.append('password', hashPassword);
			data.append('database', database);
			data.append('client_id', storage.oauthClientId);

			const options = {
				method: 'POST',
				headers: {
					'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'
				},
				body: data.toString(),
				credentials: 'include'
			};

			const requestHaveBeenSentTimestamp = Date.now();

			return fetch(storage.tokenEndpoint, options)
				.then(function(response) {
					return response.json();
				})
				.then(function(data) {
					if (data.error) {
						return Promise.reject(data);
					}

					self._setAuthorizationData(requestHaveBeenSentTimestamp, data);
				});
		},

		_requestLogonHookPasswordGrantAsync: function(scriptsUrl, database) {
			const self = this;

			const searchParams = new URLSearchParams();
			searchParams.append('return', 'token');
			searchParams.append('database', database);

			const url = scriptsUrl + 'IOMLogin.aspx?' + searchParams.toString();
			const options = {
				method: 'GET',
				credentials: 'same-origin' // Necessary for Firefox and Edge
			};

			const requestHaveBeenSentTimestamp = Date.now();

			return fetch(url, options)
				.then(function(response) {
					return response.json();
				})
				.then(function(data) {
					if (data.error) {
						return Promise.reject(data);
					}

					self._setAuthorizationData(requestHaveBeenSentTimestamp, data);
				});
		},

		_requestLogonHookPasswordGrantSync: function(scriptsUrl, database) {
			const searchParams = new URLSearchParams();
			searchParams.append('return', 'token');
			searchParams.append('database', database);

			const xhr = new XMLHttpRequest();
			const url = scriptsUrl + 'IOMLogin.aspx?' + searchParams.toString();
			xhr.open('GET', url, false);

			const requestHaveBeenSentTimestamp = Date.now();
			xhr.send(null);

			const responseData = JSON.parse(xhr.responseText);
			if (xhr.status === 200) {
				if (!responseData || responseData.error) {
					return SyncPromise.reject(responseData);
				}

				this._setAuthorizationData(requestHaveBeenSentTimestamp, responseData);
				return SyncPromise.resolve();
			}
			return SyncPromise.reject(responseData);
		},

		_startRenewTokenCycle: function(isAsync) {
			const self = this;
			const isTokenRenewInProgress = !!(this._tokenRenewPromise);

			if (!isAsync) {
				return (isTokenRenewInProgress ? SyncPromise.resolve() : this._renewToken(isAsync));
			}

			if (!isTokenRenewInProgress) {
				this._tokenRenewPromise = this._renewToken(isAsync);
			}

			return this._tokenRenewPromise.then(function() {
				self._tokenRenewPromise = null;
			});
		},

		_renewToken: function(async) {
			const self = this;
			const promiseClass = (async ? Promise : SyncPromise);

			const refreshToken = storage.refreshToken;
			const oauthClientId = storage.oauthClientId;

			if (!refreshToken) {
				return promiseClass.reject();
			}

			const data = new URLSearchParams();
			data.append('grant_type', 'refresh_token');
			data.append('refresh_token', refreshToken);
			data.append('client_id', oauthClientId);
			const body = data.toString();

			const headers = {
				'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
			};

			const requestHaveBeenSentTimestamp = Date.now();

			return (async ? this._renewTokenAsync(headers, body) : this._renewTokenSync(headers, body))
				.then(function(response) {
					if (!response || response.error) {
						return promiseClass.reject(response);
					}

					self._setAuthorizationData(requestHaveBeenSentTimestamp, response);
				})
				.catch(function(error) {
					storage.refreshToken = null;
					return promiseClass.reject(error);
				});
		},

		_renewTokenSync: function(headers, body) {
			const xhr = new XMLHttpRequest();
			xhr.open('POST', storage.tokenEndpoint, false);

			const keys = Object.keys(headers);
			keys.forEach(function(key) {
				xhr.setRequestHeader(key, headers[key]);
			});
			xhr.withCredentials = true;
			xhr.send(body);

			const responseData = JSON.parse(xhr.responseText);
			if (xhr.status === 200) {
				return SyncPromise.resolve(responseData);
			}
			return SyncPromise.reject(responseData);
		},

		_renewTokenAsync: function(headers, body) {
			const params = {
				method: 'POST',
				headers: headers,
				body: body,
				credentials: 'include'
			};

			return fetch(storage.tokenEndpoint, params)
				.then(function(response) {
					return response.json();
				})
				.then(function(data) {
					if (data.error) {
						return Promise.reject(data);
					}
					return Promise.resolve(data);
				});
		},

		_combineErrorsFromInit: function(initErrors) {
			return initErrors.reduce(function(message, initError) {
				const errorMessage = initError.error;
				// TODO: Avoid extra replace by splitting _getConfigurationEndpoints on
				// _getOAuthServerUrls and _resolveConfigurationEndpoint
				const configurationEndpoint = initError.configurationEndpoint
					.replace(configurationRoute, '');
				if (message.length) {
					message = message + '\n\n';
				}
				return message +
					errorMessage + '\n' +
					'  from ' + configurationEndpoint;
			}, '');
		},

		_getHost: function(url) {
			const location = document.createElement('a');
			location.href = url;
			return location.hostname.toLowerCase();
		},

		_prioritizeHost: function(configurationEndpoints, priorityHost) {
			const self = this;
			return configurationEndpoints.sort(function(endpoint1, endpoint2) {
				const host1 = self._getHost(endpoint1);
				if (host1 === priorityHost) {
					return -1;
				}
				const host2 = self._getHost(endpoint2);
				if (host2 === priorityHost) {
					return 1;
				}
				return 0;
			});
		},

		_prioritizeUrl: function(configurationEndpoints, priorityUrl) {
			priorityUrl = priorityUrl.toLowerCase();
			return configurationEndpoints.sort(function(endpoint1, endpoint2) {
				endpoint1 = endpoint1.toLowerCase();
				if (endpoint1 === priorityUrl) {
					return -1;
				}
				endpoint2 = endpoint2.toLowerCase();
				if (endpoint2 === priorityUrl) {
					return 1;
				}
				return 0;
			});
		},

		_getStorage: function() {
			return localStorage;
		}
	};
})(window);
