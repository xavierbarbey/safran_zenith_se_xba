﻿///BaseLeaf
function BaseLeaf(rowId) {
	/// <summary>
	/// Base class for Items in Grid
	/// </summary>
	/// <param name="rowId">Id of item in TOC.</param>
	this.rowId = rowId;
}

BaseLeaf.prototype.onClick = function BaseLeafOnClick() {
	/// <summary>
	/// onClick function should contains a logic for onClick action
	/// </summary>
};

BaseLeaf.prototype.select = function BaseLeafSelect() {
	/// <summary>
	/// Select function should contains a logic for select action
	/// if user calls onClick function manually
	/// </summary>
};

BaseLeaf.prototype.onMouseOver = function BaseLeafOnMouseOver() {
};

BaseLeaf.prototype.isUserOwner = function BaseLeafIsUserOwner(type, id) {
	var userIdentity = aras.getIsAliasIdentityIDForLoggedUser();
	var item = getItem(type, id);
	if (item.isError()) {
		aras.AlertError(item.getErrorString());
		return;
	}
	var ownerId = item.getProperty('owned_by_id');
	if (ownerId === userIdentity) {
		return true;
	} else {
		return false;
	}

	function getItem(type, id) {
		var item = aras.newIOMItem(type, 'get');
		item.setID(id);
		return item.apply();
	}
};
///BaseLeaf

///TocCategory
function TocCategory(rowId) {
	this.rowId = rowId;
	this.itemTypeName = 'TocCategory';
}

TocCategory.prototype = new BaseLeaf();

TocCategory.prototype.onClick = function TocCategoryOnClick() {
	var args = {itemID: this.rowId, itemTypeName: this.itemTypeName, item: aras.newIOMItem('', 'get')};
	eventManager.fireEvent('onTreeItemSelect', args);
};

TocCategory.prototype.onContextMenuCreate = function TocCategoryOnContextMenuCreate() {
	return false;
};
///TocCategory

///Forum
var _forumOwners = {};

function Forum(rowId) {
	this.itemTypeName = 'Forum';
	this.rowId = rowId;
}

Forum.prototype = new BaseLeaf();

Forum.prototype.onMouseOver = function ForumOnMouseOver(ownerId) {
	return getUserByIdentityId(ownerId);

	function getUserByIdentityId(id) {
		if (!_forumOwners[id]) {
			var user = aras.newIOMItem('User', 'get');
			var alias = aras.newIOMItem('Alias', 'get');
			alias.setProperty('related_id', id);
			user.addRelationship(alias);
			_forumOwners[id] = user.apply();
		}
		return _forumOwners[id];
	}
};

Forum.prototype.onClick = function ForumOnClick(withFilters) {
	if (!this.item) {
		this.item = aras.newIOMItem(this.itemTypeName, 'get');
		this.item.setID(this.rowId);
	}
	var args = {itemID: this.rowId, itemTypeName: this.itemTypeName, item: this.item};
	if (withFilters) {
		eventManager.fireEvent('onTreeItemReload', args);
	} else {
		eventManager.fireEvent('onTreeItemSelect', args);
	}
};

Forum.prototype.onContextMenuCreate = function ForumOnContextMenuCreate() {
	if (this.rowId === 'allmessages') {
		return false;
	}
	var isOwner = this.isUserOwner(this.itemTypeName, this.rowId);
	var enableShare = aras.isAdminUser() || isOwner;
	var menu = treeControl.contextMenu;
	menu.setHide('setAsDefault', false);
	menu.setHide('rename', !isOwner);
	menu.setHide('remove', !canUnsubscribe(this.rowId));
	menu.setHide('share', !enableShare);
	menu.setHide('open', true);
	return true;

	function canUnsubscribe(id) {
		var forum = getForum(id);
		var mustView = forum.getProperty('must_view_by', '');
		if (mustView) {
			var identitiesIds = getUserIdentitiesIdsArray();
			var mustIds = mustView.split('|');
			for (var i = 0, count = mustIds.length; i < count; i++) {
				if (identitiesIds.indexOf(mustIds[i]) !== -1) {
					return false;
				}
			}
		}
		return true;
	}

	function getUserIdentitiesIdsArray() {
		var userIdentities = aras.newIOMItem('User', 'VC_GetAllUserIdentities');
		userIdentities.setID(aras.getUserID());
		userIdentities = userIdentities.apply();
		if (userIdentities.isError()) {
			return [];
		} else {
			var result = userIdentities.getResult();
			return result.split('|');
		}
	}

	function getForum(id) {
		var forum = aras.newIOMItem('Forum', 'get');
		forum.setAttribute('select', 'must_view_by');
		forum.setID(id);
		return forum.apply();
	}
};

Forum.prototype.setAsDefault = function ForumSetAsDefault() {
	var menuActions = new TreeMenuActions(aras);
	menuActions.setAsDefault(this.itemTypeName, this.rowId);
};

Forum.prototype.remove = function ForumRemove() {
	var menuActions = new TreeMenuActions(aras);
	menuActions.unsubscribe(aras.getUserID(), this.rowId);
	initializeTree();
	selectStartPage();
};

Forum.prototype.rename = function ForumRename() {
	forumsHelper.renameForum(this.rowId);
};

Forum.prototype.share = function ForumShare() {
	var menuActions = new TreeMenuActions(aras);
	var forumId = this.rowId;
	menuActions.share(this.rowId, '../Modules/aras.innovator.SSVC/Views/SelectShare.html');
};

Forum.prototype.open = function ForumOpen() {
	return false;
};
///Forum

///ForumMessageGroup
function ForumMessageGroup(rowId) {
	this.itemTypeName = 'ForumMessageGroup';
	this.rowId = rowId;
}

ForumMessageGroup.prototype = new Forum();

ForumMessageGroup.prototype.canRemove = function ForumMessageGroupCanRemove(userId, fmgId) {
	if (fmgId === 'allmessages') {
		return false;
	}
	var fmg = getItem(this.itemTypeName, fmgId);
	if (fmg.isError()) {
		aras.AlertError(fmg.getErrorString());
		return false;
	}
	var userCriteriaId = fmg.getProperty('user_criteria_id');
	if (userCriteriaId && userCriteriaId === userId) {
		return false;
	}
	return true;

	function getItem(type, fmgId) {
		var item = aras.newIOMItem(type, 'get');
		item.setID(fmgId);
		return item.apply();
	}
};

ForumMessageGroup.prototype.onContextMenuCreate = function ForumMessageGroupOnContextMenuCreate() {
	var menu = treeControl.contextMenu;
	var canRemove = this.canRemove(aras.getUserID(), this.rowId);
	menu.setHide('setAsDefault', false);
	menu.setHide('rename', true);
	menu.setHide('remove', !canRemove);
	menu.setHide('share', true);
	menu.setHide('open', true);
	return true;
};

ForumMessageGroup.prototype.share = function ForumMessageGroupShare() {
	return false;
};
ForumMessageGroup.prototype.onMouseOver = function ForumMessageGroupOnMouseOver() {
};

ForumMessageGroup.prototype.remove = function ForumMessageGroupRemove() {
	var forumMessageGroup = aras.newIOMItem('ForumMessageGroup', 'delete');
	forumMessageGroup.setID(this.rowId);
	forumMessageGroup = forumMessageGroup.apply();
	if (forumMessageGroup.isError()) {
		aras.AlertError(forumMessageGroup);
		return;
	}

	initializeTree();
	selectStartPage();
};

///ForumMessageGroup

///ForumItem
function ForumItem(rowId) {
	this.itemTypeName = 'ForumItem';
	this.rowId = rowId;
}

ForumItem.prototype = new ForumMessageGroup();

ForumItem.prototype.onContextMenuCreate = function ForumItemOnContextMenuCreate() {
	var forumItem = this.getForumItem(this.itemTypeName, this.rowId, 'source_id(forum_type)');
	if (forumItem.isError()) {
		return;
	}
	var myBookmarkForum = forumItem.getItemsByXPath('//Item/source_id/Item[@type=\'Forum\'][forum_type=\'MyBookmarks\']');
	var menu = treeControl.contextMenu;
	menu.setHide('setAsDefault', !myBookmarkForum.node);
	menu.setHide('remove', false);
	menu.setHide('rename', true);
	menu.setHide('share', true);
	menu.setHide('open', false);
	return true;
};

ForumItem.prototype.remove = function ForumItemRemove() {
	var menuActions = new TreeMenuActions(aras);
	menuActions.remove(this.itemTypeName, this.rowId);
	initializeTree();
	selectStartPage();
};

ForumItem.prototype.open = function ForumItemOpen() {
	var forumItem = this.getForumItem(this.itemTypeName, this.rowId);
	if (forumItem.isError()) {
		return;
	}
	var item = this.getItemByConfigId(forumItem.getProperty('item_type'), forumItem.getProperty('item_config_id'));
	if (item.isError()) {
		return;
	}
	var menuActions = new TreeMenuActions(aras);
	menuActions.open(item.getType(), item.getID());
};

ForumItem.prototype.getForumItem = function ForumItemGetForumItem(type, id, select) {
	var item = aras.newIOMItem(type, 'get');
	item.setID(id);
	if (select && select !== '') {
		item.setAttribute('select', select);
	}
	return item.apply();
};

ForumItem.prototype.getItemByConfigId = function ForumItemGetItemByConfigId(type, configId) {
	var item = aras.newIOMItem(type, 'get');
	item.setProperty('config_id', configId);
	item.setProperty('is_current', '1');
	item = item.apply();
	if (item.isError() || item.getAttribute('discover_only') == '1') {
		item = this.getLatestNotCurrent(type, configId);
	}
	return item;
};

ForumItem.prototype.getLatestNotCurrent = function ForumItemGetLatestNotCurrent(type, configId) {
	var items = aras.newIOMItem(type, 'get');
	items.setAttribute('orderBy', 'generation');
	items.setProperty('config_id', configId);
	items.setProperty('generation', '0');
	items.setPropertyCondition('generation', 'ge');
	items = items.apply();
	if (items.isError()) {
		return items;
	} else {
		var item = null;
		for (var i = 0; i < items.getItemCount(); i++) {
			item = items.getItemByIndex(i);
			if (item.getAttribute('discover_only') !== '1') {
				break;
			}
		}
		return item;
	}
};

ForumItem.prototype.onClick = function ForumItemOnClick() {
	var forumItem = this.getForumItem(this.itemTypeName, this.rowId);
	if (forumItem.isError()) {
		aras.AlertError(forumItem.getErrorString());
		return;
	}
	var itemType = forumItem.getProperty('item_type');
	var itemConfigId = forumItem.getProperty('item_config_id');
	var item = this.getItemByConfigId(itemType, itemConfigId);
	if (item.isError()) {
		aras.AlertError(item.getErrorString());
		return;
	}
	var args = {itemID: item.getID(), itemTypeName: item.getType(), item: item};
	eventManager.fireEvent('onTreeItemSelect', args);
};
///ForumItem

///ForumSearch
function ForumSearch(rowId) {
	this.itemTypeName = 'ForumSearch';
	this.rowId = rowId;
}

ForumSearch.prototype = new ForumItem();

ForumSearch.prototype.onContextMenuCreate = function ForumSearchOnContextMenuCreate() {
	var menu = treeControl.contextMenu;
	menu.setHide('setAsDefault', true);
	menu.setHide('remove', false);
	menu.setHide('share', true);
	menu.setHide('open', true);
	return true;
};

ForumSearch.prototype.setAsDefault = function ForumSearchSetAsDefault() {
	return false;
};

ForumSearch.prototype.open = function ForumSearchOpen() {
	return false;
};

ForumSearch.prototype.onClick = function ForumSearchOnClick() {
	var item = aras.newIOMItem(this.itemTypeName, 'get');
	item.setID(this.rowId);
	var args = {itemID: this.rowId, itemTypeName: this.itemTypeName, item: item};
	eventManager.fireEvent('onTreeItemSelect', args);
};
///ForumSearch
