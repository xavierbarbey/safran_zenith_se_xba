﻿var aras = parent.parent.aras;
var UI = {};

dojo.registerModulePath('Views', aras.getInnovatorUrl() + 'Client/Modules/aras.innovator.SSVC/Views');
dojo.registerModulePath('Scripts', aras.getInnovatorUrl() + 'Client/Modules/aras.innovator.SSVC/Scripts');

var ssvcResourceUrl = '../Modules/aras.innovator.ssvc/';

dojo.setObject('SSVC.Utils', {
	aras: aras,
	feedResources: {
		'.ssvc-toolbar > div > label': aras.getResource(ssvcResourceUrl, 'feed_title_base'),
		'.ssvc-toolbar-addComment > input.btn': aras.getResource(ssvcResourceUrl, 'feed_add_comment'),
		'.ssvc-toolbar .ssvc-toolbar-search input': aras.getResource(ssvcResourceUrl, 'feed_tb_def_message_search'),
		'.ssvc-toolbar .ssvc-display-settings-popup span.ssvc-toolbar-sortingLabel': aras.getResource(ssvcResourceUrl, 'feed_tb_sort_mode_label'),
		'.ssvc-toolbar .ssvc-display-settings-popup span.ssvc-toolbar-modeLabel': aras.getResource(ssvcResourceUrl, 'feed_tb_display_mode_label'),
		'.ssvc-toolbar .ssvc-preferences-popup .ssvc-preferences-popup-title': aras.getResource(ssvcResourceUrl, 'pp_title'),
		'.ssvc-toolbar .ssvc-preferences-popup #enable_in_app_notifications + label': aras.getResource(ssvcResourceUrl, 'pp_enable_in_app_notifications'),
		'.ssvc-toolbar .ssvc-preferences-popup #enable_immediate_notifications + label':
				aras.getResource(ssvcResourceUrl, 'pp_enable_immediate_notifications'),
		'.ssvc-toolbar .ssvc-preferences-popup #enable_email_digest_notification + label': aras.getResource(ssvcResourceUrl, 'pp_enable_email_digest_notification'),
		'.ssvc-filter a': aras.getResource(ssvcResourceUrl, 'feed_show_all_messages')
	},
	setResources: function(object, dom) {
		Object.keys(object).forEach(function(key) {
			var target = dom.querySelector(key);
			if ((target.nodeName === 'INPUT' && target.type !== 'button') || target.nodeName === 'TEXTAREA') {
				target.setAttribute('placeholder', object[key]);
			} else if (target.type === 'button') {
				target.value = object[key];
			} else {
				target.textContent = object[key];
			}
		});
	},
	fillListByName: function(name, select) {
		var aras = this.aras;
		var listValues = aras.getListValues(aras.getListId(name), false);
		Array.prototype.forEach.call(listValues, function(type) {
			this.add(new Option(aras.getItemProperty(type, 'label'), aras.getItemProperty(type, 'value')));
		}, select);
	},
	GetResource: function(key) {
		return aras.getResource('../Modules/aras.innovator.ssvc/', key);
	},

	GetDateTimeInNeutralFormat: function(dateTimePar) {
		function pad(x) {
			return (x < 10) ? '0' + x : '' + x;
		}
		var dateTime = dateTimePar !== undefined ? dateTimePar : new Date();

		var months = dateTime.getMonth() + 1;
		var days = dateTime.getDate();
		var years = dateTime.getFullYear();
		var hours = dateTime.getHours();
		var minutes = dateTime.getMinutes();
		var seconds = dateTime.getSeconds();

		var dateTimeStr = '{0}/{1}/{2} {3}:{4}:{5}'.Format(pad(days), pad(months), pad(years), pad(hours), pad(minutes), pad(seconds));
		return aras.convertToNeutral(dateTimeStr, 'date', 'dd/MM/yyyy HH:mm:ss');
	},

	preloadScripts: function(strArr, shortpath) {
		var newarr = [];
		var checkProp = function(obj, prop) {
			if (obj[prop] !== undefined) {
				return obj[prop];
			}
			return false;
		};

		if (typeof strArr === 'object') {
			for (var i = 0; i < strArr.length; i++) {
				var current = strArr[i];
				var isExist = false;

				current = current.split('/');
				var checkObj = UI;
				for (var j = 0; j < current.length; j++) {
					checkObj = checkProp(checkObj, current[j]);
					if (!checkObj) {
						isExist = false;
						break;
					}
					isExist = true;
				}

				if (!isExist) {
					newarr.push(shortpath + '/' + strArr[i]);
				}
			}
		} else {
			newarr = strArr;
		}
		return newarr;
	},

	LoadModules: function(strArr) {
		require(this.preloadScripts(strArr, 'Scripts'));
	}
});

//Extensions

if (!String.prototype.Format) {
	String.prototype.Format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] !== 'undefined' ? args[number] : '';
		});
	};
}

if (!Array.prototype.Find) {
	Object.defineProperty(Array.prototype, 'Find', {
		value: function(param, value) {
			for (var i = 0; i < this.length; i++) {
				var entry = this[i];
				if (entry[param] === value) {
					return entry;
				}
			}
		}
	});
}

if (!Element.prototype.addClass) {
	Element.prototype.addClass = function(className) {
		if (this.classList === undefined) {
			this.className += ' ' + className;
		} else {
			this.classList.add(className);
		}
	};
}
