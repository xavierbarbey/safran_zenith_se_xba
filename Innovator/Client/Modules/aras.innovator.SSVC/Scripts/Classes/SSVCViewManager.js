﻿define(['SSVC/Scripts/Controls/DiscussionPanel'], function(DiscussionPanel) {
	//args = { aras: ... , dojoRequire: ... , dojo: ... }
	function SSVCViewManager(args) {
		var dojoObj = args.dojo;
		var arasObj = args.aras;
		var require = args.dojoRequire;
		var self = this;

		//args = { id: ... , onClick: ... , iconClass: ...}
		function createButton(args) {
			return new dijit.form.Button({
				id: args.id,
				iconClass: args.iconClass,
				baseClass: 'sidebarButton',
				onClick: args.onClick
			});
		}

		//args = { connectId: ... , label: ... }
		function setTooltip(args) {
			var label = args.label;
			var connectId = args.connectId;

			return new dijit.Tooltip({
				connectId: [connectId],
				label: label
			});
		}

		// args: { file }
		function getLabelForFile(args) {
			var file = args.file;
			var fTooltip = file.selectSingleNode('tooltip');
			var label;
			if (fTooltip) {
				label = fTooltip.text;
			} else {
				label = file.selectSingleNode('keyed_name').text +
					' from ' + file.getAttribute('markup_holder_itemtype_name') +
					' ' + file.getAttribute('markup_holder_keyed_name');
			}
			return label;
		}

		// args: { file }
		function createViewerParams(args) {
			var file = args.file;
			var params = {
				// File specific
				fileId: file.getAttribute('id'),
				fileName: file.selectSingleNode('filename').text,
				label: getLabelForFile(args),
				// Item specific
				fileSelectorTypeId: file.getAttribute('file_selector_type_id'),
				fileSelectorId: file.getAttribute('file_selector_id'),
				markupHolderId: file.getAttribute('markup_holder_id'),
				markupHolderItemtypeName: file.getAttribute('markup_holder_itemtype_name'),
				markupHolderConfigId: file.getAttribute('markup_holder_config_id'),
				markupHolderKeyedName: file.getAttribute('markup_holder_keyed_name')
			};
			return params;
		}

		// params = {
		//   fileId, fileSelectorTypeId, fileSelectorId,
		//   fileName,
		//   markupHolderId, markupHolderItemtypeName, markupHolderConfigId, markupHolderKeyedName
		// }
		this.createViewer = function(module, viewerName, params) {
			params._fileUrl = null;
			params.itemWindow = window;
			var argsForWidget = {
				baseClass: 'dijitContentPaneNoPadding',
				style: 'height:100%;width:100%;',
				params: params
			};
			Object.defineProperty(params, 'fileUrl', {
				get: function() {
					if (this._fileUrl && isUrlValid(this._fileUrl)) {
						return this._fileUrl;
					} else {
						this._fileUrl = getNewFileUrl(this.fileId);
						return this._fileUrl;
					}
					function isUrlValid(fileUrl) {
						var xmlhttp = arasObj.XmlHttpRequestManager.CreateRequest();
						xmlhttp.open('GET', fileUrl, false);
						xmlhttp.send(null);
						if (xmlhttp.status !== 200) {
							return false;
						} else {
							return true;
						}
					}
					function getNewFileUrl(fileId) {
						return arasObj.IomInnovator.getFileUrl(fileId, arasObj.Enums.UrlType.SecurityToken);
					}
				},
				set: function(newValue) { this._fileUrl = newValue; },
				enumerable: true,
				configurable: true
			});
			var requireViewerPromise = new Promise(function(resolve, reject) {
				require([module + '/' + viewerName], function(Viewer) {
					var result = new Viewer(argsForWidget);
					resolve(result);
				});
			});
			return requireViewerPromise;
		};

		//args = { file: ... }
		this.getViewerInfo = function(args) {
			var file = args.file;
			return this.getViewerInfoById(file.getAttribute('id'));
		};

		this.getViewerInfoById = function(id) {
			var aml = '<AML>' +
							'	<Item  id=\'' + id + '\' type=\'File\' action=\'get\'>' +
							'		<file_type>' +
							'			<Item type=\'FileType\' action=\'get\'>' +
							'				<Relationships>' +
							'					<Item type=\'View With\' action=\'get\'>' +
							'						<application>SSVC</application>' +
							'						<related_id>' +
							'							<Item type=\'Viewer\' action=\'get\'/>' +
							'						</related_id>' +
							'					</Item>' +
							'				</Relationships>' +
							'			</Item>' +
							'		</file_type>' +
							'	</Item>' +
							'</AML>';
			var data = arasObj.applyAML(aml);
			var dom = arasObj.createXMLDocument();
			dom.loadXML(data);
			return this.getViewerInfoFromFile(dom, '//Item[@type=\'Viewer\']');
		};

		this.getViewerInfoFromItem = function(item, xPath) {
			var viewers = item.selectNodes(xPath);

			if (viewers.length !== 0) {
				var viewerUrl = viewers[0].selectSingleNode('viewer_url').text;
				var module = viewerUrl.match(/[^&?]*?module=[^&?]*/)[0].split('=')[1];
				var name = viewerUrl.match(/[^&?]*?name=[^&?]*/)[0].split('=')[1];

				return {module: module, name: name};
			} else {
				return null;
			}
		};

		this.getViewerInfoFromFile = function(file, xPath) {
			if (!xPath) {
				xPath = './file_type/Item/Relationships/Item/related_id/Item[@type=\'Viewer\']';
			}
			return this.getViewerInfoFromItem(file, xPath);
		};

		//args = { viewer: {}, sidebarButtonClass: "", tooltip: "", lostMessage:""}
		//returns id of temporary viewer
		this.showTemporaryViewer = function(args) {
			if (!args) {
				return;
			}
			var viewer = args.viewer;
			var newId = arasObj.generateNewGUID();
			var newBtnId = 'button_' + newId;
			var newTabId = newId;
			var sidebar = getSidebar();
			var lostMessage = args.lostMessage;

			var tabsControl;
			if (window.getViewersTabs) {
				tabsControl = window.getViewersTabs();
			} else {
				var mainWin = arasObj.getCurrentWindow();
				tabsControl = mainWin.getViewersTabs();
			}

			var sidebarTempButton = createButton({
				id: newBtnId,
				iconClass: args.sidebarButtonClass,
				onClick: function() { }
			});
			sidebar.addChild(sidebarTempButton);
			sidebar.resize();
			setTooltip({
				connectId: newBtnId,
				label: args.tooltip
			});

			tabsControl.createTab(viewer, newTabId);
			var tab = tabsControl.getTabById(newTabId);
			tabsControl.selectTab(newTabId);
			isTempViewerShown = true;
			sidebar.setWidgetSelected(tab);
			require(['dojo/aspect'], function(aspect) {
				aspect.after(tabsControl, 'onPreSelectTab', function() {
					if (!tabsControl.getTabById(newId)) {
						return false;
					}
					if (arasObj.confirm(lostMessage, window) !== true) {
						return true;
					}
					tabsControl.closeTab(newId);
					sidebar.removeChild(sidebarTempButton);
					return false;
				});
			});

			return newId;
		};

		//args = { isVisible: ... , sidebar: ... , discussionContainer: ..., isSSVCEnabled: bool }
		function setDiscussionPanel(args) {
			var isVisible = args.isVisible;
			var discussionContainer = args.discussionContainer;
			var discussionPanel = getDiscussionPanel();
			window.tearOffMenuController.when('ToolbarInitialized').then(function(toolbar) {
				var discussionButton = toolbar.getActiveToolbar().getItem('ssvc_discussion_button')['_item_Experimental'];

				var discussionPanelArgs = {
					aras: args.aras,
					itemID: itemID,
					itemTypeName: itemTypeName,
					item: getIOMItem()
				};

				if (discussionPanel) {
					discussionPanel.reload(discussionPanelArgs);
				} else {
					discussionPanel = new DiscussionPanel({
						id: 'discussion',
						isVisible: isVisible,
						style: 'width:100%; height:100%;',
						params: discussionPanelArgs
					});
				}
				discussionContainer.appendChild(discussionPanel.domNode);

				discussionPanel.onResize = function() {
					if (discussionButton.domNode && discussionPanel.visible() !== discussionButton.domNode.classList.contains('ssv-button-activeDisscussion')) {
						discussionButton.domNode.classList.toggle('ssv-button-activeDisscussion');

						var isOpening = discussionButton.domNode.classList.contains('ssv-button-activeDisscussion');
						var prefixResource = isOpening ? 'hide' : 'show';
						var btnTitle = aras.getResource('', 'ssvc.' + prefixResource + '_dpanel');
						discussionButton.setLabel(btnTitle);
						discussionButton.titleNode.setAttribute('title', btnTitle);
					}
				};

				if (isVisible) {
					var ssvcSplitter = document.getElementById('ssvc-splitter');
					ssvcSplitter.style.display = 'block';
					discussionContainer.style.display = 'block';
					discussionPanel.show();
					var centerContainer = dijit.byId('CenterBorderContainer');
					if (centerContainer) {
						centerContainer.resize();
					}
				}
			});
		}

		//args = { tabContainer: ... , viewObjects: ... }
		function setTabs(args) {
			var tabContainer = args.tabContainer;
			var viewObjects = args.viewObjects;
			var tabsPromises = [];
			viewObjects.forEach(function(viewObject) {
				var viewerParams = viewObject.params;
				var viewerPromise = self.createViewer(viewObject.viewer.module, viewObject.viewer.name, viewerParams);
				viewerPromise = viewerPromise.then(function(viewer) {
					tabContainer.createTab(viewer, viewerParams.fileId);
				});
				tabsPromises.push(viewerPromise);
			});

			var dispatchTabsLoadedEvent = function() {
				var event = document.createEvent('Event');
				event.initEvent('ssvcSideBarTabsLoaded', true, false);
				document.dispatchEvent(event);
			};
			Promise.all(tabsPromises).then(dispatchTabsLoadedEvent);
		}

		this.getViewObjectsFromFiles = function(args) {
			var filesForViewing = args.filesForViewing;
			var viewObjects = [];
			for (var i = 0, l = filesForViewing.length; i < l; i++) {
				var viewObject = getViewObject({file: filesForViewing[i]});
				if (viewObject) {
					viewObjects.push(viewObject);
				}
			}
			return viewObjects;
		};

		function getViewObject(args) {
			var file = args.file;
			var viewObject;

			if (shouldHaveRepresentation(file)) {
				// User expect to see only representation
				// TODO: Support representations with multiple files for HoopsViewer
				viewObject = getViewObjectFromRepresentation(file);
			} else {
				viewObject = getViewObjectFromFile(file);
			}

			return viewObject;
		}

		function shouldHaveRepresentation(file) {
			var hasFrep = file.getAttribute('file_selector_has_frep');
			if (hasFrep === '0') {
				// File selector contains frep function and representations do NOT exist in response
				// TODO: Missing representation should be handled in some special way
				return true;
			}
			if (hasFrep === '1') {
				// File selector contains frep function and representations exist in response
				return true;
			}
			// File selector does NOT contain frep function
			return false;
		}

		function getViewObjectFromFile(file) {
			var viewer = self.getViewerInfoFromFile(file);
			if (viewer) {
				var params = createViewerParams({file: file});
				var viewObject = {
					viewer: viewer,
					params: params
				};
				return viewObject;
			}
		}

		var representationXPath = './Relationships/Item[@type="fr_Representation"]';
		var representationFileXPath = './Relationships/Item[@type="fr_RepresentationFile"]/related_id/Item[@type="File"]';

		// Returns viewObject from the first representation file
		// which has viewer information
		function getViewObjectFromRepresentation(masterFile) {
			var representations = masterFile.selectNodes(representationXPath);
			for (var i = 0; i < representations.length; ++i) {
				var representation = representations[i];
				var representationFiles = representation.selectNodes(representationFileXPath);
				for (var j = 0; j < representationFiles.length; ++j) {
					var representationFile = representationFiles[j];
					// TODO: Check representation of representation when representation chaining will be supported
					// see IR-051250: "FR: Allow to add representations to representations"
					var viewer = self.getViewerInfoFromFile(representationFile);
					if (viewer) {
						// Pass file specific properties from representation file
						// but item specific properties and file label from master file
						var paramsMaster = createViewerParams({file: masterFile});
						var paramsRep = createViewerParams({file: representationFile});
						paramsMaster.fileId = paramsRep.fileId;
						paramsMaster.fileName = paramsRep.fileName;
						var viewObject = {
							viewer: viewer,
							params: paramsMaster
						};
						return viewObject;
					}
				}
			}
		}

		//args = { sidebar: ... }
		function setKeyedNameElement(args) {
			var sidebar = args.sidebar;

			var node = document.getElementById('keyedName');
			if (node) {
				try {
					sidebar.domNode.removeChild(node);
				} catch (ex) { }
			}

			var keyedNameNode = document.createElement('div');
			keyedNameNode.setAttribute('id', 'keyedName');
			keyedNameNode.setAttribute('class', 'sidebarKeyedName');
			if (arasObj.getLanguageDirection() === 'rtl') {
				keyedNameNode.classList.add('text_rtl_container');
			}

			sidebar.domNode.appendChild(keyedNameNode);
		}

		//args =
		//	{
		//		dataManager: ...,
		//		discussionContainer: ... ,
		//		isSSVCEnabled: ...,
		//		aras: ...
		//	}
		// TODO: IR-052075 "SSVC: viewManager.fillContainers double called on opening SSVC"
		this.fillContainers = function(args) {
			var discussionContainer = args.discussionContainer;
			var filesForViewing = args.dataManager.getFilesForViewing();
			var viewSettings = args.dataManager.getSsvcFormViewSettings();

			setDiscussionPanel({
				isVisible: viewSettings.discussionPanel && args.isSSVCEnabled,
				discussionContainer: discussionContainer,
				isSSVCEnabled: args.isSSVCEnabled,
				aras: args.aras
			});

			var viewers = dijit.byId('viewers');
			var viewObjects = self.getViewObjectsFromFiles({
				filesForViewing: filesForViewing
			});

			setTabs({
				tabContainer: viewers,
				viewObjects: viewObjects
			});

			var sidebar = getSidebar();
			setKeyedNameElement({sidebar: sidebar});
		};

		//args =
		//	{
		//		sidebarContainer: ... ,
		//		tabContainer: ...
		//	}
		this.clearContainers = function(args) {
			const sidebarContainer = args.sidebarContainer;
			const tabContainer = args.tabContainer;
			const sidebar = getSidebar();

			const childs = [].concat(sidebarContainer.getChildren(), tabContainer.getChildren());
			for (i = 0, l = childs.length; i < l; i++) {
				// don't close fixed tabs
				if (childs[i].id !== 'formTab' && !(childs[i].declaredClass === 'Tab' && childs[i].getChildren()[0].get('fixedSidebarButtonId'))) {
					if (childs[i].declaredClass === 'Tab') {
						tabContainer.closeTab(childs[i].id);

						const buttonWidget = sidebar.getButtonWidgetById(childs[i].id);
						sidebar.removeChild(buttonWidget);
					}
					childs[i].destroyRecursive();
				}
			}
			sidebarContainer.set('loaded', false);
		};
	}

	dojo.setObject('SSVC.SSVCViewManager', (function() {
		return new SSVCViewManager({});
	}));

	return SSVCViewManager;
});
