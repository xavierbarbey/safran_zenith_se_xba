﻿define([],
	function() {
		/**
		 * @typedef MessageControlViewSettings
		 * @type Object
		 * @property {string} itemTypeIcon
		 * @property {string} secondaryIcon
		 * @property {string} style
		 */

		/**
		 * @typedef SsvcFormViewSettings
		 * @type Object
		 * @property {boolean} discussionPanel
		 * @property {string} formTooltip
		 */

		var ViewSettingsManager = (function() {
			/**
			 * @param {{aras: Object}} args
			 * @constructor
			 */
			function ViewSettingsManager(args) {
				this.aras = args.aras;
			}
			/**
			 * @param {{itemTypeName: string}} args
			 * @returns {MessageControlViewSettings}
			 */
			ViewSettingsManager.prototype.getMessageControlViewSettings = function(args) {
				var itemTypeItem = this.aras.getItemTypeForClient(args.itemTypeName);
				var itemTypeIconUrl = this.getItemTypeIconUrl(itemTypeItem);
				var secondaryIconUrl = this.getSecondaryIconUrl(itemTypeItem);
				var style = this.getStyle(itemTypeItem);
				var settings = {
					itemTypeIcon: itemTypeIconUrl,
					secondaryIcon: secondaryIconUrl,
					style: style
				};
				return settings;
			};
			/**
			 * @param {{itemTypeName: string}} args
			 * @returns {MessageControlViewSettings}
			 */
			ViewSettingsManager.prototype.getSsvcFormViewSettings = function(args) {
				var _this = this;
				return Promise.resolve()
					.then(function() {
						if (!(args && args.itemTypeName && args.itemId && args.configId)) {
							var emptySettings = {
								discussionPanel: false,
								formTooltip: ''
							};
							return emptySettings;
						}
						return _this.fetchSsvcFormViewSettings(args.itemTypeName, args.itemId, args.configId);
					})
					.then(function(settings) {
						var globalState = sessionStorage.getItem('defaultDState');
						if (globalState && globalState !== 'defaultDState') {
							switch (globalState) {
								case 'onDState':
									settings.discussionPanel = true;
									break;
								case 'offDState':
									settings.discussionPanel = false;
									break;
							}
						}
						return settings;
					});
			};
			ViewSettingsManager.prototype.getItemTypeIconUrl = function(itemTypeItem) {
				var itemTypeIconRelativeUrl = itemTypeItem.getProperty('open_icon', '../Images/DefaultItemType.svg');
				var itemTypeIconUrl = this.aras.getScriptsURL(itemTypeIconRelativeUrl);
				return itemTypeIconUrl;
			};
			ViewSettingsManager.prototype.getSecondaryIconUrl = function(itemTypeItem) {
				var secondarIconRelativeUrl = null;
				var presentationConfiguration = this.getItemTypePresentationConfigurationItem(itemTypeItem);
				if (presentationConfiguration) {
					secondarIconRelativeUrl = this.aras.getItemProperty(presentationConfiguration, 'secondary_icon');
				}
				if (!secondarIconRelativeUrl) {
					secondarIconRelativeUrl = itemTypeItem.getProperty('open_icon', '../Images/DefaultItemTypeOff.svg');
				}
				var secondaryIconUrl = this.aras.getScriptsURL(secondarIconRelativeUrl);
				return secondaryIconUrl;
			};
			ViewSettingsManager.prototype.getStyle = function(itemTypeItem) {
				var style = null;
				var presentationConfiguration = this.getItemTypePresentationConfigurationItem(itemTypeItem);
				if (presentationConfiguration) {
					style = this.aras.getItemProperty(presentationConfiguration, 'style');
				}
				return style;
			};
			ViewSettingsManager.prototype.getItemTypePresentationConfigurationItem = function(itemTypeItem) {
				var presentationConfiguration = null;
				var configs = itemTypeItem.getItemsByXPath('Relationships/Item[@type=\'ITPresentationConfiguration\']');
				for (var i = 0; i < configs.getItemCount(); i++) {
					var config = configs.getItemByIndex(i);
					if (this.aras.getItemProperty(config.node, 'client') === 'js') {
						presentationConfiguration = config.getItemsByXPath('related_id/Item').node;
						break;
					}
				}
				return presentationConfiguration;
			};
			ViewSettingsManager.prototype.fetchSsvcFormViewSettings = function(itemTypeName, itemId, configId) {
				var _this = this;
				return Promise.resolve()
					.then(function() {
						var itemType = _this.aras.getItemTypeForClient(itemTypeName);
						var itemTypeId = itemType.getID();
						var getSsvcPresentConfigurationQuery = ArasModules.jsonToXml({
							Item: {
								'@attrs': {
									type: 'Method',
									action: 'VC_GetSSVCPresentConfiguration',
									id: itemId
								},
								'client': 'js',
								'item_type_id': itemTypeId,
								'item_type_name': itemTypeName,
								'item_config_id': configId
							}
						});
						return ArasModules.soap(getSsvcPresentConfigurationQuery, {async: true});
					})
					.then(function(xmlItem) {
						return ArasModules.xmlToJson(xmlItem);
					})
					.then(function(jsonItem) {
						return ({
							discussionPanelBehavior: jsonItem.Item['discussion_panel_behavior'],
							formTooltip: jsonItem.Item['form_tooltip_template'] ? jsonItem.Item['form_tooltip_template']['@value'] : ''
						});
					})
					.then(function(presentConfiguration) {
						var discussionPanel = presentConfiguration.discussionPanelBehavior === 'discPanelOn' || false;
						var formTooltip = presentConfiguration.formTooltip || '';
						var settings = {
							discussionPanel: discussionPanel,
							formTooltip: formTooltip
						};
						return settings;
					});
			};
			return ViewSettingsManager;
		}());
		return ViewSettingsManager;
	});
