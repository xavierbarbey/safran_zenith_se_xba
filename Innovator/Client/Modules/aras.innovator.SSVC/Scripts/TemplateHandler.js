﻿dojo.setObject('SSVC.ArasHelper.TemplateHandler',
	(function(params, $) {
		function getVisibleToIdentityContext(identityId) {
			var imagesFolderUrl = aras.getBaseURL() + '/../images/';
			var isWorld = false;
			var iconName;
			if (identityId === 'A73B655731924CD0B027E4F4D5FCC0A9') { //worldId
				iconName = 'World.svg';
				isWorld = true;
			} else {
				var identityRequest = aras.newIOMItem('identity', 'get');
				identityRequest.setAttribute('id', identityId);
				identityRequest.setAttribute('select', 'is_alias');
				identityRequest = identityRequest.apply();
				var isAlias = identityRequest.dom.selectSingleNode('//Item/is_alias').text === '1';
				iconName = isAlias ? 'User.svg' : 'Group.svg';
			}

			return {isWorld: isWorld, src: imagesFolderUrl + iconName};
		}

		var aras = TopWindowHelper.getMostTopWindowWithAras(window).aras;
		var Item;
		var smTemplate;
		var id;
		if (params && params.itemTypeName && params.itemID) {
			Item = {Item: {
				'@attrs': {
					type: params.itemTypeName,
					action: 'VC_GetSecureMessageViewTemplate',
					select: 'template',
					id: params.itemID
				},
				'markup_holder_config_id': params.item.getProperty('config_id')
			}};
		} else {
			// F1E9620C1AB3408588708B68E18D51DE - id of DefaultSMTemplate.
			Item = {Item: {
				'@attrs': {
					type: 'SecureMessageViewTemplate',
					action: 'get',
					select: 'template',
					id: 'F1E9620C1AB3408588708B68E18D51DE'
				}
			}};
		}

		require(['dojox/dtl', 'dojox/dtl/Context'], function() {
			$.soap($.jsonToXml(Item)).then($.xmlToJson)
				.then(function(json) {
					smTemplate =  new dojox.dtl.Template(json.Item.template);
					tooltipContainerId = json.Item['@attrs'].id;
				}, function() {
					aras.AlertError('The Secure Messages loading is failed - SecureMessageViewTemplate item is not exist');
				});
		});

		function getMessageContent(message, contextItem) {
			if (contextItem['disabled_by_id']) {
				return message.getDisabledMessageText();
			} else {
				var content = message.getHtmlMessage(contextItem.comments);
				var builder = new dojox.string.Builder();
				builder.safe = true;
				builder.append(content);
				return builder;
			}
		}

		var tooltipContainerId;
		var tooltipContainerName = 'SecureMessageViewTemplate';
		var tooltipPropertyName = 'thumbnail_tooltip_template';
		var actionsMenuData = getActionsMenuData();

		return function(message, contextItem) {
			var smElement = document.createElement('div');
			if (!smTemplate) {
				return smElement;
			}
			smElement.id = contextItem.id;
			smElement.className = 'ssvc-message';
			smElement.className += (contextItem['item_type_name'] == 'Forum') ? ' forum-message' : '';
			smElement.className += (contextItem.classification === 'History') ? ' ssvc-message-history' : '';
			smElement.className += contextItem['disabled_by_id'] ? ' ssvc-message-disabled' : '';
			smElement.className += (contextItem['thread_id'] !== contextItem.id) ? ' ssvc-message-reply' : '';
			smElement.className += !message.fitsContext && !contextItem['disabled_by_id'] ? ' disabled' : '';

			var visibilityFilterNd = message.messageItem.node.selectSingleNode(
				'Relationships/Item[@type=\'SecureMessageVisibilityFilter\' and not(is_creator=\'1\')]/related_id');
			smElement.innerHTML = smTemplate.render(new dojox.dtl.Context({
				message: getMessageContent(message, contextItem),
				type: (contextItem.classification === 'Audio') || (contextItem.classification === 'Video') ? 'Media' : contextItem.classification,
				sourceMessage: getMediaContentObject(message),
				time: message.getSendDate(),
				sourceDataKeyedName: contextItem['item_keyed_name'] || 'empty',
				sourceDataIconNode: message.settings.itemTypeIcon,
				sourceDataTypeLineStyle: message.settings.style || '',
				itemInfo: [contextItem['item_major_rev'], message.context.isVersionable ? contextItem['item_version'] :
					'', (typeof(contextItem['item_state']) == 'string' ? contextItem['item_state'] : '')].filter(function(value) {
						return !!value;
					}).join(' - '),
				userName: message.userKeyedName,
				flagsCount: message.getUserIDsFlaggedBy().length || '',
				flagLabel: message.isFlaggedByUser(aras.getUserID()) ? 'Unflag' : 'Flag',
				visibleToIdentity: getVisibleToIdentityContext(visibilityFilterNd ? visibilityFilterNd.text : 'A73B655731924CD0B027E4F4D5FCC0A9'),
				actions: (message.context.type !== 'Forum') && actionsMenuData
			}));
			message.tooltipContainerId = tooltipContainerId;
			message.tooltipContainerName = tooltipContainerName;
			message.tooltipPropertyName = tooltipPropertyName;

			return smElement;

			function getMediaContentObject(message) {
				var sourceMessage = null;
				var media = null;
				if (contextItem['disabled_by_id']) {
					return;
				}
				switch (contextItem.classification) {
					case 'Markup':
						if (message.markup) {
							sourceMessage = {hasSnapshot: (message.markup.id !== false), file: message.markup.getThumbnail()};
						}
						break;
					case 'Video':
						media = message.media;
						if (['mp4', 'webm', 'ogg'].indexOf(media.file.extension.toLowerCase()) !== -1) {
							sourceMessage = {
								sourceType: 'video',
								file: media.file.getUrl(),
								type: 'video/' + media.file.extension
							};
						}
						break;
					case 'Audio':
						media = message.media;
						if (['mp3', 'wav', 'ogg'].indexOf(media.file.extension.toLowerCase()) !== -1) {
							sourceMessage = {
								sourceType: 'audio',
								file: media.file.getUrl(),
								type: 'audio/' + media.file.extension
							};
						}
						break;
				}
				return sourceMessage;
			}
		};

		function getActionsMenuData() {
			var cui = aras.getMostTopWindowWithAras().cui;
			var items = cui.dataLoader.loadCommandBar('Custom', {
				locationName: 'Custom',
				'item_classification': 'discussion'
			});

			// items.nodeList - could be "like array" object
			return Array.prototype.map.call(items, function(cuiItem) {
				var cuiItemName = cuiItem.selectSingleNode('name').text;
				var cuiItemLabelNode = cuiItem.selectSingleNode('label');

				return {
					type: cuiItemName,
					value: (cuiItemLabelNode) ? cuiItemLabelNode.text : cuiItemName
				};
			});
		}
	})(window.params, ArasModules));
