﻿define(['SSVC/Scripts/Mention/TributeWrapper'], function(TributeWrapper) {
	var mentionControl = {
		requestXmlTemplate: null,
		listNodeTemplate: null,
		attachedNode: null,
		userSelect: null,

		init: function(nodeContainer) {
			var self = this;
			require([aras.getInnovatorUrl() + 'Client/Modules/aras.innovator.SSVC/Scripts/UserSelect.js'], function(UserSelect) {
				self.userSelect = new UserSelect({params: {}});
				var tributeWrapper = new TributeWrapper();
				tributeWrapper.getInstance({
					menuContainer: undefined,

					values: function(text, callback) {
						self.remoteSearch(text).then(
							function(data) {
								if (data) {
									var items = data.selectNodes('Item');
									var res = [];
									for (var i = 0; i < items.length; i++) {
										var login = aras.getItemProperty(items[i], 'login_name', '');
										var keyedName = aras.getItemProperty(items[i], 'keyed_name', '');
										var userId = aras.getItemProperty(items[i], 'id', '');
										var key = '';
										var value = '';
										if (keyedName === userId) {
											key = login; // we should use login, instead of guide (keyed_name)
											value = login;
										} else {
											key = keyedName + ' ' + login;
											value = keyedName;
										}

										res.push({
											key: key, // key for searching
											value: value, // value which will be inserted after select like @username
											id: userId,
											sourceItem: items[i]
										});
									}
									callback(res);
								}
							}
						).catch(function() {
							callback([]);
						});
					},
					selectTemplate: function(item) {
						// item.original values have already escaped, so we don't need any processing.
						return '<span contenteditable="false" class="userRef" user_id="' + item.original.id +
							'" userName="' + item.original.value + '">@<a onclick="return false;" ' +
							'href="" style="text-decoration:underline" class="user-mention-link"  user_id="' + item.original.id +
							'" >' +
							item.original.value + '</a></span>';
					},

					allowSpaces: true,
					replaceTextSuffix: '',
					trigger: '@',

					menuItemTemplate: function(item) {
						var menuItem = self.addItemToList(item.original.sourceItem);
						return menuItem.outerHTML;
					},
				}, nodeContainer);
			});
		},

		addItemToList: function(userdata) {
			var userData = {
				avatarSrc: aras.getInnovatorUrl() + 'Client/images/DefaultAvatar.svg',
				userName: aras.getItemProperty(userdata, 'keyed_name', ''),
				userLogin: aras.getItemProperty(userdata, 'login_name', ''),
				companyName: aras.getItemProperty(userdata, 'company_name', '')
			};

			if (userData.userName === aras.getItemProperty(userdata, 'id', '')) {
				userData.userName = aras.getItemProperty(userdata, 'login_name', '');
			}

			var userBlock = document.createElement('div');
			userBlock.className = 'popupSelect-listItem followUsers-listItem';

			if (!userData.companyName) {
				userBlock.className += ' followUsers-listItem-noCompany';
			}

			userBlock.innerHTML = this.userSelect.listNodeTemplate.render(new dojox.dtl.Context(userData));
			return userBlock;
		},

		remoteSearch: function(text) {
			this.userSelect.nameField.value = text;
			var usersRequestPromise = this.userSelect.sendRequest();

			inputPromise = new Promise(function(resolve, reject) {
				abortRequests = function() {
					resolve();
					abortRequests = null;
				};
			});

			inputPromise.then(function() {
				usersRequestPromise.abort();
			});
			return Promise.race([inputPromise, usersRequestPromise]);
		}
	};

	return mentionControl;
});
