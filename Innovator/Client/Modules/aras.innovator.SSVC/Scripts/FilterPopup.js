﻿define([
	'dojo/_base/declare',
	'dojo/on',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dojo/text!SSVC/Views/FilterPopup.html'],

	function(declare, on, _WidgetBase, _TemplatedMixin, template) {
		function FilterPopupLogic(filterPopupWidgetPar) {
			var fpWidget = filterPopupWidgetPar;
			var cmbMessageType;
			var cmbTimeFrame;
			var cmbDateUnits;
			var cmbSourceType;
			var filteringMessage = [];

			this.onChangeTimeFrame = function() {
				var value = this.options[this.selectedIndex].value;
				fpWidget.TimeframeLastNode.style.display = (value === 'last') ? '' : 'none';
				fpWidget.TimeframeRangeNode.style.display = (value === 'range') ? '' : 'none';
			};

			this.onClickCalendarShow = function() {
				var widget = null;
				if (this.id === 'btnDStartCalendarShow') {
					widget = fpWidget.edtDateStartNode;
				} else if (this.id === 'btnDFinishCalendarShow') {
					widget = fpWidget.edtDateFinishNode;
				}

				var param = {date: widget.value, aras: aras, format: aras.getDotNetDatePattern('short_date'),
					type: 'Date'
				};
				var wndRect = aras.uiGetElementCoordinates(widget);
				var dialog = window.parent.ArasModules.Dialog.show('iframe', param);
				dialog.move(wndRect.left - wndRect.screenLeft, wndRect.top - wndRect.screenTop);
				dialog.promise.then(function(newDate) {
					widget.value = newDate || '';
				});
			};

			this.Load = function() {
				function setupSelect(listName, id) {
					var select = fpWidget[id];
					SSVC.Utils.fillListByName(listName, select);
					return select;
				}
				cmbMessageType = setupSelect('VC_MessageFilterType', 'cmbMessageType');
				cmbSourceType = setupSelect('VC_SourceFilterType', 'cmbSourceType');
				cmbTimeFrame = setupSelect('VC_TimeframeFilterType', 'cmbTimeFrame');
				cmbDateUnits = setupSelect('VC_RangeSelector', 'cmbDateUnits');

				cmbTimeFrame.addEventListener('change', this.onChangeTimeFrame);
				fpWidget.btnDStartCalendarShowNode.addEventListener('click', this.onClickCalendarShow);
				fpWidget.btnDFinishCalendarShowNode.addEventListener('click', this.onClickCalendarShow);
			};

			this.sourceSelectState = function(isEnabled) {
				var cmbSourceType = fpWidget.cmbSourceType;
				if (cmbSourceType) {
					cmbSourceType.disabled = !isEnabled;
				}
			};

			this.validate = function() {
				var validationErrors = [];

				if (fpWidget.TimeframeLastNode.style.display != 'none') {
					if (!isPositiveInteger(fpWidget.edtDateCountNode.value)) {
						validationErrors.push(SSVC.Utils.GetResource('fp_incorrect_x_times') + ' ' + cmbDateUnits.options[cmbDateUnits.selectedIndex].value);
					}
				}

				if (fpWidget.TimeframeRangeNode.style.display != 'none') {
					if (fpWidget.edtDateStartNode.value.trim() === '') {
						validationErrors.push(SSVC.Utils.GetResource('fp_val_no_start_date'));
					}
					if (fpWidget.edtDateFinishNode.value.trim() === '') {
						validationErrors.push(SSVC.Utils.GetResource('fp_val_no_finish_date'));
					}
				}

				if (validationErrors.length) {
					aras.AlertError(validationErrors.join('\r\n'));
					return false;
				}
				return true;
			};

			this.composeFilter = function(context) {
				var filterItem = {'@attrs': {type: 'SecureMessage', action: 'get'}};
				filteringMessage = [];
				/*+++criteria values+++*/
				var type = cmbMessageType.options[cmbMessageType.selectedIndex].value;
				var typeLabel = cmbMessageType.options[cmbMessageType.selectedIndex].innerHTML;
				var source = cmbSourceType.options[cmbSourceType.selectedIndex].value;
				var sourceLabel = cmbSourceType.options[cmbSourceType.selectedIndex].innerHTML;
				var from = fpWidget.edtFromUserNameNode.value;
				var revision = fpWidget.edtRevisionNode.value;
				var state = fpWidget.edtStateNode.value;
				/*---criteria values---*/

				if (type !== 'all') {
					if (type === 'neHistory') {
						filterItem.classification = {
							'@attrs': {
								condition: 'ne'
							},
							'@value': 'History'
						};
					} else {
						filterItem.classification = type;
					}
					filteringMessage.push(typeLabel);
				}

				if (context && source !== 'all') {
					var configId = context.getProperty('config_id');
					filterItem['item_config_id'] = configId;
					filteringMessage.push(sourceLabel);
				}

				if (from) {
					filterItem['created_by_id'] = {
						Item: {
							'@attrs': {
								type: 'User'
							},
							'keyed_name': {
								'@attrs': {
									condition: 'like'
								},
								'@value': '%' + from + '%'
							}
						}
					};
					filteringMessage.push(SSVC.Utils.GetResource('fp_from').toLowerCase() + ' "' + from + '"');
				}
				if (revision) {
					filterItem['item_major_rev'] = revision;
					filteringMessage.push(SSVC.Utils.GetResource('fp_revision').toLowerCase() + ' "' + revision + '"');
				}
				if (state) {
					filterItem['item_state'] = state;
					filteringMessage.push(SSVC.Utils.GetResource('fp_state').toLowerCase() + ' "' + state + '"');
				}
				switch (cmbTimeFrame.options[cmbTimeFrame.selectedIndex].value) {
					case 'last':
						var units = cmbDateUnits.options[cmbDateUnits.selectedIndex].value;
						var value = fpWidget.edtDateCountNode.value;
						setLastXCriteria(filterItem, units, value);
						break;
					case 'range':
						setRangeValue(filterItem, fpWidget.edtDateStartNode.value, fpWidget.edtDateFinishNode.value);
						break;
				}

				function setLastXCriteria(item, units, value) {
					var startDate = new Date();
					startDate.setUTCMinutes(startDate.getUTCMinutes() + aras.getCorporateToLocalOffset());
					switch (units) {
						case 'minutes':
							startDate.setMinutes(startDate.getMinutes() - value);
							break;
						case 'hours':
							startDate.setHours(startDate.getHours() - value);
							break;
						case 'days':
							startDate.setDate(startDate.getDate() - value);
							break;
						case 'months':
							startDate.setMonth(startDate.getMonth() - value);
							break;
					}
					var propertyValue = SSVC.Utils.GetDateTimeInNeutralFormat(startDate);
					item['created_on'] = {
						'@attrs': {
							condition: 'ge'
						},
						'@value': propertyValue
					};
					filteringMessage.push(SSVC.Utils.GetResource('fp_last') + ' ' + value + ' ' + units);
				}

				function setRangeValue(item, startDate, endDate) {
					var start = aras.convertToNeutral(startDate, 'date', aras.getDotNetDatePattern('short_date'));
					var end =  aras.convertToNeutral(endDate, 'date', aras.getDotNetDatePattern('short_date'));
					end = aras.parse2NeutralEndOfDayStr(new Date(end));
					item.and = {
						'created_on': [{
							'@attrs': {
								condition: 'ge'
							},
							'@value': start
						}, {
							'@attrs': {
								condition: 'le'
							},
							'@value': end
						}]
					};
					filteringMessage.push(startDate + ' ' + SSVC.Utils.GetResource('fp_to') + ' ' + endDate);
				}
				return filteringMessage.length === 0 ? null : filterItem;
			};

			this.resetFields = function() {
				var evt = document.createEvent('Event');
				evt.initEvent('change', false, true);

				[cmbMessageType, cmbSourceType, cmbTimeFrame, cmbDateUnits].forEach(function(select) {
					select.selectedIndex = (cmbDateUnits === select) ? 2 : 0;
					select.dispatchEvent(evt);
				});
				[fpWidget.edtFromUserNameNode, fpWidget.edtDateStartNode, fpWidget.edtDateFinishNode,
					fpWidget.edtRevisionNode, fpWidget.edtStateNode].forEach(function(input) {
						input.value = '';
					});
			};

			this.getFilteringMessages = function() {
				return filteringMessage;
			};

			function isPositiveInteger(cand) {
				var n = ~~Number(cand);
				return String(n) === cand && n > 0;
			}
		}

		return dojo.setObject('SSVC.UI.FilterPopup', declare([_WidgetBase, _TemplatedMixin], {
			templateString: template,

			TypeLabel: SSVC.Utils.GetResource('fp_type'),
			SourceLabel: SSVC.Utils.GetResource('fp_source'),
			FromLabel: SSVC.Utils.GetResource('fp_from'),
			FromDefValue: SSVC.Utils.GetResource('fp_anyone'),
			TimeframeLabel: SSVC.Utils.GetResource('fp_timeframe'),
			ToLabel: SSVC.Utils.GetResource('fp_to'),
			RevisionLabel: SSVC.Utils.GetResource('fp_revision'),
			RevisionDefValue: SSVC.Utils.GetResource('fp_all'),
			RangeLabel: SSVC.Utils.GetResource('fp_range_indicator'),
			StateLabel: SSVC.Utils.GetResource('fp_state'),
			StateDefValue: SSVC.Utils.GetResource('fp_all'),
			SearchButtonLabel: SSVC.Utils.GetResource('fp_search'),
			CancelButtonLabel: SSVC.Utils.GetResource('fp_cancel'),

			fpl: null,

			constructor: function() {
				this.fpl = new FilterPopupLogic(this);
			},

			postCreate: function() {
				this.fpl.Load();

				this.own(
					on(this.btnSearchNode, 'click', this['btnSearchNode_OnClick'].bind(this)),
					on(this.btnCancelNode, 'click', this['btnCancelNode_OnClick'].bind(this)));
			},

			composeFilter: function(context) {
				return this.fpl.composeFilter(context);
			},

			validate: function() {
				return this.fpl.validate();
			},

			getFilteringMessages: function() {
				return this.fpl.getFilteringMessages();
			},

			resetFields: function() {
				return this.fpl.resetFields();
			},

			OnBtnSearchNodeClick: function() {},
			OnBtnCancelNodeClick: function() {},

			'btnSearchNode_OnClick': function(event) {
				this.OnBtnSearchNodeClick(event);
			},
			'btnCancelNode_OnClick': function(event) {
				this.OnBtnCancelNodeClick(event);
			},

			sourceSelectState: function(isEnabled) {
				this.fpl.sourceSelectState(isEnabled);
			}
		}));
	});
