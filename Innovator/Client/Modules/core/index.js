﻿import aml from './aml';
import copyTextToBuffer from './copyTextToBuffer';
import Dialog from './Dialog';
import intl from './intl';
import MaximazableDialog from './MaximazableDialog';
import { SyncPromise, soap } from './Soap';
import { sessionSoap, alertSoapError } from './SessionSoap';
import SvgManager from './SvgManager';
import utils from './utils';
import vaultUploader from './vaultUploader';
import xml from './Xml';
import { xmlToJson, jsonToXml } from './XmlToJson';

import alertModule from '../components/alert';
import ContextMenu from '../components/contextMenu';
import splitter from '../components/splitter';
import confirmModule from '../components/confirm';
import notify from '../components/notify';
import dropdown from '../components/dropdown';
import dropdownButton from '../components/dropdownButton';
import Tab from '../components/tab';
import Toolbar from '../components/toolbar';
import {
	toolbarTemplates,
	toolbarFormatters
} from '../components/toolbarTemplates';
import BaseTypeahead from '../components/baseTypeahead';
import FilterList from '../components/filterList';
import ItemProperty from '../components/itemProperty';
import ClassificationProperty from '../components/classificationProperty';
import Nav from '../components/nav/nav';
import navTemplates from '../components/nav/navTemplates';

import { HeadWrap, RowsWrap } from '../components/grid/utils';
import Keyboard from '../components/grid/keyboard';
import GridActions from '../components/grid/actions';
import gridTemplates from '../components/grid/templates';
import GridView from '../components/grid/view';
import gridEditors from '../components/grid/editors';
import gridFormatters from '../components/grid/formatters';
import GridSearch from '../components/grid/search';
import Grid from '../components/grid/grid';

import TreeGrid from '../components/treeGrid/treeGrid';
import TreeGridActions from '../components/treeGrid/treeGridActions';
import treeGridTemplates from '../components/treeGrid/treeGridTemplates';
import TreeGridView from '../components/treeGrid/treeGridView';

window.Toolbar = Toolbar;
window.Toolbar.formatters = toolbarFormatters;
window.Toolbar.toolbarTemplates = toolbarTemplates;
window.BaseTypeahead = BaseTypeahead;
window.FilterList = FilterList;
window.ItemProperty = ItemProperty;
window.ClassificationProperty = ClassificationProperty;
window.Nav = Nav;
window.NavTemplates = navTemplates;

window.GridActions = GridActions;
window.Keyboard = Keyboard;
window.GridTemplates = gridTemplates;
window.HeadWrap = HeadWrap;
window.RowsWrap = RowsWrap;
window.GridView = GridView;
window.Grid = Grid;
window.Grid.editors = gridEditors;
window.Grid.formatters = gridFormatters;
window.Grid.search = GridSearch;

window.TreeGrid = TreeGrid;
window.TreeGridActions = TreeGridActions;
window.TreeGridTemplates = treeGridTemplates;
window.TreeGridView = TreeGridView;

Dialog.alert = alertModule;
Dialog.confirm = confirmModule;

window.ArasModules = window.ArasModules || {};
const core = {
	aml: aml,
	copyTextToBuffer: copyTextToBuffer,
	Dialog: Dialog,
	nativSoap: soap,
	soap: sessionSoap,
	alertSoapError: alertSoapError,
	intl: intl,
	jsonToXml: jsonToXml,
	MaximazableDialog: MaximazableDialog,
	SyncPromise: SyncPromise,
	SvgManager: SvgManager,
	utils: utils,
	vault: vaultUploader,
	xml: xml,
	xmlToJson: xmlToJson,
	ContextMenu: ContextMenu,
	splitter: splitter,
	notify: notify,
	dropdown: dropdown,
	dropdownButton: dropdownButton,
	Tab: Tab
};

const webComponents = {
	'aras-filter-list': FilterList,
	'aras-item-property': ItemProperty,
	'aras-classification-property': ClassificationProperty,
	'aras-nav': Nav
};

Object.keys(webComponents).forEach(function(name) {
	if (!customElements.get(name)) {
		customElements.define(name, webComponents[name]);
	}
});

window.ArasModules = Object.assign(window.ArasModules, core);
