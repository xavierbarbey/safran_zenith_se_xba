﻿const xmlFlags = (function() {
	const flags = {
		setDeclaretion: false,
		deleteTrashAttr: false
	};

	if ('ActiveXObject' in window) {
		return flags;
	}
	// Chrome not added xml declaretion after transform
	const xml = new DOMParser().parseFromString(
		'<?xml version="1.0" encoding="UTF-8" ?><doc/>',
		'text/xml'
	);
	const xsl = new DOMParser().parseFromString(
		'<?xml version="1.0" encoding="UTF-8" ?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">' +
			' <xsl:template match="/"><p>1</p></xsl:template>' +
			'</xsl:stylesheet>',
		'text/xml'
	);
	const p = new XSLTProcessor();
	p.importStylesheet(xsl);
	const f = p.transformToDocument(xml);
	const res = new XMLSerializer().serializeToString(f);

	flags.setDeclaretion = res.indexOf('<?xml') === -1;
	flags.deleteTrashAttr =
		res.indexOf('xmlns="http://www.w3.org/1999/xhtml"') !== -1;
	return flags;
})();

const sanitizer = {
	chars: {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'#': '&#35;',
		'(': '&#40;',
		')': '&#41;',
		'"': '&quot;',
		"'": '&apos;'
	},

	escapeRegExp: function(string) {
		return string.replace(/([.*+?^=!:${}()|[\]/\\])/g, '\\$1');
	},

	sanitize: function(value) {
		if (typeof value !== 'string') {
			return value;
		}

		return Object.keys(sanitizer.chars).reduce(function(val, key) {
			return val.replace(
				new RegExp(sanitizer.escapeRegExp(key), 'g'),
				sanitizer.chars[key]
			);
		}, value);
	}
};

const xml = {
	parseString: function(xmlString) {
		let newDocument;
		if (window.ActiveXObject !== undefined) {
			newDocument = new ActiveXObject('Msxml2.FreeThreadedDOMDocument.6.0');
			newDocument.setProperty('AllowXsltScript', true);
			newDocument.async = false;
			newDocument.preserveWhiteSpace = true;
			newDocument.validateOnParse = false;
			newDocument.loadXML(xmlString);
		} else {
			const domParser = new DOMParser();
			newDocument = domParser.parseFromString(xmlString, 'text/xml');
		}
		return newDocument;
	},
	parseFile: function(fileUrl) {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', fileUrl, false);
		xhr.send(null);
		return this.parseString(xhr.responseText);
	},
	selectSingleNode: function(xmlDocument, xPath) {
		if (window.ActiveXObject !== undefined) {
			return xmlDocument.selectSingleNode(xPath);
		}

		const xpe = new XPathEvaluator();
		const ownerDoc = !xmlDocument.ownerDocument
			? xmlDocument.documentElement
			: xmlDocument.ownerDocument.documentElement;
		const nsResolver = xpe.createNSResolver(!ownerDoc ? xmlDocument : ownerDoc);

		return xpe.evaluate(
			xPath,
			xmlDocument,
			nsResolver,
			XPathResult.FIRST_ORDERED_NODE_TYPE,
			null
		).singleNodeValue;
	},
	selectNodes: function(xmlDocument, xPath) {
		if (window.ActiveXObject !== undefined) {
			const nodesObjects = xmlDocument.selectNodes(xPath);
			if (nodesObjects) {
				return Array.prototype.slice.call(nodesObjects);
			} else {
				return [];
			}
		}

		const xpe = new XPathEvaluator();
		const ownerDoc = !xmlDocument.ownerDocument
			? xmlDocument.documentElement
			: xmlDocument.ownerDocument.documentElement;
		const nsResolver = xpe.createNSResolver(!ownerDoc ? xmlDocument : ownerDoc);

		const result = xpe.evaluate(
			xPath,
			xmlDocument,
			nsResolver,
			XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
			null
		);
		let res = null;
		if (result) {
			res = [];
			for (let i = 0; i < result.snapshotLength; i++) {
				res.push(result.snapshotItem(i));
			}
		}
		return res;
	},
	escape: function(str) {
		return sanitizer.sanitize(str);
	},
	transform: function(transformDoc, stylesheetDoc) {
		if (window.ActiveXObject !== undefined) {
			const str = transformDoc.transformNode(stylesheetDoc);
			return str.replace(
				'<?xml version="1.0"?>',
				'<?xml version="1.0" encoding="UTF-8"?>'
			);
		}
		const processor = new XSLTProcessor();
		processor.importStylesheet(stylesheetDoc);
		const outputNode = this.selectSingleNode(
			stylesheetDoc,
			'./xsl:stylesheet/xsl:output'
		);
		const isHtml = outputNode && outputNode.getAttribute('method') === 'html';

		const resultDoc = processor.transformToDocument(transformDoc);
		if (
			!isHtml &&
			xmlFlags.setDeclaretion &&
			!(outputNode && outputNode.getAttribute('omit-xml-declaration') === 'yes')
		) {
			const piElem = resultDoc.createProcessingInstruction(
				'xml',
				'version="1.0" encoding="UTF-8"'
			);
			resultDoc.insertBefore(piElem, resultDoc.firstChild);
		}
		let resultString = isHtml
			? resultDoc.documentElement.outerHTML
			: this.getXml(resultDoc);
		if (!isHtml && xmlFlags.deleteTrashAttr) {
			const trashStr = ' xmlns="http://www.w3.org/1999/xhtml"';
			resultString = resultString.split(trashStr).join('');
		}
		return resultString;
	},
	createNode: function(xmlDocument, nodeType, nodeName, namespaceUrl) {
		if (window.ActiveXObject !== undefined) {
			return xmlDocument.createNode(nodeType, nodeName, namespaceUrl);
		}
		return xmlDocument.createElementNS(namespaceUrl, nodeName);
	},
	getText: function(xmlNode) {
		if (window.ActiveXObject !== undefined) {
			return xmlNode.text;
		}
		return xmlNode.textContent;
	},
	setText: function(xmlNode, value) {
		value = value || '';
		if (window.ActiveXObject !== undefined) {
			xmlNode.text = value;
			return;
		}
		xmlNode.textContent = value;
	},
	getXml: function(xmlDocument) {
		if (window.ActiveXObject !== undefined) {
			return xmlDocument.xml;
		}
		let resultString = new XMLSerializer().serializeToString(xmlDocument);
		// Edge can't read declaration <?xml .. ?>
		if (
			xmlDocument.firstChild &&
			xmlDocument.firstChild.nodeType ===
				xmlDocument.PROCESSING_INSTRUCTION_NODE &&
			resultString.indexOf('<?xml') === -1
		) {
			resultString =
				'<?xml ' + xmlDocument.firstChild.nodeValue + ' ?>' + resultString;
		}
		return resultString;
	},
	getError: function(xmlDocument) {
		if (window.ActiveXObject !== undefined) {
			return xmlDocument.parseError;
		}
		let documentNode = xmlDocument.documentElement;
		if (documentNode) {
			let errorChildNum = 0;
			// for Chrome browser that return html instead parseerror xml block
			if (
				documentNode.nodeName === 'html' &&
				documentNode.firstChild &&
				documentNode.firstChild.firstChild
			) {
				documentNode = documentNode.firstChild.firstChild;
				errorChildNum = 1;
			}

			if (documentNode.nodeName === 'parsererror') {
				return {
					errorCode: 1,
					srcText: '',
					reason: documentNode.childNodes[errorChildNum].nodeValue
				};
			}
		}
		return { errorCode: 0 };
	}
};
export default xml;
