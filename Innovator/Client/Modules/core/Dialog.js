﻿const dialogTypes = {
	WhereUsed: {
		classList: 'whereUsed'
	},
	SearchDialog: {
		classList: 'searchDialog',
		content: 'searchDialog.html',
		title: 'Search dialog'
	},
	ImageBrowser: {
		classList: 'imageBrowser',
		content: 'ImageBrowser/imageBrowser.html',
		title: 'Image Browser'
	},
	Date: {
		classList: 'date',
		title: 'Date Dialog',
		content: 'dateDialog.html'
	},
	HTMLEditorDialog: {
		content: 'HTMLEditorDialog.html',
		size: 'max',
		classList: 'htmlEditorDialog'
	},
	RevisionsDialog: {
		classList: 'revisionsDialog',
		title: 'Item Versions',
		content: 'revisionDialog.html'
	},
	ManageFileProperty: {
		title: 'Manage File',
		content: 'manageFileDialog.html',
		classList: 'manageFileProperty'
	},
	Text: {
		title: 'Text Dialog',
		content: 'textDialog.html',
		classList: 'text'
	},
	Color: {
		title: 'Color Dialog',
		content: 'colorDialog.html',
		classList: 'color'
	}
};

const htmlHelper = {
	buildTemplate: function(template, dialog) {
		dialog.innerHTML = template;
	},
	setTitle: function(title, dialog) {
		const titleNode = dialog.querySelector(
			'.arasDialog-titleBar .arasDialog-title'
		);
		if (titleNode) {
			titleNode.textContent = title || '';
		}
	},
	setUpOptions: function(options, dialog) {
		if (options) {
			dialog.style.width = options.dialogWidth + 'px';
			dialog.style.height = options.dialogHeight + 'px';
			if (options.classList) {
				dialog.classList.add(options.classList);
			}
		}
	},
	loadContent: function(type, contentNode, data) {
		if (type === 'iframe') {
			const iframe = document.createElement('iframe');
			iframe.className = 'arasDialog-iframe';
			iframe.src = data;
			iframe.autofocus = true;
			contentNode.appendChild(iframe);
			contentNode.classList.add('arasDialog-content-iframe');
		}
	},
	normalizeCoords: function(block, x, y) {
		const height = block.offsetHeight;
		const width = block.offsetWidth;
		const docWidth = document.documentElement.offsetWidth;
		const docHeight = document.documentElement.offsetHeight;
		let normalizedX = Math.max(x, 0);
		let normalizedY = Math.max(y, 0);

		if (x + width > docWidth) {
			normalizedX = docWidth - width;
		}
		if (y + height > docHeight) {
			normalizedY = docHeight - height;
		}

		return { x: normalizedX, y: normalizedY };
	}
};

const eventsHelper = {
	onMouseDown: function(evt) {
		this.dialogNode.classList.add('arasDialog-moving');
		this.offsetX = this.dialogNode.offsetLeft || 0;
		this.offsetY = this.dialogNode.offsetTop || 0;
		this.downedX = evt.pageX;
		this.downedY = evt.pageY;
		const self = this;
		const move = eventsHelper.onMouseMove.bind(this);
		const moveup = function(evt) {
			window.removeEventListener('mousemove', move, true);
			window.removeEventListener('mouseup', moveup, false);
			self.dialogNode.classList.remove('arasDialog-moving');
			const clientX = self.offsetX + (evt.pageX - self.downedX);
			const clientY = self.offsetY + (evt.pageY - self.downedY);
			self.move(clientX, clientY);
			self.dialogNode.style.transform = '';
		};

		window.addEventListener('mousemove', move, true);
		window.addEventListener('mouseup', moveup, false);

		evt.preventDefault();
		return;
	},
	onMouseMove: function(evt) {
		const calculatedX = parseInt(evt.pageX) - this.downedX;
		const calculatedY = parseInt(evt.pageY) - this.downedY;
		this.dialogNode.style.transform =
			'translate(' + calculatedX + 'px, ' + calculatedY + 'px)';
	},
	onClose: function(data) {
		// set the focus to the main window, because when you remove an iframe which has focus
		// in IE doesn't set focus in text fields
		if (this.type === 'iframe') {
			const iframe = this.dialogNode.querySelector('.arasDialog-iframe');
			iframe.src = 'about:blank';
		}
		this.dialogNode.parentNode.removeChild(this.dialogNode);
	},
	onWindowResize: function() {
		if (this.dialogNode.classList.contains('arasDialog-moved')) {
			this.move(this.dialogNode.offsetLeft, this.dialogNode.offsetTop);
		} else {
			const scrollTop =
				document.body.scrollTop || document.documentElement.scrollTop;
			const topValue =
				scrollTop + (window.innerHeight - this.dialogNode.offsetHeight) / 2;
			this.dialogNode.style.top = Math.max(scrollTop, topValue) + 'px';
		}
	},
	attachEvents: function(dialog) {
		dialog.dialogNode.addEventListener('close', dialog._tryResolveOnClose);
		dialog.attachedEvents.tryResolveOnClose = {
			node: dialog.dialogNode,
			eventName: 'close',
			callback: dialog._tryResolveOnClose
		};

		const onWindowResize = eventsHelper.onWindowResize.bind(dialog);
		window.addEventListener('resize', onWindowResize);
		dialog.attachedEvents.onResizeWindow = {
			node: window,
			eventName: 'resize',
			callback: onWindowResize
		};
	},
	detachEvents: function(dialog) {
		const events = Object.keys(dialog.attachedEvents);
		for (let i = 0; i < events.length; i++) {
			const event = dialog.attachedEvents[events[i]];
			event.node.removeEventListener(event.eventName, event.callback, false);
		}
		dialog.attachedEvents = {};
	}
};

const iframeHelper = {
	setDialogArguments: function(params, dialog) {
		params.dialog = dialog;
		const iframeNode = dialog.dialogNode.querySelector('.arasDialog-iframe');
		if (iframeNode) {
			iframeNode.dialogArguments = params;
		}
	}
};

function init(dialogInstance, type, args) {
	let dialogType = {};
	if (args.type && dialogTypes[args.type]) {
		dialogType = Object.assign(dialogType, dialogTypes[args.type]);
		args.classList = dialogType.classList;
	}
	const dialog = document.createElement('dialog');
	dialog.classList.add('arasDialog');
	htmlHelper.buildTemplate(dialogInstance.template, dialog);

	dialogInstance.contentNode = dialog.querySelector('.arasDialog-content');

	htmlHelper.setTitle(dialogType.title || args.title, dialog);
	htmlHelper.loadContent(
		type,
		dialogInstance.contentNode,
		dialogType.content || args.content
	);

	htmlHelper.setUpOptions(args, dialog);

	if (!dialog.showModal) {
		dialogPolyfill.registerDialog(dialog);
	}
	document.body.appendChild(dialog);

	dialogInstance.dialogNode = dialog;
	dialogInstance.promise = new Promise(function(resolve, reject) {
		dialogInstance._tryResolveOnClose = function() {
			let returnValue = dialogInstance.returnValue;
			if (returnValue === undefined && type === 'iframe') {
				const iframe = dialogInstance.dialogNode.querySelector(
					'.arasDialog-iframe'
				);
				if (iframe.contentWindow && iframe.contentWindow.returnValue) {
					returnValue = iframe.contentWindow.returnValue;
				}
			}
			// Chrome prevent opening a new window when popup dialog closes
			setTimeout(function() {
				resolve(returnValue);
			}, 0);
			eventsHelper.detachEvents(dialogInstance);
			eventsHelper.onClose.call(dialogInstance);
			dialogInstance.dialogNode = null;
		};
	});

	eventsHelper.attachEvents(dialogInstance);
	iframeHelper.setDialogArguments(args, dialogInstance);
}

function Dialog(type, args) {
	this.promise = null;
	this.dialogNode = null;
	this.contentNode = null;
	this.attachedEvents = {};
	this.type = type;
	init(this, type, args);
	this.makeMove();
	this.makeClose();
}

const staticData = {
	template:
		'<div class="arasDialog-titleBar">' +
		'<span title="Close" class="arasDialog-closeButton aras-icon-close"></span>' +
		'<span class="arasDialog-title"></span>' +
		'</div>' +
		'<div class="arasDialog-content">' +
		'</div>',

	makeMove: function() {
		const onMouseDownBinded = eventsHelper.onMouseDown.bind(this);
		this.dialogNode
			.querySelector('.arasDialog-titleBar .arasDialog-title')
			.addEventListener('mousedown', onMouseDownBinded, true);
		this.attachedEvents.dragAndDrop = {
			node: this.dialogNode.querySelector(
				'.arasDialog-titleBar .arasDialog-title'
			),
			eventName: 'mousedown',
			callback: onMouseDownBinded
		};
	},
	makeClose: function() {
		const onCloseButtonBinded = this.close.bind(this, undefined);
		this.dialogNode
			.querySelector('.arasDialog-titleBar .arasDialog-closeButton')
			.addEventListener('click', onCloseButtonBinded);
		this.attachedEvents.onCloseBtn = {
			node: this.dialogNode.querySelector(
				'.arasDialog-titleBar .arasDialog-closeButton'
			),
			eventName: 'click',
			callback: onCloseButtonBinded
		};
	},
	makeAutoresize: function() {
		const oldBox = { w: window.outerWidth, h: window.outerHeight };
		const deltaWidth = 40;
		const deltaHeight = 45; // this deltas are used for detect a necessity to resize the parent window of the dialog
		const oldBoxWidthRatio = oldBox.w;
		const oldBoxHeightRation = oldBox.h;

		// trick for calculating dialog sizes because
		// when max-height or max-width less than original sizes
		// so to get it is not possible
		this.dialogNode.style.maxWidth = 'none';
		this.dialogNode.style.maxHeight = 'none';

		const widthWithDelta = this.dialogNode.offsetWidth + deltaWidth;
		const heightWithDelta = this.dialogNode.offsetHeight + deltaHeight;
		this.dialogNode.style.maxWidth = '100%';
		this.dialogNode.style.maxHeight = '100%';

		if (
			window.TopWindowHelper &&
			window.TopWindowHelper.getMostTopWindowWithAras(window)
		) {
			const topWnd = TopWindowHelper.getMostTopWindowWithAras(window);

			if (
				oldBoxWidthRatio < widthWithDelta ||
				oldBoxHeightRation < heightWithDelta
			) {
				const newHeight =
					oldBoxHeightRation < heightWithDelta
						? parseInt(heightWithDelta)
						: oldBox.h;
				const newWidth =
					oldBoxWidthRatio < widthWithDelta
						? parseInt(widthWithDelta)
						: oldBox.w;
				topWnd.aras.browserHelper.resizeWindowTo(topWnd, newWidth, newHeight);
				this.promise.then(function() {
					// Resize callback for promise which does not allow to correctly define the next chain parts.
					// This is related with browser performance for async operations
					setTimeout(function() {
						topWnd.aras.browserHelper.resizeWindowTo(
							topWnd,
							oldBox.w,
							oldBox.h
						);
					}, 0);
				});
			}
		}
	},

	show: function() {
		if (this.dialogNode.style.display !== 'none') {
			this.dialogNode.showModal();
		}
		this.dialogNode.style.display = '';
		this.makeAutoresize();
	},
	hide: function() {
		this.dialogNode.style.display = 'none';
	},
	close: function(data) {
		if (this.dialogNode && this.dialogNode.hasAttribute('open')) {
			this.returnValue = data;
			this.dialogNode.close();
		}
	},
	move: function(left, top) {
		const normalized = htmlHelper.normalizeCoords(this.dialogNode, left, top);
		this.dialogNode.style.left = normalized.x + 'px';
		this.dialogNode.style.top = normalized.y + 'px';
		if (!this.dialogNode.classList.contains('arasDialog-moved')) {
			this.dialogNode.classList.add('arasDialog-moved');
		}
	},
	setTitle: function(title) {
		htmlHelper.setTitle(title, this.dialogNode);
	},
	resizeContent: function(width, height) {
		const titleHeight = this.dialogNode.firstChild.offsetHeight + 2; // 2px border width;
		this.resize(width, height + titleHeight);
	},
	resize: function(width, height) {
		this.dialogNode.style.width = width + 'px';
		this.dialogNode.style.height = height + 'px';
		this.makeAutoresize();
		eventsHelper.onWindowResize.call(this);
	}
};
Dialog.prototype = staticData;

const classData = {
	show: function(type, args) {
		const dialog = new Dialog(type, args);
		dialog.show();
		return dialog;
	}
};
Object.assign(Dialog, classData);

export default Dialog;
