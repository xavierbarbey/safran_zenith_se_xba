﻿const utils = {};

utils.mixin = function(...arg) {
	const target = arg[0];

	const _len = arg.length;
	const sources = Array(_len > 1 ? _len - 1 : 0);

	for (let _key = 1; _key < _len; _key++) {
		sources[_key - 1] = arg[_key];
	}

	sources.forEach(function(source) {
		const descriptors = Object.keys(source).reduce(function(descriptors, key) {
			descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
			return descriptors;
		}, {});
		// by default, Object.assign copies enumerable Symbols too
		Object.getOwnPropertyNames(source).forEach(function(sym) {
			const descriptor = Object.getOwnPropertyDescriptor(source, sym);
			if (descriptor.enumerable) {
				descriptors[sym] = descriptor;
			}
		});
		Object.defineProperties(target, descriptors);
	});
	return target;
};

utils.infernoFlags = {
	text: 1,
	htmlElement: 1 << 1,
	componentClass: 1 << 2,
	componentFunction: 1 << 3,
	componentUnknown: 1 << 4,
	hasKeyedChildren: 1 << 5,
	hasNonKeyedChildren: 1 << 6,
	svgElement: 1 << 7,
	mediaElement: 1 << 8,
	inputElement: 1 << 9,
	textareaElement: 1 << 10,
	selectElement: 1 << 11,
	void: 1 << 12
};

const flagMap = {
	input: utils.infernoFlags.inputElement,
	textarea: utils.infernoFlags.textareaElement,
	select: utils.infernoFlags.selectElement,
	svg: utils.infernoFlags.svgElement
};

utils.templateToVNode = function(template) {
	if (template === null) {
		return template;
	}

	if (typeof template !== 'object' || !template.tag) {
		if (template.flags && template.type) {
			return template;
		}

		return '' + template;
	}

	let children;
	if (template.children) {
		children = template.children.map(function(child) {
			return utils.templateToVNode(child);
		});
	}

	const props = {
		style: template.style
	};
	Object.assign(props, template.attrs || {}, template.events || {});

	const flag = flagMap[template.tag] || utils.infernoFlags.htmlElement;

	return Inferno.createVNode(
		flag,
		template.tag,
		template.className,
		children,
		props,
		template.key,
		template.ref
	);
};

utils.hashFromString = function(string) {
	let hash = 0;
	if (string.length === 0) {
		return hash;
	}
	for (let i = 0; i < string.length; i++) {
		const char = string.charCodeAt(i);
		hash = (hash << 5) - hash + char;
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
};

utils.extendHTMLElement = function(...arg) {
	return typeof Reflect === 'object'
		? Reflect.construct(HTMLElement, arg, this.constructor)
		: HTMLElement.apply(this, arg) || this;
};

export default utils;
