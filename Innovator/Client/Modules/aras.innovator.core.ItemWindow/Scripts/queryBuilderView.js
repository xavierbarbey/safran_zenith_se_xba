﻿var baseSave = window.onSaveCommand;
var baseLock = window.onLockCommand;
var baseUnlock = window.onUnlockCommand;
var baseSaveUnlockAndExit = window.onSaveUnlockAndExitCommand;

window.onSaveUnlockAndExitCommand = function() {
	var thisItem = getThisItem();
	if (thisItem.getAction() === 'add') {
		if (!aras.checkItem(thisItem.node)) {
			return;
		}
		if (!setAlias()) {
			return;
		}
	}
	baseSaveUnlockAndExit().then(function(res) {
		reload();
		return true;
	});
};

window.onSaveCommand = function() {
	var thisItem = getThisItem();
	if (thisItem.getAction() === 'add') {
		if (!aras.checkItem(thisItem.node)) {
			return;
		}
		if (!setAlias()) {
			return;
		}
	}
	return baseSave().then(function(res) {
		reload();
		return true;
	});
};

window.onLockCommand = function() {
	var baseLockResult = baseLock();
	reload();
	return baseLockResult;
};

window.onUnlockCommand = function(saveChanges) {
	var baseUnlockResult = baseUnlock(saveChanges);
	reload();
	return baseUnlockResult;
};

onload = function onloadHandler() {
	document.addEventListener('loadSideBar', function() {
		dijit.byId('sidebar').switchSidebarButton('qry_ShowForm', '../Images/ShowFormOn.svg', true);
	});
	showQueryBuilderForm();
	loadQueryBuilder();
	return;
};
