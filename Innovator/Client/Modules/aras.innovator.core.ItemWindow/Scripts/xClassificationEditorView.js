﻿var baseSave = window.onSaveCommand;

window.onSaveCommand = function() {
	return baseSave().then(function(res) {
		if (!aras.isTempEx(item)) {
			dijit.byId('sidebar').getChildren().filter(function(btn) { return btn.id === 'xClass_ShowEditor'; })[0].domNode.style.display = 'block';
		}
		return true;
	});
};

onload = function onloadHandler() {
	document.addEventListener('loadSideBar', function() {
		dijit.byId('sidebar').switchSidebarButton('xClass_ShowForm', '../Images/ShowFormOn.svg', true);
	});
};
