﻿ModulesManager.define([], 'aras.innovator.core.ItemWindow/DefaultItemWindowView',
	function() {
		function getTabsState(arasObj, itemTypeNd) {
			var globalUserPref = arasObj.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_tabs_state');
			globalUserPref = globalUserPref || arasObj.getItemProperty(itemTypeNd, 'structure_view');
			if (globalUserPref === 'tab view') {
				//backward compatibility with obsolete "tab view" value
				globalUserPref = 'tabs on';
			}
			if (globalUserPref && ['tabs off', 'tabs min', 'tabs max', 'tabs on'].indexOf(globalUserPref) === -1) {
				globalUserPref = 'tabs min';
			}
			return globalUserPref;
		}

		function DefaultItemWindowView(inDom, inArgs) {
			this.inDom = inDom;
			this.inArgs = inArgs;

			if (inDom) {
				var arasObj = aras;
				var itemID = inDom.getAttribute('id');
				var itemTypeName = inDom.getAttribute('type');
				var showSSVCSidebar = false;
				if (inArgs.viewMode === 'new') {
					showSSVCSidebar = arasObj.isNeedToDisplaySSVCSidebar(inDom, 'add');
				} else {
					var isEditMode = (arasObj.isTempEx(inDom) || arasObj.isLockedByUser(inDom)) ? true : false;
					showSSVCSidebar = arasObj.isNeedToDisplaySSVCSidebar(inDom, (isEditMode ? 'edit' : 'view'));
				}
				if (showSSVCSidebar) {
					var itemTypeNd = arasObj.getItemTypeNodeForClient(itemTypeName);
					if (!itemTypeNd) {
						arasObj.AlertError(arasObj.getResource('', 'ui_methods_ex.item_type_not_found', itemTypeName));
						return null;
					}
					var discussionTemplates = itemTypeNd.selectSingleNode('Relationships/Item[@type=\'DiscussionTemplate\']');
					if (discussionTemplates) {
						this.isSSVCEnabled = true;
					} else {
						var itemConfigID = inDom.selectSingleNode('config_id');
						var checkSSVC = arasObj.IomInnovator.newItem(itemTypeName, 'VC_IsSSVCEnabledForItem');
						checkSSVC.setID(itemID);
						if (itemConfigID) {
							checkSSVC.setProperty('config_id', itemConfigID.text);
						}
						checkSSVC = checkSSVC.apply();
						var resultNode = checkSSVC.dom.selectSingleNode('//Result');

						this.isSSVCEnabled = (resultNode && resultNode.text === 'true');
					}
				} else {
					this.isSSVCEnabled = false;
				}
			}
		}

		DefaultItemWindowView.prototype = {};

		DefaultItemWindowView.prototype.getWindowProperties = function() {
			var result;

			var inArgs = this.inArgs;
			var itemNd = this.inDom;
			var arasObj = aras;
			var isNew = inArgs.viewMode === 'new';

			var itemID = itemNd.getAttribute('id');
			var itemTypeName = itemNd.getAttribute('type');
			var isEditMode = (arasObj.isTempEx(itemNd) || arasObj.isLockedByUser(itemNd)) ? true : false;

			var winH;
			var winW;
			var winBorderH;
			var winBorderW;
			var scrH = screen.availHeight;
			var scrW = screen.availWidth;
			var x;
			var y;

			var formID;

			var itemTypeNd = arasObj.getItemTypeNodeForClient(itemTypeName);
			if (!itemTypeNd) {
				arasObj.AlertError(arasObj.getResource('', 'ui_methods_ex.item_type_not_found', itemTypeName));
				return null;
			}

			var state = getTabsState(arasObj, itemTypeNd);

			if (isNew) {
				formID = arasObj.uiGetFormID4ItemEx(itemNd, 'add');
			} else {
				formID = arasObj.uiGetFormID4ItemEx(itemNd, (isEditMode ? 'edit' : 'view'));
			}

			var formNd = formID ? arasObj.getFormForDisplay(formID).node : null;
			if (formNd) {
				formH = parseInt(arasObj.getItemProperty(formNd, 'height'));
				formW = parseInt(arasObj.getItemProperty(formNd, 'width'));
			} else {
				formH = 50;
				formW = 784;
			}

			//it's important to use "top" window here to get size of OS borders
			var topWindow = window;
			winBorderH = Math.max(topWindow.outerHeight - topWindow.innerHeight, 0);
			winBorderW = Math.max(topWindow.outerWidth - topWindow.innerWidth, 0);
			if (topWindow === arasObj.getMainWindow()) {
				winBorderH += arasObj.browserHelper.getHeightDiffBetweenTearOffAndMainWindow();
			}

			//calculate window size based on a form size, window borders size and other components
			winH = formH + winBorderH + 55 /*menu*/ + 23 /*status bar*/ + (state === 'tabs off' ? 0 : 230 + 6) /*relationships and splitter*/;
			winW = formW + winBorderW + (this.isSSVCEnabled ? 30 : 0); // TODO : SSVC sidebar size 30

			x = (scrW - winW) / 2;
			if (x < 0) {
				x = 0;
			}
			y = (scrH - winH) / 2;
			if (y < 0) {
				y = 0;
			}

			// otherElements = menu + toolbar + relationship grid + status bar;
			var otherElementsH = winH - formH;
			if (winH > scrH) {
				formH = scrH - otherElementsH;
			}

			result = {height: winH, width: winW, x: x, y: y, formHeight: formH, formWidth: formW};
			return result;
		};

		DefaultItemWindowView.prototype.getWindowArguments = function() {
			var result = {};

			var itemNd = this.inDom;
			var arasObj = aras;
			var itemTypeName = itemNd.getAttribute('type');
			var itemTypeNd = arasObj.getItemTypeNodeForClient(itemTypeName);
			if (!itemTypeNd) {
				arasObj.AlertError(arasObj.getResource('', 'ui_methods_ex.item_type_not_found', itemTypeName));
				return null;
			}

			var isEditMode = (arasObj.isTempEx(itemNd) || arasObj.isLockedByUser(itemNd)) ? true : false;

			var itemID = itemNd.getAttribute('id');
			var typeID = itemNd.getAttribute('typeId');
			var isNew = (itemNd.getAttribute('action') == 'add' && itemNd.getAttribute('isTemp') == '1') ? true : false;

			var keyedName = arasObj.getKeyedNameEx(itemNd);
			var itemTypeLabel = arasObj.getItemProperty(itemTypeNd, 'label');

			var isFile = (itemTypeName == 'File');

			var viewType = '';

			if (itemTypeName == 'Report') {
				viewType += 'reporttool';
			} else if (itemTypeName == 'Method') {
				viewType += 'methodeditor';
			} else {
				viewType += 'tab';
			}

			var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
			itemTypeLabel = (itemTypeLabel ? itemTypeLabel : itemTypeName);

			var title = arasObj.getResource('', tmpKey, itemTypeLabel, keyedName);
			var databaseName = arasObj.getDatabase();

			result.isSSVCEnabled = this.isSSVCEnabled;
			result.item = itemNd;
			result.itemID = itemID;
			result.itemTypeName = itemTypeName;
			result.itemType = itemTypeNd;
			result.itemTypeLabel = itemTypeLabel;
			result.title = title;
			result.viewType = viewType;
			result.databaseName = databaseName;
			result.isNew = isNew;
			result.viewMode = 'tab view';
			result.isEditMode = isEditMode;
			result.reserveSpaceForSidebar = this.isSSVCEnabled;

			return result;
		};

		DefaultItemWindowView.prototype.getViewUrl = function() {
			return '/Modules/aras.innovator.core.ItemWindow/tabItemView';
		};

		DefaultItemWindowView.prototype.getWindowUrl = function(formHeight) {
			var result;
			var arasObj = aras;
			var itemNd = this.inDom;
			var itemTypeName = itemNd.getAttribute('type');
			var itemTypeNd = arasObj.getItemTypeNodeForClient(itemTypeName);
			if (!itemTypeNd) {
				arasObj.AlertError(arasObj.getResource('', 'ui_methods_ex.item_type_not_found', itemTypeName));
				return null;
			}

			var state = getTabsState(arasObj, itemTypeNd);

			result = arasObj.getBaseURL() + this.getViewUrl() + '?state=' + encodeURI(state);
			if (formHeight !== undefined) {
				result += '&formHeight=' + formHeight;
			}
			return result;
		};

		return DefaultItemWindowView;
	});
