﻿ModulesManager.define([],
	'aras.innovator.core.Core/AsyncResult',
	function() {
		function AsyncResult() {
			this.isResolved = false;
			var thisObj = this;
			var handlers = [];
			var resolveArgs;

			function runHandler(handler, args) {
				try {
					var argsArray;
					if (args && args.toString() === '[object Arguments]') {
						argsArray = Array.prototype.slice.call(args, 0);
					} else {
						argsArray = [].concat(args);
					}
					var callbackResult = handler.callback.apply(thisObj, argsArray);
					if (callbackResult instanceof AsyncResult || callbackResult instanceof Promise) {
						callbackResult.then(function(localArgs) {
							handler.asyncResult.resolve(localArgs);
						});
					} else {
						handler.asyncResult.resolve(callbackResult);
					}
				} catch (exception) {
					if (handler.exceptionCallback) {
						handler.exceptionCallback.call(thisObj, exception);
					} else {
						throw exception;
					}
				}
			}

			this.resolve = function(args) {
				resolveArgs = args;
				for (var i = 0; i < handlers.length; i++) {
					var handler = handlers[i];
					runHandler(handler, args);
				}

				this.isResolved = true;
			};

			this.then = function(callback, exceptionCallback) {
				var callbackAR = ModulesManager.createAsyncResult();
				var handler = {callback: callback, exceptionCallback: exceptionCallback, asyncResult: callbackAR};
				handlers.push(handler);
				if (this.isResolved) {
					runHandler(handler, resolveArgs);
				}
				return callbackAR;
			};
		}

		return AsyncResult;
	});
