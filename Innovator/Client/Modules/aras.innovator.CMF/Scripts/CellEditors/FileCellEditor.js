﻿/* global define */
define(['dojo/_base/declare', 'dijit/TooltipDialog', 'dojo/on', 'dojo/keys', './CellEditorBase', 'dijit/registry'],
function(declare, TooltipDialog, on, keys, CellEditorBase) {
	var aras = parent.aras;
	var valueToReturn;

	return declare(CellEditorBase, {
		show: function(cell, onCellEditorClosed) {
			var selectedFile = aras.newItem('File');
			if (selectedFile) {
				aras.itemsCache.addItem(selectedFile);
				aras.saveItemEx(selectedFile);
				var xmldom = aras.createXMLDocument();
				xmldom.loadXML(selectedFile.xml);
				var id = xmldom.selectSingleNode('Item').getAttribute('id');
				valueToReturn = id;
				onCellEditorClosed();
			} else {
				valueToReturn = '';
				onCellEditorClosed();
			}
		},

		getValue: function() {
			return valueToReturn;
		}
	});
});
