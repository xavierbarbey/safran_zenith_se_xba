﻿define(['dojo/_base/declare'], function(declare) {
	var snippets = {
		'and': '(${SELECTED_TEXT:${CURRENTLINE_TEXT_RANGE}}) AND (${0})',
		'or': '(${SELECTED_TEXT:${CURRENTLINE_TEXT_RANGE}}) OR (${0})',
		'not': 'NOT(${0:${SELECTED_TEXT:${CURRENTLINE_TEXT_RANGE}}})'
	};
	return declare(null, {
		constructor: function(snippetManager) {
			if (!snippetManager) {
				return;
			}
			snippetManager.variables.CURRENTLINE_TEXT_RANGE = function(editor, varname) {
				var currentRow = editor.getCursorPosition().row;
				var range =  editor.selection.getLineRange(currentRow, true);
				editor.selection.setRange(range);
				return editor.session.getLine(currentRow);
			};
		},
		getSnippets: function() {
			return snippets;
		}
	});
});
