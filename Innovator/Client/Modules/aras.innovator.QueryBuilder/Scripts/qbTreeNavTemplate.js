﻿define(['QB/Scripts/conditionsTree',
	'QB/Scripts/conditionTreeNavigator',
	'QB/Scripts/ConditionTreeVisitor/textPortionsPrinter'], function(ConditionsTree, ConditionTreeNavigator, TextPortionsPrinter) {
	return {
		initTreeTemplates: function(tree, listOfDissabled) {
			listOfDissabled = listOfDissabled || [];
			var parentNodeClass = 'aras-nav-parent';
			var iconPaddingClass = 'icon-padding';
			var parentExpandedNodeClass = 'aras-nav-parent aras-nav-parent_expanded';
			var selectedNodeClass = 'aras-nav-selected';
			var elementNodeClass = 'aras-nav-element';
			var whereUseConditionClass = 'query-item__where-use-condition';
			var joinConditionClass = 'query-item__join-condition';

			var controlsContainerClass = 'query-item-controls';

			var controlClass = 'query-item-controls__control';
			var activeControlClass = 'query-item-controls__control_active';

			var conditionsControlClass = 'query-item-controls__conditions';
			var propertiesControlClass = 'query-item-controls__properties';
			var recursionSourceControlClass = 'query-item-controls__recursion-source';
			var recursionControlClass = 'query-item-controls__recursion';
			var dissabledTreeElement = 'dissabled-tree-element';
			var iconWithUpArrowClass = 'icon-with-up-arrow';
			var imgContainerClass = 'img-container';

			var maxLengthOfConditions = 50;

			var getDissableClass = function(treeItem) {
				if (treeItem && listOfDissabled.indexOf(treeItem.id) > -1) {
					return ' ' + dissabledTreeElement;
				}
				return '';
			};

			var getSelectClass = function(navInstance, props) {
				if (navInstance.selectedItemKey === props.nodeKey) {
					return ' ' + selectedNodeClass;
				}
				return '';
			};

			return function(navInstance) {
				var infernoFlags = ArasModules.utils.infernoFlags;
				var templates = NavTemplates(navInstance);
				templates.node = function(props) {
					var lifecycle = {
						onComponentDidMount: function(node) {
							tree.tree.htmlNodes[props.value.id] =  node;
						}
					};
					return props.value && dataStore.getChildren(props.value).length > 0 ?
						Inferno.createVNode(infernoFlags.componentFunction, templates.parentNode, null, null, props, null, lifecycle) :
						Inferno.createVNode(infernoFlags.componentFunction, templates.childNode, null, null, props, null, lifecycle);
				};
				templates.parentNode = function(props) {
					var treeItem = props.value;
					var children = treeItem.children || [];

					var ulOfChildren = Inferno.createVNode(infernoFlags.htmlElement, 'ul', null,
						children.map(function(childKey) {
							return Inferno.createVNode(
								infernoFlags.componentFunction,
								templates.node, null, null,
								{
									nodeKey: childKey,
									value: navInstance.data.get(childKey)
								}
							);
						})
					);

					var defaultClasses = elementNodeClass + getDissableClass(treeItem) + getSelectClass(navInstance, props);

					var rowNode = Inferno.createVNode(infernoFlags.htmlElement, 'div', defaultClasses,
						[
							Inferno.createVNode(infernoFlags.htmlElement, 'div', 'aras-nav-icon', [
								Inferno.createVNode(infernoFlags.htmlElement, 'span')
							]), // arrow
							Inferno.createVNode(infernoFlags.componentFunction, templates.icon, null, null, {treeItem: treeItem}), // icon
							Inferno.createVNode(infernoFlags.componentFunction, templates.joinConditionLabel, null, null, {treeItem: treeItem}), //joinCondition
							Inferno.createVNode(infernoFlags.htmlElement, 'span', 'query-item__name', treeItem.element.name), // name
							Inferno.createVNode(infernoFlags.componentFunction, templates.whereUseConditionLabel, null, null, {treeItem: treeItem}), //whereUseConditionLabel
							Inferno.createVNode(infernoFlags.componentFunction, templates.controlsContainer, null, null, {treeItem: treeItem}) //controlsContainer
						]
					);

					var isExpanded = navInstance.expandedItemsKeys.has(props.nodeKey);
					var classNames = isExpanded ? parentExpandedNodeClass : parentNodeClass;

					return Inferno.createVNode(infernoFlags.htmlElement, 'li', classNames, [
							rowNode,
							ulOfChildren
						],
						{'data-key': props.nodeKey}
					);
				};

				templates.childNode = function(props) {
					var treeItem = props.value;
					var defaultClasses = elementNodeClass + getDissableClass(treeItem) + getSelectClass(navInstance, props);

					var rowNode = Inferno.createVNode(infernoFlags.htmlElement, 'div', defaultClasses,
						[
							Inferno.createVNode(infernoFlags.componentFunction, templates.icon, null, null, {treeItem: treeItem}), // icon
							Inferno.createVNode(infernoFlags.componentFunction, templates.joinConditionLabel, null, null, {treeItem: treeItem}), //joinCondition
							Inferno.createVNode(infernoFlags.htmlElement, 'span', 'query-item__name', treeItem.element.name), // name
							Inferno.createVNode(infernoFlags.componentFunction, templates.whereUseConditionLabel, null, null, {treeItem: treeItem}), //whereUseConditionLabel
							Inferno.createVNode(infernoFlags.componentFunction, templates.controlsContainer, null, null, {treeItem: treeItem}) //controlsContainer
						]
					);

					var classNames = parentNodeClass + ' ' + iconPaddingClass;

					return Inferno.createVNode(infernoFlags.htmlElement, 'li', classNames, [
							rowNode
						],
						{'data-key': props.nodeKey}
					);
				};

				var getConditionLabelVNode = function(conditionsTree, conditionClass) {
					var conditionTreeNavigator = new ConditionTreeNavigator();
					var textPrinter = new TextPortionsPrinter(conditionTreeNavigator);
					conditionTreeNavigator.accept(conditionsTree.root, textPrinter);
					var totalTextLength = 0;
					var conditionItems = [];
					textPrinter.printTextPortions(function(text, isInvalid) {
						if (totalTextLength > maxLengthOfConditions) {
							return;
						}
						var newTotalTextLength = totalTextLength + text.length;
						if (newTotalTextLength > maxLengthOfConditions) {
							text = text.substr(0, maxLengthOfConditions - totalTextLength) + '...';
						}
						totalTextLength += text.length;
						var conditionClass = isInvalid ? 'query-item_error' : conditionClass;
						var newItem = Inferno.createVNode(infernoFlags.htmlElement, 'span', conditionClass, text);
						conditionItems.push(newItem);
					});
					return Inferno.createVNode(infernoFlags.htmlElement, 'span', conditionClass, conditionItems);
				};

				templates.joinConditionLabel = function(props) {
					if (tree.tree.showJoinCondition !== true) {
						return;
					}

					var treeItem = props.treeItem;
					if (treeItem.id === 'root') {
						return null;
					}

					var queryDefinitionItem = treeItem.node.selectSingleNode('ancestor::Item[@type="qry_QueryDefinition"]');
					var parentElement = tree.getTreeElementById(treeItem.parentId);
					var qrXPath = 'Relationships/Item[@type=\'qry_QueryReference\' and (ref_id=\'' + treeItem.referenceRefId + '\')]';
					var queryReferenceItem = queryDefinitionItem.selectSingleNode(qrXPath);
					var filterXml = aras.getItemProperty(queryReferenceItem, 'filter_xml');
					var conditionDom = new XmlDocument();
					conditionDom.loadXML(filterXml.replace(/\r?\n|\r|\t/g, ''));
					var conditionsTree = new ConditionsTree();
					conditionsTree.fromXml(conditionDom, [parentElement._element, treeItem._element]);
					var result = getConditionLabelVNode(conditionsTree, joinConditionClass);
					return result;
				};

				templates.whereUseConditionLabel = function(props) {
					if (tree.tree.showWhereUseCondition !== true) {
						return;
					}

					var treeItem = props.treeItem;

					var filterXml = aras.getItemProperty(treeItem.node, 'filter_xml');
					if (!filterXml) {
						return;
					}

					var conditionDom = new XmlDocument();
					conditionDom.loadXML(filterXml.replace(/\r?\n|\r|\t/g, ''));
					var conditionsTree = new ConditionsTree();
					conditionsTree.fromXml(conditionDom, [treeItem._element]);
					var result = getConditionLabelVNode(conditionsTree, whereUseConditionClass);
					return result;
				};

				var hasInvalidProperties = function(treeItem) {
					if (!treeItem.node) {
						return false;
					}
					var queryDefinitionItem = treeItem.node.selectSingleNode('ancestor::Item[@type="qry_QueryDefinition"]');
					return tree.validateProperties(queryDefinitionItem, treeItem);
				};

				templates.icon = function(props) {
					var treeItem = props.treeItem;
					var showWarning = hasInvalidProperties(treeItem);
					var iconPath = showWarning ? '../images/RedWarning.svg' : (tree.treeStore.getIcon(treeItem) || '../images/DefaultItemType.svg');

					if (iconPath.toLowerCase().startsWith('vault:///?fileid=')) {
						var fileId = iconPath.replace(/vault:\/\/\/\?fileid=/i, '');
						iconPath = aras.IomInnovator.getFileUrl(fileId, parent.aras.Enums.UrlType.SecurityToken);
					} else {
						iconPath = dojo.baseUrl + '../' + iconPath;
					}

					var className = imgContainerClass;
					className += (treeItem.isReferencing) ? ' ' + iconWithUpArrowClass : '';

					var imgNode = Inferno.createVNode(infernoFlags.htmlElement, 'img', null, null, {
							src: iconPath
						});

					return Inferno.createVNode(infernoFlags.htmlElement, 'div', className, imgNode);
				};

				templates.controlsContainer = function(props) {
					var treeItem = props.treeItem;
					if (tree._isForReuseQueryElement || treeItem.getType() === tree.systemEnums.TreeModelType.EmptyRootItemType) {
						return null;
					}

					var recursionClass = activeControlClass;

					if (treeItem.isRecursion) {
						recursionClass = activeControlClass + ' ' + recursionControlClass;
					} else {
						var queryDefinitionItem = treeItem.node.selectSingleNode('ancestor::Item[@type="qry_QueryDefinition"]');
						var isHaveRecursionXPath = 'Relationships/Item[@type=\'qry_QueryReference\' and not(@action=\'delete\') and child_ref_id= \'' + treeItem.refId + '\']';
						var nodes = queryDefinitionItem.selectNodes(isHaveRecursionXPath);

						if (nodes.length > 1) {
							recursionClass = activeControlClass + ' ' + recursionSourceControlClass;
						}
					}

					return Inferno.createVNode(
						infernoFlags.htmlElement,
						'div',
						controlsContainerClass,
						createControlButtons({treeItem: treeItem}).concat(
							Inferno.createVNode(
								infernoFlags.htmlElement,
								'div',
								controlClass + ' ' + recursionClass,
								null,
								{'data-id': treeItem.id}
							)
						)
					);
				};
				var createControlButtons = function(props) {
					var treeItem = props.treeItem;
					var buttonClasses = {
						sortOrderButton: 'query-item-controls__control query-item-controls__sort-order' +
						(tree._isReadOnly ? '' : '') + (tree.isCommandContainerFilled(treeItem.node, 'btnSortOrderId') ? ' query-item-controls__control_active' : ''),
						conditionsButton: controlClass + ' ' + conditionsControlClass +
						(tree.isCommandContainerFilled(treeItem.node, 'btnConditionsId') ? (' ' + activeControlClass) : ''),
						propertiesButton: controlClass + ' ' + propertiesControlClass +
						(tree.isCommandContainerFilled(treeItem.node, 'btnPropertiesId') ? (' ' + activeControlClass) : '')
					};
					return Object.keys(buttonClasses).map(function(button) {
						return Inferno.createVNode(
							infernoFlags.htmlElement,
							'div',
							buttonClasses[button],
							null,
							{'data-id': treeItem.id}
						);
					});
				};

				return templates;
			};
		}
	};
});
