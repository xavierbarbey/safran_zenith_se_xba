﻿(function(externalParent) {
	function ItemController() {
		this._dRUIStore = [];
	}

	ItemController.prototype = {
		constructor: ItemController,
		set dRFItem(item) {
			this._dRFItem = item;
		},
		get dRFItem() {
			return this._dRFItem;
		},
		setViewModeOnRelationships: function setViewModeOnRelationships() {
			const relationships = window.relationships;
			const relationshipsIFrame = relationships && document.getElementById(relationships.currTabID);

			if (relationshipsIFrame && relationshipsIFrame.contentWindow && relationshipsIFrame.contentWindow.grid) {
				window.relationships.setViewMode();
				return;
			}
			// HACK: repeat the function till window.relationships is created
			setTimeout(setViewModeOnRelationships, 0);
		},
		onBeforeAction: function(isSave) {
			const dRFItem = this._dRFItem;

			this._dRUIStore = [].map.call(dRFItem.selectNodes('Relationships/Item[@type="dr_UI_RelationshipFamily"]'),
			function(drUIRelationshipFamilyNode) {
				return drUIRelationshipFamilyNode.parentNode.removeChild(drUIRelationshipFamilyNode);
			});

			if (!isSave) {
				this._dRUIStore = this._dRUIStore.filter(function(drUIRelationshipFamilyNode) {
					drUIRelationshipFamilyNode.removeAttribute('action');
					if (drUIRelationshipFamilyNode.getAttribute('isDirty') === '1') {
						const id = drUIRelationshipFamilyNode.getAttribute('id');
						const item = aras.itemsCache.getItem(id) || aras.getItemById('dr_Relationship', id);

						if (item) {
							const destinationItemType = aras.getItemTypeForClient(aras.getItemProperty(item, 'destination_itemtype_id'), 'id').node;

							aras.setItemProperty(drUIRelationshipFamilyNode, 'name', aras.getItemProperty(item, 'name'));
							aras.setItemProperty(drUIRelationshipFamilyNode, 'destination_itemtype_id', destinationItemType);
							aras.setItemProperty(drUIRelationshipFamilyNode, 'description', aras.getItemProperty(item, 'description'));
							drUIRelationshipFamilyNode.removeAttribute('isDirty');
							return true;
						}
						return false;
					}
					return true;
				});
				return;
			}

			this._dRUIStore = this._dRUIStore.filter(function(drUIRelationshipFamilyNode) {
				if (drUIRelationshipFamilyNode.getAttribute('action') === 'delete') {
					return false;
				}

				const id = drUIRelationshipFamilyNode.getAttribute('id');
				let drRelationshipNode = dRFItem.selectSingleNode('Relationships/Item/dr_relationship_id/Item[@id="' + id + '"]');

				if (!drRelationshipNode) {
					const drFamilyNode = dRFItem.selectSingleNode('Relationships/Item[dr_relationship_id="' + id + '"]');

					drRelationshipNode = aras.itemsCache.getItem(id) || aras.getItemById('dr_Relationship', id);
					aras.setItemProperty(drRelationshipNode, 'name', aras.getItemProperty(drUIRelationshipFamilyNode, 'name'));
					aras.setItemProperty(drRelationshipNode, 'destination_itemtype_id', aras.getItemProperty(drUIRelationshipFamilyNode, 'destination_itemtype_id'));
					aras.setItemProperty(drRelationshipNode, 'description', aras.getItemProperty(drUIRelationshipFamilyNode, 'description'));
					drRelationshipNode.setAttribute('action', 'merge');
					aras.setItemProperty(drFamilyNode, 'dr_relationship_id', drRelationshipNode);
				}
				drUIRelationshipFamilyNode.removeAttribute('isDirty');
				return true;
			});
		},
		onAfterAction: function() {
			const dRFItem = this._dRFItem;

			this._dRUIStore.forEach(function(drUIRelationshipFamilyNode) {
				const drRelationshipFamilyItem = aras.newIOMItem();
				const drUIRelationshipFamilyItem = aras.newIOMItem();

				drRelationshipFamilyItem.node = dRFItem;
				drRelationshipFamilyItem.dom = dRFItem.ownerDocument;

				drUIRelationshipFamilyItem.node = drUIRelationshipFamilyNode;
				drUIRelationshipFamilyItem.dom = drUIRelationshipFamilyNode.ownerDocument;

				drRelationshipFamilyItem.addRelationship(drUIRelationshipFamilyItem);
			});
		},
		validate: function() {
			const dRFItem = this._dRFItem;
			const drUIRelationshipFamilyNodes = dRFItem.selectNodes('Relationships/Item[@type="dr_UI_RelationshipFamily" and not(@action="delete")]');

			const unMapped = [].reduce.call(drUIRelationshipFamilyNodes, function(arr, drUIRelationshipFamilyNode) {
				const drFamilyElementNode = dRFItem.selectSingleNode('Relationships/Item[dr_relationship_id="' + drUIRelationshipFamilyNode.getAttribute('id') + '"]') ||
				dRFItem.selectSingleNode('Relationships/Item/dr_relationship_id/Item[@id="' + drUIRelationshipFamilyNode.getAttribute('id') + '"]').parentNode.parentNode;
				const drFamilyElementQueryItemNode = drFamilyElementNode.selectSingleNode('Relationships/Item[not(@action="delete")]');

				if (!drFamilyElementQueryItemNode) {
					arr.push(aras.getItemProperty(drUIRelationshipFamilyNode, 'name'));
				}
				return arr;
			}, []);

			return {
				isValid: unMapped.length === 0,
				unMapped: unMapped
			};
		}
	};
	externalParent.ItemController = ItemController;
}(window));
