﻿define([], function() {
	return {
		initTemplate: function(navInstance) {
			return function(navInstance) {
				const infernoFlags = ArasModules.utils.infernoFlags;
				const rootNodeClass = 'aras-nav';
				const selectedNodeClass = 'aras-nav-selected';

				const templates = {
					root: function Root() {
						const rootNodes = [];
						navInstance.roots.forEach(function(root) {
							const rootNode = navInstance.data.get(root);
							rootNodes.push(Inferno.createVNode(infernoFlags.componentFunction, templates.node, null, null, {
								nodeKey: root,
								value: rootNode
							}));
						});
						return Inferno.createVNode(infernoFlags.htmlElement, 'ul', rootNodeClass, rootNodes);
					},
					node: function Node(item) {
						const isSelected = (navInstance.selectedItemKey === item.nodeKey);
						const liProps = {
							'data-key': item.nodeKey,
							className: isSelected ? selectedNodeClass : null
						};

						const template = templates._getDefaultTemplate(item);

						return Inferno.createVNode(infernoFlags.htmlElement, 'li', null, template, liProps);
					},
					_getDefaultTemplate: function(item) {
						const imgNode = Inferno.createVNode(infernoFlags.htmlElement, 'img', null, null, {
							src: item.value.icon
						});
						const labelNode = Inferno.createVNode(infernoFlags.htmlElement, 'span', null, item.value.label);
						return [
							imgNode,
							labelNode
						];
					}
				};

				return templates;
			};
		}
	};
});
