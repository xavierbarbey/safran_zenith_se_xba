﻿define([
	'dojo/_base/declare',
	'MassPromote/Scripts/Classes/Components/ContextMenuModule'
], function(declare, ContextMenuModule) {
	return declare(null, {

		_items: [],
		_grid: null,
		_promoteType: '',

		constructor: function(mediator, dataProvider, statusBar) {
			this._dataProvider = dataProvider;
			this._mediator = mediator;
			this._initToolbar();
			this._initGrid();
			this._contextMenuModule = new ContextMenuModule(mediator, this._grid);
			this._statusBar = statusBar;
		},

		setLifeCycle: function(lifeCycleInfo) {
		},

		setTargetState: function(lifeCycleInfo) {
		},

		resolveConflicts: function(method) {
			if (method === 'move-to-another') {
				var invalidItems = this._dataProvider.getInvalidItems();
				this._massPromote(invalidItems);
			}
			this._dataProvider.removeInvalidItems();
			this._onGridRowsChanged();
		},

		updateItemPromoteStatus: function(promoteInfo) {
		},

		loadFromXml: function() {
			this._promoteType = this._dataProvider.getPromoteType();
			this._onGridRowsChanged();
		},

		onLifecycleMapChanged: function(lifecycleMap) {
			this._lifecycleMap = lifecycleMap;
			this._onGridRowsChanged();
		},

		onTargetStateChanged: function(targetState) {
			this._targetState = targetState;
			this._onGridRowsChanged();
		},

		onAddItemButtonClick: function() {
			var self = this;
			var param = {itemtypeName: this._promoteType, multiselect: true, fullMultiResponse: true};
			param.type = 'SearchDialog';

			var pickDialog = window.parent.ArasModules.MaximazableDialog.show('iframe', param);
			pickDialog.promise.then(function(res) {
				if (res) {
					var gridWasChanged = self._dataProvider.addItems(res);
					if (gridWasChanged) {
						self._onGridRowsChanged();
					}
				}
			});
		},

		onRemoveItemButtonClick: function() {

			var selectedRowIds = this._grid.getSelectedRowIds().map(function(id) {
				var row = this._grid.rows.get(id);
				if (row) {
					return row.itemId;
				}
			}, this);

			var rowsChanged = this._dataProvider.removeItems(selectedRowIds);
			if (rowsChanged) {
				this._onGridRowsChanged();
			}
		},

		onPromoteButtonClick: function() {
			var selectedRowIds = this._grid.getSelectedRowIds();
			if (selectedRowIds.length === 1) {
				var row = this._grid.rows.get(selectedRowIds[0]);
				if (row) {
					this._singlePromote(row);
				}
			} else {
				var self = this;
				var selectedItems = selectedRowIds.map(function(id) {
					curRow = self._grid.rows.get(id);
					return curRow.sourceObject;
				});
				this._massPromote(selectedItems);
			}
		},

		onItemPromoted: function(itemId, status) {
			this._dataProvider.updateItemStatus(itemId, status);
			this._onGridRowsChanged();
		},

		refreshItems: function() {
			this._dataProvider.refreshItems();
			this._onGridRowsChanged();
		},

		getItemIds: function() {
			const items = this._dataProvider.getItems();
			return items.map(function(el) {
				return el.getId();
			});
		},

		getPromoteType: function() {
			return this._promoteType;
		},

		_massPromote: function(items) {
			var newItemNd = aras.newItem('mpo_MassPromotion');
			aras.itemsCache.addItem(newItemNd);
			aras.setItemProperty(newItemNd, 'promote_type', this._promoteType);
			var relationships = newItemNd.ownerDocument.createElement('Relationships');
			for (var i = 0; i < items.length; i++) {
				relationships.appendChild(items[i].node.cloneNode(true));
			}
			newItemNd.appendChild(relationships);
			aras.uiShowItemEx(newItemNd, 'new');
		},

		_singlePromote: function(row) {
			var self = this;
			var param = {
				item: row.sourceObject.node.cloneNode(true),
				aras: aras,
				title: aras.getResource('', 'promotedlg.propmote', aras.getKeyedNameEx(row.sourceObject.node)),
				dialogWidth: 400,
				dialogHeight: 300,
				resizable: true,
				content: 'promoteDialog.html'
			};

			window.ArasModules.Dialog.show('iframe', param).promise.then(
				function(res) {
					if (!res) {
						return;
					}
					self._dataProvider.updateItem(res, row.sourceObject);
					self._onGridRowsChanged();
				}
			);
		},

		disable: function() {
			// disable module
		},

		onUnlockItemButtonClick: function() {
			var selectedRowIds = this._grid.getSelectedRowIds();
			var rowsChanged = false;

			for (var i = 0; i < selectedRowIds.length; i++) {
				var rowId = selectedRowIds[i];
				var row = this._grid.rows.get(rowId);
				if (row) {
					var success = this._unlockItem(row);
					if (success) {
						rowsChanged = true;
					}
				}
			}

			if (rowsChanged) {
				this._onGridRowsChanged();
			}
		},

		onPromotionStarted: function() {
			this._promotionStarted = true;
			this._toolbar.disable();
			this._contextMenuModule.disable();
		},

		selectAll: function() {
			this._grid.settings.selectedRows = this._grid.settings.indexRows.slice(0);
			this._grid.render();
		},

		_unlockItem: function(row) {
			if (aras.isLockedByUser(row.sourceObject.node)) {
				var unlockedNode = aras.unlockItemEx(row.sourceObject.node.cloneNode(true));
				if (unlockedNode) {
					this._dataProvider.updateItem(unlockedNode, row.sourceObject);
					return true;
				}
			}
		},

		_onGridRowsChanged: function() {
			if (!this._promotionStarted) {
				this._checkValidity();
			}
			const rows = this._getRows();
			this._grid.rows = rows;
			this._statusBar.setStatus('status', rows.size + ' ' + this._mediator.getResource('items'));
			this._mediator.onGridChanged({items: this._dataProvider.getItems(), promoteType: this._promoteType});
		},

		_initToolbar: function() {
			var topWindow = aras.getMostTopWindowWithAras(window);
			this._toolbar = new ToolbarWrapper({id: 'mpo_gridtoolbar', connectId: 'gridtoolbar'});

			var contextParams = {
				toolbarApplet: this._toolbar,
				item: window.item,
				locationName: 'CustomGridToolbar',
				itemType: window.itemType,
				itemID: window.itemID
			};

			topWindow.cui.loadToolbarFromCommandBarsAsync(contextParams).then(function() {
				topWindow.cui.initToolbarEvents(this._toolbar);
				this._toolbar.showToolbar('mpo_gridtoolbar');
			}.bind(this));
		},

		_initGrid: function() {
			var rows = new Map();
			this._grid = new Grid(document.getElementById('grid'), {editable: false, search: false});
			this._grid.head = this._createHeaders();
			this._grid.rows = rows;

			const self = this;
			this._grid.on('selectRow', function() {
				const isRowSelect = self._grid.settings.selectedRows.length > 0;
				self._toolbar.getItem('mpo_promote').setEnabled(isRowSelect);
			});

			this._initAditionalFormatters();
			this._ovverideGetCellType();

			this._grid.getSelectedRowIds = function() {
				return this.settings.selectedRows;
			};
		},

		_ovverideGetCellType: function() {
			this._grid.getCellType = function(headId, rowKey) {
				var formatters = {
					valid: 'validIcon',
					locked: 'lockedIcon'
				};
				return formatters[headId] ? formatters[headId] : 'text';
			};
		},

		_initAditionalFormatters: function() {
			var self = this;

			Grid.formatters.validIcon = function(headId, rowId, value) {
				if (!value) {
					return {
						className: 'aras-grid-row-cell__image',
						children: [
							{
								tag: 'img',
								className: 'aras-grid-row-image',
								attrs: {
									src: '../images/RedWarning.svg'
								}
							}
						]
					};
				} else {
					return {
						children: []
					};
				}
			};

			Grid.formatters.lockedIcon = function(headId, rowId, value, grid) {
				const row = grid.rows.get(rowId);
				const isNewItem = row.status === 'New Item';
				if (isNewItem || value) {
					const image = value ? self._getLockedImage(row.sourceObject) : '../Images/New.svg';
					const imageNode = ArasModules.SvgManager.createInfernoVNode(image);
					imageNode.className = 'aras-grid-row-image';
					return {
						className: 'aras-grid-row-cell__image',
						children: [imageNode]
					};
				} else {
					return {
						children: []
					};
				}
			};

			Grid.formatters.text = function(headId, rowId, value, grid) {
				var row = grid.rows.get(rowId);
				let className = row.promoted ? 'mpo-promoted-cell ' : '';
				className += row.valid ? '' : 'mpo-invalid-cell';
				return {
					className: className,
				};
			};
		},

		_getLockedImage: function(item) {
			var lockedById = item.getProperty('locked_by_id');
			var currentUserId = aras.getCurrentUserID();
			var lockedByMe = lockedById === currentUserId;
			var isDirty = item.getAttribute('isDirty');
			var image = '';
			if (lockedByMe) {
				if (isDirty) {
					image = '../images/LockedAndModified.svg';
				} else {
					image = '../images/LockedByMe.svg';
				}
			} else {
				image = '../images/LockedByOthers.svg';
			}
			return image;
		},

		_getRows: function() {
			var rows = new Map();
			var items = this._dataProvider.getItems();
			for (var i = 0; i < items.length; i++) {
				rows.set(i, {
					sourceObject: items[i],
					item: items[i].getProperty('keyed_name'),
					state: items[i].getPropertyAttribute('current_state', 'name'),
					locked: items[i].getProperty('locked_by_id') ? true : false,
					valid: items[i].getProperty('mpo_isItemValid') === '1',
					itemId: items[i].getID(),
					status: items[i].getProperty('mpo_status'),
					promoted: items[i].getProperty('mpo_promoted') === '1'
				});
			}
			return rows;
		},

		_checkValidity: function() {
			this._dataProvider.validateItems(this._lifecycleMap, this._targetState);
		},

		_createHeaders: function() {
			var head = new Map();

			head.set('locked', {
				label: this._mediator.getResource('locked'),
				width: 60,
				resize: true
			});

			head.set('item', {
				label: this._mediator.getResource('item'),
				width: 140,
				resize: true
			});

			head.set('state', {
				label: this._mediator.getResource('state'),
				width: 110,
				resize: true
			});

			head.set('valid', {
				label: this._mediator.getResource('valid'),
				width: 60,
				resize: true
			});

			head.set('status', {
				label: this._mediator.getResource('status'),
				width: 240,
				resize: true
			});

			return head;
		},

		getSelectedItems: function() {
			const selectedRowsIds = this._grid.getSelectedRowIds();
			return selectedRowsIds.map(function(id) {
				return this._grid.rows.get(id).sourceObject;
			}.bind(this));
		}
	});
});
