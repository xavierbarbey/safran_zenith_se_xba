﻿function EffectivityDefinitionEditorController(RuleEditorControl, RenderUtils, ExpressionSerializerControl, ExpressionGrammar, aras) {
	this.RuleEditorControl = RuleEditorControl;
	this.RenderUtils = RenderUtils;
	this.ExpressionSerializerControl = ExpressionSerializerControl;
	this.ExpressionGrammar = ExpressionGrammar;
	this.aras = aras;
}

EffectivityDefinitionEditorController.prototype = {
	constructor: EffectivityDefinitionEditorController,

	_ruleGrammarTemplateName: 'RuleConditionGroup',

	init: function() {
		this._setupContainerState();
		this._initializeControls();
	},

	loadEditorState: function(scopeId) {
		this.expressionSerializer = new this.ExpressionSerializerControl({
			aras: this.aras,
			renderUtils: this.RenderUtils.HTML,
			scopeId: scopeId,
		});

		const grammar = new this.ExpressionGrammar({
			groupName: this._ruleGrammarTemplateName,
			scope: this.expressionSerializer.getScope(),
			grammarFile: 'EffectivityServices/RuleGrammar/RuleGrammar.txt'
		});

		this._setupEditor(grammar);
	},

	setupItemContext: function(item) {
		this._expressionItem = item;
		this._isNew = this._expressionItem.getAttribute('isTemp') === '1';
		if (this._isNew) {
			this._setDefinitionValue('<expression />');
		}
	},

	_setupContainerState: function() {
		const editorControlContainer = document.getElementById('editorControlContainer');
		if (this.aras.isLockedByUser(this._expressionItem) || this._isNew) {
			editorControlContainer.classList.remove('disableEditorControlContainer');
		} else {
			editorControlContainer.classList.add('disableEditorControlContainer');
		}
	},

	_initializeControls: function() {
		if (!this.editorControl) {
			this._messageContainer = document.getElementById('editorMessageContainer');
			this.editorControl = new this.RuleEditorControl({
				connectId: 'ruleEditorContainer',
				isEditable: true,
				aras: this.aras
			});

			this.editorControl.addEventListener(this, null, 'onStateChanged', this._onEditorStateChanged);
			this.editorControl.addEventListener(this, null, 'onStateChanged', this._markExpressionItemAsDirty);
			this.editorControl.addEventListener(this, null, 'onGroupValueEntered', this._onEditorGroupValueEntered);
		}
	},

	_onEditorGroupValueEntered: function() {
		const group = this.editorControl.getGroupByName(this._ruleGrammarTemplateName);
		const expressionData = group.getParsedExpression();
		const value = this.expressionSerializer.serializeExpressionDataToXml(expressionData);
		this._setDefinitionValue(value);
	},

	_onEditorStateChanged: function() {
		if (this.editorControl.isInputValid()) {
			this._messageContainer.style.opacity = '0';
		} else {
			this._messageContainer.textContent = this.editorControl.getErrorString();
			this._messageContainer.style.opacity = '1';
		}
	},

	_setDefinitionValue: function(value) {
		this.aras.setItemProperty(this._expressionItem, 'definition', value);
	},

	_markExpressionItemAsDirty: function() {
		this._expressionItem.setAttribute('isDirty', '1');
	},

	_getDefinitionValue: function() {
		const definitionValue = {};
		definitionValue[this._ruleGrammarTemplateName] = this.expressionSerializer
			.deserializeExpressionToString(this.aras.getItemProperty(this._expressionItem, 'definition'));
		return definitionValue;
	},

	_setupEditor: function(grammar) {
		const grammarTemplate = grammar.getGrammarTemplate();
		const grammarData = grammar.getGrammarData();
		this.editorControl.setInputTemplate(grammarTemplate);
		this.editorControl.startNewInput();

		const grammarGroup = this.editorControl.getGroupByName(this._ruleGrammarTemplateName);
		grammarGroup.setParserProperty('_VariableNames', grammarData.variablesHash);
		grammarGroup.setParserProperty('_VariableTypes', grammarData.variablesTypesHash);
		grammarGroup.setParserProperty('_ValueNames', grammarData.namedConstantsHash);
		grammarGroup.setParserProperty('_arasObj', this.aras);

		this.editorControl.setValue(this._getDefinitionValue() || '');
	}
};
