﻿(function() {
	Number.isInteger = Number.isInteger || function(value) {
		return typeof value === 'number' &&
			isFinite(value) &&
			Math.floor(value) === value;
	};

	Number.isNaN = Number.isNaN || function(value) {
		return value !== value;
	};
})();
