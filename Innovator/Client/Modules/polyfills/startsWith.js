﻿(function() {
	var startsWith = String.prototype.startsWith || function(searchString, position) {
		position = position || 0;
		return this.substr(position, searchString.length) === searchString;
	};
	Object.defineProperty(String.prototype, 'startsWith', {
		value: startsWith,
		writable: false
	});

	const endsWith = String.prototype.endsWith || function(search, length) {
		if (length === undefined || length > this.length) {
			length = this.length;
		}
		return this.substring(length - search.length, length) === search;
	};
	Object.defineProperty(String.prototype, 'endsWith', {
		value: endsWith,
		writable: false
	});
})();
