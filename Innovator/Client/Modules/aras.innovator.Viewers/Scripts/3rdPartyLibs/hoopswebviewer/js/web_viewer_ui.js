var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Example;
(function (Example) {
    var AnnotationMarkup = /** @class */ (function (_super) {
        __extends(AnnotationMarkup, _super);
        function AnnotationMarkup(viewer, nodeId, anchorPoint, label) {
            var _this = _super.call(this) || this;
            _this._leaderLine = new Communicator.Markup.Shape.Line();
            _this._textBox = new Communicator.Markup.Shape.TextBox();
            _this._showAsColor = false;
            _this._nodeId = nodeId;
            _this._viewer = viewer;
            _this._leaderAnchor = anchorPoint.copy();
            _this._textBoxAnchor = anchorPoint.copy();
            _this._textBox.setTextString(label);
            _this._textBox.getBoxPortion().setFillOpacity(1.0);
            _this._textBox.getBoxPortion().setFillColor(Communicator.Color.white());
            _this._textBox.getTextPortion().setFillColor(Communicator.Color.red());
            _this._textBox.getTextPortion().setFontSize(16);
            _this._leaderLine.setStartEndcapType(Communicator.Markup.Shape.EndcapType.Arrowhead);
            return _this;
        }
        AnnotationMarkup.prototype.draw = function () {
            this._behindView = false;
            var leaderPoint3d = this._viewer.getView().projectPoint(this._leaderAnchor);
            var boxAnchor3d = this._viewer.getView().projectPoint(this._textBoxAnchor);
            if (leaderPoint3d.z <= 0.0)
                this._behindView = true;
            if (boxAnchor3d.z <= 0.0)
                this._behindView = true;
            var leaderPoint2d = Communicator.Point2.fromPoint3(leaderPoint3d);
            var boxAnchor2d = Communicator.Point2.fromPoint3(boxAnchor3d);
            this._leaderLine.set(leaderPoint2d, boxAnchor2d);
            this._textBox.setPosition(boxAnchor2d);
            var renderer = this._viewer.getMarkupManager().getRenderer();
            renderer.drawLine(this._leaderLine);
            renderer.drawTextBox(this._textBox);
        };
        AnnotationMarkup.prototype.hit = function (point) {
            var measurement = this._viewer.getMarkupManager().getRenderer().measureTextBox(this._textBox);
            var position = this._textBox.getPosition();
            if (point.x < position.x)
                return false;
            if (point.x > position.x + measurement.x)
                return false;
            if (point.y < position.y)
                return false;
            if (point.y > position.y + measurement.y)
                return false;
            return true;
        };
        AnnotationMarkup.prototype.setShowAsColor = function (showAsColor) {
            this._showAsColor = showAsColor;
        };
        AnnotationMarkup.prototype.getShowAsColor = function () {
            return this._showAsColor;
        };
        AnnotationMarkup.prototype.getNodeId = function () {
            return this._nodeId;
        };
        AnnotationMarkup.prototype.getLeaderLineAnchor = function () {
            return this._leaderAnchor.copy();
        };
        AnnotationMarkup.prototype.getTextBoxAnchor = function () {
            return this._textBoxAnchor;
        };
        AnnotationMarkup.prototype.setTextBoxAnchor = function (newAnchorPoint) {
            this._textBoxAnchor.assign(newAnchorPoint);
        };
        AnnotationMarkup.prototype.setLabel = function (label) {
            this._textBox.setTextString(label);
        };
        AnnotationMarkup.prototype.getLabel = function () {
            return this._textBox.getTextString();
        };
        return AnnotationMarkup;
    }(Communicator.Markup.MarkupItem));
    Example.AnnotationMarkup = AnnotationMarkup;
    var AnnotationRegistry = /** @class */ (function () {
        function AnnotationRegistry(viewer, pulseManager) {
            this._annotationMap = {}; // TODO: Use a native JS Map object.
            this._viewer = viewer;
            this._pulseManager = pulseManager;
            this._table = document.getElementById("AnnotationRegistry");
        }
        AnnotationRegistry.prototype.getAnnotation = function (markupHandle) {
            return this._annotationMap[markupHandle];
        };
        AnnotationRegistry.prototype.export = function () {
            var result = [];
            var keys = Object.keys(this._annotationMap);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key_1 = keys_1[_i];
                var annotation = this._annotationMap[key_1];
                result.push({
                    name: annotation.getLabel(),
                    position: annotation.getLeaderLineAnchor().forJson(),
                    nodeId: annotation.getNodeId(),
                    showAsColor: annotation.getShowAsColor()
                });
            }
            return JSON.stringify(result);
        };
        AnnotationRegistry.prototype.addAnnotation = function (markupHandle, annotation) {
            var _this = this;
            this._annotationMap[markupHandle] = annotation;
            var tr = document.createElement("tr");
            tr.id = markupHandle;
            var idTd = document.createElement("td");
            idTd.id = markupHandle + "-nodeId";
            idTd.innerText = annotation.getNodeId().toString();
            tr.appendChild(idTd);
            var nametd = document.createElement("td");
            nametd.id = markupHandle + "-name";
            nametd.innerText = annotation.getLabel();
            tr.appendChild(nametd);
            var actionstd = document.createElement("td");
            var renameButton = document.createElement("button");
            renameButton.innerText = "Rename";
            renameButton.onclick = function () {
                _this._renameAnnotation(markupHandle);
            };
            actionstd.appendChild(renameButton);
            var deleteButton = document.createElement("button");
            deleteButton.innerText = "Delete";
            deleteButton.onclick = function () {
                _this._deleteAnnotation(markupHandle);
            };
            actionstd.appendChild(deleteButton);
            tr.appendChild(actionstd);
            var showAsColortd = document.createElement("td");
            var showAsColor = document.createElement("input");
            showAsColor.type = "checkbox";
            showAsColor.id = markupHandle + "-showAsColor";
            showAsColor.classList.add("showAsColor");
            showAsColortd.appendChild(showAsColor);
            showAsColor.onchange = function (event) {
                _this._onPulseChange(markupHandle, event.target);
            };
            tr.appendChild(showAsColortd);
            this._table.appendChild(tr);
        };
        AnnotationRegistry.prototype._onPulseChange = function (markupHandle, target) {
            var annotation = this.getAnnotation(markupHandle);
            if (annotation === undefined) {
                return;
            }
            annotation.setShowAsColor(target.checked);
            if (target.checked) {
                this._pulseManager.add(annotation.getNodeId(), this._pulseManager.getDefaultColor1(), this._pulseManager.getDefaultColor2(), this._pulseManager.getDefaultPulseTime());
            }
            else {
                this._pulseManager.deletePulse(annotation.getNodeId());
                this._viewer.redraw();
            }
        };
        AnnotationRegistry.prototype._renameAnnotation = function (markupHandle) {
            var annotation = this._annotationMap[markupHandle];
            if (annotation === undefined) {
                return;
            }
            var newMarkupName = prompt("Enter a new name for " + annotation.getLabel(), annotation.getLabel());
            if (newMarkupName != null) {
                annotation.setLabel(newMarkupName);
                this._viewer.getMarkupManager().refreshMarkup();
                var element = document.getElementById(markupHandle + "-name");
                if (element !== null) {
                    element.innerText = newMarkupName;
                }
            }
        };
        AnnotationRegistry.prototype._deleteAnnotation = function (markupHandle) {
            this._viewer.getMarkupManager().unregisterMarkup(markupHandle);
            var annotation = this._annotationMap[markupHandle];
            if (annotation !== undefined) {
                this._pulseManager.deletePulse(annotation.getNodeId());
                delete this._annotationMap[markupHandle];
            }
            var element = document.getElementById(markupHandle);
            if (element !== null && element.parentElement !== null) {
                element.parentElement.removeChild(element);
            }
        };
        return AnnotationRegistry;
    }());
    Example.AnnotationRegistry = AnnotationRegistry;
    var AnnotationOperator = /** @class */ (function (_super) {
        __extends(AnnotationOperator, _super);
        function AnnotationOperator(viewer, annotationRegistry) {
            var _this = _super.call(this) || this;
            _this._previousAnchorPlaneDragPoint = null;
            _this._activeMarkup = null;
            _this._annotationCount = 1;
            _this._previousNodeId = null;
            _this._viewer = viewer;
            _this._annotationRegistry = annotationRegistry;
            return _this;
        }
        AnnotationOperator.prototype.needsSelection = function (eventType) {
            return (eventType === Communicator.EventType.MouseDown);
        };
        AnnotationOperator.prototype.onMouseDown = function (event) {
            var selection = event._getPickResult();
            if (selection !== null && selection.overlayIndex() !== 0)
                return;
            var downPosition = event.getPosition();
            if (this._selectAnnotation(downPosition)) {
                event.setHandled(true);
            }
            else if (selection !== null && selection.isNodeEntitySelection()) {
                this._annotationCount++;
                var nodeId = selection.getNodeId();
                var selectionPosition = selection.getPosition();
                var annotationMarkup = new AnnotationMarkup(this._viewer, nodeId, selectionPosition, this._viewer.getModel().getNodeName(nodeId) + " Connector");
                var markupHandle = this._viewer.getMarkupManager().registerMarkup(annotationMarkup);
                this._annotationRegistry.addAnnotation(markupHandle, annotationMarkup);
                this._startDraggingAnnotation(annotationMarkup, downPosition);
                event.setHandled(true);
            }
        };
        AnnotationOperator.prototype._startDraggingAnnotation = function (annotation, downPosition) {
            this._activeMarkup = annotation;
            this._previousAnchorPlaneDragPoint = this._getDragPointOnAnchorPlane(downPosition);
        };
        AnnotationOperator.prototype._selectAnnotation = function (selectPoint) {
            var markup = this._viewer.getMarkupManager().pickMarkupItem(selectPoint);
            if (markup) {
                this._activeMarkup = markup;
                this._previousAnchorPlaneDragPoint = this._getDragPointOnAnchorPlane(selectPoint);
                return true;
            }
            else {
                return false;
            }
        };
        AnnotationOperator.prototype.onDeactivate = function () {
            if (this._previousNodeId != null) {
                this._viewer.getModel().setNodesHighlighted([this._previousNodeId], false);
            }
            this._previousNodeId = null;
        };
        AnnotationOperator.prototype.onMouseMove = function (event) {
            var _this = this;
            if (this._activeMarkup !== null) {
                var currentAnchorPlaneDragPoint = this._getDragPointOnAnchorPlane(event.getPosition());
                var dragDelta = void 0;
                if (this._previousAnchorPlaneDragPoint !== null && currentAnchorPlaneDragPoint !== null) {
                    dragDelta = Communicator.Point3.subtract(currentAnchorPlaneDragPoint, this._previousAnchorPlaneDragPoint);
                }
                else {
                    dragDelta = Communicator.Point3.zero();
                }
                var newAnchorPos = this._activeMarkup.getTextBoxAnchor().add(dragDelta);
                this._activeMarkup.setTextBoxAnchor(newAnchorPos);
                this._previousAnchorPlaneDragPoint = currentAnchorPlaneDragPoint;
                this._viewer.getMarkupManager().refreshMarkup();
                event.setHandled(true);
            }
            else {
                this._viewer.getView().pickFromPoint(event.getPosition(), new Communicator.PickConfig()).then(function (pickResult) {
                    var selectedNodeId = pickResult.getNodeId();
                    if (selectedNodeId !== _this._previousNodeId) {
                        if (_this._previousNodeId != null) {
                            _this._viewer.getModel().setNodesHighlighted([_this._previousNodeId], false);
                        }
                        if (selectedNodeId != null) {
                            _this._viewer.getModel().setNodesHighlighted([selectedNodeId], true);
                        }
                    }
                    _this._previousNodeId = selectedNodeId;
                });
            }
        };
        AnnotationOperator.prototype.onMouseUp = function (event) {
            event; // unreferenced
            this._activeMarkup = null;
            this._previousAnchorPlaneDragPoint = null;
        };
        AnnotationOperator.prototype._getDragPointOnAnchorPlane = function (screenPoint) {
            if (this._activeMarkup === null) {
                return null;
            }
            var anchor = this._activeMarkup.getLeaderLineAnchor();
            var camera = this._viewer.getView().getCamera();
            var normal = Communicator.Point3.subtract(camera.getPosition(), anchor).normalize();
            var anchorPlane = Communicator.Plane.createFromPointAndNormal(anchor, normal);
            var raycast = this._viewer.getView().raycastFromPoint(screenPoint);
            if (raycast === null) {
                return null;
            }
            var intersectionPoint = Communicator.Point3.zero();
            if (anchorPlane.intersectsRay(raycast, intersectionPoint)) {
                return intersectionPoint;
            }
            else {
                return null;
            }
        };
        return AnnotationOperator;
    }(Communicator.Operator.Operator));
    Example.AnnotationOperator = AnnotationOperator;
})(Example || (Example = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var ViewTree = /** @class */ (function () {
            function ViewTree(elementId, viewer, treeScroll) {
                if (treeScroll === void 0) { treeScroll = null; }
                this._maxNodeChildrenSize = 300;
                this._tree = new Ui.Control.TreeControl(elementId, viewer, ViewTree.separator, treeScroll);
                this._viewer = viewer;
            }
            ViewTree.prototype.getElementId = function () {
                return this._tree.getElementId();
            };
            ViewTree.prototype.registerCallback = function (name, func) {
                this._tree.registerCallback(name, func);
            };
            ViewTree.prototype._splitIdStr = function (idStr) {
                return this._splitParts(idStr, ViewTree.separator);
            };
            ViewTree.prototype._splitParts = function (idStr, separator) {
                var splitPos = idStr.indexOf(separator);
                return [
                    idStr.substring(0, splitPos),
                    idStr.substring(splitPos + 1)
                ];
            };
            ViewTree.prototype.hideTab = function () {
                $("#" + this.getElementId() + "Tab").hide();
            };
            ViewTree.prototype.showTab = function () {
                $("#" + this.getElementId() + "Tab").show();
            };
            ViewTree.separator = '_';
            return ViewTree;
        }());
        Ui.ViewTree = ViewTree;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
/// <reference path="ViewTree.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var CadViewTree = /** @class */ (function (_super) {
            __extends(CadViewTree, _super);
            function CadViewTree(elementId, viewer, cuttingPlaneController) {
                var _this = _super.call(this, elementId, viewer) || this;
                _this._internalId = "cadviewtree";
                _this._annotationViewsString = "annotationViews";
                _this._annotationViewsLabel = "Annotation Views";
                _this._viewFolderCreated = false;
                _this._cuttingPlaneController = cuttingPlaneController;
                _this._initEvents();
                return _this;
            }
            CadViewTree.prototype._initEvents = function () {
                var _this = this;
                this._viewer.setCallbacks({
                    _firstModelLoaded: function () {
                        _this._onNewModel();
                        return Promise.resolve();
                    },
                    modelSwitched: function () {
                        _this._modelSwitched();
                    },
                    cadViewCreated: function (cadViewId) {
                        _this._onCadViewCreated(cadViewId);
                    }
                });
                this._tree.registerCallback("selectItem", function (id) {
                    _this._onTreeSelectItem(id);
                });
            };
            CadViewTree.prototype._onCadViewCreated = function (cadViewId) {
                // MRL: 04/09/2018: Currently there is no way to directly get a CadView's name from the public API and we are only given the CadViewId
                // This makes it necessary to pull down all the views to get the newly created view's name.
                var cadViews = this._viewer.model.getCadViews();
                var newCadView = {};
                newCadView[cadViewId] = cadViews[cadViewId];
                this._addCadViews(newCadView);
            };
            CadViewTree.prototype._modelSwitched = function () {
                this._tree.clear();
                this._onNewModel();
            };
            CadViewTree.prototype._onNewModel = function () {
                var cadViews = this._viewer.model.getCadViews();
                this._addCadViews(cadViews);
            };
            CadViewTree.prototype._addCadViews = function (cadViews) {
                cadViews = Communicator.Internal.fromIntegerMap(cadViews);
                this._createCadViewNodes(cadViews);
                // remove tab if there is no cad views
                if (cadViews.size <= 0) {
                    this.hideTab();
                }
                else {
                    this.showTab();
                }
                this._tree.expandInitialNodes(this._internalId);
                this._tree.expandInitialNodes(this._internalId + this._annotationViewsString);
            };
            CadViewTree.prototype._createCadViewNodes = function (cadViews) {
                var _this = this;
                if (cadViews.size === 0) {
                    return;
                }
                // Top Level views element should only be created once
                if (!this._viewFolderCreated) {
                    this._tree.appendTopLevelElement("Views", this._internalId, "viewfolder", true);
                    this._viewFolderCreated = true;
                }
                // non-annotated views
                cadViews.forEach(function (value, key) {
                    if (!_this._viewer.model.isAnnotationView(key)) {
                        _this._tree.addChild(value, _this._cadViewId(key), _this._internalId, "view", false, Ui.Desktop.Tree.CadView);
                    }
                });
                // annotation view folder
                cadViews.forEach(function (value, key) {
                    value;
                    if (_this._viewer.model.isAnnotationView(key)) {
                        if (document.getElementById(_this._internalId + _this._annotationViewsString) == null) {
                            _this._tree.addChild(_this._annotationViewsLabel, _this._internalId + _this._annotationViewsString, _this._internalId, "viewfolder", true, Ui.Desktop.Tree.CadView);
                        }
                    }
                });
                // annotation views
                cadViews.forEach(function (value, key) {
                    if (_this._viewer.model.isAnnotationView(key)) {
                        // the folder is already called Annotation Views, remove the annotation view text from the name
                        var parsedValue = value.split("# Annotation View")[0];
                        // add to annotation view folder
                        _this._tree.addChild(parsedValue, _this._cadViewId(key), _this._internalId + _this._annotationViewsString, "view", false, Ui.Desktop.Tree.CadView);
                    }
                });
            };
            CadViewTree.prototype._onTreeSelectItem = function (idStr) {
                var idParts = this._splitIdStr(idStr);
                switch (idParts[0]) {
                    case this._internalId:
                        var handleOperator = this._viewer.operatorManager.getOperator(Communicator.OperatorId.Handle);
                        handleOperator.removeHandles();
                        this._viewer.model.activateCadView(parseInt(idParts[1], 10));
                        break;
                }
                ;
                // toggle recursive selection based on what is clicked
                var thisElement = document.getElementById(idStr);
                if (thisElement !== null) {
                    if (thisElement.tagName === "LI" && idStr !== this._internalId && idStr !== this._internalId + this._annotationViewsString) {
                        thisElement.classList.add("selected");
                    }
                    else {
                        thisElement.classList.remove("selected");
                    }
                }
            };
            CadViewTree.prototype._cadViewId = function (id) {
                return this._internalId + Ui.ViewTree.separator + id;
            };
            return CadViewTree;
        }(Ui.ViewTree));
        Ui.CadViewTree = CadViewTree;
        /** @deprecated Use [[CadViewTree]] instead. */
        Ui.CADViewTree = CadViewTree;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
/// <reference path="ViewTree.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var ConfigurationsTree = /** @class */ (function (_super) {
            __extends(ConfigurationsTree, _super);
            function ConfigurationsTree(elementId, viewer) {
                var _this = _super.call(this, elementId, viewer) || this;
                _this._internalId = "configurationstree";
                _this._initEvents();
                return _this;
            }
            ConfigurationsTree.prototype._initEvents = function () {
                var _this = this;
                this._viewer.setCallbacks({
                    _firstModelLoaded: function () {
                        return _this._onNewModel();
                    },
                    modelSwitched: function () {
                        _this._modelSwitched();
                    },
                    configurationActivated: function (id) {
                        _this._tree.selectItem(_this._configurationsId("" + id), false);
                    }
                });
                this._tree.registerCallback("selectItem", function (id) {
                    _this._onTreeSelectItem(id);
                });
            };
            ConfigurationsTree.prototype._modelSwitched = function () {
                return this._onNewModel();
            };
            ConfigurationsTree.prototype._onNewModel = function () {
                var p;
                this._createConfigurationNodes();
                var showConfigTab = false;
                var model = this._viewer.model;
                if (model._cadConfigurationsEnabled()) {
                    var configurations = model.getCadConfigurations();
                    var configCount = Object.keys(configurations).length;
                    if (configCount > 1) {
                        showConfigTab = true;
                    }
                }
                if (showConfigTab) {
                    this.showTab();
                    p = this._activateDefault();
                }
                else {
                    this.hideTab();
                    p = Promise.resolve();
                }
                this._tree.expandInitialNodes(this._internalId);
                return p;
            };
            ConfigurationsTree.prototype._activateDefault = function () {
                var model = this._viewer.model;
                var id = model.getDefaultCadConfiguration();
                if (id !== null) {
                    return model.activateDefaultCadConfiguration();
                }
                return Promise.resolve();
            };
            ConfigurationsTree.prototype._createConfigurationNodes = function () {
                var _this = this;
                var configurations = this._viewer.model.getCadConfigurations();
                if (Object.keys(configurations).length > 0) {
                    this._tree.appendTopLevelElement("Configurations", this._internalId, "configurations", true);
                    $.each(configurations, function (key, value) {
                        _this._tree.addChild(value, _this._configurationsId(key), _this._internalId, "view", false, Ui.Desktop.Tree.Configurations);
                    });
                }
            };
            ConfigurationsTree.prototype._onTreeSelectItem = function (idstr) {
                var idParts = this._splitIdStr(idstr);
                switch (idParts[0]) {
                    case this._internalId:
                        var handleOperator = this._viewer.operatorManager.getOperator(Communicator.OperatorId.Handle);
                        handleOperator.removeHandles();
                        this._viewer.model.activateCadConfiguration(parseInt(idParts[1], 10));
                        break;
                }
                ;
            };
            ConfigurationsTree.prototype._configurationsId = function (id) {
                return this._internalId + Ui.ViewTree.separator + id;
            };
            return ConfigurationsTree;
        }(Ui.ViewTree));
        Ui.ConfigurationsTree = ConfigurationsTree;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Context;
        (function (Context) {
            var ContextMenuItem = /** @class */ (function () {
                function ContextMenuItem(action, element) {
                    this.action = action;
                    this.element = element;
                }
                ContextMenuItem.prototype.setEnabled = function (enabled) {
                    if (enabled)
                        $(this.element).removeClass("disabled");
                    else
                        $(this.element).addClass("disabled");
                };
                ContextMenuItem.prototype.setText = function (text) {
                    this.element.innerHTML = text;
                };
                ContextMenuItem.prototype.show = function () {
                    $(this.element).show();
                };
                ContextMenuItem.prototype.hide = function () {
                    $(this.element).hide();
                };
                return ContextMenuItem;
            }());
            Context.ContextMenuItem = ContextMenuItem;
            var ContextMenu = /** @class */ (function () {
                function ContextMenu(menuClass, containerId, viewer, isolateZoomHelper) {
                    var _this = this;
                    // TODO: Replace this with a real Map object.
                    this._transparencyIdHash = {};
                    this._activeItemId = null;
                    this._separatorCount = 0;
                    this._position = null;
                    this._modifiers = Communicator.KeyModifiers.None;
                    this._viewer = viewer;
                    this._containerId = containerId;
                    this._isolateZoomHelper = isolateZoomHelper;
                    this._menuElement = this._createMenuElement(menuClass);
                    this._contextLayer = this._createContextLayer();
                    this._initElements();
                    this._viewer.setCallbacks({
                        _firstModelLoaded: function () {
                            _this._onNewModel(); // XXX: This should probably be triggered from "modelSwitched" as well.
                            return Promise.resolve();
                        },
                    });
                }
                ContextMenu.prototype._onNewModel = function () {
                    if (this._viewer.model.isDrawing()) {
                        this._defaultContextItemMap.reset.hide();
                        if (this._defaultContextItemMap.meshlevel0 !== undefined)
                            this._defaultContextItemMap.meshlevel0.hide();
                        if (this._defaultContextItemMap.meshlevel1 !== undefined)
                            this._defaultContextItemMap.meshlevel1.hide();
                        if (this._defaultContextItemMap.meshlevel2 !== undefined)
                            this._defaultContextItemMap.meshlevel2.hide();
                        $(".contextmenu-separator-3").hide();
                    }
                };
                ContextMenu.prototype._isMenuItemEnabled = function () {
                    if (this._activeItemId !== null && !this._viewer._getNoteTextManager().checkPinInstance(this._activeItemId)) {
                        return true;
                    }
                    var axisOverlay = 1;
                    var selectionItems = this._viewer.selectionManager.getResults();
                    for (var _i = 0, selectionItems_1 = selectionItems; _i < selectionItems_1.length; _i++) {
                        var item = selectionItems_1[_i];
                        if (item.overlayIndex() !== axisOverlay) {
                            return true;
                        }
                    }
                    return false;
                };
                ContextMenu.prototype.setActiveItemId = function (activeItemId) {
                    this._activeItemId = activeItemId;
                    var menuItemEnabled = this._isMenuItemEnabled();
                    this._defaultContextItemMap.visibility.setText(this._isVisible(activeItemId) ? "Hide" : "Show");
                    this._defaultContextItemMap.visibility.setEnabled(menuItemEnabled);
                    this._defaultContextItemMap.isolate.setEnabled(menuItemEnabled);
                    this._defaultContextItemMap.zoom.setEnabled(menuItemEnabled);
                    this._defaultContextItemMap.transparent.setEnabled(menuItemEnabled);
                    var handleOperator = this._viewer.operatorManager.getOperator(Communicator.OperatorId.Handle);
                    if (handleOperator && handleOperator.isEnabled && menuItemEnabled) {
                        var enableHandles = (this.getContextItemIds(true, true, false).length > 0) && handleOperator.isEnabled();
                        this._defaultContextItemMap.handles.setEnabled(enableHandles);
                    }
                    else {
                        this._defaultContextItemMap.handles.setEnabled(false);
                    }
                    if (this._defaultContextItemMap.meshlevel0 !== undefined) {
                        this._defaultContextItemMap.meshlevel0.setEnabled(menuItemEnabled);
                    }
                    if (this._defaultContextItemMap.meshlevel1 !== undefined) {
                        this._defaultContextItemMap.meshlevel1.setEnabled(menuItemEnabled);
                    }
                    if (this._defaultContextItemMap.meshlevel2 !== undefined) {
                        this._defaultContextItemMap.meshlevel2.setEnabled(menuItemEnabled);
                    }
                };
                ContextMenu.prototype.showElements = function (position) {
                    this._viewer.setContextMenuStatus(true);
                    var canvasSize = this._viewer.view.getCanvasSize();
                    var menuElement = $(this._menuElement);
                    //make the context menu smaller if it is taller than the screen
                    if (menuElement.height() > canvasSize.y) {
                        menuElement.addClass("small");
                    }
                    var menuWidth = menuElement.outerWidth();
                    var menuHeight = menuElement.outerHeight();
                    var positionY = position.y;
                    var positionX = position.x;
                    // check for overflow y
                    if ((positionY + menuHeight) > canvasSize.y) {
                        positionY = canvasSize.y - menuHeight - 1;
                    }
                    //check for overflow x
                    if ((positionX + menuWidth) > canvasSize.x) {
                        positionX = canvasSize.x - menuWidth - 1;
                    }
                    $(this._menuElement).css({
                        left: positionX + "px",
                        top: positionY + "px",
                        display: "block",
                    });
                    $(this._menuElement).show();
                    $(this._contextLayer).show();
                };
                ContextMenu.prototype._onContextLayerClick = function (event) {
                    if (event.button === 0)
                        this.hide();
                };
                ContextMenu.prototype.hide = function () {
                    this._viewer.setContextMenuStatus(false);
                    $(this._menuElement).hide();
                    $(this._contextLayer).hide();
                    $(this._menuElement).removeClass("small");
                };
                ContextMenu.prototype._doMenuClick = function (event) {
                    if ($(event.target).hasClass("disabled"))
                        return;
                    var itemId = $(event.target).attr("id");
                    var contextMenuItem = this._defaultContextItemMap[itemId];
                    if (contextMenuItem !== undefined) {
                        contextMenuItem.action(this._activeItemId);
                    }
                    this.hide();
                };
                ContextMenu.prototype._createMenuElement = function (menuClass) {
                    var _this = this;
                    var menuElement = document.createElement("div");
                    menuElement.classList.add("ui-contextmenu");
                    menuElement.classList.add(menuClass);
                    menuElement.style.position = "absolute";
                    menuElement.style.zIndex = "6";
                    menuElement.style.display = "none";
                    menuElement.oncontextmenu = function () {
                        return false;
                    };
                    menuElement.ontouchmove = function (event) {
                        event.preventDefault();
                    };
                    $(menuElement).on("click", ".ui-contextmenu-item", function (event) {
                        _this._doMenuClick(event);
                    });
                    return menuElement;
                };
                ContextMenu.prototype._createContextLayer = function () {
                    var _this = this;
                    var contextLayer = document.createElement("div");
                    contextLayer.style.position = "relative";
                    contextLayer.style.width = "100%";
                    contextLayer.style.height = "100%";
                    contextLayer.style.backgroundColor = "transparent";
                    contextLayer.style.zIndex = "5";
                    contextLayer.style.display = "none";
                    contextLayer.oncontextmenu = function () {
                        return false;
                    };
                    contextLayer.ontouchmove = function (event) {
                        event.preventDefault();
                    };
                    $(contextLayer).bind("mousedown", function (event) {
                        _this._onContextLayerClick(event);
                    });
                    return contextLayer;
                };
                ContextMenu.prototype._initElements = function () {
                    this._createDefaultMenuItems();
                    var container = document.getElementById(this._containerId);
                    if (container !== null) {
                        container.appendChild(this._menuElement);
                        container.appendChild(this._contextLayer);
                    }
                };
                ContextMenu.prototype._isMenuItemExecutable = function () {
                    return this._activeItemId !== null || this._viewer.selectionManager.size() > 0;
                };
                ContextMenu.prototype._createDefaultMenuItems = function () {
                    var _this = this;
                    var model = this._viewer.model;
                    var operatorManager = this._viewer.operatorManager;
                    this._defaultContextItemMap = {};
                    var isolateFunc = function () {
                        if (_this._isMenuItemExecutable()) {
                            _this._isolateZoomHelper.isolateNodes(_this.getContextItemIds(true, true));
                        }
                    };
                    var zoomFunc = function () {
                        if (_this._isMenuItemExecutable()) {
                            _this._isolateZoomHelper.fitNodes(_this.getContextItemIds(true, true));
                        }
                    };
                    var visibilityFunc = function () {
                        if (_this._isMenuItemExecutable()) {
                            model.setNodesVisibility(_this.getContextItemIds(true, true), !_this._isVisible(_this._activeItemId));
                        }
                    };
                    var transparentFunc = function () {
                        if (_this._isMenuItemExecutable()) {
                            var contextItemIds = _this.getContextItemIds(true, true);
                            if (_this._transparencyIdHash[contextItemIds[0]] == undefined) {
                                for (var _i = 0, contextItemIds_1 = contextItemIds; _i < contextItemIds_1.length; _i++) {
                                    var id = contextItemIds_1[_i];
                                    _this._transparencyIdHash[id] = 1;
                                }
                                model.setNodesOpacity(contextItemIds, 0.5);
                            }
                            else {
                                for (var _a = 0, contextItemIds_2 = contextItemIds; _a < contextItemIds_2.length; _a++) {
                                    var id = contextItemIds_2[_a];
                                    delete _this._transparencyIdHash[id];
                                }
                                model.resetNodesOpacity(contextItemIds);
                            }
                        }
                    };
                    var handlesFunc = function () {
                        if (_this._isMenuItemExecutable()) {
                            var handleOperator = operatorManager.getOperator(Communicator.OperatorId.Handle);
                            var contextItemIds = _this.getContextItemIds(true, true, false);
                            if (contextItemIds.length > 0) {
                                handleOperator.addHandles(contextItemIds, (_this._modifiers === Communicator.KeyModifiers.Shift ? null : _this._position));
                            }
                        }
                    };
                    var resetFunc = function () {
                        model.reset(); // XXX: Returns a promise.
                        var handleOperator = operatorManager.getOperator(Communicator.OperatorId.Handle);
                        handleOperator.removeHandles(); // XXX: Returns a promise.
                    };
                    var meshLevelFunc = function (meshLevel) {
                        if (_this._isMenuItemExecutable()) {
                            model.setMeshLevel(_this.getContextItemIds(true, true), meshLevel);
                        }
                    };
                    var showAllFunc = function () {
                        _this._isolateZoomHelper.showAll();
                    };
                    this.appendItem("isolate", "Isolate", isolateFunc);
                    this.appendItem("zoom", "Zoom", zoomFunc);
                    this.appendItem("visibility", "Hide", visibilityFunc);
                    this.appendseparator();
                    this.appendItem("transparent", "Transparent", transparentFunc);
                    this.appendseparator();
                    this.appendItem("handles", "Show Handles", handlesFunc);
                    this.appendItem("reset", "Reset Model", resetFunc);
                    // Mesh level options should only be shown if the model is streaming from the server
                    if (this._viewer.getCreationParameters().hasOwnProperty("model")) {
                        this.appendseparator();
                        var _loop_1 = function (i) {
                            this_1.appendItem("meshlevel" + i, "Mesh Level " + i, function () {
                                meshLevelFunc(i);
                            });
                        };
                        var this_1 = this;
                        for (var i = 0; i < 3; ++i) {
                            _loop_1(i);
                        }
                    }
                    this.appendseparator();
                    this.appendItem("showall", "Show all", showAllFunc);
                };
                ContextMenu.prototype.getContextItemIds = function (includeSelected, includeClicked, includeRoot) {
                    if (includeRoot === void 0) { includeRoot = true; }
                    var selectionManager = this._viewer.selectionManager;
                    var model = this._viewer.model;
                    var rootId = model.getAbsoluteRootNode();
                    var itemIds = [];
                    // selected items
                    if (includeSelected) {
                        var selectedItems = selectionManager.getResults();
                        for (var _i = 0, selectedItems_1 = selectedItems; _i < selectedItems_1.length; _i++) {
                            var item = selectedItems_1[_i];
                            var id = item.getNodeId();
                            if (model.isNodeLoaded(id) && (includeRoot || (!includeRoot && id !== rootId))) {
                                itemIds.push(id);
                            }
                        }
                    }
                    if (this._activeItemId !== null) {
                        var selectionItem = Communicator.Selection.SelectionItem.create(this._activeItemId);
                        var containsParent = selectionManager.containsParent(selectionItem) !== null;
                        var containsItem = itemIds.indexOf(this._activeItemId) !== -1;
                        // also include items if they are clicked on but not selected (and not a child of a parent that is selected)
                        if (includeClicked
                            && (includeRoot
                                || (!includeRoot && this._activeItemId !== rootId)
                                    && (itemIds.length === 0 || (!containsItem && !containsParent)))) {
                            itemIds.push(this._activeItemId);
                        }
                    }
                    return itemIds;
                };
                ContextMenu.prototype.appendItem = function (itemId, label, action) {
                    var item = document.createElement("div");
                    item.classList.add("ui-contextmenu-item");
                    item.innerHTML = label;
                    item.id = itemId;
                    this._menuElement.appendChild(item);
                    var contextMenuItem = new ContextMenuItem(action, item);
                    this._defaultContextItemMap[itemId] = contextMenuItem;
                    return contextMenuItem;
                };
                ContextMenu.prototype.appendseparator = function () {
                    var item = document.createElement("div");
                    item.classList.add("contextmenu-separator-" + this._separatorCount++);
                    item.classList.add("ui-contextmenu-separator");
                    item.style.width = "100%";
                    item.style.height = "1px";
                    this._menuElement.appendChild(item);
                };
                ContextMenu.prototype._isVisible = function (itemId) {
                    if (itemId === null) {
                        var selectionItems = this._viewer.selectionManager.getResults();
                        if (selectionItems.length === 0) {
                            return false;
                        }
                        itemId = selectionItems[0].getNodeId();
                    }
                    var model = this._viewer.model;
                    return model.getNodeVisibility(itemId);
                };
                return ContextMenu;
            }());
            Context.ContextMenu = ContextMenu;
        })(Context = Ui.Context || (Ui.Context = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var AxisIndex;
        (function (AxisIndex) {
            AxisIndex[AxisIndex["X"] = 0] = "X";
            AxisIndex[AxisIndex["Y"] = 1] = "Y";
            AxisIndex[AxisIndex["Z"] = 2] = "Z";
            AxisIndex[AxisIndex["FACE"] = 3] = "FACE";
        })(AxisIndex = Ui.AxisIndex || (Ui.AxisIndex = {}));
        var CuttingPlaneInfo = /** @class */ (function () {
            function CuttingPlaneInfo() {
                this.plane = null;
                this.referenceGeometry = null;
                this.status = 0 /* Hidden */;
                this.updateReferenceGeometry = false;
            }
            return CuttingPlaneInfo;
        }());
        Ui.CuttingPlaneInfo = CuttingPlaneInfo;
        var CuttingPlaneController = /** @class */ (function () {
            function CuttingPlaneController(viewer) {
                var _this = this;
                this._cuttingSections = [];
                this._modelBounding = new Communicator.Box();
                this._planeInfo = new Map();
                this._showReferenceGeometry = true;
                this._useIndividualCuttingSections = true;
                this._boundingBoxUpdate = false;
                this._faceSelection = null;
                this._assemblyTreeReadyOccurred = false;
                this._pendingFuncs = {};
                this._viewer = viewer;
                this.resetCuttingPlanes();
                var refreshCuttingPlanes = function () {
                    return _this._updateBoundingBox().then(function () {
                        return _this.resetCuttingPlanes();
                    });
                };
                this._viewer.setCallbacks({
                    _assemblyTreeReady: function () {
                        var ps = [];
                        ps.push(_this._initSection());
                        _this._assemblyTreeReadyOccurred = true;
                        ps.push(_this._updateBoundingBox());
                        return Promise.all(ps);
                    },
                    visibilityChanged: function () {
                        _this._updateBoundingBox();
                    },
                    hwfParseComplete: function () {
                        _this._updateBoundingBox();
                    },
                    _firstModelLoaded: refreshCuttingPlanes,
                    _modelSwitched: refreshCuttingPlanes,
                    _resetAssemblyTreeBegin: function () {
                        return _this._clearCuttingSections();
                    },
                });
            }
            CuttingPlaneController.prototype._getCuttingStatus = function (axis, plane) {
                if (plane.normal.x >= 0 && plane.normal.y >= 0 && plane.normal.z >= 0 || axis === AxisIndex.FACE) {
                    return 1 /* Visible */;
                }
                else {
                    return 2 /* Inverted */;
                }
            };
            CuttingPlaneController.prototype.onSectionsChanged = function () {
                var _this = this;
                var planes = [];
                var referenceGeometry = [];
                var referenceGeometryShown = false;
                var useIndividualCuttingSections = false;
                var cuttingSectionX = this._cuttingSections[0];
                var cuttingSectionY = this._cuttingSections[1];
                var cuttingSectionZ = this._cuttingSections[2];
                var cuttingSectionFace = this._cuttingSections[3];
                if (cuttingSectionX.getCount() > 1) {
                    planes[0] = cuttingSectionX.getPlane(0);
                    planes[1] = cuttingSectionX.getPlane(1);
                    planes[2] = cuttingSectionX.getPlane(2);
                    planes[3] = cuttingSectionX.getPlane(3);
                    referenceGeometry[0] = cuttingSectionX.getReferenceGeometry(0);
                    referenceGeometry[1] = cuttingSectionX.getReferenceGeometry(1);
                    referenceGeometry[2] = cuttingSectionX.getReferenceGeometry(2);
                    referenceGeometry[3] = cuttingSectionX.getReferenceGeometry(3);
                }
                else {
                    useIndividualCuttingSections = true;
                    planes[0] = cuttingSectionX.getPlane(0);
                    planes[1] = cuttingSectionY.getPlane(0);
                    planes[2] = cuttingSectionZ.getPlane(0);
                    planes[3] = cuttingSectionFace.getPlane(0);
                    referenceGeometry[0] = cuttingSectionX.getReferenceGeometry(0);
                    referenceGeometry[1] = cuttingSectionY.getReferenceGeometry(0);
                    referenceGeometry[2] = cuttingSectionZ.getReferenceGeometry(0);
                    referenceGeometry[3] = cuttingSectionFace.getReferenceGeometry(0);
                }
                if (referenceGeometry[0] !== null ||
                    referenceGeometry[1] !== null ||
                    referenceGeometry[2] !== null ||
                    referenceGeometry[3] !== null) {
                    referenceGeometryShown = true;
                }
                this._resetCuttingData();
                this._showReferenceGeometry = referenceGeometryShown;
                this._useIndividualCuttingSections = useIndividualCuttingSections;
                for (var i = 0; i < planes.length; ++i) {
                    var plane = planes[i];
                    if (plane !== null) {
                        var axis = this._getPlaneAxis(plane);
                        var planeInfo = this._ensurePlaneInfo(axis);
                        if (planeInfo.status === 0 /* Hidden */) {
                            planeInfo.status = this._getCuttingStatus(axis, plane);
                            planeInfo.plane = plane;
                            planeInfo.referenceGeometry = referenceGeometry[i];
                        }
                    }
                }
                this._viewer.pauseRendering();
                return this._clearCuttingSections().then(function () {
                    return _this._restorePlanes().then(function () {
                        _this._viewer.resumeRendering();
                    });
                });
            };
            CuttingPlaneController.prototype._getPlaneAxis = function (plane) {
                var x = Math.abs(plane.normal.x);
                var y = Math.abs(plane.normal.y);
                var z = Math.abs(plane.normal.z);
                if (x === 1 && y === 0 && z === 0) {
                    return AxisIndex.X;
                }
                else if (x === 0 && y === 1 && z === 0) {
                    return AxisIndex.Y;
                }
                else if (x === 0 && y === 0 && z === 1) {
                    return AxisIndex.Z;
                }
                else {
                    return AxisIndex.FACE;
                }
            };
            CuttingPlaneController.prototype.getReferenceGeometryEnabled = function () {
                return this._showReferenceGeometry;
            };
            CuttingPlaneController.prototype.getIndividualCuttingSectionEnabled = function () {
                return this._useIndividualCuttingSections;
            };
            CuttingPlaneController.prototype.getPlaneInfo = function (axis) {
                return this._planeInfo.get(axis);
            };
            CuttingPlaneController.prototype._ensurePlaneInfo = function (axis) {
                var planeInfo = this._planeInfo.get(axis);
                if (planeInfo === undefined) {
                    planeInfo = new CuttingPlaneInfo();
                    this._planeInfo.set(axis, planeInfo);
                }
                return planeInfo;
            };
            CuttingPlaneController.prototype._setStatus = function (axis, status) {
                this._ensurePlaneInfo(axis).status = status;
            };
            CuttingPlaneController.prototype._updateBoundingBox = function () {
                var _this = this;
                if (!this._boundingBoxUpdate && this._assemblyTreeReadyOccurred) {
                    this._boundingBoxUpdate = true;
                    return this._viewer.model.getModelBounding(true, false).then(function (modelBounding) {
                        var minDiff = _this._modelBounding.min.equalsWithTolerance(modelBounding.min, .01);
                        var maxDiff = _this._modelBounding.max.equalsWithTolerance(modelBounding.max, .01);
                        var p;
                        if (!minDiff || !maxDiff) {
                            _this._modelBounding = modelBounding;
                            _this._ensurePlaneInfo(AxisIndex.X).updateReferenceGeometry = true;
                            _this._ensurePlaneInfo(AxisIndex.Y).updateReferenceGeometry = true;
                            _this._ensurePlaneInfo(AxisIndex.Z).updateReferenceGeometry = true;
                            _this._ensurePlaneInfo(AxisIndex.FACE).updateReferenceGeometry = true;
                            var activeStates_1 = [
                                _this._isActive(AxisIndex.X),
                                _this._isActive(AxisIndex.Y),
                                _this._isActive(AxisIndex.Z),
                                _this._isActive(AxisIndex.FACE),
                            ];
                            _this._storePlanes();
                            p = _this._clearCuttingSections().then(function () {
                                return _this._restorePlanes(activeStates_1);
                            });
                        }
                        else {
                            p = Promise.resolve();
                        }
                        return p.then(function () {
                            _this._boundingBoxUpdate = false;
                        });
                    });
                }
                else {
                    return Promise.resolve();
                }
            };
            CuttingPlaneController.prototype._resetAxis = function (axis) {
                this._planeInfo.delete(axis);
                if (axis === AxisIndex.FACE) {
                    this._faceSelection = null;
                }
            };
            CuttingPlaneController.prototype._resetCuttingData = function () {
                this._resetAxis(AxisIndex.X);
                this._resetAxis(AxisIndex.Y);
                this._resetAxis(AxisIndex.Z);
                this._resetAxis(AxisIndex.FACE);
                this._useIndividualCuttingSections = true;
                this._showReferenceGeometry = true;
                this._faceSelection = null;
            };
            CuttingPlaneController.prototype.resetCuttingPlanes = function () {
                this._resetCuttingData();
                return this._clearCuttingSections();
            };
            CuttingPlaneController.prototype._initSection = function () {
                var _this = this;
                return this._viewer.model.getModelBounding(true, false).then(function (modelBounding) {
                    _this._modelBounding = modelBounding.copy();
                    var cuttingManager = _this._viewer.cuttingManager;
                    console.assert(cuttingManager._isInitialized());
                    _this._cuttingSections[AxisIndex.X] = cuttingManager.getCuttingSection(AxisIndex.X);
                    _this._cuttingSections[AxisIndex.Y] = cuttingManager.getCuttingSection(AxisIndex.Y);
                    _this._cuttingSections[AxisIndex.Z] = cuttingManager.getCuttingSection(AxisIndex.Z);
                    _this._cuttingSections[AxisIndex.FACE] = cuttingManager.getCuttingSection(AxisIndex.FACE);
                    _this._triggerPendingFuncs();
                });
            };
            CuttingPlaneController.prototype._triggerPendingFuncs = function () {
                if (this._pendingFuncs.inverted) {
                    this._pendingFuncs.inverted();
                    delete this._pendingFuncs.inverted;
                }
                if (this._pendingFuncs.visibility) {
                    this._pendingFuncs.visibility();
                    delete this._pendingFuncs.visibility;
                }
            };
            CuttingPlaneController.prototype.toggle = function (axis) {
                var _this = this;
                var ps = [];
                switch (this._ensurePlaneInfo(axis).status) {
                    case 0 /* Hidden */:
                        if (axis === AxisIndex.FACE) {
                            var selectionItem = this._viewer.selectionManager.getLast();
                            if (selectionItem !== null && selectionItem.isFaceSelection()) {
                                this._faceSelection = selectionItem;
                                // clear any cutting planes in the face cutting section
                                ps.push(this._cuttingSections[axis].clear().then(function () {
                                    _this._setStatus(axis, 1 /* Visible */);
                                    return _this.setCuttingPlaneVisibility(true, axis);
                                }));
                            }
                        }
                        else {
                            this._setStatus(axis, 1 /* Visible */);
                            ps.push(this.setCuttingPlaneVisibility(true, axis));
                        }
                        break;
                    case 1 /* Visible */:
                        this._setStatus(axis, 2 /* Inverted */);
                        ps.push(this.setCuttingPlaneInverted(axis));
                        break;
                    case 2 /* Inverted */:
                        this._setStatus(axis, 0 /* Hidden */);
                        ps.push(this.setCuttingPlaneVisibility(false, axis));
                        break;
                }
                return Promise.all(ps);
            };
            CuttingPlaneController.prototype.getCount = function () {
                var count = 0;
                for (var _i = 0, _a = this._cuttingSections; _i < _a.length; _i++) {
                    var section = _a[_i];
                    count += section.getCount();
                }
                return count;
            };
            CuttingPlaneController.prototype.setCuttingPlaneVisibility = function (visibility, axis) {
                var _this = this;
                var index = this._getCuttingSectionIndex(axis);
                var section = this._cuttingSections[index];
                if (section === undefined) {
                    this._pendingFuncs.visibility = function () {
                        _this.setCuttingPlaneVisibility(visibility, axis);
                    };
                    return Promise.resolve();
                }
                this._viewer.delayCapping();
                var p;
                if (visibility) {
                    var planeInfo = this._ensurePlaneInfo(axis);
                    if (planeInfo.plane === null) {
                        planeInfo.plane = this._generateCuttingPlane(axis);
                        planeInfo.referenceGeometry = this._generateReferenceGeometry(axis);
                    }
                    p = this._setSection(axis);
                }
                else {
                    p = this.refreshPlaneGeometry();
                }
                var count = this.getCount();
                var active = this._isActive(axis);
                return p.then(function () {
                    if (count > 0 && !active) {
                        return _this._activatePlanes();
                    }
                    else if (active && count === 0) {
                        return _this._deactivateAxis(axis);
                    }
                    return Promise.resolve();
                });
            };
            CuttingPlaneController.prototype.setCuttingPlaneInverted = function (axis) {
                var _this = this;
                var sectionIndex = this._getCuttingSectionIndex(axis);
                var section = this._cuttingSections[sectionIndex];
                if (section === undefined) {
                    this._pendingFuncs.inverted = function () {
                        _this.setCuttingPlaneInverted(axis);
                    };
                    return Promise.resolve();
                }
                this._viewer.delayCapping();
                var index = this._getPlaneIndex(axis);
                var plane = section.getPlane(index);
                if (plane) {
                    plane.normal.negate();
                    plane.d *= -1;
                    return section.updatePlane(index, plane);
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype.toggleReferenceGeometry = function () {
                if (this.getCount() > 0) {
                    this._showReferenceGeometry = !this._showReferenceGeometry;
                    return this.refreshPlaneGeometry();
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype.refreshPlaneGeometry = function () {
                var _this = this;
                this._storePlanes();
                return this._clearCuttingSections().then(function () {
                    return _this._restorePlanes();
                });
            };
            CuttingPlaneController.prototype.toggleCuttingMode = function () {
                var _this = this;
                if (this.getCount() > 1) {
                    this._storePlanes();
                    var p = this._clearCuttingSections();
                    this._useIndividualCuttingSections = !this._useIndividualCuttingSections;
                    return p.then(function () {
                        return _this._restorePlanes();
                    });
                }
                return Promise.resolve();
            };
            /* Helper functions */
            CuttingPlaneController.prototype._isActive = function (axis) {
                return this._cuttingSections[this._getCuttingSectionIndex(axis)].isActive();
            };
            CuttingPlaneController.prototype._deactivateAxis = function (axis) {
                return this._cuttingSections[this._getCuttingSectionIndex(axis)].deactivate();
            };
            CuttingPlaneController.prototype._getCuttingSectionIndex = function (axis) {
                return this._useIndividualCuttingSections ? axis : 0;
            };
            CuttingPlaneController.prototype._clearCuttingSection = function (axis) {
                var section = this._cuttingSections[axis];
                if (section !== undefined) {
                    return section.clear();
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype._clearCuttingSections = function () {
                var ps = [];
                ps.push(this._clearCuttingSection(AxisIndex.X));
                ps.push(this._clearCuttingSection(AxisIndex.Y));
                ps.push(this._clearCuttingSection(AxisIndex.Z));
                ps.push(this._clearCuttingSection(AxisIndex.FACE));
                return Promise.all(ps);
            };
            CuttingPlaneController.prototype._activatePlane = function (axis) {
                var section = this._cuttingSections[axis];
                if (section.getCount()) {
                    var p = section.activate();
                    if (p === null) {
                        return Promise.resolve();
                    }
                    return p;
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype._activatePlanes = function (activeStates) {
                var ps = [];
                if (!activeStates || activeStates[0])
                    ps.push(this._activatePlane(AxisIndex.X));
                if (!activeStates || activeStates[1])
                    ps.push(this._activatePlane(AxisIndex.Y));
                if (!activeStates || activeStates[2])
                    ps.push(this._activatePlane(AxisIndex.Z));
                if (!activeStates || activeStates[3])
                    ps.push(this._activatePlane(AxisIndex.FACE));
                return Promise.all(ps);
            };
            CuttingPlaneController.prototype._getPlaneIndex = function (axis) {
                if (this._useIndividualCuttingSections) {
                    var index = this._getCuttingSectionIndex(axis);
                    var section = this._cuttingSections[index];
                    if (section.getPlane(0)) {
                        return 0;
                    }
                }
                else {
                    var section = this._cuttingSections[0];
                    var planeCount = section.getCount();
                    for (var i = 0; i < planeCount; i++) {
                        var plane = section.getPlane(i);
                        var normal = void 0;
                        if (this._faceSelection) {
                            normal = this._faceSelection.getFaceEntity().getNormal();
                        }
                        if (plane) {
                            if ((plane.normal.x && axis === AxisIndex.X) ||
                                (plane.normal.y && axis === AxisIndex.Y) ||
                                (plane.normal.z && axis === AxisIndex.Z) ||
                                (axis === AxisIndex.FACE && normal && plane.normal.equals(normal))) {
                                return i;
                            }
                        }
                    }
                }
                return -1;
            };
            CuttingPlaneController.prototype._setSection = function (axis) {
                var planeInfo = this._planeInfo.get(axis);
                if (planeInfo !== undefined && planeInfo.plane !== null) {
                    var cuttingSection = this._cuttingSections[this._getCuttingSectionIndex(axis)];
                    var referenceGeometry = this._showReferenceGeometry ? planeInfo.referenceGeometry : null;
                    // XXX: Refactor code to not cast Promise<boolean> to Promise<void>.
                    return cuttingSection.addPlane(planeInfo.plane, referenceGeometry);
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype._restorePlane = function (axis) {
                var planeInfo = this._planeInfo.get(axis);
                if (planeInfo !== undefined && planeInfo.plane !== null && planeInfo.status !== 0 /* Hidden */) {
                    if (planeInfo.referenceGeometry === null || planeInfo.updateReferenceGeometry) {
                        planeInfo.referenceGeometry = this._generateReferenceGeometry(axis);
                    }
                    return this._setSection(axis);
                }
                return Promise.resolve();
            };
            CuttingPlaneController.prototype._restorePlanes = function (activeStates) {
                this._restorePlane(AxisIndex.X);
                this._restorePlane(AxisIndex.Y);
                this._restorePlane(AxisIndex.Z);
                this._restorePlane(AxisIndex.FACE);
                return this._activatePlanes(activeStates);
            };
            CuttingPlaneController.prototype._storePlane = function (axis) {
                var section = this._cuttingSections[this._getCuttingSectionIndex(axis)];
                var planeInfo = this._ensurePlaneInfo(axis);
                planeInfo.plane = null;
                planeInfo.referenceGeometry = null;
                if (section.getCount() > 0 && planeInfo.status !== 0 /* Hidden */) {
                    var planeIndex = this._getPlaneIndex(axis);
                    var plane = section.getPlane(planeIndex);
                    var referenceGeometry = section.getReferenceGeometry(planeIndex);
                    planeInfo.plane = plane;
                    planeInfo.referenceGeometry = referenceGeometry;
                }
            };
            CuttingPlaneController.prototype._storePlanes = function () {
                this._storePlane(AxisIndex.X);
                this._storePlane(AxisIndex.Y);
                this._storePlane(AxisIndex.Z);
                this._storePlane(AxisIndex.FACE);
            };
            CuttingPlaneController.prototype._generateReferenceGeometry = function (axisIndex) {
                var cuttingManager = this._viewer.cuttingManager;
                var referenceGeometry = [];
                var axis;
                if (axisIndex === AxisIndex.FACE) {
                    if (this._faceSelection) {
                        var normal = this._faceSelection.getFaceEntity().getNormal();
                        var position = this._faceSelection.getPosition();
                        referenceGeometry = cuttingManager.createReferenceGeometryFromFaceNormal(normal, position, this._modelBounding);
                    }
                }
                else {
                    switch (axisIndex) {
                        case AxisIndex.X:
                            axis = Communicator.Axis.X;
                            break;
                        case AxisIndex.Y:
                            axis = Communicator.Axis.Y;
                            break;
                        case AxisIndex.Z:
                            axis = Communicator.Axis.Z;
                            break;
                    }
                    if (axis !== undefined) {
                        referenceGeometry = cuttingManager.createReferenceGeometryFromAxis(axis, this._modelBounding);
                    }
                }
                return referenceGeometry;
            };
            CuttingPlaneController.prototype._generateCuttingPlane = function (axis) {
                var plane = new Communicator.Plane();
                switch (axis) {
                    case AxisIndex.X:
                        plane.normal.set(1, 0, 0);
                        plane.d = -this._modelBounding.max.x;
                        break;
                    case AxisIndex.Y:
                        plane.normal.set(0, 1, 0);
                        plane.d = -this._modelBounding.max.y;
                        break;
                    case AxisIndex.Z:
                        plane.normal.set(0, 0, 1);
                        plane.d = -this._modelBounding.max.z;
                        break;
                    case AxisIndex.FACE:
                        if (this._faceSelection) {
                            this._faceSelection = this._faceSelection;
                            var normal = this._faceSelection.getFaceEntity().getNormal();
                            var position = this._faceSelection.getPosition();
                            plane.setFromPointAndNormal(position, normal);
                        }
                        else {
                            return null;
                        }
                }
                return plane;
            };
            return CuttingPlaneController;
        }());
        Ui.CuttingPlaneController = CuttingPlaneController;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var IsolateZoomHelper = /** @class */ (function () {
        function IsolateZoomHelper(viewer) {
            var _this = this;
            this._camera = null;
            this._deselectOnIsolate = true;
            this._deselectOnZoom = true;
            this._isolateStatus = false;
            this._viewer = viewer;
            this._noteTextManager = this._viewer._getNoteTextManager();
            this._viewer.setCallbacks({
                modelSwitched: function () {
                    _this._camera = null;
                }
            });
        }
        IsolateZoomHelper.prototype._setCamera = function (camera) {
            if (this._camera === null) {
                this._camera = camera;
            }
        };
        IsolateZoomHelper.prototype.setDeselectOnIsolate = function (deselect) {
            this._deselectOnIsolate = deselect;
        };
        IsolateZoomHelper.prototype.getIsolateStatus = function () {
            return this._isolateStatus;
        };
        IsolateZoomHelper.prototype.isolateNodes = function (nodeIds) {
            var view = this._viewer.view;
            this._setCamera(view.getCamera());
            var p = view.isolateNodes(nodeIds);
            if (this._deselectOnIsolate) {
                this._viewer.selectionManager.clear();
            }
            this._isolateStatus = true;
            return p;
        };
        IsolateZoomHelper.prototype.fitNodes = function (nodeIds) {
            var view = this._viewer.view;
            this._setCamera(view.getCamera());
            var p = view.fitNodes(nodeIds);
            if (this._deselectOnZoom) {
                this._viewer.selectionManager.clear();
            }
            return p;
        };
        IsolateZoomHelper.prototype.showAll = function () {
            var model = this._viewer.model;
            if (model.isDrawing()) {
                var sheetId = this._viewer.getActiveSheetId();
                if (sheetId !== null) {
                    return this.isolateNodes([sheetId]);
                }
                return Promise.resolve();
            }
            else {
                var ps = [];
                ps.push(model.resetNodesVisibility());
                if (this._camera !== null) {
                    this._viewer.view.setCamera(this._camera, Communicator.DefaultTransitionDuration);
                    this._camera = null;
                }
                this._isolateStatus = false;
                ps.push(this._updatePinVisibility());
                return Promise.all(ps);
            }
        };
        IsolateZoomHelper.prototype._updatePinVisibility = function () {
            this._noteTextManager.setIsolateActive(this._isolateStatus);
            return this._noteTextManager.updatePinVisibility();
        };
        return IsolateZoomHelper;
    }());
    Communicator.IsolateZoomHelper = IsolateZoomHelper;
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Desktop;
        (function (Desktop) {
            var ModelBrowserContextMenu = /** @class */ (function (_super) {
                __extends(ModelBrowserContextMenu, _super);
                function ModelBrowserContextMenu(containerId, viewer, modelTree, isolateZoomHelper) {
                    var _this = _super.call(this, "modelbrowser", containerId, viewer, isolateZoomHelper) || this;
                    _this._modelTree = modelTree;
                    _this._initEvents();
                    return _this;
                }
                ModelBrowserContextMenu.prototype._initEvents = function () {
                    var _this = this;
                    this._modelTree.registerCallback("context", function (id, position) {
                        _this._onTreeContext(id, position);
                    });
                    if (this._viewer.getStreamingMode() === Communicator.StreamingMode.OnDemand) {
                        var requestFunc = function () {
                            _this._viewer.model.requestNodes(_this.getContextItemIds(false, true));
                        };
                        this.appendseparator();
                        this.appendItem("request", "Request", requestFunc);
                    }
                };
                ModelBrowserContextMenu.prototype._onTreeContext = function (id, position) {
                    var components = id.split(Ui.ModelTree.separator);
                    switch (components[0]) {
                        case "part":
                            this.setActiveItemId(parseInt(components[1], 10));
                            break;
                        default:
                            return;
                    }
                    ;
                    this._position = null;
                    this.showElements(position);
                };
                ModelBrowserContextMenu.prototype._onContextLayerClick = function (event) {
                    event;
                    this.hide();
                };
                return ModelBrowserContextMenu;
            }(Ui.Context.ContextMenu));
            Desktop.ModelBrowserContextMenu = ModelBrowserContextMenu;
        })(Desktop = Ui.Desktop || (Ui.Desktop = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var ModelTree = /** @class */ (function (_super) {
            __extends(ModelTree, _super);
            function ModelTree(elementId, viewer, modelTreeScroll) {
                if (modelTreeScroll === void 0) { modelTreeScroll = null; }
                var _this = _super.call(this, elementId, viewer, modelTreeScroll) || this;
                _this._lastModelRoot = null;
                _this._startedWithoutModelStructure = false;
                _this._partSelectionEnabled = true;
                _this._currentSheetId = null;
                _this._measurementFolderId = "measurementitems";
                _this._updateVisibilityStateTimerId = null;
                _this._initEvents();
                return _this;
            }
            ModelTree.prototype.freezeExpansion = function (freeze) {
                this._tree.freezeExpansion(freeze);
            };
            ModelTree.prototype.modelStructurePresent = function () {
                var model = this._viewer.model;
                return model.getNodeName(model.getAbsoluteRootNode()) !== "No product structure";
            };
            ModelTree.prototype.enablePartSelection = function (enable) {
                this._partSelectionEnabled = enable;
            };
            ModelTree.prototype._initEvents = function () {
                var _this = this;
                var reset = function () {
                    _this._reset();
                    return Promise.resolve();
                };
                this._viewer.setCallbacks({
                    _assemblyTreeReady: function () {
                        _this._onNewModel();
                        return Promise.resolve();
                    },
                    _firstModelLoaded: reset,
                    hwfParseComplete: reset,
                    modelSwitched: reset,
                    selectionArray: function (events) {
                        _this._onPartSelection(events);
                    },
                    visibilityChanged: function () {
                        _this._tree.getVisibilityControl().updateModelTreeVisibilityState();
                    },
                    viewCreated: function (view) {
                        _this._onNewView(view);
                    },
                    viewLoaded: function (view) {
                        _this._onNewView(view);
                    },
                    subtreeLoaded: function (nodeIdArray) {
                        _this._onSubtreeLoaded(nodeIdArray);
                    },
                    subtreeDeleted: function (nodeIdArray) {
                        _this._onSubtreeDeleted(nodeIdArray);
                    },
                    modelSwitchStart: function () {
                        _this._tree.clear();
                    },
                    measurementCreated: function (measurement) {
                        _this._onNewMeasurement(measurement);
                    },
                    measurementLoaded: function (measurement) {
                        _this._onNewMeasurement(measurement);
                    },
                    measurementDeleted: function (measurement) {
                        _this._onDeleteMeasurement(measurement);
                    },
                    measurementShown: function () {
                        _this._tree.updateMeasurementVisibilityIcons();
                    },
                    measurementHidden: function () {
                        _this._tree.updateMeasurementVisibilityIcons();
                    },
                    sheetActivated: function (id) {
                        if (id !== _this._currentSheetId) {
                            _this._currentSheetId = id;
                            _this._refreshModelTree(id);
                        }
                    },
                    configurationActivated: function (id) {
                        _this._refreshModelTree(id);
                    }
                });
                this._tree.registerCallback("loadChildren", function (id) {
                    _this._loadNodeChildren(id);
                });
                this._tree.registerCallback("selectItem", function (id, selectionMode) {
                    _this._onTreeSelectItem(id, selectionMode);
                });
            };
            ModelTree.prototype._refreshModelTree = function (id) {
                this._tree.clear();
                var model = this._viewer.model;
                var rootId = model.getAbsoluteRootNode();
                var name = model.getNodeName(rootId);
                // add the top level root, and skip to 'id' for the first child
                this._tree.appendTopLevelElement(name, this._partId(rootId.toString()), "modelroot", model.getNodeChildren(rootId).length > 0, false, true);
                this._tree.addChild(model.getNodeName(id), this._partId("" + id), this._partId(rootId.toString()), "part", true, Ui.Desktop.Tree.Model);
                this._tree.expandInitialNodes(this._partId(rootId.toString()));
                this._refreshMarkupViews();
            };
            ModelTree.prototype._reset = function () {
                this._tree.clear();
                this._currentSheetId = null;
                this._onNewModel();
            };
            ModelTree.prototype._onNewModel = function () {
                var model = this._viewer.model;
                var rootId = model.getAbsoluteRootNode();
                var name = model.getNodeName(rootId);
                this.showTab();
                /* TODO: Clean this up: erwan currently makes a placeholder node for the root with
                * "No model structure present" text when structure is absent. In this case we do not want to operate under
                * the assumption we have loaded a model root (for further subtree loading)
                */
                this._startedWithoutModelStructure = !this.modelStructurePresent();
                this._lastModelRoot = this._tree.appendTopLevelElement(name, this._partId(rootId.toString()), "modelroot", model.getNodeChildren(rootId).length > 0);
                if (!this._viewer.model.isDrawing()) {
                    this._tree.expandInitialNodes("part_" + rootId);
                }
            };
            ModelTree.prototype._createMarkupViewFolderIfNecessary = function () {
                var $markupViewFolder = $("#markupviews");
                if ($markupViewFolder.length === 0)
                    this._tree.appendTopLevelElement("Markup Views", "markupviews", "viewfolder", false);
            };
            ModelTree.prototype._createMeasurementFolderIfNecessary = function () {
                var $measurementsFolder = $("#" + this._measurementFolderId);
                if ($measurementsFolder.length === 0)
                    this._tree.appendTopLevelElement("Measurements", this._measurementFolderId, "measurement", false);
            };
            ModelTree.prototype._parentChildrenLoaded = function (parent) {
                var parentIdString = this._partId(parent.toString());
                return this._tree.childrenAreLoaded(parentIdString);
            };
            ModelTree.prototype._onSubtreeLoaded = function (nodeIds) {
                var model = this._viewer.model;
                for (var _i = 0, nodeIds_1 = nodeIds; _i < nodeIds_1.length; _i++) {
                    var nodeId = nodeIds_1[_i];
                    if (model.getOutOfHierarchy(nodeId)) {
                        continue;
                    }
                    var parent_1 = model.getNodeParent(nodeId);
                    if (parent_1 === null) {
                        console.assert(this._lastModelRoot !== null);
                        this._lastModelRoot = this._tree.insertNodeAfter(model.getNodeName(nodeId), this._partId(nodeId.toString()), "modelroot", this._lastModelRoot, true);
                    }
                    else {
                        var initialParent = parent_1;
                        do {
                            if (this._parentChildrenLoaded(parent_1)) {
                                if (initialParent === parent_1) {
                                    this._tree.addChild(model.getNodeName(nodeId), this._partId(nodeId.toString()), this._partId(parent_1.toString()), "assembly", true, Ui.Desktop.Tree.Model);
                                }
                                this._tree.preloadChildrenIfNecessary(this._partId(nodeId.toString()));
                                break;
                            }
                            nodeId = parent_1;
                            parent_1 = model.getNodeParent(nodeId);
                        } while (parent_1 !== null);
                    }
                }
                if (this._startedWithoutModelStructure) {
                    var treeRoot = this._tree.getRoot();
                    if (treeRoot.firstChild !== null) {
                        treeRoot.removeChild(treeRoot.firstChild);
                    }
                    var visibilityRoot = this._tree.getPartVisibilityRoot();
                    if (visibilityRoot.firstChild !== null) {
                        visibilityRoot.removeChild(visibilityRoot.firstChild);
                    }
                }
            };
            ModelTree.prototype._onSubtreeDeleted = function (nodeIds) {
                for (var _i = 0, nodeIds_2 = nodeIds; _i < nodeIds_2.length; _i++) {
                    var nodeId = nodeIds_2[_i];
                    this._tree.deleteNode(this._partId(nodeId.toString()));
                }
            };
            ModelTree.prototype._buildTreePathForNode = function (nodeId) {
                // build up the path path from the root to the selected item in the tree
                var model = this._viewer.model;
                var parents = [];
                var isDrawing = this._viewer.model.isDrawing();
                var parentId = model.getNodeParent(nodeId);
                while (parentId !== null) {
                    // if it's a drawing, prevent loading other items in the tree on selection
                    if (isDrawing && this._currentSheetId !== null && (parentId === this._currentSheetId || nodeId === this._currentSheetId)) {
                        break;
                    }
                    parents.push(parentId);
                    parentId = model.getNodeParent(parentId);
                }
                parents.reverse();
                return parents;
            };
            ModelTree.prototype._expandCorrectContainerForNodeid = function (nodeId) {
                // get all children of parent and figure out which container this node is in
                var model = this._viewer.model;
                var parentId = model.getNodeParent(nodeId);
                if (parentId === null) {
                    return;
                }
                var nodes = model.getNodeChildren(parentId);
                var index = nodes.indexOf(nodeId);
                if (index > 0) {
                    var containerId = Math.floor(index / this._maxNodeChildrenSize);
                    this._tree.expandChildren(this._containerId(parentId.toString(), containerId));
                }
            };
            ModelTree.prototype._onPartSelection = function (events) {
                if (!this._partSelectionEnabled) {
                    return;
                }
                var model = this._viewer.model;
                for (var _i = 0, events_1 = events; _i < events_1.length; _i++) {
                    var event_1 = events_1[_i];
                    var id = event_1.getSelection().getNodeId();
                    if (id === null) {
                        this._tree.selectItem(null, false);
                    }
                    else if (model.isNodeLoaded(id)) {
                        var parents = this._buildTreePathForNode(id);
                        for (var _a = 0, parents_1 = parents; _a < parents_1.length; _a++) {
                            var parent_2 = parents_1[_a];
                            var $node = $("#" + this._partId(parent_2.toString()));
                            // If this node is in a container, we must first expand that container.
                            if ($node.length === 0) {
                                this._expandCorrectContainerForNodeid(parent_2);
                                $node = $("#" + this._partId(parent_2.toString()));
                            }
                            var nodeIdAttr = $node.attr("id"); // TODO: Upgrade jQuery type definitions to have proper return types.
                            if (nodeIdAttr !== undefined) {
                                this._tree.expandChildren(nodeIdAttr);
                            }
                        }
                        this._tree.selectItem(this._partId(id.toString()), false);
                    }
                }
                if (events.length === 0) {
                    this._tree.updateSelection();
                }
            };
            ModelTree.prototype._createContainerNodes = function (partId, childNodes) {
                var containerStartIndex = 0;
                var containerEndIndex = this._maxNodeChildrenSize - 1;
                var containerCount = 0;
                while (true) {
                    var rangeEnd = Math.min(containerEndIndex, childNodes.length);
                    var name_1 = "Child Nodes " + (containerStartIndex + 1) + " - " + (rangeEnd + 1);
                    this._tree.addChild(name_1, this._containerId(partId, containerCount), this._partId(partId), "container", true, Ui.Desktop.Tree.Model);
                    containerStartIndex += this._maxNodeChildrenSize;
                    ++containerCount;
                    if (containerEndIndex >= childNodes.length)
                        return;
                    else
                        containerEndIndex += this._maxNodeChildrenSize;
                }
            };
            ModelTree.prototype._loadAssemblyNodeChildren = function (id) {
                var model = this._viewer.model;
                var children = model.getNodeChildren(parseInt(id, 10));
                // If this node has a large amount of children, we need to create grouping nodes for it.
                if (children.length > this._maxNodeChildrenSize) {
                    this._createContainerNodes(id, children);
                }
                else {
                    var partId = this._partId(id);
                    this._processNodeChildren(children, partId);
                }
            };
            ModelTree.prototype._loadContainerChildren = function (containerId) {
                var model = this._viewer.model;
                var idParts = this._splitIdStr(containerId);
                var containerData = this._splitContainerId(idParts[1]);
                // First get all the children for the parent of this container node.
                var children = model.getNodeChildren(parseInt(containerData[0], 10));
                // Next we need to slice the array to contain only the children for this particular container.
                var startIndex = this._maxNodeChildrenSize * parseInt(containerData[1], 10);
                var childrenSlice = children.slice(startIndex, startIndex + this._maxNodeChildrenSize);
                this._processNodeChildren(childrenSlice, containerId);
            };
            ModelTree.prototype._processNodeChildren = function (children, parentId) {
                var _this = this;
                var model = this._viewer.model;
                var pmiFolder = null;
                for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                    var child = children_1[_i];
                    var name_2 = model.getNodeName(child);
                    var itemId = child.toString();
                    var currParentId = parentId;
                    var itemType = "assembly";
                    switch (model.getNodeType(child)) {
                        case Communicator.NodeType.Body:
                        case Communicator.NodeType.BodyInstance:
                            itemType = "body";
                            break;
                        case Communicator.NodeType.Pmi:
                            // put pmi's under pmi folder
                            if (pmiFolder === null) {
                                pmiFolder = this._tree.addChild("PMI", this._pmiPartId(itemId), parentId, "modelroot", true, Ui.Desktop.Tree.Model);
                            }
                            if (pmiFolder !== null) {
                                currParentId = pmiFolder.id;
                                itemType = "assembly";
                            }
                            break;
                    }
                    this._tree.addChild(name_2, this._partId(itemId), currParentId, itemType, model.getNodeChildren(child).length > 0, Ui.Desktop.Tree.Model);
                }
                if (children.length > 0) {
                    if (this._updateVisibilityStateTimerId !== null) {
                        clearTimeout(this._updateVisibilityStateTimerId);
                        this._updateVisibilityStateTimerId = null;
                    }
                    this._updateVisibilityStateTimerId = setTimeout(function () {
                        _this._updateVisibilityStateTimerId = null;
                        _this._tree.getVisibilityControl().updateModelTreeVisibilityState();
                    }, 50);
                }
            };
            ModelTree.prototype._loadNodeChildren = function (idStr) {
                var idParts = this._splitIdStr(idStr);
                var kind = idParts[idParts[0] === "" ? 1 : 0];
                switch (kind) {
                    case "part":
                        this._loadAssemblyNodeChildren(idParts[1]);
                        break;
                    case "container":
                        this._loadContainerChildren(idStr);
                        break;
                    case "markupviews":
                    case "measurementitems":
                    case "pmipart":
                        // do nothing
                        break;
                    default:
                        console.assert(false);
                        break;
                }
            };
            ModelTree.prototype._onTreeSelectItem = function (idStr, selectionMode) {
                if (selectionMode === void 0) { selectionMode = Communicator.SelectionMode.Set; }
                // toggle recursive selection base on what is clicked
                var thisElement = document.getElementById(idStr);
                if (thisElement === null) {
                    return;
                }
                if (thisElement.tagName === "LI" && idStr !== "markupviews") {
                    thisElement.classList.add("selected");
                }
                else {
                    var viewTree = document.getElementById("markupviews");
                    if (viewTree !== null) {
                        viewTree.classList.remove("selected");
                    }
                }
                // don't allow selection on pmi folder
                if (idStr.lastIndexOf("pmi", 0) === 0 && thisElement.classList.contains("ui-modeltree-item")) {
                    thisElement.classList.remove("selected");
                }
                var idParts = this._splitIdStr(idStr);
                switch (idParts[0]) {
                    case "part":
                        this._viewer.selectPart(parseInt(idParts[1], 10), selectionMode);
                        break;
                    case "markupview":
                        this._viewer.markupManager.activateMarkupViewWithPromise(idParts[1]);
                        break;
                    case "container":
                        this._onContainerClick(idParts[1]);
                        break;
                }
            };
            ModelTree.prototype._onContainerClick = function (containerId) {
                // behavior here to TBD, for now do nothing.
                containerId;
            };
            ModelTree.prototype._onNewView = function (view) {
                this._createMarkupViewFolderIfNecessary();
                this._addMarkupView(view);
            };
            ModelTree.prototype._refreshMarkupViews = function () {
                this._createMarkupViewFolderIfNecessary();
                var markupManager = this._viewer.markupManager;
                var viewKeys = markupManager.getMarkupViewKeys();
                var view;
                for (var _i = 0, viewKeys_1 = viewKeys; _i < viewKeys_1.length; _i++) {
                    var viewKey = viewKeys_1[_i];
                    view = markupManager.getMarkupView(viewKey);
                    if (view !== null) {
                        this._addMarkupView(view);
                    }
                }
            };
            ModelTree.prototype._addMarkupView = function (view) {
                var viewId = this._viewId(view.getUniqueId());
                this._tree.addChild(view.getName(), viewId, "markupviews", "view", false, Ui.Desktop.Tree.Model);
            };
            ModelTree.prototype._onNewMeasurement = function (measurement) {
                this._createMeasurementFolderIfNecessary();
                var measurementId = this._measurementId(measurement._getId());
                this._tree.addChild(measurement.getName(), measurementId, this._measurementFolderId, "measurement", false, Ui.Desktop.Tree.Model);
                this._updateMeasurementsFolderVisibility();
                this._tree.updateMeasurementVisibilityIcons();
            };
            ModelTree.prototype._onDeleteMeasurement = function (measurement) {
                var measurementId = this._measurementId(measurement._getId());
                this._tree.deleteNode(measurementId);
                this._tree.deleteNode("visibility_" + measurementId);
                this._updateMeasurementsFolderVisibility();
            };
            ModelTree.prototype._updateMeasurementsFolderVisibility = function () {
                var measurements = this._viewer.measureManager.getAllMeasurements();
                var measurementItems = document.getElementById(this._measurementFolderId);
                if (measurementItems !== null) {
                    measurementItems.style["display"] = measurements.length ? "inherit" : "none";
                }
                var measurementVisibilityItems = document.getElementById("visibility_" + this._measurementFolderId);
                if (measurementVisibilityItems !== null) {
                    measurementVisibilityItems.style["display"] = measurements.length ? "inherit" : "none";
                }
            };
            ModelTree.prototype._measurementId = function (id) {
                return "measurement" + ModelTree.separator + id;
            };
            ModelTree.prototype._partId = function (id) {
                return "part" + ModelTree.separator + id;
            };
            ModelTree.prototype._pmiPartId = function (id) {
                return "pmipart" + ModelTree.separator + id;
            };
            ModelTree.prototype._viewId = function (id) {
                return "markupview" + ModelTree.separator + id;
            };
            ModelTree.prototype._containerId = function (partId, containerId) {
                return "container" + ModelTree.separator + partId + "-" + containerId;
            };
            ModelTree.prototype._splitContainerId = function (idStr) {
                return this._splitParts(idStr, '-');
            };
            ModelTree.prototype.updateSelection = function () {
                this._tree.updateSelection();
            };
            return ModelTree;
        }(Ui.ViewTree));
        Ui.ModelTree = ModelTree;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Desktop;
        (function (Desktop) {
            var PropertyWindow = /** @class */ (function () {
                function PropertyWindow(viewer, isolateZoomHelper) {
                    var _this = this;
                    this._assemblyTreeReadyOccurred = false;
                    this._volumeSelectionActive = false;
                    isolateZoomHelper;
                    this._viewer = viewer;
                    this._propertyWindow = $("#propertyContainer");
                    var update = function () {
                        _this._update();
                        return Promise.resolve();
                    };
                    this._viewer.setCallbacks({
                        _assemblyTreeReady: function () {
                            _this._onModelStructureReady();
                            return Promise.resolve();
                        },
                        _firstModelLoaded: update,
                        modelSwitched: update,
                        selectionArray: function (events) {
                            for (var _i = 0, events_2 = events; _i < events_2.length; _i++) {
                                var event_2 = events_2[_i];
                                _this._onPartSelection(event_2);
                            }
                        },
                        volumeSelectionBatchBegin: function () {
                            _this._volumeSelectionActive = true;
                        },
                        volumeSelectionBatchEnd: function () {
                            _this._volumeSelectionActive = false;
                        }
                    });
                }
                PropertyWindow.prototype._update = function (text) {
                    if (text === void 0) { text = "&lt;no properties to display&gt;"; }
                    this._propertyWindow.html(text);
                };
                PropertyWindow.prototype._onModelStructureReady = function () {
                    this._assemblyTreeReadyOccurred = true;
                    this._update();
                };
                PropertyWindow.prototype._createRow = function (key, property, classStr) {
                    if (classStr === void 0) { classStr = ""; }
                    var tableRow = document.createElement("tr");
                    tableRow.id = "propertyTableRow_" + key + "_" + property;
                    if (classStr.length > 0) {
                        tableRow.classList.add(classStr);
                    }
                    var keyDiv = document.createElement("td");
                    keyDiv.id = "propertyDiv_" + key;
                    keyDiv.innerHTML = key;
                    var propertyDiv = document.createElement("td");
                    propertyDiv.id = "propertyDiv_" + property;
                    propertyDiv.innerHTML = property;
                    tableRow.appendChild(keyDiv);
                    tableRow.appendChild(propertyDiv);
                    return tableRow;
                };
                PropertyWindow.prototype._onPartSelection = function (event) {
                    var _this = this;
                    if (!this._assemblyTreeReadyOccurred || this._volumeSelectionActive)
                        return Promise.resolve();
                    this._update();
                    var model = this._viewer.model;
                    var id = event.getSelection().getNodeId();
                    if (id === null || !model.isNodeLoaded(id)) {
                        return Promise.resolve();
                    }
                    var nodeName = model.getNodeName(id);
                    var propertyTable = document.createElement("table");
                    propertyTable.id = "propertyTable";
                    return model.getNodeProperties(id).then(function (props) {
                        if (props === null) {
                            return;
                        }
                        var propsKeys = Object.keys(props);
                        if (propsKeys.length === 0) {
                            return;
                        }
                        propertyTable.appendChild(_this._createRow("Property", "Value", "headerRow"));
                        propertyTable.appendChild(_this._createRow("Name", nodeName !== null ? nodeName : "unnamed"));
                        for (var _i = 0, propsKeys_1 = propsKeys; _i < propsKeys_1.length; _i++) {
                            var key_2 = propsKeys_1[_i];
                            var k = key_2.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
                            var p = props[key_2].replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
                            propertyTable.appendChild(_this._createRow(k, p));
                        }
                        _this._update("");
                        _this._propertyWindow.append(propertyTable);
                    });
                };
                return PropertyWindow;
            }());
            Desktop.PropertyWindow = PropertyWindow;
        })(Desktop = Ui.Desktop || (Ui.Desktop = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
var Example;
(function (Example) {
    var PulseInfo = /** @class */ (function () {
        function PulseInfo(id, color1, color2, duration) {
            this.direction = 0 /* OneToTwo */;
            this.progress = 0;
            this.id = id;
            this.color1 = color1.copy();
            this.color2 = color2.copy();
            this.duration = duration;
        }
        return PulseInfo;
    }());
    var PulseManager = /** @class */ (function () {
        function PulseManager(viewer) {
            this._pulseInfoMap = {};
            this._defaultColor1 = Communicator.Color.red();
            this._defaultColor2 = new Communicator.Color(175, 0, 0);
            this._defaultPulseTime = 1000;
            this._viewer = viewer;
        }
        PulseManager.prototype.start = function () {
            var _this = this;
            setTimeout(function () {
                _this.update();
            }, 30);
        };
        PulseManager.prototype.deletePulse = function (id) {
            if (this._pulseInfoMap.hasOwnProperty(id.toString())) {
                this._viewer.getModel().unsetNodesFaceColor([id]);
                delete this._pulseInfoMap[id];
            }
        };
        PulseManager.prototype.add = function (id, color1, color2, duration) {
            this.deletePulse(id);
            var pulseInfo = new PulseInfo(id, color1, color2, duration);
            this._pulseInfoMap[id] = pulseInfo;
        };
        PulseManager.prototype.update = function () {
            if (this._previousTime == null) {
                this._previousTime = Date.now();
            }
            var currentTime = Date.now();
            var timeDelta = currentTime - this._previousTime;
            var colorMap = {};
            var itemsPresent = false;
            for (var _i = 0, _a = Object.keys(this._pulseInfoMap); _i < _a.length; _i++) {
                var key_3 = _a[_i];
                var pulseInfo = this._pulseInfoMap[key_3];
                itemsPresent = true;
                pulseInfo.progress = Math.min(pulseInfo.progress + timeDelta, pulseInfo.duration);
                var t = pulseInfo.progress / pulseInfo.duration;
                var a = void 0;
                var b = void 0;
                if (pulseInfo.direction === 0 /* OneToTwo */) {
                    a = pulseInfo.color1;
                    b = pulseInfo.color2;
                }
                else {
                    a = pulseInfo.color2;
                    b = pulseInfo.color1;
                }
                var interpolatedColor = new Communicator.Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t);
                colorMap[pulseInfo.id] = interpolatedColor;
                if (pulseInfo.progress >= pulseInfo.duration) {
                    pulseInfo.direction = (pulseInfo.direction === 0 /* OneToTwo */) ? 1 /* TwoToOne */ : 0 /* OneToTwo */;
                    pulseInfo.progress = 0;
                }
            }
            if (itemsPresent) {
                this._viewer.getModel().setNodesColors(colorMap);
                this._viewer.redraw();
            }
            this._previousTime = currentTime;
            this.start();
        };
        PulseManager.prototype.getDefaultColor1 = function () {
            return this._defaultColor1.copy();
        };
        PulseManager.prototype.getDefaultColor2 = function () {
            return this._defaultColor2.copy();
        };
        PulseManager.prototype.getDefaultPulseTime = function () {
            return this._defaultPulseTime;
        };
        return PulseManager;
    }());
    Example.PulseManager = PulseManager;
})(Example || (Example = {}));
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var RightClickContextMenu = /** @class */ (function (_super) {
            __extends(RightClickContextMenu, _super);
            function RightClickContextMenu(containerId, viewer, isolateZoomHelper) {
                var _this = _super.call(this, "rightclick", containerId, viewer, isolateZoomHelper) || this;
                _this._initEvents();
                return _this;
            }
            RightClickContextMenu.prototype._initEvents = function () {
                var _this = this;
                this._viewer.setCallbacks({
                    contextMenu: function (position, modifiers) {
                        _this._modifiers = modifiers;
                        _this.doContext(position);
                    }
                });
            };
            RightClickContextMenu.prototype.doContext = function (position) {
                var _this = this;
                var config = new Communicator.PickConfig(Communicator.SelectionMask.All);
                return this._viewer.view.pickFromPoint(position, config).then(function (selectionItem) {
                    var axisOverlay = 1;
                    var nodeType;
                    if (selectionItem.isNodeSelection()) {
                        nodeType = _this._viewer.model.getNodeType(selectionItem.getNodeId());
                    }
                    if (nodeType === undefined || nodeType === Communicator.NodeType.Pmi || nodeType === Communicator.NodeType.PmiBody || selectionItem.overlayIndex() === axisOverlay) {
                        _this.setActiveItemId(null);
                    }
                    else {
                        _this.setActiveItemId(selectionItem.getNodeId());
                    }
                    _this._position = selectionItem.getPosition();
                    _this.showElements(position);
                });
            };
            RightClickContextMenu.prototype._onContextLayerClick = function (event) {
                if (event.button === 2)
                    this.doContext(new Communicator.Point2(event.pageX, event.pageY));
                else
                    _super.prototype._onContextLayerClick.call(this, event);
            };
            return RightClickContextMenu;
        }(Ui.Context.ContextMenu));
        Ui.RightClickContextMenu = RightClickContextMenu;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="ViewTree.ts"/>
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var SheetsTree = /** @class */ (function (_super) {
            __extends(SheetsTree, _super);
            function SheetsTree(elementId, viewer) {
                var _this = _super.call(this, elementId, viewer) || this;
                _this._internalId = "sheetstree";
                _this._currentSheetId = null;
                _this._initEvents();
                return _this;
            }
            SheetsTree.prototype._initEvents = function () {
                var _this = this;
                var onNewModel = function () {
                    return _this._onNewModel();
                };
                this._viewer.setCallbacks({
                    _assemblyTreeReady: onNewModel,
                    _firstModelLoaded: onNewModel,
                    _modelSwitched: onNewModel,
                    sheetActivated: function (sheetId) {
                        _this._onSheetActivated(sheetId);
                    },
                    _resetDrawing: function () {
                        var sheets = _this._getSheets();
                        console.assert(sheets.length > 0);
                        return _this._viewer.setActiveSheetId(sheets[0]);
                    }
                });
                this._tree.registerCallback("selectItem", function (id) {
                    _this._onTreeSelectItem(id);
                });
            };
            SheetsTree.prototype._getSheets = function () {
                var model = this._viewer.model;
                var rootId = model.getAbsoluteRootNode();
                var rootChild = model.getNodeChildren(rootId);
                var rootChildChild = model.getNodeChildren(rootChild[0]);
                return model.getNodeChildren(rootChildChild[0]);
            };
            SheetsTree.prototype._onNewModel = function () {
                this._tree.clear();
                var p = Promise.resolve();
                if (this._viewer.model.isDrawing()) {
                    var model = this._viewer.model;
                    var sheetNodeIds = this._getSheets();
                    for (var _i = 0, sheetNodeIds_1 = sheetNodeIds; _i < sheetNodeIds_1.length; _i++) {
                        var sheetNodeId = sheetNodeIds_1[_i];
                        var name_3 = model.getNodeName(sheetNodeId);
                        var sheetElemId = this._sheetId("" + sheetNodeId);
                        this._tree.appendTopLevelElement(name_3, sheetElemId, "sheet", false);
                    }
                    if (sheetNodeIds.length > 0) {
                        p = this._onTreeSelectItem(this._sheetId("" + sheetNodeIds[0]));
                    }
                    this.showTab();
                }
                else {
                    // hide sheets tab if the model is not a drawing
                    this.hideTab();
                }
                return p;
            };
            SheetsTree.prototype._sheetTreeId = function (sheetId) {
                return "sheetstree_" + sheetId;
            };
            SheetsTree.prototype._onSheetActivated = function (sheetId) {
                var idStr = this._sheetTreeId(sheetId);
                if (this._currentSheetId !== null && this._currentSheetId !== idStr) {
                    var $currentSheetNode = $("#" + this._currentSheetId);
                    if ($currentSheetNode !== null) {
                        $currentSheetNode.removeClass("selected-sheet");
                    }
                }
                var $sheetNode = $("#" + idStr);
                if ($sheetNode !== null) {
                    $sheetNode.addClass("selected-sheet");
                }
                this._currentSheetId = idStr;
            };
            SheetsTree.prototype._onTreeSelectItem = function (idStr) {
                var idParts = this._splitIdStr(idStr);
                var id = parseInt(idParts[1], 10);
                return this._viewer.setActiveSheetId(id);
            };
            SheetsTree.prototype._sheetId = function (id) {
                return this._internalId + Ui.ViewTree.separator + id;
            };
            return SheetsTree;
        }(Ui.ViewTree));
        Ui.SheetsTree = SheetsTree;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var StreamingIndicator = /** @class */ (function () {
            function StreamingIndicator(elementId, viewer) {
                var _this = this;
                this._bottomLeftOffset = new Communicator.Point2(10, 10);
                this._opacity = 0.5;
                this._spinnerImageUrl = "css/images/spinner.gif";
                this._spinnerSize = new Communicator.Point2(31, 31);
                this._viewer = viewer;
                this._container = document.getElementById(elementId);
                this._initContainer();
                this._viewer.setCallbacks({
                    streamingActivated: function () { _this._onStreamingActivated(); },
                    streamingDeactivated: function () { _this._onStreamingDeactivated(); }
                });
            }
            StreamingIndicator.prototype.show = function () {
                this._container.style.display = "block";
            };
            StreamingIndicator.prototype.hide = function () {
                this._container.style.display = "none";
            };
            StreamingIndicator.prototype.setBottomLeftOffset = function (point) {
                this._bottomLeftOffset.assign(point);
                this._container.style.left = this._bottomLeftOffset.x + "px";
                this._container.style.bottom = this._bottomLeftOffset.y + "px";
            };
            StreamingIndicator.prototype.getBottomLeftOffset = function () {
                return this._bottomLeftOffset.copy();
            };
            StreamingIndicator.prototype.setSpinnerImage = function (spinnerUrl, size) {
                this._spinnerImageUrl = spinnerUrl;
                this._spinnerSize.assign(size);
                this._container.style.backgroundImage = "url(" + this._spinnerImageUrl + ")";
                this._container.style.width = this._spinnerSize.x + "px";
                this._container.style.height = this._spinnerSize.y + "\"px";
            };
            StreamingIndicator.prototype._initContainer = function () {
                this._container.style.position = "absolute";
                this._container.style.width = this._spinnerSize.x + "px";
                this._container.style.height = this._spinnerSize.y + "px";
                this._container.style.left = this._bottomLeftOffset.x + "px";
                this._container.style.bottom = this._bottomLeftOffset.y + "px";
                this._container.style.backgroundImage = "url(" + this._spinnerImageUrl + ")";
                this._container.style.opacity = "" + this._opacity;
            };
            StreamingIndicator.prototype._onStreamingActivated = function () {
                this.show();
            };
            StreamingIndicator.prototype._onStreamingDeactivated = function () {
                this.hide();
            };
            return StreamingIndicator;
        }());
        Ui.StreamingIndicator = StreamingIndicator;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var UiDialog = /** @class */ (function () {
            function UiDialog(containerId) {
                this._containerId = containerId;
                this._textDiv = UiDialog._createTextDiv();
                this._windowElement = UiDialog._createWindowElement();
                this._headerDiv = UiDialog._createHeaderDiv();
                this._initElements();
            }
            UiDialog._createWindowElement = function () {
                var windowElement = document.createElement("div");
                windowElement.classList.add("ui-timeout-window");
                windowElement.classList.add("desktop-ui-window");
                return windowElement;
            };
            UiDialog._createHeaderDiv = function () {
                var headerDiv = document.createElement("div");
                headerDiv.classList.add("desktop-ui-window-header");
                return headerDiv;
            };
            UiDialog._createTextDiv = function () {
                var textDiv = document.createElement("div");
                return textDiv;
            };
            UiDialog.prototype._initElements = function () {
                var _this = this;
                var contentDiv = document.createElement("div");
                contentDiv.classList.add("desktop-ui-window-content");
                contentDiv.appendChild(this._textDiv);
                var br = document.createElement("div");
                br.classList.add("desktop-ui-window-divider");
                contentDiv.appendChild(br);
                var button = document.createElement("button");
                button.innerHTML = "Ok";
                $(button).button().click(function () {
                    _this._onOkButtonClick();
                });
                contentDiv.appendChild(button);
                this._windowElement.appendChild(this._headerDiv);
                this._windowElement.appendChild(contentDiv);
                var container = document.getElementById(this._containerId);
                if (container !== null) {
                    container.appendChild(this._windowElement);
                }
            };
            UiDialog.prototype._onOkButtonClick = function () {
                this.hide();
            };
            UiDialog.prototype.show = function () {
                $(this._windowElement).show();
            };
            UiDialog.prototype.hide = function () {
                $(this._windowElement).hide();
            };
            UiDialog.prototype.setText = function (text) {
                $(this._textDiv).empty();
                this._textDiv.appendChild(document.createTextNode(text));
            };
            UiDialog.prototype.setTitle = function (title) {
                $(this._headerDiv).empty();
                this._headerDiv.appendChild(document.createTextNode(title));
            };
            return UiDialog;
        }());
        Ui.UiDialog = UiDialog;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="UiDialog.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var TimeoutWarningDialog = /** @class */ (function (_super) {
            __extends(TimeoutWarningDialog, _super);
            function TimeoutWarningDialog(containerId, viewer) {
                var _this = _super.call(this, containerId) || this;
                _this._viewer = viewer;
                _this._viewer.setCallbacks({
                    timeoutWarning: function () {
                        _this._onTimeoutWarning();
                    },
                    timeout: function () {
                        _this._onTimeout();
                    }
                });
                _this.setTitle("Timeout Warning");
                return _this;
            }
            TimeoutWarningDialog.prototype._onTimeoutWarning = function () {
                this.setText("Your session will expire soon. Press Ok to stay connected.");
                this.show();
            };
            TimeoutWarningDialog.prototype._onOkButtonClick = function () {
                this._viewer.resetClientTimeout();
                _super.prototype._onOkButtonClick.call(this);
            };
            TimeoutWarningDialog.prototype._onTimeout = function () {
                this.setText("Your session has been disconnected due to inactivity.");
                this.show();
            };
            return TimeoutWarningDialog;
        }(Ui.UiDialog));
        Ui.TimeoutWarningDialog = TimeoutWarningDialog;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Toolbar = /** @class */ (function () {
            function Toolbar(viewer, cuttingPlaneController, screenConfiguration) {
                if (screenConfiguration === void 0) { screenConfiguration = Communicator.ScreenConfiguration.Desktop; }
                var _this = this;
                this._toolbarSelector = "#toolBar";
                this._screenElementSelector = "#content";
                this._cuttingPlaneXSelector = "#cuttingplane-x";
                this._cuttingPlaneYSelector = "#cuttingplane-y";
                this._cuttingPlaneZSelector = "#cuttingplane-z";
                this._cuttingPlaneFaceSelector = "#cuttingplane-face";
                this._cuttingPlaneVisibilitySelector = "#cuttingplane-section";
                this._cuttingPlaneGroupToggle = "#cuttingplane-toggle";
                this._cuttingPlaneResetSelector = "#cuttingplane-reset";
                this._selectedClass = "selected";
                this._disabledClass = "disabled";
                this._invertedClass = "inverted";
                this._submenuHeightOffset = 10;
                this._viewOrientationDuration = 500;
                this._activeSubmenu = null;
                this._actionsNullary = {};
                this._actionsBoolean = {};
                this._isInitialized = false;
                this._viewer = viewer;
                this._noteTextManager = this._viewer._getNoteTextManager();
                this._screenConfiguration = screenConfiguration;
                this._cuttingPlaneController = cuttingPlaneController;
                this._viewerSettings = new Ui.Desktop.ViewerSettings(viewer);
                this._viewer.setCallbacks({
                    selectionArray: function (events) {
                        if (events.length > 0) {
                            var selection = events[events.length - 1];
                            var selectionItem = selection.getSelection();
                            if (selectionItem !== null && selectionItem.isFaceSelection()) {
                                $(_this._cuttingPlaneFaceSelector).removeClass(_this._disabledClass);
                                $("#view-face").removeClass(_this._disabledClass);
                            }
                        }
                        else {
                            $(_this._cuttingPlaneFaceSelector).addClass(_this._disabledClass);
                            $("#view-face").addClass(_this._disabledClass);
                        }
                    },
                    _cuttingSectionsLoaded: function () {
                        return _this._cuttingPlaneController.onSectionsChanged().then(function () {
                            _this._updateCuttingPlaneIcons();
                        });
                    },
                });
            }
            Toolbar.prototype.init = function () {
                var _this = this;
                if (this._isInitialized)
                    return;
                this._initIcons();
                this._removeNonApplicableIcons();
                $(".hoops-tool").bind("click", function (event) {
                    event.preventDefault();
                    _this._processButtonClick(event);
                    return false;
                });
                $(".submenu-icon").bind("click", function (event) {
                    event.preventDefault();
                    _this._submenuIconClick(event.target);
                    return false;
                });
                $(this._toolbarSelector).bind("touchmove", function (event) {
                    event.originalEvent.preventDefault();
                });
                $(this._toolbarSelector).bind("mouseenter", function () {
                    _this._mouseEnter();
                });
                $(this._toolbarSelector).bind("mouseleave", function () {
                    _this._mouseLeave();
                });
                $(".tool-icon, .submenu-icon").bind("mouseenter", function (event) {
                    _this._mouseEnterItem(event);
                });
                $(".tool-icon, .submenu-icon").bind("mouseleave", function (event) {
                    _this._mouseLeaveItem(event);
                });
                $(window).resize(function () {
                    _this.reposition();
                });
                $(this._toolbarSelector).click(function () {
                    if (_this._activeSubmenu !== null) {
                        _this._hideActiveSubmenu();
                    }
                });
                $(".toolbar-cp-plane").click(function (event) {
                    _this._cuttingPlaneButtonClick(event);
                });
                this._viewer.setCallbacks({
                    modelSwitched: function () {
                        _this._hideActiveSubmenu();
                    }
                });
                this._initSliders();
                this._initActions();
                this._initSnapshot();
                this.updateEdgeFaceButton();
                this.reposition();
                this.show();
                this._isInitialized = true;
            };
            Toolbar.prototype.disableSubmenuItem = function (item) {
                var _this = this;
                if (typeof item === "string") {
                    $("#submenus .toolbar-" + item).addClass(this._disabledClass);
                }
                else if (typeof item === "object") {
                    $.each(item, function (key, value) {
                        key;
                        $("#submenus .toolbar-" + value).addClass(_this._disabledClass);
                    });
                }
            };
            Toolbar.prototype.enableSubmenuItem = function (item) {
                var _this = this;
                if (typeof item === "string") {
                    $("#submenus .toolbar-" + item).removeClass(this._disabledClass);
                }
                else if (typeof item === "object") {
                    $.each(item, function (key, value) {
                        key;
                        $("#submenus .toolbar-" + value).removeClass(_this._disabledClass);
                    });
                }
            };
            Toolbar.prototype.setCorrespondingButtonForSubmenuItem = function (value) {
                var $item = $("#submenus .toolbar-" + value);
                this._activateSubmenuItem($item);
            };
            Toolbar.prototype._mouseEnterItem = function (event) {
                var $target = $(event.target);
                if (!$target.hasClass(this._disabledClass))
                    $target.addClass("hover");
            };
            Toolbar.prototype._mouseLeaveItem = function (event) {
                $(event.target).removeClass("hover");
            };
            Toolbar.prototype.show = function () {
                $(this._toolbarSelector).show();
            };
            Toolbar.prototype.hide = function () {
                $(this._toolbarSelector).hide();
            };
            Toolbar.prototype._initSliders = function () {
                var _this = this;
                $("#explosion-slider").slider({
                    orientation: "vertical",
                    min: 0,
                    max: 200,
                    value: 0,
                    slide: function (event, ui) {
                        event;
                        _this._onExplosionSlider((ui.value || 0) / 100);
                    },
                });
            };
            Toolbar.prototype._mouseEnter = function () {
                if (this._activeSubmenu === null) {
                    var $tools = $(this._toolbarSelector).find(".toolbar-tools");
                    $tools.stop();
                    $tools.css({
                        opacity: 1.0
                    });
                }
            };
            Toolbar.prototype._mouseLeave = function () {
                if (this._activeSubmenu === null) {
                    $(".toolbar-tools").animate({
                        opacity: 0.6,
                    }, 500, function () {
                        // Animation complete.
                    });
                }
            };
            Toolbar.prototype.reposition = function () {
                var $toolbar = $(this._toolbarSelector);
                var $screen = $(this._screenElementSelector);
                var canvasCenterX = $screen.width() / 2;
                var toolbarX = canvasCenterX - ($toolbar.width() / 2);
                $toolbar.css({
                    left: toolbarX + "px",
                    bottom: "15px"
                });
            };
            Toolbar.prototype._processButtonClick = function (event) {
                if (this._activeSubmenu !== null) {
                    this._hideActiveSubmenu();
                }
                else {
                    var $tool = $(event.target).closest(".hoops-tool");
                    if ($tool.hasClass("toolbar-radio")) {
                        if ($tool.hasClass("active-tool")) {
                            this._showSubmenu(event.target);
                        }
                        else {
                            $(this._toolbarSelector).find(".active-tool").removeClass("active-tool");
                            $tool.addClass("active-tool");
                            this._performNullaryAction($tool.data("operatorclass"));
                        }
                    }
                    else if ($tool.hasClass("toolbar-menu")) {
                        this._showSubmenu(event.target);
                    }
                    else if ($tool.hasClass("toolbar-menu-toggle")) {
                        this._toggleMenuTool($tool);
                    }
                    else {
                        this._performNullaryAction($tool.data("operatorclass"));
                    }
                }
            };
            Toolbar.prototype._toggleMenuTool = function ($tool) {
                var $toggleMenu = $("#" + $tool.data("submenu"));
                if ($toggleMenu.is(":visible")) {
                    $toggleMenu.hide();
                    this._performBooleanAction($tool.data("operatorclass"), false);
                }
                else {
                    this._alignMenuToTool($toggleMenu, $tool);
                    this._performBooleanAction($tool.data("operatorclass"), true);
                }
            };
            Toolbar.prototype._startModal = function () {
                var _this = this;
                $("body").append("<div id='toolbar-modal' class='toolbar-modal-overlay'></div>");
                $("#toolbar-modal").bind("click", function () {
                    _this._hideActiveSubmenu();
                });
            };
            Toolbar.prototype._alignMenuToTool = function ($submenu, $tool) {
                var position = $tool.position();
                var leftPositionOffset = position.left;
                if (this._screenConfiguration === Communicator.ScreenConfiguration.Mobile) {
                    // constant scale transform from Toolbar.css
                    var mobileScale = 1.74;
                    leftPositionOffset = leftPositionOffset / mobileScale;
                }
                var leftpos = leftPositionOffset - ($submenu.width() / 2) + 20;
                var topPos = -(this._submenuHeightOffset + $submenu.height());
                $submenu.css({
                    display: "block",
                    left: leftpos + "px",
                    top: topPos + "px",
                });
            };
            Toolbar.prototype._showSubmenu = function (item) {
                this._hideActiveSubmenu();
                var $tool = $(item).closest(".hoops-tool");
                var submenuId = $tool.data("submenu");
                if (submenuId != null) {
                    var $submenu = $(this._toolbarSelector + " #submenus #" + submenuId);
                    if (!$submenu.hasClass(this._disabledClass)) {
                        this._alignMenuToTool($submenu, $tool);
                        this._activeSubmenu = $submenu[0];
                        this._startModal();
                        $(this._toolbarSelector).find(".toolbar-tools").css({
                            opacity: 0.3
                        });
                    }
                }
            };
            Toolbar.prototype._hideActiveSubmenu = function () {
                $("#toolbar-modal").remove();
                if (this._activeSubmenu !== null) {
                    $(this._activeSubmenu).hide();
                    $(this._toolbarSelector).find(".toolbar-tools").css({
                        opacity: 1.0
                    });
                }
                this._activeSubmenu = null;
            };
            Toolbar.prototype._activateSubmenuItem = function (submenuItem) {
                var $submenu = submenuItem.closest(".toolbar-submenu");
                var action = submenuItem.data("operatorclass");
                if (typeof action !== "string") {
                    throw new Error();
                }
                var $tool = $("#" + $submenu.data("button"));
                var $icon = $tool.find(".tool-icon");
                if ($icon.length) {
                    $icon.removeClass($tool.data("operatorclass").toString());
                    $icon.addClass(action);
                    $tool.data("operatorclass", action);
                    $tool.attr("title", submenuItem.attr("title"));
                }
                return action;
            };
            Toolbar.prototype._submenuIconClick = function (item) {
                var $selection = $(item);
                if ($selection.hasClass(this._disabledClass))
                    return;
                var action = this._activateSubmenuItem($selection);
                this._hideActiveSubmenu();
                this._performNullaryAction(action);
            };
            Toolbar.prototype._initIcons = function () {
                $(this._toolbarSelector).find(".hoops-tool").each(function () {
                    var $element = $(this);
                    $element.find(".tool-icon").addClass($element.data("operatorclass").toString());
                });
                $(this._toolbarSelector).find(".submenu-icon").each(function () {
                    var $element = $(this);
                    $element.addClass($element.data("operatorclass").toString());
                });
            };
            Toolbar.prototype._removeNonApplicableIcons = function () {
                if (this._screenConfiguration === Communicator.ScreenConfiguration.Mobile) {
                    $("#snapshot-button").remove();
                }
            };
            Toolbar.prototype.setSubmenuEnabled = function (button, enabled) {
                var $button = $("#" + button);
                var $submenu = $("#" + $button.data("submenu"));
                if (enabled) {
                    $button.find(".smarrow").show();
                    $submenu.removeClass(this._disabledClass);
                }
                else {
                    $button.find(".smarrow").hide();
                    $submenu.addClass(this._disabledClass);
                }
            };
            Toolbar.prototype._performNullaryAction = function (action) {
                var func = this._actionsNullary[action];
                if (func) {
                    func();
                }
            };
            Toolbar.prototype._performBooleanAction = function (action, arg) {
                var func = this._actionsBoolean[action];
                if (func) {
                    func(arg);
                }
            };
            Toolbar.prototype._renderModeClick = function (action) {
                var view = this._viewer.view;
                switch (action) {
                    case "toolbar-shaded":
                        return view.setDrawMode(Communicator.DrawMode.Shaded);
                    case "toolbar-wireframe":
                        return view.setDrawMode(Communicator.DrawMode.Wireframe);
                    case "toolbar-hidden-line":
                        return view.setDrawMode(Communicator.DrawMode.HiddenLine);
                    case "toolbar-xray":
                        return view.setDrawMode(Communicator.DrawMode.XRay);
                    default:
                    case "toolbar-wireframeshaded":
                        return view.setDrawMode(Communicator.DrawMode.WireframeOnShaded);
                }
            };
            Toolbar.prototype._initSnapshot = function () {
                $("#snapshot-dialog-cancel-button").button().click(function () {
                    $("#snapshot-dialog").hide();
                });
            };
            Toolbar.prototype._doSnapshot = function () {
                var $screen = $("#content");
                var windowWidth = $screen.width();
                var windowHeight = $screen.height();
                var canvasSize = this._viewer.view.getCanvasSize();
                var percentageOfWindow = .7;
                var windowAspect = canvasSize.x / canvasSize.y;
                var renderHeight = 480;
                var renderWidth = windowAspect * renderHeight;
                if (percentageOfWindow > 0) {
                    //Using a percentage of the window size since allows the user to resize the image
                    renderHeight = windowHeight * percentageOfWindow;
                    renderWidth = windowWidth * percentageOfWindow;
                }
                var dialogWidth = renderWidth + 40;
                // snap with promise returning the composited image
                var config = new Communicator.SnapshotConfig(canvasSize.x, canvasSize.y);
                this._viewer.takeSnapshot(config)
                    .then(function (image) {
                    var xpos = (windowWidth - renderWidth) / 2;
                    var $dialog = $("#snapshot-dialog");
                    $("#snapshot-dialog-image").attr("src", image.src).attr("width", dialogWidth).attr("height", renderHeight + 40);
                    $dialog.css({
                        top: "45px",
                        left: xpos + "px",
                    });
                    $dialog.show();
                });
            };
            Toolbar.prototype._setRedlineOperator = function (operatorId) {
                this._viewer.operatorManager.set(operatorId, 1);
            };
            Toolbar.prototype._initActions = function () {
                var _this = this;
                var view = this._viewer.view;
                var operatorManager = this._viewer.operatorManager;
                this._actionsNullary["toolbar-home"] = function () {
                    if (_this._viewer.model.isDrawing()) {
                        var sheetId = _this._viewer.getActiveSheetId();
                        if (sheetId !== null) {
                            view.isolateNodes([sheetId]);
                        }
                    }
                    else {
                        _this._viewer.reset();
                        _this._noteTextManager.setIsolateActive(false);
                        _this._noteTextManager.updatePinVisibility();
                        var handleOperator = operatorManager.getOperator(Communicator.OperatorId.Handle);
                        if (handleOperator !== null && handleOperator.removeHandles) {
                            handleOperator.removeHandles();
                        }
                    }
                };
                this._actionsNullary["toolbar-redline-circle"] = function () {
                    _this._setRedlineOperator(Communicator.OperatorId.RedlineCircle);
                };
                this._actionsNullary["toolbar-redline-freehand"] = function () {
                    _this._setRedlineOperator(Communicator.OperatorId.RedlinePolyline);
                };
                this._actionsNullary["toolbar-redline-rectangle"] = function () {
                    _this._setRedlineOperator(Communicator.OperatorId.RedlineRectangle);
                };
                this._actionsNullary["toolbar-redline-note"] = function () {
                    _this._setRedlineOperator(Communicator.OperatorId.RedlineText);
                };
                this._actionsNullary["toolbar-note"] = function () {
                    operatorManager.set(Communicator.OperatorId.Note, 1);
                };
                this._actionsNullary["toolbar-select"] = function () {
                    operatorManager.set(Communicator.OperatorId.Select, 1);
                };
                this._actionsNullary["toolbar-area-select"] = function () {
                    operatorManager.set(Communicator.OperatorId.AreaSelect, 1);
                };
                this._actionsNullary["toolbar-orbit"] = function () {
                    operatorManager.set(Communicator.OperatorId.Navigate, 0);
                };
                this._actionsNullary["toolbar-turntable"] = function () {
                    operatorManager.set(Communicator.OperatorId.Turntable, 0);
                };
                this._actionsNullary["toolbar-walk"] = function () {
                    operatorManager.set(Communicator.OperatorId.WalkMode, 0);
                };
                this._actionsNullary["toolbar-face"] = function () {
                    _this._orientToFace();
                };
                this._actionsNullary["toolbar-measure-point"] = function () {
                    operatorManager.set(Communicator.OperatorId.MeasurePointPointDistance, 1);
                };
                this._actionsNullary["toolbar-measure-edge"] = function () {
                    operatorManager.set(Communicator.OperatorId.MeasureEdgeLength, 1);
                };
                this._actionsNullary["toolbar-measure-distance"] = function () {
                    operatorManager.set(Communicator.OperatorId.MeasureFaceFaceDistance, 1);
                };
                this._actionsNullary["toolbar-measure-angle"] = function () {
                    operatorManager.set(Communicator.OperatorId.MeasureFaceFaceAngle, 1);
                };
                this._actionsNullary["toolbar-cuttingplane"] = function () {
                    return;
                };
                this._actionsBoolean["toolbar-explode"] = function (visibility) {
                    _this._explosionButtonClick(visibility);
                };
                this._actionsNullary["toolbar-settings"] = function () {
                    _this._settingsButtonClick();
                };
                this._actionsNullary["toolbar-wireframeshaded"] = function () {
                    _this._renderModeClick("toolbar-wireframeshaded");
                };
                this._actionsNullary["toolbar-shaded"] = function () {
                    _this._renderModeClick("toolbar-shaded");
                };
                this._actionsNullary["toolbar-wireframe"] = function () {
                    _this._renderModeClick("toolbar-wireframe");
                };
                this._actionsNullary["toolbar-hidden-line"] = function () {
                    _this._renderModeClick("toolbar-hidden-line");
                };
                this._actionsNullary["toolbar-xray"] = function () {
                    _this._renderModeClick("toolbar-xray");
                };
                this._actionsNullary["toolbar-front"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Front, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-back"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Back, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-left"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Left, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-right"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Right, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-bottom"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Bottom, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-top"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Top, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-iso"] = function () {
                    view.setViewOrientation(Communicator.ViewOrientation.Iso, _this._viewOrientationDuration);
                };
                this._actionsNullary["toolbar-ortho"] = function () {
                    view.setProjectionMode(Communicator.Projection.Orthographic);
                };
                this._actionsNullary["toolbar-persp"] = function () {
                    view.setProjectionMode(Communicator.Projection.Perspective);
                };
                this._actionsNullary["toolbar-snapshot"] = function () {
                    _this._doSnapshot();
                };
            };
            Toolbar.prototype._onExplosionSlider = function (value) {
                return this._viewer.explodeManager.setMagnitude(value);
            };
            Toolbar.prototype._explosionButtonClick = function (visibility) {
                var explodeManager = this._viewer.explodeManager;
                if (visibility && !explodeManager.getActive()) {
                    return explodeManager.start();
                }
                return Promise.resolve();
            };
            Toolbar.prototype._settingsButtonClick = function () {
                return this._viewerSettings.show();
            };
            Toolbar.prototype.updateEdgeFaceButton = function () {
                var view = this._viewer.view;
                var edgeVisibility = view.getLineVisibility();
                var faceVisibility = view.getFaceVisibility();
                if (edgeVisibility && faceVisibility)
                    this.setCorrespondingButtonForSubmenuItem("wireframeshaded");
                else if (!edgeVisibility && faceVisibility)
                    this.setCorrespondingButtonForSubmenuItem("shaded");
                else
                    this.setCorrespondingButtonForSubmenuItem("wireframe");
            };
            Toolbar.prototype._cuttingPlaneButtonClick = function (event) {
                var _this = this;
                var $element = $(event.target).closest(".toolbar-cp-plane");
                var planeAction = $element.data("plane");
                var p;
                var axis = this._getAxis(planeAction);
                if (axis !== null) {
                    p = this._cuttingPlaneController.toggle(axis);
                }
                else if (planeAction === "section") {
                    p = this._cuttingPlaneController.toggleReferenceGeometry();
                }
                else if (planeAction === "toggle") {
                    p = this._cuttingPlaneController.toggleCuttingMode();
                }
                else if (planeAction === "reset") {
                    p = this._cuttingPlaneController.resetCuttingPlanes();
                }
                else {
                    p = Promise.resolve();
                }
                return p.then(function () {
                    _this._updateCuttingPlaneIcons();
                });
            };
            Toolbar.prototype._getAxis = function (planeAxis) {
                switch (planeAxis) {
                    case 'x':
                        return Ui.AxisIndex.X;
                    case 'y':
                        return Ui.AxisIndex.Y;
                    case 'z':
                        return Ui.AxisIndex.Z;
                    case 'face':
                        return Ui.AxisIndex.FACE;
                    default:
                        return null;
                }
            };
            Toolbar.prototype._updateCuttingPlaneIcons = function () {
                var geometryEnabled = this._cuttingPlaneController.getReferenceGeometryEnabled();
                var individualCuttingSection = this._cuttingPlaneController.getIndividualCuttingSectionEnabled();
                var count = this._cuttingPlaneController.getCount();
                this._updateCuttingPlaneIcon(Ui.AxisIndex.X, this._cuttingPlaneXSelector);
                this._updateCuttingPlaneIcon(Ui.AxisIndex.Y, this._cuttingPlaneYSelector);
                this._updateCuttingPlaneIcon(Ui.AxisIndex.Z, this._cuttingPlaneZSelector);
                this._updateCuttingPlaneIcon(Ui.AxisIndex.FACE, this._cuttingPlaneFaceSelector);
                if (geometryEnabled) {
                    $(this._cuttingPlaneVisibilitySelector).removeClass(this._selectedClass);
                }
                else {
                    $(this._cuttingPlaneVisibilitySelector).addClass(this._selectedClass);
                }
                if (individualCuttingSection) {
                    $(this._cuttingPlaneGroupToggle).removeClass(this._selectedClass);
                }
                else {
                    $(this._cuttingPlaneGroupToggle).addClass(this._selectedClass);
                }
                if (count > 0) {
                    $(this._cuttingPlaneVisibilitySelector).removeClass(this._disabledClass);
                    $(this._cuttingPlaneResetSelector).removeClass(this._disabledClass);
                }
                else {
                    $(this._cuttingPlaneVisibilitySelector).addClass(this._disabledClass);
                    $(this._cuttingPlaneResetSelector).addClass(this._disabledClass);
                }
                if (count > 1) {
                    $(this._cuttingPlaneGroupToggle).removeClass(this._disabledClass);
                }
                else {
                    $(this._cuttingPlaneGroupToggle).addClass(this._disabledClass);
                }
            };
            Toolbar.prototype._updateCuttingPlaneIcon = function (axis, cuttingPlaneSelector) {
                var $cuttingPlaneButton = $(cuttingPlaneSelector);
                $cuttingPlaneButton.removeClass(this._selectedClass);
                $cuttingPlaneButton.removeClass(this._invertedClass);
                var planeInfo = this._cuttingPlaneController.getPlaneInfo(axis);
                if (planeInfo !== undefined) {
                    if (planeInfo.status === 1 /* Visible */) {
                        $cuttingPlaneButton.addClass(this._selectedClass);
                    }
                    else if (planeInfo.status === 2 /* Inverted */) {
                        $cuttingPlaneButton.addClass(this._invertedClass);
                    }
                }
            };
            Toolbar.prototype._orientToFace = function () {
                var selectionItem = this._viewer.selectionManager.getLast();
                if (selectionItem !== null && selectionItem.isFaceSelection()) {
                    var view = this._viewer.view;
                    var normal = selectionItem.getFaceEntity().getNormal();
                    var position = selectionItem.getPosition();
                    var camera = view.getCamera();
                    var up = Communicator.Point3.cross(normal, new Communicator.Point3(0, 1, 0));
                    if (up.length() < .001) {
                        up = Communicator.Point3.cross(normal, new Communicator.Point3(1, 0, 0));
                    }
                    var zoomDelta = camera.getPosition().subtract(camera.getTarget()).length();
                    camera.setTarget(position);
                    camera.setPosition(Communicator.Point3.add(position, Communicator.Point3.scale(normal, zoomDelta)));
                    camera.setUp(up);
                    return view.fitBounding(selectionItem.getFaceEntity().getBounding(), Communicator.DefaultTransitionDuration, camera);
                }
                return Promise.resolve();
            };
            return Toolbar;
        }());
        Ui.Toolbar = Toolbar;
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/jquery/jquery.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Control;
        (function (Control) {
            var TaggedId = /** @class */ (function () {
                function TaggedId(id) {
                    this.nodeId = null;
                    this.guid = null;
                    if (typeof id === "number") {
                        this.nodeId = id;
                    }
                    else {
                        this.guid = id;
                    }
                }
                return TaggedId;
            }());
            var VisibilityControl = /** @class */ (function () {
                function VisibilityControl(viewer) {
                    var _this = this;
                    this._fullHiddenParentIds = [];
                    this._partialHiddenParentIds = [];
                    this._assemblyTreeReadyOccurred = false;
                    this._viewer = viewer;
                    var updateVisibilityState = function () {
                        _this.updateModelTreeVisibilityState();
                        return Promise.resolve();
                    };
                    this._viewer.setCallbacks({
                        _assemblyTreeReady: function () {
                            _this._assemblyTreeReadyOccurred = true;
                            return updateVisibilityState();
                        },
                        _firstModelLoaded: updateVisibilityState,
                    });
                }
                VisibilityControl.prototype._clearStyles = function () {
                    for (var _i = 0, _a = this._fullHiddenParentIds; _i < _a.length; _i++) {
                        var id = _a[_i];
                        this._removeVisibilityHiddenClass(id, "partHidden");
                    }
                    this._fullHiddenParentIds.length = 0;
                    for (var _b = 0, _c = this._partialHiddenParentIds; _b < _c.length; _b++) {
                        var id = _c[_b];
                        this._removeVisibilityHiddenClass(id, "partialHidden");
                    }
                    this._partialHiddenParentIds.length = 0;
                };
                VisibilityControl.prototype._applyStyles = function () {
                    for (var _i = 0, _a = this._fullHiddenParentIds; _i < _a.length; _i++) {
                        var id = _a[_i];
                        this._addVisibilityHiddenClass(id, "partHidden");
                    }
                    for (var _b = 0, _c = this._partialHiddenParentIds; _b < _c.length; _b++) {
                        var id = _c[_b];
                        this._addVisibilityHiddenClass(id, "partialHidden");
                    }
                };
                VisibilityControl.prototype.updateModelTreeVisibilityState = function () {
                    // guard against calling model before the model structure is ready
                    if (this._assemblyTreeReadyOccurred) {
                        this._clearStyles();
                        var model = this._viewer.model;
                        var nodeQueue = [model.getAbsoluteRootNode()];
                        for (var _i = 0, nodeQueue_1 = nodeQueue; _i < nodeQueue_1.length; _i++) {
                            var nodeId = nodeQueue_1[_i];
                            var nodeStatus = model.getBranchVisibility(nodeId);
                            if (nodeStatus === Communicator.BranchVisibility.Hidden) {
                                this._fullHiddenParentIds.push(nodeId);
                            }
                            else if (nodeStatus === Communicator.BranchVisibility.Mixed) {
                                this._partialHiddenParentIds.push(nodeId);
                                var nodeChildren = model.getNodeChildren(nodeId);
                                for (var _a = 0, nodeChildren_1 = nodeChildren; _a < nodeChildren_1.length; _a++) {
                                    var child = nodeChildren_1[_a];
                                    nodeQueue.push(child);
                                }
                            }
                        }
                        this._applyStyles();
                    }
                };
                VisibilityControl.prototype._getVisibilityItem = function (id) {
                    return $("#visibility_part_" + id);
                };
                VisibilityControl.prototype._addVisibilityHiddenClass = function (id, className) {
                    this._getVisibilityItem(id).addClass(className);
                };
                VisibilityControl.prototype._removeVisibilityHiddenClass = function (id, className) {
                    this._getVisibilityItem(id).removeClass(className);
                };
                return VisibilityControl;
            }());
            Control.VisibilityControl = VisibilityControl;
            var TreeControl = /** @class */ (function () {
                function TreeControl(elementId, viewer, separator, treeScroll) {
                    if (treeScroll === void 0) { treeScroll = null; }
                    // keeps track of the last clicked list item
                    this._$lastItem = null;
                    // keeps track of the selection items
                    this._selectedItems = [];
                    // keep track of nodes that are in the selection set but not in the model tree
                    this._futureHighlightIds = new Set();
                    // keep track of nodes that have children that are selected, but are not in the model tree
                    this._futureMixedIds = new Set();
                    // keep track of selected items parents
                    this._selectedItemsParentIds = [];
                    this._callbacks = new Map();
                    this._childrenLoaded = new Set();
                    this._loadedNodes = new Set();
                    this._slideSpeed = 100;
                    this._touchTimerId = null;
                    // prevent the model browser nodes from expanding
                    this._freezeExpansion = false;
                    // set a timeout for scrolling and clear it each time a new item in the tree should be visible.
                    // this will avoid scrolling down the tree when several nodes are selected at once
                    this._scrollTimerId = null;
                    // prevent selection highlighting from triggering if multiple items are being selected in succesion
                    this._selectionLabelHighlightTimerId = null;
                    this._elementId = elementId;
                    this._viewer = viewer;
                    this._treeScroll = treeScroll;
                    this._separator = separator;
                    this._visibilityControl = new VisibilityControl(viewer);
                    this._partVisibilityRoot = document.createElement("ul");
                    this._listRoot = document.createElement("ul");
                    this._init();
                }
                TreeControl.prototype.getElementId = function () {
                    return this._elementId;
                };
                TreeControl.prototype.getRoot = function () {
                    return this._listRoot;
                };
                TreeControl.prototype.getPartVisibilityRoot = function () {
                    return this._partVisibilityRoot;
                };
                TreeControl.prototype.getVisibilityControl = function () {
                    return this._visibilityControl;
                };
                TreeControl.prototype.registerCallback = function (name, func) {
                    if (!this._callbacks.has(name))
                        this._callbacks.set(name, []);
                    this._callbacks.get(name).push(func);
                };
                TreeControl.prototype._triggerCallback = function (name) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    var callbacks = this._callbacks.get(name);
                    if (callbacks) {
                        for (var _a = 0, callbacks_1 = callbacks; _a < callbacks_1.length; _a++) {
                            var callback = callbacks_1[_a];
                            callback.apply(null, args);
                        }
                    }
                };
                TreeControl.prototype.deleteNode = function (id) {
                    if (id.charAt(0) === '#')
                        jQuery(id).remove();
                    else
                        jQuery("#" + id).remove();
                };
                TreeControl.prototype._getTaggedId = function (id, treeType, name) {
                    var annotationViewsLabel = "Annotation Views";
                    if (name !== null && name === annotationViewsLabel && treeType === Ui.Desktop.Tree.CadView) {
                        return new TaggedId(annotationViewsLabel); // return a non null tagged id for annotation views
                    }
                    else {
                        return this._parseTaggedId(id);
                    }
                };
                TreeControl.prototype.addChild = function (name, id, parent, itemType, hasChildren, treeType) {
                    var taggedId = this._getTaggedId(id, treeType, name);
                    if (taggedId === null) {
                        return null;
                    }
                    var model = this._viewer.model;
                    if (treeType === Ui.Desktop.Tree.Model && itemType !== "container" && taggedId.nodeId !== null) {
                        if (this._loadedNodes.has(taggedId.nodeId)) {
                            return null;
                        }
                        this._loadedNodes.add(taggedId.nodeId);
                    }
                    if (name === null) {
                        name = "unnamed";
                    }
                    if (!model.isDrawing()) {
                        // add corresponding visibility icon
                        this._addVisibilityToggleChild(id, parent, itemType);
                    }
                    var $parent = jQuery("#" + parent);
                    $parent.children(".ui-modeltree-container").children(".ui-modeltree-expandNode").css("visibility", "visible");
                    var $childList = $parent.children("ul");
                    var selected = false;
                    var mixed = false;
                    if (taggedId.nodeId !== null) {
                        selected = this._futureHighlightIds.has(taggedId.nodeId);
                        mixed = this._futureMixedIds.has(taggedId.nodeId);
                        if (selected) {
                            this._futureHighlightIds.delete(taggedId.nodeId);
                        }
                        if (mixed) {
                            this._futureMixedIds.delete(taggedId.nodeId);
                        }
                    }
                    var node = this._buildNode(name, id, itemType, hasChildren, selected, mixed);
                    // ensure parent node has children
                    if ($childList.length === 0) {
                        var target = document.createElement("ul");
                        target.classList.add("ui-modeltree-children");
                        $parent.append(target);
                        target.appendChild(node);
                    }
                    else {
                        $childList.get(0).appendChild(node);
                    }
                    if (selected) {
                        var $listItem = this._getListItem(id);
                        if ($listItem !== null) {
                            this._selectedItems.push($listItem);
                        }
                    }
                    this._triggerCallback("addChild");
                    return node;
                };
                TreeControl.prototype._addVisibilityToggleChild = function (id, parent, itemType) {
                    // mirrors addChild for the visibility toggle icons
                    var $parent = jQuery("#visibility" + this._separator + parent);
                    $parent.children(".ui-modeltree-visibility-container").css("visibility", "visible");
                    var $childList = $parent.children("ul");
                    var target;
                    // ensure parent node has children
                    if ($childList.length === 0) {
                        target = document.createElement("ul");
                        target.classList.add("ui-modeltree-visibility-children");
                        $parent.append(target);
                    }
                    else {
                        target = $childList.get(0);
                    }
                    var node = this._buildPartVisibilityNode(id, itemType);
                    target.appendChild(node);
                };
                TreeControl.prototype._buildPartVisibilityNode = function (id, itemType) {
                    var itemNode = document.createElement("div");
                    itemNode.classList.add("ui-modeltree-partVisibility-icon");
                    var childItem = document.createElement("li");
                    childItem.classList.add("ui-modeltree-item");
                    childItem.classList.add("visibility");
                    childItem.id = "visibility_" + id;
                    childItem.appendChild(itemNode);
                    // measurement ids cannot be parsed to check if the node is a PMI
                    // and can result in hiding the visibility icon in the model browser
                    if (itemType !== "measurement") {
                        // hide the visibility icon on PMI items
                        var nodeId = void 0;
                        var nodeIdStr = id.split(this._separator).pop();
                        if (nodeIdStr !== undefined) {
                            nodeId = parseInt(nodeIdStr, 10);
                        }
                        if (nodeId === undefined || isNaN(nodeId)) {
                            return childItem;
                        }
                        var nodeType = this._viewer.model.getNodeType(nodeId);
                        if (nodeType === Communicator.NodeType.Pmi || nodeType === Communicator.NodeType.PmiBody) {
                            childItem.style.visibility = "hidden";
                        }
                    }
                    return childItem;
                };
                TreeControl.prototype.freezeExpansion = function (freeze) {
                    this._freezeExpansion = freeze;
                };
                TreeControl.prototype.updateSelection = function () {
                    this._updateTreeSelectionHighlight();
                    this._doUnfreezeSelection();
                };
                TreeControl.prototype.expandChildren = function (id) {
                    var $item = $("#" + id);
                    this.preloadChildrenIfNecessary(id);
                    if (!this._freezeExpansion) {
                        if ($item.length > 0) {
                            $item.children(".ui-modeltree-children").slideDown(this._slideSpeed);
                            // ensure that expand button is updated
                            $item.children(".ui-modeltree-container").children(".ui-modeltree-expandNode").addClass("expanded");
                        }
                        this._expandVisibilityChildren(id);
                    }
                };
                TreeControl.prototype._expandVisibilityChildren = function (id) {
                    var $item = $("#visibility" + (this._separator + id));
                    if ($item.length > 0) {
                        $item.children(".ui-modeltree-visibility-children").addClass("visible");
                        $item.children(".ui-modeltree-visibility-children").slideDown(this._slideSpeed);
                    }
                };
                TreeControl.prototype.collapseChildren = function (id) {
                    this._collapseVisibilityChildren(id);
                    var $item = $("#" + id);
                    if ($item.length > 0)
                        $item.children(".ui-modeltree-children").slideUp(this._slideSpeed);
                };
                TreeControl.prototype._collapseVisibilityChildren = function (id) {
                    var $item = $("#visibility" + this._separator + id);
                    if ($item.length > 0)
                        $item.children(".ui-modeltree-visibility-children").slideUp(this._slideSpeed);
                };
                TreeControl.prototype._buildNode = function (name, id, itemType, hasChildren, selected, mixed) {
                    if (selected === void 0) { selected = false; }
                    if (mixed === void 0) { mixed = false; }
                    var childItem = document.createElement("li");
                    childItem.classList.add("ui-modeltree-item");
                    if (selected) {
                        childItem.classList.add("selected");
                    }
                    if (mixed) {
                        childItem.classList.add("mixed");
                    }
                    childItem.id = id;
                    var itemNode = document.createElement("div");
                    itemNode.classList.add("ui-modeltree-container");
                    itemNode.style.whiteSpace = "nowrap";
                    var expandNode = document.createElement("div");
                    expandNode.classList.add("ui-modeltree-expandNode");
                    if (!hasChildren)
                        expandNode.style.visibility = "hidden";
                    itemNode.appendChild(expandNode);
                    var iconNode = document.createElement("div");
                    iconNode.classList.add("ui-modeltree-icon");
                    iconNode.classList.add(itemType);
                    itemNode.appendChild(iconNode);
                    var labelNode = document.createElement("div");
                    labelNode.classList.add("ui-modeltree-label");
                    labelNode.innerHTML = name;
                    labelNode.title = name;
                    itemNode.appendChild(labelNode);
                    var mixedSelection = document.createElement("div");
                    mixedSelection.classList.add("ui-mixedselection-icon");
                    itemNode.appendChild(mixedSelection);
                    childItem.appendChild(itemNode);
                    return childItem;
                };
                TreeControl.prototype.childrenAreLoaded = function (id) {
                    return this._childrenLoaded.has(id);
                };
                TreeControl.prototype.preloadChildrenIfNecessary = function (id) {
                    if (id !== null && !this._childrenLoaded.has(id)) {
                        this._triggerCallback("loadChildren", id);
                        this._childrenLoaded.add(id);
                    }
                };
                TreeControl.prototype._processExpandClick = function (event) {
                    var $target = jQuery(event.target);
                    var $listItem = $target.parents(".ui-modeltree-item");
                    var id = $listItem.get(0).id;
                    if ($target.hasClass("expanded")) {
                        this._collapseListItem(id, $target);
                    }
                    else {
                        this._expandListItem(id, $target);
                    }
                };
                TreeControl.prototype._collapseListItem = function (id, $target) {
                    this.collapseChildren(id);
                    $target.removeClass("expanded");
                    this._triggerCallback("collapse", id);
                };
                TreeControl.prototype._expandListItem = function (id, $target) {
                    // if children are not loaded, we need to request the nodes for it
                    this.expandChildren(id);
                    $target.addClass("expanded");
                    this._triggerCallback("expand", id);
                };
                TreeControl.prototype.selectItem = function (id, triggerEvent) {
                    if (triggerEvent === void 0) { triggerEvent = true; }
                    if (id !== null) {
                        var $listItem = this._getListItem(id);
                        if ($listItem !== null) {
                            this._doSelection($listItem, triggerEvent);
                        }
                    }
                    else {
                        this._doSelection(null, triggerEvent);
                    }
                };
                TreeControl.prototype._getListItem = function (id) {
                    var $listItem = $(this._listRoot).find("#" + id);
                    if ($listItem.length > 0) {
                        return $listItem;
                    }
                    return null;
                };
                TreeControl.prototype._updateNonSelectionHighlight = function ($listItem) {
                    if (this._$lastNonSelectionItem !== undefined) {
                        this._$lastNonSelectionItem.removeClass("selected");
                    }
                    $listItem.addClass("selected");
                    this._$lastNonSelectionItem = $listItem;
                };
                TreeControl.prototype._doUnfreezeSelection = function () {
                    var selectionItems = this._viewer.selectionManager.getResults();
                    for (var _i = 0, selectionItems_2 = selectionItems; _i < selectionItems_2.length; _i++) {
                        var item = selectionItems_2[_i];
                        var id = item.getNodeId();
                        var $listItem = this._getListItem("part_" + id);
                        if ($listItem !== null && !$listItem.hasClass("selected")) {
                            $listItem.addClass("selected");
                            this._selectedItems.push($listItem);
                        }
                        else if ($listItem === null) {
                            this._futureHighlightIds.add(id);
                        }
                    }
                };
                TreeControl.prototype._doSelection = function ($listItem, triggerEvent) {
                    var _this = this;
                    if (triggerEvent === void 0) { triggerEvent = true; }
                    if ($listItem !== null) {
                        var id = $listItem.get(0).id;
                        if (id.lastIndexOf("part", 0) === 0) {
                            $listItem.addClass("selected");
                            // we will keep track of selection items in an array to update the highlighting for multiple items
                            var contains = false;
                            for (var _i = 0, _a = this._selectedItems; _i < _a.length; _i++) {
                                var item = _a[_i];
                                if (id === item.get(0).id) {
                                    contains = true;
                                    break;
                                }
                            }
                            if (!contains) {
                                //only add the item to the selected items if it is not already included
                                this._selectedItems.push($listItem);
                            }
                        }
                        else if (id.lastIndexOf("sheet", 0) === 0) {
                            // nothing to do
                        }
                        else {
                            // keeps track of the item if it's not of type 'part'.
                            if (id.lastIndexOf("container", 0) === 0) {
                                return;
                            }
                            else {
                                this._updateNonSelectionHighlight($listItem);
                            }
                        }
                        if (triggerEvent) {
                            this._$lastItem = $listItem;
                            var mode = typeof key !== "undefined" && (key.ctrl || key.command) ? Communicator.SelectionMode.Toggle : Communicator.SelectionMode.Set;
                            this._triggerCallback("selectItem", id, mode);
                        }
                        /* This function gets called twice when a label is selected. Once when a label is selected,
                         * and again after the selection callback is triggered. If a label is clicked, we do not want
                         * to scroll the item into view.
                         */
                        var newListItem = this._$lastItem && this._$lastItem.get(0).id !== $listItem.get(0).id;
                        if (!this._freezeExpansion && (!triggerEvent && (newListItem || !this._$lastItem))) {
                            var offsetTop = $listItem.offset().top;
                            var containerHeight = $("#modelTreeContainer").innerHeight();
                            var hiddenTop = offsetTop < 6;
                            var hiddenBottom = offsetTop > containerHeight;
                            // only scroll to the element if it is not currently visible in the model browser
                            if (hiddenTop || hiddenBottom) {
                                if (this._scrollTimerId !== null) {
                                    clearTimeout(this._scrollTimerId);
                                }
                                this._scrollTimerId = setTimeout(function () {
                                    _this._scrollTimerId = null;
                                    if (_this._treeScroll) {
                                        _this._treeScroll.refresh();
                                        _this._treeScroll.scrollToElement($listItem.get(0), Communicator.DefaultTransitionDuration, true, true);
                                    }
                                }, 10);
                            }
                        }
                    }
                    this._$lastItem = $listItem;
                    if (this._selectionLabelHighlightTimerId !== null) {
                        clearTimeout(this._selectionLabelHighlightTimerId);
                    }
                    this._selectionLabelHighlightTimerId = setTimeout(function () {
                        _this._selectionLabelHighlightTimerId = null;
                        _this._updateTreeSelectionHighlight();
                    }, 30);
                };
                TreeControl.prototype._parseTaggedId = function (htmlId) {
                    var nodeId = this._parseNodeId(htmlId);
                    if (nodeId !== null) {
                        return new TaggedId(nodeId);
                    }
                    var guid = this._parseGuid(htmlId);
                    if (guid !== null) {
                        return new TaggedId(guid);
                    }
                    return null;
                };
                TreeControl.prototype._parseNodeId = function (htmlId) {
                    var idPart = htmlId.split(this._separator).pop();
                    if (idPart !== undefined) {
                        var nodeId = parseInt(idPart, 10);
                        if (!isNaN(nodeId)) {
                            return nodeId;
                        }
                    }
                    return null;
                };
                TreeControl.prototype._parseGuid = function (htmlId) {
                    var hyphenatedGuidLen = 36;
                    var idPart = htmlId.split(this._separator).pop();
                    if (idPart !== undefined && idPart.length === hyphenatedGuidLen) {
                        return idPart;
                    }
                    return null;
                };
                TreeControl.prototype._parseMeasurementId = function (id) {
                    return id.split(this._separator).pop();
                };
                // update the tree highlighting for selection items (not cadviews, measurements, etc)
                TreeControl.prototype._updateTreeSelectionHighlight = function () {
                    var _this = this;
                    var selectionItems = this._viewer.selectionManager.getResults();
                    var selectionIds = selectionItems.map(function (item) { return item.getNodeId(); });
                    // update the future highlight list to reflect the current selection set
                    this._futureHighlightIds.forEach(function (key) {
                        if (selectionIds.indexOf(key) >= 0) {
                            _this._futureHighlightIds.delete(key);
                        }
                    });
                    for (var _i = 0, _a = this._selectedItemsParentIds; _i < _a.length; _i++) {
                        var id = _a[_i];
                        $("#part_" + id).removeClass("mixed");
                    }
                    this._selectedItemsParentIds.length = 0;
                    this._futureMixedIds.clear();
                    // remove items that are no longer selected.
                    Communicator.Internal.inPlaceFilter(this._selectedItems, function (item) {
                        var id = _this._parseNodeId(item.get(0).id);
                        if (id === null || selectionIds.indexOf(id) < 0) {
                            item.removeClass("selected");
                            return false;
                        }
                        return true;
                    });
                    // add all parents of selected items for the "mixed" icon
                    for (var _b = 0, selectionIds_1 = selectionIds; _b < selectionIds_1.length; _b++) {
                        var id = selectionIds_1[_b];
                        this._updateParentIdList(id);
                    }
                    for (var _c = 0, _d = this._selectedItemsParentIds; _c < _d.length; _c++) {
                        var id = _d[_c];
                        var $listItem = this._getListItem("part_" + id);
                        if ($listItem !== null && !$listItem.hasClass("mixed")) {
                            $listItem.addClass("mixed");
                        }
                        else {
                            this._futureMixedIds.add(id);
                        }
                    }
                };
                // add mixed class to parents of selected items
                TreeControl.prototype._updateParentIdList = function (childId) {
                    var model = this._viewer.model;
                    if (model.isNodeLoaded(childId)) {
                        var parentId = model.getNodeParent(childId);
                        while (parentId !== null && this._selectedItemsParentIds.indexOf(parentId) === -1) {
                            this._selectedItemsParentIds.push(parentId);
                            parentId = model.getNodeParent(parentId);
                        }
                    }
                };
                TreeControl.prototype.triggerCallback = function (id, position) {
                    this._triggerCallback("context", id, position);
                };
                TreeControl.prototype._doContext = function ($listItem, position) {
                    var id = $listItem.get(0).id;
                    this.triggerCallback(id, position);
                };
                TreeControl.prototype._processLabelContext = function (event, position) {
                    var $target = jQuery(event.target);
                    var $listItem = $target.closest(".ui-modeltree-item");
                    if (!position) {
                        position = new Communicator.Point2(event.clientX, event.clientY);
                    }
                    this._doContext($listItem, position);
                };
                TreeControl.prototype._processLabelClick = function (event) {
                    var $target = jQuery(event.target);
                    var $listItem = $target.closest(".ui-modeltree-item");
                    this._doSelection($listItem, true);
                };
                TreeControl.prototype.appendTopLevelElement = function (name, id, itemType, hasChildren, loadChildren, markChildrenLoaded) {
                    if (loadChildren === void 0) { loadChildren = true; }
                    if (markChildrenLoaded === void 0) { markChildrenLoaded = false; }
                    if (name === null) {
                        name = "unnamed";
                    }
                    var childItem = this._buildNode(name, id, itemType, hasChildren);
                    if (id.substring(0, 4) === "part" && this._listRoot.firstChild) {
                        this._listRoot.insertBefore(childItem, this._listRoot.firstChild);
                    }
                    else {
                        this._listRoot.appendChild(childItem);
                    }
                    if (!this._viewer.model.isDrawing()) {
                        var childVisibilityItem = this._buildPartVisibilityNode(id, itemType);
                        this._partVisibilityRoot.appendChild(childVisibilityItem);
                    }
                    if (loadChildren) {
                        this.preloadChildrenIfNecessary(id);
                    }
                    if (markChildrenLoaded) {
                        this._childrenLoaded.add(id);
                    }
                    return childItem;
                };
                TreeControl.prototype.insertNodeAfter = function (name, id, itemType, element, hasChildren) {
                    if (name === null) {
                        name = "unnamed";
                    }
                    if (element.parentNode === null) {
                        throw new Error("element.parentNode is null");
                    }
                    var childItem = this._buildNode(name, id, itemType, hasChildren);
                    if (element.nextSibling)
                        element.parentNode.insertBefore(childItem, element.nextSibling);
                    else
                        element.parentNode.appendChild(childItem);
                    this.preloadChildrenIfNecessary(id);
                    return childItem;
                };
                TreeControl.prototype.clear = function () {
                    while (this._listRoot.firstChild) {
                        this._listRoot.removeChild(this._listRoot.firstChild);
                    }
                    while (this._partVisibilityRoot.firstChild) {
                        this._partVisibilityRoot.removeChild(this._partVisibilityRoot.firstChild);
                    }
                    this._childrenLoaded.clear();
                    this._loadedNodes.clear();
                };
                // expand to first node with multiple children
                TreeControl.prototype.expandInitialNodes = function (id) {
                    var currentNodeId = id;
                    var childNodes = [];
                    while (childNodes.length <= 1) {
                        childNodes = this._getChildItemsFromModelTreeItem($("#" + currentNodeId));
                        // If there are no children, do not try to expand them
                        if (childNodes.length === 0) {
                            break;
                        }
                        this.expandChildren(currentNodeId);
                        currentNodeId = childNodes[0].id;
                        this.preloadChildrenIfNecessary(currentNodeId);
                    }
                };
                // update the visibility of a selected part in the scene
                TreeControl.prototype.processVisibilityClick = function (event, position) {
                    position;
                    var $target = jQuery(event.target);
                    var $listItem = $target.closest(".ui-modeltree-item");
                    var id = $listItem[0].id;
                    var prefix = id.substring(0, 15);
                    if (prefix === "visibility_part") {
                        var parsedId = this._parseNodeId(id);
                        if (parsedId === null) {
                            return Promise.resolve();
                        }
                        var model = this._viewer.model;
                        var visibility = model.getNodeVisibility(parsedId);
                        return model.setNodesVisibility([parsedId], !visibility);
                    }
                    else if (prefix === "visibility_meas") {
                        this._processMeasurementVisibilityClick(id);
                    }
                    return Promise.resolve();
                };
                //update the visibility state of measurement items in the scene
                TreeControl.prototype._processMeasurementVisibilityClick = function (id) {
                    var parsedId = this._parseMeasurementId(id);
                    var measureItems = this._viewer.measureManager.getAllMeasurements();
                    if (parsedId === "measurementitems") {
                        // root folder, toggle all measurement items visibility
                        var visibility = true;
                        for (var _i = 0, measureItems_1 = measureItems; _i < measureItems_1.length; _i++) {
                            var measureItem = measureItems_1[_i];
                            if (measureItem.getVisibility()) {
                                visibility = false;
                                break;
                            }
                        }
                        for (var _a = 0, measureItems_2 = measureItems; _a < measureItems_2.length; _a++) {
                            var measureItem = measureItems_2[_a];
                            measureItem.setVisibility(visibility);
                        }
                    }
                    else {
                        for (var _b = 0, measureItems_3 = measureItems; _b < measureItems_3.length; _b++) {
                            var measureItem = measureItems_3[_b];
                            if (parsedId === measureItem._getId()) {
                                var visibility = measureItem.getVisibility();
                                measureItem.setVisibility(!visibility);
                            }
                        }
                    }
                };
                // update the visibility icons in the measurement folder
                TreeControl.prototype.updateMeasurementVisibilityIcons = function () {
                    var measureItems = this._viewer.measureManager.getAllMeasurements();
                    var hiddenCount = 0;
                    for (var _i = 0, measureItems_4 = measureItems; _i < measureItems_4.length; _i++) {
                        var measureItem = measureItems_4[_i];
                        var visibility = measureItem.getVisibility();
                        var elem = $("#visibility_measurement_" + measureItem._getId());
                        if (!visibility) {
                            hiddenCount++;
                            elem.addClass("partHidden");
                        }
                        else {
                            elem.removeClass("partHidden");
                        }
                    }
                    var measurementFolder = $("#visibility_measurementitems");
                    if (hiddenCount === measureItems.length) {
                        measurementFolder.removeClass("partialHidden");
                        measurementFolder.addClass("partHidden");
                    }
                    else if (hiddenCount > 0 && hiddenCount < measureItems.length) {
                        measurementFolder.removeClass("partHidden");
                        measurementFolder.addClass("partialHidden");
                    }
                    else {
                        measurementFolder.removeClass("partialHidden");
                        measurementFolder.removeClass("partHidden");
                    }
                    this._viewer.markupManager.updateLater();
                };
                TreeControl.prototype._init = function () {
                    var _this = this;
                    var container = document.getElementById(this._elementId);
                    if (container === null) {
                        throw new Error("container is null");
                    }
                    this._partVisibilityRoot.classList.add("ui-visibility-toggle");
                    if (!this._viewer.model.isDrawing()) {
                        container.appendChild(this._partVisibilityRoot);
                    }
                    this._listRoot.classList.add("ui-modeltree");
                    this._listRoot.classList.add("ui-modeltree-item");
                    container.appendChild(this._listRoot);
                    $(container).on("click", ".ui-modeltree-label", function (event) {
                        _this._processLabelClick(event);
                    });
                    $(container).on("click", ".ui-modeltree-expandNode", function (event) {
                        _this._processExpandClick(event);
                    });
                    $(container).on("click", ".ui-modeltree-partVisibility-icon", function (event) {
                        _this.processVisibilityClick(event);
                    });
                    $(container).on("click", "#contextMenuButton", function (event) {
                        _this._processLabelContext(event);
                    });
                    $(container).on("mouseup", ".ui-modeltree-label, .ui-modeltree-icon", function (event) {
                        if (event.which === 3)
                            _this._processLabelContext(event);
                    });
                    $(container).on("touchstart", function (event) {
                        if (_this._touchTimerId !== null) {
                            clearTimeout(_this._touchTimerId);
                        }
                        _this._touchTimerId = setTimeout(function () {
                            _this._touchTimerId = null;
                            var e = event.originalEvent;
                            var x = e.touches[0].pageX;
                            var y = e.touches[0].pageY;
                            var position = new Communicator.Point2(x, y);
                            _this._processLabelContext(event, position);
                        }, 1000);
                    });
                    $(container).on("touchmove", function (event) {
                        event;
                        if (_this._touchTimerId) {
                            clearTimeout(_this._touchTimerId);
                            _this._touchTimerId = null;
                        }
                    });
                    $(container).on("touchend", function (event) {
                        event;
                        if (_this._touchTimerId) {
                            clearTimeout(_this._touchTimerId);
                            _this._touchTimerId = null;
                        }
                    });
                    $(container).on("contextmenu", ".ui-modeltree-label", function (event) {
                        event.preventDefault();
                    });
                };
                TreeControl.prototype._getChildItemsFromModelTreeItem = function ($modeltreeItem) {
                    var $childItems = $modeltreeItem.children(".ui-modeltree-children").children(".ui-modeltree-item");
                    return $.makeArray($childItems);
                };
                return TreeControl;
            }());
            Control.TreeControl = TreeControl;
        })(Control = Ui.Control || (Ui.Control = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Desktop;
        (function (Desktop) {
            var ViewerSettings = /** @class */ (function () {
                function ViewerSettings(viewer) {
                    this._versionInfo = true;
                    this._pmiColor = Communicator.Color.black();
                    this._pmiEnabled = false;
                    this._orbitCameraTarget = false;
                    this._axisTriadEnabled = true;
                    this._navCubeEnabled = true;
                    this._splatRenderingEnabled = false;
                    this._splatRenderingSize = .003;
                    this._splatRenderingPointSizeUnit = Communicator.PointSizeUnit.ProportionOfBoundingDiagonal;
                    this._eyeDomeLightingEnabled = false;
                    this._walkSpeedUnits = 1;
                    this._viewer = viewer;
                    var view = this._viewer.view;
                    this._navCube = view.getNavCube();
                    this._axisTriad = view.getAxisTriad();
                    this._viewerSettingsSelector = "#viewer-settings-dialog";
                    this._initElements();
                }
                ViewerSettings.prototype.show = function () {
                    var p = this._readSettings();
                    this._centerWindow();
                    $(this._viewerSettingsSelector).show();
                    return p;
                };
                ViewerSettings.prototype.hide = function () {
                    $(this._viewerSettingsSelector).hide();
                };
                ViewerSettings.prototype._centerWindow = function () {
                    var $settingsDialog = $(this._viewerSettingsSelector);
                    var width = $settingsDialog.width() + 1;
                    var height = $settingsDialog.height();
                    var canvasSize = this._viewer.view.getCanvasSize();
                    var leftPos = (canvasSize.x - width) / 2;
                    var topPos = (canvasSize.y - height) / 2;
                    $settingsDialog.css({
                        left: leftPos + "px",
                        top: topPos + "px",
                    });
                };
                ViewerSettings.prototype._initElements = function () {
                    var _this = this;
                    $(this._viewerSettingsSelector).draggable({
                        handle: ".hoops-ui-window-header"
                    });
                    $("INPUT.color-picker").each(function () {
                        $(this).minicolors({
                            position: $(this).attr('data-position') || 'bottom left',
                            format: "rgb",
                            control: "hue"
                        });
                    });
                    $("#viewer-settings-ok-button").click(function () {
                        _this._applySettings();
                        _this.hide();
                    });
                    $("#viewer-settings-cancel-button").click(function () {
                        _this.hide();
                    });
                    $("#viewer-settings-apply-button").click(function () {
                        _this._applySettings();
                    });
                    $("#settings-pmi-enabled").click(function () {
                        _this._updateEnabledStyle("settings-pmi-enabled", ["settings-pmi-color-style"], ["settings-pmi-color"], $("#settings-pmi-enabled").prop("checked"));
                    });
                    $("#settings-splat-rendering-enabled").click(function () {
                        _this._updateEnabledStyle("settings-splat-rendering-enabled", ["settings-splat-enabled-style"], ["settings-splat-rendering-size", "settings-splat-rendering-point-size-unit"], $("#settings-splat-rendering-enabled").prop("checked"));
                    });
                    $("#settings-mouse-look-enabled").click(function () {
                        _this._updateEnabledStyle("settings-mouse-look-enabled", ["settings-mouse-look-style"], ["settings-mouse-look-speed"], $("#settings-mouse-look-enabled").prop("checked"));
                    });
                    $("#settings-walk-mode").change(function () {
                        var walkMode = parseInt($("#settings-walk-mode").val(), 10);
                        _this._updateKeyboardWalkModeStyle(walkMode);
                    });
                    // tabs
                    var generalTabLabel = $("#settings-tab-label-general");
                    var walkTabLabel = $("#settings-tab-label-walk");
                    var walkTab = $("#settings-tab-walk");
                    var generalTab = $("#settings-tab-general");
                    generalTabLabel.click(function () {
                        generalTabLabel.addClass("selected");
                        generalTab.addClass("selected");
                        walkTab.removeClass("selected");
                        walkTabLabel.removeClass("selected");
                    });
                    walkTabLabel.click(function () {
                        walkTab.addClass("selected");
                        walkTabLabel.addClass("selected");
                        generalTabLabel.removeClass("selected");
                        generalTab.removeClass("selected");
                    });
                };
                // takes current settings and updates the settings window.
                ViewerSettings.prototype._readSettings = function () {
                    var _this = this;
                    var view = this._viewer.view;
                    var model = this._viewer.model;
                    var selectionManager = this._viewer.selectionManager;
                    var cuttingManager = this._viewer.cuttingManager;
                    var measureManager = this._viewer.measureManager;
                    var operatorManager = this._viewer.operatorManager;
                    // show version info
                    if (this._versionInfo) {
                        $("#settings-format-version").html(this._viewer.getFormatVersionString());
                        $("#settings-viewer-version").html(this._viewer.getViewerVersionString());
                        this._versionInfo = false;
                    }
                    // read current values
                    var backgroundColor = view.getBackgroundColor();
                    if (backgroundColor.top === null) {
                        this._backgroundColorTop = this._colorFromRgbString("rgb(192,220,248)");
                    }
                    else {
                        this._backgroundColorTop = backgroundColor.top;
                    }
                    if (backgroundColor.bottom === null) {
                        this._backgroundColorBottom = this._colorFromRgbString("rgb(192,220,248)");
                    }
                    else {
                        this._backgroundColorBottom = backgroundColor.bottom;
                    }
                    this._selectionColorBody = selectionManager.getNodeSelectionColor();
                    this._selectionColorFaceLine = selectionManager.getNodeElementSelectionColor();
                    this._measurementColor = measureManager.getMeasurementColor();
                    this._projectionMode = view.getProjectionMode();
                    this._showBackfaces = view.getBackfacesVisible();
                    this._hiddenLineOpacity = view.getHiddenLineSettings().getObscuredLineOpacity();
                    this._showCappingGeometry = cuttingManager.getCappingGeometryVisibility();
                    this._enableFaceLineSelection = selectionManager.getHighlightFaceElementSelection() && selectionManager.getHighlightLineElementSelection();
                    this._cappingGeometryFaceColor = cuttingManager.getCappingFaceColor();
                    this._cappingGeometryLineColor = cuttingManager.getCappingLineColor();
                    this._ambientOcclusionEnabled = view.getAmbientOcclusionEnabled();
                    this._ambientOcclusionRadius = view.getAmbientOcclusionRadius();
                    this._antiAliasingEnabled = view.getAntiAliasingMode() === Communicator.AntiAliasingMode.SMAA;
                    this._pmiColor = model.getPmiColor();
                    this._pmiEnabled = model.getPmiColorOverride();
                    var orbitOperator = operatorManager.getOperator(Communicator.OperatorId.Orbit);
                    this._orbitCameraTarget = orbitOperator.getOrbitFallbackMode() === Communicator.OrbitFallbackMode.CameraTarget ? true : false;
                    this._axisTriadEnabled = this._axisTriad.getEnabled();
                    this._navCubeEnabled = this._navCube.getEnabled();
                    this._readWalkSettings();
                    // update settings window
                    $("#settings-selection-color-body").minicolors("value", this._rgbStringFromColor(this._selectionColorBody));
                    $("#settings-selection-color-face-line").minicolors("value", this._rgbStringFromColor(this._selectionColorFaceLine));
                    $("#settings-background-top").minicolors("value", this._rgbStringFromColor(this._backgroundColorTop));
                    $("#settings-background-bottom").minicolors("value", this._rgbStringFromColor(this._backgroundColorBottom));
                    $("#settings-measurement-color").minicolors("value", this._rgbStringFromColor(this._measurementColor));
                    $("#settings-capping-face-color").minicolors("value", this._rgbStringFromColor(this._cappingGeometryFaceColor));
                    $("#settings-capping-line-color").minicolors("value", this._rgbStringFromColor(this._cappingGeometryLineColor));
                    $("#settings-projection-mode").val("" + this._projectionMode);
                    $("#settings-show-backfaces").prop("checked", this._showBackfaces);
                    $("#settings-show-capping-geometry").prop("checked", this._showCappingGeometry);
                    $("#settings-enable-face-line-selection").prop("checked", this._enableFaceLineSelection);
                    $("#settings-orbit-mode").prop("checked", this._orbitCameraTarget);
                    $("#settings-ambient-occlusion").prop("checked", this._ambientOcclusionEnabled);
                    $("#settings-ambient-occlusion-radius").val("" + this._ambientOcclusionRadius);
                    $("#settings-anti-aliasing").prop("checked", this._antiAliasingEnabled);
                    $("#settings-axis-triad").prop("checked", this._axisTriadEnabled);
                    $("#settings-nav-cube").prop("checked", this._navCubeEnabled);
                    $("#settings-pmi-color").minicolors("value", this._rgbStringFromColor(this._pmiColor));
                    if (this._pmiEnabled !== $("#settings-pmi-enabled").prop("checked")) {
                        $("#settings-pmi-enabled").click();
                    }
                    this._viewer.getMinimumFramerate().then(function (minFramerate) {
                        _this._minFramerate = minFramerate;
                        $("#settings-framerate").val("" + _this._minFramerate);
                    });
                    if (this._hiddenLineOpacity != null) {
                        $("#settings-hidden-line-opacity").val("" + this._hiddenLineOpacity);
                    }
                    else {
                        $("#settings-hidden-line-opacity").val("");
                    }
                    var ps = [];
                    /*
                        When the default values of (1, ScreenPixels), splat rendering is considered to be off.
        
                        When splat rendering is turned on, we will use a default of (.003, ProportionOfBoundingDiagonal).
        
                        Each time the viewer settings window is opened, we will check if there have been new values set,
                        and if there are, those will become the new defaults when it is turned on.
                    */
                    ps.push(view.getPointSize().then(function (value) {
                        var splatRenderingSize = value[0];
                        var splatRenderingPointSizeUnit = value[1];
                        _this._splatRenderingEnabled = splatRenderingSize !== 1 || splatRenderingPointSizeUnit !== Communicator.PointSizeUnit.ScreenPixels;
                        if (_this._splatRenderingEnabled !== $("#settings-splat-rendering-enabled").prop("checked")) {
                            $("#settings-splat-rendering-enabled").click();
                        }
                        if (_this._splatRenderingEnabled) {
                            _this._splatRenderingSize = splatRenderingSize;
                            _this._splatRenderingPointSizeUnit = splatRenderingPointSizeUnit;
                        }
                        $("#settings-splat-rendering-size").val("" + _this._splatRenderingSize);
                        $("#settings-splat-rendering-point-size-unit").val("" + _this._splatRenderingPointSizeUnit);
                    }));
                    ps.push(view.getEyeDomeLightingEnabled().then(function (enabled) {
                        _this._eyeDomeLightingEnabled = enabled;
                        $("#settings-eye-dome-lighting-enabled").prop("checked", _this._eyeDomeLightingEnabled);
                    }));
                    return Promise.all(ps);
                };
                ViewerSettings.prototype._applySettings = function () {
                    var ps = [];
                    var view = this._viewer.view;
                    var model = this._viewer.model;
                    var cuttingManager = this._viewer.cuttingManager;
                    var selectionManager = this._viewer.selectionManager;
                    this._applyWalkSettings();
                    // set background color
                    var backgroundTop = this._colorFromRgbString($("#settings-background-top").val());
                    var backgroundBottom = this._colorFromRgbString($("#settings-background-bottom").val());
                    ps.push(this._viewer.view.setBackgroundColor(backgroundTop, backgroundBottom));
                    //set selection color
                    var selectionColorBody = this._colorFromRgbString($("#settings-selection-color-body").val());
                    ps.push(selectionManager.setNodeSelectionColor(selectionColorBody));
                    ps.push(selectionManager.setNodeSelectionOutlineColor(selectionColorBody));
                    // set face / line selection color
                    var selectionColorFaceLine = this._colorFromRgbString($("#settings-selection-color-face-line").val());
                    ps.push(selectionManager.setNodeElementSelectionColor(selectionColorFaceLine));
                    ps.push(selectionManager.setNodeElementSelectionOutlineColor(selectionColorFaceLine));
                    // enable face / line selection
                    var enableFaceLineSelection = $("#settings-enable-face-line-selection").prop("checked");
                    ps.push(selectionManager.setHighlightFaceElementSelection(enableFaceLineSelection));
                    ps.push(selectionManager.setHighlightLineElementSelection(enableFaceLineSelection));
                    // set measurement color
                    this._viewer.measureManager.setMeasurementColor(this._colorFromRgbString($("#settings-measurement-color").val()));
                    // set PMI color
                    this._pmiColor = this._colorFromRgbString($("#settings-pmi-color").val());
                    this._pmiEnabled = $("#settings-pmi-enabled").prop("checked");
                    if (this._pmiColor && this._pmiEnabled) {
                        model.setPmiColor(this._pmiColor);
                        ps.push(model.setPmiColorOverride(true));
                    }
                    else {
                        ps.push(model.setPmiColorOverride(false));
                    }
                    // set capping geometry color
                    ps.push(cuttingManager.setCappingFaceColor(this._colorFromRgbString($("#settings-capping-face-color").val())));
                    ps.push(cuttingManager.setCappingLineColor(this._colorFromRgbString($("#settings-capping-line-color").val())));
                    //set projection mode
                    view.setProjectionMode(parseInt($("#settings-projection-mode").val(), 10));
                    // set show backfaces
                    var showBackfaces = $("#settings-show-backfaces").prop("checked");
                    ps.push(view.setBackfacesVisible(showBackfaces));
                    // set show capping geometry
                    var showCappingGeometry = $("#settings-show-capping-geometry").prop("checked");
                    ps.push(cuttingManager.setCappingGeometryVisibility(showCappingGeometry));
                    // set framerate
                    var minFramerate = parseInt($("#settings-framerate").val(), 10);
                    if (minFramerate && minFramerate > 0) {
                        this._minFramerate = minFramerate;
                        ps.push(this._viewer.setMinimumFramerate(this._minFramerate));
                    }
                    // set hidden line opacity
                    var hiddenLineOpacity = parseFloat($("#settings-hidden-line-opacity").val());
                    view.getHiddenLineSettings().setObscuredLineOpacity(hiddenLineOpacity);
                    if (view.getDrawMode() === Communicator.DrawMode.HiddenLine) {
                        ps.push(view.setDrawMode(Communicator.DrawMode.HiddenLine));
                    }
                    // set orbit fallback mode
                    var orbitOperator = this._viewer.operatorManager.getOperator(Communicator.OperatorId.Orbit);
                    var orbitCameraTarget = $("#settings-orbit-mode").prop("checked");
                    orbitOperator.setOrbitFallbackMode(orbitCameraTarget ? Communicator.OrbitFallbackMode.CameraTarget : Communicator.OrbitFallbackMode.ModelCenter);
                    // set ambient occlusion mode and radius
                    ps.push(view.setAmbientOcclusionEnabled($("#settings-ambient-occlusion").prop("checked")));
                    ps.push(view.setAmbientOcclusionRadius(parseFloat($("#settings-ambient-occlusion-radius").val())));
                    // anti aliasing
                    if ($("#settings-anti-aliasing").prop("checked"))
                        ps.push(view.setAntiAliasingMode(Communicator.AntiAliasingMode.SMAA));
                    else
                        ps.push(view.setAntiAliasingMode(Communicator.AntiAliasingMode.None));
                    // axis triad
                    if ($("#settings-axis-triad").prop("checked"))
                        ps.push(this._axisTriad.enable());
                    else
                        ps.push(this._axisTriad.disable());
                    // nav cube
                    if ($("#settings-nav-cube").prop("checked"))
                        ps.push(this._navCube.enable());
                    else
                        ps.push(this._navCube.disable());
                    // set splat rendering
                    if ($("#settings-splat-rendering-enabled").prop("checked")) {
                        this._splatRenderingEnabled = true;
                        this._splatRenderingSize = parseFloat($("#settings-splat-rendering-size").val());
                        this._splatRenderingPointSizeUnit = parseInt($("#settings-splat-rendering-point-size-unit").val(), 10);
                        ps.push(view.setPointSize(this._splatRenderingSize, this._splatRenderingPointSizeUnit));
                    }
                    else {
                        this._splatRenderingEnabled = false;
                        ps.push(view.setPointSize(1, Communicator.PointSizeUnit.ScreenPixels));
                    }
                    // set eye-dome lighting
                    ps.push(view.setEyeDomeLightingEnabled($("#settings-eye-dome-lighting-enabled").prop("checked")));
                    return Promise.all(ps);
                };
                ViewerSettings.prototype._applyWalkSettings = function () {
                    var _this = this;
                    var operatorManager = this._viewer.operatorManager;
                    var keyboardWalkOperator = operatorManager.getOperator(Communicator.OperatorId.KeyboardWalk);
                    var walkModeOperator = operatorManager.getOperator(Communicator.OperatorId.WalkMode);
                    var walkMode = parseInt($("#settings-walk-mode").val(), 10);
                    walkModeOperator.setWalkMode(walkMode);
                    if (walkMode === Communicator.WalkMode.Keyboard) {
                        var rotationSpeed = parseInt($("#settings-walk-rotation").val(), 10);
                        var walkSpeed = parseFloat($("#settings-walk-speed").val()) * this._walkSpeedUnits;
                        var elevationSpeed = parseFloat($("#settings-walk-elevation").val()) * this._walkSpeedUnits;
                        var viewAngle = parseInt($("#settings-walk-view-angle").val(), 10);
                        var mouseLookEnabled = $("#settings-mouse-look-enabled").prop("checked");
                        var mouseLookSpeed = parseInt($("#settings-mouse-look-speed").val(), 10);
                        if (walkSpeed === 0) {
                            keyboardWalkOperator.resetDefaultWalkSpeeds().then(function () {
                                _this._readWalkSettingsHelper();
                            });
                        }
                        else {
                            keyboardWalkOperator.setRotationSpeed(rotationSpeed);
                            keyboardWalkOperator.setWalkSpeed(walkSpeed);
                            keyboardWalkOperator.setElevationSpeed(elevationSpeed);
                            keyboardWalkOperator.setViewAngle(viewAngle);
                            keyboardWalkOperator.setMouseLookEnabled(mouseLookEnabled);
                            keyboardWalkOperator.setMouseLookSpeed(mouseLookSpeed);
                        }
                    }
                };
                ViewerSettings.prototype._updateKeyboardWalkModeStyle = function (walkMode) {
                    this._updateEnabledStyle(null, ["walk-rotation-text", "walk-speed-text", "walk-elevation-text", "walk-view-angle-text", "walk-mouse-look-text", "settings-mouse-look-style"], ["settings-walk-rotation", "settings-walk-speed", "settings-walk-elevation", "settings-walk-view-angle", "settings-mouse-look-enabled", "settings-mouse-look-speed"], walkMode === Communicator.WalkMode.Keyboard);
                };
                ViewerSettings.prototype._updateWalkSpeedUnits = function (walkSpeed) {
                    var logWalkSpeed = Math.log(walkSpeed) / Math.LN10;
                    this._walkSpeedUnits = Math.pow(10, Math.floor(logWalkSpeed));
                    var units = "m";
                    if (this._walkSpeedUnits <= .001) {
                        units = "&micro;m";
                    }
                    else if (this._walkSpeedUnits <= 1) {
                        units = "mm";
                    }
                    else if (this._walkSpeedUnits <= 10) {
                        units = "cm";
                    }
                    else {
                        this._walkSpeedUnits = 1000; //meters
                    }
                    $("#walk-speed-units").html(units);
                    $("#elevation-speed-units").html(units);
                };
                ViewerSettings.prototype._readWalkSettingsHelper = function () {
                    var operatorManager = this._viewer.operatorManager;
                    var keyboardWalkOperator = operatorManager.getOperator(Communicator.OperatorId.KeyboardWalk);
                    var walkModeOperator = operatorManager.getOperator(Communicator.OperatorId.WalkMode);
                    var rotationSpeed = keyboardWalkOperator.getRotationSpeed();
                    var walkSpeed = keyboardWalkOperator.getWalkSpeed();
                    var elevationSpeed = keyboardWalkOperator.getElevationSpeed();
                    var viewAngle = keyboardWalkOperator.getViewAngle();
                    var mouseLookEnabled = keyboardWalkOperator.getMouseLookEnabled();
                    var mouseLookSpeed = keyboardWalkOperator.getMouseLookSpeed();
                    var walkMode = walkModeOperator.getWalkMode();
                    this._updateWalkSpeedUnits(walkSpeed);
                    $("#settings-walk-mode").val("" + walkMode);
                    $("#settings-walk-rotation").val("" + rotationSpeed);
                    $("#settings-walk-speed").val((walkSpeed / this._walkSpeedUnits).toFixed(1));
                    $("#settings-walk-elevation").val((elevationSpeed / this._walkSpeedUnits).toFixed(1));
                    $("#settings-walk-view-angle").val("" + viewAngle);
                    $("#settings-mouse-look-speed").val("" + mouseLookSpeed);
                    this._updateEnabledStyle("settings-mouse-look-enabled", ["settings-mouse-look-style"], ["settings-mouse-look-speed"], mouseLookEnabled);
                    this._updateKeyboardWalkModeStyle(walkMode);
                };
                ViewerSettings.prototype._readWalkSettings = function () {
                    var _this = this;
                    var operatorManager = this._viewer.operatorManager;
                    var keyboardWalkOperator = operatorManager.getOperator(Communicator.OperatorId.KeyboardWalk);
                    var walkSpeed = keyboardWalkOperator.getWalkSpeed();
                    if (walkSpeed === 0) {
                        keyboardWalkOperator.resetDefaultWalkSpeeds().then(function () {
                            _this._readWalkSettingsHelper();
                        });
                    }
                    else {
                        this._readWalkSettingsHelper();
                    }
                };
                ViewerSettings.prototype._colorFromRgbString = function (rgbStr) {
                    var rgb = rgbStr.replace(/[^\d,]/g, '').split(',');
                    return new Communicator.Color(Number(rgb[0]), Number(rgb[1]), Number(rgb[2]));
                };
                ViewerSettings.prototype._rgbStringFromColor = function (color) {
                    if (color == null) {
                        return "";
                    }
                    return "rgb(" + color.r + "," + color.g + "," + color.b + ")";
                };
                ViewerSettings.prototype._updateEnabledStyle = function (checkboxId, styleIds, propertyIds, enabled) {
                    if (checkboxId !== null) {
                        $("#" + checkboxId).prop("checked", enabled);
                    }
                    if (enabled) {
                        for (var _i = 0, styleIds_1 = styleIds; _i < styleIds_1.length; _i++) {
                            var styleId = styleIds_1[_i];
                            $("#" + styleId).removeClass("grayed-out");
                        }
                    }
                    else {
                        for (var _a = 0, styleIds_2 = styleIds; _a < styleIds_2.length; _a++) {
                            var styleId = styleIds_2[_a];
                            $("#" + styleId).addClass("grayed-out");
                        }
                    }
                    for (var _b = 0, propertyIds_1 = propertyIds; _b < propertyIds_1.length; _b++) {
                        var propertyId = propertyIds_1[_b];
                        $("#" + propertyId).prop("disabled", !enabled);
                    }
                };
                return ViewerSettings;
            }());
            Desktop.ViewerSettings = ViewerSettings;
        })(Desktop = Ui.Desktop || (Ui.Desktop = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Desktop;
        (function (Desktop) {
            var Tree;
            (function (Tree) {
                Tree[Tree["Model"] = 0] = "Model";
                Tree[Tree["CadView"] = 1] = "CadView";
                Tree[Tree["Sheets"] = 2] = "Sheets";
                Tree[Tree["Configurations"] = 3] = "Configurations";
            })(Tree = Desktop.Tree || (Desktop.Tree = {}));
            var ModelBrowser = /** @class */ (function () {
                function ModelBrowser(elementId, containerId, viewer, isolateZoomHelper, cuttingPlaneController, screenConfiguration) {
                    this._browserWindowMargin = 3;
                    this._scrollRefreshTimerId = null;
                    this._scrollRefreshTimestamp = 0;
                    this._scrollRefreshInterval = 300;
                    this._minimized = true;
                    this._elementId = elementId;
                    this._containerId = containerId;
                    this._viewer = viewer;
                    this._isolateZoomHelper = isolateZoomHelper;
                    this._cuttingPlaneController = cuttingPlaneController;
                    this._screenConfiguration = screenConfiguration;
                    this._canvasSize = this._viewer.view.getCanvasSize();
                    this._initElements();
                    this._initEvents();
                    this._minimizeModelBrowser(); // https://techsoft3d.atlassian.net/browse/COM-590
                }
                ModelBrowser.prototype._initEvents = function () {
                    var _this = this;
                    var onModel = function () {
                        _this._showTree(Tree.Model);
                        return Promise.resolve();
                    };
                    this._viewer.setCallbacks({
                        modelStructureLoadBegin: function () {
                            _this._onModelStructureLoadBegin();
                        },
                        modelStructureParseBegin: function () {
                            _this._onModelStructureParsingBegin();
                        },
                        _assemblyTreeReady: function () {
                            _this._onAssemblyTreeReady();
                            return Promise.resolve();
                        },
                        _firstModelLoaded: onModel,
                        _modelSwitched: onModel,
                        frameDrawn: function () {
                            _this._canvasSize = _this._viewer.view.getCanvasSize();
                            _this.onResize(_this._canvasSize.x, _this._canvasSize.y);
                        }
                    });
                    this._modelTree.registerCallback("expand", function () {
                        _this._refreshBrowserScroll();
                    });
                    this._modelTree.registerCallback("collapse", function () {
                        _this._refreshBrowserScroll();
                    });
                    this._modelTree.registerCallback("addChild", function () {
                        _this._refreshBrowserScroll();
                    });
                    $("#contextMenuButton").on("click", function (event) {
                        var position = new Communicator.Point2(event.clientX, event.clientY);
                        _this._viewer._getCallbackManager().trigger("contextMenu", position, Communicator.KeyModifiers.None);
                    });
                };
                ModelBrowser.prototype._refreshBrowserScroll = function () {
                    var _this = this;
                    var expectedTimestamp = ++this._scrollRefreshTimestamp;
                    if (this._scrollRefreshTimerId === null) {
                        this._scrollRefreshTimerId = setTimeout(function () {
                            _this._scrollRefreshTimerId = null;
                            if (_this._modelTreeScroll) {
                                _this._modelTreeScroll.refresh();
                            }
                            if (_this._cadViewTreeScroll) {
                                _this._cadViewTreeScroll.refresh();
                            }
                            if (_this._sheetsTreeScroll) {
                                _this._sheetsTreeScroll.refresh();
                            }
                            if (_this._configurationsTreeScroll) {
                                _this._configurationsTreeScroll.refresh();
                            }
                            if (expectedTimestamp !== _this._scrollRefreshTimestamp) {
                                _this._refreshBrowserScroll();
                            }
                        }, this._scrollRefreshInterval);
                    }
                };
                ModelBrowser.prototype._showTree = function (tree) {
                    var $modelTreeContainer = $("#" + this._modelTree.getElementId() + "ScrollContainer");
                    var $cadViewTreeContainer = $("#" + this._cadViewTree.getElementId() + "ScrollContainer");
                    var $sheetsTreeContainer = $("#" + this._sheetsTree.getElementId() + "ScrollContainer");
                    var $configurationsTreeContainer = $("#" + this._configurationsTree.getElementId() + "ScrollContainer");
                    var $browserTab = $("#" + this._modelTree.getElementId() + "Tab");
                    var $cadViewTab = $("#" + this._cadViewTree.getElementId() + "Tab");
                    var $sheetsTab = $("#" + this._sheetsTree.getElementId() + "Tab");
                    var $configurationsTab = $("#" + this._configurationsTree.getElementId() + "Tab");
                    if (tree !== Tree.Model) {
                        $modelTreeContainer.hide();
                        if ($browserTab) {
                            $browserTab.removeClass("browser-tab-selected");
                        }
                    }
                    if (tree !== Tree.CadView) {
                        $cadViewTreeContainer.hide();
                        if ($cadViewTab) {
                            $cadViewTab.removeClass("browser-tab-selected");
                        }
                    }
                    if (tree !== Tree.Sheets) {
                        $sheetsTreeContainer.hide();
                        if ($sheetsTab) {
                            $sheetsTab.removeClass("browser-tab-selected");
                        }
                    }
                    if (tree !== Tree.Configurations) {
                        $configurationsTreeContainer.hide();
                        if ($configurationsTab) {
                            $configurationsTab.removeClass("browser-tab-selected");
                        }
                    }
                    switch (tree) {
                        case Tree.Model:
                            $modelTreeContainer.show();
                            $browserTab.addClass("browser-tab-selected");
                            break;
                        case Tree.CadView:
                            $cadViewTreeContainer.show();
                            $cadViewTab.addClass("browser-tab-selected");
                            break;
                        case Tree.Sheets:
                            $sheetsTreeContainer.show();
                            $sheetsTab.addClass("browser-tab-selected");
                            break;
                        case Tree.Configurations:
                            $configurationsTreeContainer.show();
                            $configurationsTab.addClass("browser-tab-selected");
                            break;
                    }
                    this._refreshBrowserScroll();
                };
                ModelBrowser.prototype._initElements = function () {
                    var _this = this;
                    this._header = this._createHeader();
                    this._browserWindow = this._createBrowserWindow();
                    // property window html
                    this._createPropertyWindow();
                    $(this._browserWindow).resizable({
                        resize: function (event, ui) {
                            event;
                            _this.onResize(ui.size.width, ui.size.height);
                        },
                        minWidth: 35,
                        minHeight: 37,
                        handles: "e"
                    });
                    this._modelTree = new Ui.ModelTree("modelTree", this._viewer, this._modelTreeScroll);
                    this._contextMenu = new Desktop.ModelBrowserContextMenu(this._containerId, this._viewer, this._modelTree, this._isolateZoomHelper);
                    this._cadViewTree = new Ui.CadViewTree("cadViewTree", this._viewer, this._cuttingPlaneController);
                    this._sheetsTree = new Ui.SheetsTree("sheetsTree", this._viewer);
                    this._configurationsTree = new Ui.ConfigurationsTree("configurationsTree", this._viewer);
                };
                ModelBrowser.prototype._createBrowserWindow = function () {
                    var div = document.getElementById(this._elementId);
                    $(div).bind("touchmove", function (event) {
                        event.originalEvent.preventDefault();
                    });
                    div.classList.add("ui-modelbrowser-window");
                    div.classList.add("desktop-ui-window");
                    div.classList.add("ui-modelbrowser-small");
                    div.style.position = "absolute";
                    div.style.width = Math.max(($(window).width() / 4), 400) + "px";
                    div.style.top = this._browserWindowMargin + "px";
                    div.style.left = this._browserWindowMargin + "px";
                    div.appendChild(this._header);
                    return div;
                };
                ModelBrowser.prototype._createDiv = function (id, classList) {
                    var div = document.createElement("div");
                    div.id = id;
                    for (var _i = 0, classList_1 = classList; _i < classList_1.length; _i++) {
                        var clazz = classList_1[_i];
                        div.classList.add(clazz);
                    }
                    return div;
                };
                ModelBrowser.prototype._createHeader = function () {
                    var _this = this;
                    var div = this._createDiv("ui-modelbrowser-header", ["ui-modelbrowser-header", "desktop-ui-window-header"]);
                    var t = document.createElement("table");
                    var tr = document.createElement("tr");
                    t.appendChild(tr);
                    var minimizetd = document.createElement("td");
                    minimizetd.classList.add("ui-modelbrowser-minimizetd");
                    this._minimizeButton = this._createDiv("ui-modelbrowser-minimizebutton", ["ui-modelbrowser-minimizebutton", "minimized"]);
                    this._minimizeButton.onclick = function () {
                        _this._onMinimizeButtonClick();
                    };
                    minimizetd.appendChild(this._minimizeButton);
                    tr.appendChild(minimizetd);
                    // model browser label
                    var modelBrowserLabel = document.createElement("td");
                    modelBrowserLabel.id = "modelBrowserLabel";
                    modelBrowserLabel.innerHTML = ""; //"Model Browser";
                    tr.appendChild(modelBrowserLabel);
                    var menuNode = this._createDiv("contextMenuButton", ["ui-modeltree-icon", "menu"]);
                    tr.appendChild(menuNode);
                    div.appendChild(t);
                    this._content = this._createDiv("modelTreeContainer", ["ui-modelbrowser-content", "desktop-ui-window-content"]);
                    this._content.style.overflow = "auto";
                    var loadingDiv = this._createDiv("modelBrowserLoadingDiv", []);
                    loadingDiv.innerHTML = "Loading...";
                    this._content.appendChild(loadingDiv);
                    this._createIScrollWrapper("modelTree");
                    this._createIScrollWrapper("cadViewTree");
                    this._createIScrollWrapper("sheetsTree");
                    this._createIScrollWrapper("configurationsTree");
                    // tabs
                    this._modelBrowserTabs = this._createDiv("modelBrowserTabs", []);
                    this._createBrowserTab("modelTree", "Model Tree", true, Tree.Model);
                    this._createBrowserTab("cadViewTree", "Views, etc.", false, Tree.CadView);
                    this._createBrowserTab("sheetsTree", "Sheets", false, Tree.Sheets);
                    this._createBrowserTab("configurationsTree", "Configurations", false, Tree.Configurations);
                    div.appendChild(this._modelBrowserTabs);
                    return div;
                };
                ModelBrowser.prototype._createIScrollWrapper = function (id) {
                    // extra container wrapping the content of the model browser for touch scrolling
                    var divScrollContainer = this._createDiv(id + "ScrollContainer", []);
                    divScrollContainer.appendChild(this._createDiv(id, []));
                    this._content.appendChild(divScrollContainer);
                };
                ModelBrowser.prototype._createBrowserTab = function (id, name, selected, tree) {
                    var _this = this;
                    var tab = document.createElement("label");
                    tab.id = id + "Tab";
                    tab.textContent = name;
                    tab.classList.add("ui-modelbrowser-tab");
                    tab.classList.add("hidden");
                    if (selected) {
                        tab.classList.add("browser-tab-selected");
                    }
                    tab.onclick = function (event) {
                        event;
                        _this._showTree(tree);
                    };
                    this._modelBrowserTabs.appendChild(tab);
                    return tab;
                };
                ModelBrowser.prototype._initializeIScroll = function (id) {
                    var wrapper = $("#" + id + "ScrollContainer").get(0);
                    return new IScroll(wrapper, {
                        mouseWheel: true,
                        scrollbars: true,
                        interactiveScrollbars: true,
                        preventDefault: false
                    });
                };
                ModelBrowser.prototype._createPropertyWindow = function () {
                    var _this = this;
                    this._propertyWindow = document.createElement("div");
                    this._propertyWindow.classList.add("propertyWindow");
                    this._propertyWindow.id = "propertyWindow";
                    var container = document.createElement("div");
                    container.id = "propertyContainer";
                    this._propertyWindow.appendChild(container);
                    this._treePropertyContainer = document.createElement("div");
                    this._treePropertyContainer.id = "treePropertyContainer";
                    this._treePropertyContainer.appendChild(this._content);
                    this._treePropertyContainer.appendChild(this._propertyWindow);
                    this._browserWindow.appendChild(this._treePropertyContainer);
                    $(this._propertyWindow).resizable({
                        resize: function (event, ui) {
                            event;
                            _this.onResize(ui.size.width, _this._viewer.view.getCanvasSize().y);
                        },
                        handles: "n"
                    });
                };
                ModelBrowser.prototype._onMinimizeButtonClick = function () {
                    if (this._minimized === false) {
                        this._minimizeModelBrowser();
                    }
                    else {
                        this._maximizeModelBrowser();
                    }
                };
                ModelBrowser.prototype._maximizeModelBrowser = function () {
                    var _this = this;
                    this._minimized = false;
                    var $minimizeButton = jQuery(this._minimizeButton);
                    $minimizeButton.addClass("maximized");
                    $minimizeButton.removeClass("minimized");
                    jQuery(this._content).slideDown({
                        progress: function () {
                            _this._onSlide();
                            $("#modelBrowserWindow").removeClass("ui-modelbrowser-small");
                        },
                        complete: function () {
                            $(_this._browserWindow).children(".ui-resizable-handle").show();
                        }
                    });
                    this._refreshBrowserScroll();
                };
                ModelBrowser.prototype._minimizeModelBrowser = function () {
                    var _this = this;
                    this._minimized = true;
                    var $minimizeButton = jQuery(this._minimizeButton);
                    $minimizeButton.removeClass("maximized");
                    $minimizeButton.addClass("minimized");
                    jQuery(this._content).slideUp({
                        progress: function () {
                            _this._onSlide();
                            $("#modelBrowserWindow").addClass("ui-modelbrowser-small");
                        },
                        complete: function () {
                            $(_this._browserWindow).children(".ui-resizable-handle").hide();
                        }
                    });
                    this._refreshBrowserScroll();
                };
                ModelBrowser.prototype.onResize = function (width, height) {
                    width;
                    var $header = $(this._header);
                    var $propertyWindow = $(this._propertyWindow);
                    this._treePropertyContainer.style.height = height - $header.outerHeight() - this._browserWindowMargin * 2 + "px";
                    var contentHeight = height - $header.outerHeight() - $propertyWindow.outerHeight() - this._browserWindowMargin * 2;
                    this._browserWindow.style.height = height - this._browserWindowMargin * 2 + "px";
                    this._content.style.height = contentHeight + "px";
                    this._refreshBrowserScroll();
                };
                ModelBrowser.prototype._onSlide = function () {
                    var $header = $(this._header);
                    var $content = $(this._content);
                    var $propertyWindow = $(this._propertyWindow);
                    var browserWindowHeight = $content.outerHeight() + $header.outerHeight() + $propertyWindow.outerHeight();
                    this._browserWindow.style.height = browserWindowHeight + "px";
                };
                ModelBrowser.prototype._onModelStructureParsingBegin = function () {
                    var $loadingDiv = $("#modelBrowserLoadingDiv");
                    $loadingDiv.html("Parsing...");
                };
                ModelBrowser.prototype._onModelStructureLoadBegin = function () {
                    var $containerDiv = $("#" + this._elementId);
                    $containerDiv.show();
                };
                ModelBrowser.prototype._onAssemblyTreeReady = function () {
                    var $loadingDiv = $("#modelBrowserLoadingDiv");
                    $loadingDiv.remove();
                    this._showTree(Tree.Model);
                    this._modelTreeScroll = this._initializeIScroll(this._modelTree.getElementId());
                    this._cadViewTreeScroll = this._initializeIScroll(this._cadViewTree.getElementId());
                    this._sheetsTreeScroll = this._initializeIScroll(this._sheetsTree.getElementId());
                    this._configurationsTreeScroll = this._initializeIScroll(this._configurationsTree.getElementId());
                    var element = document.getElementById(this._elementId);
                    if (element !== null) {
                        var $modelBrowserWindow = $(element);
                        this.onResize($modelBrowserWindow.width(), $modelBrowserWindow.height());
                    }
                };
                ModelBrowser.prototype.freeze = function (freeze) {
                    this._modelTree.freezeExpansion(freeze);
                };
                ModelBrowser.prototype.enablePartSelection = function (enable) {
                    this._modelTree.enablePartSelection(enable);
                };
                ModelBrowser.prototype.updateSelection = function () {
                    this._modelTree.updateSelection();
                };
                return ModelBrowser;
            }());
            Desktop.ModelBrowser = ModelBrowser;
        })(Desktop = Ui.Desktop || (Ui.Desktop = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../Desktop/ModelBrowser.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Desktop;
        (function (Desktop) {
            var DesktopUi = /** @class */ (function () {
                function DesktopUi(viewer, screenConfiguration) {
                    if (screenConfiguration === void 0) { screenConfiguration = Communicator.ScreenConfiguration.Desktop; }
                    this._suppressMissingModelDialog = false;
                    this._viewer = viewer;
                    this._screenConfiguration = screenConfiguration;
                    this._initElements();
                    this._initEvents();
                }
                DesktopUi.prototype._initElements = function () {
                    this._cuttingPlaneController = new Ui.CuttingPlaneController(this._viewer);
                    var view = this._viewer.view;
                    var axisTriad = view.getAxisTriad();
                    var navCube = view.getNavCube();
                    if (this._screenConfiguration === Communicator.ScreenConfiguration.Mobile) {
                        axisTriad.setAnchor(Communicator.OverlayAnchor.UpperRightCorner);
                        navCube.setAnchor(Communicator.OverlayAnchor.UpperLeftCorner);
                    }
                    this._toolbar = new Ui.Toolbar(this._viewer, this._cuttingPlaneController, this._screenConfiguration);
                    this._toolbar.init();
                    // set handle size larger on mobile
                    var handleOperator = this._viewer.operatorManager.getOperator(Communicator.OperatorId.Handle);
                    if (handleOperator) {
                        handleOperator.setHandleSize(this._screenConfiguration === Communicator.ScreenConfiguration.Mobile ? 3 : 1);
                    }
                    var content = document.getElementById("content");
                    // prevent default right click menu
                    content.oncontextmenu = function () { return false; };
                    this._isolateZoomHelper = new Communicator.IsolateZoomHelper(this._viewer);
                    var modelBrowserDiv = document.createElement("div");
                    modelBrowserDiv.id = "modelBrowserWindow";
                    content.appendChild(modelBrowserDiv);
                    this._modelBrowser = new Desktop.ModelBrowser(modelBrowserDiv.id, content.id, this._viewer, this._isolateZoomHelper, this._cuttingPlaneController, this._screenConfiguration);
                    this._propertyWindow = new Desktop.PropertyWindow(this._viewer, this._isolateZoomHelper);
                    var streamingIndicatorDiv = document.createElement("div");
                    streamingIndicatorDiv.id = "streamingIndicator";
                    content.appendChild(streamingIndicatorDiv);
                    if (this._viewer.getRendererType() === Communicator.RendererType.Client)
                        this._streamingIndicator = new Ui.StreamingIndicator(streamingIndicatorDiv.id, this._viewer);
                    this._contextMenu = new Ui.RightClickContextMenu(content.id, this._viewer, this._isolateZoomHelper);
                    this._timeoutWarningDialog = new Ui.TimeoutWarningDialog(content.id, this._viewer);
                };
                DesktopUi.prototype._initEvents = function () {
                    var _this = this;
                    this._viewer.setCallbacks({
                        sceneReady: function () {
                            _this._onSceneReady();
                        },
                        _modelStructureHeaderParsed: function () {
                            return _this._updateDrawingsUI();
                        },
                        modelLoadFailure: function (modelName, reason) {
                            var errorDialog = new Ui.UiDialog("content");
                            errorDialog.setTitle("Model Load Error");
                            var text = "Unable to load ";
                            if (modelName === null) {
                                text += "model";
                            }
                            else {
                                text += "'" + modelName + "'";
                            }
                            text += ": " + reason;
                            errorDialog.setText(text);
                            errorDialog.show();
                        },
                        modelLoadBegin: function () {
                            _this._suppressMissingModelDialog = false;
                        },
                        missingModel: function (modelPath) {
                            if (!_this._suppressMissingModelDialog) {
                                _this._suppressMissingModelDialog = true;
                                var errorDialog = new Ui.UiDialog("content");
                                errorDialog.setTitle("Missing Model Error");
                                var text = "Unable to load ";
                                text += "'" + modelPath + "'";
                                errorDialog.setText(text);
                                errorDialog.show();
                            }
                        },
                        volumeSelectionBatchBegin: function () {
                            _this.freezeModelBrowser(true);
                            _this.enableModelBrowserPartSelection(false);
                        },
                        volumeSelectionBatchEnd: function () {
                            _this.freezeModelBrowser(false);
                            _this.enableModelBrowserPartSelection(true);
                        },
                        volumeSelectionEnd: function () {
                            _this._modelBrowser.updateSelection();
                        },
                        webGlContextLost: function () {
                            var errorDialog = new Ui.UiDialog("content");
                            errorDialog.setTitle("Fatal Error");
                            errorDialog.setText("WebGL context lost. Rendering cannot continue.");
                            errorDialog.show();
                        },
                        XHRonloadend: function (e, status, uri) {
                            e;
                            if (status === 404) {
                                var errorDialog = new Ui.UiDialog("content");
                                errorDialog.setTitle("404 Error");
                                errorDialog.setText("Unable to load " + uri);
                                errorDialog.show();
                            }
                        },
                        modelSwitched: function () {
                            _this._updateDrawingsUI();
                        }
                    });
                };
                DesktopUi.prototype._updateDrawingsUI = function () {
                    var view = this._viewer.view;
                    var axisTriad = view.getAxisTriad();
                    var navCube = view.getNavCube();
                    var ps = [];
                    if (this._viewer.model.isDrawing()) {
                        $("#cuttingplane-button").hide();
                        $("#explode-button").hide();
                        $("#view-button").hide();
                        $("#camera-button").hide();
                        $("#tool_separator_4").hide();
                        $("#tool_separator_1").hide();
                        $("#edgeface-button").hide();
                        this._toolbar.reposition();
                        $(".ui-modeltree").addClass("drawing");
                        ps.push(axisTriad.disable());
                        ps.push(navCube.disable());
                    }
                    else {
                        $("#cuttingplane-button").show();
                        $("#explode-button").show();
                        $("#view-button").show();
                        $("#camera-button").show();
                        $("#tool_separator_4").show();
                        $("#tool_separator_1").show();
                        $("#edgeface-button").show();
                        this._toolbar.reposition();
                        $(".ui-modeltree").removeClass("drawing");
                        ps.push(this._viewer.enableBackgroundSheet(false));
                        ps.push(axisTriad.enable());
                        ps.push(navCube.enable());
                    }
                    return Promise.all(ps);
                };
                DesktopUi.prototype._onSceneReady = function () {
                    var _this = this;
                    var ps = [];
                    this._viewer.focusInput(true);
                    var selectionManager = this._viewer.selectionManager;
                    ps.push(selectionManager.setNodeSelectionColor(DesktopUi._defaultPartSelectionColor));
                    ps.push(selectionManager.setNodeSelectionOutlineColor(DesktopUi._defaultPartSelectionOutlineColor));
                    var view = this._viewer.view;
                    ps.push(view.setXRayColor(Communicator.ElementType.Faces, DesktopUi._defaultXRayColor));
                    ps.push(view.setXRayColor(Communicator.ElementType.Lines, DesktopUi._defaultXRayColor));
                    ps.push(view.setXRayColor(Communicator.ElementType.Points, DesktopUi._defaultXRayColor));
                    ps.push(view.setBackgroundColor(DesktopUi._defaultBackgroundColor, DesktopUi._defaultBackgroundColor));
                    var canvas = this._viewer.getViewElement();
                    canvas.addEventListener("mouseenter", function () {
                        _this._viewer.focusInput(true);
                    });
                    return Promise.all(ps);
                };
                DesktopUi.prototype.setDeselectOnIsolate = function (deselect) {
                    this._isolateZoomHelper.setDeselectOnIsolate(deselect);
                };
                /* UI API functions */
                DesktopUi.prototype.freezeModelBrowser = function (freeze) {
                    this._modelBrowser.freeze(freeze);
                };
                DesktopUi.prototype.enableModelBrowserPartSelection = function (enable) {
                    this._modelBrowser.enablePartSelection(enable);
                };
                //private static _defaultBackgroundColor = new Color(192, 220, 248);
                DesktopUi._defaultBackgroundColor = Communicator.Color.white();
                DesktopUi._defaultPartSelectionColor = Communicator.Color.createFromFloat(0, 0.8, 0);
                DesktopUi._defaultPartSelectionOutlineColor = Communicator.Color.createFromFloat(0, 0.8, 0);
                DesktopUi._defaultXRayColor = Communicator.Color.createFromFloat(0, 0.9, 0);
                return DesktopUi;
            }());
            Desktop.DesktopUi = DesktopUi;
        })(Desktop = Ui.Desktop || (Ui.Desktop = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
/// <reference path="../js/hoops_web_viewer.d.ts"/>
var Communicator;
(function (Communicator) {
    var Ui;
    (function (Ui) {
        var Mobile;
        (function (Mobile) {
            var Ribbon = /** @class */ (function () {
                function Ribbon(elementId, viewer) {
                    this._sliderVisible = false;
                    this._elementId = elementId;
                    this._viewer = viewer;
                    this._initElements();
                    this._initEvents();
                }
                Ribbon.prototype._initEvents = function () {
                    var _this = this;
                    this._viewer.setCallbacks({
                        selectionArray: function () {
                            _this._onViewerSelection();
                        }
                    });
                    window.addEventListener("resize", function () {
                        _this._onOrientationChange;
                    });
                };
                Ribbon.prototype._onOrientationChange = function () {
                    var width = $(window).width();
                    var height = $(window).height();
                    if (width < height)
                        this._slidePanel.style.width = $(window).width() * 0.75 + "px";
                    else
                        this._slidePanel.style.width = $(window).width() * 0.3 + "px";
                };
                Ribbon.prototype._onViewerSelection = function () {
                    if (this._sliderVisible) {
                        this.hideSlider();
                    }
                };
                Ribbon.prototype.hideSlider = function () {
                    var $treeElement = $("#slider");
                    $treeElement.animate({
                        left: "-150%"
                    });
                    this._sliderVisible = false;
                };
                Ribbon.prototype.showSlider = function () {
                    var $treeElement = $("#slider");
                    $treeElement.animate({
                        left: "0%"
                    });
                    this._sliderVisible = true;
                };
                Ribbon.prototype._initElements = function () {
                    var _this = this;
                    var ribbon = document.getElementById("ribbon");
                    this._slidePanel = document.getElementById("slider");
                    this._slidePanel.style.position = "absolute";
                    this._onOrientationChange(); // set slider initial width
                    this._slidePanel.style.height = "100%";
                    this._slidePanel.style.background = "white";
                    this._slidePanel.style.overflow = "scroll";
                    var modelTreeDiv = document.createElement("div");
                    modelTreeDiv.id = "modelTree";
                    this._slidePanel.appendChild(modelTreeDiv);
                    this._modelTree = new Ui.ModelTree("modelTree", this._viewer);
                    this._modelTreeButton = document.createElement("div");
                    this._modelTreeButton.classList.add("ui-ribbon-modeltree");
                    this._modelTreeButton.onclick = function () {
                        _this._onModelTreeButtonClick();
                    };
                    if (ribbon !== null) {
                        ribbon.appendChild(this._modelTreeButton);
                    }
                    var $treeElement = $("#slider");
                    $treeElement.css("left", "-150%");
                };
                Ribbon.prototype._onModelTreeButtonClick = function () {
                    if (this._sliderVisible) {
                        this.hideSlider();
                    }
                    else {
                        this.showSlider();
                    }
                };
                return Ribbon;
            }());
            Mobile.Ribbon = Ribbon;
        })(Mobile = Ui.Mobile || (Ui.Mobile = {}));
    })(Ui = Communicator.Ui || (Communicator.Ui = {}));
})(Communicator || (Communicator = {}));
