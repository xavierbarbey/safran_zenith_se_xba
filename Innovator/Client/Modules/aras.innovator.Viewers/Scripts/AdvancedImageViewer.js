﻿VC.Utils.Page.LoadModules(['PdfTronClientViewer']);

dojo.setObject('VC.AdvancedImageViewer', (function() {
	var isPdfViewerSupported = (VC.Utils.isIE() && VC.Utils.isIE11()) || VC.Utils.isFirefox() || VC.Utils.isChrome();

	return dojo.declare('AdvancedImageViewer', PdfTronClientViewer, {

		constructor: function(args) {
			this.messageId = null;
			this.type = this.DialogManager.viewerType = 'advancedImage';
			this.args = args;
			this.serverMaxZoom = args.maxZoom;
			this.cData = args.cData;
		},

		initializeViewer: function() {
			var self = this;

			if (!isPdfViewerSupported) {
				return;
			}

			if (typeof (this.cData) === 'undefined' || this.cData === null || this.cData === '') {
				VC.Utils.AlertError(VC.Utils.GetResource('pdfv_license_is_invalid'));
				return;
			}
			var pdfInitOptions = {
				type: 'html5',
				documentType: 'pdf',
				enableAnnotations: false,
				hideAnnotationPanel: true,
				showToolbarControl: false,
				path: self.args.baseInnovatorUrl + 'Client/Modules/aras.innovator.Viewers/Scripts/3rdPartyLibs/pdftronwebviewer-v2.2/lib',
				pdfBackend: VC.Utils.Viewers.PDFViewerSettings.pdfBackend,
				preloadPDFWorker: false,
				useDownloader: false
			};
			this.viewer = new VC.WebViewer.PdfTronWrapper(pdfInitOptions, this.viewerContainer);

			$(this.viewerContainer).on('ready', dojo.hitch(this, this.onViewerReady));
			$(this.viewerContainer).on('documentLoaded', dojo.hitch(this, this.onViewerLoaded));
			$(this.viewerContainer).on('pageChanged', dojo.hitch(this, this.onPageChangeEventHandler));
			$(this.viewerContainer).on('zoomChanged', dojo.hitch(this, this.onZoomChangeEventHandler));

			this.hidden = false;
			this.initLoadingProgress(0);
			this.systemDPI = VC.Utils.getXYDPIs()[0];

			var params = {};
			params.viewStateData = self.viewStateData;
			params.docViewerEl = self.docViewerEl;
			this.onHandleScrollStateEvent(params);
		},

		_loadFile: function() {
			if (!isPdfViewerSupported) {
				return;
			}
			if (this.isViewerInitialized) {
				this.viewer.loadDocument(this.args.baseInnovatorUrl + 'Client/HttpHandlers/ConvertPdfToXOD.ashx?file=' +
						encodeURIComponent(this.fileUrl) + VC.Utils.Enums.PdfHandlerActions.ImageToPdf);
			}
		},

		loadViewerToolbar: function() {
			var self = this;

			this.toolbarContainer.createViewToolbar(VC.Utils.Enums.TNames.AdvancedImageViewerToolbar);
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnPageUpClick, dojo.hitch(self, self.PageUpClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnPageDownClick, dojo.hitch(self, self.PageDownClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.ntbPageNumberChange, dojo.hitch(self, self.PageNumberChange));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnZoomWindowClick, dojo.hitch(self, self.ZoomWindowClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnZoomDownClick, dojo.hitch(self, self.ZoomOut));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnZoomUpClick, dojo.hitch(self, self.ZoomIn));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.tbxZoomPercentageChange, dojo.hitch(self, self.ZoomPercentChange));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnFitWidthClick, dojo.hitch(self, self.FitWidth));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnFitHeightClick, dojo.hitch(self, self.FitHeight));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnMeasureClick, dojo.hitch(self, self.MeasureClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnCompareClick, dojo.hitch(self, self.CompareClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnDownloadClick, dojo.hitch(self, self.DownloadClick));
			this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnViewFileClick, dojo.hitch(self, self.PrintClick));

			this.bindDialogClick(this.toolbarContainer.viewToolbar.btnMeasure, VC.Utils.Enums.Dialogs.Measurement);
			this.bindDialogClick(this.toolbarContainer.viewToolbar.btnCompare, VC.Utils.Enums.Dialogs.CompareFiles);
		},

		PrintClick: function() {
			VC.Utils.Page.LoadModules(['Managers/PrintManager']);
			var newFileUrl = aras.IomInnovator.getFileUrl(this.args.fileId, aras.Enums.UrlType.SecurityToken);
			var fileUrl = this.args.baseInnovatorUrl + 'Client/HttpHandlers/ConvertPdfToXOD.ashx?file=' +
				encodeURIComponent(newFileUrl) + VC.Utils.Enums.PdfHandlerActions.ImageToPdf;
			VC.PrintManager.openPDFFile(fileUrl, VC.PrintManager.printFile);
		},

		getViewerType: function() {
			return VC.Utils.Enums.MeasurementUtils.AdvancedImageViewer;
		}
	});
})());

