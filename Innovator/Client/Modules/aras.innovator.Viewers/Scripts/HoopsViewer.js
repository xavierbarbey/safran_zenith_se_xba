﻿VC.Utils.Page.LoadModules(['Viewer', 'TreeNodeIdToModelNodeIdDictionary', 'FunctionExecutionLimiter', 'AxisTriad']);
VC.Utils.Page.LoadWidgets(['ContextMenu/RightClickContextMenu']);

require([
	'dojo/aspect',
	'dojo/_base/connect',
	'dijit/layout/ContentPane',
	'dijit/layout/BorderContainer',
	'dojo/_base/declare',
	'dojo/_base/lang'],
	function(aspect, connect, ContentPane, BorderContainer, declare, lang) {
		return dojo.setObject('VC.HoopsViewer', (function() {
			var directionDuration = 500;
			var isHoopsViewerSupported = (VC.Utils.isIE() && VC.Utils.isIE11()) || VC.Utils.isFirefox() || VC.Utils.isChrome();
			var currMeasureOperation = 'Select';

			return declare('HoopsViewer', Viewer, {
				type: '3DViewer',
				isStarted: false,
				legacyModelType: false,
				scsFileBackgroundColor: null,
				legacyFileBackgroundColorTop: null,
				legacyFileBackgroundColorBottom: null,
				defaultCappingColor: null,
				defaultCappingGeometryVisibility: true,
				treeNodeIdToModelNodeIdDictionary: null,
				hoopsContainer: null,
				content: null, // container with 'model browser left panel' and 'viewerContainer'
				resolution: 'AsSaved',
				measurementMarkup: null, // JSON Array with measurement markup
				annotationLabelSuffix: '# Annotation View',
				modelXml: null,
				contextMenu: null,

				constructor: function(args) {
					var self = this;

					this.type = 'hoops';

					this.scsFileBackgroundColor = new Communicator.Color(255, 255, 255);
					this.legacyFileBackgroundColorTop = new Communicator.Color(0, 82, 189);
					this.legacyFileBackgroundColorBottom = new Communicator.Color(217, 217, 217);
					this.defaultCappingColor = new Communicator.Color(0, 255, 0);
				},

				// sets position of 'viewerContainer' when 'model broswer left panel' are shown or hidden
				adjustViewerContainerStyles: function(shownLeftPanel) {
					var modelBrowser = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);

					this.viewerContainer.style.top = VC.Widgets.Moveable.topLimiter + 'px';
					this.setSplitterVisibility(shownLeftPanel);

					if (shownLeftPanel) {
						this.viewerContainer.style.left = modelBrowser.width + 'px';
						this.viewerContainer.style.borderLeft = '1px solid #b5bcc7';
					} else {
						this.viewerContainer.style.left = '0px';
						this.viewerContainer.style.borderLeftWidth = '0px';
					}

					this.resizeContent();
				},

				resizeContent: function() {
					this.content.resize();
					this.viewer.resizeCanvas();
				},

				setSplitterVisibility: function(isVisible) {
					var splitter = this.content.getSplitter('left');
					splitter.domNode.style.display = isVisible ? '' : 'none';
				},

				setViewerCallbacks: function() {
					var self = this;

					this.viewer.setCallbacks({
						modelStructureParseBegin: function() {
							self.setLoadingProgress(25);
						},

						sceneReady: function() {
							self.setLoadingProgress(30);
						},

						modelStructureReady: function() {
							if (self.legacyModelType) {
								importer = new Communicator.HWF.Importer(self.viewer);
								importer.import({
									url: self.fileUrl
								});
							} else {
								self.createAxisTriad();
								self.setLoadingProgress(101);
								self.onViewerLoaded();
							}

							var modelProwserPanel = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
							modelProwserPanel.viewsTab.loadViews();
						},

						hwfParseComplete: function() {
							self.setLoadingProgress(101);
							self.onViewerLoaded();
						},

						selectionArray: function(selectionEvents) {
							var selectionEvent = null;
							if (selectionEvents && selectionEvents[0]) {
								selectionEvent = selectionEvents[0];
							}
							if (VC.Utils.isNotNullOrUndefined(selectionEvent)) {
								var modelBrowser = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);

								if (!modelBrowser.modelTab.isSelectionFromTree) {
									var selection = selectionEvent.getSelection();
									var treeNodeId = self.getTreeNodeFromSelectedNode(selection);
									if (treeNodeId) {
										modelBrowser.modelTab.selectTreeItem(treeNodeId);
									}
								}

								// "Orient To Face" button's state depends on the current selection's type
								self.setBtnOrientToFaceAccessibility(selectionEvent);
							}
						},

						measurementCreated: function(measurement) {
							var measureDialog = self.DialogManager.getExistingDialog(VC.Utils.Enums.Dialogs.Measure);

							if (measureDialog) {
								measureDialog.btnDeleteAll.Enable();
							}
						}
					});
				},

				createViewerContextMenu: function() {
					var self = this;

					var hiddenItems = [];
					if (this.legacyModelType) {
						hiddenItems.push('open');
					}
					self.contextMenu = new VC.Viewers.RightClickContextMenu(this.viewerContainer.id, this.viewer, hiddenItems);

					self.contextMenu.onOpenCad = function(selectedNode) {
						if (selectedNode) {
							var treeNodeId = self.getTreeNodeFromSelectedNode(selectedNode);
							var cadId = self.treeNodeIdToModelNodeIdDictionary.getCadId('treeNodeId', treeNodeId);
							self.openCadDocument(cadId);
						}
					};

					self.contextMenu.onIsolate = function(selectedNode) {
						if (selectedNode) {
							var selectedNodeID = self.getSelectedNodeID(selectedNode);
							self.isolateSelectedNodes([selectedNodeID]);
						}
					};

					self.contextMenu.onFitAll = function() {
						self.fitAllNodes();
					};

					self.contextMenu.onResetView = function() {
						self.resetView();
					};

					self.contextMenu.onVisibility = function(selectedNode) {
						if (selectedNode) {
							var selectedNodeID = self.getSelectedNodeID(selectedNode);
							self.setVisibilitySelectedNodes([selectedNodeID]);
						}
					};

					self.contextMenu.onHideAllOther = function(selectedNode) {
						if (selectedNode) {
							var selectedNodeID = self.getSelectedNodeID(selectedNode);
							self.hideAllOtherNodes([selectedNodeID]);
						}
					};

					self.contextMenu.onDisplayAll = function() {
						self.displayAllNodes();
					};
				},

				getFileUrlExtension: function() {
					var self = this;

					var fileId = /fileID=([^&]+)/ig.exec(self.fileUrl)[1];
					var fileName = /fileName=([^&]+)/ig.exec(self.fileUrl)[1];
					var modelPath = fileId.substring(0, 1) + '/' + fileId.substring(1, 3) + '/' +
						fileId.substr(3, fileId.length - 3) + '/' + fileName;
					var ind = fileName.lastIndexOf('.');
					return fileName.substring(ind + 1).toUpperCase();
				},

				initializeViewer: function(args) {
					var self = this;
					this.treeNodeIdToModelNodeIdDictionary = new VC.Viewers.TreeNodeIdToModelNodeIdDictionary();

					if (isHoopsViewerSupported) {
						var fileExt = this.getFileUrlExtension();

						if (fileExt === 'HWF') {
							this.legacyModelType = true;
						}

						if (this.legacyModelType) {
							this.viewer = new Communicator.WebViewer({
								containerId: this.viewerContainer.id,
								empty: true
							});
						} else {
							this.viewer = new Communicator.WebViewer({
								containerId: this.viewerContainer.id,
								endpointUri: self.fileUrl,     //server URL to scs file
								model: '',
								rendererType: Communicator.RendererType.Client,
								streamingMode: Communicator.StreamingMode.Interactive
							});

							this.viewer.start();
							this.isStarted = true;
						}

						this.setViewerCallbacks();

						var splitter = this.content.getSplitter('left');
						var limiter = new VC.FunctionExecutionLimiter();

						connect.connect(splitter, 'onMouseMove', this.viewer, function() {
							limiter.execute(self.viewer.resizeCanvas.apply(this));
						});

						VC.Utils.Page.LoadModules(['Controls/CuttingPlaneControl']);

						this.createViewerContextMenu();

						this.cuttingPlaneControl = new VC.Widgets.CuttingPlaneControl(new Communicator.Ui.CuttingPlaneController(this.viewer));
						var zoomOperator = this.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Zoom);
						zoomOperator.setZoomToMousePosition(false);
					}
					this.isViewerInitialized = true;
				},

				disableMultiplePartSelection: function() {
					var self = this;

					var _inputMonitorMouseUp = this.viewer._inputMonitor._processMouseUp;
					if (_inputMonitorMouseUp) {
						this.viewer._inputMonitor._processMouseUp = function(mouseEvent) {
							if (mouseEvent.button === Communicator.Button.Left) {
								self.viewer.getSelectionManager().clear();
							}

							_inputMonitorMouseUp.apply(self.viewer._inputMonitor, arguments);
						};
					}

					//Hoops-specific implementation for IE/EDGE
					var _inputMonitorPointerUp = this.viewer._inputMonitor._processPointerUp;
					if (_inputMonitorPointerUp) {
						this.viewer._inputMonitor._processPointerUp = function(mouseEvent) {
							if (mouseEvent.button === Communicator.Button.Left) {
								self.viewer.getSelectionManager().clear();
							}

							_inputMonitorPointerUp.apply(self.viewer._inputMonitor, arguments);
						};
					}
				},

				createAxisTriad: function() {
					this._triad = new VC.AxisTriad(this.viewer);

					this.resizeContent();
				},

				setNodesVisibility: function(nodeIds, visibility) {
					this.viewer.getModel().setNodesVisibility(nodeIds, visibility);
					this.viewer.getSelectionManager().clear();
				},

				openCadDocument: function(cadId) {
					aras.uiShowItem('CAD', cadId, 'tab view');
				},

				isolateSelectedNodes: function(selectedNodeIDs) {
					this.viewer.getView().fitNodes(selectedNodeIDs);
					this.setModelTransparency(0.1, this.viewer.getModel().getAbsoluteRootNode(), selectedNodeIDs);
					this.viewer.getSelectionManager().clear();
				},

				setVisibilitySelectedNodes: function(selectedNodeIDs) {
					this.viewer.getSelectionManager().clear();

					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					for (var i = 0; i < selectedNodeIDs.length; i++) {
						var treeNodeId = this.getTreeNodeIdByNodeId(selectedNodeIDs[i]);
						if (this.viewer.getModel().getNodeVisibility(selectedNodeIDs[i])) {
							this.viewer.getModel().setNodesVisibility([selectedNodeIDs[i]], false);
							modelBrowserPanel.modelTab.setNodeDisabled(treeNodeId);
						} else {
							this.viewer.getModel().setNodesVisibility([selectedNodeIDs[i]], true);
							modelBrowserPanel.modelTab.setNodeEnabled(treeNodeId);
						}
					}
				},

				hideAllOtherNodes: function(selectedNodeIDs) {
					this.viewer.getView().isolateNodes(selectedNodeIDs);
					this.viewer.getSelectionManager().clear();

					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					for (var i = 0; i < selectedNodeIDs.length; i++) {
						var treeNodeId = this.getTreeNodeIdByNodeId(selectedNodeIDs[i]);
						modelBrowserPanel.modelTab.setNodesDisabled(treeNodeId);
						modelBrowserPanel.modelTab.setNodeEnabled(treeNodeId);
					}
				},

				fitAllNodes: function(selectedNode) {
					var fitNodesArray = [];
					this.getNodesToFit(this.viewer.getModel().getAbsoluteRootNode(), fitNodesArray);
					if (fitNodesArray.length !== 0) {
						this.viewer.getView().fitWorld();
					}
				},

				resetView: function() {
					// "Reset View" command also should cancel active "Cross Section" dialog
					var curDialog = this.DialogManager.getExistingDialog(VC.Utils.Enums.Dialogs.CrossSection);
					if (curDialog) {
						curDialog.onCrossClick();
					}

					// "Reset View" command also should cancel active "Exploded View" dialog
					curDialog = this.DialogManager.getExistingDialog(VC.Utils.Enums.Dialogs.Exploded);
					if (curDialog) {
						curDialog.onCrossClick();
					}

					this.DialogManager.closeAllDialogsButThis(VC.Utils.Enums.Dialogs.ModelBrowser);
					this.viewer.getModel().resetModelTransparency();
					this.viewer.reset();

					// Switch to the initial view of component geometry from activated CAD View
					var handleOperator = this.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Handle);
					handleOperator.removeHandles();

					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					modelBrowserPanel.modelTab.setNodesEnabled();
				},

				displayAllNodes: function() {
					this.viewer.getModel().resetNodesVisibility();
					this.viewer.getModel().resetModelTransparency();
					this.viewer.getSelectionManager().clear();

					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					modelBrowserPanel.modelTab.setNodesEnabled();
				},

				getNodesToFit: function(parentNode, fitNodesArray) {
					var model = this.viewer.getModel();
					var childs = model.getNodeChildren(parentNode);
					var nodeType;
					for (var i = 0; i < childs.length; i++) {
						if (model.getNodeVisibility(childs[i]) &&
							model.getNodeType(childs[i]) !== Communicator.NodeType.AssemblyNode) {

							fitNodesArray.push(childs[i]);
						}
						this.getNodesToFit(childs[i], fitNodesArray);
					}
				},

				setModelTransparency: function(transparency, parentNode, selectedNodeIds) {
					var model = this.viewer.getModel();
					var childs = model.getNodeChildren(parentNode);
					for (var i = 0; i < childs.length; i++) {
						if (selectedNodeIds.indexOf(childs[i]) >= 0) {
							model.setNodesTransparency([childs[i]], 1);
						} else {
							model.setNodesTransparency([childs[i]], transparency);
							this.setModelTransparency(transparency, childs[i], selectedNodeIds);
						}
					}
				},

				getSelectedNodeID: function(selection) {
					if (selection) {
						var selectionType = selection.getSelectionType();
						if (selectionType !== Communicator.SelectionType.None) {
							let selectedNodeID = selection.getNodeId();
							const model = this.viewer.getModel();

							if (model.getNodeType(selectedNodeID) == Communicator.NodeType.BodyInstance) {
								selectedNodeID = model.getNodeParent(selectedNodeID);
							}
							return selectedNodeID;
						}
					}
				},

				//function returns cadID by nodeID or parentNodeId depending on the node type
				//if node has BodyInstance type (node is a instanced mesh/body), parentNodeID is used, otherwise, nodeID
				getTreeNodeIdByNodeId: function(nodeId) {
					var model = this.viewer.getModel();
					if (model.getNodeType(nodeId) === Communicator.NodeType.BodyInstance) {
						nodeId = model.getNodeParent(nodeId);
					}

					return this.treeNodeIdToModelNodeIdDictionary.getTreeNodeId(nodeId);
				},

				getTreeNodeFromSelectedNode: function(selection) {
					var selectedNodeID = this.getSelectedNodeID(selection);
					if (selectedNodeID) {
						return this.getTreeNodeIdByNodeId(selectedNodeID);
					}
				},

				getCadIdFromModel: function(nodeId, callback) {
					if (nodeId) {
						var model = this.viewer.getModel();
						model.getNodeProperties(nodeId).then(function(props) {
							if (props) {
								if (props.hasOwnProperty('CAD ID')) {
									var selectedCadID = props['CAD ID'];
									if (callback) {
										callback(nodeId, selectedCadID);
									}
								}
							}
						});
					}
				},

				getMonolithicModelXML: function(resolution) {
					var body = '<root_cad_id>' + this.args.markupHolderId + '</root_cad_id>' +
										'<resolution>' + resolution + '</resolution>';
					var inn = aras.newIOMInnovator();
					var results = inn.applyMethod('CreateMonolithicModelXML', body);

					if (!results.isError()) {
						this.modelXml = results.getResult();
					}
				},

				setBtnOrientToFaceAccessibility: function(event) {
					if (this.viewerToolbar.btnOrientToFace) {
						var selectionItem = null;
						if (event) {
							selectionItem = event.getSelection();
						} else if (this.viewer) {
							selectionItem = this.viewer.getSelectionManager().getLast();
						}

						if (selectionItem && selectionItem.getFaceEntity()) {
							this.viewerToolbar.btnOrientToFace.Enable();
						} else {
							this.viewerToolbar.btnOrientToFace.Disable();
						}
					}
				},

				onViewerLoaded: function() {
					this.inherited(arguments);

					this.disableMultiplePartSelection();

					if (this.legacyModelType) {
						this.viewer.getView().setBackgroundColor(this.legacyFileBackgroundColorTop, this.legacyFileBackgroundColorBottom);

						// For some reason size of HWF model is bigger than WebViewer screen
						// It was decided to set "IsoView" direction to decrease initial zoom for HWF models
						this.setViewDirection('IsoView', directionDuration);
					} else {
						this.viewer.getView().setBackgroundColor(this.scsFileBackgroundColor, this.scsFileBackgroundColor);
					}

					var cuttingManager = this.viewer.getCuttingManager();
					cuttingManager.setCappingFaceColor(this.defaultCappingColor);
					cuttingManager.setCappingLineColor(this.defaultCappingColor);
					cuttingManager.setCappingGeometryVisibility(this.defaultCappingGeometryVisibility);

					this.viewer.getView().getHiddenLineSettings().setObscuredLineTransparency(0);

					this.applyViewStateData();
					this.restoreViewerState();
				},

				loadViewerToolbar: function() {
					var self = this;

					this.toolbarContainer.createViewToolbar(VC.Utils.Enums.TNames.HoopsViewerToolbar);
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnZoomUpClick, lang.hitch(self, self.zoom, -2));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnZoomDownClick, lang.hitch(self, self.zoom, +2));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnResetViewClick, lang.hitch(self, self.resetCamera));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnOrientToFaceClick, lang.hitch(self, self.orientToFaceClick));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnDisplayStyleClick, lang.hitch(self, self.displayStyleClick));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnMeasureClick, lang.hitch(self, self.measureClick));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnExplodedViewClick, lang.hitch(self, self.explodedViewClick));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnCrossSectionClick, lang.hitch(self, self.crossSectionClick));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnModelBrowserClick, lang.hitch(self, self.showModelBrowser));
					this.toolbarContainer.viewToolbar.bindButtonBehaviour(VC.Utils.Enums.TButtonEvents.btnPMIClick, lang.hitch(self, self.togglePMIs));

					this.bindDialogClick(this.toolbarContainer.viewToolbar.btnDisplayStyle, VC.Utils.Enums.Dialogs.DisplayStyle);
					this.bindDialogClick(this.toolbarContainer.viewToolbar.btnMeasure, VC.Utils.Enums.Dialogs.Measure);
					this.bindDialogClick(this.toolbarContainer.viewToolbar.btnExplodedView, VC.Utils.Enums.Dialogs.Exploded);
					this.bindDialogClick(this.toolbarContainer.viewToolbar.btnCrossSection, VC.Utils.Enums.Dialogs.CrossSection);
					this.bindDialogClick(this.toolbarContainer.viewToolbar.btnModelBrowser, VC.Utils.Enums.Dialogs.ModelBrowser);

					// "Orient To Face" button should be initially disabled because there is no selections
					this.setBtnOrientToFaceAccessibility();
				},

				_loadFile: function() {
					if (!isHoopsViewerSupported) {
						return;
					} else {
						if (!this.isStarted) {
							//this.viewer.setFilename(this.fileUrl);
							this.viewer.start();
							this.isStarted = true;
						}
					}
				},

				_toViewMode: function() {
					this.hideSnapshot();
					this.showViewer();
					this.cleanMarkupPage();
					this.ToolbarManager.currentContainer.closeAllPalettesButThis('');
					if (this.toolbarContainer.viewToolbar) {
						this.toolbarContainer.btnView.select();
						this.toolbarContainer.activeMode = this.mode;
						this.toolbarContainer.markupToolbar.hide();
					}

					if (this.onViewModeActivate) {
						this.onViewModeActivate();
					}

					this.onUpdateTearOffMenu();
				},

				_toMarkupMode: function() {
					this.showSnapshot();
					this.hideViewer();
					this.DialogManager.closeAllDialogsButThis(VC.Utils.Enums.Dialogs.ModelBrowser);
					if (this.toolbarContainer.viewToolbar) {
						this.toolbarContainer.btnMarkup.select();
						this.toolbarContainer.activeMode = this.mode;
						this.toolbarContainer.viewToolbar.hide();
					}

					if (this.onMarkupModeActivate) {
						this.onMarkupModeActivate();
					}
					this.onUpdateTearOffMenu();
				},

				displayMarkup: function(imgUrl) {
					this.inherited(arguments);
					this.DialogManager.needRestoreMeasureDialog = false;
				},

				loadContainer: function() {
					VC.Utils.Page.LoadModules(['Widgets/HoopsViewerContainer']);

					var self = this;
					var htmlContainer = new VC.Widgets.ViewerContainer();
					htmlContainer.placeAt(document.body);

					this.content = new BorderContainer({
						design: 'headline',
						gutters: false,
						style: 'height: 100%; z-index: 999'
					}, htmlContainer.OverflowContainer);

					var viewerContainerPane = new ContentPane({
						region: 'center',
						splitter: true,
						style: 'z-index: 999; padding: 0'
					}, htmlContainer.ViewerContainer);
					this.DialogManager.viewerType = this.type;

					var args = {
						cadId: this.args.markupHolderId,
						resolution: this.resolution
					};
					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser, args);

					this.content.addChild(modelBrowserPanel);
					this.content.startup();
					this.assignModelBrowserHandlers(modelBrowserPanel);
					this.setSplitterVisibility(modelBrowserPanel.isOpened);
					this.content.resize();

					return htmlContainer;
				},

				loadDictionaryAndTree: function() {
					var self = this;
					var modelBrowserPanel = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);

					self.treeNodeIdToModelNodeIdDictionary.clearDictinary();
					if (self.legacyModelType) {
						var itemWindow = self.args.itemWindow;
						var nodeProperties = {
							'treeNodeId': '1',
							'itemId': itemWindow.itemID,
							'itemName': itemWindow.item.selectSingleNode('./keyed_name').textContent
						};
						self.treeNodeIdToModelNodeIdDictionary.addPair(undefined, nodeProperties.treeNodeId, nodeProperties.itemID);
						modelBrowserPanel.modelTab.loadTreeDataForHWF(nodeProperties);
					} else {
						self.getMonolithicModelXML('Current');
						modelBrowserPanel.modelTab.loadTreeData(self.modelXml);
						modelBrowserPanel.modelTab.fillTreeNodeIdToModelNodeIdDictionary(self.treeNodeIdToModelNodeIdDictionary);
					}
				},

				showSnapshot: function() {
					if (isHoopsViewerSupported || this.snapshotUrl) {
						this.inherited(arguments);
					} else {
						VC.Utils.AlertError(VC.Utils.GetResource('snapshot_can_not_be_created'));
						return;
					}
				},

				showViewer: function() {
					if (!isHoopsViewerSupported) {
						VC.Utils.AlertError(VC.Utils.GetResource('browser_is_not_supported'));
						return;
					}

					this.inherited(arguments);
				},

				hideViewer: function() {
					// We can not set a display:none for iframe before snapshot creation due to the bug in Firefox below
					// Bug 941146 - NS_ERROR_FAILURE when other browsers work fine when setting font on a canvas context in a display:none iframe
					//			if (viewerContainer && isHoopsViewerSupported)
					//this.viewerContainer.style.display = "none";
				},

				allowSwitchToViewerMode: function() {
					if (!isHoopsViewerSupported) {
						VC.Utils.AlertError(VC.Utils.GetResource('browser_is_not_supported'));
						return false;
					} else {
						return true;
					}
				},

				applyViewStateData: function() {
					if (!this.viewer || this.viewStateData.isEmpty()) {
						return;
					}
					var cameraStr = this.viewStateData.getValue('camera');
					if (cameraStr !== '') {
						this.viewer.getView().setCamera(new Communicator.Camera.fromJson(JSON.parse(cameraStr)));
					}

					var processVisibleItemsFuncWithParam = null;
					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);

					modelBrowserPanel.modelTab.setNodesEnabled();
					this.viewer.getModel().resetNodesVisibility();

					var hiddenItemsStr = this.viewStateData.getValue('hiddenItemIds');

					if (hiddenItemsStr !== '') {
						var hiddenItemIds = JSON.parse(hiddenItemsStr);
						var processVisibleItemsFunction = lang.hitch(this, 'processHiddenItemsFromViewState');
						processVisibleItemsFuncWithParam = lang.partial(processVisibleItemsFunction, modelBrowserPanel.modelTab, hiddenItemIds);

						if (modelBrowserPanel.modelTab.isTreeReady) {
							processVisibleItemsFuncWithParam();
						} else {
							modelBrowserPanel.modelTab.onAllNodesRendered = processVisibleItemsFuncWithParam;
						}
					}
				},

				processHiddenItemsFromViewState: function(modelTab, hiddenItemIds) {
					if (hiddenItemIds.length > 0) {
						this.viewer.getModel().setNodesVisibility(hiddenItemIds.map(Number), false);
						modelTab.setNodesDisabledByModelNodeIds(hiddenItemIds, this.treeNodeIdToModelNodeIdDictionary);
					}
				},

				restoreViewerState: function() {
					this.restoreModelBrowserButtonState();

					if (this.viewerToolbar.btnPMI) {
						const cadViewId = this.getActivatedCADViewID();

						if (cadViewId) {
							var pMIs = this.viewer.getModel().getCadViewPmis(cadViewId);
							if (Object.keys(pMIs).length > 0) {
								this.restorePmiButtonPressedState();
								this.viewerToolbar.btnPMI.onClick();
							} else {
								this.viewerToolbar.btnPMI.Disable();
							}
						} else {
							this.viewerToolbar.btnPMI.Disable();
						}
					}
				},

				restoreModelBrowserButtonState: function() {
					if (this.viewerToolbar.btnModelBrowser) {
						const curState = this.restoreViewerParameter(this.type + '.' + VC.Utils.Enums.Dialogs.ModelBrowser);
						if (curState !== null && curState.localeCompare('On') === 0) {
							this.viewerToolbar.btnModelBrowser.onClick();
						}
					}
				},

				restorePmiButtonPressedState: function() {
					if (this.viewerToolbar.btnPMI) {
						const curState = this.restoreViewerParameter(this.type + '.' + VC.Utils.Enums.TButtonEvents.btnPMIClick);
						if (curState !== null && curState.localeCompare('On') === 0) {
							this.viewerToolbar.btnPMI.SetPressedState(true);
						} else {
							this.viewerToolbar.btnPMI.SetPressedState(false);
						}
					}
				},

				saveCameraToViewState: function() {
					var cameraString = JSON.stringify(this.viewer.getView().getCamera().toJson());
					this.viewStateData.updateValue('camera', cameraString);
				},

				saveHiddenItemsToViewState: function() {
					var modelBrowserPanel = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					var modelNodeIdsOfHiddenTreeNodes = modelBrowserPanel.modelTab.getModelNodeIdsOfHiddenTreeNodes();
					var hiddenModelNodeIds = [];
					for (var i = 0; i < modelNodeIdsOfHiddenTreeNodes.length; i++) {
						if (!this.viewer.getModel().getNodeVisibility(modelNodeIdsOfHiddenTreeNodes[i])) {
							hiddenModelNodeIds.push(modelNodeIdsOfHiddenTreeNodes[i]);
						}
					}

					this.viewStateData.updateValue('hiddenItemIds', JSON.stringify(hiddenModelNodeIds));
				},

				initITContainer: function() {
					var self = this;

					this.loadViewerToolbar();
					this.loadMarkupToolbar();

					this.toolbarContainer.onBtnViewClick = function() {
						if (self.markupPage && self.markupPage.hasNotations()) {
							if (VC.Utils.confirm(VC.Utils.GetResource('mark_tb_lose_unsaved_markup'), window) !== true) {
								if (typeof (self.ToolbarManager.currentContainer.btnView.isCancelled) !== 'undefined') {
									self.ToolbarManager.currentContainer.btnView.isCancelled = true;
								}

								return;
							}
						}
						if (self.mode === self.ViewerModes.Markup) {
							self.displayFile();
							self.refreshDialogWrap();
							if (self.DialogManager.needRestoreMeasureDialog) {
								self.toolbarContainer.viewToolbar.btnMeasure.onClick();
							}
							self.applyViewStateData();
						}
					};
					this.toolbarContainer.onBtnMarkupClick = function() {
						if (self.mode === self.ViewerModes.View) {
							self.refreshDialogWrap();
							self.getMarkupImg().then(function(image) {
								var existMeasurements = self.viewer.getMeasureManager().getAllMeasurements().length;
								self.displayMarkup(image.src);
								self.DialogManager.needRestoreMeasureDialog = (existMeasurements !== 0) ? true : false;
							});

							self.saveCameraToViewState();
							self.saveHiddenItemsToViewState();
						}
					};
					this.toolbarContainer.onBtnSwitchClick = function() {
						var containerTypeToSwitch = VC.Toolbar.ContainerTypeMapper.mapBasicToCommand();
						var currentContainer = self.ToolbarManager.currentContainer;

						self.ToolbarManager.showITContainer(containerTypeToSwitch);

						if (!self.ToolbarManager.currentContainer.viewToolbar || !self.ToolbarManager.currentContainer.markupToolbar) {
							self.initITContainer();
						}

						currentContainer.assignElementStatesTo(self.ToolbarManager.currentContainer);
						self.ToolbarManager.currentContainer.refresh();
						currentContainer.refresh();
						currentContainer.closeAllPalettesButThis('');
						self._saveContainerTypeToInnovator(containerTypeToSwitch);
						if (self.OnLoaded) {
							self.OnLoaded();
						}
					};

					aspect.after(this.toolbarContainer, 'onBtnSwitchClick', lang.hitch(this, function() {
						if (this.toolbarContainer.markupToolbar && this.mode === this.ViewerModes.Markup) {
							this.restoreMarkupActivePalette();
						} else {
							// "Orient To Face" button's state depends on the selection made when Basic Toolbar was active
							this.setBtnOrientToFaceAccessibility();
						}
					}));
				},

				getMarkupImg: function() {
					if (isHoopsViewerSupported) {
						var canvasSize = this.viewer.getView().getCanvasSize();

						var snapshotConfig = new Communicator.SnapshotConfig(canvasSize.x, canvasSize.y);
						return this.viewer.takeSnapshot(snapshotConfig);
					}
				},

				resetCamera: function() {
					this.resetView();
				},

				removeAllMeasurements: function() {
					this.viewer.getMeasureManager().removeAllMeasurements();
				},

				orientToFaceClick: function() {
					this.setViewDirection('OrientToFace', directionDuration);
				},

				togglePMIs: function() {
					this.viewerToolbar.btnPMI.SetPressedState(!this.viewerToolbar.btnPMI.IsPressed);

					this.setPMIsVisibility(this.viewerToolbar.btnPMI.IsPressed);

					this.saveViewerParameter(this.type + '.' + VC.Utils.Enums.TButtonEvents.btnPMIClick, this.viewerToolbar.btnPMI.IsPressed ? 'On' : 'Off');
				},

				setPMIsVisibility: function(visibility) {
					cadViewId = this.getActivatedCADViewID();

					if (cadViewId) {
						const model = this.viewer.getModel();
						var pMIs = model.getCadViewPmis(cadViewId);
						model.setNodesVisibility(pMIs, visibility);
					}
				},

				getActivatedCADViewID: function() {
					var cadViewId = this.viewer.getModel().getActiveCadConfiguration();
					if (VC.Utils.isNotNullOrUndefined(cadViewId)) {
						return cadViewId;
					} else {
						// Initially activated CAD View is not recognized as activated by code above for some reason
						// We assume that first CAD View from the list of the all existing CAD Views was activated in this case
						var cadViews = this.viewer.getModel().getCadViews(); // { string(cad view id): string(label) , ...}
						var keys = Object.keys(cadViews);

						if (keys.length > 0) {
							return +keys[0];
						}
					}

					return null;
				},

				displayStyleClick: function() {
					var self = this;

					var displayStyleDialog = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.DisplayStyle);

					if (!displayStyleDialog.isInitHandlers) {
						self.bindUnpressedState(self.toolbarContainer.viewToolbar.btnDisplayStyle, displayStyleDialog);

						displayStyleDialog.onWireframeClick = function() {
							self.setDisplayStyle('WireFrame');
							self.toolbarContainer.viewToolbar.btnDisplayStyle.SetImage(this.btnWireframe.baseSrc);
						};
						displayStyleDialog.onShadedClick = function() {
							self.setDisplayStyle('Shaded');
							self.toolbarContainer.viewToolbar.btnDisplayStyle.SetImage(this.btnShaded.baseSrc);
						};
						displayStyleDialog.onHiddenLineClick = function() {
							self.setDisplayStyle('HiddenLine');
							self.toolbarContainer.viewToolbar.btnDisplayStyle.SetImage(this.btnHiddenLine.baseSrc);
						};
						displayStyleDialog.onWireframeOnShadedClick = function() {
							self.setDisplayStyle('WireFrameShaded');
							self.toolbarContainer.viewToolbar.btnDisplayStyle.SetImage(this.btnWireframeOnShaded.baseSrc);
						};
						displayStyleDialog.isInitHandlers = true;
					}
				},

				measureClick: function() {
					var self = this;

					var measureDialog = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.Measure);

					if (!measureDialog.isInitHandlers) {
						self.bindUnpressedState(self.toolbarContainer.viewToolbar.btnMeasure, measureDialog);

						measureDialog.onMeasurePointToPointClick = function() {
							self.setMeasureOperation('MeasurePoint');
						};
						measureDialog.onMeasureEdgesClick = function() {
							self.setMeasureOperation('MeasureEdge');
						};
						measureDialog.onMeasureAngleBetweenFacesClick = function() {
							self.setMeasureOperation('MeasureAngle');
						};
						measureDialog.onMeasureDistanceBetweenFacesClick = function() {
							self.setMeasureOperation('MeasureDistance');
						};
						measureDialog.onDeleteAllClick = function() {
							self.removeAllMeasurements();
						};
						measureDialog.onOpen = function() {
							var measureManager = self.viewer.getMeasureManager();
							if (self.measurementMarkup && self.measurementMarkup.length > 0) {
								measureManager.loadData(self.measurementMarkup, self.viewer);
							}

							var existMeasurements = measureManager.getAllMeasurements().length;
							if (existMeasurements > 0) {
								measureDialog.btnDeleteAll.Enable();
							} else {
								measureDialog.btnDeleteAll.Disable();
							}
							self.DialogManager.setVisibilityOfMeasureButtons(existMeasurements);
						};
						measureDialog.onClose = function() {
							self.measurementMarkup = self.viewer.getMeasureManager().exportMarkup();
							self.removeAllMeasurements();
							self.setMeasureOperation('Select');
						};

						measureDialog.isInitHandlers = true;
					}
				},

				explodedViewClick: function() {
					var self = this;

					var explodedDialog = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.Exploded);

					if (!explodedDialog.isInitHandlers) {
						self.bindUnpressedState(self.toolbarContainer.viewToolbar.btnExplodedView, explodedDialog);

						explodedDialog.changeSliderPosition = function() {
							self.explodedView(this.sliderValue / 50);
							var existMeasurements = self.viewer.getMeasureManager().getAllMeasurements().length;
							self.DialogManager.setVisibilityOfMeasureButtons(existMeasurements);
						};
						explodedDialog.onCrossClick = function() {
							this.sliderValue = 0;
							self.explodedView(this.sliderValue);
							self.viewer.getExplodeManager().stop();
						};

						explodedDialog.isInitHandlers = true;

						var explodeManager = this.viewer.getExplodeManager();
						if (!explodeManager.getActive()) {
							this.viewer.getExplodeManager().start();
						}
					}
				},

				crossSectionClick: function() {
					var self = this;

					var crossSectionArgs = {
						cappingGeometryState: this.viewer.getCuttingManager().getCappingGeometryVisibility()
					};

					var crossSectionDialog = self.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.CrossSection, crossSectionArgs);

					if (!crossSectionDialog.isInitHandlers) {
						self.bindUnpressedState(self.toolbarContainer.viewToolbar.btnCrossSection, crossSectionDialog);

						crossSectionDialog.onXButtonClick = function() {
							self.setCrossSectionOrientation('AxisX');
						};
						crossSectionDialog.onYButtonClick = function() {
							self.setCrossSectionOrientation('AxisY');
						};
						crossSectionDialog.onZButtonClick = function() {
							self.setCrossSectionOrientation('AxisZ');
						};
						crossSectionDialog.onPButtonClick = function() {
							self.setCrossSectionOrientation('SelectFaceNormal');
						};
						crossSectionDialog.onIXButtonClick = function() {
							self.setCrossSectionOrientation('InvertX');
						};
						crossSectionDialog.onIYButtonClick = function() {
							self.setCrossSectionOrientation('InvertY');
						};
						crossSectionDialog.onIZButtonClick = function() {
							self.setCrossSectionOrientation('InvertZ');
						};
						crossSectionDialog.onIPButtonClick = function() {
							self.setCrossSectionOrientation('InvertFace');
						};
						crossSectionDialog.onHasSelectedFace = function() {
							var selectionItem = self.viewer.getSelectionManager().getLast();
							var faceSelection = (selectionItem !== null && selectionItem.getFaceEntity() !== null);

							return faceSelection;
						};
						crossSectionDialog.onShowCappingGeometry = function(visible) {
							self.viewer.getCuttingManager().setCappingGeometryVisibility(visible);
						};
						crossSectionDialog.onToggleCuttingPlaneVisibility = function() {
							self.cuttingPlaneControl.onCuttingPlaneVisibilityToggle();
						};
						crossSectionDialog.onToggleCuttingPlaneSection = function() {
							self.cuttingPlaneControl.onCuttingPlaneSectionToggle();
						};
						crossSectionDialog.onSetCuttingPlaneVisibility = function(visible) {
							self.cuttingPlaneControl.setCuttingPlaneVisibility(visible);
						};
						crossSectionDialog.onSetCuttingPlaneSectionMode = function(useIndividual) {
							self.cuttingPlaneControl.setCuttingPlaneSectionMode(useIndividual);
						};
						crossSectionDialog.onClose = function() {
							if (!this.hasActivatedPlanes()) {
								self.viewer.getCuttingManager().deactivateCuttingSections();
							}
						};
						crossSectionDialog.onCrossClick = function() {
							self.viewer.getCuttingManager().deactivateCuttingSections();
						};

						self.setCrossSectionOrientation('AxisZ');
						crossSectionDialog.isInitHandlers = true;
					}

					self.viewer.getCuttingManager().activateCuttingSections();
				},

				//  Acceptable values for the parameter are:
				//  'HiddenLine'
				//  'WireFrameShaded'
				//  'Shaded'
				//  'WireFrame'
				setDisplayStyle: function(style) {
					switch (style) {
						case 'HiddenLine':
							this.viewer.getView().setDrawMode(Communicator.DrawMode.HiddenLine);
							break;
						case 'WireFrameShaded':
							this.viewer.getView().setDrawMode(Communicator.DrawMode.WireframeOnShaded);
							break;
						case 'Shaded':
							this.viewer.getView().setDrawMode(Communicator.DrawMode.Shaded);
							break;
						case 'WireFrame':
							this.viewer.getView().setDrawMode(Communicator.DrawMode.Wireframe);
							break;
					}
				},

				//  Acceptable values for the parameter are:
				//  'FrontView'
				//  'BackView'
				//  'LeftView'
				//  'RightView'
				//  'BottomView'
				//  'TopView'
				//  'IsoView'
				//  'OrientToFace'
				setViewDirection: function(direction, viewDirectionDuration) {
					switch (direction) {
						case 'FrontView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Front, viewDirectionDuration);
							break;
						case 'BackView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Back, viewDirectionDuration);
							break;
						case 'LeftView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Left, viewDirectionDuration);
							break;
						case 'RightView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Right, viewDirectionDuration);
							break;
						case 'BottomView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Bottom, viewDirectionDuration);
							break;
						case 'TopView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Top, viewDirectionDuration);
							break;
						case 'IsoView':
							this.viewer.getView().setViewOrientation(Communicator.ViewOrientation.Iso, viewDirectionDuration);
							break;
						case 'OrientToFace':
							this.orientToFace();
							break;
					}
				},

				orientToFace: function() {
					var selectionItem = this.viewer.getSelectionManager().getLast();
					if (selectionItem && selectionItem.getFaceEntity()) {
						var normal = selectionItem.getFaceEntity().getNormal();
						var position = selectionItem.getPosition();
						var camera = this.viewer.getView().getCamera();
						var up = Communicator.Point3.cross(normal, new Communicator.Point3(0, 1, 0));
						if (up.length() < 0.001) {
							up = Communicator.Point3.cross(normal, new Communicator.Point3(1, 0, 0));
						}
						var zoomDelta = camera.getPosition().subtract(camera.getTarget()).length();
						camera.setTarget(position);
						camera.setPosition(Communicator.Point3.add(position, Communicator.Point3.scale(normal, zoomDelta)));
						camera.setUp(up);
						this.viewer.getView().fitBounding(selectionItem.getFaceEntity().getBounding(), 400, camera);
					}
				},

				//  Acceptable values for the parameters are:
				//  style
				//         'Walk'
				//         'Turntable'
				//         'OrbitCamera'
				//  operation
				//         'Select'
				//         'Note'
				//         'MeasureEdge'
				//         'MeasurePoint'
				//         'MeasureAngle'
				//         'MeasureDistance'
				setRotationStyle: function(style, operation) {
					switch (style) {
						case 'Walk':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.Walk, 0);
							break;
						case 'Turntable':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.Turntable, 0);
							break;
						case 'OrbitCamera':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.Navigate, 0);
							break;
					}

					if (operation) {
						this.setMeasureOperation(operation);
					}
				},

				// The level is an integer with a value between 0 and 100
				explodedView: function(level) {
					this.viewer.getExplodeManager().setMagnitude(level);
				},

				//  Acceptable values for the parameter are:
				//  'Select'
				//  'Note'
				//  'MeasureEdge'
				//  'MeasurePoint'
				//  'MeasureAngle'
				//  'MeasureDistance'
				setMeasureOperation: function(operation) {
					switch (operation) {
						case 'Select':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.Select, 1);
							break;
						case 'Note':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.Note, 1);
							break;
						case 'MeasureEdge':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.MeasureEdgeLength, 1);
							break;
						case 'MeasurePoint':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.MeasurePointPointDistance, 1);
							break;
						case 'MeasureAngle':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.MeasureFaceFaceAngle, 1);
							break;
						case 'MeasureDistance':
							this.viewer.getOperatorManager().set(Communicator.OperatorId.MeasureFaceFaceDistance, 1);
							break;
					}
				},

				//  Acceptable values for the parameter are:
				//  'AxisX'
				//  'AxisY'
				//  'AxisZ'
				//  'SelectFaceNormal'
				//  'Invert'
				setCrossSectionOrientation: function(orientation) {
					switch (orientation) {
						case 'AxisX':
							this.cuttingPlaneControl.onAxisToggle('x');
							break;
						case 'AxisY':
							this.cuttingPlaneControl.onAxisToggle('y');
							break;
						case 'AxisZ':
							this.cuttingPlaneControl.onAxisToggle('z');
							break;
						case 'SelectFaceNormal':
							this.cuttingPlaneControl.onAxisToggle('face');
							break;
						case 'InvertX':
							this.cuttingPlaneControl.onAxisInvert('x');
							break;
						case 'InvertY':
							this.cuttingPlaneControl.onAxisInvert('y');
							break;
						case 'InvertZ':
							this.cuttingPlaneControl.onAxisInvert('z');
							break;
						case 'InvertFace':
							this.cuttingPlaneControl.onAxisInvert('face');
							break;
					}
				},

				showModelBrowser: function() {
					var self = this;

					var modelBrowser = this.DialogManager.getExistingOrNewDialog(VC.Utils.Enums.Dialogs.ModelBrowser);
					if (!modelBrowser.isInitHandlers) {
						this.bindUnpressedState(this.toolbarContainer.viewToolbar.btnModelBrowser, modelBrowser);

						modelBrowser.isInitHandlers = true;
					}
				},

				assignModelBrowserHandlers: function(modelBrowser) {
					var self = this;

					modelBrowser.modelTab.onSelectTreeItem = function(treeNodeId) {
						var selectionManager = self.viewer.getSelectionManager();
						var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);

						selectionManager.clear();

						if (nodeIds) {
							var selectionMode = nodeIds.length > 0 ? Communicator.SelectionMode.Add : Communicator.SelectionMode.Set;
							for (var i = 0; i < nodeIds.length; i++) {
								selectionManager.selectNode(nodeIds[i], selectionMode);
							}
						}
					};

					modelBrowser.modelTab.onOpenCad = function(treeNodeId) {
						var cadId = self.treeNodeIdToModelNodeIdDictionary.getCadId('treeNodeId', treeNodeId);
						self.openCadDocument(cadId);
					};

					modelBrowser.modelTab.onIsolate = function(treeNodeId) {
						var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);
						if (nodeIds) {
							self.isolateSelectedNodes(nodeIds);
						}
					};

					modelBrowser.modelTab.onVisibility = function(treeNodeId) {
						var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);
						if (nodeIds) {
							self.setVisibilitySelectedNodes(nodeIds);
						}
					};

					modelBrowser.modelTab.onHideAllOther = function(treeNodeId) {
						var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);
						if (nodeIds) {
							self.hideAllOtherNodes(nodeIds);
						}
					};

					modelBrowser.modelTab.onFitAll = function() {
						self.fitAllNodes();
					};

					modelBrowser.modelTab.onResetView = function() {
						self.resetView();
					};

					modelBrowser.modelTab.onDisplayAll = function() {
						self.displayAllNodes();
					};

					modelBrowser.onAfterShow = function() {
						self.adjustViewerContainerStyles(true);

						self.saveViewerParameter(self.type + '.' + VC.Utils.Enums.Dialogs.ModelBrowser, 'On', true);
					};

					modelBrowser.onAfterHide = function() {
						self.adjustViewerContainerStyles(false);

						self.saveViewerParameter(self.type + '.' + VC.Utils.Enums.Dialogs.ModelBrowser, 'Off', true);
					};

					modelBrowser.modelTab.onShowPart = function(treeNodeId) {
						if (!self.legacyModelType) {
							var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);
							self.setNodesVisibility(nodeIds, true);
						}
					};

					modelBrowser.modelTab.onHidePart = function(treeNodeId) {
						if (!self.legacyModelType) {
							var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);
							self.setNodesVisibility(nodeIds, false);
						}
					};

					modelBrowser.modelTab.isLegacyModelType = function() {
						return self.legacyModelType;
					};

					modelBrowser.modelTab.isSelectedNodeVisible = function(treeNodeId) {
						var nodeIds = self.treeNodeIdToModelNodeIdDictionary.getNodeIds(treeNodeId);

						if (nodeIds && nodeIds.length > 0) {
							return self.viewer.getModel().getNodeVisibility(nodeIds[0]);
						}

						return false;
					};

					modelBrowser.modelTab.onReadyModelTree = lang.hitch(this, 'loadDictionaryAndTree');

					modelBrowser.viewsTab.onGetCustomViews = function() {
						return self.getCustomViews();
					};

					modelBrowser.viewsTab.onSelectStandardView = function(viewDirectionType) {
						self.setViewDirection(viewDirectionType, directionDuration);
					};

					modelBrowser.viewsTab.onSelectCustomView = function(cadViewId) {
						var handleOperator = self.viewer.getOperatorManager().getOperator(Communicator.OperatorId.Handle);
						handleOperator.removeHandles();

						var faceAxisIndex = VC.Utils.Enums.AxisIndex.Face;
						var faceSection = self.viewer.getCuttingManager().getCuttingSection(faceAxisIndex);

						self.viewer.getModel().activateCadView(+cadViewId);
						if (faceSection.getCount()) {
							self.cuttingPlaneControl.setAxisStatus(faceAxisIndex, VC.Utils.Enums.CuttingPlaneStatuses.Hidden);
							self.cuttingPlaneControl.setCadViewActivated(true);
							self.cuttingPlaneControl.setCuttingPlaneVisible(true, faceAxisIndex);
						}

						self.setPMIButtonState();
					};
				},

				setPMIButtonState: function() {
					if (this.viewerToolbar.btnPMI) {
						var cadViewId = this.getActivatedCADViewID();

						if (cadViewId) {
							var pMIs = this.viewer.getModel().getCadViewPmis(cadViewId);
							if (Object.keys(pMIs).length > 0) {
								this.viewerToolbar.btnPMI.Enable();
								this.viewerToolbar.btnPMI.SetPressedState(true);
								this.saveViewerParameter(this.type + '.' + VC.Utils.Enums.TButtonEvents.btnPMIClick, this.viewerToolbar.btnPMI.IsPressed ? 'On' : 'Off');
							} else {
								this.viewerToolbar.btnPMI.Disable();
								this.deleteViewerParameter(this.type + '.' + VC.Utils.Enums.TButtonEvents.btnPMIClick);
							}
						} else {
							this.viewerToolbar.btnPMI.Disable();
							this.deleteViewerParameter(this.type + '.' + VC.Utils.Enums.TButtonEvents.btnPMIClick);
						}
					}
				},

				zoom: function(zoomValue) {
					// doesn't metter
					var canvasEl = this.viewer.getViewElement();

					var evt = document.createEvent('MouseEvents');
					var mouseWheelEvt = VC.Utils.isFirefox() ? 'DOMMouseScroll' : 'mousewheel';

					var width = canvasEl.clientWidth / 2;
					var height = canvasEl.clientHeight / 2;
					evt.wheelDelta = -zoomValue;
					evt.initMouseEvent(mouseWheelEvt, true, true, window, zoomValue, 0, 0, width, height, false, false, false, false, 0, null);

					canvasEl.dispatchEvent(evt);
				},

				getSnapshotPage: function() {
					var img = document.createElement('img');
					img.src = this.snapshotImage.src;

					var width = (this.snapshotContainer.style.display === 'none') ? this.viewerContainer.offsetWidth : this.snapshotImage.offsetWidth;
					var height = (this.snapshotContainer.style.display === 'none') ? this.viewerContainer.offsetHeight : this.snapshotImage.offsetHeight;

					return this.getCanvDataUrl(img, width, height);
				},

				onWindowResizeEventHandler: function() {
					if (this.viewer && this.isStarted) {
						this.resizeContent();
					}
					this.inherited(arguments);
				},

				getCustomViews: function() {
					var returnedArray = [];
					var viewData = null;
					var cadView = null;
					var isAnnotationView = null;
					var model = this.viewer.getModel();
					var cadViews = model.getCadViews(); // { string(cad view id): string(label) , ...}
					var keys = Object.keys(cadViews);

					if (keys.length > 0) {
						for (var i = 0; i < keys.length; i++) {
							isAnnotationView = model.isAnnotationView(+keys[i]);
							var parsedCadViewLabel = cadViews[keys[i]].split(this.annotationLabelSuffix)[0];
							viewData = {id: +keys[i], isAnnotationView: isAnnotationView, label: parsedCadViewLabel};
							returnedArray.push(viewData);
						}
					}

					return returnedArray;
				}
			});
		})());
	});
