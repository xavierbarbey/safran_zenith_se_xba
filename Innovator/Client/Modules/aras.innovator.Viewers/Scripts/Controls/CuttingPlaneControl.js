﻿require([
	'dojo/_base/declare',
	'dojo/_base/lang'],
	function(declare, lang) {
		return dojo.setObject('VC.Widgets.CuttingPlaneControl', declare(null,
		{
			constructor: function(cuttingPlaneController) {
				this.controller = cuttingPlaneController;
			},

			onAxisToggle: function(planeAxis) {
				var self = this;
				var axis = this.getAxis(planeAxis);
				var faceSelection = false;
				if (axis == VC.Utils.Enums.AxisIndex.Face) {
					var selectionItem = this.controller._viewer.getSelectionManager().getLast();
					faceSelection = (selectionItem !== null && selectionItem.isFaceSelection());
					if (faceSelection) {
						this.controller._faceSelection = selectionItem;
					}
				}
				switch (this.getAxisStatus(axis)) {
					case VC.Utils.Enums.CuttingPlaneStatuses.Hidden:
						if (axis == VC.Utils.Enums.AxisIndex.Face) {
							if (faceSelection) {
								this.controller._cuttingSections[axis].clear().then(function() {
									self.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Visible);
									self.controller.setCuttingPlaneVisibility(true, axis);
								});
							}
						} else {
							this.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Visible);
							this.controller.setCuttingPlaneVisibility(true, axis);
						}
						break;
					case VC.Utils.Enums.CuttingPlaneStatuses.Visible:
					case VC.Utils.Enums.CuttingPlaneStatuses.Inverted:
						if (axis == VC.Utils.Enums.AxisIndex.Face) {
							this.controller._cuttingSections[axis].clear().then(function() {
								self.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Hidden);
								self.controller.setCuttingPlaneVisibility(false, axis);

								self.controller._cuttingPlanes[axis] = null;
								self.controller._referenceGeometry[axis] = null;
								self.controller._updateReferenceGeometry[axis] = false;
								self.controller._faceSelection = null;
							});
						} else {
							this.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Hidden);
							this.controller.setCuttingPlaneVisibility(false, axis);
						}
						break;
				}
			},

			onAxisInvert: function(planeAxis) {
				var axis = this.getAxis(planeAxis);

				if (this.getAxisStatus(axis) == VC.Utils.Enums.CuttingPlaneStatuses.Inverted) {
					this.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Visible);
				} else {
					this.controller._setStatus(axis, VC.Utils.Enums.CuttingPlaneStatuses.Inverted);
				}

				this.controller.setCuttingPlaneInverted(axis);
			},

			onCuttingPlaneVisibilityToggle: function() {
				this.controller.toggleReferenceGeometry();
			},

			onCuttingPlaneSectionToggle: function() {
				this.controller.toggleCuttingMode();
			},

			setCuttingPlaneVisibility: function(visible) {
				this.controller._showReferenceGeometry = visible;
			},

			setCuttingPlaneSectionMode: function(useIndividual) {
				this.controller._useIndividualCuttingSections = useIndividual;
			},

			getAxis: function(planeAxis) {
				switch (planeAxis) {
					case 'x':
						return VC.Utils.Enums.AxisIndex.X;
					case 'y':
						return VC.Utils.Enums.AxisIndex.Y;
					case 'z':
						return VC.Utils.Enums.AxisIndex.Z;
					case 'face':
						return VC.Utils.Enums.AxisIndex.Face;
					default:
						return null;
				}
			},

			setAxisStatus: function(axisIndex, status) {
				this.controller._setStatus(axisIndex, status);
			},

			setCadViewActivated: function(status) {
				this.controller._cadViewActivated = status;
			},

			setCuttingPlaneVisible: function(status, axisIndex) {
				this.controller.setCuttingPlaneVisibility(status, axisIndex);
			},

			getAxisStatus: function(axis) {
				return this.controller._ensurePlaneInfo(axis).status;
			}
		}));
	});
