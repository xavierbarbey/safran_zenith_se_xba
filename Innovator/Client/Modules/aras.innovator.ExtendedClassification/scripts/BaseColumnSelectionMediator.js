﻿function BaseColumnSelectionMediator(xClassBarNode) {
	this.xClassBarWrapper = new XClassBar(itemTypeName, xClassBarNode);
}

BaseColumnSelectionMediator.prototype.initToolbarFormatter = function(data) {
	const self = this;
	Toolbar.formatters.columnSelectionControl = function(data) {
		const createColumnSelectionControl = function(data) {
			const columnSelectNode = Inferno.createVNode(ArasModules.utils.infernoFlags.htmlElement, 'div', 'column-select-block hidden',
				null, {id: 'column_select_block'});
			const columnSelectDropdownBox = Inferno.createVNode(ArasModules.utils.infernoFlags.htmlElement, 'div', 'aras-dropdown', columnSelectNode);
			const dropdownButton = Inferno.createVNode(ArasModules.utils.infernoFlags.htmlElement, 'span', 'aras-toolbar__button', null, {
				title: data.item.tooltip
			});
			const columnSelectContainer = Inferno.createVNode(ArasModules.utils.infernoFlags.htmlElement, 'div', 'column-select-dropdown aras-toolbar__dropdown',
				[dropdownButton, columnSelectDropdownBox]);
			return columnSelectContainer;
		};
		return Inferno.createVNode(ArasModules.utils.infernoFlags.componentFunction, createColumnSelectionControl, null, null, data, null,
			{
				onComponentDidMount: self._onComponentDidMount.bind(self)
			}
		);
	};
};

BaseColumnSelectionMediator.prototype._onComponentDidMount = function(columnSelectContainer) {
	columnSelectionControl.attachTo(document.getElementById('column_select_block'));
	ArasModules.dropdown(columnSelectContainer, {pos: 'bottom-left', closeOnClick: false});
	columnSelectContainer.addEventListener('dropdownbeforeopen', this.columnSelectionOnBeforeOpen.bind(this));
	columnSelectContainer.addEventListener('dropdownclosed', this.apply.bind(this));
};

BaseColumnSelectionMediator.prototype.columnSelectionOnBeforeOpen = function() {
	const data = this.getDataForColumnSelection(itemTypeName, visiblePropNds);
	columnSelectionControl.node.classList.remove('hidden');
	columnSelectionControl.initTree(itemTypeName, data.columns, data.xClassList);
	const currentSearchQueryItem = aras.newIOMItem();
	currentSearchQueryItem.loadAML(searchContainer._getSearchQueryAML());
	xClassSearchWrapper.initData(itemTypeName,
		data.xClassList,
		currentSearchQueryItem.node,
		currentSearchMode.supportXClassSearch);
	xClassSearchWrapper.updateColumnSelection();
	xClassSearchWrapper.toggle();
};

BaseColumnSelectionMediator.prototype.clearSearch = function() {
	this.xClassBarWrapper.setQueryText();
	xClassSearchWrapper.xClassSearchControl.clearXClassSearchCriteria(currQryItem.item);
	searchContainer._updateAutoSavedSearch(currQryItem.item.xml);
	searchContainer._setAml(currQryItem.item.xml);
};

BaseColumnSelectionMediator.prototype.closeColumnSelectionWindow = function() {
	if (window.columnSelectionControl && columnSelectionControl.node) {
		columnSelectionControl.node.classList.add('hidden');
	}
};

BaseColumnSelectionMediator.prototype.closeXClassBarWindow = function() {
	if (this.xClassBarWrapper && this.xClassBarWrapper.container) {
		this.xClassBarWrapper.container.classList.add('hidden');
	}
};

BaseColumnSelectionMediator.prototype.updateXClassBar = function() {
	this.xClassBarWrapper.updateXClassBar(grid.getSelectedId());
};

BaseColumnSelectionMediator.prototype.getDataForColumnSelection = function(itemTypeName, propsNds) {
	const xClassList = xPropertiesUtils.getXClassificationTreesForItemType(aras.getItemTypeId(itemTypeName));
	return {
		columns: this.getColumns(propsNds),
		xClassList: xClassList
	};
};

BaseColumnSelectionMediator.prototype.apply = function() {
	if (currentSearchMode.supportXClassSearch) {
		let amlQuery = xClassSearchWrapper.xClassSearchControl.getQueryAml();
		if (amlQuery) {
			const xClassSearch = aras.newIOMItem();
			xClassSearch.loadAML(amlQuery);
			searchContainer._updateAutoSavedSearch(xClassSearch.node.xml);
			searchContainer._setAml(xClassSearch.node.xml);
		}
		if (!this.xClassBarWrapper.container.classList.contains('hidden')) {
			let xClassTextQuery = xClassSearchWrapper.xClassSearchControl.getQueryForXClassBar();
			this.xClassBarWrapper.setQueryText(xClassTextQuery);
		}
	}
	if (columnSelectionControl.isDirty) {
		const columns = columnSelectionControl.getCheckedColumns();
		const columnIdToColumn = {};
		columnSelectionControl.columns.forEach(function(column) {
			columnIdToColumn[column.propertyId] = column;
		});
		columns.forEach(function(column) {
			grid.SetColumnVisible(columnIdToColumn[column.propertyId].colNumber, !column.hidden, columnIdToColumn[column.propertyId].width);
		}.bind(this));
		const key = aras.MetadataCache.CreateCacheKey('getSelectCriteria', itemTypeID, searchContainer.searchLocation == 'Relationships Grid');
		aras.MetadataCache.SetItem(key);

		const newSearch = aras.newIOMItem();
		newSearch.loadAML(searchContainer._getDefaultSearchQueryAML());

		const oldSearch = aras.newIOMItem();
		oldSearch.loadAML(searchContainer._getSearchQueryAML());
		oldSearch.setAttribute('select', newSearch.getAttribute('select'));

		searchContainer._updateAutoSavedSearch(oldSearch.node.xml);
		searchContainer._setAml(oldSearch.node.xml);
		searchContainer.runSearch();
	}
};

BaseColumnSelectionMediator.prototype.getColumns = function(propNds) {
	const colOrderArr = grid.getLogicalColumnOrder().split(';');
	const propertyItems = {};
	let propsToShow = [];

	if (propNds) {
		for (let i = 0; i < propNds.length; i++) {
			const propertyName = aras.getItemProperty(propNds[i], 'name');
			propertyItems[propertyName] = propNds[i];
		}
	}

	for (let i = 0; i < colOrderArr.length; i++) {
		let isValidProperty = false;
		let hidden = grid.getColWidth(i) == 0 ? true : false;
		let propertyLabel = '';
		let propertyWidth = 100;
		let columnName = grid.GetColumnName(i);
		let propertyName;
		let propertyId;
		let propSortOrder;

		if (columnName == 'L') {
			propertyLabel = 'Locked';
			propertyWidth = 24;
			isValidProperty = true;
		} else {
			propertyName = columnName.substr(0, columnName.length - 2);
			const propertyItem = propertyItems[propertyName];
			if (propertyItem) {
				const tempWidth = parseInt(aras.getItemProperty(propertyItem, 'column_width'));
				propSortOrder = parseInt(aras.getItemProperty(propertyItem, 'sort_order'));

				propertyLabel = aras.getItemProperty(propertyItem, 'label') || propertyName;
				propertyId = aras.getItemProperty(propertyItem, 'id');

				if (!isNaN(tempWidth)) {
					propertyWidth = tempWidth;
				}
				isValidProperty = true;
			}
		}
		if (!isValidProperty) {
			continue;
		}
		propsToShow.push({
			colNumber: i,
			propSortOrder: propSortOrder || 0,
			name: propertyName,
			label: propertyLabel,
			width: propertyWidth,
			hidden: hidden,
			propertyId: propertyId
		});
	}
	return propsToShow;
};
