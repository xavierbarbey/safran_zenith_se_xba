﻿(function(wnd) {
	var infernoFlags = ArasModules.utils.infernoFlags;
	var formFieldsTemplates = {
		string: function(item) {
			return Inferno.createVNode(infernoFlags.inputElement, 'input', null, null, Object.assign({}, item.attrs, {
				type: 'text'
			}), null, item.refs);
		},
		text: function(item) {
			return Inferno.createVNode(infernoFlags.textareaElement, 'textarea', null, null, Object.assign({}, item.attrs), null, item.refs);
		},
		list: function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'aras-filter-list', null, null, null, null, item.refs);
		},
		boolean: function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'label', 'aras-form-boolean', [
				Inferno.createVNode(infernoFlags.inputElement, 'input', null, null, Object.assign({}, item.attrs, {
					type: 'checkbox'
				}), null, item.refs),
				Inferno.createVNode(infernoFlags.htmlElement, 'span')
			]);
		},
		color: function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'div', 'color-input', null, Object.assign({}, item.attrs));
		},
		'color list': function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'select', 'color-list',
				item.data.values.map(function(value) {
					var options = {
						selected: value.value === item.data.value,
						style: 'background-color: ' + (value.value || '#FFFFFF'),
						value: value.value
					};

					return Inferno.createVNode(infernoFlags.htmlElement, 'option', null, value.label, options);
				}), Object.assign({}, item.attrs), null, item.refs);
		},
		date: function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'div', 'aras-form-date', [
				Inferno.createVNode(infernoFlags.inputElement, 'input', null, null, Object.assign({}, item.attrs, {
					type: 'text'
				}), null, item.refs),
				Inferno.createVNode(infernoFlags.htmlElement, 'span')
			]);
		},
		'mv_list': function(item) {
			return Inferno.createVNode(infernoFlags.selectElement, 'select', null,
				item.data.values.map(function(value) {
					var options = {
						selected: item.data.selectedOptions.has(value.value),
						value: value.value
					};
					return Inferno.createVNode(infernoFlags.htmlElement, 'option', null, value.label, options);
				}), Object.assign({multiple: '1'}, item.attrs), null, item.refs);
		},
		'restricted': function() {
			return Inferno.createVNode(infernoFlags.htmlElement, 'div', null, [
				Inferno.createVNode(infernoFlags.htmlElement, 'span'),
				Inferno.createVNode(infernoFlags.htmlElement, 'span', 'aras-field-xclasses__restricted_field',
					aras.getResource('', 'common.restricted_property_warning'))
			]);
		},
		item: function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'div', null, [
				Inferno.createVNode(infernoFlags.htmlElement, 'aras-item-property', 'aras-field-xclasses__item-input-wrapper', null, null, null, item.refs),
				Inferno.createVNode(infernoFlags.htmlElement, 'span', 'sys_item_link aras-field-xclasses__item_link', item.data.value, {
					id: item.data.id + '_span'
				})
			]);
		}
	};

	wnd.FormFieldsTemplates = formFieldsTemplates;
})(window);
