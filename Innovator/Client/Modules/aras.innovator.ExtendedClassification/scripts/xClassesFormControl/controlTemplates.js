﻿(function(wnd) {
	function XControlTemplates(navInstance) {
		var infernoFlags = ArasModules.utils.infernoFlags;
		var templates = NavTemplates(navInstance);
		var parentNodeClass = 'aras-nav-parent';
		var parentExpandedNodeClass = 'aras-nav-parent aras-nav-parent_expanded';

		templates.node = function Node(dataItem) {
			return dataItem.value.children ?
				Inferno.createVNode(infernoFlags.componentFunction, templates.parentNode, null, null, dataItem) :
				Inferno.createVNode(infernoFlags.componentFunction, templates.labeledElement, null, null, dataItem);
		};

		templates.childNode = function(item) {
			var type = FormFieldsTemplates[item.data.type] ? item.data.type : 'string';
			return FormFieldsTemplates[type](item);
		};

		templates.labeledElement = function(item) {
			return Inferno.createVNode(infernoFlags.htmlElement, 'li', item.value.li.className, [
				Inferno.createVNode(infernoFlags.htmlElement, 'div', null, item.value.field.data.label, item.value.labelContainer.attrs),
				Inferno.createVNode(infernoFlags.htmlElement, 'div', null, [
					Inferno.createVNode(infernoFlags.componentFunction, templates.childNode, null, null, item.value.field)
				], item.value.fieldContainer.attrs),
				Inferno.createVNode(infernoFlags.htmlElement, 'div', 'aras-icon-vertical-ellipsis', null,
				item.value.field.data.readonly ? {style: 'visibility: hidden'} : null)
			], item.value.li.attrs);

		};

		templates.parentNode = function ParentNode(item) {
			var children = item.value.children;
			var ulOfChildren = Inferno.createVNode(infernoFlags.htmlElement, 'ul', null,
				children.map(function(childKey) {
					return Inferno.createVNode(
						infernoFlags.componentFunction,
						templates.node, null, null,
						{
							nodeKey: childKey,
							value: navInstance.data.get(childKey)
						}
					);
				})
			);

			var divLabel = Inferno.createVNode(infernoFlags.htmlElement, 'div', null,
				[
					Inferno.createVNode(infernoFlags.htmlElement, 'div', 'aras-nav-icon', [
						Inferno.createVNode(infernoFlags.htmlElement, 'span')
					]), //arrow
					Inferno.createVNode(infernoFlags.htmlElement, 'span', null, item.value.label),
					Inferno.createVNode(infernoFlags.htmlElement, 'span', 'aras-field-xclasses__delete-cross')
				]
			);

			var isExpanded = navInstance.expandedItemsKeys.has(item.nodeKey);
			var classNames = isExpanded ? parentExpandedNodeClass : parentNodeClass;
			classNames += item.value.removed ? ' aras-field-xclasses__xclass-block_disabled' : '';
			var liProps = {
				'data-key': item.nodeKey,
				className: classNames
			};
			return Inferno.createVNode(infernoFlags.htmlElement, 'li', null, [
				divLabel,
				ulOfChildren
			], liProps);
		};

		return templates;
	}

	wnd.XControlTemplates = XControlTemplates;
})(window);
