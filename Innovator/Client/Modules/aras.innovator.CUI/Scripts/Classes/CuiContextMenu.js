﻿ModulesManager.define([], 'aras.innovator.CUI/CuiContextMenu',
	function() {
		var aspect;
		require(['dojo/aspect'],
			function(_aspect) {
				aspect = _aspect;
			});

		function CuiContextMenu() {
			if (!this.private) {
				this.private = {
					declare: function(fieldName, func) {
						var boundFunc = func.bind(self);
						this[fieldName] = boundFunc;
					}
				};
			}
			this.private.aspectItems = [];
			this.private.popupMenuReinitDataById = {};
		}

		CuiContextMenu.prototype.initPopupMenu = function(popupMenu, contextItem, contextParams) {
			///<summary>Have to be called right after creating ContextMenu in order to set handlers on the menu and etc.
			///Used for both contextMenu and popupMenu
			///For mainTree.html, ItemGrid, inBasketTaskGrid, relationshipsGrid</summary>
			var customClickHandler = function(commandId, rowId, col) {
				if (this.onClickHandlerMap && this.onClickHandlerMap[commandId]) {
					var clickHandler = this.onClickHandlerMap[commandId];
					var args = {
						commandId: commandId,
						rowId: rowId,
						col: col
					};
					if (contextItem) {
						args.contextItem = contextItem;
					}
					if (contextParams) {
						args.contextParams = contextParams;
					}
					clickHandler(args);
				}
			};
			var aspectItems = this.private.aspectItems.filter(function(item) {
				return item.menu === popupMenu;
			});
			var aspectItem = aspectItems.length && aspectItems[0];
			if (aspectItem) {
				aspectItem.aspect.remove();
				this.private.aspectItems = this.private.aspectItems.filter(function(item) {
					return item !== aspectItem;
				});
			}
			aspectItem = {
				aspect: aspect.after(popupMenu, 'onItemClick', customClickHandler, true),
				menu: popupMenu
			};
			this.private.aspectItems.push(aspectItem);
		};

		CuiContextMenu.prototype.fillContextMenu = function(locationName, contextMenu) {
			///<summary>For mainTree.html</summary>
			var contextItem = this.utils.getDefaultContextItem();
			if (!contextItem.itemType && contextMenu.rowId) {
				var itemTypeFromCache = aras.MetadataCache.GetItemType(contextMenu.rowId, 'name');
				var result = itemTypeFromCache.getResult();
				if (!itemTypeFromCache.isFault() && result.childNodes[0]) {
					contextItem.itemType = result.childNodes[0];
				}
			}

			var items = this.dataLoader.loadCommandBar(locationName, contextItem);
			contextMenu.removeAll();
			if (this.utils.noItems(items)) {
				return;
			}
			var menuData = [];
			var onClickHandlerMap = {};

			for (var i = 0; i < items.length ; ++i) {
				var item = items[i];

				var initMethodName = this.utils.getOnInitHandlerName(item);
				var initData = null;
				if (initMethodName) {
					initData = this.utils.evalCommandBarItemMethod(initMethodName, contextItem);
					if (initData && initData.hasOwnProperty('cui_visible') && !initData['cui_visible']) {
						continue;
					}
				}

				var label = item.selectSingleNode('label');
				label = label ? label.text : '';
				var additionalData = this.utils.getCommandBarItemAdditionalData(item, initData);
				if (!label && additionalData && additionalData['cui_resource_key']) {
					label = aras.getResource(additionalData['cui_resource_solution'] || '', additionalData['cui_resource_key']);
				}

				var controlId = item.selectSingleNode('name');
				controlId = controlId ? controlId.text : '';
				var clickHandler = this.utils.getOnClickHandler(item);
				if (clickHandler) {
					onClickHandlerMap[controlId] = clickHandler;
				}

				menuData.push({id: controlId, name: label || controlId});
			}
			contextMenu.addRange(menuData);
			contextMenu.onClickHandlerMap = onClickHandlerMap;
		};

		CuiContextMenu.prototype.fillPopupMenu = function(locationName, popupMenu, contextItemOverride, itemStates, dontClearMenu, contextParams) {
			///<summary>For ItemGrid, inBasketTaskGrid, relationshipsGrid</summary>
			///<param name='locationName'></param>
			///<param name='popupMenu'>Innovator\Client\javascript\Aras\Client\Controls\Experimental\ContextMenu.js instance</param>
			///<param name='menuContext'></param>
			///<param name='itemStates'>id:enabled map</param>
			///<param name='dontClearMenu'></param>
			///<returns>Bool; true if menu should be shown</returns>
			var contextItem = Object.assign(this.utils.getDefaultContextItem(), contextItemOverride);
			var items = this.dataLoader.loadCommandBar(locationName, contextItem, contextParams);
			var newSeparatorId = false;
			if (!dontClearMenu) {
				popupMenu.removeAll();
			}

			if (this.utils.noItems(items)) {
				return false;
			}

			var onClickHandlerMap = {};

			for (var i = 0; i < items.length ; ++i) {
				var item = items[i];
				var type = item.getAttribute('type');
				if (!type) {
					continue;
				}
				var isSeparator = type.toLowerCase() === 'commandbarseparator' || type.toLowerCase() === 'commandbarmenuseparator';
				var commandId = item.selectSingleNode('name');
				const iconNode = item.selectSingleNode('image');
				const icon = iconNode ? iconNode.text : '';

				commandId = commandId ? commandId.text : '';

				// Relationships grid separator logic
				if (isSeparator && !commandId) {
					popupMenu.addSeparator();
					continue;
				}

				var initMethodName = this.utils.getOnInitHandlerName(item);
				this.private.popupMenuReinitDataById[commandId] = {handler: initMethodName, cuiItem: item};
				// ItemGrid separator logic
				if ((!itemStates || itemStates[commandId] || itemStates[commandId] === undefined) && !isSeparator) {
					if (newSeparatorId) {
						popupMenu.addSeparator(false, newSeparatorId);
						newSeparatorId = false;
					}

					var initData = null;
					if (initMethodName) {
						contextItem.controlId = commandId;
						contextItem.contextParams = contextParams;
						initData = this.utils.evalCommandBarItemMethod(initMethodName, contextItem);
					}
					var additionalData = this.utils.getCommandBarItemAdditionalData(item, initData);
					var label = item.selectSingleNode('label');
					label = label ? label.text : '';
					if (!label && additionalData && additionalData['cui_resource_key']) {
						label = aras.getResource(additionalData['cui_resource_solution'] || '', additionalData['cui_resource_key']);
					}
					var clickHandler = this.utils.getOnClickHandler(item);
					if (clickHandler) {
						onClickHandlerMap[commandId] = clickHandler;
					}
					popupMenu.add(commandId, label || '', {icon: icon});

					if (additionalData) {
						popupMenu.setDisable(commandId, additionalData['cui_disabled']);
						popupMenu.setHide(commandId, additionalData['cui_invisible']);
					}
				} else if (isSeparator) {
					newSeparatorId = commandId;
				}
			}

			popupMenu.onClickHandlerMap = onClickHandlerMap;

			// Call of callInitHandlersForPopupMenu should be removed from fillPopupMenu in further full migration of popup menu on CUI model.
			var eventState = {};
			this.callInitHandlersForPopupMenu(eventState, '');
			delete contextItem.controlId;
			return true;
		};

		CuiContextMenu.prototype.onGridHeaderContextMenu = function(e, grid, isAtCell) {
			var getCountVisibleColunms = function(grid) {
				var result = 0;
				for (var i = 0; i < grid.getColumnCount(); i++) {
					if (grid.isColumnVisible(i)) {
						result++;
					}
				}
				return result;
			};

			if (e.rowIndex == '-1') {
				var headerMenu = grid['getHeaderMenu_Experimental']();
				if (isAtCell && !headerMenu.initialized) {
					// fill menu if it hasn't items and click was at the header cell
					this.fillPopupMenu('PopupMenuGridHeader', headerMenu);
					// disable 'hideCol' item if number visible columns = 1
					if (headerMenu.collectionMenu && headerMenu.collectionMenu.hideCol && getCountVisibleColunms(grid) === 1) {
						headerMenu.setDisable('hideCol', true);
					}
					if (e.view && e.view.isMainGrid) {
						headerMenu.setHide('insertCol', true);
					}
				} else if (!isAtCell && headerMenu.initialized) {
					// clear menu (don't show) if menu has items and click was not at the header cell
					headerMenu.removeAll();
				}
				headerMenu.initialized = isAtCell;
			}

			return true;
		};

		CuiContextMenu.prototype.callInitHandlersForPopupMenu = function(eventState, eventType) {
			var topWnd = aras.getMostTopWindowWithAras();
			if (topWnd && topWnd.main && topWnd.main.work && topWnd.main.work.grid) {
				var grid = topWnd.main.work.grid;
				var popupMenu = grid.getMenu();
				var contextParams = {eventState: eventState, eventType: eventType, isReinit: true};
				var doReinit = (function(collectionName, isSeparator) {
					var menuItemsIds = Object.keys(popupMenu[collectionName]);
					for (var i = 0; i < menuItemsIds.length; i++) {
						var id = menuItemsIds[i];
						var menuItem = popupMenu[collectionName][id];

						var reinitData = this.private.popupMenuReinitDataById[id];
						if (reinitData && reinitData.cuiItem && reinitData.handler) {
							var additionalDataNode = reinitData.cuiItem.selectSingleNode('additional_data');
							var additionalData = additionalDataNode ? additionalDataNode.text : '';

							contextParams.control = {item: menuItem, additionalData: additionalData};
							reinitData = this.utils.evalCommandBarItemMethod(reinitData.handler, contextParams);
							if (reinitData) {
								popupMenu[isSeparator ? 'setHideSeparator' : 'setHide'](id, reinitData['cui_invisible']);
								if (!isSeparator && reinitData['cui_disabled'] !== undefined) {
									popupMenu.setDisable(id, reinitData['cui_disabled']);
								}
							}
						}
					}
				}).bind(this);

				if (popupMenu) {
					if (popupMenu.collectionMenu) {
						doReinit('collectionMenu');
					}
					if (popupMenu.collectionSeparator) {
						doReinit('collectionSeparator', true);
					}
				}
			}
		};

		return CuiContextMenu;
	}
);
