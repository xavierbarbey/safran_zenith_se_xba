﻿ModulesManager.define([], 'aras.innovator.CUI/CuiSidebar',
	function() {
		var mouse;
		var on;
		var dijit;
		var RadioButtonMenuItem;
		require(['dojo/mouse', 'dojo/on', 'dijit/dijit', 'Controls/RadioButtonMenuItem', 'dijit/form/Button', 'dijit/form/DropDownButton'],
			function(_mouse, _on, _dijit, _RadioButtonMenuItem) {
				mouse = _mouse;
				on = _on;
				dijit = _dijit;
				RadioButtonMenuItem = _RadioButtonMenuItem;
			});

		function CuiSidebar() {
			//'private' object must be created at ConfigurableUI module.
			//If CuiSidebar module is used separately from ConfigurableUI module, 'private' object should be created in CuiSidebar constructor
			if (!this.private) {
				var self = this;
				this.private = {
					declare: function(fieldName, func) {
						var boundFunc = func.bind(self);
						this[fieldName] = boundFunc;
					}
				};
			}

			this.private.declare('loadSidebarImplementation', function(contextParams, async) {
				///<summary>Top-level func</summary>
				///<param name='sidebar'>sidebar</param>
				///<param name='contextParams'>window (if called from layout view), document (if called from form rendered in iframe)
				///  or other object with IOM context properties set</param>
				///<returns>void</returns>
				try {
					contextParams = Object.assign(this.utils.getDefaultContextItem(), contextParams);
					return this.dataLoader.loadCommandBarImplementation(contextParams.locationName, contextParams, async).then(function(items) {
						if (items.length < 1) {
							return;
						}

						var cuiSidebarItems = [];
						var controlEventParams = this.utils.createClientMethodParams(contextParams);
						for (var i = 0; i < items.length ; ++i) {
							var item = items[i];

							var rtn;
							// name is used as controlId
							var name = item.selectSingleNode('name').text;
							var initMethodName = this.utils.getOnInitHandlerName(item);
							if (initMethodName) {
								contextParams.controlId = name;
								rtn = this.utils.evalCommandBarItemMethod(initMethodName, contextParams);
								if (rtn && rtn.hasOwnProperty('cui_visible') && !rtn['cui_visible']) {
									continue;
								}
							}

							var sidebarItem = {};
							sidebarItem.id = item.getAttribute('id');
							sidebarItem.name = name;
							sidebarItem.label = aras.getItemProperty(item, 'label');

							// Special logic for tooltipTemplate
							// There is should be possibility to set empty tooltip template
							var tooltipTemplateNode = item.selectSingleNode('tooltip_template');
							if (tooltipTemplateNode) {
								sidebarItem.tooltipTemplate = tooltipTemplateNode.text;
								if (!sidebarItem.tooltipTemplate && tooltipTemplateNode.getAttribute('is_null') === '1') {
									sidebarItem.tooltipTemplate = null;
								}
							}

							var additionalData = this.utils.getCommandBarItemAdditionalData(item, rtn);
							sidebarItem.additionalData = additionalData;
							sidebarItem.baseClass = additionalData ? additionalData['cui_base_class'] : null;
							sidebarItem.group = aras.getItemProperty(item, 'group_id');
							sidebarItem.iconClass = (additionalData ? additionalData['cui_icon_class'] : null) || 'sidebarButtonIcon';
							sidebarItem.image = aras.getItemProperty(item, 'image');
							sidebarItem.lcItemType = item.getAttribute('type').toLowerCase();
							sidebarItem.parent = aras.getItemProperty(item, 'parent_menu');
							sidebarItem.events = {
								onClickHandler: this.utils.getHandlerName(item, additionalData, 'on_click_handler'),
								mouseEnter: this.utils.getHandlerName(item, additionalData, 'mouse_enter'),
								mouseLeave: this.utils.getHandlerName(item, additionalData, 'mouse_leave'),
								controlEventParams: controlEventParams
							};

							cuiSidebarItems.push(sidebarItem);
						}

						return cuiSidebarItems;
					}.bind(this));
				}
				catch (e) {
					aras.AlertError('loadSidebar: ' + e);
				}
			});

			this.private.declare('setEventToSidebarItem', function(widget, methodName, eventName, handlerName, controlEventParams) {
				if (methodName) {
					var params = {
						control: widget,
						eventName: eventName,
						handlerName: handlerName
					};
					params = Object.assign(params, controlEventParams);
					on(widget, eventName, this._getEventHandler(this, methodName, eventName, params));
				}
			});
		}

		CuiSidebar.prototype.dispatchCommandBarLoadedEvent = function(locationName, commandBar) {
			var evnt = document.createEvent('Event');
			evnt.locationName = locationName;
			evnt.changeType = 'loaded';
			evnt.commandBar = commandBar;
			evnt.initEvent('commandBarChanged', true, false);
			document.dispatchEvent(evnt);
		};

		CuiSidebar.prototype.loadSidebar = function(contextParams) {
			return this.private.loadSidebarImplementation(contextParams, false);
		};

		CuiSidebar.prototype.loadSidebarAsync = function(contextParams) {
			return this.private.loadSidebarImplementation(contextParams, true);
		};

		CuiSidebar.prototype.initSidebar = function(sidebar, sidebarItems) {
			if (sidebar.get('loaded')) {
				return;
			}

			var widgets = [];
			for (var i = 0; i < sidebarItems.length; ++i) {
				var item = sidebarItems[i];
				var widget = null;
				if (dijit.byId(item.name)) {
					continue;
				}
				var args = {
					id: item.name
				};
				try {
					switch (item.lcItemType) {
						case 'commandbarbutton':
							args.iconClass = item.iconClass;
							args.baseClass = item.baseClass || 'sidebarButton';
							widget = new dijit.form.Button(args);
							break;
						case 'commandbarmenu':
							args.iconClass = item.iconClass;
							args.baseClass = item.baseClass || 'sidebarButton';
							args.dropDownPosition = ['after'];
							var dropDownMenu = new dijit.DropDownMenu({
								baseClass: 'sidebarPopup'
							});
							if (item.parent) {
								args.label = item.label || '';
								args.popup = dropDownMenu;
								widget = new dijit.PopupMenuItem(args);
							} else {
								args.dropDown = dropDownMenu;
								widget = new dijit.form.DropDownButton(args);
							}
							widgets[item.id] = args.id; // only for menus with children. args.id is item.name
							break;
						case 'commandbarmenubutton':
							args.iconClass = item.iconClass;
							args.baseClass = item.baseClass || 'sidebarButton';
							args.label = item.label || '';
							widget = new dijit.MenuItem(args);
							break;
						case 'commandbarmenucheckbox':
							args.label = item.label || '';
							if (item.additionalData && item.additionalData['cui_checked']) {
								args.checked = true;
							}
							if (item.group) {
								args.group = item.group;
								widget = new RadioButtonMenuItem(args);
							} else {
								widget = new dijit.CheckedMenuItem(args);
							}
							break;
						case 'commandbarmenuseparator':
							widget = new dijit.MenuSeparator(args);
							break;
					}
				}
				catch (e) {
					aras.AlertError('InitSidebar: ' + e);
					widget = null;
				}
				if (!widget) {
					continue;
				}

				this.utils.applyAdditionalData(widget, item.additionalData, false);
				if (item.image) {
					widget.domNode.querySelector('.' + item.iconClass).style.backgroundImage = 'url("' + item.image + '")';
				}

				this.initSidebarItemEvents(widget, item.events);
				if (item.parent) {
					var parentWidget = dijit.byId(widgets[item.parent]);
					if (parentWidget.popup) {
						parentWidget.popup.addChild(widget);
					} else {
						parentWidget.dropDown.addChild(widget);
					}
				} else {
					sidebar.addChild(widget);
				}

				if (item.tooltipTemplate || item.label) {
					this.setTooltip(widget.id, {template: item.tooltipTemplate, label: item.label});
					if (item.lcItemType == 'commandbarmenu') {
						var tooltipWidget = dijit.byId(widget.id + '_tooltip');
						if (tooltipWidget) {
							tooltipWidget.onShow = this.makeOnShowHandlerForCommandBarMenuTooltip(widget, tooltipWidget);
							widget.onClick = this.makeOnClickHandlerForCommandBarMenu(tooltipWidget);
						}
					}
				}
			} // for

			sidebar.set('loaded', true);
		};

		CuiSidebar.prototype._mapHandler = function(widget, item, additionalData, contextParams, handlerName, eventName) {
			var methodName = this.utils.getHandlerName(item, additionalData, handlerName);
			var controlEventParams = this.utils.createClientMethodParams(contextParams);
			this.private.setEventToSidebarItem(widget, eventName, handlerName, controlEventParams);
		};

		CuiSidebar.prototype.initSidebarItemEvents = function(widget, events) {
			var assignHandler = (function(methodName, handlerName, eventName) {
				this.private.setEventToSidebarItem(widget, methodName, eventName, handlerName, events.controlEventParams);
			}).bind(this);

			assignHandler(events.onClickHandler, 'on_click_handler', 'click');
			assignHandler(events.mouseEnter, 'mouse_enter', mouse.enter);
			assignHandler(events.mouseLeave, 'mouse_leave', mouse.leave);
		};

		CuiSidebar.prototype._mapOnClickHandler = function(widget, item, contextParams) {
			this._mapHandler(widget, item, null, contextParams, 'on_click_handler', 'click');
		};

		CuiSidebar.prototype._getEventHandler = function(self, methodName, eventName, controlEventParams) {
			return function() {
				var rtn = self.utils.evalCommandBarItemMethod(methodName, controlEventParams, true);
				if (!rtn || !rtn['cui_event_sent']) {
					var evnt = document.createEvent('Event');
					evnt.locationName = controlEventParams.locationName;
					evnt.changeType = eventName;
					if (controlEventParams.control) {
						evnt.commandBarTarget = controlEventParams.control.id;
					}
					if (controlEventParams['cui_item']) {
						evnt.commandBarTarget = controlEventParams['cui_item'].getProperty('name');
					}
					evnt.initEvent('commandBarChanged', true, false);
					document.dispatchEvent(evnt);
				}
			};
		};

		return CuiSidebar;
	}
);
