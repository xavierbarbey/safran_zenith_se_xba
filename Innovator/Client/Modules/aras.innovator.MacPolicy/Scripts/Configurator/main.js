﻿define([
	'MacPolicy/Scripts/Configurator/MacPolicyDefinitionView',
	'MacPolicy/Scripts/Configurator/MacPolicyFormHelper'
],
function(MacPolicyDefinitionView, MacPolicyFormHelper) {
	'use strict';
	const controller = new MacPolicyDefinitionView(document);
	controller.initialize();
	window.MacPolicyDefinitionView = controller;
	window.MacPolicyFormHelper = new MacPolicyFormHelper(aras);

	const baseSave = window.onSaveCommand;
	const baseLock = window.onLockCommand;
	const baseUnlock = window.onUnlockCommand;
	const baseSaveUnlockAndExit = window.onSaveUnlockAndExitCommand;

	window.onSaveUnlockAndExitCommand = function() {
		if (thisItem.getAction() === 'add') {
			if (!aras.checkItem(thisItem.node)) {
				return;
			}
		}
		const editor = this.MacPolicyDefinitionView.MacConditionEditor;
		if (editor.form.formWindow && editor.form.getItem() && !editor.saveCondition()) {
			return;
		}
		baseSaveUnlockAndExit().then(function(res) {
			return true;
		});
	};

	window.onSaveCommand = function() {
		if (thisItem.getAction() === 'add') {
			if (!aras.checkItem(thisItem.node)) {
				return;
			}
		}
		const editor = this.MacPolicyDefinitionView.MacConditionEditor;
		if (editor.form.formWindow && editor.form.getItem() && !editor.saveCondition()) {
			return;
		}
		return baseSave().then(function(res) {
			return true;
		});
	};

	window.onLockCommand = function() {
		const baseLockResult = baseLock();
		this.MacPolicyDefinitionView.MacConditionEditor.reload();
		return baseLockResult;
	};

	window.onUnlockCommand = function(saveChanges) {
		if (saveChanges) {
			const editor = this.MacPolicyDefinitionView.MacConditionEditor;
			if (editor.form.formWindow && editor.form.getItem() && !editor.saveCondition()) {
				return;
			}
		}
		const baseUnlockResult = baseUnlock(saveChanges);
		this.MacPolicyDefinitionView.MacConditionEditor.reload();
		return baseUnlockResult;
	};

	document.addEventListener('loadSideBar', function() {
		getSidebar().switchSidebarButton('mp_ShowForm', '../Images/ShowFormOn.svg', true);
	});

	onload = function onloadHandler() {
		this.MacPolicyDefinitionView.showForm();
		return;
	};
});
