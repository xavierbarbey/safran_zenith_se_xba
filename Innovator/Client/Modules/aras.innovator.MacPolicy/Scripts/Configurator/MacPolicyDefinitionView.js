﻿define([
	'MacPolicy/Scripts/Configurator/MacConditionEditor'
],function(MacConditionEditor) {
	'use strict';
	const MacPolicyDefinitionView = (function() {
		function MacPolicyDefinitionView(contextDocument) {
			this.isFirstShowOfEditor = true;
			this.contextDocument = contextDocument;
			this.MacConditionEditor = new MacConditionEditor();
		}
		MacPolicyDefinitionView.prototype.initialize = function() {
			this.setVisibleFormContainer(true);
		};
		MacPolicyDefinitionView.prototype.showForm = function() {
			if (!this.isFirstShowOfEditor) {
				window.onRefresh();
			}
			this.setVisibleFormContainer(true);
		};
		MacPolicyDefinitionView.prototype.showEditor = function() {
			if (getIOMItem().getAction() === 'add' && isFirstShowOfEditor) {
				reload().then(function() {
					aras.RefillWindow(getIOMItem().node, window, false);
				});
			}
			this.isFirstShowOfEditor = false;
			this.setVisibleFormContainer(false);
			this.MacConditionEditor.load();
		};
		MacPolicyDefinitionView.prototype.setVisibleFormContainer = function(showForm) {
			const formContainer = this.contextDocument.getElementById('CenterBorderContainer');
			if (formContainer) {
				formContainer.style.display = showForm ? 'flex' : 'none';
			}

			const relationshipsContainer = this.contextDocument.getElementById('relationshipContentPane');
			if (relationshipsContainer) {
				relationshipsContainer.style.display = showForm ? '' : 'none';
			}

			const MacPolicyEditorContainer = this.contextDocument.getElementById('MacPolicyEditorContainer');
			if (MacPolicyEditorContainer) {
				MacPolicyEditorContainer.style.display = showForm ? 'none' : '';
			}
		};
		return MacPolicyDefinitionView;
	}());
	return MacPolicyDefinitionView;
});
