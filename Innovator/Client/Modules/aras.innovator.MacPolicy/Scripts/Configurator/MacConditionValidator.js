﻿define([
	'MacPolicy/Scripts/Configurator/MacConditionVisitors',
	'MacPolicy/Scripts/Configurator/MacCondition',
	'MacPolicy/Scripts/Configurator/MacConditionParser'
],
function(conditionVisitors, macCondition, macConditionParser) {
	'use strict';
	const macConditionValidator = {};

	macConditionValidator.possibleMethods = [
		{
			'name': 'MacPolicy.CurrentUser.IsMemberOf',
			'args': [
				{
					'type': ['constant']
				}
			]
		},
		{
			'name': 'MacPolicy.CurrentUser.IsXPropertyDefined',
			'args': [
				{
					'type': ['constant']
				}
			]
		},
		{
			'name': 'MacPolicy.CurrentUser.IsClassifiedByXClass',
			'args': [
				{
					'type': ['constant']
				}
			]
		},
		{
			'name': 'MacPolicy.CurrentItem.IsXPropertyDefined',
			'args': [
				{
					'type': ['constant']
				}
			]
		},
		{
			'name': 'MacPolicy.CurrentItem.IsClassifiedByXClass',
			'args': [
				{
					'type': ['constant']
				}
			]
		},
		{
			'name': 'MacPolicy.String.Contains',
			'args': [
				{
					'type': ['constant', 'property']
				},
				{
					'type': ['constant', 'property']
				}
			]
		}
	];

	function checkIdentifier(branch) {
		if (branch.name === 'property') {
			if (branch.value === 'CurrentUser' || branch.value === 'CurrentItem') {
				const message = aras.getResource('../Modules/aras.innovator.QueryBuilder/', 'condition_editor.invalid_property_name', branch.value);
				throw {
					'message': message,
					'location': branch.location
				};
			}
		}
		if (branch.children) {
			branch.children.forEach(function(child) {
				checkIdentifier(child);
			});
		}
	}
	macConditionValidator.validate = function(conditionXml, items, conditionsTree) {
		try {
			const result = macConditionParser.parse(conditionXml);
			checkIdentifier(result);
			macCondition.transformAstBeforeSave(result);
			conditionsTree.fillUnknown(result, items, this.possibleMethods);
			conditionsTree.synchronize(result);
			const condition = conditionsTree.toXml();
			return {
				'isSuccess': true,
				'condition': conditionsTree.toXml().xml
			};
		} catch (e) {
			return {
				'isSuccess': false,
				'error': e
			};
		}
	};
	return macConditionValidator;
});
