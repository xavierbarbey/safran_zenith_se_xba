﻿import SvgManager from '../core/SvgManager';

const htmlElementFlag = 2;
const iconClass = 'aras-list__item-icon';

function initContextMenu(conMenuInstance, container) {
	conMenuInstance.dom = container.appendChild(document.createElement('div'));
	conMenuInstance.dom.classList.add('aras-contextMenu');
	conMenuInstance.dom.tabIndex = 0;
	conMenuInstance.roots = [];

	conMenuInstance.dom.addEventListener(
		'blur',
		function() {
			conMenuInstance.dom.classList.remove('aras-contextMenu_opened');
		},
		true
	);
	conMenuInstance.dom.addEventListener(
		'keydown',
		function(e) {
			if (e.keyCode === 27) {
				conMenuInstance.dom.classList.remove('aras-contextMenu_opened');
				e.preventDefault();
				e.stopPropagation();
			}
		},
		true
	);
}

function renderRecursive(conMenuInstance, itemIds) {
	const needIcons = itemIds.some(element => {
		const item = conMenuInstance.data.get(element);
		return !!item.icon;
	});
	const items = itemIds.map(function(value) {
		const item = conMenuInstance.data.get(value);
		let classList = '';
		let childList = item.label ? [item.label] : [];
		if (item.disabled) {
			classList += ' disabled-item';
		}
		if (!item.label) {
			classList += ' separator';
		}
		if (item.children) {
			childList = childList.concat(
				renderRecursive(conMenuInstance, item.children)
			);
			classList += ' aras-list__parent';
		}
		if (needIcons) {
			let iconNode = SvgManager.createInfernoVNode(item.icon);
			if (!iconNode) {
				iconNode = Inferno.createVNode(htmlElementFlag, 'span');
			}
			iconNode.className = iconClass;
			childList.unshift(iconNode);
		}
		return Inferno.createVNode(htmlElementFlag, 'li', classList, childList, {
			'data-index': value
		});
	});
	return Inferno.createVNode(htmlElementFlag, 'ul', 'aras-list', items);
}

function updatePosition(docSize, contextMenu, contextMenuProps) {
	if (contextMenuProps.left + contextMenuProps.width > docSize.width) {
		contextMenu.style.right = '0';
		contextMenu.style.left = 'auto';
	}

	if (contextMenuProps.top + contextMenuProps.height < docSize.height) {
		return;
	}

	contextMenu.style.bottom = '0';
	contextMenu.style.top = 'auto';
	contextMenu.style.overflowY = 'auto';
}

const ContextMenu = function(container) {
	this.data = new Map();
	initContextMenu(this, container || document.body);
};

ContextMenu.prototype = {
	on: function(event, callback) {
		const handler = function(e) {
			const node = e.target;
			if (node.tagName.toLowerCase() === 'li') {
				const elementId = node.dataset.index;
				callback(elementId, e, this.args);
				this.dom.classList.remove('aras-contextMenu_opened');
			}
		}.bind(this);
		this.dom.addEventListener(event, handler);
		return function() {
			this.dom.removeEventListener(event, handler);
		}.bind(this);
	},
	show: function(coords, args) {
		if (this.roots.length) {
			Inferno.render(renderRecursive(this, this.roots), this.dom);
		} else {
			const keys = [];
			this.data.forEach(function(element, key) {
				keys.push(key);
			});
			Inferno.render(renderRecursive(this, keys), this.dom);
		}

		this.args = args;
		this.dom.classList.add('aras-contextMenu_opened');
		this.dom.style.top = coords.y + 'px';
		this.dom.style.left = coords.x + 'px';
		this.dom.style.maxHeight = '100%';
		this.dom.style.right = 'auto';
		this.dom.style.bottom = 'auto';
		this.dom.style.overflowY = 'visible';
		const docElement = document.documentElement;
		const docSize = {
			width: docElement.clientWidth,
			height: docElement.clientHeight
		};
		const contextMenuProps = {
			top: coords.y,
			left: coords.x,
			width: this.dom.clientWidth,
			height: this.dom.clientHeight
		};
		updatePosition(docSize, this.dom, contextMenuProps);

		return new Promise(
			function(resolve) {
				setTimeout(
					function() {
						this.dom.focus();
						resolve();
					}.bind(this),
					0
				);
			}.bind(this)
		);
	},
	applyData: function(data) {
		const applyRecursive = function(data) {
			const ids = Object.keys(data);
			let children;
			for (let i = 0; i < ids.length; i++) {
				if (data[ids[i]].children) {
					children = applyRecursive.call(this, data[ids[i]].children);
				}
				this.data.set(ids[i], {
					label: data[ids[i]].label,
					disabled: data[ids[i]].disabled,
					children: children
				});
				children = null;
			}
			return ids;
		}.bind(this);

		this.roots = applyRecursive(data);
	}
};

export default ContextMenu;
