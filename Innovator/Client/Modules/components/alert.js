﻿import Dialog from '../core/Dialog';
import xml from '../core/Xml';
import copyTextToBuffer from '../core/copyTextToBuffer';

function descriptionContainerClickListener(event) {
	const cssClass = 'aml-description';
	let clicableRow;
	if (
		event.target.parentElement.classList.contains(cssClass + '__clicable-row')
	) {
		clicableRow = event.target.parentElement;
	} else if (event.target.classList.contains(cssClass + '__clicable-row')) {
		clicableRow = event.target;
	} else {
		return;
	}
	const tagBlocks = clicableRow.nextElementSibling.children;
	const link = clicableRow.querySelector('.' + cssClass + '__plus-or-minus');
	link.textContent = link.textContent === '+' ? '-' : '+';
	Array.prototype.forEach.call(tagBlocks, function(itemForTogle) {
		if (itemForTogle.classList.contains(cssClass + '__tag-block')) {
			itemForTogle.classList.toggle('aras-hide');
		}
	});
}

const stackAndTechnicalMessageType = {
	get img() {
		return aras.getBaseURL('/images/Error.svg');
	},
	title: null,
	copyCustomizator: function(copyButton, message, alertOptions) {
		let textMessage = message + ' ';
		textMessage += alertOptions.technicalMessage + ' ';
		textMessage += alertOptions.stackTrace;

		copyButton.addEventListener(
			'click',
			function() {
				copyTextToBuffer(this.text, this.container);
			}.bind({ text: textMessage, container: copyButton.parentElement })
		);
	},

	descriptionCustomizator: function(
		descriptionContainer,
		message,
		alertOptions
	) {
		const cssClassName = 'aras-dialog-alert__container';
		descriptionContainer.classList.add(cssClassName + '-dark');

		const firstConatiner = document.createElement('div');
		const secondConatiner = document.createElement('div');

		let titleBlock = document.createElement('span');
		titleBlock.textContent = aras.getResource(
			'',
			'aras_object.technical_message'
		);
		titleBlock.classList.add('aras-dialog-alert__title');

		let textBlock = document.createElement('span');
		textBlock.classList.add('aras-dialog-alert__text-description');
		textBlock.textContent = alertOptions.technicalMessage;

		firstConatiner.appendChild(titleBlock);
		firstConatiner.appendChild(textBlock);

		descriptionContainer.appendChild(firstConatiner);

		titleBlock = document.createElement('span');
		titleBlock.textContent = aras.getResource('', 'aras_object.stack_trace');
		titleBlock.classList.add('aras-dialog-alert__title');

		textBlock = document.createElement('span');
		textBlock.classList.add('aras-dialog-alert__text-description');
		textBlock.textContent = alertOptions.stackTrace;

		secondConatiner.appendChild(titleBlock);
		secondConatiner.appendChild(textBlock);

		descriptionContainer.appendChild(secondConatiner);
	}
};

const soapResultType = {
	get img() {
		return aras.getBaseURL('/images/Error.svg');
	},
	title: null,
	messageCustomizator: function(messageNode, message, alertOptions) {
		if (alertOptions.data.getParseError()) {
			messageNode.textContent = aras.getResource(
				'',
				'aras_object.invalid_soap_message'
			);
		} else {
			messageNode.textContent = alertOptions.data.getFaultString();
		}
	},

	copyCustomizator: function(copyButton, message, alertOptions) {
		copyButton.addEventListener(
			'click',
			function() {
				copyTextToBuffer(this.text, this.container);
			}.bind({
				text: alertOptions.data.resultsXML,
				container: copyButton.parentElement
			})
		);
	},

	descriptionCustomizator: function(
		descriptionContainer,
		message,
		alertOptions
	) {
		const xsldoc = aras.createXMLDocument();
		xsldoc.load(aras.getBaseURL() + '/styles/amlDescription.xsl');
		descriptionContainer.innerHTML = xml.transform(
			alertOptions.data.results,
			xsldoc
		);

		descriptionContainer.addEventListener(
			'click',
			descriptionContainerClickListener
		);
	}
};

const iomInfoType = {
	get img() {
		return aras.getBaseURL('/images/Error.svg');
	},
	title: null,
	messageCustomizator: function(messageNode, message, alertOptions) {
		messageNode.textContent = alertOptions.data.getErrorString();
	},

	copyCustomizator: function(copyButton, message, alertOptions) {
		copyButton.addEventListener(
			'click',
			function() {
				copyTextToBuffer(this.text, this.container);
			}.bind({
				text: alertOptions.data.dom.xml,
				container: copyButton.parentElement
			})
		);
	},

	descriptionCustomizator: function(
		descriptionContainer,
		message,
		alertOptions
	) {
		const xsldoc = aras.createXMLDocument();
		xsldoc.load(aras.getBaseURL() + '/styles/amlDescription.xsl');
		descriptionContainer.innerHTML = xml.transform(
			alertOptions.data.dom,
			xsldoc
		);

		descriptionContainer.addEventListener(
			'click',
			descriptionContainerClickListener
		);
	}
};

const infoAboutAlertTypes = {
	warning: {
		get img() {
			return aras.getBaseURL('/images/Warning.svg');
		},
		title: null
	},
	error: {
		get img() {
			return aras.getBaseURL('/images/Error.svg');
		},
		title: null
	},
	success: {
		img: '../images/Message.svg',
		title: null
	},
	soap: soapResultType,
	stack: stackAndTechnicalMessageType,
	iom: iomInfoType
};

function alertModule(message, options) {
	if (!infoAboutAlertTypes.error.title) {
		const defaultErrorTitle = aras.getResource('', 'aras_object.error');
		infoAboutAlertTypes.warning.title = aras.getResource('', 'common.warning');
		infoAboutAlertTypes.error.title = defaultErrorTitle;
		infoAboutAlertTypes.soap.title = defaultErrorTitle;
		infoAboutAlertTypes.stack.title = defaultErrorTitle;
		infoAboutAlertTypes.iom.title = defaultErrorTitle;
		infoAboutAlertTypes.success.title = aras.getResource(
			'',
			'aras_object.aras_innovator'
		);
	}

	message = message || '';
	options = options || {};
	let curentTypeAlert =
		infoAboutAlertTypes[options.type] || infoAboutAlertTypes.error;
	if (options.customization) {
		curentTypeAlert = Object.assign({}, curentTypeAlert, options.customization);
	}

	const alertDialog = new Dialog('html', {
		title: curentTypeAlert.title
	});

	const cssClassName = 'aras-dialog-alert';
	alertDialog.dialogNode.classList.add(cssClassName);

	const messageContainer = document.createElement('div');
	messageContainer.classList.add(cssClassName + '__container');
	alertDialog.contentNode.appendChild(messageContainer);

	const image = document.createElement('img');
	image.classList.add(cssClassName + '__img');
	image.src = curentTypeAlert.img;
	messageContainer.appendChild(image);

	const messageNode = document.createElement('span');
	messageNode.classList.add(cssClassName + '__text');
	messageNode.textContent = message;
	messageContainer.appendChild(messageNode);

	const btnContainer = document.createElement('div');
	btnContainer.classList.add(cssClassName + '__container');

	const buttonOk = document.createElement('button');
	buttonOk.autofocus = true;
	buttonOk.classList.add('aras-btn', 'aras-btn-right');
	buttonOk.textContent = aras.getResource('', 'common.ok');
	buttonOk.addEventListener('click', alertDialog.close.bind(alertDialog, null));

	btnContainer.appendChild(buttonOk);
	alertDialog.contentNode.appendChild(btnContainer);

	if (curentTypeAlert.messageCustomizator) {
		curentTypeAlert.messageCustomizator(messageNode, message, options);
	}
	let buttonCopy;

	if (curentTypeAlert.descriptionCustomizator) {
		const showTxt = aras.getResource('', 'aras_object.show_details');
		const hideTxt = aras.getResource('', 'aras_object.hide_details');

		const descriptionContainer = document.createElement('div');
		descriptionContainer.classList.add(
			cssClassName + '__container',
			cssClassName + '__container-aml',
			'aras-hide'
		);

		const buttonDescriptionSwitch = document.createElement('button');
		buttonDescriptionSwitch.classList.add(
			'aras-btn',
			'aras-btn_secondary',
			'aras-btn-right'
		);
		buttonDescriptionSwitch.textContent = showTxt;

		buttonDescriptionSwitch.addEventListener(
			'click',
			function() {
				if (!this.container.hasChildNodes()) {
					curentTypeAlert.descriptionCustomizator(
						this.container,
						message,
						options
					);
				}
				const isHidden = this.container.classList.contains('aras-hide');
				this.btn.textContent = isHidden ? this.hideTxt : this.showTxt;
				buttonCopy.style.display = isHidden ? 'inline-block' : 'none';
				this.container.classList.toggle('aras-hide');
			}.bind({
				btn: buttonDescriptionSwitch,
				container: descriptionContainer,
				showTxt: showTxt,
				hideTxt: hideTxt
			})
		);

		alertDialog.contentNode.appendChild(descriptionContainer);
		btnContainer.appendChild(buttonDescriptionSwitch);
	}

	if (curentTypeAlert.copyCustomizator) {
		buttonCopy = document.createElement('button');
		buttonCopy.classList.add('aras-btn', 'aras-btn_secondary');
		buttonCopy.textContent = aras.getResource('', 'aras_object.copy_buffer');
		buttonCopy.style.display = 'none';
		btnContainer.appendChild(buttonCopy);
		curentTypeAlert.copyCustomizator(buttonCopy, message, options);
	}

	alertDialog.show();
	return alertDialog.promise;
}

export default alertModule;
