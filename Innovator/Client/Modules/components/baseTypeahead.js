﻿import utils from '../core/utils';

function BaseTypeahead(...arg) {
	const instance = utils.extendHTMLElement(this, arg);
	this.initialize.call(instance);
	return instance;
}

BaseTypeahead.prototype = Object.assign(Object.create(HTMLElement.prototype), {
	constructor: BaseTypeahead,

	initialize: function() {
		this.state = Object.assign({
			list: [],
			label: null,
			_value: null,
			shown: false,
			focus: false,
			validation: false,
			invalid: false,
			oldLabel: null,
			refs: {},
			dom: this
		});
	},

	setState: function(next) {
		this.state = Object.assign(this.state || {}, next);
		this.render();
	},

	_onInputHandler: function(e) {
		const newLabel = e.target.value;
		const prevLabel =
			this.state.label === null
				? this._getCurrentInputValue()
				: this.state.label;
		const oldLabel = this.state.oldLabel;

		this.setState({
			oldLabel: oldLabel === null ? prevLabel : oldLabel,
			label: newLabel,
			focus: true,
			shown: newLabel !== ''
		});

		this.dispatchEvent(new CustomEvent('input', { bubbles: true }));
	},

	_onInputFocusoutHandler: function(e) {
		if (e.relatedTarget === this.state.refs.dropdown) {
			e.preventDefault();
			e.stopPropagation();
			this.setFocus();
			return;
		}

		const label = this.state.label;
		const oldLabel = this.state.oldLabel;

		if (label !== oldLabel) {
			this.state.refs.input.dispatchEvent(
				new CustomEvent('change', { bubbles: true })
			);
		}

		this.setState({
			shown: false,
			focus: false,
			oldLabel: null
		});
	},

	_onButtonClickHandler: function(e) {
		this.setState({
			shown: !this.state.shown,
			focus: true
		});
		e.preventDefault();
	},

	_setPosition: function() {
		const windowHeight = document.documentElement.clientHeight;
		const inputHeight = this.state.refs.input.clientHeight + 1;
		const containerOffsets = this.state.dom.getBoundingClientRect();
		const topOffsetInWindow = containerOffsets.top;
		const bottomOffsetInWindow =
			windowHeight - containerOffsets.top - inputHeight;

		const parentNode = this._findParent();
		const parentNodeOffsets = parentNode.getBoundingClientRect();
		const topOffsetInParent = containerOffsets.top - parentNodeOffsets.top;
		const bottomOffsetInParent =
			parentNode.scrollHeight - topOffsetInParent - inputHeight;

		const topOffset = Math.min(topOffsetInWindow, topOffsetInParent);
		const bottomOffset = Math.min(bottomOffsetInWindow, bottomOffsetInParent);

		const dropdownHeight = Math.min(
			this._getDropdownHeight(),
			Math.max(bottomOffset, topOffset)
		);
		const down = bottomOffset >= dropdownHeight || bottomOffset >= topOffset;

		this.state.maxDropdownHeight = dropdownHeight + 2;
		this.state.errorPosition =
			bottomOffset > 40 || bottomOffset >= topOffset ? 'down' : 'top';

		return down ? inputHeight : -dropdownHeight;
	},

	_getDropdownHeight: function() {
		return 0;
	},

	_checkShown: function() {
		return this.state.shown;
	},

	_getInputTemplate: function() {
		const label = this._getCurrentInputValue();

		return {
			tag: 'input',
			className: 'aras-filter-list__input aras-form-input',
			attrs: {
				value: label,
				autocomplete: 'off',
				disabled: this.state.disabled,
				readOnly: this.state.readonly
			},
			events: {
				oninput: this._onInputHandler.bind(this)
			},
			ref: function(node) {
				this.state.refs.input = node;
			}.bind(this)
		};
	},

	_getCurrentInputValue: function() {
		return this.state.label || '';
	},

	_getButtonTemplate: function() {
		return {
			tag: 'button',
			attrs: {
				disabled: this.state.disabled || this.state.readonly,
				tabIndex: -1
			},
			className:
				'aras-filter-list__button aras-btn aras-icon-arrow aras-icon-arrow_down',
			events: {
				onmousedown: this._onButtonClickHandler.bind(this)
			},
			ref: function(node) {
				this.state.refs.button = node;
			}.bind(this)
		};
	},

	_getIconContainerTemplate: function() {
		return {
			tag: 'div',
			className: 'aras-filter-list__icon-container',
			children: [
				{
					tag: 'span',
					className: 'aras-filter-list-icon aras-icon-error',
					attrs: {
						style: {
							display: this.state.invalid ? 'flex' : 'none'
						}
					}
				}
			]
		};
	},

	_findParent: function() {
		let node = this.state.dom;
		let styles;

		do {
			node = node.parentNode;
			styles = window.getComputedStyle(node);
		} while (
			node !== document.body &&
			styles['overflow-y'] !== 'hidden' &&
			node.parentNode
		);

		return node;
	},

	_getDropdownTemplate: function() {
		const shown = this._checkShown();

		const displayCssClass = shown ? ' aras-dropdown_opened' : '';

		return {
			tag: 'div',
			className: 'aras-filter-list__dropdown aras-dropdown' + displayCssClass,
			attrs: {
				tabIndex: 1
			},
			children: [],
			style: {
				display: this.state.width === 'auto' || shown ? 'block' : 'none',
				top: shown ? this._setPosition() : 0,
				maxHeight: this.state.maxDropdownHeight
			},
			ref: function(node) {
				this.state.refs.dropdown = node;
			}.bind(this)
		};
	},

	_calculateWidth: function() {
		if (this.state.width !== 'auto') {
			return;
		}

		const buttonWidth = this.state.refs.button.offsetWidth;
		const dropdownWidth = this.state.refs.dropdown.offsetWidth;
		const width = (dropdownWidth || 0) + buttonWidth + 'px';

		if (this.state.width !== width) {
			this.state.width = width;
			this._render();
		}
	},

	getTemplate: function() {
		if (!this.state) {
			this.initialize();
		}

		const button = this._getButtonTemplate();
		const icons = this._getIconContainerTemplate();
		const dropdown = this._getDropdownTemplate();
		const input = this._getInputTemplate();

		const wrapper = {
			tag: 'div',
			className: 'aras-filter-list aras-dropdown-container',
			children: [input, icons, button, dropdown],
			style: {
				width: this.state.width
			}
		};

		const validation = this.state.validation;

		if (validation) {
			if (this.state.invalid) {
				this._setPosition();
			}

			wrapper.className += ' aras-tooltip';
			wrapper.attrs = {
				'data-tooltip-show': this.state.invalid,
				'data-tooltip': aras.getResource('', 'components.value_invalid'),
				'data-tooltip-pos': this.state.errorPosition
			};
		}

		return (this.format && this.format(wrapper)) || wrapper;
	},

	componentFunction: function(props) {
		const self = props.self;
		return utils.templateToVNode(self.getTemplate(self));
	},

	component: function() {
		const self = this;
		const componentFunctionFlag = utils.infernoFlags.componentFunction;
		const bindedHandler = self._onInputFocusoutHandler.bind(self);

		return Inferno.createVNode(
			componentFunctionFlag,
			this.componentFunction,
			null,
			null,
			{ self: self },
			null,
			{
				onComponentDidMount: function() {
					self.state.refs.input.addEventListener('focusout', bindedHandler);
				},
				onComponentWillUnmount: function() {
					self.state.refs.input.removeEventListener('focusout', bindedHandler);
				}
			}
		);
	},

	setFocus: function() {
		return new Promise(
			function(resolve) {
				if (!this.state.focus) {
					resolve();
					return;
				}

				setTimeout(
					function() {
						const input = this.state.refs.input;
						if (document.activeElement === input) {
							resolve();
							return;
						}
						input.focus();
						resolve();
					}.bind(this),
					0
				);
			}.bind(this)
		);
	},

	_render: function() {
		Inferno.render(this.component(), this.state.dom);
		this._calculateWidth();

		return Promise.resolve();
	},

	render: function() {
		return this._render().then(this.setFocus.bind(this));
	},

	connectedCallback: function() {
		this.render();
	},

	attributeChangedCallback: function(attr, oldValue, newValue) {
		if (this.state[attr] === newValue) {
			return;
		}
		const changes = {};
		changes[attr] = newValue;
		this.setState(changes);
	}
});

BaseTypeahead.observedAttributes = ['value', 'label'];

export default BaseTypeahead;
