﻿const defaults = {
	pos: 'right-top',
	closeOnClick: false
};
const OPENED_CLASS_NAME = 'aras-dropdown_opened';

const getDropdownSize = function(element) {
	element.style.visibility = 'hidden';
	const size = {
		width: element.offsetWidth,
		height: element.offsetHeight
	};
	element.style.visibility = '';

	return size;
};

const updatePosition = function(parentProps, element) {
	const elmRect = element.getBoundingClientRect();
	const elementBottom = element.offsetHeight + parentProps.offsetTop;
	const docElement = document.documentElement;
	const docSize = {
		width: docElement.clientWidth,
		height: docElement.clientHeight
	};

	if (elmRect.right > docSize.width) {
		element.style.left =
			parseInt(element.style.left) - (elmRect.right - docSize.width) + 'px';
	}
	if (elementBottom > parentProps.height) {
		element.style.top =
			parseInt(element.style.top) - (elementBottom - parentProps.height) + 'px';
	}
	if (elmRect.left < 0) {
		element.style.left =
			parseInt(element.style.left) + Math.abs(elmRect.left) + 'px';
	}
	if (parentProps.offsetTop < 0) {
		element.style.top =
			parseInt(element.style.top) + Math.abs(parentProps.offsetTop) + 'px';
	}
};

const getSourcePosition = function(pos, dropdownSize, buttonSize) {
	switch (pos) {
		case 'bottom-left':
			return { top: 0 + buttonSize.height, left: 0 };
		case 'bottom-right':
			return {
				top: 0 + buttonSize.height,
				left: 0 + buttonSize.width - dropdownSize.width
			};
		case 'bottom-center':
			return {
				top: 0 + buttonSize.height,
				left: 0 + buttonSize.width / 2 - dropdownSize.width / 2
			};
		case 'top-left':
			return { top: 0 - dropdownSize.height, left: 0 };
		case 'top-right':
			return {
				top: 0 - dropdownSize.height,
				left: 0 + buttonSize.width - dropdownSize.width
			};
		case 'top-center':
			return {
				top: 0 - dropdownSize.height,
				left: 0 + buttonSize.width / 2 - dropdownSize.width / 2
			};
		case 'left-top':
			return { top: 0, left: 0 - dropdownSize.width };
		case 'left-bottom':
			return {
				top: 0 + buttonSize.height - dropdownSize.height,
				left: 0 - dropdownSize.width
			};
		case 'left-center':
			return {
				top: 0 + buttonSize.height / 2 - dropdownSize.height / 2,
				left: 0 - dropdownSize.width
			};
		case 'right-top':
			return { top: 0, left: 0 + buttonSize.width };
		case 'right-bottom':
			return {
				top: 0 + buttonSize.height - dropdownSize.height,
				left: 0 + buttonSize.width
			};
		case 'right-center':
			return {
				top: 0 + buttonSize.height / 2 - dropdownSize.height / 2,
				left: 0 + buttonSize.width
			};
	}
};

const getOffsetParentProps = function(container, offsetTop) {
	offsetTop += container.offsetTop;
	const offsetParent = container.offsetParent;
	if (!offsetParent) {
		const docElement = document.documentElement;
		return {
			offsetTop: offsetTop,
			width: docElement.clientWidth,
			height: docElement.clientHeight
		};
	}

	const elementOverflow = getComputedStyle(offsetParent).overflow;
	if (elementOverflow === 'hidden') {
		return {
			offsetTop: offsetTop,
			width: offsetParent.clientWidth,
			height: offsetParent.clientHeight
		};
	}

	return getOffsetParentProps(offsetParent, offsetTop);
};

function dropdown(container, settings) {
	if (!container) {
		return false;
	}

	const currentSettings = Object.assign({}, defaults, settings);

	const dropdownBox =
		container.querySelector('.aras-dropdown') || container.lastElementChild;
	const buttonNode =
		container.querySelector(currentSettings.buttonSelector) ||
		dropdownBox.previousElementSibling;

	if (!(buttonNode && dropdownBox)) {
		return false;
	}

	dropdownBox.tabIndex = 0;

	const buttonSize = {
		width: buttonNode.offsetWidth,
		height: buttonNode.offsetHeight
	};

	const toggleDropdown = function(e) {
		if (
			e.target === buttonNode ||
			(currentSettings.closeOnClick && e.target !== container)
		) {
			const parentProps = getOffsetParentProps(container, 0);

			dropdownBox.style.maxHeight = 'none';
			dropdownBox.style.overflowY = 'visible';
			dropdownBox.classList.toggle(OPENED_CLASS_NAME);
			if (dropdownBox.classList.contains(OPENED_CLASS_NAME)) {
				dispatchCustomEvent('dropdownbeforeopen');
				const dropdownSize = getDropdownSize(dropdownBox);
				const surcePosition = getSourcePosition(
					currentSettings.pos,
					dropdownSize,
					buttonSize
				);
				const leftPos = surcePosition.left + 'px';
				const topPos = surcePosition.top + 'px';
				if (parentProps.height < dropdownBox.clientHeight) {
					dropdownBox.style.maxHeight = parentProps.height + 'px';
					dropdownBox.style.overflowY = 'auto';
				}
				if (
					dropdownBox.style.left !== leftPos ||
					dropdownBox.style.top !== topPos
				) {
					dropdownBox.style.left = leftPos;
					dropdownBox.style.top = topPos;
				}
				parentProps.offsetTop += dropdownBox.offsetTop;
				updatePosition(parentProps, dropdownBox);

				dropdownBox.addEventListener('blur', closeDropdown, true);
				dispatchCustomEvent('dropdownopened');
				// this timeout need for MS IE11, because it needs lazy focus,
				// http://stackoverflow.com/questions/2600186/focus-doesnt-work-in-ie
				setTimeout(function() {
					dropdownBox.focus();
				}, 0);
			}
		}
	};
	const dispatchCustomEvent = function(type) {
		const evt = new CustomEvent(type, { bubbles: true, cancelable: true });
		dropdownBox.dispatchEvent(evt);
	};

	const closeDropdown = function(e) {
		if (
			dropdownBox.contains(e.relatedTarget) ||
			dropdownBox.contains(document.activeElement)
		) {
			return;
		}

		dropdownBox.removeEventListener('blur', closeDropdown);
		dropdownBox.classList.remove(OPENED_CLASS_NAME);
		dispatchCustomEvent('dropdownclosed');
	};

	// Using 'mousedown' event instead of 'click' event need, because 'blur' event triggers before the 'click' event
	// and closes dropdown, after that 'click' event triggered and opens dropdown again
	container.addEventListener('mousedown', toggleDropdown);

	return true;
}

export default dropdown;
