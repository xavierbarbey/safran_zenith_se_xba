﻿import SvgManager from '../core/SvgManager';
import DragController from './dragTabController';

const HTML_ELEMENT = 2;
const COMPONENT_FUNCTION = 8;
const tabHelper = {
	controlByScroll: function(elem, movable) {
		const scrollRight =
			movable.scrollWidth - movable.offsetWidth - movable.scrollLeft;
		elem.classList.toggle('aras-tabs_moved-right', scrollRight > 0);
		elem.classList.toggle('aras-tabs_moved-left', movable.scrollLeft > 0);
	},
	scrollLeft: function(elem, movable) {
		movable.scrollLeft = Math.max(0, movable.scrollLeft - movable.clientWidth);
		elem.classList.toggle('aras-tabs_moved-left', movable.scrollLeft !== 0);
		elem.classList.add('aras-tabs_moved-right');
	},
	scrollRight: function(elem, movable) {
		const diffWidth = movable.scrollWidth - movable.clientWidth;
		movable.scrollLeft = Math.min(
			movable.scrollLeft + movable.clientWidth,
			diffWidth
		);
		elem.classList.toggle(
			'aras-tabs_moved-right',
			movable.scrollLeft !== diffWidth
		);
		elem.classList.add('aras-tabs_moved-left');
	},
	scrollIntoView: function(elem, movable, tab) {
		const tabStyle = window.getComputedStyle(tab);
		const leftMargin = parseInt(tabStyle.marginLeft);
		const rightMargin = parseInt(tabStyle.marginRight);
		const tabOffset = tab.offsetLeft - movable.offsetLeft;
		const rightOffset = tabOffset + tab.offsetWidth - movable.clientWidth;

		if (rightOffset + rightMargin - movable.scrollLeft > 0) {
			// Firefox and IE have float tab coordinates: adding 1px to fix offset and count right scrollLeft
			movable.scrollLeft = rightOffset + rightMargin + 1;
		} else if (tabOffset - leftMargin - movable.scrollLeft < 0) {
			movable.scrollLeft = tabOffset - leftMargin;
		}
	},
	scrollByTab: function(elem, movable, tab) {
		if (movable.scrollWidth !== movable.clientWidth) {
			tabHelper.scrollIntoView(elem, movable, tab);
			tabHelper.controlByScroll(elem, movable);
			tabHelper.scrollIntoView(elem, movable, tab);
		}
	}
};

const close = function(tabInstance, event) {
	const target = event.target;
	if (target.className === 'aras-icon-close') {
		const tab = target.parentNode;
		tabInstance.removeTab(tab.getAttribute('data-id'));
		event.stopPropagation();
	}
};
const wheelClose = function(tabInstance, event) {
	const target = event.target;
	// Check if wheel(middle button) is clicked
	if (event.button === 1) {
		const tab = target.closest('li');
		if (tab) {
			const id = tab.getAttribute('data-id');
			const itemData = tabInstance.data.get(id);
			if (itemData && itemData.closable) {
				tabInstance.removeTab(id);
			}
		}
	}
};

function getImage(url) {
	SvgManager.load([url]);
	return SvgManager.createInfernoVNode(url);
}

function createListItemContent(props) {
	const content = [];
	if (props.image) {
		content.push(getImage(props.image));
	}
	if (props.closable) {
		content.push(Inferno.createVNode(HTML_ELEMENT, 'span', 'aras-icon-close'));
	}
	return content;
}

function buildTab(data) {
	const classList =
		(data.selected === data.idx ? 'aras-tabs_active' : '') +
		(data.data.closable ? ' aras-tabs__closabe' : '') +
		(data.data.disabled ? ' aras-tabs__disabled' : '');
	const listItem = {
		'data-id': data.idx,
		title: data.data.label,
		draggable: data.data.draggable
	};
	let itemContent = (data.customizator && data.customizator(data.idx)) || [];
	itemContent = itemContent.concat(
		createListItemContent(data.data),
		data.data.label
	);
	return Inferno.createVNode(
		HTML_ELEMENT,
		'li',
		classList,
		itemContent,
		listItem
	);
}

const componentLifecycle = {
	onComponentShouldUpdate: function(lastProps, nextProps) {
		return (
			(lastProps.selected !== nextProps.selected &&
				(lastProps.selected === lastProps.idx ||
					nextProps.selected === nextProps.idx)) ||
			lastProps.data !== nextProps.data
		);
	}
};

function Tab(elem) {
	this.data = new Map();
	this.tabs = [];
	this.selectedTab = 'home';
	this._elem = elem;
	this.draggableTabs = false;

	this._movable = this._elem.querySelector('div');
	if (this._movable) {
		this.makeScroll();
	}
	this.makeSelectable();
}

const TabPrototype = {
	constructor: Tab,
	_getImage: getImage,
	makeScroll: function() {
		// Binded handlers
		const controlByScrollBinded = tabHelper.controlByScroll.bind(
			null,
			this._elem,
			this._movable
		);
		const moveScrollLeftBinded = tabHelper.scrollLeft.bind(
			null,
			this._elem,
			this._movable
		);
		const moveScrollRightBinded = tabHelper.scrollRight.bind(
			null,
			this._elem,
			this._movable
		);

		// Attach Events on elements
		window.addEventListener('resize', controlByScrollBinded);
		this._elem.firstElementChild.addEventListener(
			'click',
			moveScrollLeftBinded
		);
		this._elem.lastElementChild.addEventListener(
			'click',
			moveScrollRightBinded
		);

		tabHelper.controlByScroll(this._elem, this._movable);
	},
	removeTab: function(id) {
		const listItem = this.data.get(id);
		if (listItem) {
			this.data.delete(id);
			const itemIndex = this.tabs.indexOf(id);
			this.tabs.splice(itemIndex, 1);

			if (this.selectedTab === id) {
				const siblingItemId = this.data.has(listItem.parentTab)
					? listItem.parentTab
					: null;
				this.selectTab(siblingItemId);
			}

			this.render();

			if (this._movable) {
				tabHelper.controlByScroll(this._elem, this._movable);
			}
		}
		return this.renderPromise || Promise.resolve();
	},
	makeSelectable: function() {
		const select = function(event) {
			if (event.button === 0) {
				const target = event.target;
				const tab = target.closest('li');

				if (tab) {
					this.selectTab(tab.getAttribute('data-id'));
				}
			}
		}.bind(this);
		this._elem.addEventListener('click', select);
	},
	makeDraggable: function() {
		new DragController(this, tabHelper);
		this.draggableTabs = true;
	},
	selectTab: function(id) {
		this.selectedTab = id;
		return this.render().then(
			function() {
				const tab = this._elem.querySelector('.aras-tabs_active');
				if (tab) {
					this.scrollIntoView(tab);
				}
			}.bind(this)
		);
	},
	setTabContent: function(id, props) {
		if (id) {
			const item = this.data.get(id);
			if (item) {
				this.data.set(id, Object.assign({}, item, props));
			}
			return this.render();
		}
		return this.renderPromise || Promise.resolve();
	},
	addTab: function(id, props) {
		if (id) {
			this.tabs.push(id);
			this.data.set(
				id,
				Object.assign(
					{
						closable: false,
						parentTab: this.selectedTab,
						draggable: this.draggableTabs
					},
					props
				)
			);
			return this.render();
		}
		return this.renderPromise || Promise.resolve();
	},
	scrollIntoView: function(tab) {
		if (this._movable) {
			tabHelper.scrollByTab(this._elem, this._movable, tab);
		}
	},
	render: function() {
		if (this.renderPromise) {
			return this.renderPromise;
		}

		this.renderPromise = new Promise(
			function(resolve) {
				setTimeout(
					function() {
						const listItems = this.tabs.map(
							function(itm) {
								const props = {
									data: this.data.get(itm),
									idx: itm,
									selected: this.selectedTab,
									customizator: this.tabCustomizator
								};
								return Inferno.createVNode(
									COMPONENT_FUNCTION,
									buildTab,
									null,
									null,
									props,
									null,
									componentLifecycle
								);
							}.bind(this)
						);

						const list = Inferno.createVNode(
							HTML_ELEMENT,
							'ul',
							'',
							listItems,
							{
								onclick: close.bind({}, this),
								onmouseup: wheelClose.bind({}, this)
							}
						);

						resolve(Inferno.render(list, this._movable || this._elem));
						this.renderPromise = null;
					}.bind(this),
					0
				);
			}.bind(this)
		);

		return this.renderPromise;
	},
	on: function(eventType, callback) {
		const self = this;
		const handler = function(event) {
			const tabId = event.target.dataset && event.target.dataset.id;
			if (tabId) {
				callback(tabId, event);
			}
		};
		this._elem.addEventListener(eventType, handler);
		return function() {
			self._elem.removeEventListener(eventType, handler);
		};
	}
};

Tab.prototype = TabPrototype;
export default Tab;
