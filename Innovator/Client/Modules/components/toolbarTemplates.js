﻿import utils from '../core/utils';
import SvgManager from '../core/SvgManager';
import dropdown from './dropdown';

const infernoFlags = utils.infernoFlags;

const TOOLBAR_CLASS = 'aras-toolbar';
const TOOLBAR_LABELED_CLASS = 'aras-toolbar_labeled';
const CONTENT_CLASS = 'aras-toolbar__content';
const CONTAINER_CLASS = 'aras-toolbar__container';
const DROPDOWN_CLASS = 'aras-toolbar__dropdown';
const DROPDOWN_BUTTON_CLASS =
	'aras-toolbar__dropdown-button aras-icon-arrow aras-icon-arrow_down';
const DROPDOWN_LIST_CONTAINER_CLASS = 'aras-dropdown';
const DROPDOWN_OPENED_LIST_CONTAINER_CLASS = 'aras-dropdown_opened';
const DROPDOWN_LIST_CLASS = 'aras-list';

const BUTTON_CLASS = 'aras-toolbar__button';
const SEPARATOR_CLASS = 'aras-toolbar__separator';
const FORM_CLASS = 'aras-toolbar__form aras-form';
const DATE_CLASS = 'aras-form-date';
const SINGULAR_CLASS = 'aras-form-singular';
const DISABLED_CLASS = 'aras-toolbar_disabled';
const HIDDEN_CLASS = 'aras-hide';

const defaultTemplates = {
	root: function(data) {
		const templatesObj = this;
		const leftContainerItems = this.items(
			data.leftContainerItems || [],
			templatesObj,
			data.options,
			data.toolbar
		);
		const rightContainerItems = this.items(
			data.rightContainerItems || [],
			templatesObj,
			data.options,
			data.toolbar
		);
		const dropdownItems = this.items(
			data.dropdownItems || [],
			templatesObj,
			data.options,
			data.toolbar
		);

		const leftContainerNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			CONTAINER_CLASS,
			leftContainerItems
		);
		const rightContainerNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			CONTAINER_CLASS,
			rightContainerItems
		);
		const contentNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			CONTENT_CLASS,
			[leftContainerNode, rightContainerNode]
		);

		let dropdownNode;
		if (dropdownItems.length > 0) {
			dropdownNode = Inferno.createVNode(
				infernoFlags.componentFunction,
				this.dropdownNode,
				null,
				null,
				{
					items: dropdownItems,
					options: data.options
				},
				null,
				{
					onComponentDidMount: function(domNode) {
						dropdown(domNode, { pos: 'bottom-right', closeOnClick: false });
					}
				}
			);
		}

		const toolbarNodeClass = data.options.isToolbarLabeled
			? TOOLBAR_CLASS + ' ' + TOOLBAR_LABELED_CLASS
			: TOOLBAR_CLASS;
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			toolbarNodeClass,
			dropdownNode ? [contentNode, dropdownNode] : contentNode
		);
	},
	items: function(items, itemTemplates, options, toolbar) {
		const itemNodes = items.map(function(item) {
			item = toolbar.data.get(item);
			const itemFn = window.toolbarFormatters[item.type];
			return Inferno.createVNode(
				infernoFlags.componentFunction,
				itemFn,
				null,
				null,
				{
					toolbar: toolbar,
					item: item,
					ref: lifecycle,
					forceUpdate: options.forceUpdateItems,
					itemRefCallback: options.itemRefCallback
				}
			);
		});
		return itemNodes;
	},
	dropdownNode: function(data) {
		const items = data.items;
		const options = data.options;
		let buttonClass = DROPDOWN_BUTTON_CLASS;
		if (!items || !items.length) {
			buttonClass += ' ' + HIDDEN_CLASS;
		}
		const buttonNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			buttonClass
		);

		const dropdownItemsNodes = items.map(function(item) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				item,
				{
					'data-id': item.props.item.id
				},
				item.props.item.id
			);
		});
		const listNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'ul',
			DROPDOWN_LIST_CLASS,
			dropdownItemsNodes
		);
		const listContainerClass = options.forceOpenDropdown
			? DROPDOWN_LIST_CONTAINER_CLASS +
			  ' ' +
			  DROPDOWN_OPENED_LIST_CONTAINER_CLASS
			: DROPDOWN_LIST_CONTAINER_CLASS;
		const listContainer = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			listContainerClass,
			listNode,
			{
				style: options.dropdownStyle || ''
			}
		);

		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_CLASS,
			[buttonNode, listContainer],
			{
				ref: options.dropdownRefCallback
			}
		);
	}
};

const lifecycle = {
	onComponentShouldUpdate: function(lastProps, nextProps) {
		if (lastProps.item.type === 'separator') {
			return false;
		}

		const needToUpdate = lastProps.item !== nextProps.item;
		if (needToUpdate) {
			const itemNode = lastProps.toolbar._nodesInfo.get(lastProps.item);
			nextProps.toolbar._nodesInfo.set(nextProps.item, itemNode);
		}
		return nextProps.forceUpdate || needToUpdate;
	},
	onComponentDidUpdate: function(lastProps, nextProps) {
		if (nextProps.forceUpdate) {
			const itemNode = lastProps.toolbar._nodesInfo.get(lastProps.item);
			itemNode.width = itemNode.node.clientWidth;
			nextProps.toolbar._nodesInfo.set(nextProps.item, itemNode);
		}
	},
	onComponentDidMount: function(domNode, props) {
		const item = props.item;
		const toolbar = props.toolbar;
		if (!item || item.type === 'separator') {
			return;
		}
		if (toolbar._nodesInfo.has(item)) {
			const nodeInfo = toolbar._nodesInfo.get(item);
			nodeInfo.node = domNode;
			if (props.forceUpdate) {
				nodeInfo.width = domNode.clientWidth;
			}
		} else {
			toolbar._nodesInfo.set(item, {
				node: domNode,
				width: domNode.clientWidth
			});
		}
	}
};

const defaultFormatters = {
	button: function(data) {
		const iconNode = SvgManager.createInfernoVNode(data.item.image);

		const toggleButtonClick = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { state: !data.item.state })
			);
			data.toolbar.render();
		};
		let nodeClass = data.item.disabled
			? BUTTON_CLASS + ' ' + DISABLED_CLASS
			: BUTTON_CLASS;
		nodeClass += data.item.state ? ' aras-toolbar__toggle-button' : '';
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			iconNode,
			{
				onclick: data.item.state !== null ? toggleButtonClick : null,
				title: data.item.tooltip,
				'data-id': data.item.id
			}
		);
	},
	separator: function(data) {
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			SEPARATOR_CLASS
		);
	},
	select: function(data) {
		const changeHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { value: event.target.value })
			);
			data.toolbar.render();
		};
		const optionNodes = data.item.options.map(function(option) {
			return Inferno.createVNode(
				infernoFlags.selectElement,
				'option',
				null,
				option.label,
				{
					value: option.value,
					selected: data.item.value === option.value
				}
			);
		});
		const selectNode = Inferno.createVNode(
			infernoFlags.selectElement,
			'select',
			null,
			optionNodes,
			{
				onChange: changeHandler,
				title: data.item.tooltip,
				disabled: data.item.disabled
			}
		);
		const itemLabel = data.item.label;
		const labelNode = itemLabel
			? Inferno.createVNode(
					infernoFlags.htmlElement,
					'span',
					'aras-toolbar-component__label',
					itemLabel
			  )
			: null;
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			FORM_CLASS,
			[labelNode, selectNode],
			{
				'data-id': data.item.id
			}
		);
	},
	date: function(data) {
		const changeHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { value: event.target.value })
			);
			data.toolbar.render();
		};
		const style = data.item.size ? { width: data.item.size + 'rem' } : {};
		const inputNode = Inferno.createVNode(
			infernoFlags.inputElement,
			'input',
			null,
			null,
			{
				type: 'text',
				onInput: changeHandler,
				defaultValue: data.item.value,
				disabled: data.item.disabled,
				style: style
			}
		);
		const iconNode = Inferno.createVNode(infernoFlags.htmlElement, 'span');
		const wrapperNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			DATE_CLASS,
			[inputNode, iconNode]
		);
		const nodeClass = data.item.disabled
			? FORM_CLASS + ' ' + DISABLED_CLASS
			: FORM_CLASS;
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			wrapperNode,
			{
				'data-id': data.item.id
			}
		);
	},
	singular: function(data) {
		const changeHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { value: event.target.value })
			);
			data.toolbar.render();
		};
		const inputNode = Inferno.createVNode(
			infernoFlags.inputElement,
			'input',
			null,
			null,
			{
				type: 'text',
				onInput: changeHandler,
				defaultValue: data.item.value,
				disabled: data.item.disabled
			}
		);
		const iconNode = Inferno.createVNode(infernoFlags.htmlElement, 'span');
		const wrapperNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			SINGULAR_CLASS,
			[inputNode, iconNode]
		);
		const nodeClass = data.item.disabled
			? FORM_CLASS + ' ' + DISABLED_CLASS
			: FORM_CLASS;
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			wrapperNode,
			{
				title: data.item.tooltip,
				'data-id': data.item.id
			}
		);
	},
	textbox: function(data) {
		const item = data.item;
		const changeHandler = function(event) {
			const id = item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, item, { value: event.target.value })
			);
		};
		const style = data.item.size ? { width: data.item.size + 'rem' } : {};
		const textboxInput = Inferno.createVNode(
			infernoFlags.htmlElement,
			'input',
			null,
			null,
			{
				type: 'text',
				autocomplete: 'off',
				value: data.item.value,
				title: data.item.tooltip,
				disabled: data.item.disabled,
				onInput: changeHandler,
				style: style
			},
			null,
			function(domNode) {
				if (!domNode) {
					return;
				}
				domNode.value = data.item.value;
			}
		);
		const itemLabel = data.item.label;
		const labelNode = itemLabel
			? Inferno.createVNode(
					infernoFlags.htmlElement,
					'span',
					'aras-toolbar-component__label',
					itemLabel
			  )
			: null;
		const nodeClass = 'aras-form aras-toolbar__textbox';
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			[labelNode, textboxInput],
			{
				'data-id': data.item.id
			}
		);
	},
	dropdownSelector: function(data) {
		const itemClickHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, {
					value: event.currentTarget.dataset.value
				})
			);
			const evt = new CustomEvent('change', { bubbles: true });
			event.currentTarget.dispatchEvent(evt);
			data.toolbar._doUpdateNodesInfo = true;
			data.toolbar.render();
		};
		const spanLabel = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			null,
			data.item.text
		);
		const dropdownLabelWrapper = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			'dropdown-selector__label-wrapper',
			[spanLabel, data.item.value]
		);

		const dropdownButton = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			DROPDOWN_BUTTON_CLASS,
			null,
			{
				tabindex: 0
			}
		);
		const keys = Object.keys(data.item.options);
		const itemOptions = data.item.options;
		const checkedItemValue = itemOptions[data.item.value];
		const dropdownItemsNodes = (keys || []).map(function(key) {
			const itemValue = itemOptions[key];
			const iconClass =
				'aras-list__item-icon' +
				(itemValue === checkedItemValue
					? ' aras-list__item-icon_aras-icon aras-icon-radio'
					: '');
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				[
					Inferno.createVNode(infernoFlags.htmlElement, 'div', iconClass),
					Inferno.createVNode(
						infernoFlags.htmlElement,
						'span',
						'aras-list__item-value',
						itemValue
					)
				],
				{
					onclick: itemClickHandler,
					'data-value': key
				}
			);
		});
		const listNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'ul',
			DROPDOWN_LIST_CLASS,
			dropdownItemsNodes
		);
		const listContainer = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_LIST_CONTAINER_CLASS,
			listNode,
			{
				style: data.dropdownStyle || ''
			}
		);

		const dropdown = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_CLASS,
			[dropdownButton, listContainer]
		);

		const nodeClass = 'aras-toolbar__dropdown-selector';
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			[dropdownLabelWrapper, dropdown],
			{
				'data-id': data.item.id,
				title: data.item.tooltip
			}
		);
	},
	htmlSelector: function(data) {
		const itemClickHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, {
					value: event.currentTarget.dataset.value
				})
			);
			const evt = new CustomEvent('change', { bubbles: true });
			event.currentTarget
				.closest('.aras-toolbar__html-selector')
				.dispatchEvent(evt);
			data.toolbar.render();
		};

		const selectedItemValue = data.item.value;
		let selectedItemLabel = '';

		const dropdownButton = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			DROPDOWN_BUTTON_CLASS
		);
		const dropdownItemsNodes = data.item.options.map(function(option) {
			if (option.type === 'separator') {
				return Inferno.createVNode(infernoFlags.htmlElement, 'li', 'separator');
			}

			const itemLabel = option.label;
			const itemValue = option.value;
			selectedItemLabel =
				itemValue === selectedItemValue ? itemLabel : selectedItemLabel;

			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				itemLabel,
				{
					onclick: itemClickHandler,
					'data-value': itemValue
				}
			);
		});

		const listNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'ul',
			DROPDOWN_LIST_CLASS,
			dropdownItemsNodes
		);
		const listContainer = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_LIST_CONTAINER_CLASS,
			listNode,
			{
				style: data.dropdownStyle || ''
			}
		);

		const dropdownLabelWrapper = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			'html-selector__label-wrapper',
			selectedItemLabel
		);
		const dropdown = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_CLASS,
			[dropdownLabelWrapper, dropdownButton],
			{
				tabindex: 0
			}
		);

		const itemLabel = data.item.label;
		const labelNode = itemLabel
			? Inferno.createVNode(
					infernoFlags.htmlElement,
					'span',
					'aras-toolbar-component__label',
					itemLabel
			  )
			: null;
		const htmlSelectorWrapper = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			'aras-toolbar__html-selector__wrapper',
			[dropdown, listContainer],
			{
				title: data.item.tooltip
			}
		);
		const nodeClass = 'aras-toolbar__html-selector';
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			[labelNode, htmlSelectorWrapper],
			{
				'data-id': data.item.id
			}
		);
	},
	dropdownButton: function(data) {
		const itemClickHandler = function(event) {
			const evt = new CustomEvent('dropDownItemClick', {
				bubbles: true,
				detail: { optionId: event.currentTarget.dataset.optionid }
			});
			event.currentTarget.dispatchEvent(evt);
		};

		const dropdownButton = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			DROPDOWN_BUTTON_CLASS
		);
		const dropdownItemsNodes = data.item.options.map(function(option) {
			const itemLabel = option.label;
			const itemValue = option.value;

			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				itemLabel,
				{
					onclick: itemClickHandler,
					'data-optionid': itemValue
				}
			);
		});

		const listNode = Inferno.createVNode(
			infernoFlags.htmlElement,
			'ul',
			DROPDOWN_LIST_CLASS,
			dropdownItemsNodes
		);
		const listContainer = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_LIST_CONTAINER_CLASS,
			listNode,
			{
				style: data.dropdownStyle || ''
			}
		);

		const dropdownLabelWrapper = Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			'action-dropdown-button__label-wrapper',
			data.item.text
		);
		const dropdown = Inferno.createVNode(
			infernoFlags.htmlElement,
			'div',
			DROPDOWN_CLASS,
			[dropdownLabelWrapper, dropdownButton],
			{
				tabindex: 0
			}
		);

		const nodeClass = 'aras-toolbar__action-dropdown-button';
		return Inferno.createVNode(
			infernoFlags.htmlElement,
			'span',
			nodeClass,
			[dropdown, listContainer],
			{
				'data-id': data.item.id
			}
		);
	}
};

const toolbarFormatters = (window.toolbarFormatters = defaultFormatters);
const toolbarTemplates = function(customTemplates) {
	window.toolbarFormatters = Object.assign(
		window.toolbarFormatters,
		customTemplates || {}
	);
	return Object.assign({}, defaultTemplates, customTemplates || {});
};

export { toolbarTemplates, toolbarFormatters };
