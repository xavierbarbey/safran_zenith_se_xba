﻿import Dialog from '../core/Dialog';

function confirmModule(message, options) {
	message = message || '';
	options = options || {};
	const addButton = options.additionalButton;

	const confirmDialog = new Dialog('html', {
		title: options.title
	});

	const cssClassName = 'aras-dialog-confirm';
	confirmDialog.dialogNode.classList.add(cssClassName);

	const messageContainer = document.createElement('div');
	messageContainer.classList.add(cssClassName + '__container');
	confirmDialog.contentNode.appendChild(messageContainer);

	const image = document.createElement('img');
	image.classList.add(cssClassName + '__img');
	image.src = aras.getBaseURL('/images/Warning.svg');
	messageContainer.appendChild(image);

	const messageNode = document.createElement('span');
	messageNode.classList.add(cssClassName + '__text');
	messageNode.textContent = message;

	messageContainer.appendChild(messageNode);

	const btnContainer = document.createElement('div');
	btnContainer.classList.add(cssClassName + '__container');

	if (addButton) {
		const buttonAdditional = document.createElement('button');
		buttonAdditional.classList.add('aras-btn', 'aras-btn_secondary');
		buttonAdditional.textContent = addButton.text;
		buttonAdditional.addEventListener(
			'click',
			confirmDialog.close.bind(confirmDialog, addButton.actionName)
		);
		btnContainer.appendChild(buttonAdditional);
	}

	const buttonOk = document.createElement('button');
	buttonOk.autofocus = true;
	buttonOk.classList.add('aras-btn');
	buttonOk.textContent =
		options.buttonOkText || aras.getResource('', 'common.ok');
	buttonOk.addEventListener(
		'click',
		confirmDialog.close.bind(confirmDialog, 'ok')
	);
	btnContainer.appendChild(buttonOk);

	const buttonCancel = document.createElement('button');
	buttonCancel.classList.add('aras-btn', 'btn_cancel');
	buttonCancel.textContent = aras.getResource('', 'common.cancel');
	buttonCancel.addEventListener(
		'click',
		confirmDialog.close.bind(confirmDialog, 'cancel')
	);
	btnContainer.appendChild(buttonCancel);

	confirmDialog.contentNode.appendChild(btnContainer);

	confirmDialog.show();
	return confirmDialog.promise;
}

export default confirmModule;
