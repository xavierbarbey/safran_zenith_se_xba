﻿class HTMLCustomElement extends HTMLElement {
	constructor(self) {
		self = super(self);
		self.init();
		return self;
	}

	init() {}
}

export default HTMLCustomElement;
