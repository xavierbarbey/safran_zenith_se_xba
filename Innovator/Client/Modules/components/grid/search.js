﻿import dropdown from '../dropdown';

const GridSearch = {
	text: function(head, headId, value, grid, metadata) {
		return {
			children: [
				{
					tag: 'input',
					className: 'aras-form-input',
					events: {
						oninput: function(e) {
							const index = e.target.closest('.aras-grid-search-row-cell')
								.dataset.index;
							const headId = grid.settings.indexHead[index];
							grid.head.set(headId, e.target.value, 'searchValue');
							grid.dom.dispatchEvent(new CustomEvent('filter'));
						},
						onfocus: function() {
							grid.settings.focusedCell = {
								headId: headId,
								rowId: 'searchRow'
							};
						},
						onkeydown: function(e) {
							if (e.key === 'F2' && metadata && metadata.handler) {
								metadata.handler();
							}
						}
					},
					attrs: {
						type: 'text'
					},
					ref: function(dom) {
						if (!dom || dom.value === value) {
							return;
						}
						dom.value = value;
					}
				}
			]
		};
	},
	dropDownIcon: function(head, headId, value, grid) {
		let index = 0;
		const baseUrl = window.location.href.replace(window.location.hash, '');
		const currentValue = head.searchValue;
		const items = head.options.map(function(item, indx) {
			index = item.value === currentValue ? indx : index;
			return {
				tag: 'li',
				children: [
					{
						tag: 'span',
						className: 'aras-list__item-icon',
						children: [
							{
								tag: 'svg',
								children: [
									{
										tag: 'use',
										attrs: {
											'xlink:href': baseUrl + '#' + item.icon
										}
									}
								]
							}
						]
					},
					{
						tag: 'span',
						className: 'aras-list__item-value',
						children: [item.label]
					}
				],
				events: {
					onmousedown: function(e) {
						const searchValue = e.target.closest('li').dataset.value;
						grid.head.set(headId, searchValue, 'searchValue');
					}
				},
				attrs: {
					'data-value': item.value
				}
			};
		});
		const className =
			'aras-grid-dropdown-icon__button' +
			(!currentValue ? ' aras-icon-arrow aras-icon-arrow_down' : '');
		const buttonIconElement = currentValue
			? [
					{
						tag: 'svg',
						children: [
							{
								tag: 'use',
								attrs: {
									'xlink:href': baseUrl + '#' + head.options[index].icon
								}
							}
						]
					}
			  ]
			: null;
		return {
			children: [
				{
					tag: 'div',
					className: 'aras-grid-dropdown-icon',
					children: [
						{
							tag: 'span',
							className: className,
							children: buttonIconElement,
							attrs: {
								tabindex: 0
							}
						},
						{
							tag: 'div',
							className: 'aras-dropdown',
							children: [
								{
									tag: 'ul',
									className: 'aras-list',
									children: items
								}
							]
						}
					]
				}
			]
		};
	},
	date: function(head, headId, value, grid, metadata) {
		const pattern = aras.getDotNetDatePattern('short_date');
		if (head.searchValue === null) {
			value = head.invalidValue;
		} else if (head.invalidValue && head.invalidValue !== '') {
			const invalidValueDate = aras.convertToNeutral(
				head.invalidValue,
				'date',
				pattern
			);
			if (head.searchValue === invalidValueDate) {
				value = head.invalidValue;
			} else {
				value = aras.convertFromNeutral(value, 'date', pattern);
			}
			head.invalidValue = '';
		} else {
			value = aras.convertFromNeutral(value, 'date', pattern);
			head.invalidValue = '';
		}
		const invalid = head.invalidValue ? ' aras-form-input_invalid' : '';
		return {
			className: 'aras-form',
			children: [
				{
					tag: 'div',
					className: 'aras-form-date' + invalid,
					children: [
						{
							tag: 'input',
							className: 'aras-form-calendar',
							attrs: {
								type: 'text'
							},
							ref: function(dom) {
								if (!dom || dom.value === value) {
									return;
								}
								dom.value = value;
							},
							events: {
								onfocus: function() {
									grid.settings.focusedCell = {
										headId: headId,
										rowId: 'searchRow'
									};
								},
								onkeydown: function(e) {
									if (e.key === 'F2') {
										metadata.handler();
									}
								},
								oninput: function(e) {
									const index = e.target.closest('.aras-grid-search-row-cell')
										.dataset.index;
									const headId = grid.settings.indexHead[index];
									let value = aras.convertToNeutral(
										e.target.value,
										'date',
										pattern
									);
									let invalidValue = '';
									const label = aras.convertFromNeutral(value, 'date', pattern);
									if (value === e.target.value) {
										value = null;
										invalidValue = e.target.value;
									} else if (label !== e.target.value) {
										invalidValue = e.target.value;
									}

									const head = grid.head.get(headId);
									grid.head.set(
										headId,
										Object.assign(head, {
											searchValue: value,
											invalidValue: invalidValue
										})
									);
								}
							}
						},
						{
							tag: 'span',
							className: 'aras-filter-list-icon aras-icon-error'
						},
						{
							tag: 'span',
							events: {
								onmousedown: function(e) {
									metadata.handler();
									e.stopPropagation();
								}
							}
						}
					]
				}
			]
		};
	},
	singular: function(head, headId, value, grid, metadata) {
		if (!head.componentData) {
			head.componentData = {
				ref: function(dom) {
					if (!dom) {
						delete head.componentData;
						return;
					}

					dom.setState({
						itemType: metadata.itemType,
						value: head.searchValue
					});

					const originKeydown = dom._onKeyDownHandler.bind(dom);
					dom._onKeyDownHandler = function(e) {
						if (e.key === 'F2') {
							metadata.handler();
						}
						originKeydown(e);
					};
				}
			};
		}

		return {
			children: [
				{
					tag: 'aras-item-property',
					ref: head.componentData.ref,
					events: {
						onchange: function() {
							head.searchValue = this.state.value;
						},
						onclick: function(e) {
							if (e.target.closest('.aras-filter-list__button')) {
								metadata.handler();
								e.stopPropagation();
							}
						}
					},
					attrs: {
						value: head.searchValue
					}
				}
			]
		};
	},
	filterList: function(head, headId, value, grid, metadata) {
		if (!head.componentData) {
			head.componentData = {
				ref: function(dom) {
					if (!dom) {
						delete head.componentData;
						return;
					}

					const attrs = {
						list: metadata.list,
						value: head.searchValue
					};
					if (head.searchValue === '') {
						attrs.label = '';
					} else if (head.searchValue === null) {
						attrs.label = head.inputLabel || '';
					}

					dom.setState(attrs);
				}
			};
		}

		const attrs = {
			value: head.searchValue
		};
		if (head.searchValue === '') {
			attrs.label = '';
		} else if (head.searchValue === null) {
			attrs.label = head.inputLabel || '';
		}

		return {
			children: [
				{
					tag: 'aras-filter-list',
					ref: head.componentData.ref,
					events: {
						onchange: function() {
							head.searchValue = this.state.value;
							head.inputLabel = null;
						},
						oninput: function() {
							head.searchValue = null;
							head.inputLabel = this.state.label;
						}
					},
					attrs: attrs
				}
			]
		};
	},
	classification: function(head, headId, value, grid, metadata) {
		let comp = head.componentData;
		if (!comp || comp.type !== 'classification') {
			Object.defineProperty(head, 'searchValue', {
				get: function() {
					const comp = this.componentData;
					return comp.value || comp.label;
				},
				set: function(newValue) {
					const comp = this.componentData;
					if (!newValue || newValue.indexOf('/') > -1) {
						comp.value = newValue;
						comp.label = null;
					} else {
						comp.value = null;
						comp.label = newValue;
					}
				},
				configurable: true
			});
			comp = {
				label: null,
				value: null,
				type: 'classification'
			};
			head.componentData = comp;
			head.searchValue = value;
		}

		if (!comp.ref) {
			comp.ref = function(dom) {
				if (!dom) {
					delete comp.ref;
					return;
				}

				dom.setState({
					list: metadata.list,
					value: comp.value,
					label: comp.label
				});
			};
		}

		return {
			children: [
				{
					tag: 'aras-classification-property',
					ref: comp.ref,
					events: {
						onchange: function() {
							comp.value = this.state.value;
							comp.label = this.state.label;
						},
						oninput: function() {
							comp.value = '';
							comp.label = this.state.label;
						},
						onkeydown: function(e) {
							if (e.key === 'F2') {
								metadata.handler();
							}
						},
						onclick: function(e) {
							if (e.target.closest('.aras-filter-list__button')) {
								metadata.handler();
							}
						}
					},
					attrs: {
						value: comp.value,
						label: comp.label
					}
				}
			]
		};
	},
	multiValueList: function(head, headId, value, grid, metadata) {
		const items = (metadata.list || []).map(function(item) {
			return {
				tag: 'li',
				className: 'aras-list-item aras-list-item_shown',
				attrs: {
					'data-value': item.value
				},
				children: [
					{
						tag: 'label',
						className: 'aras-form-boolean',
						children: [
							{
								tag: 'input',
								attrs: {
									type: 'checkbox',
									checked:
										head.searchValue.indexOf(item.value) > -1 ? true : false
								}
							},
							{
								tag: 'span'
							},
							item.label
						]
					}
				]
			};
		});

		return {
			children: [
				{
					tag: 'div',
					className:
						'aras-filter-list aras-dropdown-container aras-grid-multi-list',
					ref: function(node) {
						if (!node) {
							delete head.dropDownInstance;
							return;
						}

						if (!head.dropDownInstance) {
							head.dropDownInstance = dropdown(node, {
								pos: 'bottom-left'
							});
						}
					},
					children: [
						{
							tag: 'span',
							className: 'aras-filter-list__input aras-form-input',
							children: [
								aras.getResource(
									'',
									'common.options_select',
									head.searchValue.length
								)
							]
						},
						{
							tag: 'button',
							className:
								'aras-filter-list__button aras-btn aras-icon-arrow aras-icon-arrow_down'
						},
						{
							tag: 'div',
							className: 'aras-filter-list__dropdown aras-dropdown',
							style: '',
							children: [
								{
									tag: 'ul',
									className: 'aras-list',
									children: items,
									events: {
										onclick: function(event) {
											const listNode = event.target.closest('.aras-list-item');
											if (!listNode) {
												return;
											}

											const clickValue = listNode.dataset.value;
											const indexInCheckedList = this.head.searchValue.indexOf(
												clickValue
											);

											if (indexInCheckedList === -1) {
												this.head.searchValue.push(clickValue);
											} else {
												this.head.searchValue.splice(indexInCheckedList, 1);
											}
											event.preventDefault();
											this.grid.render();
										}.bind({ head: head, grid: grid })
									}
								}
							]
						}
					]
				}
			]
		};
	}
};

export default GridSearch;
