﻿function Keyboard(grid) {
	this.grid = grid;

	this.grid.dom.addEventListener('keydown', this._keydownHandler.bind(this));
}

Keyboard.prototype = {
	constructor: Keyboard,

	_dispatchFocusEvent: function(indexHead, indexRow, forceEdit) {
		this.grid.dom.dispatchEvent(
			new CustomEvent('focusCell', {
				detail: {
					indexRow: indexRow,
					indexHead: indexHead,
					forceEdit: forceEdit
				}
			})
		);
	},

	_dispatchSearchEvent: function() {
		this.grid.view.body.focus();
		this.grid.dom.dispatchEvent(new CustomEvent('focusCell'));
		this.grid.dom.dispatchEvent(new CustomEvent('search'));
	},

	_dispatchSelectEvent: function(indexRow, event) {
		const type = !this.grid.view.defaultSettings.multiSelect
			? 'single'
			: event.ctrlKey || event.metaKey
				? 'ctrl'
				: event.shiftKey
					? 'shift'
					: 'single';

		this.grid.dom.dispatchEvent(
			new CustomEvent('selectRow', {
				detail: {
					index: indexRow,
					type: type
				}
			})
		);
	},

	_dispatchSelectAllEvent: function(event) {
		if (!this.grid.view.defaultSettings.multiSelect) {
			return;
		}

		this.grid.dom.dispatchEvent(
			new CustomEvent('selectRow', {
				detail: {
					type: 'all'
				}
			})
		);
	},

	_dispatchCancelEditEvent: function() {
		this.grid.dom.dispatchEvent(new CustomEvent('cancelEdit'));
	},

	_keydownHandler: function(event) {
		const focusedCell = this.grid.settings.focusedCell;

		if (!focusedCell) {
			return;
		}

		const rowId = focusedCell.rowId;
		const headId = focusedCell.headId;
		let indexRow = this.grid.settings.indexRows.indexOf(rowId);
		let indexHead = this.grid.settings.indexHead.indexOf(headId);
		const rowsCount = this.grid.settings.indexRows.length - 1;

		const gridView = this.grid.view;
		const visibleRowCount =
			(gridView.bodyBoundary.clientHeight /
				gridView.defaultSettings.rowHeight) |
			0;
		const firstRowIndex =
			(gridView.bodyBoundary.scrollTop / gridView.defaultSettings.rowHeight) |
			0;

		const searchRowCell = event.target.closest('.aras-grid-search-row-cell');

		if (searchRowCell || rowId === 'searchRow') {
			switch (event.key) {
				case 'Enter': {
					this._dispatchSearchEvent();
					break;
				}
				case 'Tab': {
					event.preventDefault();
					indexHead = event.shiftKey ? --indexHead : ++indexHead;
					if (
						indexHead < 0 ||
						indexHead === this.grid.settings.indexHead.length
					) {
						return;
					}
					this._dispatchFocusEvent(indexHead, 'searchRow');
					break;
				}
			}
			return;
		}

		switch (event.key) {
			case 'Enter': {
				this._dispatchFocusEvent(indexHead, indexRow);
				break;
			}
			case 'Tab': {
				const lastHead = indexHead === this.grid.settings.indexHead.length - 1;
				const firstHead = indexHead === 0;
				const lastRow = indexRow === this.grid.settings.indexRows.length - 1;
				const firstRow = indexRow === 0;

				if (
					(lastHead && lastRow && !event.shiftKey) ||
					(firstHead && firstRow && event.shiftKey)
				) {
					event.preventDefault();
					return;
				}

				if (event.shiftKey) {
					indexRow = firstHead ? --indexRow : indexRow;
					indexHead = firstHead
						? this.grid.settings.indexHead.length - 1
						: --indexHead;
				} else {
					indexRow = lastHead ? ++indexRow : indexRow;
					indexHead = lastHead ? 0 : ++indexHead;
				}

				this._dispatchFocusEvent(indexHead, indexRow, true);
				event.preventDefault();
				break;
			}
			case 'Esc':
			case 'Escape': {
				if (focusedCell.editing) {
					this._dispatchCancelEditEvent();
				}
				break;
			}
		}

		if (event.target.closest('.aras-grid-active-cell')) {
			return;
		}

		switch (event.key) {
			case 'Home': {
				this._dispatchFocusEvent(indexHead, 0);
				break;
			}
			case 'End': {
				this._dispatchFocusEvent(indexHead, rowsCount);
				break;
			}
			case 'PageDown': {
				const nextFirstRow = firstRowIndex + visibleRowCount;
				const cellTopPosition =
					nextFirstRow * gridView.defaultSettings.rowHeight;
				gridView.bodyBoundary.scrollTop = cellTopPosition;

				if (indexRow < firstRowIndex || indexRow >= nextFirstRow) {
					indexRow = firstRowIndex;
				}
				const newFocusedIndexRow = indexRow + visibleRowCount;
				if (indexRow < rowsCount) {
					this._dispatchFocusEvent(
						indexHead,
						Math.min(newFocusedIndexRow, rowsCount)
					);
				}
				break;
			}
			case 'PageUp': {
				const nextFirstRow = firstRowIndex - visibleRowCount;
				const cellTopPosition =
					nextFirstRow * gridView.defaultSettings.rowHeight;
				gridView.bodyBoundary.scrollTop = cellTopPosition;

				let newFocusedIndexRow;
				if (nextFirstRow <= 0 && indexRow - visibleRowCount <= 0) {
					newFocusedIndexRow = indexRow;
				} else {
					newFocusedIndexRow = indexRow - visibleRowCount;
					if (indexRow > firstRowIndex + visibleRowCount) {
						newFocusedIndexRow = nextFirstRow;
					}
				}

				if (indexRow >= 0) {
					this._dispatchFocusEvent(
						indexHead,
						Math.max(0, nextFirstRow, newFocusedIndexRow)
					);
				}
				break;
			}
			case 'Up':
			case 'ArrowUp': {
				if (indexRow > 0) {
					this._dispatchFocusEvent(indexHead, --indexRow);
				}
				break;
			}
			case 'Down':
			case 'ArrowDown': {
				if (indexRow < this.grid.settings.indexRows.length - 1) {
					this._dispatchFocusEvent(indexHead, ++indexRow);
				}
				break;
			}
			case 'Left':
			case 'ArrowLeft': {
				if (indexHead > 0) {
					this._dispatchFocusEvent(--indexHead, indexRow);
				}
				break;
			}
			case 'Right':
			case 'ArrowRight': {
				if (indexHead < this.grid.settings.indexHead.length - 1) {
					this._dispatchFocusEvent(++indexHead, indexRow);
				}
				break;
			}
			case 'Spacebar':
			case ' ': {
				this._dispatchSelectEvent(indexRow, event);
				break;
			}
			case 'a': {
				if (event.ctrlKey) {
					this._dispatchSelectAllEvent(event);
				}
				break;
			}
			default:
				return;
		}

		event.preventDefault();
	}
};

export default Keyboard;
