﻿import gridFormatters from './formatters';
import gridSearch from './search';
import utils from '../../core/utils';

function gridTemplates(extension) {
	const infernoFlags = {
		text: 1,
		htmlElement: 1 << 1,
		componentClass: 1 << 2,
		componentFunction: 1 << 3,
		componentUnknown: 1 << 4,
		hasKeyedChildren: 1 << 5,
		hasNonKeyedChildren: 1 << 6,
		svgElement: 1 << 7,
		mediaElement: 1 << 8,
		inputElement: 1 << 9,
		textareaElement: 1 << 10,
		selectElement: 1 << 11,
		void: 1 << 12
	};

	const templates = {
		gridHeadTemplate: function(children, style) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'table',
				'aras-grid-head',
				children,
				{ style: style }
			);
		},
		gridHeadRowTemplate: function(children, className) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'tr',
				className,
				children
			);
		},
		gridHeadCellTemplate: function(children, style, index) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'td',
				'aras-grid-head-cell',
				children,
				{ style: style, 'data-index': index }
			);
		},
		resizeTemplate: function() {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'div',
				'aras-grid-head-cell-resize'
			);
		},
		labelTemplate: function(children) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'span',
				'aras-grid-head-cell-label',
				children
			);
		},
		sortIconTemplate: function(className, children) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'span',
				'aras-grid-head-cell-sort aras-icon-arrow ' + className,
				children
			);
		},
		viewportTemplate: function(children, style) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'table',
				'aras-grid-viewport',
				children,
				{ style: style }
			);
		},
		colgroupTemplate: function(children) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'colgroup',
				null,
				children
			);
		},
		colTemplate: function(props) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'col',
				null,
				null,
				props
			);
		},
		rowTemplate: function(children, className, index) {
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'tr',
				className,
				children,
				{ 'data-index': index }
			);
		},

		getCellTemplate: function(grid, row, headId, value) {
			const type = grid._getCellType(headId.id, row.id, value, grid);
			const cellMetadata = grid.getCellMetadata(headId.id, row.id, type);
			const formatter = gridFormatters[type];

			return formatter
				? formatter(headId.id, row.id, value, grid, cellMetadata)
				: {};
		},
		buildCell: function(props) {
			const template = Object.assign(
				{
					tag: 'td',
					children: [props.value]
				},
				props.template
			);

			template.className = props.className + ' ' + (template.className || '');

			return utils.templateToVNode(template);
		},
		getRowClasses: function(grid, row) {
			let classes = 'aras-grid-row';
			if (row.selected) {
				classes += ' aras-grid-row_selected';
			}
			if (row.hovered) {
				classes += ' aras-grid-row_hovered';
			}

			const customClasses = grid.getRowClasses(row.id);
			if (customClasses) {
				classes += ' ' + customClasses;
			}

			return classes;
		},
		buildRow: function(data) {
			const grid = data.grid;

			const cellsVN = data.head.map(function(head) {
				const value =
					data.row.data[head.id] === undefined ? '' : data.row.data[head.id];
				const template = templates.getCellTemplate(grid, data.row, head, value);

				return templates.buildCell({
					value: value,
					template: template,
					className: 'aras-grid-row-cell'
				});
			});
			return templates.rowTemplate(
				cellsVN,
				templates.getRowClasses(grid, data.row),
				data.row.index
			);
		},
		buildViewport: function(rows, head, style, defaults, grid) {
			const columnsVN = head.map(function(head) {
				return templates.colTemplate({
					width: head.data.width || defaults.headWidth
				});
			});

			const rowsVN = rows.map(function(row) {
				return templates.buildRow({
					row: row,
					head: head,
					defaults: defaults,
					grid: grid
				});
			});

			const viewportChildrenVN = [templates.colgroupTemplate(columnsVN)];
			return templates.viewportTemplate(
				viewportChildrenVN.concat(rowsVN),
				style
			);
		},
		buildHead: function(head, headStyle, defaults, grid) {
			const searchVN = [];
			const headVN = head.map(function(head) {
				const labelChildren = [head.data.label || ''];
				if (defaults.sortable && head.sort) {
					const className =
						'aras-icon-arrow_' + (head.sort.desc ? 'down' : 'up');
					labelChildren.push(
						templates.sortIconTemplate(
							className,
							head.sort && grid.settings.orderBy.length > 1
								? head.sort.index
								: ''
						)
					);
				}

				const children = [templates.labelTemplate(labelChildren)];

				if (defaults.resizable && head.data.resizable !== false) {
					children.unshift(templates.resizeTemplate());
				}

				const style = {
					width: head.data.width || defaults.headWidth
				};

				if (defaults.search) {
					const search = gridSearch[head.data.searchType || 'text'];
					const cellMetadata = grid.getCellMetadata(
						head.id,
						'searchRow',
						head.data.searchType || 'text'
					);
					const template = search
						? search(
								head.data,
								head.id,
								head.data.searchValue || '',
								grid,
								cellMetadata
						  )
						: {};
					template.key = head.id;
					template.attrs = {
						'data-index': head.index
					};

					searchVN.push(
						templates.buildCell({
							template: template,
							className: 'aras-grid-search-row-cell'
						})
					);
				}

				return templates.gridHeadCellTemplate(children, style, head.index);
			});

			const headRowsVN = [templates.gridHeadRowTemplate(headVN)];

			if (defaults.search) {
				headRowsVN.push(templates.gridHeadRowTemplate(searchVN));
			}

			return templates.gridHeadTemplate(headRowsVN, headStyle);
		}
	};

	if (extension) {
		Object.assign(extension, Object.assign(templates, extension));
	}

	return {
		buildViewport: templates.buildViewport,
		buildHead: templates.buildHead
	};
}

export default gridTemplates;
