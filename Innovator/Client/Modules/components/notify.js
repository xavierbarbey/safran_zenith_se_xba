﻿const defaultOptions = {
	timeout: 3000,
	position: 'bottom-left',
	icon: 'Message.svg'
};

const notifyes = new WeakMap();

function createNotifyElement(text, icon) {
	const container = document.createElement('div');
	let iconNode;
	if (icon) {
		iconNode = document.createElement('img');
		iconNode.src = aras.getBaseURL('/images/' + icon);
		container.appendChild(iconNode);
	}
	const textNode = document.createElement('span');
	textNode.textContent = text;
	container.appendChild(textNode);
	container.classList.add('aras-notify-messagebox');
	return container;
}

function notify(message, options) {
	if (!message) {
		return false;
	}

	const settings = Object.assign(
		{},
		defaultOptions,
		{ container: document.body },
		options
	);

	const notifyNode = createNotifyElement(message, settings.icon);

	const wrapper = document.createElement('div');
	wrapper.classList.add('aras-notify-wrapper');
	wrapper.appendChild(notifyNode);

	if (!notifyes.has(settings.container)) {
		notifyes.set(settings.container, {
			'bottom-left': null,
			'bottom-right': null,
			'top-left': null,
			'top-right': null
		});
	}
	const notifyObjects = notifyes.get(settings.container);

	notifyObjects[settings.position] =
		notifyObjects[settings.position] || document.createElement('div');
	notifyObjects[settings.position].classList.add(
		'aras-notify',
		'aras-notify_' + settings.position
	);
	if (!notifyObjects[settings.position].parentNode) {
		settings.container.appendChild(notifyObjects[settings.position]);
	}

	const direction = settings.position.split('-')[0];
	if (direction === 'bottom') {
		notifyObjects[settings.position].insertBefore(
			wrapper,
			notifyObjects[settings.position].firstElementChild
		);
	} else {
		notifyObjects[settings.position].appendChild(wrapper);
	}

	wrapper.classList.add('aras-notify-wrapper_visible');

	const timeoutHandler = function() {
		wrapper.classList.remove('aras-notify-wrapper_visible');
		if (direction === 'bottom') {
			wrapper.style.marginBottom = -wrapper.offsetHeight + 'px';
		} else {
			wrapper.style.marginTop = -wrapper.offsetHeight + 'px';
		}
		wrapper.addEventListener('transitionend', function(e) {
			if (wrapper.parentNode) {
				wrapper.parentNode.removeChild(wrapper);
			}
			if (
				notifyObjects[settings.position] &&
				!notifyObjects[settings.position].children.length
			) {
				settings.container.removeChild(notifyObjects[settings.position]);
				notifyObjects[settings.position] = null;
			}
		});
	};

	const timeout = setTimeout(timeoutHandler, settings.timeout);
	wrapper.addEventListener('click', function clickHandler(e) {
		clearTimeout(timeout);
		timeoutHandler();
		wrapper.removeEventListener('click', clickHandler);
	});

	return true;
}

export default notify;
