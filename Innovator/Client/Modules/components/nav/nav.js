﻿import navTemplates from './navTemplates';
import HTMLCustomElement from '../htmlCustomElement';

class Nav extends HTMLCustomElement {
	init() {
		this.dom = this;
		this._renderingPromise = null;
		this._lastAnimation = null;
		this.dataStore = {
			roots: null, // <Set>
			items: null // <Map>
		};
		this.selectedItemKey = null;
		this.expandedItemsKeys = new Set();
		this.filteredItems = null;
		this.expandOnButton = false;
		this.modifier = '';
		this.templates = navTemplates(this);

		this.dom.addEventListener('click', event => {
			let isExpandButton = false;
			const itemKey = this._getKeyByDomElement(event.target);
			if (this.expandOnButton) {
				const expandButton = this.dom.querySelector(
					'li[data-key="' + itemKey + '"] > div > .aras-nav-icon > span'
				);
				isExpandButton = event.target === expandButton;
			}

			if (!this.expandOnButton || isExpandButton) {
				this._toggleItemExpansion(itemKey);
			}

			if (event.target.closest('div > span:first-child') !== event.target) {
				this.select(itemKey);
			}
		});
	}

	set data(itemsMap) {
		this.dataStore.items = itemsMap;
		this.expandedItemsKeys.clear();
		this.selectedItemKey = null;
		this.render();
	}

	get data() {
		return this.dataStore.items;
	}

	set roots(rootsSet) {
		this.dataStore.roots = rootsSet;
		this.render();
	}

	get roots() {
		return this.dataStore.roots;
	}

	get expanded() {
		return this.expandedItemsKeys;
	}

	get selected() {
		return this.selectedItemKey;
	}

	connectedCallback(options) {
		this.render();
	}

	select(targetItemKey) {
		this.selectedItemKey = this.dataStore.items.has(targetItemKey)
			? targetItemKey
			: null;
		return this.render();
	}

	render() {
		if (this._renderingPromise) {
			return this._renderingPromise;
		}

		const self = this;
		this._renderingPromise = new Promise(function(resolve, reject) {
			setTimeout(function() {
				self._renderingPromise = null;
				self._render();
				if (self._lastAnimation) {
					self._lastAnimation.fn.call(self, self._lastAnimation.target);
					self._lastAnimation = null;
				}
				resolve();
			}, 0);
		});
		return this._renderingPromise;
	}

	_render() {
		if (!this.dataStore.roots || !this.dataStore.items) {
			return;
		}
		const rootNode = this.templates.root();
		Inferno.render(rootNode, this.dom);
	}

	expand(targetItemKey, expandParents) {
		if (!this.dataStore.items.has(targetItemKey)) {
			return this.render();
		}
		const targetItem = this.dataStore.items.get(targetItemKey);
		if (targetItem.children) {
			this.expandedItemsKeys.add(targetItemKey);
		}
		if (expandParents) {
			this._expandParents(targetItemKey);
		}
		const domNode = this.dom.querySelector(
			"[data-key='" + targetItemKey.replace('\\', '\\\\') + "']"
		);
		if (domNode) {
			this._animate(this._animateNodeExpand, domNode);
		}
		return this.render();
	}

	collapse(targetItemKey) {
		this.expandedItemsKeys.delete(targetItemKey);
		const domNode = this.dom.querySelector(
			"[data-key='" + targetItemKey.replace('\\', '\\\\') + "']"
		);
		this._animate(this._animateNodeCollapse, domNode);
		return this.render();
	}

	on(eventType, callback) {
		const self = this;
		const handler = function(event) {
			const isArrow =
				event.target.closest('div > span:first-child') === event.target;
			const targetElementKey = self._getKeyByDomElement(event.target);
			if (!isArrow && targetElementKey !== null) {
				callback(targetElementKey, event);
			}
		};
		this.dom.addEventListener(eventType, handler);

		return function() {
			self.dom.removeEventListener(eventType, handler);
		};
	}

	_expandParents(targetItemKey) {
		const self = this;
		let previousFoundKey = targetItemKey;
		const compare = function(childKey) {
			return childKey === previousFoundKey;
		};
		let lastFoundKey = targetItemKey;
		const fn = function(value, key) {
			if (value.children && value.children.some(compare)) {
				lastFoundKey = key;
				self.expandedItemsKeys.add(key);
			}
		};
		do {
			previousFoundKey = lastFoundKey;
			self.dataStore.items.forEach(fn);
		} while (previousFoundKey !== lastFoundKey);
	}

	_animate(animationFn, target) {
		this._lastAnimation = {
			fn: animationFn,
			target: target
		};
	}

	_scroll(targetNode, targetHeight) {
		const navNode = this.dom;
		const navHeight = navNode.clientHeight;
		if (navHeight >= targetNode.clientHeight) {
			const bottomHeight = navHeight + navNode.scrollTop - targetNode.offsetTop;
			const diff = targetNode.clientHeight - bottomHeight;
			if (diff > 0) {
				let options = { block: 'end', behavior: 'smooth' };
				const userAgent = navigator.userAgent;
				// {block: 'end'} option isn't supported in smoothscroll polyfill
				if (
					userAgent.indexOf('Trident') > -1 ||
					userAgent.indexOf('Edge') > -1
				) {
					options = false;
				}
				targetNode.scrollIntoView(options);
			}
		} else {
			targetNode.scrollIntoView({ block: 'start', behavior: 'smooth' });
		}
	}

	_animateNodeExpand(targetNode) {
		const listElement = targetNode.lastChild;
		if (listElement && listElement.tagName === 'UL') {
			const targetHeight = listElement.clientHeight;
			listElement.style.height = 0;

			// hack - It is necessary to use offsetHeight to cancel optimization by the browser.
			// The browser may ignore the first assignment listElement.style.height
			isNaN(listElement.offsetHeight);

			const afterTransition = event => {
				this._scroll(targetNode, targetHeight);
				listElement.removeAttribute('style');
				listElement.removeEventListener('transitionend', afterTransition);
			};
			listElement.addEventListener('transitionend', afterTransition);
			listElement.style.height = targetHeight + 'px';
		}
	}

	_animateNodeCollapse(targetNode) {
		const listElement = targetNode.lastChild;
		if (listElement && listElement.tagName === 'UL') {
			listElement.style.display = 'block';
			listElement.style.height = 'auto';
			const currentHeight = listElement.clientHeight;
			listElement.style.height = currentHeight + 'px';

			// hack - It is necessary to use offsetHeight to cancel optimization by the browser.
			// The browser may ignore the first assignment listElement.style.height
			isNaN(listElement.offsetHeight);

			const afterTransition = function(event) {
				listElement.removeAttribute('style');
				listElement.removeEventListener('transitionend', afterTransition);
			};
			listElement.addEventListener('transitionend', afterTransition);
			listElement.style.height = 0;
		}
	}

	_toggleItemExpansion(targetItemKey) {
		if (this.expandedItemsKeys.has(targetItemKey)) {
			this.collapse(targetItemKey);
		} else {
			this.expand(targetItemKey);
		}
	}

	_getKeyByDomElement(element) {
		if (element.tagName === 'UL') {
			return null;
		}
		const targetElement = element.closest('li');
		if (targetElement && targetElement.dataset) {
			return targetElement.dataset.key;
		}
		return null;
	}

	formatter() {
		return null;
	}
}

export default Nav;
