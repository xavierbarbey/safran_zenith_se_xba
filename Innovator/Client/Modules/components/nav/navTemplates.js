﻿import utils from '../../core/utils';
import SvgManager from '../../core/SvgManager';

function navTemplates(navInstance) {
	const infernoFlags = utils.infernoFlags;
	const rootNodeClass = 'aras-nav';
	const parentNodeClass = 'aras-nav-parent';
	const parentExpandedNodeClass = 'aras-nav-parent aras-nav-parent_expanded';
	const selectedNodeClass = 'aras-nav-selected';
	const savedSearchNodeClass = 'aras-nav__search-icon';
	const savedSearchIconUrl = '../images/SavedSearchOverlay.svg';

	const templates = {
		root: function Root() {
			const rootNodes = [];
			const modifier = navInstance.modifier;
			navInstance.roots.forEach(function(root) {
				const rootNode = navInstance.data.get(root);
				if (navInstance.filteredItems && !navInstance.filteredItems.has(root)) {
					return;
				}
				rootNodes.push(
					Inferno.createVNode(
						infernoFlags.componentFunction,
						templates.node,
						null,
						null,
						{
							nodeKey: root,
							value: rootNode
						}
					)
				);
			});
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'ul',
				rootNodeClass + (modifier ? ' ' + rootNodeClass + '_' + modifier : ''),
				rootNodes
			);
		},
		node: function Node(dataItem) {
			if (
				navInstance.filteredItems &&
				!navInstance.filteredItems.has(dataItem.nodeKey)
			) {
				return null;
			}
			return dataItem.value.children
				? Inferno.createVNode(
						infernoFlags.componentFunction,
						templates.parentNode,
						null,
						null,
						dataItem
				  )
				: Inferno.createVNode(
						infernoFlags.componentFunction,
						templates.childNode,
						null,
						null,
						dataItem
				  );
		},
		childNode: function ChildNode(item) {
			const isSelected = navInstance.selectedItemKey === item.nodeKey;
			const liProps = {
				'data-key': item.nodeKey,
				className: isSelected ? selectedNodeClass : null
			};

			const template = templates._getDefaultTemplate(item);

			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				template,
				liProps
			);
		},
		parentNode: function ParentNode(item) {
			const children = item.value.children;
			const ulOfChildren = Inferno.createVNode(
				infernoFlags.htmlElement,
				'ul',
				null,
				children.map(function(childKey) {
					return Inferno.createVNode(
						infernoFlags.componentFunction,
						templates.node,
						null,
						null,
						{
							nodeKey: childKey,
							value: navInstance.data.get(childKey)
						}
					);
				})
			);

			const template = templates._getDefaultTemplate(item);
			template.unshift(
				Inferno.createVNode(infernoFlags.htmlElement, 'div', 'aras-nav-icon', [
					Inferno.createVNode(infernoFlags.htmlElement, 'span')
				])
			); // arrow

			const divProps = {
				className:
					navInstance.selectedItemKey === item.nodeKey
						? selectedNodeClass
						: null
			};
			const divLabel = Inferno.createVNode(
				infernoFlags.htmlElement,
				'div',
				null,
				template,
				divProps
			);

			const isExpanded = navInstance.expandedItemsKeys.has(item.nodeKey);
			const classNames = isExpanded ? parentExpandedNodeClass : parentNodeClass;
			const liProps = {
				'data-key': item.nodeKey,
				className: classNames
			};
			return Inferno.createVNode(
				infernoFlags.htmlElement,
				'li',
				null,
				[divLabel, ulOfChildren],
				liProps
			);
		},
		_getDefaultTemplate: function(item) {
			const template = navInstance.formatter(item);
			if (template) {
				return template;
			}

			let imgProps;
			let imgNode = null;
			const levelpadNode = !item.value.children
				? Inferno.createVNode(
						infernoFlags.htmlElement,
						'span',
						'aras-nav_tree-levelpad'
				  )
				: null;
			const labelNode = Inferno.createVNode(
				infernoFlags.htmlElement,
				'span',
				null,
				item.value.label
			);
			if (!item.value.icon) {
				return [levelpadNode, imgNode, labelNode];
			}

			const isSavedSearch = item.value.saved_search_id;
			imgNode = SvgManager.createInfernoVNode(item.value.icon, {
				setAsBackground: isSavedSearch
			});
			if (isSavedSearch) {
				const overlaySvgId = SvgManager.getSymbolId(savedSearchIconUrl);
				if (imgNode.type === 'svg' && overlaySvgId) {
					const overlayNode = Inferno.createVNode(
						infernoFlags.svgElement,
						'use',
						null,
						null,
						{ 'xlink:href': '#' + overlaySvgId }
					);
					imgNode.children.push(overlayNode);
				} else {
					imgProps = {
						src: savedSearchIconUrl,
						alt: item.value.label,
						className: savedSearchNodeClass,
						style: {
							backgroundImage: 'url("' + imgNode.props.src + '")',
							backgroundSize: '1.8rem',
							backgroundRepeat: 'no-repeat'
						}
					};
					imgNode.props = Object.assign(imgNode.props, imgProps);
				}
			}

			return [levelpadNode, imgNode, labelNode];
		}
	};

	return templates;
}

export default navTemplates;
