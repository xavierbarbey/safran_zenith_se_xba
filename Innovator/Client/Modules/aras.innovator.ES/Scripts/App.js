require([
	'ES/Scripts/Classes/Page/MainPage',
	'ES/Scripts/Classes/Utils',
	'dojo/ready'
], function(MainPage, Utils, ready) {
	//Get aras object
	var arasObj = parent.parent.aras;

	var utils = new Utils({
		arasObj: arasObj
	});
	var isFeatureActivated = utils.isFeatureActivated();

	ready(function() {
		if (isFeatureActivated) {
			//Create main page
			var params = {
				arasObj: arasObj
			};
			var mainPage = new MainPage(params);
		} else {
			window.location = 'GetLicense.html';
		}
	});
});
