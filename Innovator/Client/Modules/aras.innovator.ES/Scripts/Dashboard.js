var crawlersGrid = null;
require([
	'dijit/layout/LayoutContainer',
	'dijit/layout/ContentPane',
	'dijit/TitlePane',
	'dijit/Tooltip',
	'dojo/_base/lang',
	'dijit/Dialog',
	'dojo/domReady!',
	'ES/Scripts/Classes/Wrappers/WrappedGrid',
	'ES/Scripts/Classes/Utils',
	'dojo/text!ES/Views/Templates/Legend.html',
	'dojo/text!ES/Views/Templates/ReindexDialog.html'
], function(LayoutContainer, ContentPane, TitlePane, Tooltip, lang, Dialog, DomReady, WrappedGrid, Utils, Legend, ReindexDialog) {

	//Get aras object
	var aras = parent.parent.aras;
	this.utils = new Utils({
		arasObj: aras
	});
	var inn = aras.newIOMInnovator();

	var crawlerITName = 'ES_Crawler';
	var agentITName = 'ES_Agent';
	var crawlerStateITName = 'ES_CrawlerState';
	var agentStateITName = 'ES_AgentState';

	var agentItemType = aras.getItemTypeForClient(agentITName, 'name');
	var crawlerItemType = aras.getItemTypeForClient(crawlerITName, 'name');
	var crawlerStateItemType = aras.getItemTypeForClient(crawlerStateITName, 'name');

	var fileProcessorsGrid = null;
	var internalReindexGrid = null;

	var crawlersGridLegend = null;
	var internalReindexGridTotalBar = document.getElementById('internalReindexProgressBar');
	var internalReindexProgressBarWrap = document.getElementById('internalReindexProgressBarWrap');
	var internalReindexStatusIcon = document.getElementById('internalReindexStatusIcon');
	var internalReindexProgressValue = document.getElementById('internalReindexProgressValue');
	var internalReindexTable = document.getElementById('internalReindexTable');
	var agentIndicator = null;

	var internalReindexRun = document.getElementById('internalReindexRun');
	var internalReindexContinue = document.getElementById('internalReindexContinue');
	var internalReindexStop = document.getElementById('internalReindexStop');

	var bgActive = '#59FF7A !important'; //green
	var bgDisabled = '#FF7A7A !important'; //red
	var bgWaitingForResponse = '#F7F488 !important'; //yellow
	var stopCurrentIteration = 'Stop current iteration';
	var fakeDateTime = '0001-01-01T00:00:00';
	var fakeCellValue = 'Not Available';
	var crawlersGridLegendTitle = '';
	var sortCriteria = 'sort_order';

	var crawlerPropsXpath = 'Relationships/Item[@type=\'Property\' and (name=\'crawler_state\'' +
			' or name=\'crawler_paging\'' +
			' or name=\'crawler_period\'' +
			' or name=\'crawler_threads\'' +
			' or name=\'crawler_parameters\'' +
			' or name=\'id\')]';
	var crawlerStatePropsXpath = 'Relationships/Item[@type=\'Property\' and (name=\'ca_start\'' +
			' or name=\'current_action\'' +
			' or name=\'next_action\'' +
			' or name=\'total_to_process\'' +
			' or name=\'currently_processed\'' +
			' or name=\'is_iteration_finished\'' +
			' or name=\'has_errors\'' +
			' or name=\'ca_finish\')]';
	var fileProcessorsPropsXpath = 'Relationships/Item[@type=\'FileProcessor\']';
	var fileProcessorProps = inn.applyMethod('ES_GetFileProcessors', '');
	fileProcessorProps = fileProcessorProps.getItemsByXPath(fileProcessorsPropsXpath);

	var crawlerProps = crawlerItemType.getItemsByXPath(crawlerPropsXpath);
	var crawlerPropsArr = sortItemProps(crawlerProps);

	var crawlerStateProps = crawlerStateItemType.getItemsByXPath(crawlerStatePropsXpath);
	var crawlerStatePropsArr = sortItemProps(crawlerStateProps);

	var vaultColumns = {
		'vault_id': 'Vault',
		'host': 'IP',
		'port': 'Port',
		'instance_power': 'Instance Power'
	};

	var internalReindexColumns = {
		'shard': 'Shard Name',
		'status': 'Status',
		'processing_time': 'Processing Time (ms)',
		'processed_count': 'Processed/Total Count',
		'page_size': 'Page Size',
		'error_details': 'Error'
	};

	var skipCols = ['is_iteration_finished'];

	var internalReindexActionRequest = '<AML>\n' +
		'  <Item type="Method" action="ES_InternalReindex">\n' +
		'    <action>{ACTION}</action>\n' +
		'    {PAGE_SIZE}\n' +
		'  </Item>\n' +
		'</AML>';

	function sortItemProps(itemProps) {
		var itemPropsArr = [];
		for (var i = 0, propCount = itemProps.getItemCount(); i < propCount; i++) {
			var property = itemProps.getItemByIndex(i);
			itemPropsArr.push({
				name: property.getProperty('name', ''),
				sort_order: property.getProperty('sort_order', ''),
				width: parseInt(property.getProperty('column_width', 100)),
				align: property.getProperty('column_alignment', 'l'),
				data_type: property.getProperty('data_type', ''),
				label: property.getProperty('label', '')
			});
		}
		if (itemPropsArr.length > 1) {
			itemPropsArr.sort(function(a, b) {
				var aOrder = parseInt(a[sortCriteria]);
				var bOrder = parseInt(b[sortCriteria]);
				if (aOrder > bOrder) {
					return 1;
				}
				if (aOrder < bOrder) {
					return -1;
				}
				return 0;
			});
		}
		return itemPropsArr;
	}

	function calculateBgColor(lastUpdate, crawlerState, caFinish, currentAction) {

		if (this.utils.isNullOrUndefined(lastUpdate)) {
			return bgDisabled;
		}

		var dtNow = new Date();
		var dtLastUpdateUTC = new Date(lastUpdate + 'Z');
		var offset = dtNow.getTimezoneOffset() - aras.getCorporateToLocalOffset();
		var dtLastUpdate = new Date(dtLastUpdateUTC.getTime() + offset * 60000);

		if ((!this.utils.isNullOrUndefined(caFinish) && crawlerState === 'Disabled') || (dtNow - dtLastUpdate.getTime()) / 1000 > 60) {
			return bgDisabled;
		} else if ((currentAction === 'Pause' || currentAction === 'Finish') || (dtNow - dtLastUpdate.getTime()) / 1000 > 40) {
			return bgWaitingForResponse;
		}

		return bgActive;
	}

	function calculateInstanceBgColor(currentValue, maxValue) {
		var percentage = currentValue / maxValue;
		if (percentage < 0.3) {
			return bgDisabled;
		} else if (percentage < 0.8) {
			return bgWaitingForResponse;
		} else {
			return bgActive;
		}
	}

	function refreshFileProcessorsStates() {
		fileProcessorsGrid.removeAllRows();
		var fileProcessors = inn.applyMethod('ES_GetFileProcessors', '');
		fileProcessors = fileProcessors.getItemsByXPath('Relationships/Item');
		if (fileProcessors.getItemCount() === 0) {
			return;
		}
		var vaults = checkVaultExistence(fileProcessors);

		if (fileProcessorsGrid.getColumnCount() === 0) {
			initFileProcessorsGrid();
		}

		var fileProcessorsGridCells = fileProcessorsGrid.getCells();

		for (var j = 0, count = fileProcessors.getItemCount(); j < count; j++) {
			var curProc = fileProcessors.getItemByIndex(j);
			var updateDate = new Date(parseInt(curProc.getProperty('timestamp', '0')));
			var curDate = new Date();
			if (((curDate - updateDate) / 1000) > 90) {
				continue;
			}
			fileProcessorsGrid.addRow(j, '', false);
			var vaultID = curProc.getProperty('vault_id', '');
			var name = '';
			for (var i = 0, vaultsCount = vaults.length; i < vaultsCount; i++) {
				if (vaults[i].id === vaultID) {
					name = vaults[i].name;
					break;
				}
			}
			for (var column in vaultColumns) {
				var value = curProc.getProperty(column, '');
				if (column === 'vault_id') {
					value = {
						name: name,
						type: 'Vault',
						id: vaultID
					};
				}
				var columnIndex = fileProcessorsGrid.getColumnOrderByName(column);
				if (column === 'instance_power') {
					var freeInstancePower = curProc.getProperty('free_instance_power');
					var isFIPNumber = !isNaN(parseInt(freeInstancePower));
					fileProcessorsGrid.setCellBackgroundColor(
						fileProcessorsGridCells[columnIndex].layoutIndex,
						j,
						calculateInstanceBgColor((isFIPNumber) ? +freeInstancePower : +value, +value)
					);
					value = ((isFIPNumber) ? freeInstancePower : '-') + '/' + value;
				}
				if (!this.utils.isNullOrUndefined(columnIndex)) {
					fileProcessorsGrid.setCellValue(columnIndex, j, value);
				}
			}
		}
	}

	function refreshInternalReindexGridStates() {
		internalReindexGrid.removeAllRows();

		var AMLRequest = internalReindexActionRequest.replace('{ACTION}', 'Status').replace('{PAGE_SIZE}', '');
		var internalReindex = aras.newIOMItem();
		internalReindex.loadAML(AMLRequest);
		internalReindex = internalReindex.apply();

		if (internalReindex.isError() || internalReindex.getItemCount() === 0) {
			internalReindexTable.classList.add('empty');
			internalReindexProgressValue.textContent = '0/0';
			internalReindexStatusIcon.className = '';
			internalReindexProgressBarWrap.className = '';
			return;
		} else {
			internalReindex = internalReindex.getItemsByXPath('//Item[@type="Internal Reindex Status"]');
			internalReindexTable.classList.remove('empty');
		}

		var i;
		var column;
		var value;
		var shardCount;
		var errorCount = 0;
		var runActionAvailable = true;
		var continueActionAvailable = false;
		var stopActionAvailable = false;
		var control = internalReindexGrid.getControl();
		var totalCount = internalReindex.getProperty('total_count', '0');
		var processedCount = internalReindex.getProperty('processed_count', '0');
		var reindexedProcessPercentage = (+processedCount / +totalCount) * 100;

		internalReindexGridTotalBar.style.width = reindexedProcessPercentage + '%';
		internalReindexProgressValue.textContent = processedCount + '/' + totalCount;
		internalReindexProgressBarWrap.className = '';
		internalReindexStatusIcon.className = '';

		internalReindexProgressBarWrap.classList.add('green');
		internalReindexStatusIcon.classList.add('green');
		if (reindexedProcessPercentage !== 100) {
			internalReindexStatusIcon.classList.add('spinning');
		}

		var internalReindexShards = internalReindex.getItemsByXPath('./Relationships/Item[@type="Shard Status"]');
		var internalReindexGridCells = internalReindexGrid.getCells();

		for (i = 0, shardCount = internalReindexShards.getItemCount(); i < shardCount; i++) {
			var internalReindexShard = internalReindexShards.getItemByIndex(i);
			var showError = false;
			internalReindexGrid.addRow(i, '', false);

			for (column in internalReindexColumns) {
				value = internalReindexShard.getProperty(column, '');
				if (column === 'error_details') {
					if (value !== '' && showError) {
						errorCount++;
						var rowItem = control.grid_Experimental.getItem(i);
						rowItem.tooltip = [value];
						value = '../../Images/RedWarning.svg';
						internalReindexProgressBarWrap.classList.add('hasErrors');
					} else {
						value = '';
					}
				}
				if (column === 'processed_count') {
					var shardTotalCount = internalReindexShard.getProperty('total_count', '0');
					value += '/' + shardTotalCount;
				}
				var columnIndex = internalReindexGrid.getColumnOrderByName(column);
				if (column === 'status') {
					var color = bgWaitingForResponse;
					switch (value) {
						case 'Stopped':
							color = bgDisabled;
							continueActionAvailable = true;
							break;
						case 'Error':
						case 'PluginException':
							color = bgDisabled;
							showError = true;
							continueActionAvailable = true;
							break;
						case 'Running':
							color = bgActive;
							runActionAvailable = false;
							continueActionAvailable = false;
							stopActionAvailable = true;
							break;
						case 'Finished':
							color = bgWaitingForResponse;
					}

					internalReindexGrid.setCellBackgroundColor(internalReindexGridCells[columnIndex].layoutIndex,i, color);
				}
				if (!this.utils.isNullOrUndefined(columnIndex)) {
					internalReindexGrid.setCellValue(columnIndex, i, value);
				}
			}
		}

		// Enable/disable buttons
		if (runActionAvailable) {
			internalReindexRun.classList.remove('disabled');
			if (reindexedProcessPercentage !== 100) {
				internalReindexStatusIcon.classList.add('red');
				internalReindexStatusIcon.classList.remove('spinning');
			}
		} else {
			internalReindexRun.classList.add('disabled');
		}

		if (continueActionAvailable && !stopActionAvailable) {
			internalReindexContinue.classList.remove('disabled');
		} else {
			internalReindexContinue.classList.add('disabled');
		}

		if (stopActionAvailable) {
			internalReindexStop.classList.remove('disabled');
		} else {
			internalReindexStop.classList.add('disabled');
		}

		if (errorCount === shardCount) {
			internalReindexProgressBarWrap.className = '';
			internalReindexProgressBarWrap.classList.add('red');

			internalReindexStatusIcon.classList.add('red');
			internalReindexStatusIcon.classList.remove('spinning');
		}
	}

	function refreshCrawlersStates() {
		// hide RMB menu
		crawlersGrid.blurMenu();

		var crawlersGridCells = crawlersGrid.getCells();

		// change crawlers grid legend
		if (this.utils.isNullOrUndefined(crawlersGridLegend)) {
			crawlersGridLegend = document.getElementById('crawlersGridLegend');
		}
		crawlersGridLegend.textContent = agentItemType.getProperty('name', '') + ' ' + this.utils.getResourceValueByKey('dashboard.crawler_grid_title');

		// set color for crawlers grid indicator
		var bgColor = '';
		var agentState = aras.newIOMItem(agentStateITName, 'get');
		agentState.setAttribute('select', 'modified_on');
		agentState = agentState.apply();
		if (!agentState.isError()) {
			bgColor = calculateBgColor(agentState.getProperty('modified_on',''));
			if (this.utils.isNullOrUndefined(agentIndicator)) {
				agentIndicator = document.getElementById('agentIndicator');
			}
			agentIndicator.setAttribute('style', 'background-color: ' + bgColor);
		}

		var crawlersItems = aras.newIOMItem(crawlerITName, 'get');
		var crawlerStates = aras.newIOMItem(crawlerStateITName, 'get');
		crawlersItems.addRelationship(crawlerStates);
		crawlersItems = crawlersItems.apply();
		if (!crawlersItems.isError()) {
			crawlersItems = crawlersItems.getItemsByXPath(aras.XPathResult('/Item[@type=\'ES_Crawler\']'));
			var crawlers = crawlersItems.getItemCount();
			if (crawlers > 0) {
				var crawler = '';
				var crawlerId = '';

				for (var i = 0; i < crawlers; ++i) {
					crawler = crawlersItems.getItemByIndex(i);
					crawlerId = crawler.getID();
					if (crawlersGrid.getRowIndex(crawlerId) == -1) {
						crawlersGrid.addRow(crawlerId, '', false);
					}
				}

				for (i = 0; i < crawlers; ++i) {
					crawler = crawlersItems.getItemByIndex(i);
					crawlerId = crawler.getID();
					var stateColumnIndex = crawlersGrid.getColumnOrderByName('crawler_state');
					var columnIndex = '';
					var value = '';
					var crawlerState = crawlersItems.getItemsByXPath(aras.XPathResult('/Item[@type=\'ES_Crawler\' and @id=\'' +
					crawlerId + '\']/Relationships/Item[@type=\'ES_CrawlerState\']'));
					if (crawlerState.getItemCount() === 0) {
						crawler.setProperty('crawler_state', fakeCellValue);
						crawlerState = aras.newIOMItem('ES_CrawlerState', 'get');
						crawlerState.setProperty('current_action', fakeCellValue);
						crawlerState.setProperty('next_action', fakeCellValue);
						crawlerState.setProperty('ca_start', fakeDateTime);
						crawlerState.setProperty('ca_finish', fakeDateTime);
						crawlerState.setProperty('currently_processed', fakeCellValue);
						crawlerState.setProperty('total_to_process', fakeCellValue);
					}
					var isIterationFinished = crawlerState.getProperty('is_iteration_finished', '') === '1';

					for (var j = 0; j < crawlersGridCells.length; j++) {
						var cell = crawlersGridCells[j];
						if (cell.layoutIndex < crawlerPropsArr.length) {
							value = crawler.getProperty(cell.field, '');
						} else {
							value = crawlerState.getProperty(cell.field, '');
						}
						if (cell.editableType === 'item') {
							value = {
								name: crawler.getPropertyAttribute(cell.field, 'keyed_name'),
								type: crawler.getPropertyAttribute(cell.field, 'type'),
								id: crawler.getProperty(cell.field, '')
							};
						}
						if (value === stopCurrentIteration || (cell.field === 'current_action' && value === 'Run' && isIterationFinished)) {
							value = crawlersGrid.getCellValue(stateColumnIndex, crawlerId) === 'Active' ? 'Finish' : '';
						}
						if (cell.field === 'next_action' && value === 'Restart' && isIterationFinished) {
							value = crawlersGrid.getCellValue(stateColumnIndex, crawlerId) === 'Active' ? 'Run' : '';
						}
						crawlersGrid.setCellValue(j, crawlerId, value);
					}

					if (!crawlerState.isError() && !crawlerState.isEmpty()) {
						var currentAction = '';
						if (crawlerState.getProperty('current_action','') === 'Run' && isIterationFinished) {
							currentAction = 'Finish';
						} else {
							currentAction = crawlerState.getProperty('current_action','');
						}

						if (currentAction !== fakeCellValue) {
							bgColor = calculateBgColor(
								crawlerState.getProperty('modified_on',''),
								crawlersGrid.getCellValue(stateColumnIndex, crawlerId),
								crawlerState.getProperty('ca_finish',''),
								currentAction);

							crawlersGrid.setCellBackgroundColor(crawlersGridCells[stateColumnIndex].layoutIndex, crawlerId, bgColor);
						}
					}
				}
			}
		}
	}

	function onLink(linkVal) {

		if (linkVal.length) {
			linkVal = linkVal.replace(/'/g, '');
			var typeName = linkVal.split(',')[0];
			var id = linkVal.split(',')[1];

			var itm = aras.getItemById(typeName, id, 0);
			if (itm) {
				aras.uiShowItemEx(itm, undefined);
			}
		}
	}

	function getEditType(editType) {
		switch (editType) {
			case 'date':
				return 'date';
			case 'boolean':
				return 'checkbox';
			case 'item':
				return 'item';
			case 'image':
				return 'image';
			default:
				return 'text';
		}
	}

	function checkVaultExistence(fileProcessors) {
		var vaults = [];

		for (var i = 0, count = fileProcessors.getItemCount(); i < count; i++) {
			var property = fileProcessors.getItemByIndex(i);
			var vaultID = property.getProperty('vault_id','');

			var vaultItem = aras.newIOMItem('Vault', 'get');
			vaultItem.setAttribute('select', 'name');
			vaultItem.setID(vaultID);
			vaultItem = vaultItem.apply();

			if (!vaultItem.isError() || !vaultItem.isEmpty()) {
				vaults.push({
					name: vaultItem.getProperty('name',''),
					id: vaultID
				});
			}
		}

		return vaults;
	}

	function initCrawlersGrid() {
		crawlersGrid.on('gridMenuInit', onShowMenuCrawlersGrid);
		crawlersGrid.on('gridMenuClick', onMenuClickedCrawlersGrid);
		crawlersGrid.on('gridLinkClick', onLink);

		for (var i = 0; i < crawlerPropsArr.length; i++) {
			if (crawlerPropsArr[i].name == 'id') {
				crawlerPropsArr[i].label = 'Crawler';
			}
			crawlersGrid.addColumn(
				crawlerPropsArr[i].name,
				crawlerPropsArr[i].label,
				getEditType(crawlerPropsArr[i].data_type),
				crawlerPropsArr[i].width, crawlerPropsArr[i].align
			);
		}

		for (i = 0; i < crawlerStatePropsArr.length; i++) {
			if (skipCols.indexOf(crawlerStatePropsArr[i].name) !== -1) {
				continue;
			}
			crawlersGrid.addColumn(
				crawlerStatePropsArr[i].name,
				crawlerStatePropsArr[i].label,
				getEditType(crawlerStatePropsArr[i].data_type),
				crawlerStatePropsArr[i].width,
				crawlerStatePropsArr[i].align
			);
		}

		crawlersGrid.init();
		refreshCrawlersStates();
	}

	function initFileProcessorsGrid() {
		fileProcessorsGrid.on('gridLinkClick', onLink);

		for (var column in vaultColumns) {
			var dataType = (column === 'vault_id') ? 'item' : 'text';
			fileProcessorsGrid.addColumn(column, vaultColumns[column], getEditType(dataType), 100);
		}

		fileProcessorsGrid.init();
	}

	function initInternalReindexGridGrid() {
		for (var column in internalReindexColumns) {
			var dataType = 'text';
			var dataAlign = 'c';
			switch (column) {
				case 'error_details':
					dataType = 'image';
					break;
				case 'status':
				case 'shard_name':
					dataAlign = 'l';
			}
			internalReindexGrid.addColumn(column, internalReindexColumns[column], getEditType(dataType), 100, dataAlign);
		}

		internalReindexGrid.init();

		// Show tooltip
		internalReindexGrid.on('onCellMouseOver', function(e) {
			if (e.cell.field === 'error_details' && e.cellNode.hasChildNodes()) {
				var rowItem = e.grid.getItem(e.rowIndex);
				if (rowItem) {
					var tooltipText = rowItem.tooltip;
					if (!this.utils.isNullOrUndefined(tooltipText)) {
						dijit.showTooltip(tooltipText[0], e.cellNode);
					}
				}
			}
		}.bind(this));

		// Hide tooltip
		internalReindexGrid.on('onCellMouseOut', function(e) {
			dijit.hideTooltip(e.cellNode);
		});

		// Resize general status bar according to grid width
		var resizeStatusBar = function() {
			var internalReindexGeneralStatus = document.getElementById('internalReindexGeneralStatus');
			var gridNode = document.getElementById('internalReindexGrid');
			if (!this.utils.isNullOrUndefined(gridNode)) {
				var headerNode = gridNode.getElementsByClassName('dojoxGridMasterHeader');
				if (headerNode.length > 0) {
					var headerRow = headerNode[0].getElementsByClassName('dojoxGridRowTable');
					if (headerRow.length > 0) {
						var gridNodeWidth = +headerRow[0].offsetWidth;

						var scrollBoxRow = gridNode.getElementsByClassName('dojoxGridScrollbox');
						var scrollBoxWidth = scrollBoxRow[0].offsetWidth;

						var width = (gridNodeWidth > scrollBoxWidth) ? scrollBoxWidth : gridNodeWidth;
						internalReindexGeneralStatus.style.width = width + 'px';
					}
				}
			}
		};

		resizeStatusBar.call(this);
		internalReindexGrid.on('onResizeColumn', resizeStatusBar.bind(this));
	}

	onload = function() {

		var checkFeature = aras.newIOMItem('ItemType', 'ES_CheckFeature');
		checkFeature = checkFeature.apply();

		if (checkFeature.isError()) {
			window.location = aras.getScriptsURL() + '../Modules/aras.innovator.ES/Views/GetLicense.html';
			return;
		}

		fixMainGridContainerHeight();
		loadData();

		crawlersGrid = new WrappedGrid();
		crawlersGrid.createControl('crawlersGrid', initCrawlersGrid);

		fileProcessorsGrid = new WrappedGrid();
		fileProcessorsGrid.createControl('fileProcessorsGrid', function() {
			initFileProcessorsGrid();
			refreshFileProcessorsStates();
		});

		internalReindexGrid = new WrappedGrid();
		internalReindexGrid.createControl('internalReindexGrid', function() {
			initInternalReindexGridGrid();
			refreshInternalReindexGridStates();
		});

		var actions = crawlerItemType.getItemsByXPath('Relationships/Item[@type=\'Item Action\']/related_id/Item[@type=\'Action\']');
		for (var i = 0, count = actions.getItemCount(); i < count; i++) {
			var action = actions.getItemByIndex(i);
			var label = action.getProperty('label', action.getProperty('name',''));
			crawlersGrid.addMenuItem(action.getID(), (label === stopCurrentIteration) ? 'Finish' : label);
		}

		// fill legend grid and other fields with test from resources
		var caption = this.utils.getResourceValueByKey('legend.title');
		var color = this.utils.getResourceValueByKey('legend.color');
		var serviceability = this.utils.getResourceValueByKey('legend.serviceability');
		var greenHint1 = this.utils.getResourceValueByKey('legend.green_hint_1');
		var yellowHint1 = this.utils.getResourceValueByKey('legend.yellow_hint_1');
		var yellowHint2 = this.utils.getResourceValueByKey('legend.yellow_hint_2');
		var redHint1 = this.utils.getResourceValueByKey('legend.red_hint_1');
		var redHint2 = this.utils.getResourceValueByKey('legend.red_hint_2');

		var legendTable = document.getElementById('legendTable');
		if (legendTable !== null) {
			legendTable.innerHTML = dojo.replace(Legend, [caption, color, serviceability, greenHint1, yellowHint1, yellowHint2, redHint1, redHint2]);
		}
		var fileProcessorsRow = document.getElementById('fileProcessorsRow');
		if (fileProcessorsRow !== null) {
			fileProcessorsRow = fileProcessorsRow.getElementsByTagName('legend')[0];
			fileProcessorsRow.textContent = this.utils.getResourceValueByKey('dashboard.file_processors_grid_title');
		}
		var internalReindexRow = document.getElementById('internalReindexRow');
		if (internalReindexRow !== null) {
			internalReindexRow = internalReindexRow.getElementsByTagName('legend')[0];
			internalReindexRow.textContent = this.utils.getResourceValueByKey('dashboard.internal_reindex_grid_title');
		}
		var internalReindexStatus = document.getElementById('internalReindexStatus');
		if (internalReindexStatus !== null) {
			internalReindexStatus.textContent = this.utils.getResourceValueByKey('dashboard.internal_reindex_general_status');
		}
		var errorContainer = document.getElementById('errorContainer');
		if (errorContainer !== null) {
			errorContainer = errorContainer.getElementsByTagName('h1')[0];
			errorContainer.textContent = this.utils.getResourceValueByKey('dashboard.error_no_agent');
		}
		var headerContainer = document.getElementById('headerContainer');
		if (headerContainer !== null) {
			headerContainer = headerContainer.getElementsByTagName('h1')[0];
			headerContainer.textContent = this.utils.getResourceValueByKey('dashboard.name');
		}

		setInterval(function() {
			refreshCrawlersStates();
			refreshFileProcessorsStates();
			refreshInternalReindexGridStates();
		}, 30000);

		window.addEventListener('resize', fixMainGridContainerHeight);

		var internalReindexRun = document.getElementById('internalReindexRun');
		if (internalReindexRun !== null) {
			internalReindexRun.onclick = runInternalReindexAction.bind(this, 'Start');
		}
		var internalReindexContinue = document.getElementById('internalReindexContinue');
		if (internalReindexContinue !== null) {
			internalReindexContinue.onclick = runInternalReindexAction.bind(this, 'Continue');
		}
		var internalReindexStop = document.getElementById('internalReindexStop');
		if (internalReindexStop !== null) {
			internalReindexStop.onclick = runInternalReindexAction.bind(this, 'Stop');
		}
		var refreshButton = document.getElementById('refreshButton');
		if (refreshButton !== null) {
			refreshButton.setAttribute('value', this.utils.getResourceValueByKey('buttons.refresh'));
			refreshButton.onclick = refreshDashboard;
		}
		refreshButton.classList.remove('invisibleDivStyle');
	};

	function loadData() {
		var qry = aras.newIOMItem(agentITName, 'get');
		qry.setAttribute('select', 'id');
		var res = qry.apply();

		if (res.isError() || res.isEmpty()) {
			document.getElementById('errorContainer').className = 'visibleErrorStyle';
			document.getElementById('gridsContainer').className = 'invisibleDivStyle';
		} else {
			document.getElementById('gridsContainer').className = 'visibleDivStyle';
		}
	}

	function onShowMenuCrawlersGrid(rowId) {
		var rowCrawlerState = '';
		var rowCurrentAction = '';
		var occurenceCounter = null;
		var actionsArray = [];
		var availableActions = [];
		var menu = this.GetMenu();
		var selectedRows = this.getSelectedItemIDs();
		var actionsBehavior = {
			'Active': {
				'Pause': ['Restart', 'Resume', 'Finish', 'Disable'],
				'Run': ['Pause', 'Restart', 'Finish', 'Disable'],
				'Finish': ['Run', 'Disable'],
				'Restart': ['Finish', 'Disable'],
			},
			'Disabled': {
				'': ['Enable']
			}
		};

		// get all actions for selected rows
		for (var i = 0; i < selectedRows.length; i++) {
			rowCrawlerState = crawlersGrid.getCellValue(this.getColumnIndex('crawler_state'), selectedRows[i]);
			rowCurrentAction = crawlersGrid.getCellValue(this.getColumnIndex('current_action'), selectedRows[i]);
			actionsArray[i] = actionsBehavior[rowCrawlerState][rowCurrentAction];
		}

		// show only common actions for all selected rows
		if (actionsArray.length > 1) {
			for (i = 0; i < actionsArray[0].length; i++) {
				occurenceCounter = 0;
				for (var j = 1; j < actionsArray.length; j++) {
					if (actionsArray[j] !== undefined && actionsArray[j].indexOf(actionsArray[0][i]) !== -1) {
						occurenceCounter++;
					}
				}
				if (occurenceCounter === actionsArray.length - 1) {
					availableActions = availableActions.concat(actionsArray[0][i]);
				}
			}
		} else {
			availableActions = actionsArray[0];
		}

		for (var actionID in menu.collectionMenu) {
			var label = menu.collectionMenu[actionID].item.label === stopCurrentIteration ? 'Finish' : menu.collectionMenu[actionID].item.label;
			menu.setHide(actionID, availableActions.indexOf(label) === -1);
		}
		return !(rowId === undefined || rowId == 'header_row');
	}

	function onMenuClickedCrawlersGrid(actionId) {
		var selectedIDs = crawlersGrid.getSelectedItemIDs('|').split('|');
		var action = crawlerItemType.getItemsByXPath('Relationships/Item[@type=\'Item Action\']/related_id/Item[@type=\'Action\' and id=\'' + actionId + '\']');

		// try to unlock crawler items if it is currently locked
		var crawlerItems = aras.newIOMItem('ES_Crawler', 'get');
		crawlerItems.setAttribute('idlist', selectedIDs);
		crawlerItems = crawlerItems.apply();
		if (!crawlerItems.isError() && crawlerItems.getItemCount() > 0) {
			for (var j = 0, count = crawlerItems.getItemCount(); j < count; ++j) {
				var crawlerItem = crawlerItems.getItemByIndex(j);
				if (aras.isLocked(crawlerItem.node)) {
					aras.unlockItemEx(crawlerItem.node, false);
				}
			}
		}

		// perform clicked action(s)
		for (i = 0; i < selectedIDs.length; i++) {
			aras.invokeAction(action.node, crawlerItemType.getID(), selectedIDs[i]);

			var columnIndex = crawlersGrid.getColumnOrderByName('next_action');
			var label = action.getProperty('label', '');
			crawlersGrid.setCellValue(columnIndex, selectedIDs[i], (label === stopCurrentIteration) ? 'Finish' : label);
		}

		refreshDashboard();
	}

	function fixMainGridContainerHeight() {
		var dashboardElement = document.getElementsByClassName('adminDashboard')[0];
		var dashboardRectangle = dashboardElement.getBoundingClientRect();
		var gridTop = dashboardRectangle.top;
		var wndHeight = window.innerHeight;
		var height = wndHeight - gridTop;
		if (height > 200) {
			dashboardElement.style.height = '200px';
		} else {
			dashboardElement.style.height = height + 'px';
		}
	}

	function refreshDashboard() {
		toggleSpinner(true, function() {
			try {
				refreshCrawlersStates();
				refreshFileProcessorsStates();
				refreshInternalReindexGridStates();
			} catch (ex) {
				aras.AlertError(ex.message);
			} finally {
				toggleSpinner(false);
			}
		});
	}

	function runInternalReindexAction(action, e) {
		if (e.target.classList.contains('disabled')) {
			return;
		}

		var showPageSize = false;
		var message = '';
		switch (action) {
			case 'Start':
			case 'Continue':
				message = this.utils.getResourceValueByKey('dashboard.internal_reindex_get_page_size');
				showPageSize = true;
				break;
			case 'Stop':
				message = this.utils.getResourceValueByKey('dashboard.internal_reindex_stop_reindex_confirm');
		}

		var templateProperties = {
			message: message,
			display: showPageSize ? 'block' : 'none',
			okButtonValue: aras.getResource('', 'common.ok'),
			cancelButtonValue: aras.getResource('', 'common.cancel')
		};

		var markup = lang.replace(ReindexDialog, templateProperties);
		var reindexDialog = new Dialog({
			title: action + ' reindexing',
			content: markup,
			style: 'width: 300px'
		});
		reindexDialog.show();
		reindexDialog.baseClass += ' reindexDialog';
		reindexDialog.closeButtonNode.classList.add('aras-icon-close');

		dojo.query('button[name=\"buttonOk\"]').connect('onclick', function(e) {
			var pageSize = e.target.parentNode.previousElementSibling.value;
			var reindexRequest = internalReindexActionRequest
				.replace('{ACTION}', action)
				.replace('{PAGE_SIZE}', (pageSize === '') ? '' : '<page_size>' + pageSize + '</page_size>');
			var internalReindex = aras.newIOMItem();
			internalReindex.loadAML(reindexRequest);
			internalReindex = internalReindex.apply();
			if (internalReindex.isError()) {
				aras.AlertError(internalReindex.getErrorString());
			}
			reindexDialog.destroy();
			refreshInternalReindexGridStates();
		});

		dojo.query('button[name=\"buttonCancel\"]').connect('onclick', function() {
			reindexDialog.destroy();
		});
	}

	function toggleSpinner(state, callback) {
		var spinner = document.getElementById('dashboardSpinner');

		if (!spinner) {
			return false;
		}

		if (dojo.hasClass(spinner, 'hidden') && state) {
			dojo.removeClass(spinner, 'hidden');
		}

		if (!dojo.hasClass(spinner, 'hidden') && !state) {
			dojo.addClass(spinner, 'hidden');
		}

		if (callback && state) {
			if (window.webkitRequestAnimationFrame) {
				setTimeout(callback, 1000 / 60);
			} else {
				window.requestAnimationFrame(callback);
			}
		}

		return true;
	}
});
