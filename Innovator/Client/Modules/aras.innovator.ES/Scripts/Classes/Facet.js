define([
	'dojo/_base/declare'
], function(declare) {
	return declare('ES.SelectQueryServiceRequest', null, {
		id: 0,
		name: '',
		title: '',
		_options: null,
		_areOptionsSorted: false,

		constructor: function(args) {
			declare.safeMixin(this, args);

			this._options = [];
		},

		addOption: function(value, label, count, isSelected) {
			this._options.push({
				name: value,
				label: label,
				count: count,
				isSelected: isSelected
			});

			this._areOptionsSorted = false;
		},

		getOptions: function() {
			if (!this._areOptionsSorted) {
				this._options.sort(this.naturalSort);
				this._areOptionsSorted = true;
			}

			return this._options;
		},

		getOption: function(value) {
			var res = null;

			for (var i = 0; i < this._options.length; i++) {
				var option = this._options[i];
				if (option.name === value) {
					res = this._options[i];
					break;
				}
			}

			return res;
		},

		isOptionSelected: function(name) {
			var res = false;

			for (var i = 0; i < this._options.length; i++) {
				var option = this._options[i];
				if (option.name === name) {
					res = option.isSelected;
					break;
				}
			}

			return res;
		},

		isAnyOptionSelected: function() {
			var res = false;

			for (var i = 0; i < this._options.length; i++) {
				var option = this._options[i];
				if (option.isSelected) {
					res = true;
					break;
				}
			}

			return res;
		},

		naturalSort: function(a, b) {
			a = a.label.toLowerCase();
			b = b.label.toLowerCase();

			var aChunkArr = chunkify(a);
			var bChunkArr = chunkify(b);

			var numS = Math.max(aChunkArr.length, bChunkArr.length);
			for (var i = 0; i < numS; i++) {
				var aN = parseFloat(aChunkArr[i]);
				var bN = parseFloat(bChunkArr[i]);
				var aChunk = aN || aChunkArr[i];
				var bChunk = bN || bChunkArr[i];

				if (typeof aChunk !== typeof bChunk) {
					aChunk += '';
					bChunk += '';
				}
				if (aChunk > bChunk) {
					return 1;
				} else if (aChunk < bChunk) {
					return -1;
				}
			}
			return 0;

			function chunkify(label) {
				var re = /(-?[0-9.]+)/g;
				var nC = String.fromCharCode(0);
				var chunkArr = label.replace(re, nC + '$1' + nC).split(nC);
				chunkArr = chunkArr.filter(function(str) {
					return /\S/.test(str);
				});
				return chunkArr;
			}
		}
	});
});
