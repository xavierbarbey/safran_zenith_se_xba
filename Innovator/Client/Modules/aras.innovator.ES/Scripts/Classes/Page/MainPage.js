define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/dom',
	'dojo/parser',
	'dojo/io-query',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Classes/Request/SelectQueryServiceRequest',
	'ES/Scripts/Controls/SearchPanel',
	'ES/Scripts/Controls/FiltersPanel',
	'ES/Scripts/Controls/ResultPanel'
], function(declare, lang, dom, parser, ioQuery, Utils, SelectQueryServiceRequest, SearchPanel, FiltersPanel, ResultPanel) {
	return declare(null, {
		_arasObj: null,
		_utils: null,

		//Request
		_selectQueryServiceRequest: null,

		//Widgets
		_searchPanelWidget: null,
		_filtersPanelWidget: null,
		_resultsPanelWidget: null,

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			//Show container
			var layoutContainer = dom.byId('layoutContainer');
			this._utils.setNodeVisibility(layoutContainer, true);

			//Parse layout
			parser.parse();

			//Create new request object
			this._selectQueryServiceRequest = new SelectQueryServiceRequest({
				arasObj: this._arasObj
			});

			/*---------------------------------------------------------------------------------------------*/
			//Show search toolbar

			this._searchPanelWidget = new SearchPanel({
				arasObj: this._arasObj,
				onSearch: this._onSearch.bind(this),
				onFiltersChange: this._onFiltersChange.bind(this),
				onPageChange: this._onPageChange.bind(this)
			});
			this._searchPanelWidget.placeAt('searchContainer');
			this._searchPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Show filters

			this._filtersPanelWidget = new FiltersPanel({
				arasObj: this._arasObj,
				onFiltersReset: this._onFiltersReset.bind(this),
				onFilterReset: this._onFilterReset.bind(this),
				onFiltersChange: this._onFiltersChange.bind(this),
				onFilterLoadOptions: this._onFilterLoadOptions.bind(this)
			});
			this._filtersPanelWidget.placeAt('filtersContainer');
			this._filtersPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Show results

			this._resultsPanelWidget = new ResultPanel({
				arasObj: this._arasObj
			});
			this._resultsPanelWidget.placeAt('resultsContainer');
			this._resultsPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Init configurations for search

			var resItm = this._arasObj.newIOMItem('Method', 'ES_InitSearchConfigurations');
			resItm.apply();

			/*---------------------------------------------------------------------------------------------*/
			//Perform search if have parameter

			var queryObject = ioQuery.queryToObject(decodeURIComponent(window.location.search.slice(1)));
			if (!this._utils.isNullOrUndefined(queryObject.q)) {
				this._searchPanelWidget.setQueryText(queryObject.q);

				this._onSearch(true);
			}
		},

		/**
		 * Perform search
		 *
		 * @private
		 */
		_search: function() {
			var self = this;
			this._utils.toggleSpinner(true, function() {
				try {
					self._sendRequest();
					self._update();
				} catch (ex) {
					self._arasObj.AlertError(ex.message);
				} finally {
					self._utils.toggleSpinner(false, null);
				}
			});
		},

		/**
		 * Update UI
		 *
		 * @private
		 */
		_update: function() {
			var totalRows = this._selectQueryServiceRequest.getTotalRows();
			var requestFacets = this._selectQueryServiceRequest.getQueryFacets();
			var resultFacets = [];
			var resultItems = [];
			if (totalRows > 0) {
				resultFacets = this._selectQueryServiceRequest.getResultFacets();
				resultItems = this._selectQueryServiceRequest.getResultItems();
			}

			//Update search panel
			this._searchPanelWidget.totalRows = totalRows;
			this._searchPanelWidget.facets = requestFacets;
			this._searchPanelWidget.update();

			//Update filters panel
			this._filtersPanelWidget.items = resultFacets;
			this._filtersPanelWidget.update();

			//Update results panel
			this._resultsPanelWidget.items = resultItems;
			this._resultsPanelWidget.update();

			this._updateSearchStatus();
		},

		/**
		 * Send request to server
		 *
		 * @private
		 */
		_sendRequest: function() {
			this._selectQueryServiceRequest.queryText = this._searchPanelWidget.getQueryText();
			this._selectQueryServiceRequest.start = this._searchPanelWidget.start;
			this._selectQueryServiceRequest.rows = this._searchPanelWidget.pageSize;
			this._selectQueryServiceRequest.run();
		},

		/**
		 * Update search status
		 *
		 * @private
		 */
		_updateSearchStatus: function() {
			var pageSize = this._searchPanelWidget.pageSize;
			var totalRows = this._selectQueryServiceRequest.getTotalRows();
			var currentPage = Math.round(this._selectQueryServiceRequest.start / pageSize);
			var pages = Math.ceil(totalRows / pageSize);

			var pageStatus = '';
			if (totalRows === 0) {
				pageStatus = '0 Items found.';
			} else {
				var bottomLimit = currentPage * pageSize + 1;
				var topLimit = ((totalRows - bottomLimit) > pageSize) ? bottomLimit + pageSize - 1 : totalRows;

				pageStatus = lang.replace('Items {0} - {1} of {2}. Page {3} of {4}', [bottomLimit, topLimit, totalRows, currentPage + 1, pages]);
			}

			this._arasObj.showStatusMessage('page_status', pageStatus);
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onSearch: function(resetFacets) {
			this._searchPanelWidget.start = 0;
			if (resetFacets) {
				this._selectQueryServiceRequest.resetQueryFacets();
				this._selectQueryServiceRequest.restoreDefaultQueryFacets();
			}

			this._search();
		},

		_onPageChange: function() {
			this._search();
		},

		_onFiltersReset: function() {
			this._searchPanelWidget.start = 0;
			this._selectQueryServiceRequest.resetQueryFacets();

			this._search();
		},

		_onFilterReset: function(facet) {
			this._searchPanelWidget.start = 0;
			this._selectQueryServiceRequest.removeQueryFacet(facet);

			this._search();
		},

		_onFilterLoadOptions: function(facet) {
			var selectQueryServiceRequestFacet = new SelectQueryServiceRequest({
				arasObj: this._arasObj
			});
			selectQueryServiceRequestFacet.rows = 0;
			selectQueryServiceRequestFacet.start = 0;

			selectQueryServiceRequestFacet.setRequestFacet(facet.name);
			var queryFacets = this._selectQueryServiceRequest.getQueryFacets();
			for (var i = 0; i < queryFacets.length; i++) {
				selectQueryServiceRequestFacet.setQueryFacet(queryFacets[i]);
			}
			selectQueryServiceRequestFacet.updateQueryFacet(facet);
			selectQueryServiceRequestFacet.run();

			//there is only one facet in response so return item with zero index
			var newFacet = selectQueryServiceRequestFacet.getResultFacets()[0];
			if (!this._utils.isNullOrUndefined(newFacet)) {
				return newFacet;
			}
			return facet;
		},

		_onFiltersChange: function(filters) {
			var self = this;

			this._searchPanelWidget.start = 0;
			filters.forEach(function(filter) {
				self._selectQueryServiceRequest.updateQueryFacet(filter);
			});

			this._search();
		}
	});
});
