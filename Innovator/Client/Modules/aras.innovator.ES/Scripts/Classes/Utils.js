define([
	'dojo',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojox/html/entities'
], function(dojo, declare, lang, entities) {
	return declare('ES.Utils', null, {
		_arasObj: null,

		constructor: function(args) {
			this._arasObj = args.arasObj;
		},

		/**
		 * Retunrs true if parameter is null or undefied, otherwise false.
		 *
		 * @param {Object} obj Some variable
		 * @returns {boolean}
		 */
		isNullOrUndefined: function(obj) {
			return ((typeof(obj) === 'undefined') || (obj === null));
		},

		/**
		 * Itearate through collection of innovator items
		 *
		 * @param {Array} itms Collection of items
		 * @param {Function} callback Callback
		 */
		iterateThroughItemsCollection: function(itms, callback) {
			if (itms.isError()) {
				return;
			}

			for (var i = 0; i < itms.getItemCount(); i++) {
				var itm = itms.getItemByIndex(i);

				callback(itm);
			}
		},

		/**
		 * Get image url
		 *
		 * @param {string} path Relative path or vault url
		 * @returns {string}
		 */
		getImageUrl: function(path) {
			var imageUrl = '';

			if (path === '') {
				return imageUrl;
			}

			if (!this.isNullOrUndefined(path) && (path !== '')) {
				if (path.indexOf('vault:///?fileId') === -1) {
					//It's a relative path
					imageUrl = this._arasObj.getBaseURL() + '/' + path;
				} else {
					//It's a vault url
					var fileId = path.replace(/vault:\/\/\/\?fileid\=/i, '');
					imageUrl = this._arasObj.IomInnovator.getFileUrl(fileId, this._arasObj.Enums.UrlType.SecurityToken);
				}
			}

			return imageUrl;
		},

		/**
		 * Convert Solr dates to local date format
		 *
		 * @param {string} date
		 * @returns {string}
		 */
		convertDateToLocalDateString: function(date) {
			var res = '';

			if (date === '') {
				return res;
			}

			res = this._arasObj.convertFromNeutral(date, 'date', 'short_date');

			return res;
		},

		/**
		 * Convert Solr dates to 'MM DD, YYYY' format
		 *
		 * @param {string} date
		 * @returns {string}
		 */
		convertDateToCustomFormat: function(date) {
			var res = '';

			if (date === '') {
				return res;
			}

			var monthShortNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

			var dateObj = new Date(date);
			var currentDay = dateObj.getDate();
			var currentMonthShortName = monthShortNames[dateObj.getMonth()];
			var currentYear = dateObj.getFullYear();

			res = lang.replace('{0} {1}, {2}', [currentMonthShortName, currentDay, currentYear]);

			return res;
		},

		/**
		 * Convert file size from bytes to user friendly format
		 *
		 * @param {integer} byteLength
		 * @returns {string}
		 */
		convertBytesToSize: function(byteLength) {
			if (byteLength) {
				var sizeUnits = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
				var i = Math.floor(Math.log(byteLength) / Math.log(1024));

				return (byteLength / Math.pow(1024, i)).toFixed(1) + ' ' + sizeUnits[i];
			} else {
				return '0 Bytes';
			}
		},

		/**
		 * Convert date to Aras Innovator neutral format
		 *
		 * @param {Date} dt Date object
		 * @returns {string}
		 */
		convertDateToNeutralFormat: function(dt) {
			var dateString = dt.getFullYear() + '-';
			dateString += this.pad(dt.getMonth() + 1) + '-';
			dateString += this.pad(dt.getDate()) + 'T';
			dateString += this.pad(dt.getHours()) + ':';
			dateString += this.pad(dt.getMinutes()) + ':';
			dateString += this.pad(dt.getSeconds());

			return dateString;
		},

		/**
		 * Add zero to number if it's less than 10 (e.g. 1 => '01')
		 *
		 * @param {int} x Number
		 * @returns {string}
		 */
		pad: function(x) {
			return (x < 10) ? '0' + x : '' + x;
		},

		/**
		 * Set visibility of specified HTML node
		 *
		 * @param {object} nd HTML node
		 * @param {boolean} visible Is visible?
		 */
		setNodeVisibility: function(nd, visible) {
			if (dojo.hasClass(nd, 'hidden') && visible) {
				dojo.removeClass(nd, 'hidden');
			}

			if (!dojo.hasClass(nd, 'hidden') && !visible) {
				dojo.addClass(nd, 'hidden');
			}
		},

		/**
		 * Get xml resource by key
		 *
		 * @param {string} resourceKey Resource key
		 */
		getResourceValueByKey: function(resourceKey) {
			var resultDom = this._arasObj.createXMLDocument();
			resultDom.load(this._arasObj.getI18NXMLResource('ui_resources.xml', this._arasObj.getScriptsURL() + '../Modules/aras.innovator.ES/'));
			var resourceNode = resultDom.selectSingleNode(lang.replace('ui_resources/resource[@key="{0}"]', [resourceKey]));

			return this.isNullOrUndefined(resourceNode) ? '' : resourceNode.getAttribute('value');
		},

		/**
		 * Get state of 'Aras.EnterpriseSearch' feature (true if activated, false otherwise)
		 *
		 * @returns {boolean}
		 */
		isFeatureActivated: function() {
			var itm = this._arasObj.newIOMItem('ItemType', 'ES_CheckFeature');
			itm = itm.apply();

			return !itm.isError();
		},

		/**
		 * Encode special symbols in string
		 *
		 * @param {string} str
		 * @returns {string}
		 */
		encode: function(str) {
			return entities.encode(str);
		},

		/**
		 * Get parent window
		 *
		 * @returns {object}
		 */
		getTopWindow: function() {
			return this._arasObj.getMostTopWindowWithAras(window);
		},

		/**
		 * Toggle spinner on ES main window
		 *
		 * @param {boolean} state Spinner state
		 * @param {Function} callback Callback function that will be executed after turning on ES spinner
		 * @returns {boolean}
		 */
		toggleSpinner: function(state, callback) {
			var spinner = document.getElementById('searchSpinner');

			if (!spinner) {
				return false;
			}

			this.setNodeVisibility(spinner, state);

			if (callback && state) {
				if (window.webkitRequestAnimationFrame) {
					setTimeout(callback, 1000 / 60);
				} else {
					window.requestAnimationFrame(callback);
				}
			}

			return true;
		},

		/**
		 * Truncate string to specified length
		 *
		 * @param {string} sourceString String which should be truncated
		 * @param {int} maxLength Max length of result string
		 * @returns {string}
		 */
		truncateString: function(sourceString, maxLength) {
			if (this.isNullOrUndefined(sourceString) || maxLength <= 0) {
				return sourceString;
			}

			var truncatedString = sourceString;
			if (truncatedString.length > maxLength) {
				truncatedString = truncatedString.substring(0, maxLength).concat('...');
			}

			return truncatedString;
		}
	});
});
