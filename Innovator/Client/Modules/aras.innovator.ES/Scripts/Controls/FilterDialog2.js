define([
	'dojo',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/TooltipDialog',
	'dijit/popup',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/FilterDialog2.html',
	'dojo/text!./../../Views/Templates/FilterDialog2FilterOption1.html',
	'dojo/text!./../../Views/Templates/FilterDialog2FilterOption2.html'
], function(dojo,
			declare,
			lang,
			_WidgetBase,
			_TemplatedMixin,
			TooltipDialog,
			popup,
			Utils,
			filterDialog2Template,
			filterDialog1FilterOption1,
			filterDialog1FilterOption2) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		_tooltipDialog: null,
		_isOpened: false,

		_maxItemCountInOneColumn: 15,

		_onOkButtonClick: null,
		_onCancelButtonClick: null,

		facet: null,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this.facet = args.facet;
			this._onOkButtonClick = args.onOkButtonClick;
			this._onCancelButtonClick = args.onCancelButtonClick;

			var leftFilterOptionsMarkup = '';
			var rightFilterOptionsMarkup = '';
			var filterOptionsMarkup = '';

			var options = this.facet.getOptions();
			var currentItemCountInLeftColumn = 0;
			var maxItemCountInLeftColumn = (options.length > this._maxItemCountInOneColumn * 2) ? options.length / 2 : this._maxItemCountInOneColumn;
			var prevCharacter = '';
			options.forEach(function(option, index) {
				var character = option.label[0].toUpperCase();
				var characterWasChanged = (character !== prevCharacter);

				var templateProperties = {};
				templateProperties.index = index;
				templateProperties.label = characterWasChanged ? character : '';
				templateProperties.value = option.label;
				templateProperties.count = option.count;
				templateProperties.checked = option.isSelected ? 'checked' : '';

				var markup = lang.replace(filterDialog1FilterOption1, templateProperties);

				if (characterWasChanged) {
					if (currentItemCountInLeftColumn < maxItemCountInLeftColumn) {
						leftFilterOptionsMarkup += markup;

						currentItemCountInLeftColumn++;
					} else {
						rightFilterOptionsMarkup += markup;
					}
				} else {
					leftFilterOptionsMarkup += markup;

					currentItemCountInLeftColumn++;
				}

				templateProperties.visibility = option.isSelected ? 'block' : 'none';
				filterOptionsMarkup += lang.replace(filterDialog1FilterOption2, templateProperties);

				prevCharacter = character;
			});

			//Initialize labels
			var selectedFiltersLabel = this._utils.getResourceValueByKey('filters.selected_filters');
			var okLabel = this._utils.getResourceValueByKey('buttons.ok');
			var cancelLabel = this._utils.getResourceValueByKey('buttons.cancel');

			this.templateString = lang.replace(
				filterDialog2Template,
				[this.facet.title, leftFilterOptionsMarkup, rightFilterOptionsMarkup, selectedFiltersLabel, filterOptionsMarkup, okLabel, cancelLabel]
			);
		},

		postCreate: function() {
			this._tooltipDialog = new TooltipDialog({
				style: 'width: 568px; outline: none;',
				content: this.domNode
			});
		},

		/**
		 * Show dialog
		 *
		 * @param {object|string} node DOM node or id of node for placing the pop-up
		 */
		show: function(node) {
			var self = this;

			var layoutContainer = document.getElementById('layoutContainer');
			if (layoutContainer) {
				cloakNode = document.createElement('div');
				cloakNode.setAttribute('id', 'bodyCloakDiv');
				cloakNode.setAttribute('class', 'bodyCloak');
				layoutContainer.appendChild(cloakNode);
			}

			if (!this._isOpened) {
				popup.open({
					popup: this._tooltipDialog,
					around: node,
					maxHeight: dojo.window.getBox().h,
					orient: ['after-centered', 'below'],
					onCancel: function() {
						popup.close(self._tooltipDialog);
						self.hide();
					}
				});

				this._isOpened = true;
			}
		},

		/**
		 * Hide dialog
		 */
		hide: function() {
			popup.close(this._tooltipDialog);
			var layoutContainer = document.getElementById('layoutContainer');
			var bodyCloak = document.getElementById('bodyCloakDiv');
			if (layoutContainer && bodyCloak) {
				layoutContainer.removeChild(bodyCloak);
			}

			this._isOpened = false;
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onOkButtonClickEventHandler: function() {
			var self = this;

			var options = this.facet.getOptions();
			options.forEach(function(option, index) {
				option.isSelected = self['filterOptionCheckBox' + index].checked;
			});

			this.hide();

			if (!this._utils.isNullOrUndefined(this._onOkButtonClick)) {
				this._onOkButtonClick(self.facet);
			}
		},

		_onCancelButtonClickEventHandler: function() {
			this.hide();

			if (!this._utils.isNullOrUndefined(this._onCancelButtonClick)) {
				this._onCancelButtonClick();
			}
		},

		_onSearchTextBoxChangedEventHandler: function() {
			var self = this;
			var searchText = this._filterDialog2SearchTextBox.value.toLowerCase();

			if (searchText && searchText.length > 0) {
				this._filterDialog2ClearSearchButton.style.visibility = 'visible';
				this._filterDialog2SearchIcon.style.visibility = 'hidden';
			} else {
				this._filterDialog2ClearSearchButton.style.visibility = 'hidden';
				this._filterDialog2SearchIcon.style.visibility = 'visible';
			}

			var options = this.facet.getOptions();
			options.forEach(function(option, index) {
				if (option.label.toLowerCase().indexOf(searchText) !== -1) {
					self['filterOptionContainer' + index].style.display = 'block';
				} else {
					self['filterOptionContainer' + index].style.display = 'none';
				}
			});
		},

		_onSearchTextBoxClearSearchCriteriaButtonHandler: function() {
			this._filterDialog2SearchTextBox.value = '';
			this._onSearchTextBoxChangedEventHandler();
		},

		_onFilterOptionStateChangedEventHandler: function(ev) {
			var optionIndex = ev.target.getAttribute('data-filter-option-index');

			var isSelected = false;
			if (ev.target.tagName.toLowerCase() === 'input') {
				isSelected = ev.target.checked;
			} else {
				this['filterOptionCheckBox' + optionIndex].checked = isSelected;
			}

			this['selectedFilterOptionContainer' + optionIndex].style.display = isSelected ? 'block' : 'none';
		}
	});
});
