define([
	'dojo',
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dojo/_base/lang',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/ResultItem.html',
	'dojo/text!./../../Views/Templates/ResultItemProperty.html',
	'dojo/text!./../../Views/Templates/ResultItemHighlight.html',
	'dojo/text!./../../Views/Templates/ResultFileHighlight.html',
	'dojo/text!./../../Views/Templates/SubResultItem.html',
	'dojo/text!./../../Views/Templates/FileItem.html'
], function(dojo,
			declare,
			_WidgetBase,
			_TemplatedMixin,
			lang,
			Utils,
			resultItemTemplate,
			resultItemPropertyTemplate,
			resultItemHighlightTemplate,
			resultFileHighlightTemplate,
			subResultItemTemplate,
			fileItemTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		_item: null,

		_modifiedInfoTextResource: null,
		_propertiesTextResource: null,
		_currentTextResource: null,
		_notCurrentTextResource: null,

		templateString: '',
		baseClass: 'resultItem',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._item = args.item;
			this._modifiedInfoTextResource = args.modifiedInfoTextResource;
			this._propertiesTextResource = args.propertiesTextResource;
			this._currentTextResource = args.currentTextResource;
			this._notCurrentTextResource = args.notCurrentTextResource;

			var keyedName = this._getHighlightContent(this._item, 'keyed_name');

			var templateProperties = {};
			templateProperties.iconPath = this._utils.getImageUrl(this._item.configurations.iconPath);
			templateProperties.itemTypeColor = this._item.configurations.itemTypeColor;
			templateProperties.type = this._item.getProperty('aes_doc_type', 'label');

			if (keyedName === '') {
				keyedName = this._item.getProperty('keyed_name', 'label');
				var highlightedFileShortKeyedName = this._getHighlightContent(this._item, 'filename_short');

				if (templateProperties.type === 'File' && highlightedFileShortKeyedName !== '') {
					var fileNameShort = this._item.getProperty('filename_short', 'label'); // Get short name

					if (keyedName.length > fileNameShort.length) {
						var extension = keyedName.substring(fileNameShort.length);
						keyedName = highlightedFileShortKeyedName.trim() + extension;
					}
				}
			}

			templateProperties.keyedName = keyedName;
			templateProperties.generalInfo = this.getGeneralInfo(this._item);
			templateProperties.modifiedMessage = lang.replace(
				this._modifiedInfoTextResource,
				[this._utils.convertDateToCustomFormat(this._item.getProperty('modified_on', 'value')) + '<br>', this._item.getProperty('modified_by_id', 'label')]
			);
			templateProperties.propertiesLabel = this._propertiesTextResource;
			templateProperties.propertiesMarkup = this._getPropertiesMarkup(this._item);
			templateProperties.highlightsMarkup = this._getHighlightsMarkup(this._item);
			templateProperties.childItemsMarkup = this._getChildItemsMarkup(this._item);

			this.templateString = lang.replace(resultItemTemplate, templateProperties);
		},

		postCreate: function() {
			if (this._item.subItems.length > 0 && this._item.highlights.length > 0) {
				dojo.addClass(this.childItemsContainer, 'parentItemHighlighted');
			}
			if (this._item.subItems.length > 1) {
				//We need to hide some elements and display 'More' link
				dojo.addClass(this.childItemsContainer, 'childItemsContainerCollapsed');
				this.moreLinkContainer.style.display = 'block';
			}
		},

		/**
		 * Get HTML markup of properties
		 *
		 * @param {object} item Item
		 * @returns {string}
		 * @private
		 */
		_getPropertiesMarkup: function(item) {
			var markup = '';
			var globalPropertyCounter = -1;
			var isDivOpened = false;
			var colls = 5;

			for (var propertyName in item.properties) {
				if (!item.properties.hasOwnProperty(propertyName)) {
					continue;
				}

				var isUI = item.getProperty(propertyName, 'is_ui');
				if (!isUI) {
					continue;
				}

				var title = item.getProperty(propertyName, 'title');
				var label = item.getProperty(propertyName, 'label');

				var cssClass = '';
				var userId = '';
				var openingDiv = '';
				var closingDiv = '';

				if ((propertyName === 'created_by_id') || (propertyName === 'modified_by_id') || (propertyName === 'locked_by_id')) {
					userId = item.getProperty(propertyName, 'value');
					cssClass = 'linkButton';
				}

				globalPropertyCounter++;

				if (globalPropertyCounter % colls === 0 && !isDivOpened) {
					openingDiv = '<div>';
					isDivOpened = true;
				} else if ((globalPropertyCounter + 1) % colls === 0 && isDivOpened) {
					closingDiv = '</div>';
					isDivOpened = false;
				}

				markup += lang.replace(resultItemPropertyTemplate, [title, cssClass, userId, label, openingDiv, closingDiv]);
			}

			if (isDivOpened) {
				markup += '</div>';
			}

			return markup;
		},

		/**
		 * Get highlight content by property name
		 *
		 * @param {object} item Item
		 * @param {string} name Property name
		 * @returns {string}
		 * @private
		 */
		_getHighlightContent: function(item, name) {
			var res = '';

			for (var i = 0; i < item.highlights.length; i++) {
				var highlight = item.highlights[i];
				if (highlight.name === name) {
					res = this._prepareHighlightContent(highlight.content);
				}
			}

			return res;
		},

		/**
		 * Get HTML markup of highlights
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		_getHighlightsMarkup: function(item) {
			var self = this;
			var markup = '';

			item.highlights.forEach(function(highlight) {
				var highlightContent = self._prepareHighlightContent(highlight.content);
				var propertyTitle = item.getProperty(highlight.name, 'title');
				var propertyLabel = propertyTitle !== '' ? propertyTitle : highlight.name;

				if (item.getProperty('aes_doc_type', 'value') !== 'File') {
					markup += lang.replace(resultItemHighlightTemplate, [propertyLabel, highlightContent]);
				} else {
					if (propertyLabel === 'content') {
						markup += lang.replace(resultFileHighlightTemplate, [highlightContent]);
					}
				}
			});

			return markup;
		},

		/**
		 * Generate info about item
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		getGeneralInfo: function(item) {
			var string = '';
			var isRoot = (item.getProperty('aes_root', 'value') === 'true') ? true : false;
			var infoArr = ['major_rev', 'generation', 'state'];

			var name = (isRoot) ? this._getHighlightContent(item, 'name') : '';
			if (name === '') {
				name = item.getProperty('name', 'label');
			}
			if (name !== '') {
				string += '<strong>' + name.trim() + '</strong>, ';
			}

			infoArr.forEach(function(infoElem, index) {
				var property = item.getProperty(infoElem, 'label');
				if (property.length > 0) {
					string += (index === 0) ? property : ' - ' + property;
				}
			});

			if (this._utils.isNullOrUndefined(item.subItems)) {
				var isCurrent = item.getProperty('is_current', 'label');
				string += (isCurrent === 'true') ? ', ' + this._currentTextResource : ', ' + this._notCurrentTextResource;
			}

			return string;
		},

		/**
		 * Get HTML markup of child items
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		_getChildItemsMarkup: function(item) {
			var markup = '';
			var self = this;

			item.subItems.forEach(function(childItem, index) {
				var keyedLabel = childItem.getProperty('keyed_name', 'label');
				var keyedName = self._getHighlightContent(childItem, 'keyed_name');
				if (keyedName === '') { // Search object isn't full file name
					keyedName = self._getHighlightContent(childItem, 'filename_short');
					if (keyedName === '') { // Search object isn't short file name (without extension)
						keyedName = keyedLabel;
					} else {
						var fileShortName = childItem.getProperty('filename_short', 'label'); // Get short name
						if (keyedLabel.length > fileShortName.length) {
							keyedName = keyedName.trim() + keyedLabel.substring(fileShortName.length);
						}
					}
				}

				var templateProperties = {};
				if (childItem.getProperty('aes_doc_type', 'value') !== 'File') {
					templateProperties.index = index;
					templateProperties.iconPath = self._utils.getImageUrl(childItem.configurations.iconPath);
					templateProperties.itemTypeColor = childItem.configurations.itemTypeColor;
					templateProperties.id = childItem.getProperty('id', 'value');
					templateProperties.type = childItem.getProperty('aes_doc_type', 'value');
					templateProperties.keyedName = keyedName;
					templateProperties.generalInfo = self.getGeneralInfo(childItem);
					templateProperties.propertiesLabel = self._propertiesTextResource;
					templateProperties.modifiedMessage = lang.replace(
						self._modifiedInfoTextResource,
						[self._utils.convertDateToCustomFormat(childItem.getProperty('modified_on', 'value'))  + '<br>', childItem.getProperty('modified_by_id', 'label')]
					);
					templateProperties.highlightsMarkup = self._getHighlightsMarkup(childItem);
					templateProperties.propertiesMarkup = self._getPropertiesMarkup(childItem);

					markup += lang.replace(subResultItemTemplate, templateProperties);
				} else {
					templateProperties.iconPath = self._utils.getImageUrl(childItem.configurations.iconPath);
					templateProperties.id = childItem.getProperty('id', 'value');
					templateProperties.keyedName = keyedName;
					templateProperties.type = childItem.getProperty('aes_doc_type', 'value');
					templateProperties.highlightsMarkup = self._getHighlightContent(childItem, 'content');

					markup += lang.replace(fileItemTemplate, templateProperties);
				}
			});

			return markup;
		},

		/**
		 * Prepare highlight content:
		 *  - escape special HTML characters
		 *  - replace pre and post highlight strings with &lt;b&gt; and &lt;/b&gt;
		 *
		 * @param {string} highlightContent Content of highlight
		 * @returns {string}
		 * @private
		 */
		_prepareHighlightContent: function(highlightContent) {
			var res = this._escapeHtml(highlightContent);

			res = res.replace(new RegExp(this._escapeRegExp('[es_start_selection]'), 'g'), '<b>');
			res = res.replace(new RegExp(this._escapeRegExp('[es_end_selection]'), 'g'), '</b>');

			return res;
		},

		/**
		 * Escape HTML special characters
		 *
		 * @param {string} htmlMarkup HTML markup
		 * @returns {string}
		 * @private
		 */
		_escapeHtml: function(htmlMarkup) {
			return htmlMarkup.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#039;');
		},

		/**
		 * Escape regular expression special characters
		 *
		 * @param {string} str Regular expression
		 * @returns {string}
		 * @private
		 */
		_escapeRegExp: function(str) {
			return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
		},

		_setNodeVisibility: function(node, visibility) {
			if (!this._utils.isNullOrUndefined(node)) {
				node.style.display = visibility ? 'block' : 'none';
			}
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onResultItemClickEventHandler: function() {
			this._arasObj.uiShowItem(this._item.getProperty('aes_doc_type', 'value'), this._item.getProperty('id', 'value'));
		},

		_onSubResultItemClickEventHandler: function(ev) {
			var element = ev.target;
			if (element.tagName !== 'DIV') {
				element = element.parentNode;
			}

			var id = element.getAttribute('data-item-id');
			var type = element.getAttribute('data-item-type');

			this._arasObj.uiShowItem(type, id);
		},

		_onFileItemClickEventHandler: function(ev) {
			var element = ev.target;
			if (element.tagName !== 'DIV') {
				element = element.parentNode;
			}

			var id = element.getAttribute('data-item-id');
			var fileUrl = this._arasObj.IomInnovator.getFileUrl(id, this.arasObj.Enums.UrlType.SecurityToken);

			const wnd = window.open('', '_blank');
			wnd.opener = null;
			wnd.location = fileUrl;
		},

		_onItemPropertiesLinkClickEventHandler: function(ev) {
			if (dojo.hasClass(ev.target, 'propertiesLabelContainerCollapsed')) {
				dojo.removeClass(ev.target, 'propertiesLabelContainerCollapsed');
				dojo.addClass(ev.target, 'propertiesLabelContainerExpanded');

				//TODO: maybe it makes sense to use utils function
				this._setNodeVisibility(this.propertiesContainerWrapper, true);
			} else {
				dojo.removeClass(ev.target, 'propertiesLabelContainerExpanded');
				dojo.addClass(ev.target, 'propertiesLabelContainerCollapsed');

				//TODO: maybe it makes sense to use utils function
				this._setNodeVisibility(this.propertiesContainerWrapper, false);
			}
		},

		_onItemPropertyLinkClickEventHandler: function(ev) {
			var userId = ev.target.getAttribute('data-user-id');
			if (!this._utils.isNullOrUndefined(userId)) {
				this._arasObj.uiShowItem('User', userId);
			}
		},

		_onSubItemPropertiesLinkClickEventHandler: function(ev) {
			var index = ev.target.getAttribute('data-index');

			if (dojo.hasClass(ev.target, 'propertiesLabelContainerCollapsed')) {
				dojo.removeClass(ev.target, 'propertiesLabelContainerCollapsed');
				dojo.addClass(ev.target, 'propertiesLabelContainerExpanded');

				// jscs:disable
				//TODO: maybe it makes sense to use utils function
				this._setNodeVisibility(this['subItemPropertiesContainerWrapper' + index], true);
				// jscs:enable
			} else {
				dojo.removeClass(ev.target, 'propertiesLabelContainerExpanded');
				dojo.addClass(ev.target, 'propertiesLabelContainerCollapsed');

				// jscs:disable
				//TODO: maybe it makes sense to use utils function
				this._setNodeVisibility(this['subItemPropertiesContainerWrapper' + index], false);
				// jscs:enable
			}
		},

		_onMoreLinkClickEventHandler: function(ev) {
			if (dojo.hasClass(ev.target, 'moreButton')) {
				dojo.removeClass(ev.target, 'moreButton');
				dojo.addClass(ev.target, 'lessButton');
				dojo.removeClass(this.childItemsContainer, 'childItemsContainerCollapsed');
			} else {
				dojo.removeClass(ev.target, 'lessButton');
				dojo.addClass(ev.target, 'moreButton');
				dojo.addClass(this.childItemsContainer, 'childItemsContainerCollapsed');
			}
		}
	});
});
