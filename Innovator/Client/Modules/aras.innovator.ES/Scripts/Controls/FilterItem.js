define([
	'dojo',
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dojo/_base/lang',
	'ES/Scripts/Controls/FilterDialog2',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/FilterItem.html',
	'dojo/text!./../../Views/Templates/FilterOption.html'
], function(dojo, declare, _WidgetBase, _TemplatedMixin, lang, FilterDialog2, Utils, filterItemTemplate, filterOptionTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		//Labels
		_clearLabel: '',
		_moreLabel: '',

		id: '',

		item: null,

		showCounts: true,

		allItemsLoaded: false,

		_filterDialog2: null,

		templateString: '',

		_maxVisibleOptionsCount: 10,

		_onFilterChange: null,

		_onFilterReset: null,

		_onFilterLoadOptions: null,

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._lang = lang;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this.item = args.item;
			this.showCounts = args.showCounts;
			this._onFilterChange = args.onFilterChange;
			this._onFilterReset = args.onFilterReset;
			this._onFilterLoadOptions = args.onFilterLoadOptions;

			var templateProperties = {};
			templateProperties.filterLabel = this.item.title;
			var optionsCount = this.item.getOptions().length;
			templateProperties.filterOptionsCount =
				(optionsCount >= 100) ? '' : (optionsCount > this._maxVisibleOptionsCount) ? optionsCount - this._maxVisibleOptionsCount : optionsCount;
			templateProperties.filterOptionsMarkup = this._getFilterOptionsMarkup();

			//Initialize labels
			templateProperties.clearLabel = args.clearLabel;
			templateProperties.moreLabel = args.moreLabel;

			this.templateString = lang.replace(filterItemTemplate, templateProperties);
		},

		postCreate: function() {
			if (this.item.getOptions().length > this._maxVisibleOptionsCount) {
				//We need to show "more" link
				this._utils.setNodeVisibility(this.filterItemMoreOptionsLinkContainer, true);
			}
		},

		/**
		 * Uncheck all options
		 */
		clearOptions: function() {
			//Reset all checkboxes
			var inputs = this.domNode.getElementsByTagName('input');
			for (var i = 0; i < inputs.length; i++) {
				var input = inputs[i];

				input.checked = false;
			}

			//Reset all options
			var options = this.item.getOptions();
			for (i = 0; i < options.length; i++) {
				var option = options[i];

				option.isSelected = false;
			}
		},

		_getFilterOptionsMarkup: function() {
			var res = '';

			var options = this.item.getOptions();
			var count = (options.length > this._maxVisibleOptionsCount) ? this._maxVisibleOptionsCount : options.length;
			var templateProperties = {};
			for (var i = 0; i < count; i++) {
				var option = options[i];

				templateProperties.index = i;
				templateProperties.name = option.name;
				templateProperties.label = option.label;
				templateProperties.count = option.count;
				templateProperties.state = option.isSelected ? 'checked' : '';
				templateProperties.visibility = this.showCounts ? 'visible' : 'hidden';

				res += lang.replace(filterOptionTemplate, templateProperties);
			}

			return res;
		},

		_openFilterDialog: function(ev) {
			this._filterDialog2 = FilterDialog2({
				arasObj: this._arasObj,
				facet: this.item,
				onOkButtonClick: (function() {
					if (!this._utils.isNullOrUndefined(this._onFilterChange)) {
						this._onFilterChange(this.item);
					}
				}).bind(this)
			});
			this._filterDialog2.show(ev.target);
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onFilterExpandCollapseEventHandler: function() {
			if (dojo.hasClass(this.filterItemContainer, 'filterItemExpanded')) {
				dojo.removeClass(this.filterItemContainer, 'filterItemExpanded');
				dojo.addClass(this.filterItemContainer, 'filterItemCollapsed');
			} else {
				dojo.removeClass(this.filterItemContainer, 'filterItemCollapsed');
				dojo.addClass(this.filterItemContainer, 'filterItemExpanded');
			}
		},

		_onFilterItemClearOptionsClickEventHandler: function() {
			this.clearOptions();

			if (!this._utils.isNullOrUndefined(this._onFilterReset)) {
				this._onFilterReset(this.item);
			}
		},

		_onFilterOptionChangeEventHandler: function(ev) {
			var name = ev.target.getAttribute('data-option-name');
			var option = this.item.getOption(name);

			option.isSelected = ev.target.checked;

			if (!this._utils.isNullOrUndefined(this._onFilterChange)) {
				this._onFilterChange(this.item);
			}
		},

		_onMoreLinkClickEventHandler: function(ev) {
			var self = this;
			self.originalItemClone = this._lang.clone(self.item);

			//Destroy old dialog
			if (!this._utils.isNullOrUndefined(this._filterDialog2)) {
				this._filterDialog2.destroy();
			}

			if (!self.allItemsLoaded) {
				this._utils.toggleSpinner(true, function() {
					try {
						if (!self._utils.isNullOrUndefined(self._onFilterLoadOptions)) {
							self.resultItem = self._onFilterLoadOptions(self.item);
						}

						var newOptions = self.resultItem.getOptions();
						for (var i = 0; i < newOptions.length; i++) {
							var option = newOptions[i];
							if (self._utils.isNullOrUndefined(self.item.getOption(option.name))) {
								self.item.addOption(option.name, option.label, option.count, option.isSelected);
							}
						}

						self.allItemsLoaded = true;
					} catch (ex) {
						self._arasObj.AlertError(ex.message);
					} finally {
						self._utils.toggleSpinner(false, null);
						self._openFilterDialog(ev);
					}
				});
			} else {
				self._openFilterDialog(ev);
			}
		}
	});
});
