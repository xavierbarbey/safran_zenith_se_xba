define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Controls/ResultItem',
	'dojo/text!./../../Views/Templates/ResultPanel.html'
], function(declare, _WidgetBase, _TemplatedMixin, Utils, ResultItem, resultPanelTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		arasObj: null,

		items: [],
		widgets: [],

		templateString: '',
		baseClass: 'resultItem',

		_modifiedInfoTextResource: '',
		_propertiesTextResource: '',
		_currentTextResource: '',
		_notCurrentTextResource: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			this._modifiedInfoTextResource = this._utils.getResourceValueByKey('results.modified_info');
			this._propertiesTextResource = this._utils.getResourceValueByKey('results.properties');
			this._currentTextResource = this._utils.getResourceValueByKey('results.current');
			this._notCurrentTextResource = this._utils.getResourceValueByKey('results.not_current');

			this.templateString = resultPanelTemplate;

			declare.safeMixin(this, args);
		},

		postCreate: function() {
			this.inherited(arguments);
		},

		update: function() {
			var self = this;

			//Destroy old widgets
			this.widgets.forEach(function(widget) {
				widget.destroy();
			});
			this.widgets = [];

			//Create new widgets
			var docFragment = document.createDocumentFragment();
			this.items.forEach(function(item) {
				var widget = new ResultItem({
					arasObj: self._arasObj,
					item: item,
					modifiedInfoTextResource: self._modifiedInfoTextResource,
					propertiesTextResource: self._propertiesTextResource,
					currentTextResource: self._currentTextResource,
					notCurrentTextResource: self._notCurrentTextResource
				});
				docFragment.appendChild(widget.domNode);

				self.widgets.push(widget);
			});

			this.domNode.appendChild(docFragment);
		}
	});
});
