define([
	'dojo',
	'dojo/keys',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'ES/Scripts/Controls/SearchPanelFilterOption',
	'ES/Scripts/Controls/FilterDialog1',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/SearchPanel.html'
], function(dojo, keys, declare, lang, _WidgetBase, _TemplatedMixin, SearchPanelFilterOption, FilterDialog1, Utils, searchPanelTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		arasObj: null,

		start: 0,
		pageSize: 10,
		maxPageSize: 25,
		totalRows: 0,

		widgets: [],

		facets: [],

		_filterDialog1: null,

		_onSearch: null,
		_onFiltersChange: null,
		_onPageChange: null,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			this._onSearch = args.onSearch;
			this._onFiltersChange = args.onFiltersChange;
			this._onPageChange = args.onPageChange;

			var runSearchHint = this._utils.getResourceValueByKey('hint.run_search');
			var pageSizeHint = this._utils.getResourceValueByKey('hint.page_size');
			var previousPageHint = this._utils.getResourceValueByKey('hint.previous_page');
			var nextPageHint = this._utils.getResourceValueByKey('hint.next_page');
			var refreshHint = this._utils.getResourceValueByKey('hint.refresh');

			this.templateString = lang.replace(
				searchPanelTemplate,
				[runSearchHint, pageSizeHint, previousPageHint, nextPageHint, refreshHint]
			);

			window.addEventListener('resize', this._updateButtons.bind(this));
		},

		postCreate: function() {
			this._rowCountTextBox.value = this.pageSize;
		},

		/**
		 * Get query text
		 *
		 * @returns {string}
		 */
		getQueryText: function() {
			return this._searchTextBox.value;
		},

		/**
		 * Set query text
		 *
		 * @param {string} value
		 */
		setQueryText: function(value) {
			this._searchTextBox.value = value;
			this._updateClearSearchButtonState();
		},

		/**
		 * Update widget
		 */
		update: function() {
			var self = this;

			//Destroy old widgets
			this.widgets.forEach(function(widget) {
				widget.destroy();
			});
			this.widgets = [];

			//Create new widgets
			var docFragment = document.createDocumentFragment();
			this.facets.forEach(function(facet, index) {
				var options = facet.getOptions();
				options.forEach(function(option) {
					if (option.isSelected) {
						var widget = new SearchPanelFilterOption({
							arasObj: self._arasObj,
							filterIndex: index,
							filterOptionName: option.name,
							filterOptionLabel: option.label,
							onRemoveFilterOption: self._onRemoveFilterOptionClickEventHandler.bind(self)
						});
						docFragment.appendChild(widget.domNode);

						self.widgets.push(widget);
					}
				});
			});
			this._filterOptionsContainer.appendChild(docFragment);

			this._updateButtons();
		},

		/**
		 * Update state of widget buttons
		 *
		 * @private
		 */
		_updateButtons: function() {
			//Update state of "Prevous Page" button
			if (this.start === 0) {
				if (!dojo.hasClass(this._previousButton, 'transparentImage')) {
					dojo.addClass(this._previousButton, 'transparentImage');
					this._previousButton.style.cursor = 'default';
				}
			} else {
				if (dojo.hasClass(this._previousButton, 'transparentImage')) {
					dojo.removeClass(this._previousButton, 'transparentImage');
					this._previousButton.style.cursor = 'pointer';
				}
			}

			//Update state of "Next Page" button
			if ((this.start + this.pageSize) >= this.totalRows) {
				if (!dojo.hasClass(this._nextButton, 'transparentImage')) {
					dojo.addClass(this._nextButton, 'transparentImage');
					this._nextButton.style.cursor = 'default';
				}
			} else {
				if (dojo.hasClass(this._nextButton, 'transparentImage')) {
					dojo.removeClass(this._nextButton, 'transparentImage');
					this._nextButton.style.cursor = 'pointer';
				}
			}

			//Update state of expand button
			if (this._isFilterOptionsContainerOverflowed()) {
				this._filterOptionsShowButton.style.visibility = 'visible';
			} else {
				this._filterOptionsShowButton.style.visibility = 'hidden';
			}
		},

		/**
		 * Returns true if "Previous Page" button enabled
		 *
		 * @returns {boolean}
		 * @private
		 */
		_isPreviousPageButtonEnabled: function() {
			return !dojo.hasClass(this._previousButton, 'transparentImage');
		},

		/**
		 * Returns true if "Next Page" button enabled
		 *
		 * @returns {boolean}
		 * @private
		 */
		_isNextPageButtonEnabled: function() {
			return !dojo.hasClass(this._nextButton, 'transparentImage');
		},

		_isFilterOptionsContainerOverflowed: function() {
			return this._filterOptionsContainer.scrollHeight - 5 > this._filterOptionsContainer.clientHeight;
		},

		/**
		 * Update search status
		 *
		 * @private
		 */
		_updateSearchStatus: function() {
			var currentPage = Math.round(this.start / this.pageSize);
			var pages = Math.round(this.totalRows / this.pageSize);

			var pageStatus = '';
			if (this.totalRows === 0) {
				pageStatus = '0 Items found.';
			} else {
				var bottomLimit = currentPage * this.pageSize + 1;
				var topLimit = ((this.totalRows - bottomLimit) > this.pageSize) ? bottomLimit + this.pageSize - 1 : this.totalRows;

				pageStatus = lang.replace('Items {0} - {1} of {2}. Page {3} of {4}', [bottomLimit, topLimit, this.totalRows, currentPage + 1, (pages === 0) ? '1' : pages]);
			}

			this._arasObj.showStatusMessage('page_status', pageStatus);
		},

		_updateClearSearchButtonState: function() {
			var value = this._searchTextBox.value;
			if (value && value.length > 0) {
				this._clearSearchButton.style.visibility = 'visible';
			} else {
				this._clearSearchButton.style.visibility = 'hidden';
			}
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onSearchPanelTextBoxKeyPressEventHandler: function(ev) {
			if ((ev.keyCode === keys.ENTER) && (!this._utils.isNullOrUndefined(this._onSearch))) {
				this._onSearch(true);
			}
		},

		_onSearchPanelTextBoxKeyUpEventHandler: function() {
			this._updateClearSearchButtonState();
		},

		_onSearchPanelSearchButtonClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onSearch)) {
				this._onSearch(true);
			}
		},

		_onSearchPanelRefreshButtonClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onSearch)) {
				this._onSearch(false);
			}
		},

		_onRowCountTextBoxChangeEventHandler: function(ev) {
			var rows = +ev.target.value;

			if (!this._arasObj.isPositiveInteger(rows)) {
				this._arasObj.AlertError(this._utils.getResourceValueByKey('message.page_size_should_be_positive'));
				this._rowCountTextBox.value = this.pageSize;
			} else if (rows > this.maxPageSize) {
				this._arasObj.AlertError(lang.replace(this._utils.getResourceValueByKey('message.page_size_should_be_less'), [this.maxPageSize]));
				this._rowCountTextBox.value = this.pageSize;
			} else {
				this.pageSize = rows;
			}
		},

		_onSearchPanelPreviousPageButtonClickEventHandler: function() {
			if (!this._isPreviousPageButtonEnabled()) {
				return;
			}

			var start = this.start - this.pageSize;
			this.start = (start > 0) ? start : 0;

			if (!this._utils.isNullOrUndefined(this._onPageChange)) {
				this._onPageChange();
			}
		},

		_onSearchPanelNextPageButtonClickEventHandler: function() {
			if (!this._isNextPageButtonEnabled()) {
				return;
			}

			this.start += this.pageSize;

			if (!this._utils.isNullOrUndefined(this._onPageChange)) {
				this._onPageChange();
			}
		},

		_onExpandButtonClickEventHandler: function() {
			//Destroy old dialog
			if (!this._utils.isNullOrUndefined(this._filterDialog1)) {
				this._filterDialog1.destroy();
			}

			this._filterDialog1 = new FilterDialog1({
				arasObj: this._arasObj,
				filters: this.facets,
				onOkButtonClick: function(changedFilters) {
					if (!this._utils.isNullOrUndefined(this._onFiltersChange)) {
						this._onFiltersChange(changedFilters);
					}
				}.bind(this)
			});
			this._filterDialog1.show();
		},

		_onRemoveFilterOptionClickEventHandler: function(filterIndex, filterOptionName) {
			//Unselect filter option
			var facet = this.facets[filterIndex];
			var option = facet.getOption(filterOptionName);
			if (!this._utils.isNullOrUndefined(option)) {
				option.isSelected = false;
			}

			if (!this._utils.isNullOrUndefined(this._onFiltersChange)) {
				this._onFiltersChange([facet]);
			}
		},

		_onSearchPanelClearSearchCriteriaButtonHandler: function(ev) {
			this.setQueryText('');
		}
	});
});
