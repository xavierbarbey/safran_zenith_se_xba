define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Controls/FilterItem',
	'dojo/text!./../../Views/Templates/FiltersPanel.html'
], function(declare, _WidgetBase, _TemplatedMixin, Utils, FilterItem, filtersPanelTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		_onClearAll: null,

		_onFiltersChange: null,

		_onFiltersReset: null,

		_onFilterReset: null,

		_onFilterLoadOptions: null,

		//Labels
		_filtersLabel: '',
		_clearAllLabel: '',
		_clearLabel: '',
		_moreLabel: '',

		items: [],
		widgets: [],

		itemsWithHiddenCount: ['aes_root_types'],

		templateString: '',

		baseClass: 'resultItem',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._onFiltersChange = args.onFiltersChange;
			this._onFiltersReset = args.onFiltersReset;
			this._onFilterReset = args.onFilterReset;
			this._onFilterLoadOptions = args.onFilterLoadOptions;

			//Initialize labels
			this._filtersLabel = this._utils.getResourceValueByKey('filters.filters');
			this._clearAllLabel = this._utils.getResourceValueByKey('filters.clear_all');
			this._clearLabel = this._utils.getResourceValueByKey('filters.clear');
			this._moreLabel = this._utils.getResourceValueByKey('filters.more');

			this.templateString = filtersPanelTemplate;
		},

		/**
		 * Update widget
		 */
		update: function() {
			var self = this;

			//Destroy old widgets
			this.widgets.forEach(function(widget) {
				widget.destroy();
			});
			this.widgets = [];

			//Create new widgets
			var docFragment = document.createDocumentFragment();
			this.items.forEach(function(item) {
				var widget = new FilterItem({
					arasObj: self._arasObj,
					item: item,
					onFilterChange: self._onFilterChangeEventHandler.bind(self),
					onFilterReset: self._onFilterReset,
					onFilterLoadOptions: self._onFilterLoadOptions,
					clearLabel: self._clearLabel,
					moreLabel: self._moreLabel,
					showCounts: (self.itemsWithHiddenCount.indexOf(item.name) === -1)
				});
				docFragment.appendChild(widget.domNode);

				self.widgets.push(widget);
			});

			this.domNode.appendChild(docFragment);

			if (this.widgets.length > 0) {
				dojo.removeClass(this.filterItemClearAllLink, 'hidden');
			} else if (!dojo.hasClass(this.filterItemClearAllLink, 'hidden')) {
				dojo.addClass(this.filterItemClearAllLink, 'hidden');
			}
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onFilterChangeEventHandler: function(filter) {
			if (!this._utils.isNullOrUndefined(this._onFiltersChange)) {
				this._onFiltersChange([filter]);
			}
		},

		_onFiltersPanelClearAllLinkClickEventHandler: function() {
			for (var i = 0; i < this.widgets.length; i++) {
				var widget = this.widgets[i];

				widget.clearOptions();
			}

			if (!this._utils.isNullOrUndefined(this._onFiltersReset)) {
				this._onFiltersReset();
			}
		}
	});
});
