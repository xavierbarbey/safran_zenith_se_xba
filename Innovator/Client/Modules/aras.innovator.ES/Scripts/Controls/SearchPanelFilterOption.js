define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dojo/_base/lang',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/SearchPanelFilterOption.html'
], function(declare, _WidgetBase, _TemplatedMixin, lang, Utils, filterOptionTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_filterIndex: 0,
		_filterOptionName: '',
		_filterOptionLabel: '',
		_onRemoveFilterOption: null,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			this._filterIndex = args.filterIndex;
			this._filterOptionName = args.filterOptionName;
			this._filterOptionLabel = args.filterOptionLabel;
			this._onRemoveFilterOption = args.onRemoveFilterOption;

			this.templateString = lang.replace(filterOptionTemplate, [this._filterOptionLabel]);

			//declare.safeMixin(this, args);
		},

		postCreate: function() {
			//this.inherited(arguments);
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onRemoveFilterOptionClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onRemoveFilterOption)) {
				this._onRemoveFilterOption(this._filterIndex, this._filterOptionName);
			}
		}
	});
});
