﻿var menuApplet = null;

function initializeMainMenu() {
	sessionStorage.setItem('defaultDState', 'defaultDState');

	document.toolbar = window.toolbarApplet = window.toolbar = window.activeToolbar = new ToolbarWrapper({
		id: 'top_toolbar',
		connectId: 'header'
	});
	initToolbar();

	var cuiContext = {menuId: 'main_menubar'};
	// call getExistMenuBarId is necessary for calculating id of main menubar. This value will be stored in cui and it will be used for menubar switching.
	window.cui.getExistMenuBarIdAsync('MainWindowMainMenu', cuiContext).then(function() {
		window.cui.loadMenuFromCommandBarsAsync('MainWindowMainMenu', cuiContext).then(function(mainMenuXml) {
			var prefix = 'com.aras.innovator.cui_default.mwmm_';
			window.loadMainMenu({
				aras: aras,
				xml: mainMenuXml,
				xmlArgType: 'xml',
				cuiContext: cuiContext},
				function(control) {
					menuApplet = document.menu = control;
					menuApplet.set('default_package_name_prefix', prefix);
					window.cui.initMenuEvents(menuApplet, {prefix: prefix});
					menuFrameReady = true;
				}
			);
		});
	});
	var boundedInit = window.cui.callInitHandlersForMenu.bind(window.cui, {}, 'SelectInToc');
	aras.registerEventHandler('PreferenceValueChanged', window, boundedInit);

	window.addEventListener('unload', function() {
		aras.unregisterEventHandler('PreferenceValueChanged', window, boundedInit);
	});
}

function UpdateMenuAfterCacheReset(itemTypeName) {
	if (window.work && window.tree) {
		window.cui.resetMenuCache();
		var itemInToc = new window.tree.ItemInToc('');
		itemInToc.loadMenuForItemType(itemTypeName, window.work.currItemType);
	}
}

function setControlEnabled(ctrlName, b) {
	if (b === undefined) {
		b = true;
	}
	try {
		var mi = menuApplet.findItem(ctrlName);
		if (mi) {
			mi.setEnabled(b);
		}
	}
	catch (excep) { }
}

function setControlState(ctrlName, b) {
	if (b === undefined) {
		b = true;
	}

	try {
		var tbi = activeToolbar.getItem(ctrlName);
		if (tbi) {
			tbi.setState(b);
		}
	}
	catch (excep) { }

	try {
		var mi = menuApplet.findItem(ctrlName);
		if (mi) {
			mi.setState(b);
		}
	}
	catch (excep) { }
}

function setActiveToolbar(toolbarId) {
	if (!toolbarApplet) {
		return null;
	}
	toolbarApplet.showToolbar(toolbarId);
	return activeToolbar;
}

function showDefaultToolbar() {
	var cuiContext = {toolbarId: 'main_toolbar'};
	var toolbarId = window.cui.getExistToolbarId('MainWindowToolbar', cuiContext);
	return setActiveToolbar(toolbarId);
}

function setAllControlsEnabled(b) {
	if (b === undefined) {
		b = true;
	}

	var mainMenuElements = ['new', 'save', 'open', 'download', 'print', 'export2Excel', 'export2Word', 'purge',
		'delete', 'edit', 'view', 'saveAs', 'lock', 'unlock', 'undo', 'promote', 'revisions', 'copy2clipboard'];
	for (var i = 0; i < mainMenuElements.length; i++) {
		var mi = menuApplet.findItem(mainMenuElements[i]);
		if (mi) {
			mi.setEnabled(b);
		}
	}
}

var menuFrameReady = false;

function initToolbar() {
	window.cui.initToolbarEvents(toolbarApplet);
	toolbarApplet.showLabels(aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_show_labels') == 'true');
	var cuiContext = {toolbarId: 'main_toolbar', 'item_classification': '%all_grouped_by_classification%'};
	window.cui.getExistToolbarIdAsync('MainWindowToolbar', cuiContext).then(function(toolbarId) {
		if (!toolbarApplet.isToolbarExist(toolbarId)) {
			cuiContext.toolbarApplet = toolbarApplet;
			cuiContext.locationName = 'MainWindowToolbar';
			cuiContext.mainSubPrefix = /com.aras.innovator.cui_default.mwt_main_toolbar_/g;
			cuiContext.toolbarId = toolbarId;
			window.cui.loadToolbarFromCommandBars(cuiContext);
		}
		setActiveToolbar(toolbarId);
	});
}
