﻿var Tabs = function(idContainer) {
	this._container = document.getElementById(idContainer);
	this._container.style.display = 'flex';
	this._homeTab = document.getElementById('home-tab');

	var dropdownContainer = document.getElementById('tabs-dropdown');
	window.ArasModules.dropdown(dropdownContainer, {pos: 'bottom-right', closeOnClick: true});
	dropdownContainer.addEventListener('dropdownbeforeopen', function() {
		this.dropdownTabRender();
	}.bind(this));

	this._dropdownBox = dropdownContainer.querySelector('div.aras-dropdown');
	this._homeTab.addEventListener('click', this.selectHomeTab.bind(this));
	this._frameCssClass = 'tabs_content-iframe';

	ArasModules.Tab.call(this, this._container.children[1]);
	this.makeExpandable();
	this.makeDraggable();
	if (!aras.Browser.isEdge()) {
		var HTML_ELEMENT = 2;
		this.tabCustomizator = function() {
			return [Inferno.createVNode(HTML_ELEMENT, 'span', 'aras-tab-expand')];
		};
	}
};
Tabs.prototype = Object.create(ArasModules.Tab.prototype);

Tabs.prototype._hideAll = function() {
	Array.prototype.forEach.call(document.getElementsByClassName(this._frameCssClass), function(iframe) {
		iframe.style.opacity = 0;
		iframe.style['z-index'] = '-1';
	});
};

Tabs.prototype.makeExpandable = function() {
	var expand = function(event) {
		var target = event.target;
		if (target.className === 'aras-tab-expand') {
			event.stopPropagation();
			var tab = target.parentNode;
			this.clickOpenInTearOff(tab.getAttribute('data-id'));
		}
	}.bind(this);
	this._elem.addEventListener('click', expand, true);
};

Tabs.prototype.selectHomeTab = function() {
	this.selectTab('home');
	this._homeTab.classList.add('aras-tabs_active');
};

Tabs.prototype.selectTab = function(id) {
	if (!id) {
		return this.selectHomeTab();
	}
	this._homeTab.classList.remove('aras-tabs_active');

	var parentSelectTab = ArasModules.Tab.prototype.selectTab.bind(this);
	parentSelectTab(id);
	this._hideAll();
	if (id && id !== 'home') {
		document.getElementById(id).style.opacity = 1;
		document.getElementById(id).style['z-index'] = 'auto';
	}
};

Tabs.prototype.removeTab = function(id) {
	var parentRemoveTab = ArasModules.Tab.prototype.removeTab.bind(this);
	var frame = document.getElementById(id);

	var remove = function() {
		parentRemoveTab(id);
		var itemId = frame.id.replace(aras.mainWindowName + '_', '');
		aras.uiUnregWindowEx(itemId);
		frame.src = aras.getScriptsURL() + '../scripts/blank.html';
		frame.parentNode.removeChild(frame);

		if (!this.tabs.length) {
			this.selectHomeTab();
		}
	}.bind(this);
	if (frame.contentWindow.close.toString().indexOf('[native code]') > -1) {
		remove();
		return Promise.resolve();
	}

	return new Promise(function(resolve) {
		frame.contentWindow.close(function(isClosed) {
			if (isClosed) {
				remove();
			}
			resolve(isClosed);
		});
	});

};

Tabs.prototype.clickOpenInTearOff = function(id) {
	if (id) {
		var win = document.getElementById(id).contentWindow;
		if (!win.windowType) {
			var selectItem = win.item;
			var itemId = selectItem.getAttribute('id');
			var itemType = selectItem.getAttribute('type');

			return this.removeTab(id).then(function(isClosed) {
				if (isClosed) {
					var gettedItem = aras.getItemById(itemType, itemId);
					var openInTearOff = true;
					aras.uiShowItemEx(gettedItem, 'tab view', openInTearOff);
				}
			});
		}
		return this.openNonItemWindow(win, id);
	}
};

Tabs.prototype.openNonItemWindow = function(win, id) {
	var itemId = win.itemId;
	var itemTypeName = win.itemTypeName;
	var windowType = win.windowType;
	return this.removeTab(id).then(function(isClosed) {
		if (isClosed) {
			if (windowType === 'whereUsed' || windowType === 'structureBrowser') {
				window.Dependencies.View(itemTypeName, itemId, windowType === 'whereUsed', aras, true);
			}
		}
	});
};

Tabs.prototype.setTitleTabWithFrame = function(frameWindow) {
	var itemTypeName;
	var item;

	if (frameWindow.item && frameWindow.itemTypeName) {
		item = frameWindow.item;
		itemTypeName = frameWindow.itemTypeName;
	} else {
		var win = window[frameWindow.paramObjectName];
		itemTypeName = window[frameWindow.paramObjectName].itemTypeName;
		item = win.item;
	}

	var winName = frameWindow.paramObjectName.replace('_params', '');
	var itemTypeNd = aras.getItemTypeDictionary(itemTypeName).node;
	var itemTypeImgSrc = aras.getItemProperty(itemTypeNd, 'open_icon') || '../images/DefaultItemType.svg';
	var keyedName = aras.getKeyedNameEx(item);
	var props = {
		label: keyedName,
		image: itemTypeImgSrc
	};
	this.updateTitleTab(winName, props);
};

Tabs.prototype.updateTitleTab = function(id, props) {
	this.setTabContent(id, props).then(function() {
		var tab = this._container.querySelector('li[data-id="' + id + '"]');
		if (tab) {
			this.scrollIntoView(tab);
		}
	}.bind(this));

};

Tabs.prototype.open = function(src, winName, isUnfocused) {
	var topWin = window.main || window;
	var iframe = topWin.document.createElement('IFRAME');
	var isCh = aras.Browser.isCh();
	if (isCh) {
		topWin.document.body.appendChild(iframe);
	}
	this.addTab(winName, {closable: true});
	var tabsRect = this._container.getBoundingClientRect();
	var topPosition = tabsRect.top + this._container.offsetHeight + 1;
	iframe.style.top = topPosition + 'px';
	iframe.style.height = 'calc(100% - ' + (topPosition + 1) + 'px)';
	iframe.id = winName;
	iframe.className = this._frameCssClass;
	iframe.src = src;
	if (!isCh) {
		topWin.document.body.appendChild(iframe);
	}
	if (isUnfocused) {
		iframe.style.opacity = 0;
		iframe.style['z-index'] = '-1';
	} else {
		this.selectTab(winName);
	}
	var win = iframe.contentWindow;
	win.opener = topWin;
	win.name = iframe.name = winName;
	return win;
};

Tabs.prototype.dropdownTabRender = function() {
	var HTML_ELEMENT = 2;
	var select = function(event) {
		var target = event.target;
		var listTab = target.closest('li');

		if (listTab) {
			this.selectTab(listTab.getAttribute('list-data-id'));
		}
	}.bind(this);

	var items = this.tabs.map(function(tab) {
		var data = this.data.get(tab);
		var props = {
			'list-data-id': tab,
			class: this.selectedTab === tab ? 'selected' : '',
		};
		var point = Inferno.createVNode(HTML_ELEMENT, 'span', 'condition-icon aras-icon-radio');
		var icon = this._getImage(data.image);

		return Inferno.createVNode(HTML_ELEMENT, 'li', null, [point, icon, data.label], props);
	}.bind(this));
	var list = Inferno.createVNode(HTML_ELEMENT, 'ul', 'aras-list', items, {'onmousedown': select});
	Inferno.render(list, this._dropdownBox);
};

Tabs.prototype.updateTabInformation = function(currentID, newID) {
	const index = this.tabs.indexOf(currentID);
	if (index !== -1) {
		this.tabs[index] = newID;
	}

	const dataObj = this.data.get(currentID);
	this.data.delete(currentID);
	this.data.set(newID, Object.assign({}, dataObj));

	this.selectedTab = newID;

	const frame = document.getElementById(currentID);
	frame.id = frame.name = newID;
	this.render();
};
