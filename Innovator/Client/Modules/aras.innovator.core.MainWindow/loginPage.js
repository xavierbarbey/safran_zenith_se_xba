﻿var loginPage = {
	initialize: function() {
		this.config = clientConfig;

		var solution = new Solution('core', new Solution('core').getBaseURL() + this.config.slashAndSalt);
		this.rm = new ResourceManager(solution, 'ui_resources.xml', this.config.httpAcceptLanguage);

		this._initPageSettings();
		this._initUsernameField();
		this._initPasswordField();
		this._initDatabaseField();
		this._initServerField();
		this._initUpdateInfo();
		this._initBookmark();
		this._initLabels();

		setTimeout(this._setInitialFocus.bind(this), 0); //setTimeout for fix IE11 side effect
		this._afterCompleteLoad();
	},

	_initPageSettings: function() {
		var self = this;

		parent.window.focus();
		parent.document.title = this.config.productName + ' ' + this.rm.getString('login.login_page_title_sfx');

		if (document.msCapsLockWarningOff === false) {
			document.msCapsLockWarningOff = true;
		}

		document.oncontextmenu = function() {
			return false;
		};

		document.onkeypress = function(e) {
			if (!e) {
				e = window.event;
			}

			if (e.keyCode == 13) {
				var passwordField = document.getElementById('password');
				var usernameField = document.getElementById('username');
				var username = self._getUsername();
				var password = self._getPassword();
				var element = e.srcElement || e.target;

				if (passwordField && element.id === 'username' && username && !password) {
					passwordField.focus();
				}

				if (usernameField && element.id === 'password' && !username && password) {
					usernameField.focus();
				}

				if (password !== '' && username !== '') {
					// fix side effect from promis polyfill in IE9-11
					if (element.nodeName.toLowerCase() !== 'body') {
						element.blur();
					}

					self.login();
				}
			}
		};
	},

	_initUsernameField: function() {
		var username = document.getElementById('username');
		var usernameValue;

		username.setAttribute('placeholder', this.rm.getString('login.login_name_html'));
		if (this.config.loginIgnoreCookies !== 'true') {
			usernameValue = localStorage.getItem('innovatorUser');
			username.onchange = function() {
				localStorage.setItem('innovatorUser', this.value);
			};
		}
		username.value = usernameValue || this.config.loginUsername;
		if (this.config.loginNoPromptUsername === 'true') {
			username.disabled = true;
		}
	},

	_initPasswordField: function() {
		var self = this;
		var password = document.getElementById('password');
		var passwordWrap = password.parentNode;

		if (this.config.loginHidePassword === 'true') {
			passwordWrap.parentNode.removeChild(passwordWrap);
		} else {
			password.setAttribute('placeholder', this.rm.getString('login.pwd_html'));

			password.addEventListener('keypress', function(e) {
				var ctrlKey = e.ctrlKey;
				var	shiftKey = e.shiftKey;
				var	keyCode = e.keyCode || e.which;

				if (!ctrlKey && (((keyCode >= 65 && keyCode <= 90) && !shiftKey) || ((keyCode >= 97 && keyCode <= 122) && shiftKey))) {
					self._showTooltip(true);
				} else {
					if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)) {
						self._showTooltip(false);
					}
				}
			});

			password.addEventListener('keydown', function(e) {
				var kc = e.keyCode || e.which;
				if (20 == kc) {
					self._showTooltip(false);
				}
			});

			password.addEventListener('blur', function() {
				self._showTooltip(false);
			});
		}
	},

	_showTooltip: function(show) {
		var tooltip = document.getElementById('password').parentNode;

		if (tooltip) {
			tooltip.setAttribute('data-tooltip-show', show);
		}
	},

	_initDatabaseField: function() {
		var resXMLDOM = ArasModules.xml.parseFile(this.rm.getSolution().getServerBaseURL() + 'DBList.aspx');
		var dbs = ArasModules.xml.selectNodes(resXMLDOM, '/DBList/DB');
		var databaseSelect = document.getElementById('database_select');
		var databaseName = this.config.loginDatabase;
		var databaseSelectWrap = databaseSelect.parentNode;

		if (this.config.loginHideDatabase !== 'true') {
			dbs.forEach(function(db) {
				databaseSelect.add(new Option(db.getAttribute('id')));
			});

			if (this.config.loginIgnoreCookies !== 'true') {
				databaseName = localStorage.getItem('databaseName');
				databaseSelect.onchange = function() {
					localStorage.setItem('databaseName', this.options[this.selectedIndex].text);
				};
			}

			for (var i = 0; i < databaseSelect.length; i++) {
				if (databaseSelect.options[i].value === databaseName) {
					databaseSelect.selectedIndex = i;
				}
			}
		} else {
			databaseSelectWrap.parentNode.removeChild(databaseSelectWrap);
		}
	},

	_initServerField: function() {
		var serverSelect = document.getElementById('server_select');
		var	selectWrap = serverSelect.parentNode;

		if (this.config.loginHideServer !== 'true' && this.config.serversOptionsHtml) {
			serverSelect.innerHTML = this.config.serversOptionsHtml;
		} else {
			selectWrap.parentNode.removeChild(selectWrap);
		}
	},

	_initBookmark: function() {
		var self = this;
		var bookmark = document.getElementById('login.bookmark_this_page_txt');
		var utility = new ClientUrlsUtility(this.rm.getSolution().getBaseURL());

		bookmark.title = this.config.productName + ' Login';
		bookmark.href = utility.getBaseUrlWithoutSalt();

		if (window.external && ('AddFavorite' in window.external)) {
			bookmark.onclick = function() {
				window.external.AddFavorite(this.href, self.config.productName);
				return false;
			};
		} else if (!window.sidebar) {
			bookmark.style.display = 'none';
		}
	},

	_initUpdateInfo: function() {
		var updateInfo = document.getElementById('login.default_link_html');

		updateInfo.href = this.config.updateInfoUrl;
		updateInfo.innerHTML = this.config.updateInfoText;
		updateInfo.onclick = function() {
			window.open(this.href, '_blank');
			return false;
		};

		if (this.config.isNoUpdateInfo) {
			updateInfo.removeAttribute('id');
		}
	},

	_initLabels: function() {
		var self = this;
		var fieldsForUpdate = [
			'login.version_html',
			'login.build_html',
			'login.bookmark_this_page_txt',
			'login.default_link_html',
			'login.login_btn_label',
			'login.cancel_btn_label'
		];

		fieldsForUpdate.forEach(function(fieldId) {
			var formElement = document.getElementById(fieldId);

			if (formElement) {
				formElement.innerHTML = self.rm.getString(fieldId);
			}
		});
	},

	_setInitialFocus: function() {
		var username = document.getElementById('username');
		var password = document.getElementById('password');

		if (this._getUsername() === '') {
			username.focus();
		} else {
			if (password) {
				password.focus();
			}
		}
	},

	_afterCompleteLoad: function() {
		if (this.config.loginOnloadSubmit === 'true') {
			if (!parent.arasObjectIsReady) {
				setTimeout(this._afterCompleteLoad.bind(this), 50);
				return;
			}
			var iwaits = this.config.bypassLogonWait || '500';
			setTimeout(this.login.bind(this), iwaits);
		}
	},

	_getUsername: function() {
		var username = document.getElementById('username');
		return username.value;
	},

	_getPassword: function() {
		var password = document.getElementById('password');

		return password ? password.value : this.config.loginPassword;
	},

	_getDatabase: function() {
		var database = document.getElementById('database_select');

		return database ? database.options[database.selectedIndex].text : this.config.loginDatabase;
	},

	_getServerBaseUrl: function() {
		var server = document.getElementById('server_select');

		return server ? server.options[server.selectedIndex].getAttribute('BaseURL') : this.config.serverBaseUrl;
	},

	_getServerExt: function() {
		var server = document.getElementById('server_select');

		return server ? server.options[server.selectedIndex].getAttribute('EXT') : this.config.serverExt;
	},

	_getDoHashFlag: function() {
		return this.config.hashPasswordFlag !== 'false';
	},

	close: function() {
		parent.opener = parent;
		parent.open('blank.html', '_top');
		parent.close();
	},

	login: function() {
		var alert = parent.ArasModules.Dialog.alert;
		var username = this._getUsername().trim();
		var password = this._getPassword();
		var database = this._getDatabase();
		var serverBaseUrl = this._getServerBaseUrl();
		var serverExt = this._getServerExt();
		var doHashFlag = this._getDoHashFlag();
		var stat = 'ok';
		const loginHidePassword = this.config.loginHidePassword;
		const useLogonHookPasswordGrant = this.config.useLogonHookPasswordGrant;
		const oauthClientId = this.config.oauthClientId;
		const loginButton = document.getElementById('login.login_btn_label');
		loginButton.disabled = true;

		if (loginHidePassword === 'true' && useLogonHookPasswordGrant === 'true') {
			// LogonHook returns empty password hash when supports token proxying
			// by IOMLogin.aspx?return=token and to avoid missing password
			// warning we set password to null.
			// This also will help to distinguish case when use password or
			// logonHookPassword grant inside user.login because clientConfig
			// is unaccessible inside user.login and we decided to do not
			// pass extra options in aras.login and in user.login calls.
			// TODO: The best solution is to call OAuthClient.login() rights after init
			// but it requires too much changes and better to do it later.
			password = null;
			doHashFlag = false; // Avoid calculate hashing for null password
		}

		if (username === '') {
			stat = this.rm.getString('login.missing_login_name_wrn');
		} else if (password === '') {
			stat = this.rm.getString('login.missing_pwd_wrn');
		} else if (database === '') {
			stat = this.rm.getString('login.missing_db_wrn');
		}

		if (stat === 'ok' && parent.aras) {
			const aras = parent.aras;
			aras.setServer(serverBaseUrl, serverExt);
			aras.OAuthClient.init(oauthClientId, aras.getServerBaseURL())
				.then(function() {
					return aras.login(username, password, database, doHashFlag, serverBaseUrl, serverExt);
				})
				.then(function() {
					parent.onSuccessfulLogin();
				})
				.catch(function(errorResponse) {
					if (errorResponse && errorResponse.error) {
						return alert(errorResponse.errorDescription || errorResponse.error);
					}

					return Promise.reject(errorResponse);
				})
				.catch(function(err) {
					if (err && err.type === 'soap') {
						return alert('', err);
					}

					return alert(err);
				})
				.then(function() {
					var passwordField = document.getElementById('password');
					loginButton.disabled = false;
					if (passwordField) {
						passwordField.value = '';
						passwordField.focus();
					}
				});
		} else {
			alert(stat);
			loginButton.disabled = false;
		}
	}

};
