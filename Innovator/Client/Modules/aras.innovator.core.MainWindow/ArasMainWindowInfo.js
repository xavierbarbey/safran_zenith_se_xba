﻿var ArasMainWindowInfo = (function() {
	function ArasMainWindowInfo(aras) {
		this.aras = aras;
	}
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getIdentityListResult', {
		get: function() {
			return this.getSoapResult('getIdentityList', '/*/*/*/GetIdentityListResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getUserResult', {
		get: function() {
			return this.getSoapResult('getUser', '/*/*/*/GetUserResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getLanguageResult', {
		get: function() {
			return this.getSoapResult('getLanguage', '/*/*/*/GetLanguageResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getMainTreeItemsResult', {
		get: function() {
			return this.getSoapResult('getMainTreeItems', '/*/*/*/GetMainTreeItemsResult');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getActionResult', {
		get: function() {
			return this.getSoapResult('getAction', '/*/*/*/GetActionResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getReportResult', {
		get: function() {
			return this.getSoapResult('getReport', '/*/*/*/GetReportResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'isFeatureTreeExpiredResult', {
		get: function() {
			return this.provider
				.getResultNode('isFeatureTreeExpired')
				.selectSingleNode('/*/*/*/IsFeatureTreeExpiredResult/Result')
				.text;
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'getCheckUpdateInfoResult', {
		get: function() {
			return this.getSoapResult('getCheckUpdateInfo', '/*/*/*/GetCheckUpdateInfoResult/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'IsSSVCLicenseOk', {
		get: function() {
			return this.provider
				.getResultNode('IsSSVCLicenseOk')
				.selectSingleNode('/*/*/*/IsSSVCLicenseOk/Result')
				.text === 'True';
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'MessageCheckInterval', {
		get: function() {
			return this.provider
				.getResultNode('MessageCheckInterval')
				.selectSingleNode('/*/*/*/MessageCheckInterval/Result')
				.text;
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'ClientDisplayMode', {
		get: function() {
			return this.provider
				.getResultNode('ClientDisplayMode')
				.selectSingleNode('/*/*/*/ClientDisplayMode/Result')
				.text;
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'MetadataServiceSalt', {
		get: function() {
			return this.provider
				.getResultNode('MetadataServiceSalt')
				.selectSingleNode('/*/*/*/MetadataServiceSalt/Result')
				.text;
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'SSVC_Preferences', {
		get: function() {
			return this.getSoapResult('SSVC_Preferences', '/*/*/*/SSVC_Preferences/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'Core_GlobalLayout', {
		get: function() {
			return this.getSoapResult('CoreGlobalLayoutPreference', '/*/*/*/Core_GlobalLayout/*');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'GetAllMetadataDates', {
		get: function() {
			var soap = this.getSoapResult('GetMetadataInfoDates', '/*/*/*/GetMetadataInfoDates');
			return soap.results.selectSingleNode('GetMetadataInfoDates');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'SearchCountMode', {
		get: function() {
			return this.provider
				.getResultNode('GetSearchCountMode')
				.selectSingleNode('/*/*/*/SearchCountMode/Result')
				.text;
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'Core_SearchCountModeException', {
		get: function() {
			return this.provider
				.getResultNode('GetSearchCountModeExceptions')
				.selectSingleNode('/*/*/*/Core_SearchCountModeException/Result')
				.text
				.split(',');
		},
		enumerable: true,
		configurable: true
	});
	Object.defineProperty(ArasMainWindowInfo.prototype, 'ES_Settings', {
		get: function() {
			return this.getSoapResult('GetEsSettings', '/*/*/*/ES_Settings/*');
		},
		enumerable: true,
		configurable: true
	});
	ArasMainWindowInfo.prototype.setProvider = function(provider) {
		this.provider = provider;
	};
	ArasMainWindowInfo.prototype.getSoapResult = function(queryType, xpath) {
		if (this.provider === null) {
			throw new Error('Invalid operation. Set provider before.');
		}
		var resultNode = this.provider.getResultNode(queryType);
		var node = resultNode.selectSingleNode(xpath);
		return new SOAPResults(this.aras, node.xml);
	};
	return ArasMainWindowInfo;
}());
