﻿(function() {
	var todayFieldSetId = 'today';
	var todayCorpFieldSetId = 'today_corp';

	var updtBnnrsTmout;

	window.updateBanners = function() {
		updateBanner(todayFieldSetId);
		if (document.corporateToLocalOffset) {
			updateBanner(todayCorpFieldSetId);
		}
		var secs = (new Date()).getSeconds();
		updtBnnrsTmout = setTimeout(updateBanners, (60 - secs) * 1000 + 1);
	};

	function updateBanner(bannerId) {
		var currentDate = new Date();
		if (bannerId == todayCorpFieldSetId) {
			currentDate = new Date(currentDate.valueOf() + document.corporateToLocalOffset * 60000);
		}

		var date = ArasModules.intl.date.format(currentDate, 'longDate');
		var time = ArasModules.intl.date.format(currentDate, 'shortTime');

		document.getElementById(bannerId + '_date').innerHTML = date;
		document.getElementById(bannerId + '_time').innerHTML = time.replace(new RegExp('\\b(am|pm|AM|PM)$'), '<sup>$&</sup>');
	}

	window.showCorporateTime = function() {
		var todayLableCrp = document.getElementById('today_lbl_corp');
		if (todayLableCrp) {
			todayLableCrp.style.visibility = document.corporateToLocalOffset ? 'visible' : 'hidden';
		}

		var todayBlockCrp = document.getElementById('today_corp_block');
		if (todayBlockCrp) {
			todayBlockCrp.style.visibility = document.corporateToLocalOffset ? 'visible' : 'hidden';
		}

		var todayLable = document.getElementById('today_lbl');
		if (todayLable) {
			todayLable.style.visibility = document.corporateToLocalOffset ? 'visible' : 'hidden';
		}
	};

	window.stopTimeBannersUpdate = function() {
		if (updtBnnrsTmout !== undefined) {
			clearTimeout(updtBnnrsTmout);
		}
	};
})();
