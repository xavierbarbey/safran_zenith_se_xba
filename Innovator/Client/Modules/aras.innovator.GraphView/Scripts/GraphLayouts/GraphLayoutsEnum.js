﻿define('GraphView/Scripts/GraphLayouts/GraphLayoutsEnum', [], {
	LayoutType: {
		Force: 10,
		HorizontalTree: 20,
		VerticalTree: 30
	},

	getLayoutItemsList: function() {
		return [
			{id: 'force_layout', label: 'Force Layout'},
			{id: 'horizontal_tree_layout', label: 'Horizontal Tree Layout'},
			{id: 'vertical_tree_layout', label: 'Vertical Tree Layout'}
		];
	},

	getLayoutTypeById: function(layoutId) {
		let selectedLayout;

		switch (layoutId) {
			case 'vertical_tree_layout':
				selectedLayout = this.LayoutType.VerticalTree;
				break;
			case 'horizontal_tree_layout':
				selectedLayout = this.LayoutType.HorizontalTree;
				break;
			default:
				selectedLayout = this.LayoutType.Force;
				break;
		}

		return selectedLayout;
	},

	getLayoutIdByType: function(layoutType) {
		let selectedId;

		switch (layoutType) {
			case this.LayoutType.VerticalTree:
				selectedId = 'vertical_tree_layout';
				break;
			case this.LayoutType.HorizontalTree:
				selectedId = 'horizontal_tree_layout';
				break;
			default:
				selectedId = 'force_layout';
				break;
		}

		return selectedId;
	}
});
