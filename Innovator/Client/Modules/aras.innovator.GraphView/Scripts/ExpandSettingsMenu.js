﻿define([
	'dojo/_base/declare'
], function(declare) {
	return declare('GraphNavigation.ExpandSettingsMenu', null, {
		containerNode: null,
		domNode: null,
		relationshipContainerNode: null,
		propertyContainerNode: null,
		selectAllCheckbox: null,
		isActive: null,
		nodeData: null,
		_properties: null,
		_relationships: null,
		_allConnectorsHash: null,
		domNodeTemplate: '' +
		'<div class="selectAllRowContainer">' +
			'<label class="aras-form-boolean select-all">' +
				'<input type="checkbox" checked>' +
				'<span><span></span></span>' +
			'</label>' +
			'<label class="selectAllLabel">Select All</label>' +
		'</div>' +
		'<div class="scrollContainer">' +
			'<div class="relationshipsContainer">' +
				'<label class="label">Relationships</label>' +
				'<div class="relationshipsRowContainer"></div>' +
			'</div>' +
			'<div class="propertiesContainer">' +
				'<label class="label">Properties</label>' +
				'<div class="propertiesRowContainer"></div>' +
			'</div>' +
		'</div>' +
		'<div class="buttonContainer">' +
			'<div class="popupButton">Apply</div>' +
			'<div class="popupButton cancelButton">Cancel</div>' +
		'</div>',
		htmlRowTemplate: '' +
		'<div class="relatedItemRowContainer">' +
			'<label class="aras-form-boolean">' +
				'<input id="{id}" type="checkbox" {check_state}>' +
				'<span><span></span></span>' +
			'</label>' +
			'<img class="relatedItemImage" src="{image}">' +
			'<label>{name}</label>' +
		'</div>',
		emptyRelationshipsHtml: '<div class="no-result">Relationships not found</div>',
		emptyPropertiesHtml: '<div class="no-result">Properties not found</div>',

		constructor: function(initialParameters) {
			initialParameters = initialParameters || {};

			this.containerNode = initialParameters.containerNode || document.body;
			this.domNode = this.containerNode.ownerDocument.createElement('div');
			this.domNode.setAttribute('class', 'expandCollapsePopup');
			this.domNode.innerHTML = this.domNodeTemplate;

			this.relationshipContainerNode = this.domNode.querySelector('.relationshipsRowContainer');
			this.propertyContainerNode = this.domNode.querySelector('.propertiesRowContainer');
			this.domNode.querySelector('.popupButton').addEventListener('click', this._onApply.bind(this));
			this.domNode.querySelector('.cancelButton').addEventListener('click', this._onCancel.bind(this));
			this.selectAllCheckbox = this.domNode.querySelector('input');
			this.selectAllCheckbox.addEventListener('click', this._onToggleSelectAll.bind(this));

			this.containerNode.appendChild(this.domNode);
		},

		showPopupWindow: function(nodeData, positionX, positionY) {
			this.nodeData = nodeData;
			this._relationships = [];
			this._properties = [];
			this._allConnectorsHash = {};

			Array.prototype.forEach.call(nodeData.activeOutgoingConnectors.concat(nodeData.hiddenOutgoingConnectors), function(connectorKey) {
				if (!this._allConnectorsHash[connectorKey.pname || connectorKey.rname]) {
					this._allConnectorsHash[connectorKey.pname || connectorKey.rname] = [];

					if (connectorKey.pname) {
						this._properties.push({
							name: connectorKey.pname,
							isChecked: nodeData.hiddenOutgoingConnectors.indexOf(connectorKey) === -1
						});
					}
					if (connectorKey.rname) {
						this._relationships.push({
							name: connectorKey.rname,
							isChecked: nodeData.hiddenOutgoingConnectors.indexOf(connectorKey) === -1
						});
					}
				}

				this._allConnectorsHash[connectorKey.pname || connectorKey.rname].push(connectorKey);
			}.bind(this));
			this._renderPopupWindowContent();

			this.domNode.classList.add('active');
			if (positionX + this.domNode.clientWidth - this.containerNode.scrollLeft > this.containerNode.clientWidth) {
				positionX -= this.domNode.clientWidth;
			}
			if (positionY + this.domNode.clientHeight - this.containerNode.scrollTop > this.containerNode.clientHeight) {
				positionY -= this.domNode.scrollHeight;
			}
			this.domNode.style.left = positionX + 'px';
			this.domNode.style.top = positionY + 'px';
			this.isActive = true;
		},

		hidePopupWindow: function() {
			if (this.isActive) {
				this.nodeData = null;
				this._properties = null;
				this._relationships = null;
				this._allConnectorsHash = null;
				this.domNode.classList.remove('active');

				this.isActive = false;
			}
		},

		onApplyButtonClick: function() {
		},

		onCancelButtonClick: function() {
		},

		_renderPopupWindowContent: function() {
			let relationshipsContent = '';
			let propertiesContent = '';
			if (this._relationships.length) {
				this._relationships.sort();
				Array.prototype.forEach.call(this._relationships, function(relationship) {
					relationshipsContent += this.htmlRowTemplate
						.replace('{id}', relationship.name)
						.replace('{check_state}', relationship.isChecked ? 'checked' : '')
						.replace('{image}', '../images/RelationshipType.svg')
						.replace('{name}', this._getNormalizedText(relationship.name));
				}.bind(this));
			} else {
				relationshipsContent = this.emptyRelationshipsHtml;
			}

			if (this._properties.length) {
				Array.prototype.sort.call(this._properties, function(a, b) {
					return a.name.localeCompare(b.name);
				});
				Array.prototype.forEach.call(this._properties, function(property) {
					propertiesContent += this.htmlRowTemplate
						.replace('{id}', property.name)
						.replace('{check_state}', property.isChecked ? 'checked' : '')
						.replace('{image}', '../images/Properties.svg')
						.replace('{name}', this._getNormalizedText(property.name));
				}.bind(this));
			} else {
				propertiesContent = this.emptyPropertiesHtml;
			}

			this.relationshipContainerNode.innerHTML = relationshipsContent;
			this.propertyContainerNode.innerHTML = propertiesContent;

			const allSelectionItems = this._getSelectItems();
			Array.prototype.forEach.call(allSelectionItems, function(selectItem) {
				selectItem.addEventListener('change', this._updateSelectAllButton.bind(this));
			}.bind(this));
			this._updateSelectAllButton();
		},

		_onToggleSelectAll: function() {
			const allSelectionItems = this._getSelectItems();
			Array.prototype.forEach.call(allSelectionItems, function(selectItem) {
				selectItem.checked = this.selectAllCheckbox.checked;
			}.bind(this));

			if (!this.selectAllCheckbox.checked) {
				this.selectAllCheckbox.parentNode.classList.remove('mixed');
			}
		},

		_updateSelectAllButton: function() {
			const allSelectionItems = this._getSelectItems();
			const selectedItemsCount = this._getCheckedItemsCount(allSelectionItems);

			if (selectedItemsCount === allSelectionItems.length) {
				this.selectAllCheckbox.checked = true;
				this.selectAllCheckbox.parentNode.classList.remove('mixed');
			} else if (selectedItemsCount < allSelectionItems.length && selectedItemsCount) {
				this.selectAllCheckbox.checked = false;
				this.selectAllCheckbox.parentNode.classList.add('mixed');
			} else {
				this.selectAllCheckbox.checked = false;
				this.selectAllCheckbox.parentNode.classList.remove('mixed');
			}
		},

		_onApply: function() {
			this.nodeData.activeOutgoingConnectors = [];
			this.nodeData.hiddenOutgoingConnectors = [];

			Array.prototype.forEach.call(this._getSelectItems(), function(selectItem) {
				let targetOutgoingConnectors = (selectItem.checked ? this.nodeData.activeOutgoingConnectors : this.nodeData.hiddenOutgoingConnectors);
				Array.prototype.forEach.call(this._allConnectorsHash[selectItem.getAttribute('id')], function(selectedConnector) {
					targetOutgoingConnectors.push(selectedConnector);
				});
			}.bind(this));

			this.onApplyButtonClick(this.nodeData);
			this.hidePopupWindow();
		},

		_onCancel: function() {
			this.onCancelButtonClick();
			this.hidePopupWindow();
		},

		_getSelectItems: function() {
			return this.domNode.querySelectorAll('.relatedItemRowContainer>label>input');
		},

		_getCheckedItemsCount: function(selectItems) {
			if (selectItems) {
				return Array.prototype.filter.call(selectItems, function(selectedItem) {
					return selectedItem.checked;
				}).length;
			}
		},

		_getNormalizedText: function(text) {
			if (text) {
				const requiredSpace = this._getTextLength(text);
				const availableSpace = 120;
				const maxSpace = Math.floor(availableSpace / (requiredSpace / text.length));

				return text ? (text.length > maxSpace ? text.substring(0, maxSpace) + '\u2026' : text) : '';
			}
		},

		_getTextLength: function(text) {
			const canvas = this._getTextLength.canvas || (this._getTextLength.canvas = document.createElement('canvas'));
			const context = canvas.getContext('2d');
			context.font = '12px sans-serif';
			return context.measureText(text).width;
		}
	});
});
