﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- © Copyright by Aras Corporation, 2004-2013. -->
<!-- #INCLUDE FILE="scripts/include/utils.aspx" -->
<%
	Dim qs As String = Request.ServerVariables("QUERY_STRING")
	If Not String.IsNullOrEmpty(qs) Then
		qs = "?" + qs
	Else
		qs = ""
	End If

	Dim clConfig As ClientConfig = ClientConfig.GetServerConfig()
	Dim urlContainsSalt As Boolean = (-1 < Request.RawUrl.LastIndexOf("Client/" + ClientHttpApplication.FullSaltForCachedContent, StringComparison.OrdinalIgnoreCase))
	If clConfig.UseCachingModule AndAlso Not urlContainsSalt Then
		Response.Cache.SetCacheability(HttpCacheability.NoCache)
		Response.Redirect(ClientHttpApplication.FullSaltForCachedContent + "/default.aspx" + qs, True)
	End If

	Dim login_window_width As String = Client_Config("login_window_width")
	Dim login_window_height As String = Client_Config("login_window_height")

	If login_window_width = "" Then login_window_width = "413"
	If login_window_height = "" Then login_window_height = "337"
%>
<html>
	<head>
		<title></title>
			<style type="text/css">
			html, body
			{
				margin: 0px;
				height: 100%;
			}
			table
			{
				position: absolute;
				top: 0px;
				left: 0px;
				width: 100%;
				height: 100%;
				text-align: center;
			}
		</style>
		<script type="text/javascript">
		window.__staticVariablesStorage = true;//this is a flag for StaticVariablesStorage to initialize static variables storage here in this window.
		</script>
		<script type="text/javascript" src="javascript/include.aspx?classes=polyfillsBundle,BrowserInfo,ResourceManager,Solution,StaticVariablesStorage,XmlDocument"></script>
		<script type="text/javascript" src="jsBundles/core.js"></script>
		<script type="text/javascript">
			var browserInfo;
			try {
				browserInfo = new BrowserInfo();
				if (!browserInfo.isKnown()) {
					//we even cannot recognize this browser
					location = "SupportedBrowsers.html";
				}
			}
			catch (excep) {
				//Everything is so bad that we even cannot instantiate BrowserInfo and check if browser is supported or not.
				location = "SupportedBrowsers.html";
			}
		</script>
		<script type="text/javascript">
			try {
				rm = new ResourceManager(new Solution("core"), "ui_resources.xml", '<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(Request.ServerVariables.Item("HTTP_ACCEPT_LANGUAGE"),false)%>');
			}
			catch (e) {
				//it's theoretically possible that browser is known but ResourceManager or Solution is not implemented for that.
			}

			onload = function onload_handler() {
				var successCheckBrowserCallback = function () {
					checkCookiesAvailability(loadClient);
				};

				checkBrowser(successCheckBrowserCallback);
			};

			function checkCookiesAvailability(successCallback) {
				<%
				If Client_config("skip_cookie_check") = "true" Then
					trace.write("cookie_check","skipping")
				%>
				successCallback();
				<%
				Else
				%>
					// Check cookies enabled
					// First in browser through javascript
					var clientCookiesEnabled = (navigator.cookieEnabled) &&  (typeof navigator.cookieEnabled != "undefined");
					if (!clientCookiesEnabled) {
						showSystemRequirements("login.cookies_are_disabled_msg_html");
						return;
					}

					document.cookie = "test_client_cookies=True; path=/";
					if(document.cookie.indexOf("test_client_cookies") == -1) {
						showSystemRequirements("login.cookies_are_disabled_msg_html");
						return;
					}

					//Then through the special web page
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function(xhr) {
						if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
							var serverCookieEnabled = window.document.cookie.indexOf("test_server_cookies") != -1;
							if (xmlhttp.responseText == "True" && serverCookieEnabled) {
								var cookie_date = new Date ( );
								cookie_date.setTime ( cookie_date.getTime() - 24 * 3600 * 1000 );
								window.document.cookie = "test_server_cookies=; path=/; expires=" + cookie_date.toGMTString();
								successCallback();
							} else {
								showSystemRequirements("login.cookies_are_disabled_msg_html");
								return;
							}
						}
					}

					xmlhttp.open("GET", './scripts/checkCookiesEnabled.aspx', true);
					xmlhttp.send();
				<% End If %>
			}

			function loadClient() {
				// edge incorrectly returns the size of maximized window state
				var verticalBorders = Math.min(window.outerHeight, screen.availHeight) - window.innerHeight;
				var horizontalBorders = Math.min(window.outerWidth, screen.availWidth) - window.innerWidth;
				var width = <%=login_window_width%> + horizontalBorders;
				var height = <%=login_window_height%> + verticalBorders;
				var topVar = (screen.availHeight - height) / 2;
				var left = (screen.availWidth - width) / 2;
				var isInnovatorInIFrame = window.parent !== window;
				var innovatorUrl = 'scripts/Innovator.aspx<%=System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(qs,false)%>';

				if (isInnovatorInIFrame || browserInfo.isEdge()) {
					window.location.replace(innovatorUrl);
				} else {
					var features = "scrollbars=no,resizable=yes,status=no,width=" + width + ",height=" + height;
					features += ",left=" + left + ",top=" + topVar;

					//restore code from a70332fb2de1f39bc648fb40313348399c2a2531. The commit is removing window.open for main page
					var winName = 'Innovator' + location.hostname.replace(/\.|:|-|_/g, '');
					var d = new Date();
					winName += d.getHours() + 'h' + d.getMinutes() + 'm' + d.getSeconds() + 's';
					win = window.open(innovatorUrl, winName, features);
					try
					{
						win.focus();
						// the "open" method is workaround for IE
						// proof: https://jeffclayton.wordpress.com/2015/02/13/javascript-window-close-method-that-still-works/
						if ('ActiveXObject' in window) {
							window.open('scripts/blank.html', '_top');
							window.close();
							win.opener = null;
						}
					}
					catch (excep) {}
				}

				document.getElementById("warning_footer").style.display = "none";
				document.getElementById("SystemRequirements").style.display = "none";
			}

			function showSystemRequirementsArea() {
				document.getElementById("SystemRequirements").style.display = "";
				document.getElementById("warning_image").src = "imagesLegacy/32x32_warning.gif";
			}

			function showSystemRequirements(description, params, title)
			{
				showSystemRequirementsArea();
				document.getElementById("error_title").innerHTML = rm.getString(title || "system_requirements.failed_initialize_innovator");
				document.getElementById("error_description").innerHTML = params ? rm.getFormatedString(description, params) : rm.getString(description);
			}

			function isInLocalStorage(key) {
				var baseUrl = window.location.href.replace(/\Client\/.*/i, "\Client").toLowerCase();
				var urlsDom = new XmlDocument();
				var localStorageItem = window.localStorage.getItem("ArasAppUrlsKeys");
				if (localStorageItem) {
					urlsDom.loadXML(localStorageItem);
					var urlNode = urlsDom.selectSingleNode(".//url[./text() = '" + baseUrl + "']");
					if (!urlNode) {
						return false;
					}

					if (urlNode.getAttribute(key) === "1") {
						return true;
					}
				}

				return false;
			}

			function addToLocalStorage(key) {
				var baseUrl = window.location.href.replace(/\Client\/.*/i, "\Client").toLowerCase();
				var urlsDom = new XmlDocument();
				var localStorageItem = window.localStorage.getItem("ArasAppUrlsKeys"); 
				if (localStorageItem) {
					urlsDom.loadXML(localStorageItem);
				} 
				else {
					urlsDom.loadXML("<appUrlKeys></appUrlKeys>");
				}

				var urlNode = urlsDom.selectSingleNode(".//url[./text() = '" + baseUrl + "']");
				if (!urlNode) {
					urlNode = urlsDom.createElement('url');
					urlNode.text = baseUrl;
					urlsDom.documentElement.appendChild(urlNode);
				}

				urlNode.setAttribute(key, "1");

				window.localStorage.setItem("ArasAppUrlsKeys", urlsDom.xml);
			}

			function checkBrowser(successCallback) {
				if (browserInfo.isCertified()) {
					successCallback();
					return;
				}

				var e;
				var allowProceedWithThisBrowser = true;
				showSystemRequirementsArea();

				//Display the Warning
				var errorTitleHtml = "Warning";
				try {
					errorTitleHtml = rm.getString("common.warning");
				}
				catch (e) {
					allowProceedWithThisBrowser = false;
				}
				document.getElementById("error_title").innerHTML = errorTitleHtml;

				//Display the browser name and message.
				var errorDescriptionHtml = "The browser has failed the browser requirement check. Please contact your system administrator to confirm your client against the platform requirements."
				try {
					var browserName = browserInfo.getBrowserName();
					errorDescriptionHtml = "You are currently running browser \"" + browserName + "\". " + errorDescriptionHtml;
					errorDescriptionHtml = rm.getFormatedString("system_requirements.certification_browser_notification", browserName);
				}
				catch (e) {
					allowProceedWithThisBrowser = false;
				}
				document.getElementById("error_description").innerHTML = errorDescriptionHtml;

				allowProceedWithThisBrowser = allowProceedWithThisBrowser && browserInfo.isSupported();
				if (!allowProceedWithThisBrowser) {
					return;
				}

				if (isInLocalStorage("NonBrowserCertified")) {
					successCallback();
					return;
				}

				//Display buttons to Continue or Cancel
				document.getElementById("warning_footer").style.display = "";
				document.getElementById("warning_checkbox_text").innerHTML = rm.getString("system_requirements.certification_browser_decision_label");
				document.getElementById("warning_continue_button").innerHTML = rm.getString("common.continue");
				document.getElementById("warning_continue_button").onclick = function(e) {
					if (document.getElementById("warning_checkbox").checked) {
						addToLocalStorage("NonBrowserCertified");
						document.getElementById("warning_checkbox").checked = false;
					}
					document.getElementById("warning_footer").style.display = "none";
					document.getElementById("SystemRequirements").style.display = "none";
					successCallback();
				}
				document.getElementById("warning_cancel_button").innerHTML = rm.getString("common.cancel");
				document.getElementById("warning_cancel_button").onclick = warningCancelButtonClick;
			}

			function warningCancelButtonClick() {
				if(browserInfo.isIe()) {
					window.open("blank.html", "_top");
					window.close();
				} else {
					window.open("about:blank", "_self");;
				}
			}
		</script>
	</head>
	<body>
		<table id="SystemRequirements" cellspacing="10" cellpadding="0" style="display: none;">
			<tr>
				<td style="width: 20%; padding-right:10px; text-align: right;">
					<img id="warning_image" alt="Warning!">
				</td>
				<td style="font: 12pt Arial; width:60%; text-align: left;">
					<div id="error_title"></div><br>
					<div id="error_description"></div>
					<br/>
					<div  id="warning_footer" style="display:none; text-align: left;font: 8pt Arial;">
						<input type="checkbox" id="warning_checkbox"/>
						<span  id="warning_checkbox_text"></span>
						<button id="warning_continue_button"></button>
						<button id="warning_cancel_button"></button>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>
