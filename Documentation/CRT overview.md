CRT overview
============

Definitions
-----------
* **_AML package_** - an aggregate of AML files, which is used as a delivery unit.
* **_Baseline_** - a pair of **_CodeTree.zip_** and **_DB.bak_** corresponding the current production Innovator instance state. (See **Baseline** section for more details).
* **_ConsoleUpgrade_** - an Innovator tool for AML packages import/export. Located by the 'AutomatedProcedures\tools\PackageImportExportUtilities\consoleUpgrade\consoleUpgrade.exe' path.
* **_CRT_** - stands for CustomerRepositoryTemplate.git repository, which contains branches for all released Innovator versions. All *\_work.git repositories are created from the corresponding branch from the **_CRT_**.
* **_Continuous Integration (CI)_** - _a **software development practice** where members of a team integrate their work frequently, usually each person integrates at least daily - leading to multiple integrations per day. Each integration is verified by an automated build (including test) to detect integration errors as quickly as possible. Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly. (Matrin Fowler)_.
* **_Continuous Delivery (CD)_** - an ability to get changes of all types including new features, configuration changes, bug fixes and experiments into production, or into the hands of users, safely and quickly in a sustainable way.
* **_Default.Settings.include_** - an XML file with settings for build/deployment procedure located by 'AutomatedProcedures\Default.Settings.include' path.
* **_Deployment Package_** - a zip archive containing delta, which can be applied to an Innovator instance to update it from the **_Last.Commit.Before.Current.Release_** state to a desirable state.
* **_DeploymentTool_** - a C# tool that was written to simplify setup, configuration and deployment process. Project is located in the 'AutomatedProcedures\src\DeploymentProcedure' directory.
* **_HEAD_** - a synonym of the current branch/tag in the repository.
* **_Instance_** - as a rule, it means Innovator instance.
* **_Instance.Config_** - an XML-like config for **_DeploymentTool_**, describing the Innovator instance structure and steps to apply to it.
* **_LanuageTool_** - an Innovator tool for applying Language packs. Located by the 'AutomatedProcedures\tools\LanguagePackManagementUtility\LanguageTool.exe' path.
* **_Last.Commit.Before.Current.Release_** - a **_Default.Settings.include_** property. ID of the last commit before current release. This is the commit that is used as a baseline to figure out which files were changed during current release. These files define code tree and AML packages changes, which should be deployed to a target server.
* **_Machine.Specific.include_** - an XML file with the same settings as in the **_Default.Settings.include_**, but overwriting them for the local machine. It's created automatically after run **_SetupInnovatorHere.bat_** first time. A path to it is generated as 'c:\\_machine_specific_includes\\**_Project.Prefix_**-**_HEAD_**.Settings.include'.
* **_Project.Prefix_** - a **_Default.Settings.include_** property means the name of the current repository which is used as a prefix for generated Innovator instance names, in order to not cause conflict if there are several CustomerRepositoryTemplate based repositories.
* **_SetupInnovatorHere.bat_** - a batch file which installs Innovator instance inside the repository. It uses the code tree directly from the repository and database, which is specified in the **_Machine.Specific.include_** file. You can find the batch file in the root of a repository.

Table of Contents
-----------------
* #### [Before start](#BeforeStart)
* #### [General CI/CD pipeline](#GeneralCICDPipeline)
* #### [Target Innovator instance lifecycle](#TargetInnovatorInstanceLifecycle)
* #### [Baseline](#Baseline)
* #### [Deployment package](#DeploymentPackage)
* #### [Deployment tool](#DeploymentTool)

<a name="BeforeStart"></a>Before start
--------------------------------------
Since the material in this document is based on basic understanding of how Git works, what are Continuous Integration and Conituous Delivery we highly recommend you to get familiarized with the following topics:
* At least first 3 chapters (Getting Started, Git Basics, Git Branching) of the Pro Git book: https://git-scm.com/book/en/v2
* Martin Fowler's article on **_Continuous Integration (CI)_**: https://martinfowler.com/articles/continuousIntegration.html
* **_Continuous Delivery (CD)_** basics on the https://continuousdelivery.com/

<a name="GeneralCICDPipeline"></a>General CI/CD pipeline
--------------------------------------------------------
What does **_Continuous Integration (CI)_** do?
* Validates development team work
* Makes build artifacts
* **Doesn't deploy anything to the end user**

What does **_Continuous Delivery (CD)_** do?
* Extends **Continuous Integration** to make sure that you can release new changes to your customers quickly in a sustainable way
* Provides reliable, repeatable automatic procedure of deployment

CI/CD key practices:
* Store all kind of data under version control system. (e. g. sources, tests, documentation, scripts etc)
* Code review
* Automate everything: code validation, tests, deployment
* Use the same deployment procedure at all stages: SIT, UAT, Pre-PROD, PROD

A CI/CD pipeline which is used in CRT is presented below:

![Image of CI/CD pipeline](images\CICDPipeline.png)

As you can see the pipeline consists of steps. In CRT all these steps logic is implemented in NAnt scripts, which in its turn run a lot of Aras (black boxes on the schema below) and 3rd party tools.

![Image of CI/CD pipeline tools](images\CICDPipelineTools.png)

<a name="TargetInnovatorInstanceLifecycle"></a>Target Innovator instance lifecycle
----------------------------------------------------------------------------------
Development and deployment are highly interrelated: deployment applies development results on one hand, and development relies on the latest deployment state on the other hand. An overall schema of how the development process flows into a deployment and vice versa is below:
![Image of development and deployment relationships](images\DevelopmentAndDeploymentRelationships.png)

Detailed workflow steps are:
1. At t0 moment, assume that it is a project start, we should have a working Innovator instance, **_Baseline_** taken from this instance and a clean git repository.
2. _If there is no Innovator instance for that moment it should be installed as clean Aras Innovator of a specific version, on which development will be based (e. g. 11.0 SP15, 12.0, etc)_.
3. A repository should be synchronized with current **_Baseline_**.
    * Commit code tree to the repository.
    * Export all AML packages and put them to the repository too.
    * Update **_Last.Commit.Before.Current.Release_**. It means that since that moment a **_Deployment Package_** will be generated based on just specified value (git point in the repository).
    * Update **_Baseline_** for CI both locally and on build environment to ensure that restored instance will correspond **_Last.Commit.Before.Current.Release_**.
4. Develop, develop and develop.
5. At t1 moment we are going to deploy some result since last deployment (**_Last.Commit.Before.Current.Release_**). It can be sprint results, some hotfixes etc.
6. First, we need to generate a **_Deployment Package_** based on **_Last.Commit.Before.Current.Release_**.
7. Apply **_Deployment Package_** to the instance.
8. Create a new **_Baseline_** once **_Deployment Package_** has been deployed. Return to 3rd step.

**The main idea of a deployment using CRT is to prevent all manual changes of production instance and do all stuff using a git repository.** In that case, the repository will be a mirror of all metadata from a production. It leads to simplifying development process and making deployment more reliable. In a way, you know the current production AML packages state and can change them appropriately.

<a name="Baseline"></a>Baseline
-------------------------------
To setup an Innovator instance we need two artifacts: CodeTree.zip and DB.bak, which together are called **_baseline_**. **Important! A baseline is stored outside of the repository.** As a rule, it's located somewhere in the file system. To specify the paths to a new baseline update two properties in the **_Machine.Specific.include_**:
* Path.To.CodeTree.Zip
* Path.To.DB.Bak

_CodeTree.zip_ is an archive, which contains all required Innovator libraries, HTML pages, images, icons, JavaScript files, etc. In other words, it is an archive created from the folder where Innovator is installed. The content of the archive is about the following:
* CodeTree.zip
    * AgentService
    * ConversionServer
    * Innovator
    * OAuthServer
    * SelfServiceReporting
    * VaultServer

_DB.bak_ is a backup of the current production Innovator database. To reduce size of a backup use the following SQL query (it shrinks database log file and uses compression):
```
DBCC SHRINKDATABASE(N'InnovatorSolutions'); BACKUP DATABASE [InnovatorSolutions] TO DISK = 'C:\\Database Backups\\InnovatorSolutions.bak' WITH NOFORMAT, INIT, COMPRESSION;
```

Any baseline must correspond to a certain point in the git repository (to a commit). For example, to start development based on Innovator XX SPYY, we set CleanInnovatorXXSPYY tag to the repository, and use baseline which we got from a clean installation of this version. 

The baseline should be updated after each production deployment.

<a name="DeploymentPackage"></a>Deployment package
--------------------------------------------------
_Deployment Package_ is a zip archive containing delta, which can be applied to an Innovator instance to update it from the **_Last.Commit.Before.Current.Release_** state to a desirable state.
![Image of deployment package delta calculation](images\DeploymentPackageDeltaCalculation.png)

The detailed content of the _Deployment Package_ is:
* Delta - is calculated using **git diff** between **_Last.Commit.Before.Current.Release_** and **_HEAD_**.
    * AmlDeploymentScripts delta
    * CodeTree delta
    * Imports/AML-packages delta
    * Language packs delta
* Deployment procedure - is taken by including necessary files from 'AutomatedProcedures' folder.

For more details see _GetGitDiffBetweenCurrentHeadStateAndLastRelease_ target in the 'AutomatedProcedures\NantScript.DeploymentPackageHelper.xml' file.
As a result, it provides a package that can be applied by a **_DeploymentTool_** or manually.

<a name="DeploymentTool"></a>Deployment tool
--------------------------------------------
It is a C# tool with CLI which allows us to Deploy or Cleanup Innovator instance, which is described in the XML config file. Through this config file, we can specify a distributed Innovator instance structure, including multi servers, some custom components, like web services, and set what packages should be applied.

![General schema of deployment tool](images\DeploymentToolGenaralSchema.png)

By default **_DeploymentTool_** is being built during package preparation step. The output is located in the 'AutomatedProcedures\\tools\\Aras.Deployment.Tool'. You can operate this tool manually if you need to perform deployment or cleanup using the following arguments (can be also retrieved by using --help switch):
 * -t, --targets - a comma separated list of targets you want to run. (e. g. -t Deploy,Cleanup) Available options are Deploy and Cleanup.
 * -c, --config  - a path to an XML file, describing desirable Innovator instance structure and steps to apply to it. We usually call it **_Instance.Config_**.
 * --help - display help information.
 * --version - display version information

For example:
```
Aras.Deployment.Tool.exe -t Deploy -c InstanceTemplate.deploy.xml
```
or
```
Aras.Deployment.Tool.exe --targets Deploy --config "C:\Instance Configs\company.uat.deploy.xml"
```

Some **_DeploymentTool_** implementation details:
* It's written on .NET Framework using C#.
* It can handle multiple servers Innovator instance. See:

![How deployment tool hanldes multiple servers](images\DeploymentToolMultiServer.png)

* Microsoft.Web.Administration.dll is used to handle IIS applications on remote Windows servers. It's based on the COM mechanisms and that's why it requires Windows Firewall adjustment:
```
netsh advfirewall firewall add rule name="Aras.Deployment Remote Web Administration" description="Allows manage IIS remotely for Aras.Deployment tools" dir=in action=allow enable=yes program="%systemroot%\system32\dllhost.exe" protocol=tcp localport=rpc profile=domain
```
* It uses _robocopy_ if it exists to operate files copying. Otherwise, it uses Microsoft.Experimental.IO.dll at the local system and standard System.IO at remote servers.
* It uses UNC paths to handle components. If _ServerName_ property from **_Instance.Config_** is not a local machine, it combines _ServerName_ with _InstallationPath_ in the following way. \\\\_ServerName_\\_InstallationPath_ (also, it replaces ':' character to '$' sign). For example, if _ServerName_ is 'application.production.net' and _InstallationPath_ is 'C:\\Program Files (x86)\\Aras\\Innovator\\Innovator' then the path to the component will be '\\\\application.proudction.net\\C$\\Program Files (x86)\\Aras\\Innovator\\Innovator'. Note that the user as which **_DeploymentTool_** is running should be able to access Admin Share at _ServerName_.
* Before applying the package it runs some health checks validations (ping _ServerName_, check write permissions to _InstallationPath_ folder, IIS application working condition, etc).
