FROM mcr.microsoft.com/windows:1809

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

ENV FIREFOXESR_VERSION=45.3.0 \
    MSBUILDTOOLS_VERSION=14.0.25420.1 \
    NODEJS_VERSION=8.9.4 \
    SLAVE_FILENAME=slave.jar \
    REMOTING_VERSION=3.23

# Disable IE customization windows during first run to allow client unit tests
RUN New-Item -Path "\"HKLM:\\SOFTWARE\\Policies\\Microsoft\\Internet Explorer\\Main\"" -Force; \
    New-ItemProperty -Path "\"HKLM:\\SOFTWARE\\Policies\\Microsoft\\Internet Explorer\\Main\"" -Name "DisableFirstRunCustomize" -Value 1 -Force

# Setup NetFx3
RUN Invoke-WebRequest -UseBasicParsing -Uri https://dotnetbinaries.blob.core.windows.net/dockerassets/microsoft-windows-netfx3-1809.zip -OutFile microsoft-windows-netfx3.zip; \
    Expand-Archive microsoft-windows-netfx3.zip; \
    Remove-Item -Force microsoft-windows-netfx3.zip; \
    Add-WindowsPackage -Online -PackagePath .\\microsoft-windows-netfx3\\microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab; \
    Remove-Item -Force -Recurse microsoft-windows-netfx3

# Enable .Net and IIS Features
RUN Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServer,IIS-ASP,IIS-ASPNET,IIS-ASPNET45,IIS-NetFxExtensibility,IIS-ISAPIExtensions,IIS-ISAPIFilter,IIS-WindowsAuthentication -All

# Download chocolatey
RUN Set-ExecutionPolicy Unrestricted -Force; \
    Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')

# Install Java required for Jenkins Agent
RUN choco install jre8 -y

# Get the Slave from the Jenkins Artifacts Repository
RUN Invoke-WebRequest "https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/$env:REMOTING_VERSION/remoting-$env:REMOTING_VERSION.jar" -OutFile $env:SLAVE_FILENAME -UseBasicParsing; \
    Invoke-WebRequest "https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/$env:REMOTING_VERSION/remoting-$env:REMOTING_VERSION.jar.sha1" -OutFile "\"$env:SLAVE_FILENAME.sha1\"" -UseBasicParsing; \
    if ((Get-FileHash $env:SLAVE_FILENAME -Algorithm SHA1).Hash -ne $(Get-Content "\"$env:SLAVE_FILENAME.sha1\"")) { exit 1 };

#Copy launch script used by entry point
COPY "AutomatedProcedures\\PowershellScripts\\slave-launch.ps1" "slave-launch.ps1"

ENTRYPOINT .\slave-launch.ps1

# Install chocolatey packages
RUN choco install git.install -y
# Since Google maintains only last version of Google Chrome
RUN choco install googlechrome -y
RUN choco install netfx-4.5.2-devpack -y
RUN choco install microsoft-build-tools --version $env:MSBUILDTOOLS_VERSION -y
RUN choco install nodejs --version $env:NODEJS_VERSION -y
RUN choco install firefoxesr --version $env:FIREFOXESR_VERSION -y

# Enhance a limit for a filename to 4096 characters to avaoid long path issues
# Allow Windows to handle paths more than 260 characters
RUN & 'C:\\Program Files\\Git\\bin\\git.exe' config --global core.longpaths true;